package pi2go.runtime;

import vacuumrobotspecification.pi2go.runtime.observers.RobotObserver;
import vacuumrobotspecification.runtime.MQTTDistributedRunconfig_Robot;

public class MQTTDistributedRunconfig_Robot_PI2GO extends MQTTDistributedRunconfig_Robot {

	@Override
	protected void registerObservers() {
		// addScenarioObserver(new RobotObserver());
		addScenarioObserver(new RobotObserver(getAdapter()));
	}

	@Override
	protected void setUpGUI(String name) {
		// do nothing
	}

}
