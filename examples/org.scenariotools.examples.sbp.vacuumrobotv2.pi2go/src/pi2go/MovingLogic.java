package pi2go;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import pi2go.runtime.MQTTDistributedRunconfig_Robot_PI2GO;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.adapter.RuntimeAdapter;
import sbp.specification.events.Message;
import sbp.specification.role.Role;
import topology.Room.CardinalDirection;
import topology.RoomsTopo;
import vacuumrobot.VacuumRobot;
import vacuumrobot.VacuumRobotController;
import vacuumrobotspecification.runtime.VacuumrobotSpecificationObjectSystem;

/**
 * MoveToNextRoom Program for Pi2Go
 * 
 * @author Joel Greenyer
 * 
 *         modified by Jianwei Shi
 * 
 *         Go to next crossing (move to next room) using line follower program,
 *         the robot will stop at the next crossing
 */
public class MovingLogic {

	// basic setting
	public static Pi2Go pi2go_no_2 = new Pi2Go();

	// speed when the robot goes straight
	static int goSpeed = 40;

	// speed when the robot turns left or right
	// static int spinSpeed = 40;// TODO: PARAMETER NOT SURE
	static int spinSpeed = 25;// to make the turning more smooth 30

	// the direction of move to next room
	public enum RobotMovingDirection {
		FORWARD, BACKWARD, LEFT, RIGHT
	};

	// create GPIO controller
	final static GpioController gpio = GpioFactory.getInstance();

	// initialise GPIO input
	final static GpioPinDigitalInput lineRight_gpio = gpio.provisionDigitalInputPin(RaspiPin.GPIO_03, "lineRight",
			PinPullResistance.PULL_UP);
	final static GpioPinDigitalInput lineLeft_gpio = gpio.provisionDigitalInputPin(RaspiPin.GPIO_01, "lineLeft",
			PinPullResistance.PULL_UP);

	public static Map map = new HashMap<vacuumrobot.Room, topology.Room>();

	// variables for sending scenario message (specification level events)
	public static final String SENDER_NAME = "sender";
	public static final String RECEIVER_NAME = "receiver";

	/**
	 * the move logic for robot (following the line)
	 * 
	 * @param pi
	 *            object of the robot
	 * @throws InterruptedException
	 */
	public static void runProgram(Pi2Go pi) throws InterruptedException {
		// System.out.println("runProgram called.");
		if (!pi.isCrossingReached() && !pi.isTurning()) {
			if (pi.isLineRight() && !pi.isLineLeft()) {
				pi.spinRight(spinSpeed);
				System.out.println("Turning Right.....");
			} else if (pi.isLineLeft() && !pi.isLineRight()) {
				pi.spinLeft(spinSpeed);
				System.out.println("Turning Left.....");
			} else if (pi.isLineRight() && pi.isLineLeft()) {
				// to go straight for a short distance for good turning
				pi.go(40);
				Thread.sleep(510);
				// Thread.sleep(520);
				// Thread.sleep(300);
				pi.setCrossingReached(true);
				pi.go(0);
				System.out.println("set speed to 0!");
			} else {
				pi.go(goSpeed);
				System.out.println("Go straight!");
			}
		}

	}

	// create gpio pin listener for leftSensor
	static GpioPinListenerDigital sensorLeftListener = new GpioPinListenerDigital() {

		@Override
		public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
			// display pin state on console
			System.out.println(" --> line LEFT  STATE CHANGE: " + event.getPin() + " = " + event.getState());
			if (event.getState() == PinState.LOW)
				pi2go_no_2.setLineLeft(false);
			if (event.getState() == PinState.HIGH)
				pi2go_no_2.setLineLeft(true);
			try {
				runProgram(pi2go_no_2);
			} catch (InterruptedException e) {
				System.out.println("Errors regarding Thread!");
				e.printStackTrace();
			}
		}

	};

	// create gpio pin listener for rightSensor
	static GpioPinListenerDigital sensorRightListener = new GpioPinListenerDigital() {

		@Override
		public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
			// display pin state on console
			System.out.println(" --> line RIGHT  STATE CHANGE: " + event.getPin() + " = " + event.getState());
			if (event.getState() == PinState.LOW)
				pi2go_no_2.setLineRight(false);
			if (event.getState() == PinState.HIGH)
				pi2go_no_2.setLineRight(true);
			try {
				runProgram(pi2go_no_2);
			} catch (InterruptedException e) {
				System.out.println("Errors regarding Thread!");
				e.printStackTrace();
			}
		}

	};

	/**
	 * 
	 * To move to the next room following the line
	 * 
	 * @param pi
	 *            object of the robot
	 * @throws InterruptedException
	 */
	@SuppressWarnings("unchecked")
	public static void MoveToNextRoom(Pi2Go pi, RobotMovingDirection direction, RuntimeAdapter runtimeAdapter,
			Message messageEvent, vacuumrobot.Room room) throws InterruptedException {

		if (direction != null) {
			System.out.println("<--Pi4J--> GPIO MoveToNextRoom ... started.");

			// Initialisation
			pi.setCrossingReached(false);
			switch (direction) {
			case FORWARD:
				// do nothing
				break;

			case BACKWARD:
				// make a turn of 180 degrees
				SpinNightyDegrees(pi, true);
				SpinNightyDegrees(pi, true);
				break;

			case LEFT:
				// spin left for 90 degrees
				SpinNightyDegrees(pi, true);
				break;

			case RIGHT:
				// spin right for 90 degrees
				SpinNightyDegrees(pi, false);
				break;

			}

			// move robot
			runProgram(pi);
			// keep program running for 50 * 100 ms (5s), show progress in log
			// console
			// TODO: Tuning...
			for (int i = 0; i < 500; i++) { // this time is great enough for the
											// robot to cross a grid
				Thread.sleep(10);

				// System.out.println("cycle " + i);

				// if the robot has reached the crossing, stop then program
				if (pi.isCrossingReached()) {
					break;
				}
			}

			// send scenario message
			// robot->robotCtrl.arrivedInRoom(room)
			Object robotCtrlObj = messageEvent.getSender().getBinding(); // robotCtrl
			Object robotObj = messageEvent.getReceiver().getBinding(); // robot

			Role<VacuumRobot> newSender = new Role<VacuumRobot>(VacuumRobot.class, SENDER_NAME); // robot
			newSender.setBinding((VacuumRobot) robotObj);
			Role<VacuumRobotController> newReceiver = new Role<VacuumRobotController>(
					(Class<VacuumRobotController>) robotCtrlObj.getClass(), RECEIVER_NAME); // robotCtrl
			newReceiver.setBinding((VacuumRobotController) robotCtrlObj );

			String msgStrArrivedInRoom = "arrivedInRoom";
			List<Object> roomParameters = new ArrayList<Object>();
			roomParameters.add(room);
			Message arrivedInRoomMsg = new Message(false, false, newSender, newReceiver, msgStrArrivedInRoom,
					roomParameters);

			runtimeAdapter.receive(arrivedInRoomMsg);
			runtimeAdapter.publish(arrivedInRoomMsg);
			

			// stirct committed robot->robotCtrl.setCharge()
			String msgStrSetCharge = "SETCharge";
			List<Object> chargeParameters = new ArrayList<Object>();

			Message setChargeMsg = null;
			if (room.isHasChargingStation()) {
				// stirct comitted robot->robotCtrl.setCharge(robot.maxCharge)
				VacuumRobot robot = newSender.getBinding();
				int maxCharge = robot.getMaxCharge();
				chargeParameters.add(maxCharge);
				setChargeMsg = new Message(true, false, newSender, newReceiver, msgStrSetCharge, chargeParameters);
			} else {
				// strict comitted robot->robotCtrl.setCharge(robotCtrl.charge - 1)
				VacuumRobotController robotCtrl = newReceiver.getBinding();
				// uncomment it with new modelling
				int charge = robotCtrl.getCharge();
				//int charge = 2; // ONLY example! comment it afterwards and
								// use the above
				chargeParameters.add(charge-1);
				setChargeMsg = new Message(true, false, newSender, newReceiver, msgStrSetCharge, chargeParameters);

			}

			runtimeAdapter.receive(setChargeMsg);
			runtimeAdapter.publish(setChargeMsg);
			
			// System.out.println("done, turning wheels off.");

			// stop the pi robot
			pi.shutdown();

		}

	}

	/**
	 * Driver of spin function
	 * 
	 * @param isLeft
	 *            if isLeft == true, spin left for 90 degrees else spin right
	 *            for 90 degrees
	 * @throws InterruptedException
	 */
	public static void SpinNightyDegrees(Pi2Go pi2go, boolean isLeft) throws InterruptedException {
		pi2go.setTurning(true);
		// int spinNightyDegreesSpeed = 40;
		int spinNightyDegreesSpeed = 30;

		System.out.println("SPIN 90 DEGREES Started!");

		boolean isLine = false;
		if (isLeft) {
			pi2go.spinLeft(spinNightyDegreesSpeed);
		} else {
			pi2go.spinRight(spinNightyDegreesSpeed);
		}

		// make timing enough to turn for 90 degrees
		// int timing = 65;
		int totalTime = 1500;
		int unitTime = 13;
		for (int i = 0; i < totalTime / unitTime; i++) { // here 0.61 s with
															// spinSpeed 40
			// update isLine
			if (isLeft) {
				isLine = pi2go.isLineLeft();
			} else {
				isLine = pi2go.isLineRight();
			}

			// System.out.println("isLine: "+isLine);
			Thread.sleep(unitTime);
			if (i > (totalTime / unitTime) * 0.3) {// sometime later, maybe
													// turned for 30 degrees...
				// if the robot is turned to a line, stop the turning
				// for example, if the robot is turning to left
				// at first, isLineLeft == false
				// then, isLineLeft == true, THIS TIME the turning will be
				// terminated
				// next, isLineLeft == false,
				// System.out.println(isLine + " " + isLineLast);
				if (isLine) {
					// then we assume the turning is finished
					// change the cardinal direction of the robot
					CardinalDirection dir = pi2go.getRobotCardinalDirection();
					if (isLeft) {
						if (dir == CardinalDirection.NORTH)
							dir = CardinalDirection.WEST;
						else if (dir == CardinalDirection.WEST)
							dir = CardinalDirection.SOUTH;
						else if (dir == CardinalDirection.SOUTH)
							dir = CardinalDirection.EAST;
						else if (dir == CardinalDirection.EAST)
							dir = CardinalDirection.NORTH;
					} else {
						if (dir == CardinalDirection.NORTH)
							dir = CardinalDirection.EAST;
						else if (dir == CardinalDirection.WEST)
							dir = CardinalDirection.NORTH;
						else if (dir == CardinalDirection.SOUTH)
							dir = CardinalDirection.WEST;
						else if (dir == CardinalDirection.EAST)
							dir = CardinalDirection.SOUTH;
					}

					pi2go.setRobotCardinalDirection(dir);
					// terminate turning
					break;
				}
			}
			// System.out.println("spin cycle " + i);
		}
		pi2go.setTurning(false);
		System.out.println("SPIN 90 DEGREES Ended!");
	}

	public static void GPIOInitialisation() {
		// register gpio pin listener
		lineLeft_gpio.addListener(sensorLeftListener);
		lineRight_gpio.addListener(sensorRightListener);
	}

	public static void GPIOShutdown() {
		lineLeft_gpio.removeAllListeners();
		lineRight_gpio.removeAllListeners();

		// stop all GPIO activity/threads by shutting down the GPIO controller
		// (this method will forcefully shutdown all GPIO monitoring threads and
		// scheduled tasks)
		gpio.shutdown();
	}

	// Invoker
	public static void main(String[] args) throws InterruptedException {

		/**
		 * a small example without ScenarioTools
		 */
		// GPIOInitialisation();
		// // create room topology
		// RoomsTopo threeRoomsTopo = new RoomsTopo();
		// threeRoomsTopo.createThreeRooms();
		// List<Room> threeRooms = threeRoomsTopo.getRooms();
		//
		// // calculate direction which the robot should go
		// RobotMovingDirection robotDir;
		// robotDir = DirectionUtil.DecideDirection(CardinalDirection.EAST,
		// threeRooms.get(2), threeRooms.get(0));
		//
		// MoveToNextRoom(pi2go_no_2, robotDir);

		// SpinNightyDegrees(pi2go_no_2, true);
		// SpinNightyDegrees(pi2go_no_2, true);
		//
		// pi2go_no_2.shutdown();
		// GPIOShutdown();

		/**
		 * an example with ScenarioTools
		 */

		// VacuumrobotSpecificationObjectSystem objectSystem =
		// VacuumrobotSpecificationObjectSystem.getInstance();

		// Generate a topology and map it the the model in the scenarioTools
		// 1 - Generate a topology
		RoomsTopo threeRoomsTopo = new RoomsTopo();
		threeRoomsTopo.createThreeRooms();
		List<topology.Room> threeRooms = threeRoomsTopo.getRooms();

		// 2 - Mapping
		List<vacuumrobot.Room> eRooms = VacuumrobotSpecificationObjectSystem.getInstance().VacuumRobotSystem.getRoom();
		for (vacuumrobot.Room eRoom : eRooms) {
			if (eRoom.getName().equals("Room01")) {
				map.put(eRoom, threeRooms.get(0));
			} else if (eRoom.getName().equals("Room02")) {
				map.put(eRoom, threeRooms.get(1));
			} else if (eRoom.getName().equals("Room03")) {
				map.put(eRoom, threeRooms.get(2));
			}
		}

		// set default currentRoom to room01
		pi2go_no_2.setCurrentRoom(threeRooms.get(0));

		// SpecificationRunconfig.run(DistributedRunconfig_Robot_PI2GO.class);

		SpecificationRunconfig.run(MQTTDistributedRunconfig_Robot_PI2GO.class);

	}

}
