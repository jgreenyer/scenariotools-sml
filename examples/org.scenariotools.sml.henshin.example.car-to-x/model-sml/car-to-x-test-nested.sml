import "car-to-x.ecore"

specification CarToX {

	domain cartox
	
	controllable {
		Car
		StreetSectionControl
		ObstacleBlockingOneLaneControl
	}

	non-spontaneous events {
		Car.setApproachingObstacle
		Car.entersNarrowPassage
		Car.hasLeftNarrowPassage
	}

	collaboration CarsRegisterWithObstacleControl2 {

		dynamic role Environment env
		dynamic role Car car
		dynamic role Obstacle obstacleControl
		dynamic role LaneArea currentArea
		dynamic role LaneArea nextArea

		/*
		 * A car that has been told that it may pass the obstacle will enter the narrow passage
		 */
		assumption scenario CarDrivesIntoNarrowPassageIfItMayPassObstacleVariable bindings [
		] {
			alternative {
				env -> car.carMovesToNextAreaOnOvertakingLane()
				alternative {
					strict eventually env -> car.carMovesToNextArea()
					strict eventually env -> car.carMovesToNextArea()
					while {
						strict eventually env -> car.carMovesToNextArea()
						var Car i
					} constraints [
						forbidden env -> car.carMovesToNextAreaOnOvertakingLane()
						forbidden env -> car.carMovesToNextArea()
					]
				}
				while {
					alternative {
						strict eventually env -> car.carMovesToNextArea()
						var Car i
					}
				}
			} constraints [
				ignore env -> car.carMovesToNextAreaOnOvertakingLane()
			]
		}

	}

}