import "car-to-x.ecore"

specification CarToX {
	
	domain cartox
	
	controllable {
		Car
		StreetSectionControl
		ObstacleBlockingOneLaneControl
	}
	
	non-spontaneous events { 
		Car.setApproachingObstacle
		Car.entersNarrowPassage
		Car.hasLeftNarrowPassage
	}
	
	
	collaboration CarDriving{ // the primary purpose of these scenarios is to enable the car movement events   
		
		dynamic role Environment env
		dynamic role Car car
		dynamic role LaneArea currentArea
		dynamic role LaneArea oppositeArea
		
		guarantee scenario CarMovesToNextArea 
		{
			env -> car.carMovesToNextArea()
		}
		
		guarantee scenario CarChangesToOppositeArea 
		{
			env -> car.carChangesToOppositeArea()
		}

		guarantee scenario CarMovesToNextAreaOnOvertakingLane 
		{
			env -> car.carMovesToNextAreaOnOvertakingLane()
		}		

		guarantee scenario PassApproachingCar 
		{
			env -> car.passApproachingCar()
		}

		guarantee scenario Crash
		bindings [
			currentArea = car.inArea
			oppositeArea = currentArea.nextTo
		] {
			env -> car.crash()
			interrupt [oppositeArea.obstacle == null]
			violation [true]
		}		
		
	}
	
	
	collaboration CarApproachingObstacleAssumptions{
	
		dynamic role Environment env
		dynamic role Car car
		dynamic role LaneArea currentArea
		dynamic role LaneArea nextArea
		dynamic role LaneArea nextToNextArea
		dynamic role Obstacle obstacle

		/*
		 * When a car approaches an obstacle on the blocked lane, an according event will occur
		 */
		assumption scenario ApproachingObstacleOnBlockedLaneAssumption
		bindings [
			currentArea = car.inArea
			nextArea = currentArea.next
			obstacle = nextArea.obstacle
		]{
			env->car.carMovesToNextArea()
			interrupt [obstacle == null]
			strict eventually env->car.setApproachingObstacle(obstacle)			
		} constraints [
			forbidden env->car.carMovesToNextArea()
			forbidden env->car.carMovesToNextAreaOnOvertakingLane()
		]
		
		assumption scenario ApproachingObstacleOnBlockedLaneAssumption_OppositeDirection
		bindings [
			currentArea = car.inArea
			nextArea = currentArea.next
			nextToNextArea = nextArea.nextTo
			obstacle = nextToNextArea.obstacle
		]{
			env->car.carMovesToNextArea()
			interrupt [obstacle == null]
			strict eventually env->car.setApproachingObstacle(obstacle)			
		} constraints [
			forbidden env->car.carMovesToNextArea()
			forbidden env->car.carMovesToNextAreaOnOvertakingLane()
		]

		/*
		 * When a car approaches an obstacle but is currently driving on the lane that is
		 * not blocked, an according event will occur 
		 */
		assumption scenario ApproachingObstacleOnBlockedLaneAssumption_Overtaking
		bindings [
			currentArea = car.inArea
			nextArea = currentArea.previous
			nextToNextArea = nextArea.nextTo
			obstacle = nextToNextArea.obstacle
		]{
			env->car.carMovesToNextAreaOnOvertakingLane()
			interrupt [obstacle == null]
			strict eventually env->car.setApproachingObstacle(obstacle)			
		} constraints [
			forbidden env->car.carMovesToNextArea()
			forbidden env->car.carMovesToNextAreaOnOvertakingLane()
		]

		assumption scenario ApproachingObstacleOnBlockedLaneAssumption_Overtaking_OppositeDirection
		bindings [
			currentArea = car.inArea
			nextArea = currentArea.previous
			obstacle = nextArea.obstacle
		]{
			env->car.carMovesToNextAreaOnOvertakingLane()
			interrupt [obstacle == null]
			strict eventually env->car.setApproachingObstacle(obstacle)			
		} constraints [
			forbidden env->car.carMovesToNextArea()
			forbidden env->car.carMovesToNextAreaOnOvertakingLane()
		]

	}
	
	collaboration CarEntersOrLeavesNarrowPassage {
				
		dynamic role Environment env
		dynamic role Car car
		dynamic role LaneArea currentArea
		dynamic role LaneArea nextToArea
		dynamic role LaneArea nextToPrevArea


		
		/*
		 * When a car enters the narrow passage area,
		 * a corresponding event "entersNarrowPassage" will occur.
		 */
		assumption scenario CarEntersNarrowPassage
		bindings [
			currentArea = car.inArea
			nextToArea = currentArea.nextTo
			nextToPrevArea = nextToArea.previous
		]{
			env->car.carMovesToNextAreaOnOvertakingLane()
			interrupt [nextToArea.obstacle == null || nextToPrevArea.obstacle != null]
			strict eventually env->car.entersNarrowPassage()
		}

		assumption scenario CarEntersNarrowPassage_OppositeDirection
		bindings [
			currentArea = car.inArea
			nextToArea = currentArea.nextTo
			nextToPrevArea = nextToArea.next
		]{
			env->car.carMovesToNextArea()
			interrupt [nextToArea.obstacle == null || nextToPrevArea.obstacle != null]
			strict eventually env->car.entersNarrowPassage()
		}
		
		/*
		 * When a car has left the narrow passage area,
		 * a corresponding event "hasLeftNarrowPassage" will occur.
		 */
		 assumption scenario CarHasLeftNarrowPassage
		 bindings [
		 	currentArea = car.inArea
		 	nextToArea = currentArea.nextTo
		 	nextToPrevArea = nextToArea.previous
		 ] {
		 	env -> car.carMovesToNextAreaOnOvertakingLane()
		 	interrupt [nextToArea.obstacle != null || nextToPrevArea.obstacle == null]
		 	strict eventually env -> car.hasLeftNarrowPassage()
		 } constraints [
		 	forbidden env -> car.carChangesToOppositeArea()
		 ]
		 
		 assumption scenario CarHasLeftNarrowPassage_OppositeDirection
		 bindings [
		 	currentArea = car.inArea
		 	nextToArea = currentArea.nextTo
		 	nextToPrevArea = nextToArea.next
		 ] {
		 	env -> car.carMovesToNextArea()
		 	interrupt [nextToArea.obstacle != null || nextToPrevArea.obstacle == null]
		 	strict eventually env -> car.hasLeftNarrowPassage()
		 } constraints [
		 	forbidden env -> car.carChangesToOppositeArea()
		 ]
	}

	collaboration CarsRegisterWithObstacleControl {

		dynamic role Environment env
		dynamic role Car car
		dynamic role Car nextCar
		dynamic role Dashboard dashboard
		dynamic role Obstacle obstacle
		dynamic role ObstacleBlockingOneLaneControl obstacleControl
		dynamic role LaneArea currentArea
		dynamic role LaneArea nextArea


		/*
		 * Stop or go must be shown to the driver when the car approaches an obstacle
		 */
		guarantee scenario ControlStationAllowsCarToEnterNarrowPassageOrNot
		bindings [
			obstacle = car.approachingObstacle
			obstacleControl = obstacle.controlledBy
			dashboard = car.dashboard
		]{
			env->car.setApproachingObstacle(*)			
			strict urgent car->obstacleControl.register()
			alternative {
				strict urgent obstacleControl->car.enteringAllowed()
			} or {
				strict urgent obstacleControl->car.enteringDisallowed()
			}
		}
		

//		/*
//		 * Stop or go must be shown to the driver when the car approaches an obstacle
//		 */
//		guarantee scenario ControlStationAllowsCarToEnterNarrowPassageOrNot
//		bindings [
//			obstacle = car.approachingObstacle
//			obstacleControl = obstacle.controlledBy
//			dashboard = car.dashboard
//		]{
//			env->car.setApproachingObstacle(*)			
//			strict eventually car->obstacleControl.register()
//			alternative [obstacleControl.carsOnNarrowPassageLaneAllowedToPass.isEmpty()]{
//				strict eventually obstacleControl->car.enteringAllowed()
//				strict car->dashboard.showGo()
//			} or [!obstacleControl.carsOnNarrowPassageLaneAllowedToPass.isEmpty()]{
//				strict eventually obstacleControl->car.enteringDisallowed()
//				strict car->dashboard.showStop()
//			}
//		}
		
		/*
		 * The obstacle control tells the next car that it may now enter the
		 * narrow passage
		 */
		guarantee scenario ControlStationTellsNextRegisteredCarToAdvance
		bindings [
			dashboard = car.dashboard
			obstacle = car.approachingObstacle
			obstacleControl = obstacle.controlledBy
			nextCar = obstacleControl.registeredCarsWaiting.first()
		] {
			env -> car.hasLeftNarrowPassage()
			interrupt [nextCar == null]
			strict urgent obstacleControl -> nextCar.enteringAllowed()
		}

		/*
		 * A car that has been told to wait may eventually continue driving and pass the obstacle
		 */
		guarantee scenario CarEventuallyMayPassObstacle {
			obstacleControl -> car.enteringDisallowed()
			strict monitored eventually obstacleControl -> car.enteringAllowed()
		}

		/*
		 * Drivers adhere to what they are told by the obstacle control
		 */
		assumption scenario CarWaitsWhileNotBeingAllowedIntoTheNarrowPassage
		bindings [
			env = car.environment // ugly workaround
		] {
			obstacleControl -> car.enteringDisallowed()
			obstacleControl -> car.enteringAllowed()		 	
		} constraints [
			forbidden env -> car.carMovesToNextArea()
			forbidden env -> car.carMovesToNextAreaOnOvertakingLane()
			//forbidden env -> car.carChangesToOppositeArea()
		]

		/*
		 * A car that has been told that it may pass the obstacle will enter the narrow passage
		 */		
		assumption scenario CarDrivesIntoNarrowPassageIfItMayPassObstacle
		bindings [
			currentArea = car.inArea
			nextArea = currentArea.next
			env = car.environment // ugly hack
		] {
			obstacleControl -> car.enteringAllowed()
			interrupt [car.onLane != car.drivingInDirectionOfLane]
			alternative [nextArea.obstacle == null] {
				strict eventually env -> car.carMovesToNextArea()
			} or [nextArea.obstacle != null] {
				strict eventually env -> car.carChangesToOppositeArea()
				strict eventually env -> car.carMovesToNextAreaOnOvertakingLane()
			}
		}		

		assumption scenario CarDrivesIntoNarrowPassageIfItMayPassObstacle_OppositeDirection
		bindings [
			currentArea = car.inArea
			nextArea = currentArea.previous
			env = car.environment // ugly hack
		] {
			obstacleControl -> car.enteringAllowed()
			interrupt [car.onLane == car.drivingInDirectionOfLane]
			alternative [nextArea.obstacle == null] {
				strict eventually env -> car.carMovesToNextAreaOnOvertakingLane()
			} or [nextArea.obstacle != null] {
				strict eventually env -> car.carChangesToOppositeArea()
				strict eventually env -> car.carMovesToNextArea()
			}
		}		

		/*assumption scenario CarEventuallyPassesObstacle
		bindings [
			env = car.environment // ugly hack
		] {
			obstacleControl -> car.enteringAllowed()
			alternative {
				strict eventually env -> car.carChangesToOppositeArea()
				alternative {
					strict eventually env -> car.carMovesToNextArea()
					strict eventually env -> car.carMovesToNextArea()					
				} or {			
					strict eventually env -> car.carMovesToNextAreaOnOvertakingLane()
					strict eventually env -> car.carMovesToNextAreaOnOvertakingLane()
				}
			} or {
				strict eventually env -> car.carMovesToNextArea()
				strict eventually env -> car.carMovesToNextArea()					
			} or {			
				strict eventually env -> car.carMovesToNextAreaOnOvertakingLane()
				strict eventually env -> car.carMovesToNextAreaOnOvertakingLane()
			}
		}*/		
	}
	
	/*
	 * Turn on and off lights on the dashboard
	 */	
	collaboration ShowStopOrGoLights{

		dynamic role Environment env
		dynamic role Car car
		dynamic role Driver driver
		dynamic role Dashboard dashboard
		dynamic role ObstacleBlockingOneLaneControl obstacleControl
	
		/*
		 * Show stop or go to the driver when approaching the obstacle before actually
		 * reaching it
		 */
		guarantee scenario DashboardOfCarApproachingOnBlockedLaneShowsStopOrGo
		bindings [
			driver = car.driver
			dashboard = car.dashboard
		]{
			env->car.setApproachingObstacle(*)			
			alternative{
				strict car->dashboard.showGo()
			} or {
				strict car->dashboard.showStop()
			}
			env->car.obstacleReached()			
		}
		
		/*
		 * The dashboard shows a go or a stop light as reaction to the obstacle
		 * controls response after registering
		 */
		guarantee scenario DashboardShowsGoLight
		bindings [
			dashboard = car.dashboard
		] {
			obstacleControl -> car.enteringAllowed()
			strict urgent car -> dashboard.showGo()
		}
		
		guarantee scenario DashboardShowsStopLight
		bindings [
			dashboard = car.dashboard
		] {
			obstacleControl -> car.enteringDisallowed()
			strict urgent car -> dashboard.showStop()
		}
		
		/*
		 * When the car orders the dashboard to how GO,
		 * the GO light must be turned ON and
		 * the STOP light must be turned OFF
		 */
/*/		guarantee scenario showGo{
				car->dashboard.showGo()			
				strict urgent car->dashboard.setGoLight(true)
				strict urgent car->dashboard.setStopLight(false)
		} */
		
		/*
		 * When the car orders the dashboard to how STOP,
		 * the GO light must be turned OFF and
		 * the STOP light must be turned ON
		 */
/*		guarantee scenario showStop{
				car->dashboard.showStop()			
				strict urgent car->dashboard.setGoLight(false)
				strict urgent car->dashboard.setStopLight(true)
		} */
		
		
	}
	
}