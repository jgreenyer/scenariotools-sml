import "car-to-x.ecore"

specification CarToX {

	domain cartox
	
	controllable {
		Car
		StreetSectionControl
		ObstacleBlockingOneLaneControl
	}

	non-spontaneous events {
		Car.setApproachingObstacle
		Car.entersNarrowPassage
		Car.hasLeftNarrowPassage
	}

	collaboration CarApproachingObstacleAssumptions {

		dynamic role Environment env
		dynamic role Car car
		dynamic role LaneArea currentArea
		dynamic role LaneArea nextArea
		dynamic role Obstacle obstacle

		assumption scenario ApproachingObstacleOnBlockedLaneAssumption bindings [
			currentArea = car.inArea
			nextArea = currentArea.next
			obstacle = nextArea.obstacle
		] {
			env -> car.carMovesToNextArea()
			strict eventually  env -> car.setApproachingObstacle(obstacle)
		} constraints [
			forbidden env -> car.carMovesToNextArea()
			forbidden env -> car.carMovesToNextAreaOnOvertakingLane()
		]
	}
}