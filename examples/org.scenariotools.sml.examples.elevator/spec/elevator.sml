import "../model/elevator.ecore"

specification ElevatorSpecification {

	/*
	 * Refer to a package in the imported ecore model.
	 * Hint: Use ctrl+alt+R to rename packages and classes.
	 */
	domain elevator

	/* 
	 * Define classes of objects that are controllable
	 * or uncontrollable.
	 */
	controllable {
		Controller
	}

	/*
	 * Collaborations describe how objects interact in a certain
	 * context to collectively accomplish some desired functionality.
	 */
	collaboration ElevatorCollaboration {

		dynamic role Controller controller
		dynamic role Button button

		guarantee scenario MyScenario {
			button -> controller.request()
			strict requested controller -> button.response()
		}

	}

}
