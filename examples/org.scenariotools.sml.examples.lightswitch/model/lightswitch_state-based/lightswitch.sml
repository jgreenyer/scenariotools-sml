import "../lightswitch.ecore"

specification LightSwitch_StateBased{
	
	domain lightswitch
	
	controllable {
		Light
	}
	
	non-spontaneous events { 
		Light.hold
		Light.release
	}
	
	collaboration lightswitch {
		static role Environment env
		static role Light light
		
		guarantee scenario Light{
			env->light.press()
			var EInt t = 1
			while [ t != -1 ] {
				alternative [t == 0] { // off
					strict urgent light->env.lightOff()
					env->light.press()
					t = 1
				} or [t == 1] { // low
					strict urgent light->env.lightLow()
					alternative {
						env->light.press()
						t = 0
					} or {
						env->light.hold()
						t = 2
					}
				} or [t == 2] { // high
					strict urgent light->env.lightHigh()
					alternative {
						env->light.press()
						t = 0
					} or {
						env->light.hold()
						t = 1
					}					
				}
			}
		}		
		
		assumption scenario LightSwitchPressAssumption{
			env->light.press()
			var EInt s = 1
			while [ s != -1 ] {
				alternative [s == 0] { // released
					strict eventually env->light.press()
					s = 1
				} or [s == 1] { // pressed
						strict eventually env->light.release()
						s = 0
				} or [s == 1] { // pressed
						strict eventually env->light.hold()
						s = 1
				}
				
			}
		}
		
	}// end collaboration lightswitch
	
}