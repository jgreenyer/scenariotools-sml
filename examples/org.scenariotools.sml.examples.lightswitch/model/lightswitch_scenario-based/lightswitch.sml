import "../lightswitch.ecore"

specification LightSwitch_ScenarioBased{
	
	
	domain lightswitch
	
	controllable {
		Light
	}
	
	non-spontaneous events { 
		Light.hold
		Light.release
	}
	
	collaboration lightswitch {
		static role Environment env
		static role Light light
		
		guarantee scenario LightOn{
			env->light.press()
			urgent light->env.lightLow()
			alternative{
				env->light.press()
				strict urgent light->env.lightOff()
			} or {
				env->light.hold()
				strict urgent light->env.lightHigh()				
			}
		}
		
		guarantee scenario LightHigh{
			light->env.lightHigh()
			alternative{
				env->light.press()
				strict urgent light->env.lightOff()
			} or {
				env->light.hold()
				strict urgent light->env.lightLow()				
			}
		}
		
		guarantee scenario LightLow{
			light->env.lightLow()
			alternative{
				env->light.press()
				strict urgent light->env.lightOff()
			} or {
				env->light.hold()
				strict urgent light->env.lightHigh()				
			}
		}
		
		
		
		assumption scenario LightSwitchPressAssumption{
			env->light.press()
			alternative {
				strict eventually env->light.release()
			} or {
				strict eventually env->light.hold()				
			}
		}
		
		assumption scenario LightSwitchHoldAssumption{
			env->light.hold()
			alternative {
				strict eventually env->light.release()
			} or {
				strict eventually env->light.hold()				
			}
		}constraints[
			forbidden env->light.press()
		]
		
		
	}// end collaboration lightswitch
	
}