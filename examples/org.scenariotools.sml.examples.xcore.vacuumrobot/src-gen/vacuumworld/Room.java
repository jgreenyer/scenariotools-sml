/**
 */
package vacuumworld;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link vacuumworld.Room#getEnv <em>Env</em>}</li>
 *   <li>{@link vacuumworld.Room#isHasChargingStation <em>Has Charging Station</em>}</li>
 *   <li>{@link vacuumworld.Room#isDirty <em>Dirty</em>}</li>
 *   <li>{@link vacuumworld.Room#getAdjacentRooms <em>Adjacent Rooms</em>}</li>
 * </ul>
 *
 * @see vacuumworld.VacuumworldPackage#getRoom()
 * @model
 * @generated
 */
public interface Room extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Env</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link vacuumworld.Environment#getRooms <em>Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Env</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Env</em>' container reference.
	 * @see #setEnv(Environment)
	 * @see vacuumworld.VacuumworldPackage#getRoom_Env()
	 * @see vacuumworld.Environment#getRooms
	 * @model opposite="rooms" transient="false"
	 * @generated
	 */
	Environment getEnv();

	/**
	 * Sets the value of the '{@link vacuumworld.Room#getEnv <em>Env</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Env</em>' container reference.
	 * @see #getEnv()
	 * @generated
	 */
	void setEnv(Environment value);

	/**
	 * Returns the value of the '<em><b>Has Charging Station</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Charging Station</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Charging Station</em>' attribute.
	 * @see #setHasChargingStation(boolean)
	 * @see vacuumworld.VacuumworldPackage#getRoom_HasChargingStation()
	 * @model unique="false" required="true"
	 * @generated
	 */
	boolean isHasChargingStation();

	/**
	 * Sets the value of the '{@link vacuumworld.Room#isHasChargingStation <em>Has Charging Station</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Charging Station</em>' attribute.
	 * @see #isHasChargingStation()
	 * @generated
	 */
	void setHasChargingStation(boolean value);

	/**
	 * Returns the value of the '<em><b>Dirty</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dirty</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dirty</em>' attribute.
	 * @see #setDirty(boolean)
	 * @see vacuumworld.VacuumworldPackage#getRoom_Dirty()
	 * @model unique="false" required="true"
	 * @generated
	 */
	boolean isDirty();

	/**
	 * Sets the value of the '{@link vacuumworld.Room#isDirty <em>Dirty</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dirty</em>' attribute.
	 * @see #isDirty()
	 * @generated
	 */
	void setDirty(boolean value);

	/**
	 * Returns the value of the '<em><b>Adjacent Rooms</b></em>' reference list.
	 * The list contents are of type {@link vacuumworld.Room}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Adjacent Rooms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Adjacent Rooms</em>' reference list.
	 * @see vacuumworld.VacuumworldPackage#getRoom_AdjacentRooms()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%vacuumworld.Room%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%vacuumworld.Room%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%vacuumworld.Room%&gt; room)\n\t{\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf(((!&lt;%com.google.common.base.Objects%&gt;.equal(room, this)) &amp;&amp; &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%vacuumworld.Door%&gt;&gt;exists(&lt;%this%&gt;.getEnv().getDoors(), new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%vacuumworld.Door%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n\t\t{\n\t\t\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%vacuumworld.Door%&gt; it)\n\t\t\t{\n\t\t\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf((it.getConnectedRooms().contains(room) &amp;&amp; it.getConnectedRooms().contains(this)));\n\t\t\t}\n\t\t})));\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%vacuumworld.Room%&gt;&gt;toEList(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%vacuumworld.Room%&gt;&gt;filter(this.getEnv().getRooms(), _function));'"
	 * @generated
	 */
	EList<Room> getAdjacentRooms();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='this.setDirty(true);'"
	 * @generated
	 */
	void makeDirty();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='this.setDirty(false);'"
	 * @generated
	 */
	void clean();

} // Room
