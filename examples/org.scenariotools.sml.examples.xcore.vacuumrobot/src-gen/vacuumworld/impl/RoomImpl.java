/**
 */
package vacuumworld.impl;

import com.google.common.base.Objects;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

import vacuumworld.Door;
import vacuumworld.Environment;
import vacuumworld.Room;
import vacuumworld.VacuumworldPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Room</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link vacuumworld.impl.RoomImpl#getEnv <em>Env</em>}</li>
 *   <li>{@link vacuumworld.impl.RoomImpl#isHasChargingStation <em>Has Charging Station</em>}</li>
 *   <li>{@link vacuumworld.impl.RoomImpl#isDirty <em>Dirty</em>}</li>
 *   <li>{@link vacuumworld.impl.RoomImpl#getAdjacentRooms <em>Adjacent Rooms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomImpl extends NamedElementImpl implements Room {
	/**
	 * The default value of the '{@link #isHasChargingStation() <em>Has Charging Station</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasChargingStation()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HAS_CHARGING_STATION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHasChargingStation() <em>Has Charging Station</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasChargingStation()
	 * @generated
	 * @ordered
	 */
	protected boolean hasChargingStation = HAS_CHARGING_STATION_EDEFAULT;

	/**
	 * The default value of the '{@link #isDirty() <em>Dirty</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDirty()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DIRTY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDirty() <em>Dirty</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDirty()
	 * @generated
	 * @ordered
	 */
	protected boolean dirty = DIRTY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VacuumworldPackage.Literals.ROOM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Environment getEnv() {
		if (eContainerFeatureID() != VacuumworldPackage.ROOM__ENV) return null;
		return (Environment)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Environment basicGetEnv() {
		if (eContainerFeatureID() != VacuumworldPackage.ROOM__ENV) return null;
		return (Environment)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEnv(Environment newEnv, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newEnv, VacuumworldPackage.ROOM__ENV, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnv(Environment newEnv) {
		if (newEnv != eInternalContainer() || (eContainerFeatureID() != VacuumworldPackage.ROOM__ENV && newEnv != null)) {
			if (EcoreUtil.isAncestor(this, newEnv))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newEnv != null)
				msgs = ((InternalEObject)newEnv).eInverseAdd(this, VacuumworldPackage.ENVIRONMENT__ROOMS, Environment.class, msgs);
			msgs = basicSetEnv(newEnv, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VacuumworldPackage.ROOM__ENV, newEnv, newEnv));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHasChargingStation() {
		return hasChargingStation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasChargingStation(boolean newHasChargingStation) {
		boolean oldHasChargingStation = hasChargingStation;
		hasChargingStation = newHasChargingStation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VacuumworldPackage.ROOM__HAS_CHARGING_STATION, oldHasChargingStation, hasChargingStation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDirty() {
		return dirty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDirty(boolean newDirty) {
		boolean oldDirty = dirty;
		dirty = newDirty;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VacuumworldPackage.ROOM__DIRTY, oldDirty, dirty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Room> getAdjacentRooms() {
		final Function1<Room, Boolean> _function = new Function1<Room, Boolean>() {
			public Boolean apply(final Room room) {
				return Boolean.valueOf(((!Objects.equal(room, this)) && IterableExtensions.<Door>exists(RoomImpl.this.getEnv().getDoors(), new Function1<Door, Boolean>() {
					public Boolean apply(final Door it) {
						return Boolean.valueOf((it.getConnectedRooms().contains(room) && it.getConnectedRooms().contains(this)));
					}
				})));
			}
		};
		return ECollections.<Room>toEList(IterableExtensions.<Room>filter(this.getEnv().getRooms(), _function));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void makeDirty() {
		this.setDirty(true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void clean() {
		this.setDirty(false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VacuumworldPackage.ROOM__ENV:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetEnv((Environment)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VacuumworldPackage.ROOM__ENV:
				return basicSetEnv(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case VacuumworldPackage.ROOM__ENV:
				return eInternalContainer().eInverseRemove(this, VacuumworldPackage.ENVIRONMENT__ROOMS, Environment.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VacuumworldPackage.ROOM__ENV:
				if (resolve) return getEnv();
				return basicGetEnv();
			case VacuumworldPackage.ROOM__HAS_CHARGING_STATION:
				return isHasChargingStation();
			case VacuumworldPackage.ROOM__DIRTY:
				return isDirty();
			case VacuumworldPackage.ROOM__ADJACENT_ROOMS:
				return getAdjacentRooms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VacuumworldPackage.ROOM__ENV:
				setEnv((Environment)newValue);
				return;
			case VacuumworldPackage.ROOM__HAS_CHARGING_STATION:
				setHasChargingStation((Boolean)newValue);
				return;
			case VacuumworldPackage.ROOM__DIRTY:
				setDirty((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VacuumworldPackage.ROOM__ENV:
				setEnv((Environment)null);
				return;
			case VacuumworldPackage.ROOM__HAS_CHARGING_STATION:
				setHasChargingStation(HAS_CHARGING_STATION_EDEFAULT);
				return;
			case VacuumworldPackage.ROOM__DIRTY:
				setDirty(DIRTY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VacuumworldPackage.ROOM__ENV:
				return basicGetEnv() != null;
			case VacuumworldPackage.ROOM__HAS_CHARGING_STATION:
				return hasChargingStation != HAS_CHARGING_STATION_EDEFAULT;
			case VacuumworldPackage.ROOM__DIRTY:
				return dirty != DIRTY_EDEFAULT;
			case VacuumworldPackage.ROOM__ADJACENT_ROOMS:
				return !getAdjacentRooms().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case VacuumworldPackage.ROOM___MAKE_DIRTY:
				makeDirty();
				return null;
			case VacuumworldPackage.ROOM___CLEAN:
				clean();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (hasChargingStation: ");
		result.append(hasChargingStation);
		result.append(", dirty: ");
		result.append(dirty);
		result.append(')');
		return result.toString();
	}

} //RoomImpl
