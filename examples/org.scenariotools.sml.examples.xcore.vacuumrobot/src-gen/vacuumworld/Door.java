/**
 */
package vacuumworld;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Door</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link vacuumworld.Door#getConnectedRooms <em>Connected Rooms</em>}</li>
 * </ul>
 *
 * @see vacuumworld.VacuumworldPackage#getDoor()
 * @model
 * @generated
 */
public interface Door extends EObject {
	/**
	 * Returns the value of the '<em><b>Connected Rooms</b></em>' reference list.
	 * The list contents are of type {@link vacuumworld.Room}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connected Rooms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connected Rooms</em>' reference list.
	 * @see vacuumworld.VacuumworldPackage#getDoor_ConnectedRooms()
	 * @model lower="2" upper="2"
	 * @generated
	 */
	EList<Room> getConnectedRooms();

} // Door
