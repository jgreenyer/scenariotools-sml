import "../doublecascading.ecore"
specification doublecascading05{ 
	domain Cascade
	
	controllable {
		A
		B
	}
	
	collaboration Doublecascading {
		
		static role Environment e
		static role A a
		static role B b
		
		guarantee scenario Cascade1 {
			e->a.startCascade()
			strict urgent a->b.msg1()
			strict urgent a->b.msg1()
		}

		guarantee scenario Cascade2 {
			a->b.msg1()
			strict urgent a->b.msg2()
			strict urgent a->b.msg2()
		}

		guarantee scenario Cascade3 {
			a->b.msg2()
			strict urgent a->b.msg3()
			strict urgent a->b.msg3()
		}

		guarantee scenario Cascade4 {
			a->b.msg3()
			strict urgent a->b.msg4()
			strict urgent a->b.msg4()
		}

		guarantee scenario Cascade5 {
			a->b.msg4()
			strict urgent a->b.msg5()
			strict urgent a->b.msg5()
		}

		
	}
	
}