import "productioncell_ProductionCellIntegrated.ecore"

specification ProductionCellSpecification_ArmAAndPress {

	domain ProductionCellIntegrated

	controllable {
		Controller
	}

	non-spontaneous events {
		Controller.arrivedAtDepositBelt
		Controller.arrivedAtPress
		Controller.arrivedAtTable
		Controller.pressingFinished
//		Controller.accelerate
		Controller.decelerate
	}

	collaboration ProductionCellIntegrated {

		static role TableSensor ts
		static role Controller c
		static role ArmA a
		static role ArmB b
		static role Press p

		 
		/*
		 * Arm A must pick up a blank after it arrived.
		 * Blanks may arrive before or after the arm has returned to the table
		 */
		singular guarantee scenario ArmAPickUpBlank {
			ts -> c.blankArrived()
			strict urgent c -> a.pickUp()
			strict urgent c -> a.moveToPress()
			while [true] {
				parallel {
					ts -> c.blankArrived()
				} and {
					a -> c.arrivedAtTable()
				}			
				strict urgent c -> a.pickUp()
				strict urgent c -> a.moveToPress()
			}
		}

		/*
		 * After arriving at the press, arm A must release the blank
		 */
		guarantee scenario ArmATransportBlankToPress {
			a -> c.arrivedAtPress()
			strict urgent c -> a.releaseBlank()
		}
		
		/*
		 * The press must press the blank after it is released by arm A
		 */
		guarantee scenario PressPlateAfterArmAReleasesBlankPlate {
			c -> a.releaseBlank()
			strict urgent c -> p.press()
		}
		
		/*
		 * After releasing the blank, arm A must return to the table
		 */
		guarantee scenario ArmAMoveToTableAfterReleaseBlank {
			c -> a.releaseBlank()
			strict urgent c -> a.moveToTable()
		}
		
		/*
		 * We assume that not two blanks arrive before arm A has picked up one.
		 */
		assumption scenario NoBlankArrivesBeforeArmAPicksUpBlank {
			ts -> c.blankArrived()
			c -> a.pickUp()
		} constraints [
			forbidden ts -> c.blankArrived()
		]
		
				/*
		 * Arm A movement from table to press: 
		 * When the controller tells the arm to move to the press, it will eventually arrive there.
		 */
		assumption scenario ArmAMoveFromTableToPressAssumption {
			c -> a.moveToPress()
			// ignore acceleration for simplicity
//			strict eventually a -> c.accelerate()
//			strict eventually a -> c.decelerate()
			strict eventually a -> c.arrivedAtPress()
		} constraints [
	 		// while the arm is moving to the press, it will not arrive at the table
			forbidden a -> c.arrivedAtTable()
	 		// unless meanwhile the controller tells the arm to move to the table
			interrupt c -> a.moveToTable()
	 		// ignore if the controller tell the arm again to move to the press.
			ignore c -> a.moveToPress()
		]

		/*
		 * Arm A movement from press to table: 
		 * When the controller tells the arm to move to the table, it will eventually arrive there.
		 */
		assumption scenario ArmAMoveFromPressToTableAssumption {
			c -> a.moveToTable()
			// ignore acceleration for simplicity
//			strict eventually a -> c.accelerate()
//			strict eventually a -> c.decelerate()
			strict eventually a -> c.arrivedAtTable()
		} constraints [
	 		// while the arm is moving to the table, it will not arrive at the press
			forbidden a -> c.arrivedAtPress()
	 		// unless meanwhile the controller tells the arm to move to the press
			interrupt c -> a.moveToPress()
	 		// ignore if the controller tell the arm again to move to the press.
			ignore c -> a.moveToTable()
		]
		
	}
}