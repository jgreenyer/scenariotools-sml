import "../../model/tunnelcontrol.ecore"

specification TunnelSystemSimple {
	
	domain tunnelcontrol
	
	controllable {
		Gate
	}
	
	channels{
		Gate.entry over EntryExitSensor.gate
		Gate.exit over EntryExitSensor.gate
		Gate.carApproaching over CarApproachSensor.gate
	}

	collaboration IncrementGateCounterWhenCarEntersTunnel {
		
		dynamic role EntryExitSensor entryExitSensor
		dynamic role Gate gate
		dynamic role TrafficLight trafficLight
		
		guarantee scenario IncrementGateCounterWhenCarEntersTunnel{
			entryExitSensor -> gate.entry()
			strict urgent gate -> gate.setCarsInTunnel(gate.carsInTunnel+1)
		}
		
		assumption scenario NoCarEntersWhenTrafficLightShowsStop bindings [
			trafficLight = gate.trafficLight
		]{
			entryExitSensor -> gate.entry()
			violation [trafficLight.showStop]
		}
		
		assumption scenario EntryExitSensorOnlySendsEntryToLocalGate{
			entryExitSensor -> gate.entry()
			violation [gate.entryExitSensor != entryExitSensor]
		}

		assumption scenario MaxNumberOfCarsInTunnel{
			gate -> gate.setCarsInTunnel(*)
			violation [gate.carsInTunnel > 2]
		}
		
	}

	collaboration DecrementGateCounterWhenCarExitsTunnel {
		
		dynamic role EntryExitSensor entryExitSensor
		dynamic role Gate gate
		dynamic role Gate oppositeGate
		
		guarantee scenario NotifyOppositeGateWhenCarExitsTunnel bindings [
			oppositeGate = gate.oppositeGate
		]{
			entryExitSensor -> gate.exit()
			strict urgent gate -> oppositeGate.carLeftTunnel()
		}
		
		guarantee scenario DecrementGateCounterWhenCarLeftTunnel{
			gate -> oppositeGate.carLeftTunnel()
			strict urgent oppositeGate -> oppositeGate.setCarsInTunnel(oppositeGate.carsInTunnel+-1)
			violation [oppositeGate.carsInTunnel < 0]
		}
		
		assumption scenario NoCarLeavesWhenTunnelEmpty bindings [
			oppositeGate = gate.oppositeGate
		]{
			entryExitSensor -> gate.exit()
			violation [oppositeGate.carsInTunnel == 0]
		}

		assumption scenario EntryExitSensorOnlySendsExitToLocalGate{
			entryExitSensor -> gate.exit()
			violation [gate.entryExitSensor != entryExitSensor]
		}
		
	}

	collaboration CarApproachesGateWithStopLight {

		dynamic role CarApproachSensor carApproachSensor
		dynamic role Gate gate
		dynamic role Gate oppositeGate
		dynamic role TrafficLight trafficLight
		dynamic role TrafficLight oppositeTrafficLight
		
		guarantee scenario CarApproachesGateWithStopLight bindings [
			oppositeGate = gate.oppositeGate
			oppositeTrafficLight = oppositeGate.trafficLight
			trafficLight = gate.trafficLight
		]{
			carApproachSensor -> gate.carApproaching()
			// we must close the opposite gate and open this gate when all cars have left the tunnel.
			strict urgent gate -> oppositeGate.close()
			strict urgent oppositeGate -> oppositeTrafficLight.setShowStop(true)
			wait [oppositeGate.carsInTunnel == 0]
			strict urgent gate -> trafficLight.setShowStop(false)
		}

	
	}
	
	
}