import "../../model/tunnelcontrol.ecore"

specification TunnelWithApproachZone {
	
	domain tunnelcontrol
	
	controllable {
		Gate
	}
	
	channels{
		Gate.entry over EntryExitSensor.gate
		Gate.exit over EntryExitSensor.gate
		Gate.carApproaching over CarApproachSensor.gate
	}
	
	non-spontaneous events {
		Gate.exit
		Gate.entry
	}

	collaboration CarApproachAndEntryAssumption {
		
		dynamic role CarApproachSensor carApproachSensor
		dynamic role Gate gate
		dynamic role EntryExitSensor entryExitSensor
	
		singular assumption scenario CarApproachAndEntryAssumption bindings [
			entryExitSensor = gate.entryExitSensor
		]{
			carApproachSensor -> gate.carApproaching()
			var EInt carsInApproach = 1
			var EInt MAXINAPPROACH = 1 // INCREASE HERE IF NECESSARY
			while [true]{
				alternative [carsInApproach < MAXINAPPROACH]{
					/* at most MAXINAPPROACH cars can be in approach.
					 * If carsInApproach >= MAXINAPPROACH, then only 
					 * message strict entryExitSensor -> gate.entry()
					 * in the alternative below is enabled.
					 * (Otherwise both.)
					 * Since it is strict, an occurrence of
					 * carApproachSensor -> gate.carApproaching()
					 * is then forbidden.
					 * 
					 * Likewise, if carsInApproach is zero,
					 * the second case is disabled, and only
					 * message strict carApproachSensor -> gate.carApproaching()
					 * is enabled. In this case, Since it is strict, 
					 * occurrences of entryExitSensor -> gate.entry()
					 * are then forbidden. 
					 */ 
					strict carApproachSensor -> gate.carApproaching()					
					carsInApproach = carsInApproach + 1
				}or [carsInApproach > 0]{
					strict entryExitSensor -> gate.entry()
					carsInApproach = carsInApproach - 1
				}
			}
		}
		
	}

	collaboration CarApproachAndEntryCounter {
		
		dynamic role CarApproachSensor carApproachSensor
		dynamic role Gate gate
		dynamic role EntryExitSensor entryExitSensor
	
		guarantee scenario IncreaseCarsInApproachCount {
			carApproachSensor -> gate.carApproaching()
			strict urgent gate->gate.setCarsInApproach(gate.carsInApproach + 1)			
			//assertion
			violation [gate.carsInApproach > 3]
		}

		guarantee scenario DecreaseCarsInApproachCount {
			entryExitSensor -> gate.entry()
			strict urgent gate->gate.setCarsInApproach(gate.carsInApproach - 1)			
			//assertion
			violation [gate.carsInApproach < 0]
		}
		
	}

	collaboration CarEntryAndExitAssumption {
	
		dynamic role Gate gate
		dynamic role EntryExitSensor entryExitSensor
		dynamic role TrafficLight trafficLight
		dynamic role Gate oppositeGate
		dynamic role EntryExitSensor oppositeEntryExitSensor
		
		
		assumption scenario NoEntryWhenTrafficLightRed bindings [
			trafficLight = gate.trafficLight
		]{
			entryExitSensor -> gate.entry()
			violation [trafficLight.showStop]
		}
	
		singular assumption scenario CarEntryAndExitAssumption bindings [
			oppositeGate = gate.oppositeGate
			oppositeEntryExitSensor = oppositeGate.entryExitSensor
		]{
			entryExitSensor -> gate.entry()
			var EInt carsInTunnel = 1
			var EInt MAXINTUNNEL = 2 // INCREASE HERE IF NECESSARY
			while [true]{
				alternative [carsInTunnel < MAXINTUNNEL]{
					/* at most MAXINTUNNEL cars can be in the tunnel
					 * if carsInTunnel >= MAXINTUNNEL, then only 
					 * message strict oppositeEntryExitSensor -> oppositeGate.exit()
					 * in the alternative below is enabled.
					 * (Otherwise both.)
					 * Since it is strict, an occurrence of
					 * entryExitSensor -> gate.entry()
					 * is then forbidden.
					 * 
					 * Likewise, if carsInTunnel is zero,
					 * the second case is disabled, and only
					 * message strict entryExitSensor -> gate.entry()
					 * is enabled. In this case, Since it is strict, 
					 * occurrences of oppositeEntryExitSensor -> oppositeGate.exit()
					 * are then forbidden. 
					 */ 
					strict entryExitSensor -> gate.entry()
					carsInTunnel = carsInTunnel + 1
				}or [carsInTunnel > 0]{
					strict oppositeEntryExitSensor -> oppositeGate.exit()
					carsInTunnel = carsInTunnel - 1
				}
			}
		}
		
	}

	collaboration CarEntryAndExitCounter {
		
		dynamic role EntryExitSensor entryExitSensor
		dynamic role Gate gate
		dynamic role Gate oppositeGate
		dynamic role EntryExitSensor oppositeEntryExitSensor
		
		guarantee scenario IncrementGateCounterWhenCarEntersTunnel{
			entryExitSensor -> gate.entry()
			strict urgent gate -> gate.setCarsInTunnel(gate.carsInTunnel + 1)
			//assertion
			violation [gate.carsInTunnel > 3]
		}
		
		guarantee scenario DecrementGateCounterWhenCarLeftTunnel bindings [
			gate = oppositeGate.oppositeGate
		]{
			oppositeEntryExitSensor -> oppositeGate.exit()
			strict urgent oppositeGate -> gate.carLeftTunnel()
			strict urgent gate -> gate.setCarsInTunnel(gate.carsInTunnel - 1)
			//assertion
			violation [gate.carsInTunnel < 0]
		}
		
	}


	collaboration OpenTunnel {
		
		dynamic role CarApproachSensor carApproachSensor
		dynamic role Gate gate
		dynamic role Gate oppositeGate
		dynamic role EntryExitSensor oppositeEntryExitSensor
		dynamic role TrafficLight trafficLight
		dynamic role TrafficLight oppositeTrafficLight
		
		/*
		 * When a car approaches a gate G, then the opposite gate O must
		 * close after all cars in approach of gate O have entered gate O.
		 * Then the gate G must open when the last car has left the tunnel. 
		 */
		 singular guarantee scenario OpenTunnel bindings [
		 	trafficLight = gate.trafficLight
		 	oppositeGate = gate.oppositeGate
		 	oppositeEntryExitSensor = oppositeGate.entryExitSensor
		 	oppositeTrafficLight = oppositeGate.trafficLight
		 ]{
		 	carApproachSensor->gate.carApproaching()
		 	var EInt carsInApproachOfOppositeGate = oppositeGate.carsInApproach
		 	while [carsInApproachOfOppositeGate > 0]{
		 		oppositeEntryExitSensor->oppositeGate.entry()
		 		carsInApproachOfOppositeGate = carsInApproachOfOppositeGate - 1 
		 	}
		 	strict urgent oppositeGate->oppositeTrafficLight.setShowStop(true)
			wait [oppositeGate.carsInTunnel == 0]
		 	strict urgent gate->trafficLight.setShowStop(false)
		 }constraints[
		 	ignore carApproachSensor->gate.carApproaching()
		 ]
	}
	
}