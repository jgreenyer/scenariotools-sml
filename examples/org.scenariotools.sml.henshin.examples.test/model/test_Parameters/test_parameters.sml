import "../model.ecore"

specification ParameterTestWithInstanceModelHenshin {
	
	domain model
	controllable {
		A
		B
	}
	
	collaboration Cyclic {
		
		dynamic role A a
		dynamic role B b
		dynamic role Environment env
		
		guarantee scenario  UpdateB bindings [b = a.b] {
			env -> a.opA1()
			strict urgent a -> a.opA2()
		}
	}
	
	
	
}