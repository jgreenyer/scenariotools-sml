import "../model/productioncell.ecore"

specification ProductioncellSpecification {

	domain productioncell

	controllable {
		Controller
	}
	
	non-spontaneous events {
		Controller.pickedUpItem
		Controller.arrivedAt
		Controller.releasedItem
		Controller.pressingFinished
		RobotArm.setCarriesItem
		RobotArm.setLocation
		Press.setHasItem
	}

	collaboration FeedBeltBehavior {

		static role Controller controller
		static role ConveyorBelt feedBelt
		static role RobotArm feedArm
		static role Press press

		guarantee scenario BlankArrives {
			feedBelt -> controller.blankArrived()
			wait [feedArm.location == feedBelt && !feedArm.carriesItem]
			urgent controller -> feedArm.pickUp()
		}
		
		guarantee scenario ArmDeliversItemToPress {
			feedArm -> controller.pickedUpItem()
			urgent controller -> feedArm.moveTo(press)
			feedArm -> controller.arrivedAt(press)
			wait [!press.hasItem]
			urgent controller -> feedArm.releaseItem()
			feedArm -> controller.releasedItem()
			urgent controller -> feedArm.moveTo(feedBelt)
		}
		
		assumption scenario ArmPicksUpItemBeforeNewBlankArrives {
			feedBelt -> controller.blankArrived()
			monitored eventually feedArm -> controller.pickedUpItem()
		} constraints [
			forbidden feedBelt -> controller.blankArrived()
		]

	}
	
	collaboration PressBehavior {
		
		static role Controller controller
		static role RobotArm feedArm
		static role RobotArm depositArm
		static role Press press

		guarantee scenario PressStartsPressing {
			feedArm -> controller.releasedItem()
			urgent controller -> press.startPressing()
		}
		
		guarantee scenario PickUpPressedItem {
			press -> controller.pressingFinished()
			wait [depositArm.location == press && !depositArm.carriesItem]
			urgent controller -> depositArm.pickUp()
		}
		
		assumption scenario PressEventuallyFinishes {
			controller -> press.startPressing()
			strict eventually press -> controller.pressingFinished()
		}
	}
	
	collaboration DepositBeltBehavior {
		
		static role Controller controller
		static role ConveyorBelt depositBelt
		static role RobotArm depositArm
		static role Press press

		guarantee scenario DepositArmDeliversPressedItem {
			depositArm -> controller.pickedUpItem()
			urgent controller -> depositArm.moveTo(depositBelt)
			depositArm -> controller.arrivedAt(depositBelt)
			urgent controller -> depositArm.releaseItem()
			depositArm -> controller.releasedItem()
			urgent controller -> depositArm.moveTo(press)
		}		
	}
	
	collaboration RobotArmBehavior {
		
		dynamic role Controller controller
		dynamic role RobotArm arm
		dynamic role Location targetLocation
		static role Press press
		
		assumption scenario ArmMovesToLocation {
			controller -> arm.moveTo(bind targetLocation)
			strict eventually arm -> controller.arrivedAt(targetLocation)
			strict committed arm -> arm.setLocation(targetLocation)
		}
		
		assumption scenario ArmPicksUpItem {
			controller -> arm.pickUp()
			strict eventually arm -> controller.pickedUpItem()
			strict committed arm -> arm.setCarriesItem(true)
			interrupt [arm.location != press]
			strict committed press -> press.setHasItem(false)				
		}
		
		assumption scenario ArmReleasesItem {
			controller -> arm.releaseItem()
			strict eventually arm -> controller.releasedItem()
			strict committed arm -> arm.setCarriesItem(false)
			interrupt [arm.location != press]
			strict committed press -> press.setHasItem(true)
		}
	}
}
