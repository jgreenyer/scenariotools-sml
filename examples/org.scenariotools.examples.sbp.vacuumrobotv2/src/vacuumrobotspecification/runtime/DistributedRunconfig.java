package vacuumrobotspecification.runtime;

import mqttadapter.MQTTAdapter;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.settings.Settings;
import vacuumrobot.NamedElement;
import vacuumrobotspecification.specification.VacuumrobotSpecificationSpecification;
import vacuumrobotspecification.ui.VacuumrobotSpecificationEnvironmentFrame;

public abstract class DistributedRunconfig extends ADistributedRunconfig {

	public final static int ROBOT_PORT = 9000;
	public final static int ROBOTCTRL_PORT = 9001;
	
	public DistributedRunconfig(String name, int port) {
		super();
		enableDistributedExecutionMode(port);

		setUpGUI(name);
	}
	
//	// for robot
//	// no GUI
//	public DistributedRunconfig(int port){
//		super(new VacuumrobotSpecificationSpecification());
//		//enableDistributedExecutionMode(port);
//		setAdapter(new MQTTAdapter(this, name));
//	}

	public DistributedRunconfig(String name) {
		super();
		setAdapter(new MQTTAdapter(this, name));

		// just set up GUI for RobotCtrl
		if (name=="RobotCtrl"){
			setUpGUI(name);
		}
		
	}
}
