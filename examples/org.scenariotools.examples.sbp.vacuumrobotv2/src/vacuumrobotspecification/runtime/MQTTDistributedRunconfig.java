package vacuumrobotspecification.runtime;

import mqttadapter.MQTTAdapter;

public abstract class MQTTDistributedRunconfig extends ADistributedRunconfig {

	public MQTTDistributedRunconfig(String name) {
		super();
		setAdapter(new MQTTAdapter(this, name));

		setUpGUI(name);
	}

	@Override
	protected final void registerNetworkAdressesForObjects() {
	}

}
