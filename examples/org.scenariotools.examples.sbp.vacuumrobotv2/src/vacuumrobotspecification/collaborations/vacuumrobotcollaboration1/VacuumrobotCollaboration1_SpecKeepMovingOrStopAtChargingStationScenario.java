package vacuumrobotspecification.collaborations.vacuumrobotcollaboration1;

import java.util.ArrayList;
import java.util.List;

import sbp.specification.events.Message;
import sbp.specification.scenarios.violations.Violation;
import vacuumrobot.DirtyableRoom;
// Roles
import vacuumrobot.Room;

@SuppressWarnings("serial")
public class VacuumrobotCollaboration1_SpecKeepMovingOrStopAtChargingStationScenario
		extends VacuumrobotCollaboration1Collaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(robot, robotCtrl, "arrivedInRoom", RANDOM);
		//TODO: any in parameter
		//setBlocked(robotCtrl, robot, "moveToAdjacentRoom", room.getBinding().getAdjacentRooms().get(0));
		setBlocked(robotCtrl, robot, "moveToAdjacentRoom", RANDOM);
		setBlocked(robotCtrl, robotCtrl, "SETMoving", false);
	}

	@Override
	protected void initialisation() {
		// []
		addInitializingMessage(new Message(robot, robotCtrl, "arrivedInRoom", RANDOM));
		// true
		// 1
	}

	@Override
	protected void registerRoleBindings() {
	}

	@Override
	protected void body() throws Violation {
		room.setBinding((Room) getLastMessage().getParameters().get(0));
		// true
		// Begin Alternative
		List<Message> requestedMessages = new ArrayList<Message>();
		List<Message> waitedForMessages = new ArrayList<Message>();
		//TODO: any in parameter
		if (true) {
			// TODO: any in parameter => Alternative
			//strict urgent robotCtrl->robot.moveToAdjacentRoom(room.adjacentRooms.any())
			/*for(int index = 0; index < room.getBinding().getAdjacentRooms().size(); index++) {
				requestedMessages.add(new Message(STRICT, robotCtrl, robot, "moveToAdjacentRoom", room.getBinding().getAdjacentRooms().get(index)));
			}*/
			List<DirtyableRoom> dirtyRooms = rManager.getBinding().getDirtyRooms();
			Room nextDirtyNearestRoom = NextDirtyRoomSelection.getNextRoomOnShortestPathToDirty(room.getBinding(), dirtyRooms);
			//Settings.NETWORK_OUT.println("nextDirtyNearestRoom = " + nextDirtyNearestRoom);
			requestedMessages.add(new Message(STRICT, robotCtrl, robot, "moveToAdjacentRoom", nextDirtyNearestRoom));
			// true
		}
		if (room.getBinding().isHasChargingStation()) {
			requestedMessages.add(new Message(STRICT, robotCtrl, robotCtrl, "SETMoving", false));
			// true
		}
		doStep(requestedMessages, waitedForMessages);
		// Determine which alternative was chosen
		//TODO any alternative
		boolean alternativeAny = false;
		for(int index = 0; index < room.getBinding().getAdjacentRooms().size(); index++) {
			if (getLastMessage().equals(
					new Message(robotCtrl, robot, "moveToAdjacentRoom", room.getBinding().getAdjacentRooms().get(index)))) {
				// true
				alternativeAny = true;
				break;
			}
		}
		if (alternativeAny) {
			// true
		} else if (getLastMessage().equals(new Message(robotCtrl, robotCtrl, "SETMoving", false))) {
			// true
		}
		// End Alternative
	}

}
