package vacuumrobotspecification.collaborations.vacuumrobotcollaboration1;

import java.util.List;

import vacuumrobot.DirtyableRoom;
import vacuumrobot.Room;

public class NextDirtyRoomSelection {
	public static Room getNextRoomOnShortestPathToDirty(Room currentRoom, List<DirtyableRoom> dirtyRooms) {
		for (Room adjacentRoom : currentRoom.getAdjacentRooms()) {
			if (dirtyRooms.contains(adjacentRoom)) {
				return adjacentRoom;
			}
		}
		for (Room adjacentRoom : currentRoom.getAdjacentRooms()) {
			if (adjacentRoom.isHasChargingStation()) {
				return adjacentRoom;
			}
		}
		return currentRoom;
	}
}
