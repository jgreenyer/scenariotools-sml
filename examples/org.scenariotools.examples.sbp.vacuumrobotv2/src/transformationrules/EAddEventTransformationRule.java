package transformationrules;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import sbp.runtime.objectsystem.TransformationRule;
import sbp.specification.events.Message;
import sbp.specification.events.ParameterRandom;

public class EAddEventTransformationRule extends TransformationRule{

	public static final ParameterRandom RANDOM = ParameterRandom.getInstance();

	public boolean match(Message event) {
		boolean matches = event.getMessage().startsWith("ADD");
		return matches;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void execute(Message event){
		//System.out.println("#####executing SetEventTransformationRule");
		EObject receiver = (EObject) event.getReceiver().getBinding();
		String featureName = event.getMessage().substring(3);
		featureName = Character.toLowerCase(featureName.charAt(0)) + featureName.substring(1); 
		EStructuralFeature feature = receiver.eClass().getEStructuralFeature(featureName);
		((EList)receiver.eGet(feature)).add(event.getParameters().get(0));
		System.out.println(receiver);
	}

	@Override
	public Message getTriggerMessage() {
		return null;
	}

}
