package vacuumrobotspecification.collaborations.vacuumrobotcollaboration1;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;

// Collaboration
import vacuumrobotspecification.collaborations.vacuumrobotcollaboration1.VacuumrobotCollaboration1Collaboration;

// Roles
import vacuumrobot.DirtyableRoom;

@SuppressWarnings("serial")
public class VacuumrobotCollaboration1_EventuallyCleanDirtyRoomScenario extends VacuumrobotCollaboration1Collaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(rManager, rManager, "ADDDirtyRooms", RANDOM);
	}

	@Override
	protected void initialisation() {
		// []
		addInitializingMessage(new Message(rManager, rManager, "ADDDirtyRooms", RANDOM));
		// true
		// 1
	}

	@Override
	protected void registerRoleBindings() {
	}

	@Override
	protected void body() throws Violation {
		dirtyableRoom.setBinding((DirtyableRoom) getLastMessage().getParameters().get(0));
		//true
		//TODO: type WaitCondition is not supported
	}

}
