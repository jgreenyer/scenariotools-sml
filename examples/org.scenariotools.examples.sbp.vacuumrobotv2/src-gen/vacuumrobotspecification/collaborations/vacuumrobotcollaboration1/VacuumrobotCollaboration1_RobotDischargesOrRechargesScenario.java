package vacuumrobotspecification.collaborations.vacuumrobotcollaboration1;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;

import java.util.List;
import java.util.ArrayList;

// Collaboration
import vacuumrobotspecification.collaborations.vacuumrobotcollaboration1.VacuumrobotCollaboration1Collaboration;

// Roles
import vacuumrobot.Room;

@SuppressWarnings("serial")
public class VacuumrobotCollaboration1_RobotDischargesOrRechargesScenario
		extends VacuumrobotCollaboration1Collaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(robot, robotCtrl, "arrivedInRoom", RANDOM);
		setBlocked(robot, robotCtrl, "SETCharge", robot.getBinding().getMaxCharge());
		setBlocked(robot, robotCtrl, "SETCharge", (robotCtrl.getBinding().getCharge() - 1));
	}

	@Override
	protected void initialisation() {
		// []
		addInitializingMessage(new Message(robot, robotCtrl, "arrivedInRoom", RANDOM));
		// true
		// 1
	}

	@Override
	protected void registerRoleBindings() {
	}

	@Override
	protected void body() throws Violation {
		room.setBinding((Room) getLastMessage().getParameters().get(0));
		// true
		// Begin Alternative
		// SETCharge env-messge => waitFor 
		List<Message> requestedMessages = new ArrayList<Message>();
		List<Message> waitedForMessages = new ArrayList<Message>();
		if (room.getBinding().isHasChargingStation()) {
			waitedForMessages
					.add(new Message(STRICT, robot, robotCtrl, "SETCharge", robot.getBinding().getMaxCharge()));
			// true
		}
		//TODO : assign impossible value
		int charge = robot.getBinding().getMaxCharge() + 1;
		if (!room.getBinding().isHasChargingStation()) {
			charge = robotCtrl.getBinding().getCharge() - 1;
			waitedForMessages
					.add(new Message(STRICT, robot, robotCtrl, "SETCharge", (charge)));
			// true
		}
		doStep(requestedMessages, waitedForMessages);
		// Determine which alternative was chosen
		if (getLastMessage().equals(new Message(robot, robotCtrl, "SETCharge", robot.getBinding().getMaxCharge()))) {
			// true
		} else if (getLastMessage()
				.equals(new Message(robot, robotCtrl, "SETCharge", (charge)))) {
			// true
		}
		// End Alternative
	}

}
