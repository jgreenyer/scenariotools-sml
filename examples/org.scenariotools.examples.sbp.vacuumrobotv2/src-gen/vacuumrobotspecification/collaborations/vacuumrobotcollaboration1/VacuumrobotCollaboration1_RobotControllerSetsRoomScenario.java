package vacuumrobotspecification.collaborations.vacuumrobotcollaboration1;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;

// Collaboration
import vacuumrobotspecification.collaborations.vacuumrobotcollaboration1.VacuumrobotCollaboration1Collaboration;

// Roles
import vacuumrobot.Room;

@SuppressWarnings("serial")
public class VacuumrobotCollaboration1_RobotControllerSetsRoomScenario extends VacuumrobotCollaboration1Collaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(robot, robotCtrl, "arrivedInRoom", RANDOM);
		//TODO : parameter provided by inline binding
		//setBlocked(robotCtrl, robotCtrl, "SETRoom", room.getBinding());
		setBlocked(robotCtrl, robotCtrl, "SETRoom", RANDOM);
		setBlocked(robotCtrl, robot, "moveToAdjacentRoom", RANDOM);
	}

	@Override
	protected void initialisation() {
		// []
		addInitializingMessage(new Message(robot, robotCtrl, "arrivedInRoom", RANDOM));
		// true
		// 1
	}

	@Override
	protected void registerRoleBindings() {
	}

	@Override
	protected void body() throws Violation {
		room.setBinding((Room) getLastMessage().getParameters().get(0));
		// true
		request(STRICT, robotCtrl, robotCtrl, "SETRoom", room.getBinding());
		waitFor(robotCtrl, robot, "moveToAdjacentRoom", RANDOM);
	}

}
