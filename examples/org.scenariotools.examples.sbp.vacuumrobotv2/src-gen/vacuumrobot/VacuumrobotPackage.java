/**
 */
package vacuumrobot;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see vacuumrobot.VacuumrobotFactory
 * @model kind="package"
 * @generated
 */
public interface VacuumrobotPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "vacuumrobot";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.scenariotools.sml.examples.vacuumrobot";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "org.scenariotools.sml.examples.vacuumrobot";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	VacuumrobotPackage eINSTANCE = vacuumrobot.impl.VacuumrobotPackageImpl.init();

	/**
	 * The meta object id for the '{@link vacuumrobot.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see vacuumrobot.impl.NamedElementImpl
	 * @see vacuumrobot.impl.VacuumrobotPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link vacuumrobot.impl.VacuumRobotSystemImpl <em>Vacuum Robot System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see vacuumrobot.impl.VacuumRobotSystemImpl
	 * @see vacuumrobot.impl.VacuumrobotPackageImpl#getVacuumRobotSystem()
	 * @generated
	 */
	int VACUUM_ROBOT_SYSTEM = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_SYSTEM__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Room</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_SYSTEM__ROOM = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Vacuum Robot</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Vacuum Robot Manager</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT_MANAGER = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Vacuum Robot System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_SYSTEM_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Vacuum Robot System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_SYSTEM_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link vacuumrobot.impl.RoomImpl <em>Room</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see vacuumrobot.impl.RoomImpl
	 * @see vacuumrobot.impl.VacuumrobotPackageImpl#getRoom()
	 * @generated
	 */
	int ROOM = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Has Charging Station</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__HAS_CHARGING_STATION = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Adjacent Rooms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__ADJACENT_ROOMS = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link vacuumrobot.impl.VacuumRobotImpl <em>Vacuum Robot</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see vacuumrobot.impl.VacuumRobotImpl
	 * @see vacuumrobot.impl.VacuumrobotPackageImpl#getVacuumRobot()
	 * @generated
	 */
	int VACUUM_ROBOT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT__CONTROLLER = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Max Charge</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT__MAX_CHARGE = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Vacuum Robot</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Move To Adjacent Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT___MOVE_TO_ADJACENT_ROOM__ROOM = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Vacuum Robot</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link vacuumrobot.impl.VacuumRobotControllerImpl <em>Vacuum Robot Controller</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see vacuumrobot.impl.VacuumRobotControllerImpl
	 * @see vacuumrobot.impl.VacuumrobotPackageImpl#getVacuumRobotController()
	 * @generated
	 */
	int VACUUM_ROBOT_CONTROLLER = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_CONTROLLER__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Robot</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_CONTROLLER__ROBOT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Moving</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_CONTROLLER__MOVING = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Room</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_CONTROLLER__ROOM = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Charge</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_CONTROLLER__CHARGE = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Vacuum Robot Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_CONTROLLER_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Arrived In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_CONTROLLER___ARRIVED_IN_ROOM__ROOM = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Start Moving</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_CONTROLLER___START_MOVING = NAMED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Vacuum Robot Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_CONTROLLER_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link vacuumrobot.impl.VacuumRobotManagerImpl <em>Vacuum Robot Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see vacuumrobot.impl.VacuumRobotManagerImpl
	 * @see vacuumrobot.impl.VacuumrobotPackageImpl#getVacuumRobotManager()
	 * @generated
	 */
	int VACUUM_ROBOT_MANAGER = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_MANAGER__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Vacuum Robot Controllers</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_MANAGER__VACUUM_ROBOT_CONTROLLERS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dirty Rooms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_MANAGER__DIRTY_ROOMS = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Vacuum Robot Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_MANAGER_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Dirt Detected</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_MANAGER___DIRT_DETECTED = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Robot Cleaned Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_MANAGER___ROBOT_CLEANED_ROOM__DIRTYABLEROOM = NAMED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Order Robot To Clean Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_MANAGER___ORDER_ROBOT_TO_CLEAN_ROOM__VACUUMROBOTCONTROLLER = NAMED_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Vacuum Robot Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_MANAGER_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link vacuumrobot.impl.DirtyableRoomImpl <em>Dirtyable Room</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see vacuumrobot.impl.DirtyableRoomImpl
	 * @see vacuumrobot.impl.VacuumrobotPackageImpl#getDirtyableRoom()
	 * @generated
	 */
	int DIRTYABLE_ROOM = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRTYABLE_ROOM__NAME = ROOM__NAME;

	/**
	 * The feature id for the '<em><b>Has Charging Station</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRTYABLE_ROOM__HAS_CHARGING_STATION = ROOM__HAS_CHARGING_STATION;

	/**
	 * The feature id for the '<em><b>Adjacent Rooms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRTYABLE_ROOM__ADJACENT_ROOMS = ROOM__ADJACENT_ROOMS;

	/**
	 * The number of structural features of the '<em>Dirtyable Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRTYABLE_ROOM_FEATURE_COUNT = ROOM_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dirtyable Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRTYABLE_ROOM_OPERATION_COUNT = ROOM_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link vacuumrobot.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see vacuumrobot.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link vacuumrobot.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see vacuumrobot.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link vacuumrobot.VacuumRobotSystem <em>Vacuum Robot System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vacuum Robot System</em>'.
	 * @see vacuumrobot.VacuumRobotSystem
	 * @generated
	 */
	EClass getVacuumRobotSystem();

	/**
	 * Returns the meta object for the containment reference list '{@link vacuumrobot.VacuumRobotSystem#getRoom <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Room</em>'.
	 * @see vacuumrobot.VacuumRobotSystem#getRoom()
	 * @see #getVacuumRobotSystem()
	 * @generated
	 */
	EReference getVacuumRobotSystem_Room();

	/**
	 * Returns the meta object for the containment reference list '{@link vacuumrobot.VacuumRobotSystem#getVacuumRobot <em>Vacuum Robot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Vacuum Robot</em>'.
	 * @see vacuumrobot.VacuumRobotSystem#getVacuumRobot()
	 * @see #getVacuumRobotSystem()
	 * @generated
	 */
	EReference getVacuumRobotSystem_VacuumRobot();

	/**
	 * Returns the meta object for the containment reference '{@link vacuumrobot.VacuumRobotSystem#getVacuumRobotManager <em>Vacuum Robot Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Vacuum Robot Manager</em>'.
	 * @see vacuumrobot.VacuumRobotSystem#getVacuumRobotManager()
	 * @see #getVacuumRobotSystem()
	 * @generated
	 */
	EReference getVacuumRobotSystem_VacuumRobotManager();

	/**
	 * Returns the meta object for class '{@link vacuumrobot.Room <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room</em>'.
	 * @see vacuumrobot.Room
	 * @generated
	 */
	EClass getRoom();

	/**
	 * Returns the meta object for the attribute '{@link vacuumrobot.Room#isHasChargingStation <em>Has Charging Station</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Charging Station</em>'.
	 * @see vacuumrobot.Room#isHasChargingStation()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_HasChargingStation();

	/**
	 * Returns the meta object for the reference list '{@link vacuumrobot.Room#getAdjacentRooms <em>Adjacent Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Adjacent Rooms</em>'.
	 * @see vacuumrobot.Room#getAdjacentRooms()
	 * @see #getRoom()
	 * @generated
	 */
	EReference getRoom_AdjacentRooms();

	/**
	 * Returns the meta object for class '{@link vacuumrobot.VacuumRobot <em>Vacuum Robot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vacuum Robot</em>'.
	 * @see vacuumrobot.VacuumRobot
	 * @generated
	 */
	EClass getVacuumRobot();

	/**
	 * Returns the meta object for the containment reference '{@link vacuumrobot.VacuumRobot#getController <em>Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Controller</em>'.
	 * @see vacuumrobot.VacuumRobot#getController()
	 * @see #getVacuumRobot()
	 * @generated
	 */
	EReference getVacuumRobot_Controller();

	/**
	 * Returns the meta object for the attribute '{@link vacuumrobot.VacuumRobot#getMaxCharge <em>Max Charge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Charge</em>'.
	 * @see vacuumrobot.VacuumRobot#getMaxCharge()
	 * @see #getVacuumRobot()
	 * @generated
	 */
	EAttribute getVacuumRobot_MaxCharge();

	/**
	 * Returns the meta object for the '{@link vacuumrobot.VacuumRobot#moveToAdjacentRoom(vacuumrobot.Room) <em>Move To Adjacent Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move To Adjacent Room</em>' operation.
	 * @see vacuumrobot.VacuumRobot#moveToAdjacentRoom(vacuumrobot.Room)
	 * @generated
	 */
	EOperation getVacuumRobot__MoveToAdjacentRoom__Room();

	/**
	 * Returns the meta object for class '{@link vacuumrobot.VacuumRobotController <em>Vacuum Robot Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vacuum Robot Controller</em>'.
	 * @see vacuumrobot.VacuumRobotController
	 * @generated
	 */
	EClass getVacuumRobotController();

	/**
	 * Returns the meta object for the container reference '{@link vacuumrobot.VacuumRobotController#getRobot <em>Robot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Robot</em>'.
	 * @see vacuumrobot.VacuumRobotController#getRobot()
	 * @see #getVacuumRobotController()
	 * @generated
	 */
	EReference getVacuumRobotController_Robot();

	/**
	 * Returns the meta object for the attribute '{@link vacuumrobot.VacuumRobotController#isMoving <em>Moving</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Moving</em>'.
	 * @see vacuumrobot.VacuumRobotController#isMoving()
	 * @see #getVacuumRobotController()
	 * @generated
	 */
	EAttribute getVacuumRobotController_Moving();

	/**
	 * Returns the meta object for the reference '{@link vacuumrobot.VacuumRobotController#getRoom <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room</em>'.
	 * @see vacuumrobot.VacuumRobotController#getRoom()
	 * @see #getVacuumRobotController()
	 * @generated
	 */
	EReference getVacuumRobotController_Room();

	/**
	 * Returns the meta object for the attribute '{@link vacuumrobot.VacuumRobotController#getCharge <em>Charge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Charge</em>'.
	 * @see vacuumrobot.VacuumRobotController#getCharge()
	 * @see #getVacuumRobotController()
	 * @generated
	 */
	EAttribute getVacuumRobotController_Charge();

	/**
	 * Returns the meta object for the '{@link vacuumrobot.VacuumRobotController#arrivedInRoom(vacuumrobot.Room) <em>Arrived In Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Arrived In Room</em>' operation.
	 * @see vacuumrobot.VacuumRobotController#arrivedInRoom(vacuumrobot.Room)
	 * @generated
	 */
	EOperation getVacuumRobotController__ArrivedInRoom__Room();

	/**
	 * Returns the meta object for the '{@link vacuumrobot.VacuumRobotController#startMoving() <em>Start Moving</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Start Moving</em>' operation.
	 * @see vacuumrobot.VacuumRobotController#startMoving()
	 * @generated
	 */
	EOperation getVacuumRobotController__StartMoving();

	/**
	 * Returns the meta object for class '{@link vacuumrobot.VacuumRobotManager <em>Vacuum Robot Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vacuum Robot Manager</em>'.
	 * @see vacuumrobot.VacuumRobotManager
	 * @generated
	 */
	EClass getVacuumRobotManager();

	/**
	 * Returns the meta object for the reference list '{@link vacuumrobot.VacuumRobotManager#getVacuumRobotControllers <em>Vacuum Robot Controllers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Vacuum Robot Controllers</em>'.
	 * @see vacuumrobot.VacuumRobotManager#getVacuumRobotControllers()
	 * @see #getVacuumRobotManager()
	 * @generated
	 */
	EReference getVacuumRobotManager_VacuumRobotControllers();

	/**
	 * Returns the meta object for the reference list '{@link vacuumrobot.VacuumRobotManager#getDirtyRooms <em>Dirty Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Dirty Rooms</em>'.
	 * @see vacuumrobot.VacuumRobotManager#getDirtyRooms()
	 * @see #getVacuumRobotManager()
	 * @generated
	 */
	EReference getVacuumRobotManager_DirtyRooms();

	/**
	 * Returns the meta object for the '{@link vacuumrobot.VacuumRobotManager#dirtDetected() <em>Dirt Detected</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Dirt Detected</em>' operation.
	 * @see vacuumrobot.VacuumRobotManager#dirtDetected()
	 * @generated
	 */
	EOperation getVacuumRobotManager__DirtDetected();

	/**
	 * Returns the meta object for the '{@link vacuumrobot.VacuumRobotManager#robotCleanedRoom(vacuumrobot.DirtyableRoom) <em>Robot Cleaned Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Robot Cleaned Room</em>' operation.
	 * @see vacuumrobot.VacuumRobotManager#robotCleanedRoom(vacuumrobot.DirtyableRoom)
	 * @generated
	 */
	EOperation getVacuumRobotManager__RobotCleanedRoom__DirtyableRoom();

	/**
	 * Returns the meta object for the '{@link vacuumrobot.VacuumRobotManager#orderRobotToCleanRoom(vacuumrobot.VacuumRobotController) <em>Order Robot To Clean Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Order Robot To Clean Room</em>' operation.
	 * @see vacuumrobot.VacuumRobotManager#orderRobotToCleanRoom(vacuumrobot.VacuumRobotController)
	 * @generated
	 */
	EOperation getVacuumRobotManager__OrderRobotToCleanRoom__VacuumRobotController();

	/**
	 * Returns the meta object for class '{@link vacuumrobot.DirtyableRoom <em>Dirtyable Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dirtyable Room</em>'.
	 * @see vacuumrobot.DirtyableRoom
	 * @generated
	 */
	EClass getDirtyableRoom();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	VacuumrobotFactory getVacuumrobotFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link vacuumrobot.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see vacuumrobot.impl.NamedElementImpl
		 * @see vacuumrobot.impl.VacuumrobotPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link vacuumrobot.impl.VacuumRobotSystemImpl <em>Vacuum Robot System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see vacuumrobot.impl.VacuumRobotSystemImpl
		 * @see vacuumrobot.impl.VacuumrobotPackageImpl#getVacuumRobotSystem()
		 * @generated
		 */
		EClass VACUUM_ROBOT_SYSTEM = eINSTANCE.getVacuumRobotSystem();

		/**
		 * The meta object literal for the '<em><b>Room</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VACUUM_ROBOT_SYSTEM__ROOM = eINSTANCE.getVacuumRobotSystem_Room();

		/**
		 * The meta object literal for the '<em><b>Vacuum Robot</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT = eINSTANCE.getVacuumRobotSystem_VacuumRobot();

		/**
		 * The meta object literal for the '<em><b>Vacuum Robot Manager</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT_MANAGER = eINSTANCE.getVacuumRobotSystem_VacuumRobotManager();

		/**
		 * The meta object literal for the '{@link vacuumrobot.impl.RoomImpl <em>Room</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see vacuumrobot.impl.RoomImpl
		 * @see vacuumrobot.impl.VacuumrobotPackageImpl#getRoom()
		 * @generated
		 */
		EClass ROOM = eINSTANCE.getRoom();

		/**
		 * The meta object literal for the '<em><b>Has Charging Station</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__HAS_CHARGING_STATION = eINSTANCE.getRoom_HasChargingStation();

		/**
		 * The meta object literal for the '<em><b>Adjacent Rooms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM__ADJACENT_ROOMS = eINSTANCE.getRoom_AdjacentRooms();

		/**
		 * The meta object literal for the '{@link vacuumrobot.impl.VacuumRobotImpl <em>Vacuum Robot</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see vacuumrobot.impl.VacuumRobotImpl
		 * @see vacuumrobot.impl.VacuumrobotPackageImpl#getVacuumRobot()
		 * @generated
		 */
		EClass VACUUM_ROBOT = eINSTANCE.getVacuumRobot();

		/**
		 * The meta object literal for the '<em><b>Controller</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VACUUM_ROBOT__CONTROLLER = eINSTANCE.getVacuumRobot_Controller();

		/**
		 * The meta object literal for the '<em><b>Max Charge</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VACUUM_ROBOT__MAX_CHARGE = eINSTANCE.getVacuumRobot_MaxCharge();

		/**
		 * The meta object literal for the '<em><b>Move To Adjacent Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation VACUUM_ROBOT___MOVE_TO_ADJACENT_ROOM__ROOM = eINSTANCE.getVacuumRobot__MoveToAdjacentRoom__Room();

		/**
		 * The meta object literal for the '{@link vacuumrobot.impl.VacuumRobotControllerImpl <em>Vacuum Robot Controller</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see vacuumrobot.impl.VacuumRobotControllerImpl
		 * @see vacuumrobot.impl.VacuumrobotPackageImpl#getVacuumRobotController()
		 * @generated
		 */
		EClass VACUUM_ROBOT_CONTROLLER = eINSTANCE.getVacuumRobotController();

		/**
		 * The meta object literal for the '<em><b>Robot</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VACUUM_ROBOT_CONTROLLER__ROBOT = eINSTANCE.getVacuumRobotController_Robot();

		/**
		 * The meta object literal for the '<em><b>Moving</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VACUUM_ROBOT_CONTROLLER__MOVING = eINSTANCE.getVacuumRobotController_Moving();

		/**
		 * The meta object literal for the '<em><b>Room</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VACUUM_ROBOT_CONTROLLER__ROOM = eINSTANCE.getVacuumRobotController_Room();

		/**
		 * The meta object literal for the '<em><b>Charge</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VACUUM_ROBOT_CONTROLLER__CHARGE = eINSTANCE.getVacuumRobotController_Charge();

		/**
		 * The meta object literal for the '<em><b>Arrived In Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation VACUUM_ROBOT_CONTROLLER___ARRIVED_IN_ROOM__ROOM = eINSTANCE.getVacuumRobotController__ArrivedInRoom__Room();

		/**
		 * The meta object literal for the '<em><b>Start Moving</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation VACUUM_ROBOT_CONTROLLER___START_MOVING = eINSTANCE.getVacuumRobotController__StartMoving();

		/**
		 * The meta object literal for the '{@link vacuumrobot.impl.VacuumRobotManagerImpl <em>Vacuum Robot Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see vacuumrobot.impl.VacuumRobotManagerImpl
		 * @see vacuumrobot.impl.VacuumrobotPackageImpl#getVacuumRobotManager()
		 * @generated
		 */
		EClass VACUUM_ROBOT_MANAGER = eINSTANCE.getVacuumRobotManager();

		/**
		 * The meta object literal for the '<em><b>Vacuum Robot Controllers</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VACUUM_ROBOT_MANAGER__VACUUM_ROBOT_CONTROLLERS = eINSTANCE.getVacuumRobotManager_VacuumRobotControllers();

		/**
		 * The meta object literal for the '<em><b>Dirty Rooms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VACUUM_ROBOT_MANAGER__DIRTY_ROOMS = eINSTANCE.getVacuumRobotManager_DirtyRooms();

		/**
		 * The meta object literal for the '<em><b>Dirt Detected</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation VACUUM_ROBOT_MANAGER___DIRT_DETECTED = eINSTANCE.getVacuumRobotManager__DirtDetected();

		/**
		 * The meta object literal for the '<em><b>Robot Cleaned Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation VACUUM_ROBOT_MANAGER___ROBOT_CLEANED_ROOM__DIRTYABLEROOM = eINSTANCE.getVacuumRobotManager__RobotCleanedRoom__DirtyableRoom();

		/**
		 * The meta object literal for the '<em><b>Order Robot To Clean Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation VACUUM_ROBOT_MANAGER___ORDER_ROBOT_TO_CLEAN_ROOM__VACUUMROBOTCONTROLLER = eINSTANCE.getVacuumRobotManager__OrderRobotToCleanRoom__VacuumRobotController();

		/**
		 * The meta object literal for the '{@link vacuumrobot.impl.DirtyableRoomImpl <em>Dirtyable Room</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see vacuumrobot.impl.DirtyableRoomImpl
		 * @see vacuumrobot.impl.VacuumrobotPackageImpl#getDirtyableRoom()
		 * @generated
		 */
		EClass DIRTYABLE_ROOM = eINSTANCE.getDirtyableRoom();

	}

} //VacuumrobotPackage
