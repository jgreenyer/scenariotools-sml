/**
 */
package vacuumrobot;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vacuum Robot System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link vacuumrobot.VacuumRobotSystem#getRoom <em>Room</em>}</li>
 *   <li>{@link vacuumrobot.VacuumRobotSystem#getVacuumRobot <em>Vacuum Robot</em>}</li>
 *   <li>{@link vacuumrobot.VacuumRobotSystem#getVacuumRobotManager <em>Vacuum Robot Manager</em>}</li>
 * </ul>
 *
 * @see vacuumrobot.VacuumrobotPackage#getVacuumRobotSystem()
 * @model
 * @generated
 */
public interface VacuumRobotSystem extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Room</b></em>' containment reference list.
	 * The list contents are of type {@link vacuumrobot.Room}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room</em>' containment reference list.
	 * @see vacuumrobot.VacuumrobotPackage#getVacuumRobotSystem_Room()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Room> getRoom();

	/**
	 * Returns the value of the '<em><b>Vacuum Robot</b></em>' containment reference list.
	 * The list contents are of type {@link vacuumrobot.VacuumRobot}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vacuum Robot</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vacuum Robot</em>' containment reference list.
	 * @see vacuumrobot.VacuumrobotPackage#getVacuumRobotSystem_VacuumRobot()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<VacuumRobot> getVacuumRobot();

	/**
	 * Returns the value of the '<em><b>Vacuum Robot Manager</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vacuum Robot Manager</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vacuum Robot Manager</em>' containment reference.
	 * @see #setVacuumRobotManager(VacuumRobotManager)
	 * @see vacuumrobot.VacuumrobotPackage#getVacuumRobotSystem_VacuumRobotManager()
	 * @model containment="true" required="true"
	 * @generated
	 */
	VacuumRobotManager getVacuumRobotManager();

	/**
	 * Sets the value of the '{@link vacuumrobot.VacuumRobotSystem#getVacuumRobotManager <em>Vacuum Robot Manager</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vacuum Robot Manager</em>' containment reference.
	 * @see #getVacuumRobotManager()
	 * @generated
	 */
	void setVacuumRobotManager(VacuumRobotManager value);

} // VacuumRobotSystem
