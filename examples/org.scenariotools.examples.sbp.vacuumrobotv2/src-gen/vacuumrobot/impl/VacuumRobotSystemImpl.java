/**
 */
package vacuumrobot.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import vacuumrobot.Room;
import vacuumrobot.VacuumRobot;
import vacuumrobot.VacuumRobotManager;
import vacuumrobot.VacuumRobotSystem;
import vacuumrobot.VacuumrobotPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Vacuum Robot System</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link vacuumrobot.impl.VacuumRobotSystemImpl#getRoom <em>Room</em>}</li>
 *   <li>{@link vacuumrobot.impl.VacuumRobotSystemImpl#getVacuumRobot <em>Vacuum Robot</em>}</li>
 *   <li>{@link vacuumrobot.impl.VacuumRobotSystemImpl#getVacuumRobotManager <em>Vacuum Robot Manager</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VacuumRobotSystemImpl extends NamedElementImpl implements VacuumRobotSystem {
	/**
	 * The cached value of the '{@link #getRoom() <em>Room</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoom()
	 * @generated
	 * @ordered
	 */
	protected EList<Room> room;

	/**
	 * The cached value of the '{@link #getVacuumRobot() <em>Vacuum Robot</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVacuumRobot()
	 * @generated
	 * @ordered
	 */
	protected EList<VacuumRobot> vacuumRobot;

	/**
	 * The cached value of the '{@link #getVacuumRobotManager() <em>Vacuum Robot Manager</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVacuumRobotManager()
	 * @generated
	 * @ordered
	 */
	protected VacuumRobotManager vacuumRobotManager;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VacuumRobotSystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VacuumrobotPackage.Literals.VACUUM_ROBOT_SYSTEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Room> getRoom() {
		if (room == null) {
			room = new EObjectContainmentEList<Room>(Room.class, this, VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__ROOM);
		}
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VacuumRobot> getVacuumRobot() {
		if (vacuumRobot == null) {
			vacuumRobot = new EObjectContainmentEList<VacuumRobot>(VacuumRobot.class, this, VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT);
		}
		return vacuumRobot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VacuumRobotManager getVacuumRobotManager() {
		return vacuumRobotManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVacuumRobotManager(VacuumRobotManager newVacuumRobotManager, NotificationChain msgs) {
		VacuumRobotManager oldVacuumRobotManager = vacuumRobotManager;
		vacuumRobotManager = newVacuumRobotManager;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT_MANAGER, oldVacuumRobotManager, newVacuumRobotManager);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVacuumRobotManager(VacuumRobotManager newVacuumRobotManager) {
		if (newVacuumRobotManager != vacuumRobotManager) {
			NotificationChain msgs = null;
			if (vacuumRobotManager != null)
				msgs = ((InternalEObject)vacuumRobotManager).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT_MANAGER, null, msgs);
			if (newVacuumRobotManager != null)
				msgs = ((InternalEObject)newVacuumRobotManager).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT_MANAGER, null, msgs);
			msgs = basicSetVacuumRobotManager(newVacuumRobotManager, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT_MANAGER, newVacuumRobotManager, newVacuumRobotManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__ROOM:
				return ((InternalEList<?>)getRoom()).basicRemove(otherEnd, msgs);
			case VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT:
				return ((InternalEList<?>)getVacuumRobot()).basicRemove(otherEnd, msgs);
			case VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT_MANAGER:
				return basicSetVacuumRobotManager(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__ROOM:
				return getRoom();
			case VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT:
				return getVacuumRobot();
			case VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT_MANAGER:
				return getVacuumRobotManager();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__ROOM:
				getRoom().clear();
				getRoom().addAll((Collection<? extends Room>)newValue);
				return;
			case VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT:
				getVacuumRobot().clear();
				getVacuumRobot().addAll((Collection<? extends VacuumRobot>)newValue);
				return;
			case VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT_MANAGER:
				setVacuumRobotManager((VacuumRobotManager)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__ROOM:
				getRoom().clear();
				return;
			case VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT:
				getVacuumRobot().clear();
				return;
			case VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT_MANAGER:
				setVacuumRobotManager((VacuumRobotManager)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__ROOM:
				return room != null && !room.isEmpty();
			case VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT:
				return vacuumRobot != null && !vacuumRobot.isEmpty();
			case VacuumrobotPackage.VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT_MANAGER:
				return vacuumRobotManager != null;
		}
		return super.eIsSet(featureID);
	}

} //VacuumRobotSystemImpl
