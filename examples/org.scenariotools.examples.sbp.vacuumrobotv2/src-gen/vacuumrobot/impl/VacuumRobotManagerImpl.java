/**
 */
package vacuumrobot.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import vacuumrobot.DirtyableRoom;
import vacuumrobot.VacuumRobotController;
import vacuumrobot.VacuumRobotManager;
import vacuumrobot.VacuumrobotPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Vacuum Robot Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link vacuumrobot.impl.VacuumRobotManagerImpl#getVacuumRobotControllers <em>Vacuum Robot Controllers</em>}</li>
 *   <li>{@link vacuumrobot.impl.VacuumRobotManagerImpl#getDirtyRooms <em>Dirty Rooms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VacuumRobotManagerImpl extends NamedElementImpl implements VacuumRobotManager {
	/**
	 * The cached value of the '{@link #getVacuumRobotControllers() <em>Vacuum Robot Controllers</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVacuumRobotControllers()
	 * @generated
	 * @ordered
	 */
	protected EList<VacuumRobotController> vacuumRobotControllers;

	/**
	 * The cached value of the '{@link #getDirtyRooms() <em>Dirty Rooms</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDirtyRooms()
	 * @generated
	 * @ordered
	 */
	protected EList<DirtyableRoom> dirtyRooms;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VacuumRobotManagerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VacuumrobotPackage.Literals.VACUUM_ROBOT_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VacuumRobotController> getVacuumRobotControllers() {
		if (vacuumRobotControllers == null) {
			vacuumRobotControllers = new EObjectResolvingEList<VacuumRobotController>(VacuumRobotController.class, this, VacuumrobotPackage.VACUUM_ROBOT_MANAGER__VACUUM_ROBOT_CONTROLLERS);
		}
		return vacuumRobotControllers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DirtyableRoom> getDirtyRooms() {
		if (dirtyRooms == null) {
			dirtyRooms = new EObjectResolvingEList<DirtyableRoom>(DirtyableRoom.class, this, VacuumrobotPackage.VACUUM_ROBOT_MANAGER__DIRTY_ROOMS);
		}
		return dirtyRooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void dirtDetected() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void robotCleanedRoom(DirtyableRoom dirtyableRoom) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void orderRobotToCleanRoom(VacuumRobotController vacuumRobotController) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT_MANAGER__VACUUM_ROBOT_CONTROLLERS:
				return getVacuumRobotControllers();
			case VacuumrobotPackage.VACUUM_ROBOT_MANAGER__DIRTY_ROOMS:
				return getDirtyRooms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT_MANAGER__VACUUM_ROBOT_CONTROLLERS:
				getVacuumRobotControllers().clear();
				getVacuumRobotControllers().addAll((Collection<? extends VacuumRobotController>)newValue);
				return;
			case VacuumrobotPackage.VACUUM_ROBOT_MANAGER__DIRTY_ROOMS:
				getDirtyRooms().clear();
				getDirtyRooms().addAll((Collection<? extends DirtyableRoom>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT_MANAGER__VACUUM_ROBOT_CONTROLLERS:
				getVacuumRobotControllers().clear();
				return;
			case VacuumrobotPackage.VACUUM_ROBOT_MANAGER__DIRTY_ROOMS:
				getDirtyRooms().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT_MANAGER__VACUUM_ROBOT_CONTROLLERS:
				return vacuumRobotControllers != null && !vacuumRobotControllers.isEmpty();
			case VacuumrobotPackage.VACUUM_ROBOT_MANAGER__DIRTY_ROOMS:
				return dirtyRooms != null && !dirtyRooms.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case VacuumrobotPackage.VACUUM_ROBOT_MANAGER___DIRT_DETECTED:
				dirtDetected();
				return null;
			case VacuumrobotPackage.VACUUM_ROBOT_MANAGER___ROBOT_CLEANED_ROOM__DIRTYABLEROOM:
				robotCleanedRoom((DirtyableRoom)arguments.get(0));
				return null;
			case VacuumrobotPackage.VACUUM_ROBOT_MANAGER___ORDER_ROBOT_TO_CLEAN_ROOM__VACUUMROBOTCONTROLLER:
				orderRobotToCleanRoom((VacuumRobotController)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //VacuumRobotManagerImpl
