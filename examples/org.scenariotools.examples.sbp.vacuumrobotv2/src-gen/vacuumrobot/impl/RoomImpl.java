/**
 */
package vacuumrobot.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import vacuumrobot.Room;
import vacuumrobot.VacuumrobotPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Room</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link vacuumrobot.impl.RoomImpl#isHasChargingStation <em>Has Charging Station</em>}</li>
 *   <li>{@link vacuumrobot.impl.RoomImpl#getAdjacentRooms <em>Adjacent Rooms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomImpl extends NamedElementImpl implements Room {
	/**
	 * The default value of the '{@link #isHasChargingStation() <em>Has Charging Station</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasChargingStation()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HAS_CHARGING_STATION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHasChargingStation() <em>Has Charging Station</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasChargingStation()
	 * @generated
	 * @ordered
	 */
	protected boolean hasChargingStation = HAS_CHARGING_STATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAdjacentRooms() <em>Adjacent Rooms</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdjacentRooms()
	 * @generated
	 * @ordered
	 */
	protected EList<Room> adjacentRooms;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VacuumrobotPackage.Literals.ROOM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHasChargingStation() {
		return hasChargingStation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasChargingStation(boolean newHasChargingStation) {
		boolean oldHasChargingStation = hasChargingStation;
		hasChargingStation = newHasChargingStation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VacuumrobotPackage.ROOM__HAS_CHARGING_STATION, oldHasChargingStation, hasChargingStation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Room> getAdjacentRooms() {
		if (adjacentRooms == null) {
			adjacentRooms = new EObjectResolvingEList<Room>(Room.class, this, VacuumrobotPackage.ROOM__ADJACENT_ROOMS);
		}
		return adjacentRooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VacuumrobotPackage.ROOM__HAS_CHARGING_STATION:
				return isHasChargingStation();
			case VacuumrobotPackage.ROOM__ADJACENT_ROOMS:
				return getAdjacentRooms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VacuumrobotPackage.ROOM__HAS_CHARGING_STATION:
				setHasChargingStation((Boolean)newValue);
				return;
			case VacuumrobotPackage.ROOM__ADJACENT_ROOMS:
				getAdjacentRooms().clear();
				getAdjacentRooms().addAll((Collection<? extends Room>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VacuumrobotPackage.ROOM__HAS_CHARGING_STATION:
				setHasChargingStation(HAS_CHARGING_STATION_EDEFAULT);
				return;
			case VacuumrobotPackage.ROOM__ADJACENT_ROOMS:
				getAdjacentRooms().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VacuumrobotPackage.ROOM__HAS_CHARGING_STATION:
				return hasChargingStation != HAS_CHARGING_STATION_EDEFAULT;
			case VacuumrobotPackage.ROOM__ADJACENT_ROOMS:
				return adjacentRooms != null && !adjacentRooms.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (hasChargingStation: ");
		result.append(hasChargingStation);
		result.append(')');
		return result.toString();
	}

} //RoomImpl
