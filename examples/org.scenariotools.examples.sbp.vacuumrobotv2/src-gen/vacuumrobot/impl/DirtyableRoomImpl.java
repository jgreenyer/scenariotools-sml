/**
 */
package vacuumrobot.impl;

import org.eclipse.emf.ecore.EClass;

import vacuumrobot.DirtyableRoom;
import vacuumrobot.VacuumrobotPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dirtyable Room</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DirtyableRoomImpl extends RoomImpl implements DirtyableRoom {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DirtyableRoomImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VacuumrobotPackage.Literals.DIRTYABLE_ROOM;
	}

} //DirtyableRoomImpl
