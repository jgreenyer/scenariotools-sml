import "../model.ecore"

specification test_variables {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_variables{
		
		static role A a 
		dynamic role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 bindings [b = a.b] {
			
			env -> a.opA1()
			var EString x = "test"
			var EString y = x
			var EString z = a.name
			urgent a -> env.opEnv1()		 
			z = "bla bla"
			urgent a -> env.opEnv1()	
		}
	}
}