import "../model.ecore"

specification ManyValuedReferenceModifications {

	domain model

	controllable {
		A
		B
	}

	collaboration ManyValuedReferenceModifications {

		dynamic role A a
		dynamic role B b
		dynamic role Environment env
		/**
		 * This scenario demonstrates different collection modification operations such as 
		 * adding an element, clearing a list, etc. The comments assume that the "AdvancedSystem" instance model is used.
		 */
		guarantee scenario ManyValuedReferenceModificationsDemo bindings [
			b = a.b
		] {
			env -> a.opA1()
			// add the strings "world" and "hello", such that b.strings will be "[Hello, World"].
			strict urgent a -> b.strings.clear()
			strict urgent a -> b.strings.add("World")
			strict urgent a -> b.strings.addToFront("Hello")
			strict urgent a -> b.strings.clear()
			// add the same object to a non-unique reference. this should result in a.nonUniqueListOfB=[b,b]
			strict urgent b -> a.nonUniqueListOfB.addToFront(b)
			strict urgent b -> a.nonUniqueListOfB.addToFront(b)
			// do the same, but this time with a unique reference. This should result in a.listOfB=[b1,b]
			strict urgent b -> a.listOfB.addToFront(b)
			strict urgent b -> a.listOfB.addToFront(b)
			strict urgent b -> a.listOfB.addToFront(b.next)
			// remove b from the list. This results in a.listOfB=[b1]
			strict urgent b -> a.listOfB.remove(b)
			// this should clear the list.
			strict urgent b -> a.listOfB.clear()
			// Remove the first occurence from the list. TODO discuss if this is the intended semantics of remove actually.
			strict urgent b -> a.nonUniqueListOfB.remove(b)
			// clear the list. This message should lead to the initial state.
			strict urgent b -> a.nonUniqueListOfB.clear()
		}

	}

}