import "../model.ecore"

specification test_scenario {
	domain model
	
	controllable {
		A
		B
	}
	
	non-spontaneous events {
		A.opA2
	}
	
	collaboration test_nonSpontaneous {
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario scenario1 {
			env -> a.opA1()
			strict urgent a -> b.opB1()
		}

		guarantee scenario scenario2 {
			env -> a.opA2()
			strict urgent a -> b.opB2()
		}

		assumption scenario scenario3 {
			a -> b.opB1()
			env -> a.opA2()
		}
	}
}