import "../model.ecore"

specification test_interrupt_strictWait {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_interrupt_strictWait{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 {
			env -> a.opA1()
			strict urgent a -> b.opB1()
			interrupt [ false ] 
			wait strict [ b.intValue > 0]
			alternative [b.intValue > 5]{
				urgent a -> b.opB2()
			}or [b.intValue <= 5]{
				urgent a -> b.opB3()
			}
			strict urgent a -> b.opB1()  		 
		}
		
		assumption scenario AssumptionIncreaseBInt {
			env -> a.opA1()
			eventually env -> b.setIntValue(3)
		}
	}
}