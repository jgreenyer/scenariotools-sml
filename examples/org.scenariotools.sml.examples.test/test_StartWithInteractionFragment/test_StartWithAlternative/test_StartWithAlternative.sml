import "../../model.ecore"

specification test_StartWithAlternative {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_StartWithAlternative{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 {
			alternative{
				env -> a.opA1()
			}or{
				env -> a.opA2()
			}	
			strict urgent b -> a.opA1()
			strict urgent b -> a.opA2()
		}
	}	
}