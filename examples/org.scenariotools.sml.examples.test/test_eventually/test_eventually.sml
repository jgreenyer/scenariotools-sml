import "../model.ecore"

specification test_eventually {
	domain model
	
	controllable {
		A
	}
	
	collaboration test_eventually {
		
		static role A a 
		static role Environment env
		
		guarantee scenario buildGameGraph {
			env -> a.opA1()
			alternative {
				eventually a -> a.opA2()
			} or {
				eventually a -> a.opA3()
			} or {
				env -> a.opA4()
			}
			committed a -> a.opA5()
		} constraints [
			forbidden env -> a.opA1()
		]
		
		guarantee scenario choiceA {
			a -> a.opA2()
			a -> a.opA5()
		}
		
		guarantee scenario choiceB {
			a -> a.opA3()
			a -> a.opA5()
		}
	}
}