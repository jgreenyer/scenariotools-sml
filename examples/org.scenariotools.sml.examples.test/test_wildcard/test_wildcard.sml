import "../model.ecore"

specification test_wildcard {

	domain model

	controllable {
		A
		B
	}

	collaboration test_wildcard {

		static role Environment env
		static role A a
		static role B rightB

		guarantee scenario UpdatingB {
			env -> a.opA1()
			var EBoolean bool = false
			strict a -> rightB.opBP1(*)
			violation [ bool != false]
			urgent a -> rightB.opBP1(false)
			urgent a -> rightB.opBP1(true)
		}

		guarantee scenario WaitingForRightB {
			env -> a.opA1()
			strict urgent a -> rightB.opBP1(true)
		}

	}

}