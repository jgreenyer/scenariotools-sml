import "../model.ecore"

specification test_multipleActiveCopiesOfOneScenario {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_scenario{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 {
			env -> a.opA1()
			strict urgent a -> b.opB1()
			strict urgent b -> a.opA2()			 
			strict urgent a -> b.opB1()
			strict urgent b -> a.opA2()			 
		}
		
		guarantee scenario allowMutlipleActiveCopiesOfMe {
			a -> b.opB1()
			b -> a.opA2()			 
			a -> b.opB1()
			b -> a.opA2()			 
		}
		
	}
}