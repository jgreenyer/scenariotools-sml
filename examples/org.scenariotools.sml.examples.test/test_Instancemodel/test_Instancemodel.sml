import "../model.ecore"
specification InstanceModelTest {
	domain model
	controllable {
		A
		B
		ExampleSystem
	}
	collaboration InstanceModelTest{
		static role Environment env
		dynamic role A a
		dynamic role B b
		guarantee scenario  RelayScenario bindings [ b = a.b] {
			env -> a.opA1()
			strict urgent a -> b.opB1()
		}
	}
	
	
}