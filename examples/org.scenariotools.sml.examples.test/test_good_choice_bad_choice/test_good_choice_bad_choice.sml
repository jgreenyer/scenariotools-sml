import "../model.ecore"

specification test_good_choice_bad_choice {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_good_choice_bad_choice{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario scenario1 {
			env -> a.opA1()
//			var EBoolean x
//			urgent a -> b.opBP1(bind to x)
			alternative {
				urgent a -> b.opBP1(true)
			} or {
				urgent a -> b.opBP1(false)
			}
			a -> b.opB1()
		}

		guarantee scenario scenario2 {
			env -> a.opA1()
			urgent a -> b.opBP1(true)
			strict env -> a.opA2()
		}
		
				
	}
}