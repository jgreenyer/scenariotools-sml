import "../model.ecore"

specification test_unlive_cycle {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_unlive_cycle{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario scenario1 {
			env -> a.opA1()
			strict urgent a -> b.opB1()
		}

		guarantee scenario scenario2 {
			a -> b.opB1()
			strict urgent a -> b.opB2()
		}
		
		guarantee scenario scenario3 {
			a -> b.opB2()
			strict urgent a -> b.opB3()
		}
		
		guarantee scenario scenario4 {
			a -> b.opB2()
			strict urgent a -> b.opB1()
		}
//		constraints[
//			forbidden message a -> b.opB3()
//		]
		
		
	}
}