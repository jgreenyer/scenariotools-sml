import "../model.ecore"

specification ParameterTest {
	
	domain model
	controllable {
		A
		B
	}
	
	collaboration Cyclic {
		
		static role  A a
		dynamic role B b
		static role B lastB
		static role Environment env
		
		guarantee scenario  UpdateB bindings [b = a.b] {
			env -> a.opA1()
			interrupt [ a.b == lastB] 
			strict urgent a -> a.setB(b.next)
		}
	}
	
	
	
}