import "../model.ecore"

specification EnumerationTest {
	
	domain model

	controllable {
		A
		B
	}	
	
	collaboration SetEnumValues {
		
		static role Environment env
		static role A a
		static role B b
		
		guarantee scenario SetToA {
			//env -> a.setEnumAttr(EnumA:A) //TODO: add support for the first message of a scenario using enums
			env -> a.opA1()
			strict urgent a -> a.setEnumAttr(EnumA:A)
			strict urgent b -> b.setEnumAttr(EnumB:A)
		} constraints [
			forbidden env -> a.opA2()
			forbidden env -> a.opA3()
		]
	
		guarantee scenario SetToB {
			//env -> a.setEnumAttr(EnumA:B)
			env -> a.opA2()
			strict urgent a -> a.setEnumAttr(EnumA:B)
			strict urgent b -> b.setEnumAttr(EnumB:B)
		} constraints [
			forbidden env -> a.opA1()
			forbidden env -> a.opA3()
		]
	
		guarantee scenario SetToC {
			//env -> a.setEnumAttr(EnumA:C)
			env -> a.opA3()
			strict urgent a -> a.setEnumAttr(EnumA:C)
			strict urgent b -> b.setEnumAttr(EnumB:C)
		} constraints [
			forbidden env -> a.opA1()
			forbidden env -> a.opA2()
		]
	}	
}