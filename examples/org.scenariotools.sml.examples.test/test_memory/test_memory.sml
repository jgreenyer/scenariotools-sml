import "../model.ecore"

specification test_memory {
	domain model
	
	controllable {
		A
	}
	
	collaboration test_memory {
		
		static role A a 
		static role Environment env
		
		guarantee scenario buildGameGraph {
			env -> a.opA1()
			alternative {
				urgent a -> a.opA2()
			} or {
				urgent a -> a.opA3()
			}
			urgent a -> a.opA4()
		}

		guarantee scenario condition1 {
			env -> a.opA1()
			monitored eventually a -> a.opA2()
		}
		
		guarantee scenario condition2 {
			env -> a.opA1()
			monitored eventually a -> a.opA3()			
		}	
	}
}