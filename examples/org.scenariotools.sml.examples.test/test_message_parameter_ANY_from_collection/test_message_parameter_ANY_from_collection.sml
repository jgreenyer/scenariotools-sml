import "../model.ecore"

specification test_message_parameter_ANY_from_collection {
	domain model
	
	controllable {
		A
		B
	}
	
	non-spontaneous events {
		A.chosenB
		A.opA2
	}
	
	collaboration test_Base{
		
		static role A a 
		static role Environment env
		dynamic role B b 
		
		guarantee scenario SpecificationScenario1 {
			env -> a.chosenB(bind b)
			strict urgent a -> b.opB1()
		}

		assumption scenario AssumptionScenario1 {
			env -> a.opA1()
			env -> a.opA2()
		}
		
	}
	
	collaboration test1{
		
		static role A a 
		static role Environment env
		
		assumption scenario AssumptionScenario1 {
			env -> a.opA1()
			strict env -> a.chosenB(a.setOfB.any())
		}
	}

	collaboration test2{
		
		static role A a 
		static role Environment env
		
		assumption scenario AssumptionScenario1 {
			env -> a.opA1()
			strict committed env -> a.chosenB(a.setOfB.any())
		}
	}
	
	collaboration test3{
		
		static role A a 
		static role Environment env
		
		assumption scenario AssumptionScenario1 {
			env -> a.opA1()
			strict eventually env -> a.chosenB(a.setOfB.any())
		}
	}	

	collaboration test4{
		
		static role A a 
		static role Environment env
		
		assumption scenario AssumptionScenario1 {
			env -> a.opA1()
			strict eventually env -> a.chosenB(a.setOfB.any())
		}
	}	

	collaboration test5{
		
		static role A a 
		static role Environment env
		
		assumption scenario AssumptionScenario1 {
			env -> a.opA1()
			strict eventually env -> a.chosenB(a.setOfB.any())
		}
	}
		
}