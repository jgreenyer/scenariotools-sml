import "../model/organicdesign.ecore"
import "../model/organicdesignhelper.ecore"

specification OrganicDesign{
	
	domain organicdesign

    contexts organicdesignhelper

	controllable {
		Agent
	}
	

	collaboration OrganicDesign {

		static role Environment env
		dynamic role TransformationAgent a
		dynamic role Resource r
		dynamic multi role TransportAgent c
		dynamic multi role TransformationAgent nextA

		guarantee scenario ResourceArrives 
		context PerformTaskContext{
			env->a.resourceArrived(bind r)
			while [!PerformTaskContext.tasksThatAgentCanPerform.isEmpty()]{
				strict requested a->r.performTask(PerformTaskContext.tasksThatAgentCanPerform.any())
				r->a.taskPerformed()
			} 
			strict requested a->a.arrangePickUpFor(r)
		}constraints[
			ignore env->a.resourceArrived(*)
			ignore a->a.arrangePickUpFor(*)
			ignore r->a.taskPerformed()
		]
		

		guarantee scenario ArrangePickUpForResource
		context ArrangePickUpForResource
		bindings [
			c = a.connectedBy
		]
		{
			a->a.arrangePickUpFor(bind r)
			strict requested a->c.pickUpResourceAtLocationRequest(r,a)
			alternative [c.locatedResources.isEmpty()]{
				strict requested c->a.availableForPickUpOfResourceAtLocation(r, a)
				strict committed a->a.pickUpArrangedForResource(r)
			} or [!c.locatedResources.isEmpty()]{
				strict requested c->a.unavailableForPickUpOfResourceAtLocation(r, a)
			}			
		}constraints[
			interrupt a->a.pickUpArrangedForResource(r)
		]

		guarantee scenario TransportAgentMovesToPickUpResourceUponRequest
		{
			c->a.availableForPickUpOfResourceAtLocation(bind r, a)
			strict requested c->env.moveToLocation(a)
			env->c.arrivesAtLocation(a)
			strict requested c->a.readyToLoadResource(r)
		}		

		guarantee scenario TransportAgentPicksUpResource
		{
			c->a.readyToLoadResource(bind r)
			strict requested a->env.loadResourceOntoTransportAgent(r, c)
			env->a.resourceLoadedOntoTransportAgent(r, c)
			strict requested a->c.resourceLoaded(r)
		}		

		guarantee scenario TransportAgentSeeksNextAgentForResource
//		context NextAgentForResourceHelper
		bindings [
			nextA = c.connectedLocations
		]
		{
			a->c.resourceLoaded(bind r)
			strict requested a->nextA.transformResourceRequest(r)
			alternative { // TODO add if...
				strict requested nextA->c.availableForTransformationOfResource(r)
				strict requested c->c.destinationTransformationAgentFound(nextA)
			} or {
				strict requested nextA->c.unavailableForTransformationOfResource(r)
			}
		}constraints[
			interrupt c->c.destinationTransformationAgentFound(*)
		]

		assumption scenario TransportAgentFullfillsMoveToLocationCommand
		{
			c->env.moveToLocation(bind a)
			
			// not strict since another moveToLocation command shall not violate assumption
			eventually env->c.arrivesAtLocation(a)
		}

		assumption scenario ResourceWillBeLoadedOntoTransportAgent
		{
			a->env.loadResourceOntoTransportAgent(bind r, bind c)
			eventually env->a.resourceLoadedOntoTransportAgent(r, c)
		}
		
		
	}
	
}