import "../model/productioncell.ecore"

specification ProductioncellSpecification {

	domain productioncell

	controllable {
		Controller
	}
	
	non-spontaneous events {
		Controller.accelerateAndMove
		Controller.decelerate
		Controller.arrived
		Controller.finishedTask
		Robot.setReady
		Robot.setAtHomeLocation
	}

	collaboration PerformTasks {

		static role Environment env
		dynamic role Controller controller
		dynamic multi role Robot robot

		guarantee scenario PerformTasks
		optimize cost
		bindings [
			robot = env.robots
		] {
			env -> controller.performTasks()
			violation [!(robot.atHomeLocation && robot.ready)]
			eventually controller -> robot.move()				
			robot -> controller.arrived()
			eventually controller -> robot.performTask()
			robot -> controller.finishedTask()
			eventually controller -> robot.move()
			robot -> controller.arrived()
			eventually controller -> robot.getReady()
		}

		assumption scenario WaitBeforeTriggeringNextIteration
		bindings [
			robot = env.robots
		] {
			env -> controller.performTasks()
			wait [!robot.ready] // wait for the robot to switch ready states once
			wait [robot.ready]
		} constraints [
			forbidden env -> controller.performTasks()
		]		
	}
	
	collaboration RobotBehavior {
		
		dynamic role Controller controller
		dynamic role Robot robot
		
		assumption scenario RobotMovesToLocation {
			controller -> robot.move()
			committed robot -> controller.accelerateAndMove()
			eventually robot -> controller.decelerate()
			eventually robot -> robot.setAtHomeLocation(!robot.atHomeLocation)
			committed robot -> controller.arrived()
		}
		
		assumption scenario RobotPerformsTask {
			controller -> robot.performTask()
			committed robot -> robot.setReady(false)
			eventually robot -> controller.finishedTask()
		}
		
		assumption scenario RobotGetsReadyForNextIteration {
			controller -> robot.getReady()
			eventually robot -> robot.setReady(true)
		}
	}

	collaboration Costs {
		
		dynamic role Controller controller
		dynamic role Robot robot
		
		assumption scenario AccelerationAndMovementEnergy
		cost [15.0] {
			robot -> controller.accelerateAndMove()
			monitored eventually robot -> controller.decelerate()
		}
		
		assumption scenario BrakingEnergy
		cost [-5.0] {
			robot -> controller.decelerate()
			monitored eventually robot -> controller.arrived()
		}
	}
}
