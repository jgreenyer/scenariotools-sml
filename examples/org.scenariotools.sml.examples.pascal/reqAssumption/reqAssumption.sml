import "../model.ecore"

specification test_scenario {
	domain model
	
	controllable {
		A
		B
	}

	non-spontaneous events {
		A.opA2		
	}
	
	collaboration test_scenario{
		
		static role A a 
		static role B b 
		static role Environment env
		
 		assumption scenario Environment {
			env -> a.opA1()
			while [true] {
				strict eventually env -> a.opA2()
			}
		}
		
		guarantee scenario req {
			env -> a.opA1()
			while [true] {
				strict monitored eventually env -> a.opA2()
				strict monitored eventually a -> b.opB1()
			}
		}
		
		guarantee scenario start {
			env -> a.opA1()
			while [true] {
				strict env -> a.opA2()
				strict urgent a -> b.opB1()
			}
		}
	}
}