import "../model/CrosswalkSystem.ecore"

specification CrosswalkSystemSpecification {
	domain CrosswalkSystem

	controllable {
		Car 
		/*
		 * Only cars are controllable (software) components.
		 * The pedestrians (class 'Pedestrian') and the 
		 * physical environment (class 'Environment') are 
		 * uncontrollable (environment) objects.
		 */
	}

	/*
	 * Here we list the environment events that cannot
	 * occur spontaneously:
	 * Spontaneous are car and pedestrian movements.
	 * 'pedestrian detected' for example is an environment event 
	 * that is not spontaneous, since it only occurs as a direct 
	 * consequence of a pedestrian moving somewhere where it will
	 * be detected by a car that is close by.
	 */
	non-spontaneous events {
		Car.enteredActiveCWRegion
		Car.pedestrianDetected
		Car.setCarLocation
		Car.setReachedPointToStopBeforeCrosswalk
		Pedestrian.setPedestrianLocation
		Car.exitsCrosswalk
	}

	/*
	 * We need some general assumptions on car and pedestrian movements.
	 * 1. When a move event occurs for a car (modeled as a message from the
	 *    physical environment represented by the role 'env'), then the
	 *    car's location also changes to the next car location (road segment).
	 *    This is again modeled as a message from the physical environment
	 *    'env' to the car. The 'committed' modality means that this event
	 *    will occur in direct consequence to the move event.
	 *    (Remember that setCarLocation is a non-spontaneous event: this
	 *    is how we explicitly specify when such events occur.)
	 * 2. The same pattern is applied for pedestrian movements.
	 */
	collaboration CarsAndPedestriansMove {
		
		dynamic role Car car
        dynamic role Environment env
		dynamic role Pedestrian pedestrian
		
		assumption scenario CarMoves{
			env -> car.move 
			var CarLocation currentCarLoc = car.carLocation
			committed env -> car.setCarLocation(currentCarLoc.nextCarLocation)
		}
		
		assumption scenario PedestrianMoves{
			env -> pedestrian.move 
			var PedestrianLocation currentPedestrianLoc = pedestrian.pedestrianLocation
			committed env -> pedestrian.setPedestrianLocation(currentPedestrianLoc.nextPedestrianLocation)
		}
		
	}
	
	
	/*
	 * 1. Upon entering the active crosswalk region, the vehicle transmits 
	 *    a crosswalk recognition signal (e.g. through a designated display) 
	 *    and reduces its speed to a legally specified speed.
	 */ 
	collaboration CarEntersActiveCrosswalkRegion {

		dynamic role Car car
		dynamic role ActiveCrosswalkRegion carsCurrentActiveCrosswalkRegion
        dynamic role Environment env

		/*
		 * So far, we only defined that there can be car movements (that can lead
		 * to location changes, cf. above), but there is no event "enteredActiveCWRegion"
		 * yet. Therefore, we again apply the pattern of assuming that an additional 
		 * environment message occurs exactly when (using committed) the car moves onto
		 * a location that is an active crosswalk region.
		 */
		assumption scenario CarMovesOntoActiveCrosswalkRegion{
			env -> car.setCarLocation(*)
			alternative [car.isOnActiveCWRegion]{
				committed env -> car.enteredActiveCWRegion(car.currentActiveCrosswalkRegion)
			}
		}
		
		/*
		 * This guarantee scenario finally formalizes the first property.
		 * 
		 * The "urgent" modality means that the system should execute the message event
		 * when the scenario progresses to the urgent message.
		 * 
		 * The "strict" modality means that when the scenario progresses to a strict
		 * message, events that the scenario expects elsewhere must not happen 
		 * (they are blocked until the scenario progresses)
		 */
		guarantee scenario SendCrosswalkRecognitionSignal{
			env -> car.enteredActiveCWRegion(bind carsCurrentActiveCrosswalkRegion)
			strict urgent car -> car.setCrosswalkRecognitionSignal(true)
			strict urgent car -> car.setSpeed(Speed:reduced)
		}
		
	}

	/*
	 * 2. While entering the active crosswalk region, if a pedestrian 
	 *    is detected by the vehicle, either on the designated crossing request 
	 *    zone (at the entrance to the crosswalk) or on the crosswalk itself, a 
	 *    stopping commitment signal is transmitted by the vehicle to the pedestrian 
	 *    to indicate the commitment to stop. Concurrently, a stopping commitment 
	 *    signal is also displayed to other vehicles in the active crosswalk region.
	 *    The vehicle reduces speed and comes to a full stop in front of the crosswalk.
	 * 
	 *    NOTE by J.G.: according to 1., the car is already at reduced speed in the
	 *                  active crosswalk region, so we omit setting the car
	 *                  to reduced speed, and only model the part of 
	 *                  "comes to a full stop in front of the crosswalk".
	 */ 
	collaboration CarDetectsPedestriansInActiveCrosswalkRegion {

		dynamic role Car car
		dynamic multi role Pedestrian detectedPedestrian
		dynamic role ActiveCrosswalkRegion carsCurrentActiveCrosswalkRegion
        static role Environment env

		/*
		 * So far, in the above scenarios, the only environment events considered are 
		 * the movements of cars and pedestrians, but we did not yet consider the environment
		 * events of "a car detects a pedestrian". Such events are, in fact, depend on the 
		 * movements of cars and pedestrians.
		 * 
		 * In the following assumption scenario we assume that a car that enters the active
		 * crosswalk region will detect all the pedestrians that are 
		 *  - on the crosswalk and
		 *  - on the crosswalk request zone.
		 * 
		 * On the semantics of MULTI ROLES:
		 * A multi role represents a set of object and a scenario with one multi roles
		 * must apply to all the objects that the role represents.
		 * The set of objects that a multi role represents is determined dynamically, 
		 * upon the activation of the scenario via evaluating a 
		 * binding expression (see "bindings[ ... ]" section). 
		 * In this scenario the 'detectedPedestrian' role is bound to all the
		 * pedestrians on the crosswalk or in the crosswalk request zone.
		 * (See in the domain class model that 'pedestriansAtCrosswalk' is a 
		 * derived attribute of class Car.)
		 * For a scenario with one multi role for which the binding expression evaluates
		 * to multiple objects multiple corresponding active scenarios are created,
		 * so that here they require that for every pedestrian at the crosswalk,
		 * the car entering the active crosswalk region receives a pedestrian
		 * detected event.
		 * 
		 * We need the "ignore" message because otherwise the pedestrianDetected
		 * events would lead to the mutual interruption of multiple active copies
		 * of this scenario.
		 */		
		assumption scenario CarEnteringActiveCrosswalkRegionDetectsPedestrians
		bindings[
			detectedPedestrian = carsCurrentActiveCrosswalkRegion.pedestriansAtCrosswalk
		]{
			env -> car.enteredActiveCWRegion(bind carsCurrentActiveCrosswalkRegion)
			interrupt[detectedPedestrian == null]
			committed env -> car.pedestrianDetected(detectedPedestrian)
		}constraints[
			ignore env -> car.pedestrianDetected(*)
		]
		
		
		/*
		 * When a car detects a pedestrian and the car is in the active crosswalk
		 * region, then the car must set its stop commitment signal to true.
		 * 
		 * If the car has already reached the point to stop before the crosswalk
		 * then the car also comes to a stop.
		 */
		guarantee scenario OnPedestrianDetectionSetStopCommitmentSignalTrue{
			env -> car.pedestrianDetected(bind detectedPedestrian) 
			interrupt [!car.isOnActiveCWRegion] 
			strict urgent car -> car.setStopCommittmentSignal(true)
			interrupt [!car.reachedPointToStopBeforeCrosswalk]
			strict urgent car -> car.setSpeed(Speed:stop)
		}

		/*
		 * When the car reaches the point to stop before the crosswalk,
		 * and it's stop commitment is true, it must stop.
		 * 
		 * We introduce and EXTRA ASSERTION, using the "violation" operation:
		 * it must be that when the car reaches the point to stop before the crosswalk,
		 * and there are pedestrians on the crosswalk, then the stop commitment
		 * signal must be on.
		 */
		guarantee scenario StopWhenStopCommitmentSignalTrueAndReachedPointToStop{
			env -> car.setReachedPointToStopBeforeCrosswalk(true)
			
			var EBoolean pedestriansAtCrosswalkImpliesCarShowsStopCommittment
				= car.stopCommittmentSignal || car.pedestriansAtCrosswalk.isEmpty()				
			violation[!pedestriansAtCrosswalkImpliesCarShowsStopCommittment]
			
			alternative [car.stopCommittmentSignal]{
				strict urgent car -> car.setSpeed(Speed:stop)
			}
		}
		
		/*
		 * We again need an assumption in order to specify when the 
		 * "reachedPointToStopBeforeCrosswalk" must occur.
		 */
		assumption scenario ReachesPointToStopBeforeCrossingBeforeNextMove{
			env -> car.enteredActiveCWRegion(*)
			eventually env -> car.setReachedPointToStopBeforeCrosswalk(true)
		}constraints[
			forbidden env -> car.move()
		]
		
		/*
		 * And we need an assumption that a stopped car will not move.
		 */
		assumption scenario CarDoesNotMoveWhenStopped{
			car -> car.setSpeed(Speed:stop)
			car -> car.setSpeed(*)
		}constraints[
			forbidden env -> car.move()
		]
			
	}


	/*
	 * 3. All vehicles in the active crosswalk region that observe a pedestrian 
	 *    or a stopping commitment signal transmitted by another vehicle transmit 
	 *    a stopping commitment signal as well and reduce their speed to a full 
	 *    stop ahead of the crosswalk.
	 * 
	 * Particular modeling issues here:
	 * 
	 *   1. OnPedestrianDetectionSetStopCommitmentSignalAndComeToStop already covers 
	 *      the issue of detecting a pedestrian, so this is taken care of already.
	 *      What is left to model is an assumptions on pedestrians and their 
	 *      detection, namely, that pedestrians who enter the crosswalk request zone
	 *      will be detected by all cars in the active crosswalk region.
	 * 
	 *   2. Because all cars in the active crosswalk region always become equally aware
	 *      of all pedestrians, there is no need to react other stopping commitment signals.
	 *      We could add detail to the model, for example that there is a the active 
	 *      crosswalk region has a part close to the crosswalk where cars will spot the
	 *      pedestrians, and a part that is further away and where the cars don't detect
	 *      the pedestrians, but must react to the stop commitment by the other cars.
	 */
	collaboration CarsObservingAPedestrianSendStoppingCommittmentSignalAndStop{
		
		
		dynamic multi role Car carInActiveCrosswalkRegion
		dynamic role Pedestrian pedestrian
		static role Environment env
		
		assumption scenario PedestrianEnteringCrosswalkRequestZoneWillBeDetectedByCars
		bindings[
			carInActiveCrosswalkRegion = pedestrian.carsInActiveCrosswalkRegion
		]{
			env -> pedestrian.setPedestrianLocation(*)
			interrupt [carInActiveCrosswalkRegion == null]
			alternative [pedestrian.isOnCrosswalkRequestZone] {
				committed env -> carInActiveCrosswalkRegion.pedestrianDetected(pedestrian)
			}
		}constraints[
			ignore env -> carInActiveCrosswalkRegion.pedestrianDetected(*)
		]
		
	}
	
	/*
	 * 4. A pedestrian standing in the crossing request zone is obligated to cross, provided 
	 *    that: (1) no vehicle is present in the active crosswalk region, or (2) all 
	 *    vehicles in the active crosswalk region have either stopped in front of the 
	 *    crosswalk or display the stopping commitment signal.
	 * 
	 *    We actually have to model two aspects here:
	 *    First, we have to assume that pedestrians in the crossing request zone
	 *    will move on if they can. Second, we also have to assume that pedestrians
	 *    who are on the crosswalk will move onto the other side.
	 */
	collaboration PedestrianObligatedToCross{

		dynamic role Pedestrian pedestrian
		static role Environment env
		
		/*
		 * We model that a pedestrian in the crosswalk request zone
		 * will step on the crosswalk AS SOON AS the pedstrian can.
		 * 
		 * Relaxing this assumption so that the pedestrian must EVENTUALLY
		 * move would make the model more complicated, because we also
		 * have to assume that meanwhile again cars may arrive that
		 * should then revoke the pedestians obligation to move onto
		 * the crosswalk.
		 */
		assumption scenario PedestrianEnteringCrosswalkRequestZoneObligatedToCross{
			env -> pedestrian.setPedestrianLocation(*)
			interrupt[!pedestrian.isOnCrosswalkRequestZone]
			wait strict [pedestrian.allCarsInActiveCrosswalkRegionSignalStopCommittment]
			committed env -> pedestrian.move
		}


		/*
		 * When a pedestrian enters the crosswalk, the pedestrian
		 * eventually also exits it.
		 */
		assumption scenario PedestrianOnCrossingIsObligatedToCross{
			env -> pedestrian.setPedestrianLocation(*)
			interrupt[!pedestrian.isOnCrosswalk]
			strict eventually env -> pedestrian.move
		}
		
	}
	
	/*
	 * 5. Vehicles resume their motion only after all pedestrians have completely crossed.
	 */
	collaboration VehicledResumeMotionConstraint {
		
		dynamic role Car car
		dynamic role Environment env
		
		/*
		 * When a car reaches the point stop before the crosswalk and stops, then
		 * it must not set its speed to cruising or reduced speed before all
		 * pedestrians have cleared the crosswalk.
		 */
		guarantee scenario VehicledResumeMotionOnlyAfterAllPedestriansCrossed {
			car -> car.setSpeed(Speed:stop)
			interrupt[!car.reachedPointToStopBeforeCrosswalk]
			wait [car.pedestriansAtCrosswalk.isEmpty()]			
		}constraints[
			forbidden car -> car.setSpeed(Speed:reduced)
			forbidden car -> car.setSpeed(Speed:cruising)
		]
		
	}
	
	/*
	 * 6. If at the specified closer distance from the crosswalk still no pedestrian is 
	 * recognized and no stopping commitment signal from other vehicles is observed, 
	 * the vehicle may continue its motion at the specified reduced speed through the 
	 * crosswalk zone at which point it may resume cruising speed. 
	 */
	collaboration VehicledResumeMotion {
		
		dynamic role Car car
		static role Environment env
		dynamic multi role Pedestrian pedestrianOnSidewalk
		
		/*
		 * When a car enters the crosswalk and then clears the crosswalk,
		 * the environment event "existsCrosswalk" is detected by the car.
		 */
		assumption scenario CarLeavesCrossing {
			env -> car.setCarLocation(*)
			interrupt[!car.isOnCrosswalk]
			wait [car.isClearOfCrosswalk]
			committed env -> car.exitsCrosswalk()
		}constraints[
			ignore env -> car.setCarLocation(*)
		]
		
		/*
		 * When a car exists the crosswalk, it resumes with cruising or reduced speed
		 */
		guarantee scenario VehicledResumesCruisingOrReducedSpeedOnExitOfCrosswalk {
			env -> car.exitsCrosswalk
			alternative {
				urgent car -> car.setSpeed(Speed:cruising) // car may return to cruising speed
			} or {
				urgent car -> car.setSpeed(Speed:reduced) // or stay at reduced speed
			}
		}


		/*
		 * When a car stops before the crosswalk, it resumes cruising speed 
		 * as soon as the crosswalk is free of pedestrians.
		 */
		guarantee scenario VehicledResumeMotionAfterAllPedestriansCrossed {
			car -> car.setSpeed(Speed:stop)
			interrupt [!car.reachedPointToStopBeforeCrosswalk]
			wait eventually [car.pedestriansAtCrosswalk.isEmpty()]
			urgent car -> car.setStopCommittmentSignal(false)
			committed car -> car.setSpeed(Speed:reduced)
		}
		
		/*
		 * This assumption says that we assume that when 
		 * (1) a car is at the point to stop before the crosswalk, 
		 * (2) and there is no pedestrian on the crosswalk or the crosswalk 
		 * request zone, and the car resumes driving with reduced speed,
		 * then the car will move to the next steet section, i.e., the crosswalk
		 * before any other environment event happens.
		 * (like a pedestrian stepping on the crosswalk request zone).
		 * 
		 * This shall enable cars to move onto the crosswalk eventually and
		 * not be stopped by a pedestrian that again moves into the
		 * crosswalk request zone before the car can advance.    
		 */
		assumption scenario CarResumingMotionWillBeFasterToEnterCrosswalkThanAnyPedestrians{
			car -> car.setSpeed(Speed:reduced)
			interrupt[!car.reachedPointToStopBeforeCrosswalk]
			interrupt[!car.pedestriansAtCrosswalk.isEmpty()]
			committed env -> car.move()
		}
		
		guarantee scenario ResetCrosswalkRecognitionSignal{
			env -> car.exitsCrosswalk()
			urgent car -> car.setCrosswalkRecognitionSignal(false)
		}

		/*
		 * If the flag reachedPointToStopBeforeCrosswalk is set to true,
		 * set it to false as soon as the car has moved.
		 */
		guarantee scenario ResetReachedPointToStopBeforeCrosswalk{
			env -> car.move()
			interrupt [!car.reachedPointToStopBeforeCrosswalk]
			urgent car -> car.setReachedPointToStopBeforeCrosswalk(false)
		}
				
	}
	

}
