import "../model/cockpitdoorlockingsystem.ecore"
import "CockpitDoorLockingSystemExistential.collaboration"

specification CockpitDoorLockingSystem {

	domain cockpitdoorlockingsystem

	controllable {
		Controller
	}
	
	non-spontaneous events {
		Controller.toggleInNormal // toggle switch can only move into normal after previously in lock or unlock position, see assumptions
		Controller.doorClosed
	}
	include collaboration CockpitDoorBehaviourReference

	collaboration CockpitDoorNormalLockedBehavior {

		static role CodePad codepad
		static role Buzzer buzzer
		static role Controller controller
		static role Timer timer
		static role ToggleSwitch toggleSwitch
		static role Door door

		/*
		 * When the access mode is set to Normal Locked Mode, 
		 * - turn off buzzer
		 */
		guarantee scenario EnteringNormalLockedModeTurnsOffBuzzer{
			controller -> controller.setAccessMode(AccessMode:LOCKED)
			strict urgent controller -> buzzer.soundBuzzer(false)
		} 
		
		/*
		 * When the access mode is set to Normal Locked Mode, 
		 * - turn off buzzer
		 */
		guarantee scenario EnteringNormalLockedModeEngagesLatches{
			controller -> controller.setAccessMode(AccessMode:LOCKED)
			strict urgent controller -> door.setLatchesEngaged(true)
		}
		
		/*
		 * When the access mode is set to Normal Locked Mode, 
		 * - turn off buzzer
		 */
		guarantee scenario EnteringNormalLockedModeTurnsOffGreenCockpitLight{
			controller -> controller.setAccessMode(AccessMode:LOCKED)
			strict urgent controller -> door.setLatchesEngaged(true)
		}
		
	}


	collaboration CockpitDoorNormalEntryBehavior {

		static role CodePad codepad
		static role Buzzer buzzer
		static role Controller controller
		static role Timer timer
		static role ToggleSwitch toggleSwitch
		static role Door door
		
		/*
		 * If the hash key is pressed,
		 * and the access mode is normal locked,
		 * - set access mode to Normal Access urgent
		 * - sound the buzzer (in the cockpit)
		 * - start a timer (3 sec.)
		 */
		guarantee scenario NormalAccessRequest {
			codepad -> controller.hashKeyPressed()
			interrupt [ controller.accessMode != AccessMode:LOCKED]
			strict urgent controller -> controller.setAccessMode(AccessMode:ACCESSREQUESTED)
			strict urgent controller -> timer.restart(3)
		}

		/*
		 * When entering normal access urgent mode 
		 * - sound the buzzer
		 */
		guarantee scenario EnteringNormalACCESSREQUESTEDSoundsBuzzer {
			controller -> controller.setAccessMode(AccessMode:ACCESSREQUESTED)
			strict urgent controller -> buzzer.soundBuzzer(true)
		}

		/*
		 * When the timer expires in normal access urgent mode 
		 * - set access mode to Normal Locked
		 */
		guarantee scenario StopNormalAccessRequestAfterTimeout {
			timer -> controller.timerExpired()
			interrupt [ controller.accessMode != AccessMode:ACCESSREQUESTED]
			strict urgent controller -> controller.setAccessMode(AccessMode:LOCKED)
		}
		
	}

	collaboration CockpitDoorLockedDownBehavior {

		static role Buzzer buzzer
		static role Controller controller
		static role ToggleSwitch toggleSwitch
		static role Timer timer
		static role Door door
		static role Light greenLightCP
		static role Light redLightCP
		static role Light openLight
		
		/*
		 * When the toggle switch is moved into the lock position
		 * - set cockpit locked mode to TRUE
		 * - start a timer (5 min = 300 sec)
		 */
		guarantee scenario CockpitCrewLocksDoor {
			toggleSwitch -> controller.toggleInLock()
			strict urgent controller -> controller.setAccessMode(AccessMode:LOCKDOWN)
			strict urgent controller -> timer.restart(300) // = 5 minutes
		}
		
		/*
		 * When the timer expires in LockDown mode 
		 * - set access mode to Normal Locked
		 */
		guarantee scenario DisableInhibitedStateAfterTimeout {
			timer -> controller.timerExpired()
			interrupt [ controller.accessMode != AccessMode:LOCKDOWN ]
			strict urgent controller -> controller.setAccessMode(AccessMode:LOCKED)
		}
		
		/*
		 * When the access mode is set to lock down, 
		 * - turn off buzzer
		 */
		guarantee scenario EnteringLockDownStopsBuzzer {
			controller -> controller.setAccessMode(AccessMode:LOCKDOWN)
			strict urgent controller -> buzzer.soundBuzzer(false)
		}

		/*
		 * When the access mode is set to lock down, 
		 * - engage latches
		 */
		guarantee scenario EnteringLockDownEngagesLatches {
			controller -> controller.setAccessMode(AccessMode:LOCKDOWN)
			strict urgent controller -> door.setLatchesEngaged(true)
		}

		/*
		 * When the access mode is set to lock down, 
		 * - turn off open light
		 */
		guarantee scenario EnteringLockDownTurnsOffOpenLight {
			controller -> controller.setAccessMode(AccessMode:LOCKDOWN)
			strict urgent controller -> openLight.setState(LightState:OFF)
		}

		/*
		 * When the access mode is set to lock down, 
		 * - turn off green cockpit light
		 */
		guarantee scenario EnteringLockDownTurnsOffGreenCockpitLight {
			controller -> controller.setAccessMode(AccessMode:LOCKDOWN)
			strict urgent controller -> greenLightCP.setState(LightState:OFF)
		}

		/*
		 * When the access mode is set to lock down, 
		 * - turn on red cockpit light
		 */
		guarantee scenario EnteringLockDownTurnsOnRedCockpitLight {
			controller -> controller.setAccessMode(AccessMode:LOCKDOWN)
			strict urgent controller -> redLightCP.setState(LightState:ON)
		}
		
		/*
		 * When the access mode is set to anything else than lock down, 
		 * - turn off red cockpit light
		 */
		guarantee scenario LeavingLockDownTurnsOffRedCockpitLight {
			var AccessMode am
			controller -> controller.setAccessMode(bind am)
			interrupt [am == AccessMode:LOCKDOWN]
			strict urgent controller -> redLightCP.setState(LightState:OFF)
		}
	}

	collaboration CockpitDoorEmergencyEntryBehavior {

		static role CodePad codepad
		static role Buzzer buzzer
		static role Controller controller
		static role ToggleSwitch toggleSwitch
		static role Door door
		static role Timer timer
		static role Light greenLightCP
		static role Light redLightCP
		static role Light openLight

		/*
		 * When the purser enters the emergency code
		 * in the normal lock mode 
		 * (and presses the hash key -> skipped for simplicity) 
		 */
		guarantee scenario EmergencyAccessRequest{
			codepad -> controller.emergencyCodeEntered()
			interrupt [ controller.accessMode != AccessMode:LOCKED ]
			strict urgent controller -> controller.setAccessMode(AccessMode:EMERGENCYACCESSREQUESTED)
			strict urgent controller -> timer.restart(30)
		} 

		/*
		 * When the access mode is set to emergency access urgent, 
		 * - set green cockpit light to blinking
		 */
		guarantee scenario EnteringEmergencyAccessRequestModeBlinkGreenCockpitLight{
			controller -> controller.setAccessMode(AccessMode:EMERGENCYACCESSREQUESTED)
			strict urgent controller -> greenLightCP.setState(LightState : BLINKING)
		} 

		/*
		 * When the access mode is set to emergency access urgent, 
		 * - set open light to blinking
		 */
		guarantee scenario EnteringEmergencyAccessRequestModeBlinkOpenLight{
			controller -> controller.setAccessMode(AccessMode:EMERGENCYACCESSREQUESTED)
			strict urgent controller -> openLight.setState(LightState : BLINKING)
		} 

		/*
		 * When the access mode is set to emergency access urgent, 
		 * - sound buzzer
		 */
		guarantee scenario EnteringEmergencyAccessRequestModeSoundBuzzer{
			controller -> controller.setAccessMode(AccessMode:EMERGENCYACCESSREQUESTED)
			strict urgent controller -> buzzer.soundBuzzer(true)
		} 

		
//		/*
//		 * Repeated pressing of hash key leads to aborting the emergency access request.
//		 */
//		guarantee scenario EmergencyAccessRequestAbort{
//			codepad -> controller.hashKeyPressed()
//			interrupt if [ !controller.EMERGENCYACCESSREQUESTED ]
//			strict urgent controller -> controller.setEMERGENCYACCESSREQUESTED(false)
//			strict urgent controller -> greenLightCP.setState(LightState:OFF)
//			strict urgent controller -> openLight.setState(LightState:OFF)
//			strict urgent controller -> buzzer.soundBuzzer(false)
//			strict urgent controller -> timer.stop()
//		} 

		/*
		 * Toggling toggleInLock 
		 * - stop timer
		 */
		guarantee scenario EmergencyAccessRequestAbortByLockDown{
			toggleSwitch -> controller.toggleInLock() 
			strict urgent controller -> timer.stop()
		}
		

		/*
		 * Automatically granting emergency access after timer expired.
		 * - turn off buzzer
		 * - disengage latches
		 * - turn on green light in cockpit
		 * - turn on open light
		 * - start timer for 5 seconds
		 */
		guarantee scenario GrantEmergencyAccess{
			timer -> controller.timerExpired()
			interrupt [ controller.accessMode == AccessMode:EMERGENCYACCESSREQUESTED ]
			strict urgent controller -> controller.setAccessMode(AccessMode:EMERGENCYUNLOCKED)
			strict urgent controller -> timer.restart(5)
		}
		
		/*
		 * When the access mode is set to emergency unlocked, 
		 * - turn off buzzer
		 */
		guarantee scenario EnteringEmergencyUnlockModeTurnOffBuzzer{
			controller -> controller.setAccessMode(AccessMode:EMERGENCYUNLOCKED)
			strict urgent controller -> buzzer.soundBuzzer(false)
		} 

		/*
		 * When the access mode is set to emergency unlocked, 
		 * - disengage latches
		 */
		guarantee scenario EnteringEmergencyUnlockModeDisengageLatches{
			controller -> controller.setAccessMode(AccessMode:EMERGENCYUNLOCKED)
			strict urgent controller -> door.setLatchesEngaged(false)
		} 

		/*
		 * When the access mode is set to emergency unlocked, 
		 * - turn on open light
		 */
		guarantee scenario EnteringEmergencyUnlockModeTurnOnOpenLight{
			controller -> controller.setAccessMode(AccessMode:EMERGENCYUNLOCKED)
			strict urgent controller -> openLight.setState(LightState : ON)
		} 

		/*
		 * When the access mode is set to emergency unlocked, 
		 * - turn on green cockpit light
		 */
		guarantee scenario EnteringEmergencyUnlockModeTurnOnGreenCockpitLight{
			controller -> controller.setAccessMode(AccessMode:EMERGENCYUNLOCKED)
			strict urgent controller -> greenLightCP.setState(LightState : ON)
		} 
		
	}


	collaboration TimerAssumption {

		static role Controller controller
		static role Timer timer
		
		assumption scenario TimerExpires {
			controller -> timer.restart(*)
			eventually timer -> controller.timerExpired()
		}constraints[
			interrupt controller -> timer.stop()
		]
		
//		guarantee scenario NotTwoTimerStarted {
//			controller -> timer.restart(*)
//			controller -> timer.restart(*)
//			violation if [true]
//		}constraints[
//			interrupt timer -> controller.timerExpired()
//			interrupt controller -> timer.stop()
//		]
	}


	collaboration LockControl {

		static role Controller controller
		static role ToggleSwitch toggleSwitch
		static role Door door

		/*
		 * After toggling the switch into the lock or unlock
		 * position, the switch will eventually toggle back into
		 * the normal position without lock or unlock occurring again. 
		 */
		assumption scenario ToggleSwitchReturnsToNormal {
			alternative {
				toggleSwitch -> controller.toggleInLock()
			} or {
				toggleSwitch -> controller.toggleInUnlock()
			}
			strict eventually toggleSwitch -> controller.toggleInNormal()
		}

		guarantee scenario UnlockDoor {
			toggleSwitch -> controller.toggleInUnlock()
			strict urgent controller -> door.setLatchesEngaged(false)
			controller -> controller.setAccessMode(AccessMode:LOCKED)
		}

		/*
		 * After toggling the switch into the normal position, the latches will
		 * be engaged.
		 */
		guarantee scenario LockDoor {
			toggleSwitch -> controller.toggleInNormal()
			strict urgent controller -> door.setLatchesEngaged(true)
		}
		
		/*
		 * No door opening is possible when the latches are engaged 
		 */
		assumption scenario NoDoorOpeningWhenLatchesEngaged{
			door -> controller.doorOpened()
			violation [door.latchesEngaged]
		}
		
		/*
		 * After opening the door, the door cannot be opened again
		 * unless closed, and vice versa.
		 * (Initially, the door can be opened or closed,
		 * we don't define the start configuration.)
		 */
		assumption scenario DoorOpeningAndClosingAlternate {
			alternative{
				door -> controller.doorOpened()
				strict door -> controller.doorClosed()
			}or{
				door -> controller.doorClosed()
				strict door -> controller.doorOpened()
			}
		}

	}

	collaboration LightControl {

		static role Controller controller
		static role Door door
		static role Light greenLightCP
		static role Light redLightCP
		static role Light openLight

		guarantee scenario ChangeLightStatusOnDoorUnlocking {
			controller -> door.setLatchesEngaged(false)
			strict urgent controller -> greenLightCP.setState(LightState : ON)
		}

		guarantee scenario ChangeLightStatusOnDoorLocking {
			controller -> door.setLatchesEngaged(true)
			strict urgent controller -> greenLightCP.setState(LightState : OFF)
		}

		guarantee scenario ChangeOpenLightStatusOnDoorOpening {
			door -> controller.doorOpened()
			strict urgent controller -> openLight.setState(LightState : ON)
		}

		guarantee scenario ChangeOpenLightStatusOnDoorClosing {
			door -> controller.doorClosed()
			strict urgent controller -> openLight.setState(LightState : OFF)
		}

		guarantee scenario ChangeRedLightStatusOnInhibited {
			controller -> controller.setAccessMode(AccessMode:LOCKDOWN)
			strict urgent controller -> redLightCP.setState(LightState : ON)
		}

		guarantee scenario ChangeRedLightStatusOnUninhibited {
			controller -> controller.setAccessMode(AccessMode:LOCKED)
			strict urgent controller -> redLightCP.setState(LightState : OFF)
		}

	}

}