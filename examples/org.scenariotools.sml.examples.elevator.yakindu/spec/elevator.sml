import "../model/elevator.ecore"

specification ElevatorSpecification {

	domain elevator

	controllable{
		ElevatorController
	}
	
	non-spontaneous events {
		ElevatorController.movedDown
		ElevatorController.movedUp
		ElevatorController.revokeEmergencyStop
	}

	parameter ranges {
		ElevatorController.requestFloor(floorNumber = [ 0 .. 2 ])
	}

	collaboration ElevatorCollaboration {

		static role ElevatorController ctrl
		static role Environment env

		singular guarantee scenario SetElevatorMovingOnRequestIfNotStillOtherRequestActive {
			var EInt floorNumber 
			env -> ctrl.requestFloor(bind floorNumber)
			interrupt[env.currentFloor != ctrl.requestedFloor] // do not process new request if previous request is not yet fulfilled
			strict urgent ctrl -> ctrl.setRequestedFloor(floorNumber)
			interrupt [ctrl.emergencyStop]
			alternative [env.currentFloor > floorNumber]{
				strict eventually ctrl->ctrl.setMovingUp(false)
				strict urgent ctrl->ctrl.setMovingDown(true)
			} or [env.currentFloor < floorNumber] {
				strict eventually ctrl->ctrl.setMovingDown(false)
				strict urgent ctrl->ctrl.setMovingUp(true)
			} or [env.currentFloor == floorNumber] {
				strict eventually ctrl->ctrl.setMovingUp(false)
				strict urgent ctrl->ctrl.setMovingDown(false)
			}
		}constraints[
			ignore env -> ctrl.requestFloor(*) // otherwise violation of strict eventually messages would happen
			interrupt env->ctrl.requestEmergencyStop()
		]

		guarantee scenario SendSignalRequestRefusalOnRequestWhileRequestActive {
			var EInt floorNumber 
			env -> ctrl.requestFloor(bind floorNumber)
			interrupt[env.currentFloor == ctrl.requestedFloor]
			urgent ctrl->env.signalRequestRefused
		}

		
		assumption scenario FloorCounterIncrease {
			env->ctrl.movedUp()
			alternative [env.currentFloor < 2]{
				committed env->env.setCurrentFloor(env.currentFloor + 1)
			}
		}

		assumption scenario FloorCounterDecrease {
			env->ctrl.movedDown()
			alternative [env.currentFloor > 0]{
				committed env->env.setCurrentFloor(env.currentFloor - 1)
			}
		}

		guarantee scenario StopElevatorWhenMovedToRequestedFloor {
			env->env.setCurrentFloor(*)
			alternative [env.currentFloor == ctrl.requestedFloor]{
				urgent ctrl->ctrl.setMovingUp(false)
				urgent ctrl->ctrl.setMovingDown(false)
			}
		}
		
		assumption scenario MoveUpWhenSetToMovingUp{
			ctrl->ctrl.setMovingUp(true)
			while [ctrl.movingUp]{
				eventually env->ctrl.movedUp()
			}constraints[
				interrupt ctrl->ctrl.setMovingUp(false)
			]
		}

		assumption scenario MoveDownWhenSetToMovingDown{
			ctrl->ctrl.setMovingDown(true)
			while [ctrl.movingDown]{
				eventually env->ctrl.movedDown()
			}constraints[
				interrupt ctrl->ctrl.setMovingDown(false)
			]
		}
		
		guarantee scenario SetEmergencyStop {
			env->ctrl.requestEmergencyStop
			strict urgent ctrl->ctrl.setEmergencyStop(true)
		}

		guarantee scenario StopMovingOnEmergencyStop {
			env->ctrl.requestEmergencyStop
			strict urgent ctrl->ctrl.setMovingUp(false)
			strict urgent ctrl->ctrl.setMovingDown(false)
		}

		guarantee scenario SetEmergencyStopRevoked {
			env->ctrl.revokeEmergencyStop
			strict urgent ctrl->ctrl.setEmergencyStop(false)
		}

		assumption scenario RevokeEmergencyStopAfterRequest{
			env->ctrl.requestEmergencyStop
			strict env->ctrl.revokeEmergencyStop
		}

		guarantee scenario ResumeMovementWhenEmergencyStopRevoked {
			ctrl->ctrl.setEmergencyStop(false)
			var EInt floorNumber = ctrl.requestedFloor 
			alternative [env.currentFloor > floorNumber]{
				strict urgent ctrl->ctrl.setMovingUp(false)
				strict urgent ctrl->ctrl.setMovingDown(true)
			} or [env.currentFloor < floorNumber] {
				strict urgent ctrl->ctrl.setMovingDown(false)
				strict urgent ctrl->ctrl.setMovingUp(true)
			} or [env.currentFloor == floorNumber] {
				strict urgent ctrl->ctrl.setMovingUp(false)
				strict urgent ctrl->ctrl.setMovingDown(false)
			}
		}constraints[
			interrupt env->ctrl.requestEmergencyStop()
		]
		

	}

}
