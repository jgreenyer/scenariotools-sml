package org.scenariotools.sml.testing.views;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;
import org.scenariotools.sml.InteractionFragment;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.ScenarioKind;
import org.scenariotools.sml.runtime.ActiveModalMessage;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.Transition;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.wb.swt.SWTResourceManager;

public class SMLTestResultsView extends ViewPart {

	public static final String ID = "org.scenariotools.sml.testing.views.SMLTestResultsView"; //$NON-NLS-1$
	private Tree tree;
	private Label numTests;
	private Label numFailedTests;
	private ProgressBar progressBar;

	public SMLTestResultsView() {
	}

	/**
	 * Create contents of the view part.
	 * @param parent
	 */
	@Override
	public void createPartControl(Composite parent) {
		Composite container = new Composite(parent, SWT.V_SCROLL);
		container.setLayout(new GridLayout(1, false));
		
		Composite composite = new Composite(container, SWT.NONE);
		composite.setLayout(new GridLayout(5, false));
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		composite.setBounds(0, 0, 32, 32);
		
		Label lblNewLabel = new Label(composite, SWT.NONE);
		lblNewLabel.setText("No. of tests:");
		
		numTests = new Label(composite, SWT.NONE);
		GridData gd_numTests = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_numTests.widthHint = 40;
		numTests.setLayoutData(gd_numTests);
		numTests.setText("0");
		
		Label lblNewLabel_2 = new Label(composite, SWT.NONE);
		lblNewLabel_2.setText("No. of failed tests:");
		
		numFailedTests = new Label(composite, SWT.NONE);
		GridData gd_numFailedTests = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_numFailedTests.widthHint = 40;
		numFailedTests.setLayoutData(gd_numFailedTests);
		numFailedTests.setText("0");
		
		progressBar = new ProgressBar(composite, SWT.NONE);
		progressBar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Composite composite_1 = new Composite(container, SWT.NONE);
		composite_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite_1.setBounds(0, 0, 32, 32);
		composite_1.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		tree = new Tree(composite_1, SWT.BORDER);

		createActions();
		initializeToolBar();
		initializeMenu();
	}

	/**
	 * Create the actions.
	 */
	private void createActions() {
		// Create the actions
	}

	/**
	 * Initialize the toolbar.
	 */
	private void initializeToolBar() {
		IToolBarManager toolbarManager = getViewSite().getActionBars().getToolBarManager();
	}

	/**
	 * Initialize the menu.
	 */
	private void initializeMenu() {
		IMenuManager menuManager = getViewSite().getActionBars().getMenuManager();
	}

	@Override
	public void setFocus() {
		// Set the focus
	}
	
	public void runTests(Configuration configuration) {
		tree.removeAll();

		SMLRuntimeStateGraph stateGraph = RuntimeFactory.eINSTANCE.createSMLRuntimeStateGraph();
		stateGraph.init(configuration);

		List<ActiveScenario> activeScenarios = getActiveExistentialScenarios(stateGraph);
		numTests.setText("" + activeScenarios.size());
		
		int failedTests = 0;
		for (ActiveScenario activeScenario : activeScenarios) {
			TreeItem newItem = new TreeItem(tree, 0);
			
			String text = activeScenario.getScenario().getName() + " [";
			for(Iterator<Entry<Role, EObject>> iterator = activeScenario.getRoleBindings().getRoleBindings().iterator(); iterator.hasNext();) {
				Entry<Role, EObject> entry = iterator.next();
				text += entry.getKey().getName() + "=" + getEObjectName(entry.getValue());
				if(iterator.hasNext())
					text += ", ";
				else
					text += "]";
			}

			newItem.setText(text);
			
			boolean testRanSuccessfully = runTest(stateGraph, activeScenario, newItem);
			if(!testRanSuccessfully) {
				failedTests += 1;
				newItem.setForeground(SWTResourceManager.getColor(SWT.COLOR_DARK_RED));		
			}
		}

		numFailedTests.setText("" + failedTests);
	
		progressBar.setMinimum(0);
		progressBar.setSelection(0);
		progressBar.setMaximum(activeScenarios.size());
		progressBar.setSelection(activeScenarios.size() - failedTests);
	}
	
	private static List<ActiveScenario> getActiveExistentialScenarios(SMLRuntimeStateGraph stateGraph) {
		List<ActiveScenario> result = new LinkedList<ActiveScenario>();
		
		SMLRuntimeState startState = stateGraph.getStartState();
		stateGraph.generateAllSuccessors(startState);
		
		for(Transition transition : startState.getOutgoingTransition()) {
			for(ActiveScenario activeScenario : transition.getTargetState().getActiveScenarios()) {
				if(activeScenario.getScenario().getKind() == ScenarioKind.EXISTENTIAL) {
					result.add(activeScenario);
				}				
			}
		}
		
		return result;
	}

	private static boolean runTest(SMLRuntimeStateGraph stateGraph, ActiveScenario activeScenario, TreeItem parent) {
		SMLRuntimeState state = findInitialState(stateGraph, activeScenario, parent);
		
		outer:
		while(activeScenario != null) {
			ActiveModalMessage activeModalMessage = (ActiveModalMessage)activeScenario.getMainActiveInteraction().getEnabledNestedActiveInteractions().get(0);
			MessageEvent enabledEvent = activeModalMessage.getEnabledEvents().get(0);
			
			stateGraph.generateAllSuccessors(state);
			for(Transition transition : state.getOutgoingTransition()) {
				if(transition.getEvent() instanceof MessageEvent) {
					MessageEvent event = (MessageEvent)transition.getEvent();
					if(event.isParameterUnifiableWith(enabledEvent)) {
						state = transition.getTargetState();
						activeScenario = findProgressedActiveScenario(state, activeScenario);
						
						TreeItem newItem = new TreeItem(parent, 0);
						newItem.setText(event.toString());
						
						continue outer;
					}
				}
			}
			
			TreeItem newItem = new TreeItem(parent, 0);
			newItem.setText("FAILURE AT: " + enabledEvent.toString());

			return false;
		}
		
		return true;
	}

	private static SMLRuntimeState findInitialState(SMLRuntimeStateGraph stateGraph, ActiveScenario activeScenario, TreeItem parent) {
		for(Transition transition : stateGraph.getStartState().getOutgoingTransition()) {
			SMLRuntimeState state = transition.getTargetState();
			if(state.getActiveScenarios().contains(activeScenario)) {
				TreeItem newItem = new TreeItem(parent, 0);
				newItem.setText(transition.getEvent().toString());
				return state;
			}
		}
		
		assert false; // this line should never be reached
		return null;
	}

	private static ActiveScenario findProgressedActiveScenario(SMLRuntimeState state, ActiveScenario activeScenario) {
		outer:
		for(ActiveScenario activeScenario2 : state.getActiveScenarios()) {
			if(activeScenario2.getScenario() == activeScenario.getScenario()) {
				for(Entry<Role, EObject> entry : activeScenario.getRoleBindings().getRoleBindings()) {
					EObject otherValue = activeScenario2.getRoleBindings().getRoleBindings().get(entry.getKey());
					if(!otherValue.equals(entry.getValue()))
						continue outer;
				}
				
				return activeScenario2;
			}
		}
		
		return null;
	}
	
	static String getEObjectName(EObject object) {
		EStructuralFeature eStructuralFeature = object.eClass().getEStructuralFeature("name");
		if(eStructuralFeature != null) {
			return (String)object.eGet(eStructuralFeature);
		} else {
			return object.toString();
		}
	}
} 
