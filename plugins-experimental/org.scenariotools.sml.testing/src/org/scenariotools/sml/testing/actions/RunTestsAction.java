package org.scenariotools.sml.testing.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.ObjectPluginAction;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.testing.views.SMLTestResultsView;

public class RunTestsAction implements IObjectActionDelegate {
	/**
	 * @see IActionDelegate#run(IAction)
	 */
	@Override
	public void run(IAction action) {
		ObjectPluginAction opa = (ObjectPluginAction)action;
		TreeSelection selection = (TreeSelection)opa.getSelection();
		IFile scenarioRunConfigurationResourceFile = (IFile)selection.getFirstElement();

		final ResourceSet resourceSet = new ResourceSetImpl();
		
	    resourceSet.getURIConverter().getURIMap().putAll(EcorePlugin.computePlatformURIMap(true));

		Resource scenarioRunConfigurationResource = resourceSet
				.getResource(URI.createPlatformResourceURI(scenarioRunConfigurationResourceFile.getFullPath()
						.toString(), true), true);
		
		Configuration scenarioRunConfiguration = (Configuration) scenarioRunConfigurationResource.getContents().get(0);

		try {
			SMLTestResultsView view = (SMLTestResultsView)PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView("org.scenariotools.sml.testing.views.SMLTestResultsView");
			view.runTests(scenarioRunConfiguration);
		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// TODO Auto-generated method stub
		
	}

}
