package org.scenariotools.sml.yakindu.testgenerator.exceptions;

public class NoPathsFoundException extends Exception {
	/**
	 * automatically added
	 */
	private static final long serialVersionUID = -4394161060196765474L;

	public NoPathsFoundException() {
		super("YakinduTestGenerator: There are no found paths!");
	}
}
