package org.scenariotools.sml.yakindu.testgenerator.exceptions;

public class StateGraphFileNotFoundException extends Exception {
	/**
	 * automatically added
	 */
	private static final long serialVersionUID = 1790089626954163449L;

	public StateGraphFileNotFoundException() {
		super("YakinduTestGenerator: Unable to find StateGraph File");
	}
}
