package org.scenariotools.sml.yakindu.testgenerator.ui.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.Transition;
import org.scenariotools.sml.yakindu.testgenerator.exceptions.NoPathsFoundException;
import org.scenariotools.sml.yakindu.testgenerator.exceptions.StateGraphFileNotFoundException;
import org.scenariotools.sml.yakindu.testgenerator.ui.sctunitgenerator.StateGraph2SCTUnitConverter;

public class ResourceUtils {
	private static final String DESTINATION_FOLDER_NAMEFILE = "SCTUnit-gen";
	private static final String FILE_EXTENSION_SCTUNIT = "sctunit";

	/**
	 * store the SCTUnit file to the project
	 * 
	 * @param stateGraphFile
	 *            the source file object of the state graph
	 * @param currentWindow
	 *            to show information message box to currentWindow
	 */
	public static void storeSCTUnit(IFile stateGraphFile, Collection<List<Transition>> tests, IWorkbenchWindow currentWindow) throws NoPathsFoundException {
		// Create path to new SCTUnit file
		IPath path = stateGraphFile.getProjectRelativePath();
		path = path.removeLastSegments(1);
		IProject project = stateGraphFile.getProject();
		IFolder destinationFolder = project.getFolder(path).getFolder(DESTINATION_FOLDER_NAMEFILE);

		if (!destinationFolder.exists()) {
			try {
				destinationFolder.create(IResource.NONE, true, null);
			} catch (CoreException e) {
				MessageDialog.openInformation(currentWindow.getShell(), "YakinduTestGenerator",
						"Failure creating " + DESTINATION_FOLDER_NAMEFILE + "!");
				e.printStackTrace();
			}
		}
		String destFileName = stateGraphFile.getName().substring(0, stateGraphFile.getName().lastIndexOf("."));
		IFile file = destinationFolder.getFile(destFileName + "." + FILE_EXTENSION_SCTUNIT);
		IPath p = file.getFullPath();
		if (p != null) {
			// create SCTUnit file
			if (!(FILE_EXTENSION_SCTUNIT).equalsIgnoreCase(p.getFileExtension()))
				p = p.addFileExtension(FILE_EXTENSION_SCTUNIT);
			IWorkspace workspace = ResourcesPlugin.getWorkspace();
			IWorkspaceRoot root = workspace.getRoot();
			IFile outputFile = root.getFile(p);
			// Get the name of the State Chart
			String stateChartName = getStateChartName(stateGraphFile, currentWindow);
			ByteArrayInputStream is = new ByteArrayInputStream(StateGraph2SCTUnitConverter
					.generateSCTUnitFromStateGraph(stateChartName, tests, currentWindow).toString().getBytes());
			try {
				if (outputFile.exists()) {
					outputFile.setContents(is, true, false, null);
				} else {
					outputFile.create(is, true, null);
				}
			} catch (CoreException e) {
				MessageDialog.openInformation(currentWindow.getShell(), "YakinduTestGenerator",
						"Can't create SCTUnit file!");
				e.printStackTrace();
			}
		}
	}

	public static String getStateChartName(IFile stateGraphFile, IWorkbenchWindow currentWindow) {
		// Get the Project of the state graph file
		IProject project = stateGraphFile.getProject();
		File folder = project.getRawLocation().toFile();
		File yakinduFolder = new File(folder, "yakindu");
		File[] yakinduFiles = yakinduFolder.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith("sct");
			}
		});

		// If many .sct files are found, the test code will be default generated from
		// the first .sct file
		if (yakinduFiles.length > 1) {
			MessageDialog.openInformation(currentWindow.getShell(), "YakinduTestGenerator",
					"Many .sct files found. The test code will be generated from file " + yakinduFiles[0].getName()
							+ ".");
		}

		// Get IFile
		IFile yakinduFile = convert(yakinduFiles[0]);

		// Get Resource
		Resource sctResource = getResource(yakinduFile);

		String stateChartName = getEObjectName(sctResource.getContents().get(0));
		return stateChartName;
	}

	public static String getEObjectName(EObject eObject) {
		if (eObject == null)
			return "<null>";
		for (EAttribute eAttribute : eObject.eClass().getEAllAttributes()) {
			if ("name".equals(eAttribute.getName())
					&& eAttribute.getEType().getInstanceClass() == String.class) {
				return (String) eObject.eGet(eAttribute);
			}
		}
		return eObject.hashCode() + "";
	}

	
	private static Resource getResource(IFile f) {
		ResourceSetImpl resourceSet = new ResourceSetImpl();
		return resourceSet.getResource(fromFile(f), true);
	}

	private static URI fromFile(IFile f) {
		return URI.createPlatformResourceURI(f.getFullPath().toString(), true);
	}

	public static IFile getSelectedStateGraphFile() throws StateGraphFileNotFoundException {
		ISelection selection = retrieveWorkbenchSelection();

		IFile stateGraphFile = null;

		if (selection instanceof IStructuredSelection) {
			for (Object object : ((IStructuredSelection) selection).toArray()) {
				if (object instanceof IFile) {
					stateGraphFile = (IFile) object;
				}
			}
		}

		if (stateGraphFile == null) {
			throw new StateGraphFileNotFoundException();
		}

		return stateGraphFile;
	}

	public static IFile convert(File file) {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IPath location = Path.fromOSString(file.getAbsolutePath());
		IFile iFile = workspace.getRoot().getFileForLocation(location);
		return iFile;
	}

	private static ISelection retrieveWorkbenchSelection() {
		return PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().getSelection();
	}
	

	public static SMLRuntimeStateGraph getStateGraph(IFile stateGraphFile) {
		URI UriResource= URI.createPlatformResourceURI(stateGraphFile.getFullPath().toString(), true);
		ResourceSetImpl resourceSet = new ResourceSetImpl();
		Resource resource =  resourceSet.getResource(UriResource, true);
		SMLRuntimeStateGraph runtimeStateGraph = (SMLRuntimeStateGraph) resource.getContents().get(0);
		return runtimeStateGraph;
	}
}
