package org.scenariotools.sml.yakindu.testgenerator.ui.sctunitgenerator

import java.util.Collection
import java.util.HashSet
import java.util.List
import org.eclipse.emf.ecore.EOperation
import org.eclipse.ui.IWorkbenchWindow
import org.scenariotools.sml.runtime.MessageEvent
import org.scenariotools.sml.runtime.Transition
import org.scenariotools.sml.runtime.WaitEvent
import org.scenariotools.sml.runtime.util.SMLRuntimeStateUtil

public class StateGraph2SCTUnitConverter {
	static var int testFile_MAX_SYSTEM_DELAY_CYCLES = 5

	/**
	 * Visits the state graph using corresponding handler, Converts the visited path to SCTUnit file.
	 */
	public def static generateSCTUnitFromStateGraph(String stateChartName, Collection<List<Transition>> testPaths, IWorkbenchWindow window) {
		var String testFileName = "testSuite"
		
			/**
			 * The output of the SCTUnit file
			 */
			'''
				testclass «testFileName» for statechart «stateChartName» {
					const MAX_SYSTEM_DELAY_CYCLES : integer = «testFile_MAX_SYSTEM_DELAY_CYCLES»
					
					// number of tests in the test suite: «testPaths.size»
					
					«var int operationNumber = 0»
					«FOR testPath : testPaths»
						«val Collection<String> outEventNameAlphabet = getOutEventNameAlphabet(testPath)»
						@Test
						/* test #«operationNumber = operationNumber + 1», test sequence: 
						«var int step = 0»
						«FOR transition : testPath»
							«IF transition.event instanceof WaitEvent»
								«" "» * STEP «step = step + 1»: WAIT (system waits for the next environment event)
								«getAlternativeEventsCommentsText(transition)»
							 «ELSEIF !SMLRuntimeStateUtil.isControllable(transition)»
								«" "» * STEP «step = step + 1»: IN  «getTransitionLabel(transition)»
								«getAlternativeEventsCommentsText(transition)»
							 «ELSE»
								«" "» * STEP «step = step + 1»: OUT «getTransitionLabel(transition)»
								«getAlternativeEventsCommentsText(transition)»
							 «ENDIF»
						«ENDFOR»
						 */
						operation test«operationNumber»(){
							enter
							
							var counter : integer = 0
							var observed : boolean = false
							
							«IF testPath !== null»
								«var int stepNumber = 0»

								«FOR transition : testPath»
									
									«IF transition.event instanceof WaitEvent»
										// ***  STEP  «stepNumber = stepNumber + 1»: WAIT (system waits for the next environment event) ***
									«ELSEIF !SMLRuntimeStateUtil.isControllable(transition)»
										// >>> BEGIN STEP  «stepNumber = stepNumber + 1» >>>
										«getEnvironmentAction(transition)»
										proceed 1 cycle
										// <<< END STEP  «stepNumber» <<<
										
									«ELSE»
										// >>> BEGIN STEP  «stepNumber = stepNumber + 1» >>>
										// Begin "assert that «getAssertCommentFromTransition(transition)» within «testFile_MAX_SYSTEM_DELAY_CYCLES» cycle(s)" 
										while (counter < MAX_SYSTEM_DELAY_CYCLES && !observed) {
											// behavior expected by test to continue
											if («getConditionFromTransition(transition)») {
												observed = true
											}
											
											// behavior that is allowed, but not expected by the test, and leads to an early termination of the test:
											«FOR t : transition.sourceState.outgoingTransition»
												«IF t.event instanceof MessageEvent»
													«var messageEvent = t.event as MessageEvent»
													«var String otherEventName = getStateChartEventFromMessageEvent(messageEvent)»
													«IF  transition.event instanceof MessageEvent
															&& otherEventName != getStateChartEventFromMessageEvent(transition.event as MessageEvent)»
													if (!observed && «getConditionFromTransition(t)») {
														return; //assert false message "Remove test (test fails in Step «stepNumber»)"
													}
													«ENDIF»
												«ENDIF»
											«ENDFOR»
											
											// behavior that is forbidden, because the observed event order violates the specification:
											«FOR outEventName : outEventNameAlphabet»
												«IF outEventName != transition.eventName»
													if («outEventName»){
														assert false message "The event '«outEventName»' must not be raised this point (Step «stepNumber»)."
													}
												«ENDIF»
											«ENDFOR»
											
											if (!observed && counter < MAX_SYSTEM_DELAY_CYCLES) {
												proceed 1 cycle
											}
											counter += 1
										}
										assert observed message "«getAssertCommentFromTransition(transition)» was not observed within «testFile_MAX_SYSTEM_DELAY_CYCLES» cycle(s) (Step «stepNumber»)."
										counter = 0
										observed = false  
										// End "assert «getAssertCommentFromTransition(transition)» within «testFile_MAX_SYSTEM_DELAY_CYCLES» cycle(s)"
										// <<< END STEP  «stepNumber» <<<
										
									«ENDIF»
								«ENDFOR»
							«ENDIF»
							exit
						}
						
					«ENDFOR»
				}
			'''
	}
	
	protected def static CharSequence getAlternativeEventsCommentsText(Transition transition){
		if (!SMLRuntimeStateUtil.isControllable(transition)) 
			''''''
		else
			'''«FOR alternativeTransition : transition.sourceState.outgoingTransition»
				«IF alternativeTransition.event instanceof MessageEvent»
					«var String otherEventName = getStateChartEventFromMessageEvent(alternativeTransition.event as MessageEvent)»
					«IF transition.event instanceof MessageEvent
						&& otherEventName != getStateChartEventFromMessageEvent(transition.event as MessageEvent)»
						«"  "»* 		alternative step (terminates when observed): «getAssertCommentFromTransition(alternativeTransition)»
					«ENDIF»
				«ENDIF»
			«ENDFOR»'''
	}
	
	def static getOutEventNameAlphabet(List<Transition> testPath) {
		var outEventNameAlphabet = new HashSet<String>();
		for (transition : testPath) {
			if(!SMLRuntimeStateUtil.isControllable(transition) && transition.event instanceof MessageEvent && !isLabeledWithSetEvent(transition)) {
				var messageEvent = transition.event as MessageEvent
				if (messageEvent.typedElement instanceof EOperation){
					outEventNameAlphabet.add(messageEvent.typedElement.name)
				}
			}
		}
		return outEventNameAlphabet
	}
	
	def static eventName(Transition transition){
		if(transition.event instanceof MessageEvent) {
			var messageEvent = transition.event as MessageEvent
			if (messageEvent.typedElement instanceof EOperation){
				return messageEvent.typedElement.name
			}
		}
	}
	
	
	def static getTransitionLabel(Transition t) {
		//return SMLRuntimeStateUtil.getEObjectName((t.getEvent() as MessageEvent).getTypedElement())
		return t.getEvent.toString
	}

	def static getEventStringWithParameter(Transition t) {
		return SMLRuntimeStateUtil.getEObjectName((t.getEvent() as MessageEvent).getTypedElement()) + "(" + getParameterValueString(t) + ")"
	}
	
	def static getParameterValueString(Transition transition) {
		val messageEvent = transition.getEvent() as MessageEvent
		if (messageEvent.parameterValues.isEmpty)
			""
		else 
			messageEvent.parameterValues.get(0).value
	}
		
	def static getStateChartEventFromMessageEvent(MessageEvent messageEvent) {
		return SMLRuntimeStateUtil.getEObjectName(messageEvent.getTypedElement())
	}

	def static isLabeledWithSetEvent(Transition transition){
		!((transition.event as MessageEvent).typedElement instanceof EOperation)
	}
	
	def static getEnvironmentAction(Transition t) {
		val messageEvent = t.event as MessageEvent
		if(messageEvent.typedElement instanceof EOperation) {
			var parameterString = "" 
			if(!messageEvent.parameterValues.empty) {
				parameterString = " : " +  messageEvent.parameterValues.get(0).value
			}
			return "raise " + getStateChartEventFromMessageEvent(messageEvent) + parameterString 
		} else { // instanceof EStructuralFeature
			return getStateChartEventFromMessageEvent(messageEvent) + " = " + messageEvent.parameterValues.get(0).value
		}		
	}
	
	def static getConditionFromTransition(Transition t) {
		val messageEvent = t.event as MessageEvent
		if(messageEvent.typedElement instanceof EOperation) {
			return getStateChartEventFromMessageEvent(messageEvent) // out events with parameters seem to be not supported in Yakindu SCTUnitTest
		} else { // instanceof EStructuralFeature
			val eTypedElement = (t.getEvent() as MessageEvent).getTypedElement();
			val stateEAnnotation = eTypedElement.getEAnnotation("TESTING_state")
			if (stateEAnnotation !== null){
				if(messageEvent.parameterValues.get(0).value instanceof Boolean && messageEvent.parameterValues.get(0).value == false)
					return "!active(" + stateEAnnotation.details.get("state") + ")"
				else
					return "active(" + stateEAnnotation.details.get("state") + ")"
			}else
				return getStateChartEventFromMessageEvent(messageEvent) + " == " + messageEvent.parameterValues.get(0).value
		}
	}	
	
	
	def static getAssertCommentFromTransition(Transition transition) {
		val messageEvent = transition.event as MessageEvent
		if(messageEvent.typedElement instanceof EOperation) {
			'''the event '«getEventStringWithParameter(transition)»' happens'''
		} else { // instanceof EStructuralFeature
			'''the condition '«getConditionFromTransition(transition)»' is true'''
		}
	}	
	
}
