package org.scenariotools.sml.yakindu.testgenerator.ui.handlers;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.Transition;
import org.scenariotools.sml.yakindu.testgenerator.ui.testpathextraction.TestPathGeneratorForEdgeCoverateOfPOGraph;
import org.scenariotools.sml.yakindu.testgenerator.ui.utils.ResourceUtils;

public class GenerateTestSuiteWithEdgeCoverageCommandHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);

		try {
			IFile stateGraphFile = ResourceUtils.getSelectedStateGraphFile();
			SMLRuntimeStateGraph stateGraph = ResourceUtils.getStateGraph(stateGraphFile);
			Collection<List<Transition>> testPaths = TestPathGeneratorForEdgeCoverateOfPOGraph.getTestPaths(stateGraph);
			ResourceUtils.storeSCTUnit(stateGraphFile, testPaths, window);			
		} catch(Exception e) {
			e.printStackTrace();
		}

		return null;
	}



	
}
