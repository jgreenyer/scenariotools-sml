package org.scenariotools.sml.yakindu.testgenerator.ui.testpathextraction;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.Map.Entry;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.ETypedElement;
import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.Transition;
import org.scenariotools.sml.runtime.WaitEvent;
import org.scenariotools.sml.runtime.util.SMLRuntimeStateUtil;

public class TestPathGeneratorForEdgeCoverateOfPOGraph {

	public static Collection<List<Transition>> getTestPaths(SMLRuntimeStateGraph stateGraph) {
		Collection<List<Transition>> result = new HashSet<List<Transition>>();
		Set<Transition> covered = new HashSet<Transition>();
		
		Queue<Map.Entry<Transition, Integer>> depths = getTransitionDepths(stateGraph);
		
		while(!depths.isEmpty()) {
			Entry<Transition, Integer> entry = depths.poll();
			Transition target = entry.getKey();
			if(covered.contains(target))
				continue;
			
			List<Transition> path = findPathToTransition(stateGraph.getStartState(), target, entry.getValue());
			path = filterOutEvents(extendPath(path));
			
			for(Transition t : path) {
				covered.add(t);
			}
			result.add(path);
		}

		result = removeEquivalentPaths(result);
		
		return result;
	}
	
	private static Collection<List<Transition>> removeEquivalentPaths(Collection<List<Transition>> result) {
		Map<String, List<Transition>> stringToPathMap = new HashMap<String, List<Transition>>();
		for (List<Transition> path : result) {
			stringToPathMap.put(getPathMessageEventSequenceString(path), path);
		}
		return stringToPathMap.values();
	}

	private static String getPathMessageEventSequenceString(List<Transition> path) { // we might want to replace this with something more efficient.
		String pathString = "";
		for (Transition t : path) {
			if (t.getEvent() instanceof MessageEvent) {
				pathString+=t.getEvent().toString();
			}
		}
		return pathString;
	}


	private static Queue<Map.Entry<Transition, Integer>> getTransitionDepths(SMLRuntimeStateGraph stateGraph) {
		
		Queue<Map.Entry<Transition, Integer>> result = new PriorityQueue<Map.Entry<Transition, Integer>>(stateGraph.getStates().size(), new Comparator<Map.Entry<Transition, Integer>>() {
			// order elements from highest depth (=path length) to lowest depth. The depth will be stored in the 'value' (Integer)
			@Override
			public int compare(Entry<Transition, Integer> o1, Entry<Transition, Integer> o2) {
				return o2.getValue() - o1.getValue(); //if the 1st is smaller than the 2nd the result will be positve, indicating they are ordered in the "wrong"(for us) way
			}
		});
		Queue<Map.Entry<SMLRuntimeState, Integer>> bfsQueue = new LinkedList<Map.Entry<SMLRuntimeState, Integer>>();
		bfsQueue.add(new AbstractMap.SimpleImmutableEntry<SMLRuntimeState, Integer>(stateGraph.getStartState(), 0));
		Set<SMLRuntimeState> visited = new HashSet<SMLRuntimeState>();
		visited.add(stateGraph.getStartState());
		
		while(!bfsQueue.isEmpty()) {
			Map.Entry<SMLRuntimeState, Integer> entry = bfsQueue.poll();
			SMLRuntimeState state = entry.getKey();
			Integer newDepth = entry.getValue() + 1;
			
			for(Transition t : state.getOutgoingTransition()) {
				if(t.getEvent() instanceof MessageEvent) {
					result.add(new AbstractMap.SimpleImmutableEntry<Transition, Integer>(t, newDepth));					
				}
				
				SMLRuntimeState nextState = t.getTargetState();
				if(!visited.contains(nextState)) {
					bfsQueue.add(new AbstractMap.SimpleImmutableEntry<SMLRuntimeState, Integer>(nextState, newDepth));
					visited.add(nextState);
				}
			}
		}

		return result;
	}
	
	private static List<Transition> findPathToTransition(SMLRuntimeState startState, Transition target, int targetDepth) {
		Stack<List<Transition>> stack = new Stack<List<Transition>>();
		
		for(Transition t : startState.getOutgoingTransition()) {
			List<Transition> path = new ArrayList<Transition>(1);
			path.add(t);
			stack.push(path);
		}
		
		while(!stack.isEmpty()) {
			List<Transition> path = stack.pop();
			Transition last = path.get(path.size()-1);
			
			if(path.size() < targetDepth) {
				for(Transition t : last.getTargetState().getOutgoingTransition()) {
					List<Transition> newPath = new ArrayList<Transition>(path);
					newPath.add(t);
					stack.push(newPath);
				}				
			} else {
				if(last == target)
					return path;		
			}
		}
		
		assert false;
		return null; // will never be reached
	}

	// extend path s.t. it ends with a complete super step
	private static List<Transition> extendPath(List<Transition> initialPath) {
		
		Stack<List<Transition>> stack = new Stack<List<Transition>>();
		stack.push(initialPath);
		
		while(!stack.isEmpty()) {
			List<Transition> path = stack.pop();
			Transition last = path.get(path.size()-1);
			
			if(SMLRuntimeStateUtil.isControllable(last) && !isControllable(last.getTargetState())) {
				return path;
			} else {
				if(path != initialPath && endsWithCycle(path)) {
					continue;
				}
				
				for(Transition t : last.getTargetState().getOutgoingTransition()) {
					List<Transition> newPath = new ArrayList<Transition>(path);
					newPath.add(t);
					stack.push(newPath);
				}				
			}
		}		

		throw new RuntimeException("State space contains an infinite super step.");
	}


	public static boolean isControllable(SMLRuntimeState state) {
		if(state.getOutgoingTransition().size() == 0) {
			throw new RuntimeException("Test generation does not work for specifications which can lead to deadlocks.");
		}
		
		return SMLRuntimeStateUtil.isControllable(state.getOutgoingTransition().get(0));
	}

	private static boolean endsWithCycle(List<Transition> path) {
		SMLRuntimeState last = path.get(path.size()-1).getTargetState();
		for(Transition t : path) {
			if(t.getSourceState() == last) {
				return true;
			}
		}
		
		return false;
	}
	
	private static List<Transition> filterOutEvents(List<Transition> path) {
		for(Iterator<Transition> iterator = path.iterator(); iterator.hasNext();) {
			Transition t = iterator.next();
			if (t.getEvent() instanceof MessageEvent) {
				MessageEvent messageEvent = (MessageEvent) t.getEvent();
				if (!t.getSourceState().getObjectSystem().isControllable(messageEvent.getSendingObject())
						&& !t.getSourceState().getObjectSystem().isControllable(messageEvent.getReceivingObject())) {
					iterator.remove();
				} else if (hasIgnoreAnnotation(messageEvent.getTypedElement())) {
					iterator.remove();
				}
			}
		}
		
		return path;
	}

	private static boolean hasIgnoreAnnotation(ETypedElement eTypedElement) {
		return eTypedElement.getEAnnotation("TESTING_ignore") != null;
	}
		
}
