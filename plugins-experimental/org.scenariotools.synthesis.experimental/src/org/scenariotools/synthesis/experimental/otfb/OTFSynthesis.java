/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.synthesis.experimental.otfb;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.eclipse.core.runtime.IProgressMonitor;
import org.scenariotools.sml.ScenarioKind;
import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.Transition;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.runtime.util.SMLRuntimeStateUtil;
import org.scenariotools.synthesis.AbstractSynthesisAlgorithm;
import org.scenariotools.synthesis.ResultMetaData;
import org.scenariotools.synthesis.SynthesisResult;

public class OTFSynthesis extends AbstractSynthesisAlgorithm {

	protected OTFBAlgorithm otfbAlgorithm;

	private Map<SMLRuntimeState, SMLRuntimeState> specStateToControllerStateMap;

	public OTFSynthesis() {
		this(true);
	}
	
	public OTFSynthesis(boolean includeSafetyViolatingStatesInController) {
		this.includeSafetyViolatingStatesInController = includeSafetyViolatingStatesInController;
	}
	

	public void initializeOTFBAlgorithm() {
		otfbAlgorithm = new OTFBAlgorithm();
	}

	public boolean otfSynthesizeController(SMLRuntimeStateGraph runtimeStateGraph, IProgressMonitor monitor) {

		strategyExists = otfbAlgorithm.OTFB(runtimeStateGraph.getStartState(), monitor);

		extractController();

		SMLRuntimeStateUtil.annotateStrategyStates(
				runtimeStateGraph, 
				specStateToControllerStateMap.keySet(),
				strategyExists);

		return strategyExists;
	}
	
	Set<Transition> excludedTransitions;
	
	Set<SMLRuntimeState> visitedOuter;
	Set<SMLRuntimeState> visitedInner;
	Stack<SMLRuntimeState> stackOuter;
	Stack<Transition> stackInner;
	private boolean includeSafetyViolatingStatesInController = false;
	
	/**
	 * Extracts the controller as follows
	 * 1. Perform nested DFS over state graph returned by the synthesis algorithm
	 * 		This procedure will mark certain transitions as excluded (adding them to the excludedTransitions set)
	 * 		Excluded transitions are:
	 * 		a. in case there IS a strategy:
	 * 			1. controllable transitions to losing states
	 * 			2. controllable transitions that open loops back to their source state without encountering goal states ("unlive loops") 
	 * 		b. in case there IS NO strategy:
	 * 			1. uncontrollable transitions to winning states
	 * 			2. uncontrollable transitions that open loops back to their source state with encountering goal states ("live loops")
	 *   (a.1./b.1 will be added by the outer DFS, a.2/b.2 will be added by the inner DFS)
	 * 2. Perform DFS traversal of the state graph, ignoring excluded transitions, 
	 *    and translate the traversed part of the graph to a controller graph
	 *    
	 * (This should in visit each state in the state graph AT MOST THREE TIMES.) 
	 */
	protected void extractController() {
		
//		System.out.println("EXTRACTING CONTROLLER");
		
		excludedTransitions = new HashSet<Transition>();
		
		visitedOuter = new HashSet<SMLRuntimeState>();
		stackOuter = new Stack<SMLRuntimeState>();
		visitedInner = new HashSet<SMLRuntimeState>();
		
		outerDFS(otfbAlgorithm.getStartState());

		// Extraction by DFS traversal of state graph excluding the excludedTransitions.
		createControllerFromStateGraph();
		
	}

	/**
	 * Returns true iff 
	 * 		   strategy exists and state is not goal
	 *      OR no strategy exists and state is goal
	 *   which is equivalent to:
	 *         strategy exists XOR state is goal
	 * @param state
	 * @return
	 */
	private boolean isRelevantStartStateForLoopDetection(SMLRuntimeState state){
		return strategyExists ^ otfbAlgorithm.getGoal().contains(state);
	}
	
	/**
	 * Returns true iff 
	 * 		   strategy exists and transition is controllable
	 *      OR no strategy exists and transition is uncontrollable
	 *   which is equivalent to:
	 *         strategy exists XOR transition is uncontrollable
	 * @param transition
	 * @return
	 */
	private boolean canTransitionBeRemoved(Transition transition){
		return strategyExists ^ !otfbAlgorithm.isControllable(transition);
	}
	
	/**
	 * Returns true iff transition can be removed and 
	 * 		   strategy exists and target state is losing
	 * 		OR no strategy exists and target state is not losing
	 *    which is equivalent to:
	 *    	strategy exists XOR target state is not losing
	 * @param transition
	 * @return
	 */
	private boolean mustTransitionBeRemoved(Transition transition){
		return (canTransitionBeRemoved(transition)
				&& (strategyExists ^ !isLosing(transition.getTargetState())))
			|| removeSafetyViolatingStatesInController(transition.getTargetState());
	}
	
	private boolean isLosing(SMLRuntimeState state){
		return (!otfbAlgorithm.getWinAndNotLoseBuechiStates().contains(state)) 
				|| isDeadlock(state); 
	}
	
	/** 
	 * returns true if includeSafetyViolatingStatesInController is false and
	 *  a) a strategy exists and there is an assumption safety violation in the state
	 *  a) no strategy exists and there is a guarantee safety violation in the state
	 * @param state
	 * @return
	 */
	private boolean removeSafetyViolatingStatesInController(SMLRuntimeState state){
		return !includeSafetyViolatingStatesInController
				&& (strategyExists 
						&& state.isSafetyViolationOccurredInAssumptions() 
					|| !strategyExists 
						&& (state.isSafetyViolationOccurredInGuarantees()
								|| isDeadlock(state)));
	}
	
	private boolean isDeadlock(SMLRuntimeState state) {
		return state.getOutgoingTransition().isEmpty();
//		return state.getStringToStringAnnotationMap().get("deadlock") != null;
	}

	// for eliminating transitions to deadlock states
	private Map<SMLRuntimeState,Set<Transition>> stateToIncomingTransitionsMap;
	
	/**
	 * 
	 * @param startState
	 */
	private void outerDFS(SMLRuntimeState startState) {

		stackOuter.push(startState);
		visitedOuter.add(startState);
		
		stateToIncomingTransitionsMap = new HashMap<SMLRuntimeState,Set<Transition>>();
		stateToIncomingTransitionsMap.put(startState, new HashSet<Transition>()); // no need to remember incoming transitions for start state
		
		
		while (!stackOuter.isEmpty()) {
			SMLRuntimeState s = stackOuter.peek();
			SMLRuntimeState unvisitedSuccessor = null;
			for (Transition t : s.getOutgoingTransition()) {
				if (excludedTransitions.contains(t)) continue;
				if (mustTransitionBeRemoved(t)){
					excludeTransition(t);
					continue;
				}
				SMLRuntimeState targetState = t.getTargetState();
				
				if (!visitedOuter.contains(targetState)){
					// unvisited successor found:
					unvisitedSuccessor = targetState;
					
					// remember incoming transition -- not necessary for DFS, only for eliminating paths to deadlocks.
					// This elimination is triggered when popping states from the stack, see below.
					Set<Transition> incomingTransitions = stateToIncomingTransitionsMap.get(unvisitedSuccessor);
					if (incomingTransitions == null){
						incomingTransitions = new HashSet<Transition>();
						stateToIncomingTransitionsMap.put(unvisitedSuccessor, incomingTransitions);
					}
					incomingTransitions.add(t);

					// one unvisited successor found, now we continue with the surrounding while loop.
					break;
				}
			}
			if (unvisitedSuccessor != null){ // there was an unvisited successor
				stackOuter.push(unvisitedSuccessor);
				visitedOuter.add(unvisitedSuccessor);
			}else{ // there was no unvisited successor
				stackOuter.pop(); // removes s from stack
				
				 
				// continue with loop detection only if s remains reachable
				if (!excludeIncomingTransitionIfDeadlockState(s)){// this call eliminates path to deadlock
					
					// continue with loop detection
					if(isRelevantStartStateForLoopDetection(s)){
						for (Transition t : s.getOutgoingTransition()) {
							if (canTransitionBeRemoved(t) && 
									innerDFS(t)){
								excludeTransition(t);
								// s may now be a deadlock state, eliminate again 
								excludeIncomingTransitionIfDeadlockState(s);
							} 
						}
					}
				}
			}
		}
	}
	
	/**
	 * removes transitions leading to s if s is a deadlock state.
	 * 
	 * @param s
	 * @return
	 */
	private boolean excludeIncomingTransitionIfDeadlockState(SMLRuntimeState s) {
		
		if (isSafetyViolatingStateThatCanStayDeadlockInController(s)){
//			System.out.println("state " + getStateIndex(s) + " can stay deadlock state");
			return false;
		}
		
		int outGoingNonExcludedTransitions = s.getOutgoingTransition().size();
		for (Transition outTransition : s.getOutgoingTransition()) {
			if (excludedTransitions.contains(outTransition))
				outGoingNonExcludedTransitions--;
		}
		
		if (outGoingNonExcludedTransitions == 0){
			Set<Transition> transitionsToExclude = stateToIncomingTransitionsMap.get(s);
			assert (transitionsToExclude != null); // cannot be null if state was visited.
			excludedTransitions.addAll(transitionsToExclude);
//			System.out.println("state " + getStateIndex(s) + " must not be reachable in controller");
			return true;
		}else{
//			System.out.println("state " + getStateIndex(s) + " can (for now) remain reachable in controller");
			return false;
		}
	}
	
	private boolean isSafetyViolatingStateThatCanStayDeadlockInController(SMLRuntimeState runtimeState){
		boolean returnValue = (strategyExists 
								&& runtimeState.isSafetyViolationOccurredInAssumptions() 
								//&& otfbAlgorithm.hasMandatoryMessageEvents(runtimeState,false)
							||!strategyExists 
								&& (//runtimeState.isSafetyViolationOccurredInRequirements()|| 
									 runtimeState.isSafetyViolationOccurredInGuarantees())
								&& !otfbAlgorithm.scenariosAreInRequestedState(runtimeState,ScenarioKind.ASSUMPTION));
		return returnValue;
	}

	/**
	 * The procedure is called from outerDFS:
	 * returns true if it finds cycle from the source state and via the target state of the startTransition back to the source state.
	 * 
	 * a) if a strategy exists: 
	 * 	1. the source state of startTransition is a non-goal state 
	 *  2. only cycles will be searched without goal states    
	 * 
	 * b) if no strategy exists: 
	 * 	1. the source state of startTransition is a goal state 
	 *  2. only cycles will be searched with goal states    
	 * 
	 * @return
	 */
	private boolean innerDFS(Transition startTransition) {
		
//		System.out.println("inner DFS called for transition : " + getTransitionString(startTransition));
		
		// return true if the transition itself is a cycle
		if (startTransition.getSourceState() == startTransition.getTargetState())
			return true;
		
		// return false if target state of transition is not relevant (e.g., there is a strategy 
		// and the target state is a goal state, which means this will not be an unlive loop).
		if (!isRelevantStartStateForLoopDetection(startTransition.getTargetState()))
			return false;
		
		stackInner = new Stack<Transition>();
		stackInner.push(startTransition);
		SMLRuntimeState startState = startTransition.getSourceState();
		
		while(!stackInner.isEmpty()){
			Transition t = stackInner.pop();
			SMLRuntimeState targetState = t.getTargetState();
			if (targetState == startState) {
				// cycle detected!
//				System.out.println("cycleDetected");
				return true;
			}
			if (!visitedInner.contains(targetState)){
				for (Transition outgoingTransition : targetState.getOutgoingTransition()) {
					// only explore not already excluded transitions
					if (!excludedTransitions.contains(outgoingTransition)){
						// only explore not already excluded transitions
						if (isRelevantStartStateForLoopDetection(outgoingTransition.getTargetState())){
//							System.out.println("inner DFS -- pushing transition " + getTransitionString(outgoingTransition));
							stackInner.push(outgoingTransition);							
						}else{
//							System.out.println("inner DFS -- target state not relevant for loop, stopping fwd exploration here: " +  getTransitionString(outgoingTransition));
						}
					}
					else{
//						System.out.println("inner DFS -- transition already excluded: " +  getTransitionString(outgoingTransition));
					}
				}
				visitedInner.add(targetState);
			}
		}

		// no cycle detected
		return false;
	}
	
	private void excludeTransition(Transition transition){
//		System.out.println("excludeTransition " + getTransitionString(transition));
		assert(canTransitionBeRemoved(transition));
		excludedTransitions.add(transition);
	}
	
	
	// stack is a transition-stack and not a state-stack because this simplifies the translation of transitions 
	private Stack<Transition> stackForExtraction;
	private Set<Event> eventsOnControllerTransition;

	/**
	 *  Perform DFS traversal of the state graph, ignoring excluded transitions, 
	 *    and translate the traversed part of the graph to a controller graph.
	 */
	private void createControllerFromStateGraph() {
		strategy = RuntimeFactory.eINSTANCE.createSMLRuntimeStateGraph();
		stackForExtraction = new Stack<Transition>();
		// keyset of this map also acts as passed set for depth-first-traversal of graph in createControllerFromStateGraph.
		specStateToControllerStateMap = new HashMap<SMLRuntimeState, SMLRuntimeState>();
		
		eventsOnControllerTransition = new HashSet<Event>();
		
		SMLRuntimeState startState = otfbAlgorithm.getStartState();
		SMLRuntimeState controllerStartState = createControllerState(startState);
		specStateToControllerStateMap.put(startState, controllerStartState);
		((SMLRuntimeStateGraph)strategy).setStartState(controllerStartState);
		pushOutTransitionsOnStack(startState, stackForExtraction);
		
		while(!stackForExtraction.isEmpty()){
			Transition t = stackForExtraction.pop();
			SMLRuntimeState targetState = t.getTargetState();
			if(!specStateToControllerStateMap.containsKey(targetState)){
				SMLRuntimeState controllerTargetState = createControllerState(targetState);
				specStateToControllerStateMap.put(targetState, controllerTargetState);
				createControllerTransition(t);
				pushOutTransitionsOnStack(targetState, stackForExtraction);
			}else{ // only translate transition
				createControllerTransition(t);
			}
		}
		Set<MessageEvent> eventsNotOnControllerTransition = new HashSet<MessageEvent>(otfbAlgorithm.getEventsOnExploredTransitions());
		eventsNotOnControllerTransition.removeAll(eventsOnControllerTransition);
		
		SMLRuntimeState stateWithForbiddenEvents = RuntimeFactory.eINSTANCE.createSMLRuntimeState();
		SMLRuntimeStateUtil.setPassedIndex(stateWithForbiddenEvents, "forbidden");
		stateWithForbiddenEvents.setObjectSystem(startState.getObjectSystem());
		((SMLRuntimeStateGraph)strategy).getStates().add(stateWithForbiddenEvents);
		
		for (MessageEvent messageEvent : eventsNotOnControllerTransition) {
			createControllerTransition(stateWithForbiddenEvents, stateWithForbiddenEvents, messageEvent);
		}
	}
	
	private SMLRuntimeState createControllerState(SMLRuntimeState state){
		SMLRuntimeState newControllerState = RuntimeFactory.eINSTANCE.createSMLRuntimeState();
		SMLRuntimeStateUtil.setPassedIndex(newControllerState, SMLRuntimeStateUtil.getPassedIndex(state));
		
		newControllerState.setObjectSystem(state.getObjectSystem());
		
		if (state.getStringToBooleanAnnotationMap().get("win") != null)
			newControllerState.getStringToBooleanAnnotationMap().put("win",
					state.getStringToBooleanAnnotationMap().get("win"));
		if (state.getStringToBooleanAnnotationMap().get("loseBuechi") != null)
			newControllerState.getStringToBooleanAnnotationMap().put("loseBuechi",
					state.getStringToBooleanAnnotationMap().get("loseBuechi"));
		if (state.getStringToBooleanAnnotationMap().get("goal") != null)
			newControllerState.getStringToBooleanAnnotationMap().put("goal",
					state.getStringToBooleanAnnotationMap().get("goal"));
//		if (state.isSafetyViolationOccurredInRequirements())
//			newControllerState.getStringToBooleanAnnotationMap().put("requirementsSafetyViolation", true);
		if (state.isSafetyViolationOccurredInGuarantees())
			newControllerState.getStringToBooleanAnnotationMap().put("specificationsSafetyViolation", true);
		if (state.isSafetyViolationOccurredInAssumptions())
			newControllerState.getStringToBooleanAnnotationMap().put("assumptionSafetyViolation", true);
		
		((SMLRuntimeStateGraph)strategy).getStates().add(newControllerState);
		return newControllerState;
	} 

	private void createControllerTransition(Transition transition){
		createControllerTransition(
				specStateToControllerStateMap.get(transition.getSourceState()),
				specStateToControllerStateMap.get(transition.getTargetState()),
				transition.getEvent()
				);
	} 
	
	private void createControllerTransition(SMLRuntimeState sourceControllerState, SMLRuntimeState targetControllerState, Event event){
		Transition controllerTransition = RuntimeFactory.eINSTANCE.createTransition();
		controllerTransition.setSourceState(sourceControllerState);
		controllerTransition.setTargetState(targetControllerState);
		// reference original event
		controllerTransition.setEvent(event);
		eventsOnControllerTransition.add(event);
	}
	
	private void pushOutTransitionsOnStack(SMLRuntimeState state, Stack<Transition> stack){
		for (Transition outgoingTransition : state.getOutgoingTransition()) {
			if (!excludedTransitions.contains(outgoingTransition)){
				stack.push(outgoingTransition);
			}
		}
	}

	public Map<SMLRuntimeState, SMLRuntimeState> getSpecStateToControllerStateMap() {
		return specStateToControllerStateMap;
	}

	public OTFBAlgorithm getOTFBAlgorithm() {
		return otfbAlgorithm;
	}

	@ResultMetaData(name="time [ms]", group="Synthesis Time", position=1)
	public long synthesisTime = 0;
	
	@ResultMetaData(name="number of winning states", group=SynthesisResult.STATEGRAPH_GROUP_NAME, position=1)
	public long numWinningStates = 0;
	
	@Override
	public Object synthesize(Configuration configuration, IProgressMonitor monitor) {
		long time = System.currentTimeMillis();
		stateGraph = RuntimeFactory.eINSTANCE.createSMLRuntimeStateGraph();
		stateGraph.init(configuration);
		initializeOTFBAlgorithm();
		otfSynthesizeController(stateGraph, monitor);
		synthesisTime = System.currentTimeMillis() - time;
		return this;
	}
}
