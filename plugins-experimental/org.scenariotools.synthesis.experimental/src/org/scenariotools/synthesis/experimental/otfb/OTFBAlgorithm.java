/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Mohammed Abed
 */
package org.scenariotools.synthesis.experimental.otfb;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.ScenarioKind;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.runtime.util.SMLRuntimeStateUtil;
import org.scenariotools.synthesis.experimental.Activator;
import org.scenariotools.sml.runtime.Transition;

public class OTFBAlgorithm {

	private static Logger logger = Activator.getLogManager().getLogger(OTFBAlgorithm.class.getName());

	private Set<SMLRuntimeState> goal;

	public Set<SMLRuntimeState> getGoal() {
		return goal;
	}

	private Set<SMLRuntimeState> lose;
	private Map<SMLRuntimeState, Set<SMLRuntimeState>> requiredGoals;

	SMLRuntimeState startState;

	// Set<Transition> liveLoopTransitions;
	// Map<RuntimeState, Set<RuntimeState>> predecessorSet;

	public boolean OTFB(SMLRuntimeState startState, IProgressMonitor monitor) {
		initPostProcessingRelevantStructures();
		this.startState = startState;
		goal = new HashSet<SMLRuntimeState>();
		if (isGoal(startState)) {
			goal.add(startState);
		}
		lose = new HashSet<SMLRuntimeState>();
		requiredGoals = new HashMap<SMLRuntimeState, Set<SMLRuntimeState>>();

		initStateAnnotations(startState);
		// check if startState is losing already
		if (!OTFR(startState)) {
			lose.add(startState);

			// decoration -- only relevant for debug purposes / graphical
			// rendering
			if (logger.isDebugEnabled()) {
				addStateAnnotation(startState, "+l");
				setStateStatusFlags(startState.getStateGraph());
			}

			return false;
		}
		Stack<SMLRuntimeState> reevaluate = getUndecidedGoalStates();
		while (!reevaluate.isEmpty() && !monitor.isCanceled()) {
			SMLRuntimeState g = reevaluate.pop();
			if (!OTFR(g)) {
				lose.add(g);

				// decoration -- only relevant for debug purposes / graphical
				// rendering
				if (logger.isDebugEnabled())
					addStateAnnotation(g, "+l");

				if (g == startState) {

					// decoration -- only relevant for debug purposes /
					// graphical rendering
					if (logger.isDebugEnabled())
						setStateStatusFlags(startState.getStateGraph());

					return false;
				}
			}

			if (reevaluate.isEmpty())
				reevaluate = getUndecidedGoalStates();
		}
		
		if(monitor.isCanceled()){
			logger.debug("Synthesis canceled!");
			throw new OperationCanceledException("Synthesis canceled!");
		}
		// cleanup
		cleanAllRequiredGoals();
		for (SMLRuntimeState winningState : requiredGoals.keySet()) {
			removeDanglingTemporaryTransitions(winningState.getOutgoingTransition());
		}

		// decoration -- only relevant for debug purposes / graphical rendering
		if (logger.isDebugEnabled())
			setStateStatusFlags(startState.getStateGraph());

		return true;
	}

	private void initPostProcessingRelevantStructures() {
		eventsOnExploredTransitions = new HashSet<>();
	}

	/**
	 * Returns all goal states, whose winning/losing status is not determined
	 * yet.
	 * 
	 * @return
	 */
	private Stack<SMLRuntimeState> getUndecidedGoalStates() {
		Stack<SMLRuntimeState> undecidedGoalStates = new Stack<SMLRuntimeState>();
		for (SMLRuntimeState goalState : goal) {
			if (!lose.contains(goalState)) {
				cleanRequiredGoals(goalState);
				if (requiredGoals.get(goalState) == null)
					undecidedGoalStates.add(goalState);
			}
		}

		return undecidedGoalStates;
	}

	/**
	 * Determines if the system can guarantee reaching a goal state from this
	 * state
	 * 
	 * @param startState
	 * @return true, if the system can guarantee reaching a goal state, false
	 *         otherwise
	 */
	private boolean OTFR(SMLRuntimeState startState) {
		// logger.debug("OTFR with state: " + startState);
		// set of visited states
		Set<SMLRuntimeState> passed = new HashSet<SMLRuntimeState>();
		// stack of transitions to be explored
		Stack<Transition> waiting = new Stack<Transition>();
		// maps states to transitions from predecessors
		Map<SMLRuntimeState, Set<Transition>> depend = new HashMap<SMLRuntimeState, Set<Transition>>();

		// only relevant for post-processing
		addStateAnnotation(startState, "+OTFR");

		passed.add(startState);

		addTransitionsForAllOutgoingMessageEvents(startState, waiting);
		while (!waiting.isEmpty() && !requiredGoals.containsKey(startState)) {
			Transition t = waiting.pop();

			// boolean newTransition = false;

			if (t.getTargetState() == null) {
				t = generateSuccessor(t);
				initStateAnnotations(t.getTargetState());
			}

			SMLRuntimeState sourceState = t.getSourceState();
			SMLRuntimeState targetState = t.getTargetState();

			// forward exploration
			if (!passed.contains(targetState)) {
				passed.add(targetState);
				addDepend(depend, targetState, t);
				cleanRequiredGoals(targetState);
				if (requiredGoals.containsKey(targetState) || lose.contains(targetState) || isGoal(targetState)) {
					waiting.push(t);
					// if(targetState.isSafetyViolationOccurredInAssumptions())
					// win.add(targetState);//we can assume that from here we
					// can always win.
				} else {
					boolean targetStateIsNoDeadlockState = addTransitionsForAllOutgoingMessageEvents(targetState, waiting);
					if (!targetStateIsNoDeadlockState && !scenariosAreInRequestedState(targetState, ScenarioKind.ASSUMPTION)){
						lose.add(targetState);
					}
				}
			} else { // backward re-evaluation

				// System.out.println("bdw-evaluating:"+sourceState.getStringToStringAnnotationMap().get("passedIndex"));
				// System.out.println("lose:"+lose.contains(sourceState));
				// System.out.println("isLose:"+isLose(sourceState));

				if (!lose.contains(sourceState) && isLose(sourceState)) {
					// mark sourceState, if it becomes losing
					lose.add(sourceState);
					// reschedule all states that depend on sourceState
					if (depend.get(sourceState) != null)
						waiting.addAll(depend.get(sourceState));
					// JG: optimization
					removeOutgoingTransitionsFromStack(waiting, sourceState);

					// only relevant for post-processing
					addStateAnnotation(sourceState, "+l");
				} else if (!requiredGoals.containsKey(sourceState)
						// only if the winning status changes
						&& updateRequiredGoals(sourceState)) {

					// logger.debug("marking winning: " + sourceState +
					// " req goal states: " + requiredGoals.get(sourceState));
					if (depend.get(sourceState) != null)
						waiting.addAll(depend.get(sourceState));
					// JG: optimization
					removeOutgoingTransitionsFromStack(waiting, sourceState);

					// only relevant for post-processing
					addStateAnnotation(sourceState, "+w");

				}
				// necessary?:
				// if (!requiredGoals.containsKey(targetState)
				// && !lose.contains(targetState))
				addDepend(depend, targetState, t);

			}

		}

		// decoration -- only relevant for debug purposes / graphical rendering
		addStateAnnotation(startState, "-OTFR");

		// logger.debug("OTFR returns: " +
		// requiredGoals.containsKey(startState));
		return requiredGoals.containsKey(startState);
	}

	/**
	 * Removes <em>state</em> from the reqiredGoals map, if it is losing or if
	 * it depends on a state that was marked losing.
	 * 
	 * @param state
	 */
	private void cleanRequiredGoals(SMLRuntimeState state) {
		// losing states do not require goal states
		if (lose.contains(state)) {
			requiredGoals.remove(state);
			return;
		}
		Set<SMLRuntimeState> requiredGoalStates = requiredGoals.get(state);
		if (requiredGoalStates != null) {
			// search for a goal state that is required but losing and clean
			// state's requiredGoals if one is found
			for (SMLRuntimeState runtimeState : requiredGoalStates) {
				if (lose.contains(runtimeState)) {
					requiredGoals.remove(state);
					return;
				}
			}
		}
	}

	/**
	 * Removes requiredGoals entries for states that are losing or depend on
	 * goal states that are losing.
	 */
	private void cleanAllRequiredGoals() {
		Iterator<Map.Entry<SMLRuntimeState, Set<SMLRuntimeState>>> requiredGoalsKeySetIterator = requiredGoals.entrySet()
				.iterator();
		outer: while (requiredGoalsKeySetIterator.hasNext()) {
			Map.Entry<SMLRuntimeState, Set<SMLRuntimeState>> requiredGoalsEntry = (Map.Entry<SMLRuntimeState, Set<SMLRuntimeState>>) requiredGoalsKeySetIterator.next();
			if (lose.contains(requiredGoalsEntry.getKey())) {
				requiredGoalsKeySetIterator.remove();
				break;
			}
			Set<SMLRuntimeState> requiredGoalStates = requiredGoalsEntry.getValue();
			Assert.isTrue(requiredGoalStates != null && !requiredGoalStates.isEmpty());
			for (SMLRuntimeState runtimeState : requiredGoalStates) {
				if (lose.contains(runtimeState)) {
					requiredGoalsKeySetIterator.remove();
					break outer;
				}
			}

		}
	}

	/**
	 * returns true if an entry in requiredGoals can be created for a state.
	 * Such an entry can be created I) if there is one state reachable via a
	 * controllable transition that has an entry in requiredGoals and where the
	 * value set V in requireGoals for that state does not contain any losing
	 * state. Then an entry state->V is created in requiredGoals II) OR if there
	 * is at least one outgoing uncontrollable transition and for all
	 * uncontrollable outgoing transitions all successors have an entry in
	 * requiredGoals and where the value sets V1..Vn for these successors in
	 * requiredGoals do not contain any losing state. Then an entry
	 * state->U1..nVi (union of V1..Vn) is created in requiredGoals for the
	 * respective successor states.
	 * 
	 * @param state
	 * @return
	 */
	private boolean updateRequiredGoals(SMLRuntimeState state) {

		if (state.getOutgoingTransition().isEmpty())
			return false;

		EList<Transition> controllableOutgoingTransitions = new BasicEList<>();
		EList<Transition> uncontrollableOutgoingTransitions = new BasicEList<>();

		for (Transition transition : state.getOutgoingTransition()) {
			if (isControllable(transition)) {
				controllableOutgoingTransitions.add(transition);
			} else {
				uncontrollableOutgoingTransitions.add(transition);
			}
		}

		for (Transition transition : controllableOutgoingTransitions) {
			SMLRuntimeState targetState = transition.getTargetState();
			if (targetState != null) {
				if (lose.contains(targetState))
					continue;
				if (isGoal(targetState)) {
					Set<SMLRuntimeState> newRequiredGoalsValueSet = new HashSet<SMLRuntimeState>();
					newRequiredGoalsValueSet.add(targetState);
					requiredGoals.put(state, newRequiredGoalsValueSet);
					return true;
				} else {
					cleanRequiredGoals(targetState);
					Set<SMLRuntimeState> targetStateRequiredGoals = requiredGoals.get(targetState);
					if (targetStateRequiredGoals != null) {
						// target state has entry in requiredGoals after
						// cleanRequiredGoals. This means that the targetState
						// is marked "winning".

						// mark state winning, based on the same required goals
						// as the winning target state.
						requiredGoals.put(state, targetStateRequiredGoals);
						return true;
					}
				}
			}
		}

		// there were no outgoing controllable transitions
		// leading to goal or winning states..

		// return false if there are no outgoing uncontrollable transitions
		if (uncontrollableOutgoingTransitions.isEmpty())
			return false;

		Set<SMLRuntimeState> requiredGoalsForState = new HashSet<SMLRuntimeState>();

		for (Transition transition : uncontrollableOutgoingTransitions) {
			SMLRuntimeState targetState = transition.getTargetState();
			if (targetState == null) {
				// not explored yet, so we cannot say anything about the winning
				// status of the state
				return false;
			}
			if (lose.contains(targetState)) {
				// losing successor, therefore state cannot be winning
				return false;
			}
			cleanRequiredGoals(targetState);
			Set<SMLRuntimeState> targetStateRequiredGoals = requiredGoals.get(targetState);
			if (isGoal(targetState)) {
				// successor is goal state -> mark goal state as required goal
				// state
				requiredGoalsForState.add(targetState);
			} else if (targetStateRequiredGoals != null) {
				// successor is winning state -> remember required goal states
				// of target state as required goal states for current state
				Assert.isTrue(!targetStateRequiredGoals.isEmpty());
				requiredGoalsForState.addAll(targetStateRequiredGoals);
			} else { // successor not winning -> state cannot be winning
				return false;
			}
		}

		// the above loop must have had at least one iteration in which it
		// either returned false at some point
		// or have at least added one element in requiredGoalsForState
		Assert.isTrue(!requiredGoalsForState.isEmpty());

		requiredGoals.put(state, requiredGoalsForState);

		return true;

	}

	/**
	 * Returns true if <em>state</em> has no outgoing transitions <em>or</em>
	 * all outgoing controllable or at least one outgoing uncontrollable
	 * transition lead to a losing state
	 * 
	 * @param state
	 * @return
	 */
	private boolean isLose(SMLRuntimeState state) {
		// System.out.println("isLose("+sourceState.getStringToStringAnnotationMap().get("passedIndex")+")");
		if (state.getOutgoingTransition().isEmpty())
			return true;
		boolean noUncontrollableSuccessors = true;
		boolean losingUncontrollableSuccessor = false;
		for (Transition transition : state.getOutgoingTransition()) {
			if (isControllable(transition)) {
				if (transition.getTargetState() == null) {
					// it's an unexplored temporary transition
					return false;
				}
				if (!lose.contains(transition.getTargetState())) {
					// state has non-losing controllable successor
					return false;
				}
			} else {
				noUncontrollableSuccessors = false;
				if (transition.getTargetState() == null) {
					// it's an unexplored temporary transition
					continue;
				}
				if (lose.contains(transition.getTargetState())) {
					// may NOT return true here, because there may still be
					// controllable transitions
					losingUncontrollableSuccessor = true;
				}
			}
		}
		// no non-losing controllable successors at this point
		return (noUncontrollableSuccessors
				// env will pick losing move if possible
				|| losingUncontrollableSuccessor
				|| ((false//state.isSafetyViolationOccurredInRequirements()
						|| state.isSafetyViolationOccurredInGuarantees())
						&& !state.isSafetyViolationOccurredInAssumptions()
						// env will not prevent system from losing if not
						// mandatory
						&& !scenariosAreInRequestedState(state, ScenarioKind.ASSUMPTION)));
	}

	/**
	 * Replaces temporary with "real" transition, i.e. with a transition
	 * returned by {@link RuntimeStateGraph#generateSuccessor}.
	 */
	protected Transition generateSuccessor(Transition t) {
		SMLRuntimeState sourceState = (SMLRuntimeState) t.getSourceState();
		t.setSourceState(null); // must detach temporary transition from source
								// state again!
		SMLRuntimeStateGraph sg = (SMLRuntimeStateGraph) sourceState.getStateGraph();

		t = sg.generateSuccessor(sourceState, (MessageEvent) t.getEvent());

		// t.getStringToStringAnnotationMap().put("exploreOrder", c1+"_"+c2);
		return t;
	}

	/**
	 * Add t to the set of transitions that depend on state q.
	 * 
	 * @param depend
	 * @param q
	 * @param t
	 */
	private void addDepend(Map<SMLRuntimeState, Set<Transition>> depend, SMLRuntimeState q, Transition t) {
		Set<Transition> dependingTransitions = depend.get(q);
		if (dependingTransitions == null) {
			dependingTransitions = new HashSet<Transition>();
			depend.put(q, dependingTransitions);
		}
		dependingTransitions.add(t);
	}

	/**
	 * Returns true if q is a goal state. A state is a goal state, if any of the
	 * following conditions is met.
	 * <ul>
	 * <li>no safety violation has occurred in the guarantee
	 * scenarios, and there are no enabled requested events in active
	 * guarantee scenarios</li>
	 * <li>a safety violation of the assumptions occurred</li>
	 * <li>a safety violation of the guarantee scenarios
	 * occurred and there are enabled requested events in the assumptions, but
	 * no requested events in guarantee scenarios.</li>
	 * 
	 * @param q
	 * @return
	 */
	public boolean isGoal(SMLRuntimeState q) {
		if (goal.contains(q))
			return true;
		else {
			boolean guaranteeScenariosAreInRequestedState = scenariosAreInRequestedState(q, ScenarioKind.GUARANTEE);
			boolean assumptionScenariosAreInRequestedState = scenariosAreInRequestedState(q, ScenarioKind.ASSUMPTION);
			
			// 1. no safety violation must have occurred in the guarantees,
			// and there must not be any enabled requested interaction fragment in active 
			// guarantee scenarios
			// OR 2. there was a safety violation in the assumptions
			// OR 3. A safety violation occurred in the guarantees,
			// and there are enabled requested events in the assumptions,
			// and there are no requested events in guarantee scenarios
			if ((!/*q.isSafetyViolationOccurredInRequirements()*/false && !q.isSafetyViolationOccurredInGuarantees())
					&& !guaranteeScenariosAreInRequestedState
					|| q.isSafetyViolationOccurredInAssumptions()
							|| (q.isSafetyViolationOccurredInGuarantees())
							&& assumptionScenariosAreInRequestedState
							&& (!guaranteeScenariosAreInRequestedState)) {
				goal.add(q);
				return true;
			}
		}
		return false;
	}

	public boolean scenariosAreInRequestedState(SMLRuntimeState runtimeState, ScenarioKind scenarioKind) {
		for (ActiveScenario s : runtimeState.getActiveScenarios()) {
			if (s.getScenario().getKind() == scenarioKind && s.isInRequestedState())
				return true;
		}
		return false;
	}

	
//	public boolean hasMandatoryMessageEvents(RuntimeState runtimeState, ScenarioKind scenarioKind) {
//		for (ActiveScenario s : ((SMLRuntimeState) runtimeState).getActiveScenarios()) {
//			if (s.getScenario().getKind() == scenarioKind && !s.getRequestedEvents().isEmpty())
//				return true;
//		}
//		return false;
//	}

	/**
	 * Removes the outgoing transitions of the given source state from the given
	 * stack and removes unexplored transitions from the source state, i.e.,
	 * transitions where the target state is null.
	 */
	private void removeOutgoingTransitionsFromStack(Stack<Transition> waiting, SMLRuntimeState sourceState) {
		for (Iterator<Transition> transitionsIterator = sourceState.getOutgoingTransition()
				.iterator(); transitionsIterator.hasNext();) {
			Transition transition = transitionsIterator.next();
			waiting.remove(transition);
			// remove unexplored tmp transitions
			if (transition.getTargetState() == null)
				transitionsIterator.remove();
		}
	}

	/**
	 * Generates successors of the given runtime state and pushes the outgoing
	 * transitions on the top of the passed waiting stack. It is ensured that
	 * among the pushed transitions, the controllable transitions are on the top
	 * of the stack.
	 * 
	 * @param runtimeState
	 * @param waiting
	 * 
	 */
	protected boolean addTransitionsForAllOutgoingMessageEvents(SMLRuntimeState runtimeState, Stack<Transition> waiting) {

		boolean atLeastOneTransitionWasAdded = false;
		Map<Integer, Set<Transition>> prioritySetsMap = new HashMap<Integer, Set<Transition>>();
		int maxPriorityValue = 0;

		for (Event event : runtimeState.getEnabledEvents()) {
			if(!(event instanceof MessageEvent))
				continue;
			MessageEvent messageEvent = (MessageEvent)event;
			
			Transition transition = runtimeState.getEventToTransitionMap().get(messageEvent);

			if (transition == null) {
				transition = createTmpTransition(runtimeState, messageEvent);
			}
			eventsOnExploredTransitions.add(messageEvent);

			// Assert.isTrue(!waiting.contains(transition));
			// TODO priority was used for incremental extension (http://dx.doi.org/10.1145/2491411.2491445). Do we still need it?
			Integer transitionPriority = getTransitionPriority(transition, messageEvent);
			if (maxPriorityValue < transitionPriority)
				maxPriorityValue = transitionPriority;
			if (prioritySetsMap.get(transitionPriority) == null) {
				prioritySetsMap.put(transitionPriority, new HashSet<Transition>());
			}
			prioritySetsMap.get(transitionPriority).add(transition);

			atLeastOneTransitionWasAdded = true;

		}

		for (int i = maxPriorityValue; i >= 0; i--) {
			if (prioritySetsMap.get(i) != null) {
				waiting.addAll(prioritySetsMap.get(i));
			}
		}

		return atLeastOneTransitionWasAdded;
	}

	private Set<MessageEvent> eventsOnExploredTransitions;

	public Set<MessageEvent> getEventsOnExploredTransitions() {
		return eventsOnExploredTransitions;
	}

	/**
	 *
	 * "0" is the highest priority.
	 *
	 * @param transition
	 * @param modalMessageEvent
	 * @return
	 */
	protected int getTransitionPriority(Transition transition, MessageEvent messageEvent) {
		if (isControllable(transition)) {
			for (ActiveScenario s : ((SMLRuntimeState) transition.getSourceState()).getActiveScenarios()) {
				if (s.getScenario().getKind() == ScenarioKind.GUARANTEE) {
					if (s.isBlocked(messageEvent)) {
						return 0;
					}
				}
			}
		} else
			return 1;
		return 0;
	}

	protected Transition createTmpTransition(SMLRuntimeState runtimeState, MessageEvent messageEvent) {
		Transition tmpTransition = RuntimeFactory.eINSTANCE.createTransition();
		tmpTransition.setEvent(messageEvent);
		tmpTransition.setSourceState(runtimeState);
		return tmpTransition;
	}

	protected boolean isControllable(SMLRuntimeState runtimeState, MessageEvent messageEvent) {
		return runtimeState.getObjectSystem().isControllable(messageEvent.getSendingObject());
	}

	protected boolean isControllable(Transition transition) {
		return transition.getSourceState().getObjectSystem()
				.isControllable(((MessageEvent) transition.getEvent()).getSendingObject());
	}

	/**
	 * Removes transitions that have no target state.
	 * 
	 * @param transitions
	 */
	private void removeDanglingTemporaryTransitions(Collection<Transition> transitions) {
		for (Transition transition : transitions) {
			if (transition.getTargetState() == null)
				transition.setSourceState(null);
		}
	}

	public Set<SMLRuntimeState> getWinAndNotLoseBuechiStates() {
		return requiredGoals.keySet();
	}

	public Set<SMLRuntimeState> getLosingStates() {
		return lose;
	}

	public SMLRuntimeState getStartState() {
		return startState;
	}

	public void setStartState(SMLRuntimeState startState) {
		this.startState = startState;
	}

	private int passedStatesCounter = 1;

	private Set<SMLRuntimeState> globalPassed = new HashSet<SMLRuntimeState>();

	private void initStateAnnotations(SMLRuntimeState state) {
		if (logger.isDebugEnabled()) {
			if (!globalPassed.contains(state)) {
				SMLRuntimeStateUtil.setPassedIndex(state, String.valueOf(passedStatesCounter++));
				state.getStringToStringAnnotationMap().removeKey("stateLog");
				state.getStringToBooleanAnnotationMap().removeKey("win");
				state.getStringToBooleanAnnotationMap().removeKey("loseBuechi");
				state.getStringToBooleanAnnotationMap().removeKey("goal");
				if (scenariosAreInRequestedState(state, ScenarioKind.ASSUMPTION)) {
					addStateAnnotation(state, "hasActiveAssumMsg");
				}
				for (ActiveScenario s : ((SMLRuntimeState) state).getActiveScenarios()) {
					if (s.getScenario().getKind() == ScenarioKind.ASSUMPTION && !s.getRequestedEvents().isEmpty())
						state.getStringToBooleanAnnotationMap().put(s.getScenario().getName()+ "_ASSUMPTION", Boolean.TRUE);
					if (s.getScenario().getKind() == ScenarioKind.GUARANTEE && !s.getRequestedEvents().isEmpty())
						state.getStringToBooleanAnnotationMap().put(s.getScenario().getName()+ "_SPECIFICATION", Boolean.TRUE);
				}
				globalPassed.add(state);
			}
		}
	}

	/**
	 * Annotates state with the String newAnnotation.
	 * 
	 * @param state
	 * @param newAnnotation
	 */
	private void addStateAnnotation(SMLRuntimeState state, String newAnnotation) {
		if (logger.isDebugEnabled()) {
			String stateLog = state.getStringToStringAnnotationMap().get("stateLog");
			String newStateLog;
			if (stateLog != null && !stateLog.isEmpty())
				newStateLog = stateLog + ", " + newAnnotation + "(" + (passedStatesCounter - 1) + ")";
			else
				newStateLog = newAnnotation + "(" + (passedStatesCounter - 1) + ")";
			String[] lines = newStateLog.split("\\n");
			if (lines[lines.length - 1].length() > 35)
				newStateLog += "\\n";
			state.getStringToStringAnnotationMap().put("stateLog", newStateLog);
		}
	}

	private void setStateStatusFlags(SMLRuntimeStateGraph graph) {
		if (logger.isDebugEnabled()) {
			for (SMLRuntimeState smlState : graph.getStates()) {
				smlState.getStringToBooleanAnnotationMap().put("goal", goal.contains(smlState));
				smlState.getStringToBooleanAnnotationMap().put("loseBuechi", lose.contains(smlState));
				smlState.getStringToBooleanAnnotationMap().put("win", requiredGoals.containsKey(smlState));

				if(smlState.getOutgoingTransition().isEmpty()){
					String annotationString = SMLRuntimeStateUtil.getBlockedGuaranteeScenarioInformation(smlState);
					if (!annotationString.isEmpty())
						SMLRuntimeStateUtil.setDeadlockString(smlState, annotationString);
					else{
						if (smlState.getEnabledEvents().isEmpty()){
							SMLRuntimeStateUtil.setDeadlockString(smlState, "No enabled message events.");
						}
					}
				}
				
				if(SMLRuntimeStateUtil.isSystemStepBlockedButAssumptionRequestedEvents(smlState)){
					String annotationString = SMLRuntimeStateUtil.getBlockedGuaranteeScenarioInformation(smlState);
					if (!annotationString.isEmpty())
						SMLRuntimeStateUtil.setSystemStepBlockedButAssumptionRequestedEventsString(smlState, annotationString);					
				}
				
			}
		}
	}
		

	
}
