package org.scenariotools.synthesis.experimental.gr1.synthesis;

import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.synthesis.AbstractSynthesisAlgorithm;
import org.scenariotools.synthesis.experimental.gr1.acceptancesetcalculator.AcceptanceStatesSetsCollector;
import org.scenariotools.synthesis.experimental.gr1.gamealgorithms.GR1Game;
import org.scenariotools.synthesis.experimental.gr1.stategraphexploration.DFSExplorer;
import org.scenariotools.synthesis.experimental.gr1.strategyextraction.AttractorStrategy;
import org.scenariotools.synthesis.experimental.gr1.strategyextraction.AttractorStrategyCalculator;
import org.scenariotools.synthesis.experimental.gr1.strategyextraction.SMLRuntimeStateGraphStrategyInformationAnnotator;
import org.scenariotools.synthesis.experimental.gr1.strategyextraction.StrategyExtractor;

public class GR1SynthesisAlgorithm extends AbstractSynthesisAlgorithm {
	@Override
	public Object synthesize(Configuration configuration, IProgressMonitor monitor) {
		GR1SynthesisResult result = new GR1SynthesisResult();
		
		long currentTimeMillis = System.currentTimeMillis();
		stateGraph = RuntimeFactory.eINSTANCE.createSMLRuntimeStateGraph();
		stateGraph.init(configuration);
		new DFSExplorer().exploreStateGraph(stateGraph, monitor);
		result.timeMillisForStateGraphExploration = System.currentTimeMillis() - currentTimeMillis;
		
		currentTimeMillis = System.currentTimeMillis();
		AcceptanceStatesSetsCollector acceptanceStatesSetsCollector = new AcceptanceStatesSetsCollector(stateGraph);		
		result.timeForAcceptanceSetCalculation = System.currentTimeMillis() - currentTimeMillis;
		
		currentTimeMillis = System.currentTimeMillis();
		GR1Game.gr1GameBasic(stateGraph, acceptanceStatesSetsCollector, result);
		result.timeMillisForGameSolvingOnGraph = System.currentTimeMillis() - currentTimeMillis;
		result.totalTime = result.timeMillisForStateGraphExploration + result.timeForAcceptanceSetCalculation + result.timeMillisForGameSolvingOnGraph;
				
		strategyExists = result.winningStates.contains(stateGraph.getStartState());
		if(strategyExists) {
			currentTimeMillis = System.currentTimeMillis();
			Map<String, AttractorStrategy> strategies = AttractorStrategyCalculator.calculate(true, result.winningStates, result.losingStates, new AttractorStrategy.Factory());
			strategy = StrategyExtractor.extractStrategy(stateGraph, strategies);
			result.timeStrategyExtraction = System.currentTimeMillis() - currentTimeMillis;
			SMLRuntimeStateGraphStrategyInformationAnnotator.annotateAttractorStrategyInformation(strategies, strategyExists);
		}		
		SMLRuntimeStateGraphStrategyInformationAnnotator.annotateStrategyOrCounterStrategyStates(result.winningStates, result.losingStates, strategyExists);		
		
		return result;
	}	
}
