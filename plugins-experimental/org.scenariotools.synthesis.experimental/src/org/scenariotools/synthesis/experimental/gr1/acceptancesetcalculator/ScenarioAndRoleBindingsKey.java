package org.scenariotools.synthesis.experimental.gr1.acceptancesetcalculator;

import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;

public class ScenarioAndRoleBindingsKey {

	public final Scenario scenario;
	public final ActiveScenarioRoleBindings activeScenarioBindings;
	private final int hashCode;

	public ScenarioAndRoleBindingsKey(Scenario scenario, ActiveScenarioRoleBindings roleBindings) {
		this.scenario = scenario;
		this.activeScenarioBindings = roleBindings;
		hashCode = scenario.hashCode() * 31 + roleBindings.hashCode();
	}
	
	public ScenarioAndRoleBindingsKey(ActiveScenario activeScenario) {
		this(activeScenario.getScenario(), activeScenario.getRoleBindings());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || obj.getClass() != this.getClass())
			return false;
		else {
			ScenarioAndRoleBindingsKey otherScenarioAndRoleBindingsKey = (ScenarioAndRoleBindingsKey) obj;
			return (otherScenarioAndRoleBindingsKey.scenario == this.scenario
					&& otherScenarioAndRoleBindingsKey.activeScenarioBindings == this.activeScenarioBindings);
		}
	}

	@Override
	public int hashCode() {
		return hashCode;
	}

}
