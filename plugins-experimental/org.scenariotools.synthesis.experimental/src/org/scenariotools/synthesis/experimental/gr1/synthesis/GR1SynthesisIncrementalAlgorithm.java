package org.scenariotools.synthesis.experimental.gr1.synthesis;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.util.SMLRuntimeStateUtil;
import org.scenariotools.sml.runtime.util.TransitionUtil;
import org.scenariotools.synthesis.experimental.gr1.acceptancesetcalculator.AcceptanceStatesSetsCollector;
import org.scenariotools.synthesis.experimental.gr1.gamealgorithms.GR1Game;
import org.scenariotools.synthesis.experimental.gr1.gamealgorithms.Player;
import org.scenariotools.synthesis.experimental.gr1.stategraphexploration.IncrementalDFSExplorer;
import org.scenariotools.synthesis.experimental.gr1.strategyextraction.AttractorStrategy;
import org.scenariotools.synthesis.experimental.gr1.strategyextraction.AttractorStrategyCalculator;
import org.scenariotools.synthesis.experimental.gr1.strategyextraction.SMLRuntimeStateGraphStrategyInformationAnnotator;
import org.scenariotools.synthesis.experimental.gr1.strategyextraction.StrategyExtractor;
import org.scenariotools.sml.runtime.Transition;
import org.scenariotools.sml.runtime.configuration.Configuration;

public class GR1SynthesisIncrementalAlgorithm extends GR1SynthesisAlgorithm {
	@Override
	public Object synthesize(Configuration configuration, IProgressMonitor monitor) {
		IncrementalGR1SynthesisResult result = new IncrementalGR1SynthesisResult();
		
		long totalTimeForGameSolvingOnGraph = 0;
		long totalTimeForStateGraphExploration = 0;
		long totalTimeForAcceptanceSetCalculation = 0;
		int numberOfGR1GameSolvingIncrements = 0;
		int howOftenSwitchedSides = 0;

		long currentTime = System.currentTimeMillis();

		monitor.beginTask("GR(1) Synthesis Incremental", 250);
		monitor.subTask("Initial exploration of state graph until first cycle is found");
		stateGraph = RuntimeFactory.eINSTANCE.createSMLRuntimeStateGraph();
		stateGraph.init(configuration);
		IncrementalDFSExplorer incrementalExplorer = new IncrementalDFSExplorer(stateGraph, monitor);
		incrementalExplorer.initialExplorationUntilFirstCycleIsFound();
		AcceptanceStatesSetsCollector acceptanceStatesSetsCollector = new AcceptanceStatesSetsCollector(stateGraph);
		monitor.worked(1);
		
		labelInterationIndices(stateGraph, numberOfGR1GameSolvingIncrements);

		totalTimeForStateGraphExploration += System.currentTimeMillis() - currentTime;

		// boolean gameSolved = false;

		Set<SMLRuntimeState> winningStates = new HashSet<SMLRuntimeState>();

		int numberOfIncrementsDoneForTheSamePlayer = 0;

		Player previousIterationWinner = Player.SYSTEM;
		
		GR1SynthesisResult synthesisResult = null;
		
		while (true && !monitor.isCanceled()) {
			numberOfGR1GameSolvingIncrements++;
			currentTime = System.currentTimeMillis();

			String lastWinner = "SYSTEM";
			if (previousIterationWinner == Player.ENVIRONMENT)
				lastWinner = "ENVIRONMENT";

			monitor.subTask("Solving GR(1) game (stategraph size: " + stateGraph.getStates().size()
					+ " -- increments for last winner: " + numberOfIncrementsDoneForTheSamePlayer 
					+ " -- last winner " + lastWinner + ")");
			
			//System.out.println("iteration # " + numberOfGR1GameSolvingIncrements);
			
			synthesisResult = new GR1SynthesisResult();			
			GR1Game.gr1GameBasic(stateGraph, acceptanceStatesSetsCollector, synthesisResult);
			monitor.worked(1);
			winningStates = synthesisResult.winningStates;

			totalTimeForGameSolvingOnGraph += (System.currentTimeMillis() - currentTime);

			Player currentIterationWinner;
			if (winningStates.contains(stateGraph.getStartState())) {
				currentIterationWinner = Player.SYSTEM;
			}else {
				currentIterationWinner = Player.ENVIRONMENT;
			}
			
			if (currentIterationWinner != previousIterationWinner) {
				previousIterationWinner = currentIterationWinner;
				numberOfIncrementsDoneForTheSamePlayer = 0;
				howOftenSwitchedSides++;
			}

			currentTime = System.currentTimeMillis();

			numberOfIncrementsDoneForTheSamePlayer++;

			int stateSpaceGrowthInNextIteration;
			boolean stateSpaceGrowthAsNumCycles; // iff true interpret stateSpaceGrowthInNextIteration as number of cycles to explore, otherwise interpret it as number of states to explore
			
			// GROWTH RATE:
			// none
//			stateSpaceGrowthInNextIteration = 1;
			// linear: 1, 2, 3, 4, 5, ....
//			stateSpaceGrowthInNextIteration = numberOfIncrementsDoneForTheSamePlayer;
			// quadratic: 1, 4, 9, 16, 25, ...
//			 stateSpaceGrowthInNextIteration = numberOfIncrementsDoneForTheSamePlayer * numberOfIncrementsDoneForTheSamePlayer;
			// exponential 2^x: 2, 4, 16, 32, ...
//			stateSpaceGrowthInNextIteration = (int) Math.pow(2,
//			 numberOfIncrementsDoneForTheSamePlayer);
			// exponential 3^x: 3, 9, 27, 81, ...
//			stateSpaceGrowthInNextIteration = (int) Math.pow(3, numberOfIncrementsDoneForTheSamePlayer - 1);
			// exponential x^x: 1, 4, 27, 256, 3125, ...
			// stateSpaceGrowthInNextIteration = (int)
//			 Math.pow(numberOfIncrementsDoneForTheSamePlayer,
//			 numberOfIncrementsDoneForTheSamePlayer);

			
//			stateSpaceGrowthAsNumCycles = true;
//			stateSpaceGrowthInNextIteration = 1;
//			stateSpaceGrowthInNextIteration = numberOfIncrementsDoneForTheSamePlayer;
//			stateSpaceGrowthInNextIteration = numberOfIncrementsDoneForTheSamePlayer * numberOfIncrementsDoneForTheSamePlayer;
//			stateSpaceGrowthInNextIteration = (int) Math.pow(2, numberOfIncrementsDoneForTheSamePlayer);
			
			stateSpaceGrowthAsNumCycles = false;
//			stateSpaceGrowthInNextIteration = (int)(smlRuntimeStateGraph.getStates().size() * .06);
//			stateSpaceGrowthInNextIteration = (int)(smlRuntimeStateGraph.getStates().size() * .12);
			stateSpaceGrowthInNextIteration = (int)(stateGraph.getStates().size() * .25);
//			stateSpaceGrowthInNextIteration = (int)(smlRuntimeStateGraph.getStates().size() * .50);
//			stateSpaceGrowthInNextIteration = smlRuntimeStateGraph.getStates().size();
			
			

//			stateSpaceGrowthInNextIteration = 1;

//			stateSpaceGrowthInNextIteration = numberOfIncrementsDoneForTheSamePlayer
//			+ Math.round(smlRuntimeStateGraph.getStates().size() / 100);

			
//			if(smlRuntimeStateGraph.getStates().size() < 300) {
//				stateSpaceGrowthInNextIteration = 1;
//			}else {
//				if(previousIterationWinner == Player.ENVIRONMENT)
//				stateSpaceGrowthInNextIteration = numberOfIncrementsDoneForTheSamePlayer
//						+ Math.round(smlRuntimeStateGraph.getStates().size() / 100);
//				else
//					stateSpaceGrowthInNextIteration = 1;
//			}

//			if(previousIterationWinner == Player.SYSTEM)
//				stateSpaceGrowthInNextIteration = 1000;
				
//			stateSpaceGrowthInNextIteration = Math.round(smlRuntimeStateGraph.getStates().size() / 5);

			
			
			// stateSpaceGrowthInNextIteration = 1 +
			// Math.round(smlRuntimeStateGraph.getStates().size() / 100);
			// stateSpaceGrowthInNextIteration = 1;

			// System.out.println(stateSpaceGrowthInNextIteration + " --
			// System winning?: " + (previousIterationWinner == Player.SYSTEM));

			boolean anythingLeftToExplore = true;

			monitor.subTask("Continuing incremental exploration (growth: "
					+ stateSpaceGrowthInNextIteration + ")");
			if (previousIterationWinner == Player.SYSTEM) { // explore further
															// uncontrollable
															// transitions
				anythingLeftToExplore = incrementalExplorer.exploreNextCyclesForSomeStateInGivenScopeSet(winningStates,
						stateSpaceGrowthInNextIteration, stateSpaceGrowthAsNumCycles, Player.ENVIRONMENT, true);
			} else { // explore further controllable transitions
				Set<SMLRuntimeState> losingStates = new HashSet<SMLRuntimeState>(stateGraph.getStates());
				losingStates.removeAll(winningStates);
				anythingLeftToExplore = incrementalExplorer.exploreNextCyclesForSomeStateInGivenScopeSet(losingStates,
						stateSpaceGrowthInNextIteration, stateSpaceGrowthAsNumCycles, Player.SYSTEM, true);
			}
			totalTimeForStateGraphExploration += (System.currentTimeMillis() - currentTime);
			monitor.worked(1);
			
			currentTime = System.currentTimeMillis();
			
			acceptanceStatesSetsCollector = new AcceptanceStatesSetsCollector(stateGraph); //TODO: change incremental explorer to update this efficiently
			//acceptanceStatesSetsCollector.updateAcceptanceStateSetsForExtendedStateGraph(stateGraph);

			totalTimeForAcceptanceSetCalculation += (System.currentTimeMillis() - currentTime);

//			labelInterationIndices(smlRuntimeStateGraph, numberOfGR1GameSolvingIncrements);

			if (!anythingLeftToExplore)
				break;
		}

		//System.out.println(LoggerUtil.concatenateStateIndicesStringForStateSet(synthesisResult.winningStates));
		
		result.timeMillisForStateGraphExploration = totalTimeForStateGraphExploration;
		result.timeForAcceptanceSetCalculation = totalTimeForAcceptanceSetCalculation;
		result.timeMillisForGameSolvingOnGraph = totalTimeForGameSolvingOnGraph;
		result.totalTime = totalTimeForStateGraphExploration + totalTimeForAcceptanceSetCalculation + totalTimeForGameSolvingOnGraph;
		
		result.numberOfGR1GameSolvingIncrements = numberOfGR1GameSolvingIncrements;
		result.howOftenSwitchedSides = howOftenSwitchedSides;
		
		result.winningStates = synthesisResult.winningStates;
		result.numWinningStates = synthesisResult.numWinningStates;

		result.losingStates = synthesisResult.losingStates;
		result.numLosingStates = synthesisResult.numLosingStates;
		
//		result.guaranteeAcceptanceStateSets = synthesisResult.guaranteeAcceptanceStateSets;
//		result.assumptionAcceptanceStateSets = synthesisResult.assumptionAcceptanceStateSets;
		
		strategyExists = result.winningStates.contains(stateGraph.getStartState());
		if(strategyExists) {
			currentTime = System.currentTimeMillis();
			Map<String, AttractorStrategy> strategies = AttractorStrategyCalculator.calculate(true, result.winningStates, result.losingStates, new AttractorStrategy.Factory());
			strategy = StrategyExtractor.extractStrategy(stateGraph, strategies);
			result.timeStrategyExtraction = System.currentTimeMillis() - currentTime;
			SMLRuntimeStateGraphStrategyInformationAnnotator.annotateAttractorStrategyInformation(strategies, strategyExists);
		}		
		SMLRuntimeStateGraphStrategyInformationAnnotator.annotateStrategyOrCounterStrategyStates(result.winningStates, result.losingStates, strategyExists);		
		
		return result;
	}

	private void labelInterationIndices(SMLRuntimeStateGraph smlRuntimeStateGraph,
			int numberOfGR1GameSolvingIncrements) {
		for (SMLRuntimeState state : smlRuntimeStateGraph.getStates()) {
			if (SMLRuntimeStateUtil.getIterationInWhichStateWasExplored(state) == null) {
				SMLRuntimeStateUtil.setIterationInWhichStateWasExplored(state, String.valueOf(numberOfGR1GameSolvingIncrements));
			}
			for (Transition outTransition : state.getOutgoingTransition()) {
				if (TransitionUtil.getIterationInWhichTransitionWasExplored(outTransition) == null) {
					TransitionUtil.setIterationInWhichTransitionWasExplored(outTransition, String.valueOf(numberOfGR1GameSolvingIncrements));
				}
			}
		}
	}

}
