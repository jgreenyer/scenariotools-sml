/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     
 * Author:
 * 		Joel Greenyer
 *		Daniel Gritzner
 */
package org.scenariotools.synthesis.experimental.gr1.strategyextraction;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.synthesis.experimental.gr1.gamealgorithms.AttractorCalculator;
import org.scenariotools.synthesis.experimental.gr1.gamealgorithms.Player;
import org.scenariotools.synthesis.experimental.gr1.util.SynthesisUtil;
import org.scenariotools.sml.runtime.Transition;

public class AttractorStrategy {

	public final Set<SMLRuntimeState> targetStates = new HashSet<SMLRuntimeState>();
	public final Set<SMLRuntimeState> attractorStates = new HashSet<SMLRuntimeState>();
	
	public final Map<SMLRuntimeState, Transition> move = new HashMap<SMLRuntimeState, Transition>();

	private AttractorStrategy() {
		
	}
	
	protected AttractorStrategy(Set<SMLRuntimeState> _targetStates, Set<SMLRuntimeState> subGraph, Player player) {
		targetStates.addAll(_targetStates);
		targetStates.retainAll(subGraph);
		attractorStates.addAll(targetStates);
		
		Set<SMLRuntimeState> newAttractorStates = new HashSet<SMLRuntimeState>();
		
		// calculate attractor states via backtracking and remember moves/transitions
		do {
			attractorStates.addAll(newAttractorStates);
			newAttractorStates.clear();
			
			for (SMLRuntimeState smlRuntimeState : attractorStates) {
				for (Transition transition : smlRuntimeState.getIncomingTransition()) {
					SMLRuntimeState predState = (SMLRuntimeState) transition.getSourceState();
					
					if (!subGraph.contains(predState)){
						continue;
					}
					if (attractorStates.contains(predState)) {
						continue;
					}
					if (!AttractorCalculator.isAttractor(predState, subGraph, attractorStates, player)) {
						continue;
					}
					
					newAttractorStates.add(predState);
					if (SynthesisUtil.isControllable(transition, player)) {
						move.put(predState, transition);
					}
				}
			}
			
		} while(!newAttractorStates.isEmpty());

		// add moves for controllable target states (might be required if all goals are fulfilled at the same time)
		outer:
		for(SMLRuntimeState smlRuntimeState : targetStates) {
			if(SynthesisUtil.isControllable(smlRuntimeState, player) && move.get(smlRuntimeState) == null) {
				for (Transition transition : smlRuntimeState.getOutgoingTransition()) {
					if(attractorStates.contains(transition.getTargetState())) {
						move.put(smlRuntimeState, transition);
						continue outer;
					}
				}
			}
		}		
	}
	
	public static class Factory {
		public AttractorStrategy create() {
			return new AttractorStrategy();
		}
		
		public AttractorStrategy create(Set<SMLRuntimeState> targetStates, Set<SMLRuntimeState> subGraph, Player player) {
			return new AttractorStrategy(targetStates, subGraph, player);
		}
	}
}
