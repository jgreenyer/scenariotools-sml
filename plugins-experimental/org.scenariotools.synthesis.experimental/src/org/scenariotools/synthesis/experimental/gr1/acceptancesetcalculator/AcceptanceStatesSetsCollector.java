/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     
 * Author:
 * 		Joel Greenyer
 */
package org.scenariotools.synthesis.experimental.gr1.acceptancesetcalculator;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.synthesis.experimental.gr1.util.SynthesisUtil;

public class AcceptanceStatesSetsCollector {
	private final Map<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> assumptionScenarioAndRoleBindingsTo_NON_ACCEPTANCE_StatesMap = new HashMap<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>>();
	private final Map<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> guaranteeScenarioAndRoleBindingsTo_NON_ACCEPTANCE_StatesMap = new HashMap<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>>();

	//TODO: for more performance: rework attractor calculation to use non-acceptance states instead of acceptance states (and update any code in between this class and attractor calculation accordingly)
	public final Map<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> assumptionScenarioAndRoleBindingsTo_ACCEPTANCE_StatesMap = new HashMap<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>>();
	public final Map<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> guaranteeScenarioAndRoleBindingsTo_ACCEPTANCE_StatesMap = new HashMap<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>>();

	// create an acceptance set for visiting infinitely often states where
	// the system waits for the environment
	public final Set<SMLRuntimeState> waitingForEnvironmentAcceptanceStateSet = new HashSet<SMLRuntimeState>();
	
	public final Set<SMLRuntimeState> assumptionNonSafetyViolatingStates = new HashSet<SMLRuntimeState>();
	public final Set<SMLRuntimeState> guaranteeNonSafetyViolatingStates = new HashSet<SMLRuntimeState>();
	
	private final Set<SMLRuntimeState> exploredStates = new HashSet<SMLRuntimeState>();

	public AcceptanceStatesSetsCollector(SMLRuntimeStateGraph smlRuntimeStateGraph) {
		update(smlRuntimeStateGraph.getStates());
	}

	public AcceptanceStatesSetsCollector(Set<SMLRuntimeState> graphStatesToSearchWithin) {
		update(graphStatesToSearchWithin);
	}
	
	public void update(Collection<SMLRuntimeState> newStates) {
		exploredStates.addAll(newStates);
		
		assumptionScenarioAndRoleBindingsTo_NON_ACCEPTANCE_StatesMap.clear();
		guaranteeScenarioAndRoleBindingsTo_NON_ACCEPTANCE_StatesMap.clear();		
		
		for(SMLRuntimeState state : newStates) {
			activeScenarioLoop:
			for(ActiveScenario activeScenario : state.getActiveScenarios()) {
				if(!activeScenario.isInRequestedState())
					continue;
				
				Scenario scenario = activeScenario.getScenario();
				
				Map<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> theMap = null;
				switch(scenario.getKind()) {
				case ASSUMPTION:
					theMap = assumptionScenarioAndRoleBindingsTo_NON_ACCEPTANCE_StatesMap;
					break;
				case GUARANTEE:
					theMap = guaranteeScenarioAndRoleBindingsTo_NON_ACCEPTANCE_StatesMap;
					break;
				default:
					continue activeScenarioLoop;
				}
				
				ScenarioAndRoleBindingsKey key = new ScenarioAndRoleBindingsKey(scenario, activeScenario.getRoleBindings());
				Set<SMLRuntimeState> targetStates = theMap.get(key);
				
				if(targetStates == null) {
					targetStates = new HashSet<SMLRuntimeState>();
					theMap.put(key, targetStates);
				}
				
				targetStates.add(state);
			}
			
			if(stateWaitsOnlyForEnvironmentEvents(state))
				waitingForEnvironmentAcceptanceStateSet.add(state);
			if(!state.isSafetyViolationOccurredInAssumptions())
				assumptionNonSafetyViolatingStates.add(state);
			if(!state.isSafetyViolationOccurredInGuarantees())
				guaranteeNonSafetyViolatingStates.add(state);
		}
		
		updateMap(guaranteeScenarioAndRoleBindingsTo_NON_ACCEPTANCE_StatesMap, guaranteeScenarioAndRoleBindingsTo_ACCEPTANCE_StatesMap, newStates);
		updateMap(assumptionScenarioAndRoleBindingsTo_NON_ACCEPTANCE_StatesMap, assumptionScenarioAndRoleBindingsTo_ACCEPTANCE_StatesMap, newStates);
	}

	private boolean stateWaitsOnlyForEnvironmentEvents(SMLRuntimeState state) {
		if(state.getOutgoingTransition().size() > 0 && !SynthesisUtil.isControllable(state.getOutgoingTransition().get(0)))
			return true;
		
		return false;
	}
	
	private void updateMap(Map<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> source, Map<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> target, Collection<SMLRuntimeState> newStates) {
		for(Entry<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> entry : source.entrySet()) {
			ScenarioAndRoleBindingsKey key = entry.getKey();
			Set<SMLRuntimeState> acceptingStates = target.get(key);
			
			if(acceptingStates == null) {
				acceptingStates = new HashSet<SMLRuntimeState>(exploredStates);
				acceptingStates.removeAll(entry.getValue());
				target.put(key, acceptingStates);
			} else {
				Set<SMLRuntimeState> newAcceptingStates = new HashSet<SMLRuntimeState>(newStates);
				newAcceptingStates.removeAll(entry.getValue());
				acceptingStates.addAll(newAcceptingStates);
			}
		}
	}
	
	public AcceptanceSets getGuaranteeAcceptanceStateSets() {
		return new AcceptanceSets(this, true);
	}
	
	public AcceptanceSets getAssumptionAcceptanceStateSets() {
		return new AcceptanceSets(this, false);		
	}
	
	public static class AcceptanceSets implements Iterable<Set<SMLRuntimeState>> {
		private AcceptanceStatesSetsCollector collector;
		private boolean isGuarantee;
		
		private AcceptanceSets(AcceptanceStatesSetsCollector collector, boolean isGuarantee) {
			this.collector = collector;
			this.isGuarantee = isGuarantee;
		}
		
		@Override
		public Iterator<Set<SMLRuntimeState>> iterator() {
			if(isGuarantee) {
				return new AcceptanceSetsIterator(collector.guaranteeScenarioAndRoleBindingsTo_ACCEPTANCE_StatesMap, collector.guaranteeNonSafetyViolatingStates, collector.waitingForEnvironmentAcceptanceStateSet);
			} else {
				return new AcceptanceSetsIterator(collector.assumptionScenarioAndRoleBindingsTo_ACCEPTANCE_StatesMap, collector.assumptionNonSafetyViolatingStates);
			}
		}
		
		public Set<Set<SMLRuntimeState>> asSet() {
			Set<Set<SMLRuntimeState>> result = new HashSet<Set<SMLRuntimeState>>();
			
			for(Set<SMLRuntimeState> set : this) {
				result.add(set);
			}
			
			return result;
		}
		
		private static class AcceptanceSetsIterator implements Iterator<Set<SMLRuntimeState>> {
			private int state = 0;
			private Iterator<Set<SMLRuntimeState>> mapIterator;
			private Set<SMLRuntimeState> first;
			private Set<SMLRuntimeState> second;

			private AcceptanceSetsIterator(Map<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> theMap, Set<SMLRuntimeState> first)  {
				this(theMap, first, null);
			}			
			
			private AcceptanceSetsIterator(Map<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> theMap, Set<SMLRuntimeState> first, Set<SMLRuntimeState> second)  {
				mapIterator = theMap.values().iterator();
				this.first = first;
				this.second = second;
			}
			
			@Override
			public boolean hasNext() {
				return state < 2;
			}

			@Override
			public Set<SMLRuntimeState> next() {
				if(mapIterator.hasNext())
					return mapIterator.next();
				
				switch(state) {
				case 0:
					state += (second == null ? 2 : 1);
					return first;
				case 1:
					state += 1;
					return second;
				default:
					return null;
				}
			}
		}
	}
}
