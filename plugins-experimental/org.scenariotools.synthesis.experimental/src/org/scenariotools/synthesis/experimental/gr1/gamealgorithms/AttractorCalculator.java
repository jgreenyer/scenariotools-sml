/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     
 * Author:
 * 		Joel Greenyer
 */
package org.scenariotools.synthesis.experimental.gr1.gamealgorithms;

import java.util.HashSet;
import java.util.Set;

import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.Transition;
import org.scenariotools.synthesis.experimental.gr1.util.SynthesisUtil;

public class AttractorCalculator {

	public static Set<SMLRuntimeState> calculateAttractorStatesForPlayerWithinGraphForTargetStates(Player playerKind,
			Set<SMLRuntimeState> graphStatesToSearchWithin, Set<SMLRuntimeState> statesToBeReachableSet) {

		final Set<SMLRuntimeState> attractorSet = new HashSet<SMLRuntimeState>(statesToBeReachableSet);
		final Set<SMLRuntimeState> newAttractorStates = new HashSet<SMLRuntimeState>();
		
		do {
			attractorSet.addAll(newAttractorStates);
			newAttractorStates.clear();
			
			for (SMLRuntimeState smlRuntimeState : attractorSet) {
				for (Transition transition : smlRuntimeState.getIncomingTransition()) {
					SMLRuntimeState predState = (SMLRuntimeState) transition.getSourceState();
					if (graphStatesToSearchWithin.contains(predState)
							&& !attractorSet.contains(predState)
							&& isAttractor(predState, graphStatesToSearchWithin, attractorSet, playerKind)){
						newAttractorStates.add(predState);
					}
				}
			}
			
		}while (!newAttractorStates.isEmpty());
		
		return attractorSet;

	}


	public static boolean isAttractor(SMLRuntimeState state, Set<SMLRuntimeState> graphStatesToSearchWithin, Set<SMLRuntimeState> attractorSet, Player player) {
		boolean isAttractor = (player == Player.SYSTEM && isSystemAttractorState(state, graphStatesToSearchWithin, attractorSet)
				|| player == Player.ENVIRONMENT
				&& isEnvironmentAttractorState(state, graphStatesToSearchWithin, attractorSet));
		
		return isAttractor;
		
	}


	private static boolean isEnvironmentAttractorState(SMLRuntimeState state, Set<SMLRuntimeState> graphStatesToSearchWithin, Set<SMLRuntimeState> attractorSet) {
		boolean atLeastOneTransitionsLeadsToAttractorState = false;
		for (Transition outgoingTransition : state.getOutgoingTransition()) {
			if (!graphStatesToSearchWithin.contains(outgoingTransition.getTargetState()))
				continue;
			if (SynthesisUtil.isControllable(outgoingTransition)
					&& !attractorSet.contains(outgoingTransition.getTargetState())) {
				// one controllable transition leads to non-attractor state
				return false;
			}
			if (!atLeastOneTransitionsLeadsToAttractorState
					&& attractorSet.contains(outgoingTransition.getTargetState())) {
				atLeastOneTransitionsLeadsToAttractorState = true;
			}
		}
		return atLeastOneTransitionsLeadsToAttractorState;
	}

	private static boolean isSystemAttractorState(SMLRuntimeState state, Set<SMLRuntimeState> graphStatesToSearchWithin, Set<SMLRuntimeState> attractorSet) {
		
		boolean hasOutgoingUncontrollableTransitions = false;
		boolean allUncontrollableTransitionsLeadToAttractorState = true;
		
		for (Transition outgoingTransition : state.getOutgoingTransition()) {
			if (!graphStatesToSearchWithin.contains(outgoingTransition.getTargetState()))
				continue;
			if (SynthesisUtil.isControllable(outgoingTransition)) {
				if (attractorSet.contains(outgoingTransition.getTargetState()))
					return true;
			} else {
				hasOutgoingUncontrollableTransitions = true;
				if (allUncontrollableTransitionsLeadToAttractorState
						&& !attractorSet.contains(outgoingTransition.getTargetState())) {
					allUncontrollableTransitionsLeadToAttractorState = false;
				}
			}
		}
		return hasOutgoingUncontrollableTransitions && allUncontrollableTransitionsLeadToAttractorState;
	}

	
}
