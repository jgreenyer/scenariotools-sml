/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     
 * Author:
 * 		Joel Greenyer
 *		Daniel Gritzner
 */
package org.scenariotools.synthesis.experimental.gr1.strategyextraction;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.util.SMLRuntimeStateUtil;
import org.scenariotools.sml.runtime.util.TransitionUtil;
import org.scenariotools.synthesis.experimental.gr1.util.SynthesisUtil;
import org.scenariotools.sml.runtime.Transition;

public class StrategyExtractor {
	private List<Entry<String, AttractorStrategy>> goals;
	private int goalIndex;
	private Map<String, SMLRuntimeState> memorylessStrategyStatesMap;
	private SMLRuntimeStateGraph memorylessStrategyGraph;
	
	public static SMLRuntimeStateGraph extractStrategy(SMLRuntimeStateGraph stateGraph, Map<String, AttractorStrategy> strategies) {
		return (new StrategyExtractor(stateGraph, strategies)).memorylessStrategyGraph;
	}

	private StrategyExtractor(SMLRuntimeStateGraph stateGraph, Map<String, AttractorStrategy> strategies) {
		goals = new ArrayList<Entry<String, AttractorStrategy>>(strategies.size());
		goals.addAll(strategies.entrySet());
		goalIndex = 0;
		memorylessStrategyStatesMap = new HashMap<String, SMLRuntimeState>();
		memorylessStrategyGraph = RuntimeFactory.eINSTANCE.createSMLRuntimeStateGraph();
		memorylessStrategyGraph.setConfiguration(stateGraph.getConfiguration());
		
		SMLRuntimeState startState = stateGraph.getStartState();
		memorylessStrategyGraph.setStartState(createNewStrategyState(startState));

		Stack<Map.Entry<Integer, SMLRuntimeState>> stack = new Stack<Map.Entry<Integer,SMLRuntimeState>>();
		stack.push(new AbstractMap.SimpleEntry<Integer, SMLRuntimeState>(goalIndex, startState));
		
		while(!stack.isEmpty()) {
			Entry<Integer, SMLRuntimeState> theEntry = stack.pop();
			goalIndex = theEntry.getKey();
			SMLRuntimeState state = theEntry.getValue();
			
			AttractorStrategy strategy = goals.get(goalIndex).getValue();
			Set<Transition> moveTransitions = new HashSet<Transition>();
			
			Transition move = strategy.move.get(state);
			if(move != null) {
				// controllable
				moveTransitions.add(move);
			} else {
				// uncontrollable
				moveTransitions.addAll(state.getOutgoingTransition());
			}
			
			state = memorylessStrategyStatesMap.get(getKeyString(state));
			
			for(Transition t : moveTransitions) {
				goalIndex = theEntry.getKey();
				
				SMLRuntimeState targetState = (SMLRuntimeState)t.getTargetState();
				for(int i = 0; i < goals.size(); i++) {
					int nextIndex = (goalIndex+i) % goals.size();
					AttractorStrategy toFulfillNext = goals.get(nextIndex).getValue();
					if(toFulfillNext.targetStates.contains(targetState))
						continue;

					// found next unfulfilled condition
					goalIndex = nextIndex;
					break;
				}
				
				if(createStrategyTransition(state, t))
					stack.push(new AbstractMap.SimpleEntry<Integer, SMLRuntimeState>(goalIndex, targetState));
			}			
		}
	}
	
	private boolean createStrategyTransition(SMLRuntimeState sourceState, Transition moveTransition) {
		Transition transition = RuntimeFactory.eINSTANCE.createTransition();

		transition.setSourceState(sourceState);
		transition.setEvent(moveTransition.getEvent());
		TransitionUtil.setControllableTransition(transition, SynthesisUtil.isControllable(moveTransition));

		SMLRuntimeState targetState = (SMLRuntimeState)moveTransition.getTargetState();
		String key = getKeyString(targetState);
		
		SMLRuntimeState memorylessStrategyTargetState = memorylessStrategyStatesMap.get(key);
		boolean createdNewState = false;
		
		if (memorylessStrategyTargetState == null) {
			memorylessStrategyTargetState = createNewStrategyState(targetState);
			createdNewState = true;
		}
		
		transition.setTargetState(memorylessStrategyTargetState);
		
		return createdNewState;
	}
	
	private SMLRuntimeState createNewStrategyState(SMLRuntimeState state){
		String key = getKeyString(state);
		
		SMLRuntimeState newState = RuntimeFactory.eINSTANCE.createSMLRuntimeState();
		memorylessStrategyGraph.getStates().add(newState);
		memorylessStrategyStatesMap.put(key, newState);
		
		String safetyViolationString = "";
		if (state.isSafetyViolationOccurredInAssumptions()) {
			safetyViolationString = "\nSAFETY-VIOLATION occurred in assumptions";
		}else if (state.isSafetyViolationOccurredInGuarantees()) {
			safetyViolationString = "\nSAFETY-VIOLATION occurred in guarantees";
		}
		SMLRuntimeStateUtil.setPassedIndex(newState, key+safetyViolationString);
		
		newState.setObjectSystem(state.getObjectSystem());
		
		return newState;
	}
	
	private String getKeyString(SMLRuntimeState state) {
		return SMLRuntimeStateUtil.getPassedIndex(state) + " " + goals.get(goalIndex).getKey();
	}	
}
