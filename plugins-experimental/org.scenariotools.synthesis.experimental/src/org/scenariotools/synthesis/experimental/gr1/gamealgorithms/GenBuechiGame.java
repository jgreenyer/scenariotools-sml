/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     
 * Author:
 * 		Joel Greenyer
 */
package org.scenariotools.synthesis.experimental.gr1.gamealgorithms;

import java.util.HashSet;
import java.util.Set;

import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.synthesis.experimental.gr1.util.SynthesisUtil;
import org.scenariotools.synthesis.experimental.gr1.gamealgorithms.AttractorCalculator;
import org.scenariotools.synthesis.experimental.gr1.gamealgorithms.Player;

public class GenBuechiGame {

	public static Set<SMLRuntimeState> buechiGameBasic(Player player, Set<SMLRuntimeState> retainedStates,
			Set<Set<SMLRuntimeState>> acceptanceStates) {

		Set<SMLRuntimeState> opponentDominion = new HashSet<SMLRuntimeState>();

		do {
			opponentDominion.clear();

			for (Set<SMLRuntimeState> scceptingStates : acceptanceStates) {

				Set<SMLRuntimeState> playerAttractorForAcceptingStates = AttractorCalculator
						.calculateAttractorStatesForPlayerWithinGraphForTargetStates(player, retainedStates,
								scceptingStates);

				opponentDominion.addAll(retainedStates);
				opponentDominion.removeAll(playerAttractorForAcceptingStates);

				if (!opponentDominion.isEmpty())
					break;
			}

			if (!opponentDominion.isEmpty()) {
				opponentDominion = AttractorCalculator.calculateAttractorStatesForPlayerWithinGraphForTargetStates(
						SynthesisUtil.opponentOf(player), retainedStates, opponentDominion);

				retainedStates.removeAll(opponentDominion);

				for (Set<SMLRuntimeState> acceptanceStateSet : acceptanceStates) {
					acceptanceStateSet.removeAll(opponentDominion);
				}
			}

		} while (!opponentDominion.isEmpty());

		return retainedStates;
	}

}
