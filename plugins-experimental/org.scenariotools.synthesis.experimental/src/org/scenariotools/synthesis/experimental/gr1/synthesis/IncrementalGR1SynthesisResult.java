package org.scenariotools.synthesis.experimental.gr1.synthesis;

import org.scenariotools.synthesis.ResultMetaData;

public class IncrementalGR1SynthesisResult extends GR1SynthesisResult {
	@ResultMetaData(name="Number of game solving / exploration increments", group="Incremental GR(1) Synthesis", position=1)
	public int numberOfGR1GameSolvingIncrements;
	
	@ResultMetaData(name="How often did incremental game solving switch sides (environment<->system)", group="Incremental GR(1) Synthesis", position=2)
	public int howOftenSwitchedSides;
}
