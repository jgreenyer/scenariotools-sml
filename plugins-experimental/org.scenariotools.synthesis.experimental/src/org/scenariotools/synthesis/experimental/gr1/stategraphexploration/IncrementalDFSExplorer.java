/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     
 * Author:
 * 		Joel Greenyer
 */
package org.scenariotools.synthesis.experimental.gr1.stategraphexploration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import org.eclipse.core.runtime.IProgressMonitor;
import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.util.SMLRuntimeStateUtil;
import org.scenariotools.sml.runtime.util.TransitionUtil;
import org.scenariotools.synthesis.experimental.gr1.gamealgorithms.Player;
import org.scenariotools.sml.runtime.Transition;

public class IncrementalDFSExplorer {

	private final SMLRuntimeStateGraph smlRuntimeStateGraph;
	private final Set<SMLRuntimeState> statesExplored = new HashSet<SMLRuntimeState>();
	private final IProgressMonitor monitor;

	private SMLRuntimeState lastStateExplored;

	private int stateCounter = 0;
	private int transitionCounter = 0;
	
	private Player lastPlayer;
	
	public IncrementalDFSExplorer(SMLRuntimeStateGraph smlRuntimeStateGraph, IProgressMonitor monitor){
		this.smlRuntimeStateGraph = smlRuntimeStateGraph;
		this.monitor = monitor;
	}
	
	public void initialExplorationUntilFirstCycleIsFound() {
		exploreUntilFirstLoopIsFound((SMLRuntimeState) smlRuntimeStateGraph.getStartState());
	}
	
	public boolean exploreNextCyclesForSomeStateInGivenScopeSet(Set<SMLRuntimeState> scopeStateSet, int growth, boolean growthAsNumCycles, Player player, boolean continueWithLastStateExplored) {
		
		Set<SMLRuntimeState> exploredScopeSetStatesFromWhereToFindUnexploredSuccessor = new HashSet<SMLRuntimeState>(scopeStateSet);
		
		if(exploredScopeSetStatesFromWhereToFindUnexploredSuccessor.isEmpty()) // nothing to do. 
			return false;
		
		int numStatesPreGrowth = smlRuntimeStateGraph.getStates().size();

		SMLRuntimeState stateFromWhereToExplore = null;
		
		outer:
		while(!exploredScopeSetStatesFromWhereToFindUnexploredSuccessor.isEmpty() && !monitor.isCanceled()) {
			
			SMLRuntimeState someExploredStateInScope;
			
			if(continueWithLastStateExplored
					&& player == lastPlayer
					&& exploredScopeSetStatesFromWhereToFindUnexploredSuccessor.contains(lastStateExplored)) {
				someExploredStateInScope = lastStateExplored;
			}else {
				someExploredStateInScope = exploredScopeSetStatesFromWhereToFindUnexploredSuccessor.iterator().next();
			}
			
			
			continueWithLastStateExplored = false;
			
			exploredScopeSetStatesFromWhereToFindUnexploredSuccessor.remove(someExploredStateInScope);

			for (Event enabledEvent : someExploredStateInScope.getEnabledEvents()) {
				if (someExploredStateInScope.getEventToTransitionMap().get(enabledEvent) != null)
					continue;
				boolean isEnabledMessageEventUnontrollable = someExploredStateInScope.getObjectSystem().isEnvironmentMessageEvent(enabledEvent);
				if (isEnabledMessageEventUnontrollable != (player == Player.ENVIRONMENT))
					continue;
				
				Transition outTransition = smlRuntimeStateGraph.generateSuccessor(someExploredStateInScope, enabledEvent);
				TransitionUtil.setPassedIndex(outTransition, String.valueOf(++transitionCounter));
				
				stateFromWhereToExplore = (SMLRuntimeState) outTransition.getTargetState();
				exploredScopeSetStatesFromWhereToFindUnexploredSuccessor.add(stateFromWhereToExplore);
				exploreUntilFirstLoopIsFound(stateFromWhereToExplore);
				
				if(growthAsNumCycles) {
					growth -= 1;
					if(growth == 0)
						break outer;					
				} else {
					if((smlRuntimeStateGraph.getStates().size() - numStatesPreGrowth) >= growth)
						break outer;
				}
			}
		}
		
		lastPlayer = player;
		
		if(stateFromWhereToExplore == null)
			return false; // nothing to do.
		
		return true;
	}
	
	private void exploreUntilFirstLoopIsFound(SMLRuntimeState startState) {

		Stack<TempTransition> stack = new Stack<TempTransition>();
		markStateAsExplored(startState);
		
		stack.addAll(generateTempOutgoingTransitions(startState));
		
		while(!stack.isEmpty() && !monitor.isCanceled()) {
			SMLRuntimeState targetState = (SMLRuntimeState) generateTransitionForTempTransition(stack.pop()).getTargetState();
			if(!statesExplored.contains(targetState)) {
				markStateAsExplored(targetState);
				stack.addAll(generateTempOutgoingTransitions(targetState));
			}else {
				return;
			}
		}
	}

	private void markStateAsExplored(SMLRuntimeState smlRuntimeState) {
		statesExplored.add(smlRuntimeState);
		SMLRuntimeStateUtil.setPassedIndex(smlRuntimeState, String.valueOf(++stateCounter));
		lastStateExplored = smlRuntimeState;
	}

	private List<TempTransition> generateTempOutgoingTransitions(SMLRuntimeState state){
		List<TempTransition> allTempTransitions = new ArrayList<TempTransition>(state.getEnabledEvents().size());
		for (Event event : state.getEnabledEvents()) {
			if(state.getEventToTransitionMap().get(event) == null)
				allTempTransitions.add(new TempTransition(state, event));
		}
		return allTempTransitions;
	}
	
	private Transition generateTransitionForTempTransition(TempTransition tempTransition) {
		Transition transition = smlRuntimeStateGraph.generateSuccessor((SMLRuntimeState)tempTransition.smlRuntimeState, tempTransition.event);
		TransitionUtil.setPassedIndex(transition, String.valueOf(++transitionCounter));
		return transition;
	}
	
	private class TempTransition{
		public final SMLRuntimeState smlRuntimeState;
		public final Event event;

		TempTransition(SMLRuntimeState smlRuntimeState, Event event){
			this.smlRuntimeState = smlRuntimeState;
			this.event = event;
		}
	}
	
}
