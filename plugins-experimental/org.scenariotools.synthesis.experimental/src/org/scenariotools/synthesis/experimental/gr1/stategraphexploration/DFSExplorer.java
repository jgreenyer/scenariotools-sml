/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     
 * Author:
 * 		Joel Greenyer
 */
package org.scenariotools.synthesis.experimental.gr1.stategraphexploration;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import org.eclipse.core.runtime.IProgressMonitor;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.util.SMLRuntimeStateUtil;
import org.scenariotools.sml.runtime.Transition;

public class DFSExplorer {

	private final Set<SMLRuntimeState> exploredStates = new HashSet<SMLRuntimeState>();
	private SMLRuntimeStateGraph smlRuntimeStateGraph;
	private IProgressMonitor monitor;

	private int stateCounter;
	
	public void exploreStateGraph(SMLRuntimeStateGraph smlRuntimeStateGraph, IProgressMonitor monitor) {
		this.smlRuntimeStateGraph = smlRuntimeStateGraph;
		this.monitor = monitor;
		exploredStates.clear();
		stateCounter = 0;
		exploreStateDFS((SMLRuntimeState) smlRuntimeStateGraph.getStartState());		
	}
	
	
	
	private void exploreStateDFS(SMLRuntimeState startSMLRuntimeState) {
	
		Stack<SMLRuntimeState> stack = new Stack<SMLRuntimeState>();
		
		stack.push(startSMLRuntimeState);
		
		while(!stack.isEmpty()) {
			SMLRuntimeState smlRuntimeState = stack.pop();
			
			if(monitor.isCanceled()) return;
			
			if (!exploredStates.contains(smlRuntimeState)) {

				exploredStates.add(smlRuntimeState);
				SMLRuntimeStateUtil.setPassedIndex(smlRuntimeState, String.valueOf(++stateCounter));

				List<Transition> allOutgoingTransitions = smlRuntimeStateGraph
						.generateAllSuccessors((SMLRuntimeState) smlRuntimeState);
				for (Transition transition : allOutgoingTransitions) {
					stack.push((SMLRuntimeState) transition.getTargetState());
				}
			}
		}
	}

}
