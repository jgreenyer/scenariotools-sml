/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     
 * Author:
 * 		Joel Greenyer
 */
package org.scenariotools.synthesis.experimental.gr1.gamealgorithms;

import java.util.HashSet;
import java.util.Set;

import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.synthesis.experimental.gr1.acceptancesetcalculator.AcceptanceStatesSetsCollector;
import org.scenariotools.synthesis.experimental.gr1.synthesis.GR1SynthesisResult;

public class GR1Game {

	
	public static void gr1GameBasic(SMLRuntimeStateGraph smlRuntimeStateGraph, AcceptanceStatesSetsCollector acceptanceStatesSetsCollector, GR1SynthesisResult result) {

		// G1 <- G
		final Set<SMLRuntimeState> retainedStates = new HashSet<SMLRuntimeState>(smlRuntimeStateGraph.getStates());

		// {U1} <- {U}
		final Set<Set<SMLRuntimeState>> guaranteeAcceptanceStateSets = acceptanceStatesSetsCollector.getGuaranteeAcceptanceStateSets().asSet();
		// {L1} <- {L}
		final Set<Set<SMLRuntimeState>> assumptionAcceptanceStateSets = acceptanceStatesSetsCollector.getAssumptionAcceptanceStateSets().asSet();

		Set<SMLRuntimeState> environmentDominion = new HashSet<SMLRuntimeState>();
		
		do {
			environmentDominion.clear();

			for (Set<SMLRuntimeState> guaranteeAcceptingStateSet : guaranteeAcceptanceStateSets) {

				Set<SMLRuntimeState> systemAttractorForGuaranteeAcceptingStates = AttractorCalculator
						.calculateAttractorStatesForPlayerWithinGraphForTargetStates(Player.SYSTEM, retainedStates,
								guaranteeAcceptingStateSet);
				
				environmentDominion = genBuechiGameForEnvironment(retainedStates,
						systemAttractorForGuaranteeAcceptingStates, assumptionAcceptanceStateSets);

				if (!environmentDominion.isEmpty())
					break;
			}
			if (!environmentDominion.isEmpty()) {
				environmentDominion = AttractorCalculator.calculateAttractorStatesForPlayerWithinGraphForTargetStates(
						Player.ENVIRONMENT, retainedStates, environmentDominion);

				retainedStates.removeAll(environmentDominion);

				for (Set<SMLRuntimeState> acceptanceStateSet : guaranteeAcceptanceStateSets) {
					acceptanceStateSet.removeAll(environmentDominion);
				}
				for (Set<SMLRuntimeState> acceptanceStateSet : assumptionAcceptanceStateSets) {
					acceptanceStateSet.removeAll(environmentDominion);
				}
			}

		} while (!environmentDominion.isEmpty());

		result.winningStates = retainedStates;
		result.numWinningStates = result.winningStates.size();
		
		result.losingStates = new HashSet<SMLRuntimeState>(smlRuntimeStateGraph.getStates());
		result.losingStates.removeAll(retainedStates);
		result.numLosingStates = result.losingStates.size();
		
//		result.guaranteeAcceptanceStateSets = guaranteeAcceptanceStateSets;
//		result.assumptionAcceptanceStateSets = assumptionAcceptanceStateSets;
	}

	private static Set<SMLRuntimeState> genBuechiGameForEnvironment(Set<SMLRuntimeState> retainedStates,
			Set<SMLRuntimeState> systemAttractorForGuaranteeAcceptingStates,
			Set<Set<SMLRuntimeState>> assumptionAcceptanceStateSets) {

		Set<SMLRuntimeState> retainedStatesWOAttractorForGuaranteeAcceptingStates = new HashSet<SMLRuntimeState>(
				retainedStates);
		retainedStatesWOAttractorForGuaranteeAcceptingStates.removeAll(systemAttractorForGuaranteeAcceptingStates);

		Set<Set<SMLRuntimeState>> reducedAssumptionAcceptanceStateSets = new HashSet<Set<SMLRuntimeState>>();

		for (Set<SMLRuntimeState> assumptionAcceptanceStates : assumptionAcceptanceStateSets) {
			Set<SMLRuntimeState> reducedAcceptanceSet = new HashSet<SMLRuntimeState>(
					assumptionAcceptanceStates);
			reducedAcceptanceSet.removeAll(systemAttractorForGuaranteeAcceptingStates);
			reducedAssumptionAcceptanceStateSets
					.add(reducedAcceptanceSet);
		}

		return GenBuechiGame.buechiGameBasic(Player.ENVIRONMENT, retainedStatesWOAttractorForGuaranteeAcceptingStates,
				reducedAssumptionAcceptanceStateSets);

	}

}
