/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     
 * Author:
 * 		Joel Greenyer
 *		Daniel Gritzner
 */
package org.scenariotools.synthesis.experimental.gr1.strategyextraction;

import java.util.Map.Entry;
import java.util.Map;
import java.util.Set;

import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.util.SMLRuntimeStateUtil;
import org.scenariotools.sml.runtime.util.TransitionUtil;
import org.scenariotools.sml.runtime.Transition;

public class SMLRuntimeStateGraphStrategyInformationAnnotator {
	
	public static void annotateAttractorStrategyInformation(Map<String, AttractorStrategy> strategies, boolean systemWinning) {
		for (Entry<String, AttractorStrategy> entry : strategies.entrySet()) {
			for (Transition transition : entry.getValue().move.values()) {
				TransitionUtil.appendAttractorStrategyInformation(transition, entry.getKey() + "\n");
				annotateTransitionAsStrategyOrCounterStrategyMove(transition, systemWinning);
			}
		}
	}

	private static void annotateTransitionAsStrategyOrCounterStrategyMove(Transition transition, boolean isWinning) {
		if (isWinning) {
			TransitionUtil.setStrategyTransition(transition, true);
		}else {
			TransitionUtil.setCounterStrategyTransition(transition, true);
		}
	}

	public static void annotateStrategyOrCounterStrategyStates(Set<SMLRuntimeState> winningStates, Set<SMLRuntimeState> losingStates, boolean systemWinning) {
		if (systemWinning) {
			for (SMLRuntimeState smlRuntimeState : winningStates) {
				SMLRuntimeStateUtil.setStrategyState(smlRuntimeState, true);
			}
		}else {
			for (SMLRuntimeState smlRuntimeState : losingStates) {
				SMLRuntimeStateUtil.setCounterStrategyState(smlRuntimeState, true);
			}
		}
	}

	

}
