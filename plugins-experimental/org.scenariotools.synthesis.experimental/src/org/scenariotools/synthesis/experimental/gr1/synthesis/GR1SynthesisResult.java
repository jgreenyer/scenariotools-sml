package org.scenariotools.synthesis.experimental.gr1.synthesis;

import java.util.Set;

import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.synthesis.ResultMetaData;
import org.scenariotools.synthesis.SynthesisResult;

public class GR1SynthesisResult {
	public Set<SMLRuntimeState> winningStates;
	public Set<SMLRuntimeState> losingStates;

//	public Set<Set<SMLRuntimeState>> guaranteeAcceptanceStateSets;
//	public Set<Set<SMLRuntimeState>> assumptionAcceptanceStateSets;
	
	@ResultMetaData(name="number of winning states", group=SynthesisResult.STATEGRAPH_GROUP_NAME, position=1)
	public long numWinningStates;
	
	@ResultMetaData(name="number of losing states", group=SynthesisResult.STATEGRAPH_GROUP_NAME, position=2)
	public long numLosingStates;
	
	@ResultMetaData(name="state graph exploration [ms]", group="GR(1) Synthesis Time Measurements", position=1)
	public long timeMillisForStateGraphExploration;

	@ResultMetaData(name="GR(1) game solving [ms]", group="GR(1) Synthesis Time Measurements", position=3)
	public long timeMillisForGameSolvingOnGraph;

	@ResultMetaData(name="acceptance sets calculation [ms]", group="GR(1) Synthesis Time Measurements", position=2)
	public long timeForAcceptanceSetCalculation;
	
	@ResultMetaData(name="total [ms]", group="GR(1) Synthesis Time Measurements", position=4)
	public long totalTime;
	
	@ResultMetaData(name="time [ms]", group="Strategy Extraction", position=1)
	public long timeStrategyExtraction = 0;
}
