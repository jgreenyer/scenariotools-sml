/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     
 * Author:
 * 		Joel Greenyer
 *		Daniel Gritzner
 */
package org.scenariotools.synthesis.experimental.gr1.strategyextraction;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.synthesis.experimental.gr1.acceptancesetcalculator.AcceptanceStatesSetsCollector;
import org.scenariotools.synthesis.experimental.gr1.acceptancesetcalculator.ScenarioAndRoleBindingsKey;
import org.scenariotools.synthesis.experimental.gr1.acceptancesetcalculator.AcceptanceStatesSetsCollector.AcceptanceSets;
import org.scenariotools.synthesis.experimental.gr1.gamealgorithms.Player;
import org.scenariotools.synthesis.experimental.gr1.util.GR1SynthesisToStringUtil;
import org.scenariotools.synthesis.experimental.gr1.util.SynthesisUtil;
import org.scenariotools.sml.runtime.Transition;

public class AttractorStrategyCalculator {
	private Set<SMLRuntimeState> winningStates;
	private Set<SMLRuntimeState> losingStates;
	private AttractorStrategy.Factory strategyFactory;
	private Map<String, AttractorStrategy> strategies;
	
	public static Map<String, AttractorStrategy> calculate(boolean strategyExists, Set<SMLRuntimeState> winningStates, Set<SMLRuntimeState> losingStates) {
		return calculate(strategyExists, winningStates, losingStates, new AttractorStrategy.Factory());
	}
	
	public static Map<String, AttractorStrategy> calculate(boolean strategyExists, Set<SMLRuntimeState> winningStates, Set<SMLRuntimeState> losingStates, AttractorStrategy.Factory strategyFactory) {
		return new AttractorStrategyCalculator(strategyExists, winningStates, losingStates, strategyFactory).strategies;		
	}
	
	private AttractorStrategyCalculator(boolean strategyExists, Set<SMLRuntimeState> winningStates, Set<SMLRuntimeState> losingStates, AttractorStrategy.Factory strategyFactory) {
		this.winningStates = winningStates;
		this.losingStates = losingStates;
		this.strategyFactory = strategyFactory;
		strategies = new HashMap<String, AttractorStrategy>();
		
		if(strategyExists) {
			calculateSystemAttractorStrategies();
		} else {
			calculateEnvironmentAttractorStrategies();
		}
	}
	
	private void calculateSystemAttractorStrategies() {
		AcceptanceStatesSetsCollector acceptanceStatesSetsCollector = new AcceptanceStatesSetsCollector(winningStates);
		
		// calculate strategies to fulfill guarantees 
		for (Entry<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> entry : acceptanceStatesSetsCollector.guaranteeScenarioAndRoleBindingsTo_ACCEPTANCE_StatesMap.entrySet()) {
			String key = GR1SynthesisToStringUtil.calculateStringForScenarioAndRoleBindingsKey(entry.getKey());
			AttractorStrategy attractorStrategy = strategyFactory.create(entry.getValue(), winningStates, Player.SYSTEM);
			strategies.put(key, attractorStrategy);
		}

		AttractorStrategy aStrategy = strategyFactory.create(acceptanceStatesSetsCollector.guaranteeNonSafetyViolatingStates, winningStates, Player.SYSTEM);
		strategies.put("avoid guarantee safety violation", aStrategy);
		
		aStrategy = strategyFactory.create(acceptanceStatesSetsCollector.waitingForEnvironmentAcceptanceStateSet, winningStates, Player.SYSTEM);
		strategies.put("reach env-controlled state", aStrategy);
		
		// add strategies to violate assumptions to each previously calculated strategy
		for (Entry<String, AttractorStrategy> entry : strategies.entrySet()) {
			Map<String, AttractorStrategy> temp = new HashMap<String, AttractorStrategy>();
			temp.put(entry.getKey(), entry.getValue());
			
			Set<SMLRuntimeState> remainingGraph = new HashSet<SMLRuntimeState>(winningStates);
			remainingGraph.removeAll(entry.getValue().attractorStates);
			
			fulfillConditionsWhileViolatingAtLeastOneOpponentCondition(temp, acceptanceStatesSetsCollector.getAssumptionAcceptanceStateSets(), remainingGraph, Player.SYSTEM);
		}
	}

	private void calculateEnvironmentAttractorStrategies() {
		AcceptanceStatesSetsCollector acceptanceStatesSetsCollector = new AcceptanceStatesSetsCollector(losingStates);

		for (Entry<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> entry : acceptanceStatesSetsCollector.assumptionScenarioAndRoleBindingsTo_ACCEPTANCE_StatesMap.entrySet()) {
			String key = GR1SynthesisToStringUtil.calculateStringForScenarioAndRoleBindingsKey(entry.getKey());
			AttractorStrategy strategy = strategyFactory.create();
			strategy.targetStates.addAll(entry.getValue());
			strategies.put(key, strategy);
		}
		AttractorStrategy noSafetyViolationStrategy = strategyFactory.create();
		noSafetyViolationStrategy.targetStates.addAll(acceptanceStatesSetsCollector.assumptionNonSafetyViolatingStates); 
		strategies.put("avoid assumption safety violation", noSafetyViolationStrategy);

		fulfillConditionsWhileViolatingAtLeastOneOpponentCondition(strategies, acceptanceStatesSetsCollector.getGuaranteeAcceptanceStateSets(), losingStates, Player.ENVIRONMENT);
	}
	
	private final static AttractorStrategy.Factory unoptimizedStrategyFactory = new AttractorStrategy.Factory(); // when violating opponent, optimization becomes irrelevant: we want to efficiently achieve our goals, not efficiently violate our opponent
	private static void fulfillConditionsWhileViolatingAtLeastOneOpponentCondition(Map<String, AttractorStrategy> conditionsToFulfill, AcceptanceSets conditionsToViolate, Set<SMLRuntimeState> gameGraph, Player player) {
		Set<SMLRuntimeState> remainingGraph = new HashSet<SMLRuntimeState>(gameGraph);
		Player opponent = SynthesisUtil.opponentOf(player);
		
		outer:
		while(!remainingGraph.isEmpty()) {
			for(Set<SMLRuntimeState> conditionToViolate : conditionsToViolate) {
				AttractorStrategy opponentStrategy = unoptimizedStrategyFactory.create(conditionToViolate, remainingGraph, opponent);
				Set<SMLRuntimeState> subGraph = new HashSet<SMLRuntimeState>(remainingGraph);
				subGraph.removeAll(opponentStrategy.attractorStates);
				
				Set<SMLRuntimeState> allWinningSubset = null;
				Map<String, AttractorStrategy> tempStrategies = new HashMap<String, AttractorStrategy>();
	
				if(player == Player.ENVIRONMENT) {
					// find the subset in which all assumptions hold AND at the current guarantee under test is violated
					for (Entry<String, AttractorStrategy> entry : conditionsToFulfill.entrySet()) {
						String key = entry.getKey();
						
						AttractorStrategy tempStrategy = unoptimizedStrategyFactory.create(entry.getValue().targetStates, subGraph, player); 
						tempStrategies.put(key, tempStrategy);
						
						if(allWinningSubset == null) {
							allWinningSubset = new HashSet<SMLRuntimeState>(tempStrategy.attractorStates);
						} else {
							allWinningSubset.retainAll(tempStrategy.attractorStates);
						}
					}					
				} else {
					// we just want to violate the current assumption under test anyway
					allWinningSubset = subGraph;
					tempStrategies = conditionsToFulfill;
				}

				if(!allWinningSubset.isEmpty()) {
					AttractorStrategy strategyToReachCommonSubset = unoptimizedStrategyFactory.create(allWinningSubset, remainingGraph, player); 
					for(SMLRuntimeState state : strategyToReachCommonSubset.attractorStates) {
						if(!SynthesisUtil.isControllable(state, player))
							continue;
						
						for (Entry<String, AttractorStrategy> entry : conditionsToFulfill.entrySet()) {
							String key = entry.getKey();
							AttractorStrategy tempStrategy = tempStrategies.get(key);
							AttractorStrategy conditionStrategy = entry.getValue();
							
//							conditionStrategy.attractorStates.addAll(tempStrategy.attractorStates);
							Transition candidate = tempStrategy.move.get(state);
							if(candidate == null)
								candidate = strategyToReachCommonSubset.move.get(state);
							conditionStrategy.move.put(state, candidate);
						}						
					}
					
					remainingGraph.removeAll(strategyToReachCommonSubset.attractorStates);
					continue outer;
				}
			}
			
			break; // remainingGraph can not be reduced further but it is only an unreachable subgraph anyway 
		}
	}
}
