/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.xtext.ui.xmi;


import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtext.ui.shared.SharedStateModule;
import org.osgi.framework.BundleContext;
import org.scenariotools.xtext.resources.xmi.XMIRuntimeModule;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.util.Modules;
 
/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin  {
 
 
    // The plug-in ID
    public static final String PLUGIN_ID = "org.scenariotools.xtext.ui.xmi"; //$NON-NLS-1$
 
    // The shared instance
    private static Activator plugin;
 
    private Injector injector;
 
    /**
     * The constructor
     */
    public Activator() {
    }
 
    public Injector getInjector() {
        return injector;
    }
 
    private void initializeEcoreInjector() {
        injector = Guice.createInjector(
                Modules.override(Modules.override(new XMIRuntimeModule())
                .with(new XMIUiModule(plugin)))
                .with(new SharedStateModule()));
    }
 
    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);
        plugin = this;
        try {
            initializeEcoreInjector();
        } catch(Exception e) {
//            logger.error(e.getMessage(), e);
            throw e;
        }
    }
 
    @Override
    public void stop(BundleContext context) throws Exception {
        plugin = null;
        injector = null;
        super.stop(context);
    }
 
    /**
     * Returns the shared instance
     *
     * @return the shared instance
     */
    public static Activator getDefault() {
        return plugin;
    }
 
}