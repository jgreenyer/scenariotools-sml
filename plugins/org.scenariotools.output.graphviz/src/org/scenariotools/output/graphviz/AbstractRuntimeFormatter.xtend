/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.output.graphviz

import org.scenariotools.sml.runtime.MessageEvent
import org.scenariotools.sml.runtime.SMLRuntimeState
import org.scenariotools.sml.runtime.Transition
import org.scenariotools.sml.runtime.WaitEvent

abstract class AbstractRuntimeFormatter implements GraphFormatter {
	 
	override formatTransition(Transition t) {
		var String colorLabel = "";		
		if (Boolean.TRUE.equals(t.targetState.stringToBooleanAnnotationMap.get("loseBuechi"))) {
			colorLabel= " color=\"#FF0000\" ";
		}
		if (!t.stringToStringAnnotationMap.get("closes non-progress loop for assumption scenarios").isNullOrEmpty){
			colorLabel= " color=\"#CC00CC\" ";
		}else if (!t.stringToStringAnnotationMap.get("closes non-progress loop for requirement scenarios").isNullOrEmpty
				|| (t.stringToBooleanAnnotationMap.get("closes loop of non-goal states") != null
					&& t.stringToBooleanAnnotationMap.get("closes loop of non-goal states")
				)){
			colorLabel= " color=\"#CC0000\" ";
		}
		var String labelString = "unknown event"
		if(t.event instanceof WaitEvent)
			labelString = t.event.toString()
		if(t.event instanceof MessageEvent) 
			labelString = (t.event as MessageEvent).messageName.replace("->", "→")
		if (!t.stringToStringAnnotationMap.get("closes non-progress loop for assumption scenarios").isNullOrEmpty)
			labelString += "\n(non-progress assumption: " + t.stringToStringAnnotationMap.get("closes non-progress loop for assumption scenarios") + ")"
		if (!t.stringToStringAnnotationMap.get("closes non-progress loop for requirement scenarios").isNullOrEmpty)
			labelString += "\n(non-progress requirement: " + t.stringToStringAnnotationMap.get("closes non-progress loop for requirement scenarios") + ")"
		
		'''[ «IF !t.controllable»style=dashed «ENDIF» «colorLabel» label="«labelString»"]'''
	}
	
	def boolean isControllable(Transition transition) {
		if(transition.event instanceof WaitEvent)
			return true
		else if(transition.event instanceof MessageEvent)
			return transition.sourceState.objectSystem.isControllable(
				(transition.event as MessageEvent).sendingObject)
		else
			return false
	}
	def getColor(SMLRuntimeState state) {
		var color = "white"
//		if(state instanceof SMLRuntimeState) {
			val a = state.isSafetyViolationOccurredInAssumptions // RED
			val r = false//state.isSafetyViolationOccurredInRequirements // BLUE
			val s = state.isSafetyViolationOccurredInGuarantees // YELLOW
			if (a && r && s)
				color = "grey"
			else if (a && r)
				color = "purple"
			else if (a && s)
				color = "goldenrod2"
			else if (r && s)
				color = "greenyellow"
			else if (a)
				color = "red3"
			else if (r)
				color = "dodgerblue"
			else if (s)
				color = "yellow"
//		}
		return color
	}
}