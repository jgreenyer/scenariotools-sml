/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.output.graphviz

import org.scenariotools.sml.runtime.ActiveScenario
import org.scenariotools.sml.runtime.SMLRuntimeState
import org.scenariotools.sml.runtime.ActivePart
import org.scenariotools.sml.runtime.ActiveCase
import org.scenariotools.sml.InteractionFragment
import org.scenariotools.sml.Interaction
import java.util.Arrays
import java.util.List
import org.scenariotools.sml.ScenarioKind

class SMLRuntimeStateFormatter extends AbstractRuntimeFormatter {
	
	public static final String specificationViolationColor = "#ff9999";
	public static final String assumptionViolationColor = "#d2a0ff";
	public static final String requirementViolationColor = "#ff7ca4";
	public static final String deadlockColor = "#ff8888";
	public static final String winColorFill = "#c8ffc8"
	public static final String loseColorFill = "#ffc8c8"
	public static final String assumptionViolationBorder = "#d2a0ff"
	public static final String deadlockBorder = "#ff2222"
	public static final String assumptionRequestedEventColor = "#ebd6ff";
	public static final String specificationRequestedEventColor = "#d6e3ff";
	public static final String requirementRequestedEventColor = "#fff4d6";
	public static final String systemStepBlockedButAssumptionRequestedEventsColor = "#d996ff";
	public static final String defaultStateColor = "#f1f4f6";
	
	
	override formatState(SMLRuntimeState s) {
		val boolean useSynthesisFormat = s.stringToBooleanAnnotationMap.containsKey("goal")
		val rs = s as SMLRuntimeState
		var color = defaultStateColor
		var borderColor = "black"
		var border = 1
		if (useSynthesisFormat) {
			if (rs.stringToBooleanAnnotationMap.get("win")) {
				color = winColorFill
			} else if (rs.stringToBooleanAnnotationMap.get("loseBuechi")) {
				color = loseColorFill
				if (rs.outgoingTransition.empty){
					borderColor = deadlockBorder
					border = 8
				}
			}
			if(s.stringToBooleanAnnotationMap.containsKey("goal") &&
				s.stringToBooleanAnnotationMap.get("goal")){
					borderColor = "blue"
					border = 5
			}
			if(rs.safetyViolationOccurredInAssumptions){
				borderColor = assumptionViolationBorder
				border = 5
			}
		}

		'''[shape=box,margin=0 penwidth=«border» color="«borderColor»" style=filled fillcolor="«color»" label=«stateLabel(rs)»]'''
	}

	def stateName(SMLRuntimeState s) {
		if (s.stringToStringAnnotationMap.containsKey("passedIndex"))
			return s.stringToStringAnnotationMap.get("passedIndex")
		else {
			if(s.stateGraph.startState === s) return "START" else return "not explored"
		}
	}

	def stateLabel(SMLRuntimeState state) {
		var activeScearioInformation = ""
		if (!state.activeScenarios.empty) {
			activeScearioInformation = '''«FOR ActiveScenario ActiveScenario : state.activeScenarios»«scenarioRow(ActiveScenario, state)»«ENDFOR»''' 			
		}
		var violatedScearioInformation = ""
		if (state.getStringToStringAnnotationMap.keySet.contains("violatedAssumptionScenario")
			|| state.getStringToStringAnnotationMap.keySet.contains("violatedRequirementScenario")
			|| state.getStringToStringAnnotationMap.keySet.contains("violatedSpecificationScenario")
		){
			violatedScearioInformation = '''«FOR String keyString : state.getStringToStringAnnotationMap.keySet»«additionalInformationRows(keyString, state.getStringToStringAnnotationMap.get(keyString))»«ENDFOR»'''
		}
		
		if (state.getStringToStringAnnotationMap.keySet.contains("deadlock")){
			violatedScearioInformation += buildRowString("Deadlock state:", state.getStringToStringAnnotationMap.get("deadlock"), deadlockColor)
		}
		
		var systemStepBlockedInfo = state.stringToStringAnnotationMap.get("SystemStepBlockedButAssumptionRequestedEvents")
		if(systemStepBlockedInfo != null)
			violatedScearioInformation += buildRowString("System step blocked (safety violation), but assumptions w requested events", systemStepBlockedInfo, systemStepBlockedButAssumptionRequestedEventsColor)

		if(state.safetyViolationOccurredInAssumptions){
			violatedScearioInformation += buildRowString("safety violation occurred in assumptions", assumptionViolationColor)
		}
		if(state.safetyViolationOccurredInGuarantees){
			violatedScearioInformation += buildRowString("safety violation occurred in specification", specificationViolationColor)
		}
		if(false){//state.safetyViolationOccurredInRequirements){
			violatedScearioInformation += buildRowString("safety violation occurred in requirements", requirementRequestedEventColor)
		}

		
		if (violatedScearioInformation == "" && activeScearioInformation == "")
			'''"«state.stateName»"'''
		else
			'''<<TABLE cellborder="1" color="black" border="0" cellspacing="0">«state.titleRow»«activeScearioInformation»«violatedScearioInformation»</TABLE>>'''
	}

	def titleRow(SMLRuntimeState s) {
		'''<TR><TD colspan="3">«s.stateName»</TD></TR>'''
	}

	def scenarioRow(ActiveScenario activeScenario, SMLRuntimeState state) {

		var color = "bgcolor=\""

		var isAssumptionEventRequested = state.stringToBooleanAnnotationMap.get(activeScenario.scenario.name+"_ASSUMPTION");
		var isSpecificationEventRequested = state.stringToBooleanAnnotationMap.get(activeScenario.scenario.name+"_SPECIFICATION");
		var isRequirementEventRequested = state.stringToBooleanAnnotationMap.get(activeScenario.scenario.name+"_REQUIREMENT");
		
		if(isAssumptionEventRequested != null && isAssumptionEventRequested)
			color += assumptionRequestedEventColor + "\""
		else if(isSpecificationEventRequested != null && isSpecificationEventRequested)
			color += specificationRequestedEventColor + "\""
		else if(isRequirementEventRequested != null && isRequirementEventRequested)
			color += requirementRequestedEventColor + "\""
		else
			color += "#FFFFFF\""
		
		var scenarioKind = ""
		
		if (activeScenario.scenario.kind == ScenarioKind.ASSUMPTION)
			scenarioKind = "A"
		else if (activeScenario.scenario.kind == ScenarioKind.GUARANTEE)
			scenarioKind = "S"
		
		'''<TR><TD>«scenarioKind»</TD><TD>«activeScenario.scenario.name»</TD><TD «color»>«activeScenario.mainActiveInteraction.cutCoordinates»</TD></TR>'''
	}

	def additionalInformationRows(String key, String value) {
		if (key.equals("violatedAssumptionScenario"))
			violatedAssumptionScenarioRow(value)
		else if(key.equals("violatedRequirementScenario"))
			violatedRequirementScenarioRow(value)
		else if(key.equals("violatedSpecificationScenario"))
			violatedSpecificationScenarioRow(value)
	}

	def violatedAssumptionScenarioRow(String value) {
		var List<String> violatedScenarioNames = Arrays.asList(value.split("\\s*,\\s*"));
		var returnString = ''''''
		for (violatedScenarioName : violatedScenarioNames) {
			returnString += buildRowString("Violated Assumption Scenario:", violatedScenarioName, assumptionViolationColor) 
		}
		return returnString;
	}
	def violatedRequirementScenarioRow(String value) {
		var List<String> violatedScenarioNames = Arrays.asList(value.split("\\s*,\\s*"));
		var returnString = ''''''
		for (violatedScenarioName : violatedScenarioNames) {
			returnString += buildRowString("Violated Requirement Scenario:", violatedScenarioName, requirementViolationColor) 
		}
		return returnString;
		
	}
	def violatedSpecificationScenarioRow(String value) {
		var List<String> violatedScenarioNames = Arrays.asList(value.split("\\s*,\\s*"));
		var returnString = ''''''
		for (violatedScenarioName : violatedScenarioNames) {
			returnString += buildRowString("Violated Specification Scenario:", violatedScenarioName, specificationViolationColor) 
		}
		return returnString;
		
	}
	
	//"SystemStepBlockedButAssumptionScenariosWithRequestedEvents"
	
	def buildRowString(String key, String value, String color){
		return '''<TR><TD colspan="2" bgcolor="«color»">«key»</TD><TD bgcolor="«color»">«value»</TD></TR>'''
	}

	def buildRowString(String value, String color){
		return '''<TR><TD colspan="3" bgcolor="«color»">«value»</TD></TR>'''
	}
	
	/**
	 * Assumptions: fragment is not null
	 */
	def dispatch String getCutCoordinates(ActivePart part) {
		val coordinateInParent = part.interactionFragment.coordinateInInteraction
		if ("".equals(coordinateInParent))
			part.enabledNestedCoordinates // "" implies there are nested.
		else {
			if (!part.enabledNestedActiveInteractions.
				empty) '''«coordinateInParent»:«part.enabledNestedCoordinates»''' else
				coordinateInParent
		}
	}

	def String getEnabledNestedCoordinates(ActivePart part) {
		if (part.enabledNestedActiveInteractions.size == 1)
			return part.enabledNestedActiveInteractions.get(0).cutCoordinates
		else {
			val result = '''(«FOR ActivePart enabledNestedPart : part.enabledNestedActiveInteractions»«enabledNestedPart.cutCoordinates»,«ENDFOR»'''
			result.subSequence(0, result.length - 1).toString + ")"
		}
	}

	def dispatch String getCutCoordinates(ActiveCase activeCase) {
		activeCase.enabledNestedActiveInteractions.get(0).cutCoordinates
	}

	/**
	 * Returns the index of the fragment in the interaction containing this fragment. If
	 * the fragment is not contained by an interaction, return the empty string.
	 */
	def String getCoordinateInInteraction(InteractionFragment f) {
		if (f.eContainer instanceof Interaction)
			(f.eContainer as Interaction).fragments.indexOf(f).toString
		else
			return ""
	}
//	override formatState(State s) {
//		val rs = s as SMLRuntimeState
//		var color = rs.color
//		'''[shape=record style=filled fillcolor=«color» label=«stateLabel(rs)»]'''
//	}
//
//	def stateLabel(SMLRuntimeState s) {
//		'''"{«s.hashCode»«FOR a : s.activeScenarios»«outputScenario(a)»«ENDFOR»«FOR string : s.stringToStringAnnotationMap»|{«string.key»|«string.value»}«ENDFOR» }"'''
//	}
//	
//	def outputScenario(ActiveScenario s) {
//		System.out.println(s.requestedEvents.isEmpty)
//		'''|{«s.scenario.name»(«s.scenario.kind.literal»)}''' //|{«IF !s.requestedEvents.isEmpty»r|«ENDIF»«IF s.hasBlockedEvents»b|«ENDIF»l}| «outputMessages(s)»}
//	}
//	def hasBlockedEvents(ActiveScenario s) {
//		return s.alphabet.exists[e | s.isBlocked(e)]
//	}
//	def outputMessages(ActiveScenario s) {
//		val result = new StringBuilder("{")
//		if (!s.requestedEvents.isEmpty){
//			s.requestedEvents.forEach[e|result.append(e.toString.replace("->", "→")).append(",")]
//			result.deleteCharAt(result.length - 1)
//			result.append("|")
//		}
//		// blocked events
//		if (!s.alphabet.isEmpty){
//			var blockedExist = false
//			for (MessageEvent e : s.alphabet) {
//				if (s.isBlocked(e)) {
//					result.append((e.toString.replace("->", "→"))).append(",")
//					blockedExist = true
//				}
//			}
//			if (blockedExist) {
//				result.deleteCharAt(result.length - 1)
//			}
//			result.append('|')
//		}
//		// TODO listening events
//		if (!s.alphabet.isEmpty) {
//			s.alphabet.forEach[e|result.append((e.toString.replace("->", "→"))).append(",")]
//			result.deleteCharAt(result.length - 1)
//		}
//		result.append('}')
//	}
}