/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.output.graphviz

import java.util.ArrayDeque
import java.util.HashMap
import java.util.HashSet

import org.scenariotools.sml.runtime.SMLRuntimeState
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph
import org.scenariotools.sml.runtime.Transition

class DotOutput {
	GraphFormatter formatter 
	/**
	 * Set of all reachable transitions in the graph
	 */
	def allTransitions(SMLRuntimeState s) {
		val result = new HashSet<Transition>();
		val states = new HashSet<SMLRuntimeState>();
		val q = new ArrayDeque<SMLRuntimeState>();
		q.add(s);
		while (!q.empty) {
			val state = q.poll
			if (!states.contains(state)) {
				result.addAll(state.outgoingTransition)
				states.add(state)
				state.outgoingTransition.forEach[t|q.add(t.targetState)]
			}

		}
		return result
	}

	def output(SMLRuntimeStateGraph stategraph, GraphFormatter f) {
		formatter = f
		val states = new HashMap<SMLRuntimeState, String>()
		// not efficient, but better readable
		stategraph.states.forEach[s|states.put(s, String.valueOf(s.hashCode))]
		'''digraph {
		«FOR s : stategraph.states»
			«stateOutput(s, states.get(s))»
		«ENDFOR»
		«FOR t : allTransitions(stategraph.startState)»
			«states.get(t.sourceState)» -> «states.get(t.targetState)» «formatter.formatTransition(t)»
		«ENDFOR»
		}'''
	}
	

	def stateOutput(SMLRuntimeState s, String name) {

		'''«name» «formatter.formatState(s)»'''
	}
}
