/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.output.graphviz

import org.scenariotools.output.graphviz.AbstractRuntimeFormatter
import org.scenariotools.sml.runtime.SMLRuntimeState

class StrategyFormatter extends AbstractRuntimeFormatter {

	override formatState(SMLRuntimeState s) {
		//'''"{«s.hashCode»«FOR a : s.stringToBooleanAnnotationMap»|«a.key»:«a.value» «ENDFOR» «FOR a : s.stringToEObjectAnnotationMap»|«a.key»:«a.value» «ENDFOR»«FOR a : s.stringToStringAnnotationMap»|«a.key»:«a.value»«ENDFOR»}"'''
		var shape="oval"
		var peripheries=1
		var label ="x"
		if(Boolean.TRUE.equals(s.stringToBooleanAnnotationMap.get("goal"))) {
			peripheries=2
		}
		val index = s.stringToStringAnnotationMap.get("passedIndex")
		if(index !== null) {
			label = index
		}
		//label = s.stringToStringAnnotationMap.get("passedIndex") 
		'''[fillcolor=«getColor(s)» shape=«shape» peripheries=«peripheries» label="«label»"]'''
	}
	
	
	
}