package org.scenariotools.plc.codegen;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.WaitEvent;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.util.SMLRuntimeStateUtil;
import org.scenariotools.sml.runtime.Transition;

import plccodegenerationconfiguration.EventPair;
import plccodegenerationconfiguration.PLCCodeGenerationConfiguration;

/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
public class CodeGenerator {
	static final int STATE_INDEX_STEP_SIZE = 25;
	static final String AUX_STM_STATE_SUFFIX = "State";
	static final String FUNCTION_KEYWORD = "METHOD";
	static final String FUNCTION_SUFFIX = "_Function";
	
	private static Logger logger = Activator.getLogManager().getLogger(CodeGenerator.class.getName());

	PLCCodeGenerationConfiguration config;
	String targetDirectory;
	SMLRuntimeStateGraph controller;
	Map<MessageEvent, MessageEvent> eventPairs;
	Map<MessageEvent, Integer> eventToStateIndex;
	List<String> actions;
	Set<String> variables;
	
	public static void generate(PLCCodeGenerationConfiguration config) {
		new CodeGenerator(config).generate();
	}
	
	CodeGenerator(PLCCodeGenerationConfiguration config) {
		this.config = config;
	}
	
	boolean generate() {
		ConfigurationValidator validator = new ConfigurationValidator(config);
		if(validator.isValid()) {
			createNewController();
			convertEventPairListToMap();
			prepareTargetDirectory();
			actions = new ArrayList<String>();
			variables = new HashSet<String>();
			generateAuxiliaryControllers();
			generateMainController(generateEnvironmentTransitions());
			generateMetaController();
			generateMainProgram();
			return true;
		} else {
			logger.error("Cannot generate PLC code: " + validator.getMessage());
		}
		
		return false;
	}
	
	void createNewController() {
		controller = RuntimeFactory.eINSTANCE.createSMLRuntimeStateGraph();
		Map<SMLRuntimeState, SMLRuntimeState> stateMap = new HashMap<SMLRuntimeState, SMLRuntimeState>();

		int index = 1;
		for(SMLRuntimeState state : config.getController().getStates()) {
			if(state.getOutgoingTransition().size() > 0) {
				Event firstEvent = state.getOutgoingTransition().get(0).getEvent();
				if(firstEvent instanceof WaitEvent || Helper.containsUnifiableEvent(config.getEventsToRemove(), (MessageEvent)firstEvent))
					continue;				
			}
			
			SMLRuntimeState newState = RuntimeFactory.eINSTANCE.createSMLRuntimeState();
			SMLRuntimeStateUtil.setPassedIndex(newState, "" + index);
			index += 1;
		
			stateMap.put(state, newState);
			controller.getStates().add(newState);
		}
		
		SMLRuntimeState newStartState = getNextStatePostRemoval(config.getController().getStartState(), stateMap); 
		controller.setStartState(newStartState);
		for(SMLRuntimeState state : config.getController().getStates()) {
			if(!stateMap.containsKey(state))
				continue;
			
			SMLRuntimeState sourceState = stateMap.get(state);
			
			for(Transition transition : state.getOutgoingTransition()) {
				Transition newTransition = RuntimeFactory.eINSTANCE.createTransition();
				newTransition.setSourceState(sourceState);
				newTransition.setEvent(transition.getEvent());
				
				SMLRuntimeState targetState = getNextStatePostRemoval(transition.getTargetState(), stateMap);
				newTransition.setTargetState(targetState);
				
				sourceState.getOutgoingTransition().add(newTransition);
			}
		}
		
		Helper.remapEvents(config.getEventPairs(), controller);
		controller.setConfiguration(config.getController().getConfiguration());
	}

	static SMLRuntimeState getNextStatePostRemoval(SMLRuntimeState state, Map<SMLRuntimeState, SMLRuntimeState> stateMap) {
		while(!stateMap.containsKey(state))
			state = state.getOutgoingTransition().get(0).getTargetState();
		return stateMap.get(state);
	}

	void convertEventPairListToMap() {
		eventPairs = new HashMap<MessageEvent, MessageEvent>();
		
		for(EventPair pair : config.getEventPairs()) {
			eventPairs.put(pair.getFirst(), pair.getSecond());
		}
	}

	void prepareTargetDirectory() {
		String platformString = config.eResource().getURI().toPlatformString(true);
		IFile file = (IFile)ResourcesPlugin.getWorkspace().getRoot().findMember(platformString);
		IPath path = file.getLocation().removeLastSegments(1).append("plc-gen");
		IFolder folder = file.getProject().getFolder(path.makeRelativeTo(file.getProject().getLocation()));
		
		try {
			if(folder.exists())
				folder.delete(true, null);
			folder.create(true, true, null);
			folder.setDerived(true, null);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		targetDirectory = path.toString() + "/";
	}
	
	void generateAuxiliaryControllers() {
		Map<EObject, List<EventPair>> auxiliaryComponents = new HashMap<EObject, List<EventPair>>(); 
		
		for(EventPair pair : config.getEventPairs()) {
			EObject theObject = pair.getFirst().getReceivingObject();
			if(!auxiliaryComponents.containsKey(theObject)) {
				auxiliaryComponents.put(theObject, new LinkedList<EventPair>());
			}
			
			auxiliaryComponents.get(theObject).add(pair);
		}
		
		eventToStateIndex = new HashMap<MessageEvent, Integer>();
		for(Entry<EObject, List<EventPair>> entry : auxiliaryComponents.entrySet()) {
			try {
				String name = Helper.getName(entry.getKey());
				int counter = STATE_INDEX_STEP_SIZE;
				
				Writer writer = getWriter(name + "STM");
				actions.add(name + "STM();");
				
				writer.write("CASE " + name + AUX_STM_STATE_SUFFIX + " OF\n");
				addIntVariable(name + AUX_STM_STATE_SUFFIX);
				writer.write("\t0:\n\t\t(* idle *)\n");
				
				Collections.sort(entry.getValue(), new Comparator<EventPair>() {
					@Override
					public int compare(EventPair pair1, EventPair pair2) {
						return pair1.getFirst().toString().compareTo(pair2.getFirst().toString());
					}
				});
				
				for(EventPair pair : entry.getValue()) {
					writer.write("\t" + counter + ":\n");
					eventToStateIndex.put(pair.getFirst(), counter);
					counter += STATE_INDEX_STEP_SIZE;
					writer.write("\t\t(* " + pair.getFirst() + " *)\n");
					writer.write("\t\t// TODO: perform action\n");
					writer.write("\t\tIF (* TODO: check if action is done *) THEN\n");
					writer.write("\t\t\t// TODO: clean up after action\n");
					
					String varName = Helper.createIDFromEvent(pair.getSecond());
					writer.write("\t\t\t" + varName +" := TRUE;\n");
					addBooleanVariable(varName);
					
					writer.write("\t\t\t" + name + AUX_STM_STATE_SUFFIX + " := 0;\n");
					writer.write("\t\tEND_IF\n");
				}
				writer.write("END_CASE\n");
				writer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	Writer getWriter(String fileName) throws IOException {
		return new BufferedWriter(new FileWriter(targetDirectory + fileName + ".txt"));
	}
	
	void addIntVariable(String name) {
		variables.add(name + ":INT := 0;");
	}
	
	void addBooleanVariable(String name) {
		variables.add(name + ":BOOL := FALSE;");
	}

	Set<String> generateEnvironmentTransitions() {
		Map<String, Set<Transition>> envTransitionsMap = new HashMap<String, Set<Transition>>();
		
		for(SMLRuntimeState state : controller.getStates()) {
			for(Transition t : state.getOutgoingTransition()) {
				if(!Helper.isControllable(controller, t.getEvent())) {
					String key = Helper.createIDFromEvent((MessageEvent)t.getEvent());
					Set<Transition> transitions = envTransitionsMap.get(key);
					
					if(transitions == null) {
						transitions = new HashSet<Transition>();
						envTransitionsMap.put(key, transitions);
					}
					
					transitions.add(t);
				}
			}
		}
		
		for(Entry<String, Set<Transition>> entry : envTransitionsMap.entrySet()) {
			generateEnvironmentTransitionFunction(entry.getKey(), entry.getValue());
		}
		
		return envTransitionsMap.keySet();
	}
		
	void generateEnvironmentTransitionFunction(String event, Set<Transition> transitions) {
		List<Mapping> map = new ArrayList<Mapping>();
		for(Transition t : transitions)
			map.add(new Mapping(t));
		Collections.sort(map);
		
		try {
			String functionName = event + FUNCTION_SUFFIX;
			
			Writer writer = getWriter(functionName);
			writer.write(FUNCTION_KEYWORD + " " + functionName + " : BOOL\n"
					+ "VAR_INPUT\n"
					+ "END_VAR\n\n"
					+ "VAR\n"
					+ "\tlower, upper, index : INT;\n");
			writer.write("\tsource : ARRAY [0.." + (map.size()-1) + "] OF INT := [");
			for(int i = 0; i < map.size(); i++)
				writer.write(map.get(i).key + (i < map.size()-1 ? ", " : "];\n"));
			writer.write("\ttarget : ARRAY [0.." + (map.size()-1) + "] OF INT := [");
			for(int i = 0; i < map.size(); i++)
				writer.write(map.get(i).value + (i < map.size()-1 ? ", " : "];\n"));
			writer.write("END_VAR\n\n\n\n");
			writer.write("IF " + event + " THEN\n"
					+ "\tlower := 0;\n"
					+ "\tupper := " + (map.size()-1) + ";\n\n"
					+ "\tREPEAT\n"
					+ "\t\tindex := (lower + upper) / 2;\n"
					+ "\t\tIF source[index] < controllerState THEN\n"
					+ "\t\t\tlower := index + 1;\n"
					+ "\t\tELSIF source[index] > controllerState THEN\n"
					+ "\t\t\tupper := index - 1;\n"
					+ "\t\tEND_IF\n"
					+ "\t\tUNTIL (upper < lower) OR (source[index] = controllerState)\n"
					+ "\tEND_REPEAT\n\n"
					+ "\tIF source[index] = controllerState THEN\n"
					+ "\t\tcontrollerState := target[index];\n"
					+ "\t\t" + event + " := FALSE;\n"
					+ "\t\t" + functionName + " := TRUE;\n"
					+ "\tELSE\n"
					+ "\t\t" + functionName + " := FALSE;\n"
					+ "\tEND_IF\n"
					+ "ELSE\n"
					+ "\t" + functionName + " := FALSE;\n"
					+ "END_IF\n");
			writer.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	static class Mapping implements Comparable<Mapping> {
		int key;
		Object value;
		
		Mapping(Transition t) {
			key = Integer.valueOf(SMLRuntimeStateUtil.getPassedIndex(t.getSourceState()));
			value = Integer.valueOf(SMLRuntimeStateUtil.getPassedIndex(t.getTargetState()));
		}
		
		Mapping(SMLRuntimeState source, SMLRuntimeState target, Map<String, Integer> value) {
			key = Integer.valueOf(SMLRuntimeStateUtil.getPassedIndex(source));
			this.value = new Mapping(target, value);
		}

		Mapping(SMLRuntimeState state, Map<String, Integer> value) {
			key = Integer.valueOf(SMLRuntimeStateUtil.getPassedIndex(state));
			this.value = value;
		}
		
		@Override
		public int compareTo(Mapping other) {
			return key - other.key;
		}
	}

	void generateMainController(Set<String> envTransitionsFunctions) {
		Set<String> auxControllers = new HashSet<String>();
		List<Mapping> map = new ArrayList<Mapping>();
		for(SMLRuntimeState state : controller.getStates()) {
			if(Helper.isControllable(controller, state)) {
				Map<String, Integer> auxControllerStates = new HashMap<String, Integer>();
				
				SMLRuntimeState currentState = state;
				do {
					Transition t = currentState.getOutgoingTransition().get(0);
					
					MessageEvent event = (MessageEvent)t.getEvent();
					String auxControllerName = Helper.getName(event.getReceivingObject());
					auxControllerStates.put(auxControllerName, eventToStateIndex.get(event));
					auxControllers.add(auxControllerName);
					
					currentState = t.getTargetState();
				} while(Helper.isControllable(controller, currentState));
				
				map.add(new Mapping(state, currentState, auxControllerStates));
			}
		}
		Collections.sort(map);
		
		addIntVariable("controllerState");
		
		try {
			Writer writer = getWriter("controller");
			writer.write(FUNCTION_KEYWORD + " controller : BOOL\n"
					+ "VAR_INPUT\n"
					+ "END_VAR\n\n"
					+ "VAR\n"
					+ "\tchanged : BOOL;\n"
					+ "\tlower, upper, index : INT;\n");
			writer.write("\tsource : ARRAY [0.." + (map.size()-1) + "] OF INT := [");
			for(int i = 0; i < map.size(); i++)
				writer.write(map.get(i).key + (i < map.size()-1 ? ", " : "];\n"));
			writer.write("\ttarget : ARRAY [0.." + (map.size()-1) + "] OF INT := [");
			for(int i = 0; i < map.size(); i++) {
				Mapping m = (Mapping)map.get(i).value;
				writer.write(m.key + (i < map.size()-1 ? ", " : "];\n"));
			}
			for(String auxControllerName : auxControllers) {
				writer.write("\t" + auxControllerName + "Target : ARRAY [0.." + (map.size()-1) + "] OF INT := [");
				for(int i = 0; i < map.size(); i++) {
					Mapping m = (Mapping)map.get(i).value;
					@SuppressWarnings("unchecked")
					Map<String, Integer> auxControllerStates = (Map<String, Integer>)m.value;
					Integer value = auxControllerStates.get(auxControllerName);
					if(value == null)
						value = 0;
					writer.write(value + (i < map.size()-1 ? ", " : "];\n"));
				}				
			}			
			writer.write("END_VAR\n\n\n\n");
			writer.write("REPEAT\n"
					+ "\tchanged := FALSE;\n");
			for(String envTransitionFunc : envTransitionsFunctions) {
				String functionName = envTransitionFunc + FUNCTION_SUFFIX;
				writer.write("\tchanged := " + functionName + "() OR changed;\n");
			}
			writer.write("\tUNTIL NOT changed\n"
					+ "END_REPEAT\n\n");
			writer.write("lower := 0;\n"
					+ "upper := " + (map.size()-1) + ";\n\n"
					+ "REPEAT\n"
					+ "\tindex := (lower + upper) / 2;\n"
					+ "\tIF source[index] < controllerState THEN\n"
					+ "\t\tlower := index + 1;\n"
					+ "\tELSIF source[index] > controllerState THEN\n"
					+ "\t\tupper := index - 1;\n"
					+ "\tEND_IF\n"
					+ "\tUNTIL (upper < lower) OR (source[index] = controllerState)\n"
					+ "END_REPEAT\n\n"
					+ "IF source[index] = controllerState THEN\n"
					+ "\tcontrollerState := target[index];\n");
			for(String auxControllerName : auxControllers) {
				writer.write("\tIF " + auxControllerName + "Target[index] <> 0 THEN\n"
						+ "\t\t" + auxControllerName + AUX_STM_STATE_SUFFIX + " := " + auxControllerName + "Target[index];\n"
						+ "\tEND_IF\n");
			}
			writer.write("END_IF\n\n"
					+ "controller := TRUE;\n");
			writer.close();
		} catch(IOException e) {
			e.printStackTrace();
		}		
	}
	
	void generateMetaController() {
		try {
			Writer writer = getWriter("metaControllerSTM");
			actions.add("metaControllerSTM();");
			
			writer.write("CASE metaControllerState OF\n");
			addIntVariable("metaControllerState");
			
			writer.write("\t0:\n\t\t// TODO: perform start up; initialize components\n");
			writer.write("\t\tcontrollerState := " + SMLRuntimeStateUtil.getPassedIndex(controller.getStartState()) + ";\n");
			writer.write("\t\tmetaControllerState := " + STATE_INDEX_STEP_SIZE + ";\n");
			writer.write("\t" + STATE_INDEX_STEP_SIZE + ":\n\t\tcontroller();\n");
//			writer.write("\t" + (STATE_INDEX_STEP_SIZE * 2) + ":\n\t\t// TODO: shut down system; assumptions violated\n");
			writer.write("END_CASE\n");

			writer.close();
		} catch(IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void generateMainProgram() {
		Set<MessageEvent> sensorEvents = new HashSet<MessageEvent>();
		
		for(SMLRuntimeState state : controller.getStates()) {
			for(Transition transition : state.getOutgoingTransition()) {				
				if(Helper.isControllable(controller, transition.getEvent()))
					continue;
				MessageEvent event = (MessageEvent)transition.getEvent();
				if(sensorEvents.contains(event))
					continue;
				if(eventPairs.values().contains(event))
					continue;
				sensorEvents.add(event);
			}
		}
		
		List<MessageEvent> sortedSensorEvents = new ArrayList<MessageEvent>(sensorEvents);
		Collections.sort(sortedSensorEvents, new Comparator<MessageEvent>() {
			@Override
			public int compare(MessageEvent event1, MessageEvent event2) {
				return event1.toString().compareTo(event2.toString());
			}
		});
		
		List<String> sortedVariables = new ArrayList<String>(variables);
		Collections.sort(sortedVariables);
		
		try {
			Writer writer = getWriter("main");
			writer.write("PROGRAM PLC_PRG\n");
			writer.write("VAR\n");
			writer.write("\t(* USER VARIABLES *)\n\n");
			writer.write("\t(* GENERATED SENSOR EVENT VARIABLES *)\n");
			for(MessageEvent event : sortedSensorEvents) {
				writer.write("\t" + Helper.createIDFromEvent(event) + ":BOOL := FALSE;\n");
			}
			writer.write("\n\t(* GENERATED INTERNAL CONTROL FLOW VARIABLES *)\n");
			for(String variable : sortedVariables) {
				writer.write("\t" + variable + "\n");
			}
			writer.write("END_VAR\n\n\n\n");
			writer.write("(* GENERATED ACTIONS *)\n");
			for(String action : actions) {
				writer.write(action + "\n");
			}
			writer.write("\n(* USER PROGRAM *)\n");
			writer.write("// TODO: add function block calls\n");
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
}
