package org.scenariotools.plc.codegen;

import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.osgi.framework.Bundle;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.Transition;

import plccodegenerationconfiguration.PLCCodeGenerationConfiguration;

/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
public class CodeGeneratorCODESYS extends CodeGenerator {
	public static void generate(PLCCodeGenerationConfiguration config) {
		new CodeGeneratorCODESYS(config).generate();
	}
	
	private CodeGeneratorCODESYS(PLCCodeGenerationConfiguration config) {
		super(config);
	}
	
	@Override
	boolean generate() {
		if(super.generate()) {
			Bundle bundle = Activator.getContext().getBundle();		
			try {
				InputStream input = FileLocator.openStream(bundle, new Path("plc-lib/BinarySearch.txt"), false);
				Writer writer = getWriter("BinarySearch");
				while(input.available() > 0)
					writer.write(input.read());
				writer.close();
				input.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}
		
		return false;
	}
	
	@Override
	Set<String> generateEnvironmentTransitions() { // only used by super class
		return null;
	}

	@Override
	void generateMainController(Set<String> envTransitionsFunctions) { // parameter is ignored when using CODESYS-enhancements
		Set<String> auxControllers = new HashSet<String>();
		List<Mapping> map = new ArrayList<Mapping>();
		for(SMLRuntimeState state : controller.getStates()) {
			if(Helper.isControllable(controller, state)) {
				Map<String, Integer> auxControllerStates = new HashMap<String, Integer>();
				
				SMLRuntimeState currentState = state;
				do {
					Transition t = currentState.getOutgoingTransition().get(0);
					
					MessageEvent event = (MessageEvent)t.getEvent();
					String auxControllerName = Helper.getName(event.getReceivingObject());
					auxControllerStates.put(auxControllerName, eventToStateIndex.get(event));
					auxControllers.add(auxControllerName);
					
					currentState = t.getTargetState();
				} while(Helper.isControllable(controller, currentState));
				
				map.add(new Mapping(state, currentState, auxControllerStates));
			}
		}
		Collections.sort(map);
		
		Map<String, List<Mapping>> envTransitionsMaps = createEnvironmentTransitionMappings();
		
		addIntVariable("controllerState");
		
		try {
			Writer writer = getWriter("controller");
			writer.write(FUNCTION_KEYWORD + " controller : BOOL\n"
					+ "VAR_INPUT\n"
					+ "END_VAR\n\n"
					+ "VAR\n"
					+ "\tchanged : BOOL;\n"
					+ "\tindex : INT;\n");
			writeArray(writer, "source", map, m -> { return String.valueOf(m.key); });
			writeArray(writer, "target", map, m -> { Mapping otherM = (Mapping)m.value; return String.valueOf(otherM.key); });
			for(String auxControllerName : auxControllers) {
				writeArray(writer, auxControllerName + "Target", map, m -> {
					Mapping otherM = (Mapping)m.value;
					@SuppressWarnings("unchecked")
					Map<String, Integer> auxControllerStates = (Map<String, Integer>)otherM.value;
					Integer value = auxControllerStates.get(auxControllerName);
					if(value == null)
						value = 0;
					return value.toString();
				});
			}
			for(Entry<String, List<Mapping>> entry : envTransitionsMaps.entrySet()) {
				writeArray(writer, entry.getKey() + "Source", entry.getValue(), m -> { return String.valueOf(m.key); });
				writeArray(writer, entry.getKey() + "Target", entry.getValue(), m -> { return m.value.toString(); });
			}			
			writer.write("END_VAR\n\n\n\n");
			writer.write("REPEAT\n"
					+ "\tchanged := FALSE;\n");
			for(Entry<String, List<Mapping>> entry : envTransitionsMaps.entrySet()) {
				writer.write("\tchanged := (BinarySearch("
						+ "ADR(" + entry.getKey() + "), "
						+ "ADR(" + entry.getKey() + "Source), "
						+ "ADR(" + entry.getKey() + "Target), "
						+ "(SizeOf(" + entry.getKey() + "Source)/2)-1)"
						+ " <> -1) OR changed;\n");
			}
			writer.write("\tUNTIL NOT changed\n"
					+ "END_REPEAT\n\n");
			writer.write("changed := TRUE;\n"
					+ "index := BinarySearch(ADR(changed), ADR(source), ADR(target), (SizeOf(source)/2)-1);\n\n"
					+ "IF index <> -1 THEN\n");
			for(String auxControllerName : auxControllers) {
				writer.write("\tIF " + auxControllerName + "Target[index] <> 0 THEN\n"
						+ "\t\t" + auxControllerName + AUX_STM_STATE_SUFFIX + " := " + auxControllerName + "Target[index];\n"
						+ "\tEND_IF\n");
			}
			writer.write("END_IF\n\n"
					+ "controller := TRUE;\n");
			writer.close();
		} catch(IOException e) {
			e.printStackTrace();
		}		
	}
	
	Map<String, List<Mapping>> createEnvironmentTransitionMappings() {
		Map<String, List<Mapping>> result = new HashMap<String, List<Mapping>>();
		Map<String, Set<Transition>> envTransitionsMap = new HashMap<String, Set<Transition>>();
		
		for(SMLRuntimeState state : controller.getStates()) {
			for(Transition t : state.getOutgoingTransition()) {
				if(!Helper.isControllable(controller, t.getEvent())) {
					String key = Helper.createIDFromEvent((MessageEvent)t.getEvent());
					Set<Transition> transitions = envTransitionsMap.get(key);
					
					if(transitions == null) {
						transitions = new HashSet<Transition>();
						envTransitionsMap.put(key, transitions);
					}
					
					transitions.add(t);
				}
			}
		}
		
		for(Entry<String, Set<Transition>> entry : envTransitionsMap.entrySet()) {
			List<Mapping> map = new ArrayList<Mapping>();
			for(Transition t : entry.getValue())
				map.add(new Mapping(t));
			Collections.sort(map);

			result.put(entry.getKey(), map);
		}
		
		return result;
	}
	
	void writeArray(Writer writer, String name, List<Mapping> array, Function<Mapping, String> f) throws IOException {
		writer.write("\t" + name + " : ARRAY [0.." + (array.size()-1) + "] OF INT := [");
		for(int i = 0; i < array.size(); i++)
			writer.write(f.apply(array.get(i)) + (i < array.size()-1 ? ", " : "];\n"));		
	}
}
