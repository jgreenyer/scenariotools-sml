package org.scenariotools.plc.codegen;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.ParameterValue;
import org.scenariotools.sml.runtime.WaitEvent;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.Transition;

import plccodegenerationconfiguration.EventPair;

/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
class Helper {
	static boolean containsUnifiableEvent(Collection<MessageEvent> collection, MessageEvent event) {
		for(MessageEvent messageEvent : collection) {
			if(messageEvent.isParameterUnifiableWith(event))
				return true;
		}
		
		return false;
	}
	
	static void remapEvents(EList<EventPair> eventPairs, SMLRuntimeStateGraph controller) {
		Map<String, MessageEvent> eventMap = new HashMap<String, MessageEvent>();
		
		for(EventPair pair : eventPairs) {
			eventMap.put(pair.getFirst().toString(), pair.getFirst());
			eventMap.put(pair.getSecond().toString(), pair.getSecond());
		}
		
		for(SMLRuntimeState state : controller.getStates()) {
			for(Transition transition : state.getOutgoingTransition()) {
				if(transition.getEvent() instanceof WaitEvent)
					continue; // ignore 'wait' events
					
				MessageEvent event = (MessageEvent)transition.getEvent();
				String eventString = event.toString();
				
				if(!eventMap.containsKey(eventString))
					eventMap.put(eventString, event);
				
				transition.setEvent(eventMap.get(eventString));
			}
		}
	}
	
	static boolean isControllable(SMLRuntimeStateGraph controller, Event event) {
		if(event instanceof WaitEvent)
			return true; // system explicitly waits
		return controller.getConfiguration().getSpecification().getControllableEClasses().contains(((MessageEvent)event).getSendingObject().eClass());
	}
	
	static boolean isControllable(SMLRuntimeStateGraph controller, SMLRuntimeState state) {
		if(state.getOutgoingTransition().size() > 0)
			return isControllable(controller, state.getOutgoingTransition().get(0).getEvent());
		
		return false;
	}

	static String createIDFromEvent(MessageEvent event) {
		String sender = getName(event.getSendingObject());
		String receiver = getName(event.getReceivingObject());
		String element = event.getTypedElement().getName();
		String result = sender + "_" + receiver + "_" + element;
		
		if(event.getParameterValues().size() > 0) {
			for(ParameterValue parameterValue : event.getParameterValues()) {
				Object value = parameterValue.getValue();
				if(value instanceof EObject)
					value = getName((EObject)value);
				result = result + "_" + value.toString();									
			}
		}
		
		return result;
	}
	
	static String getName(EObject theObject) {
		EClass theClass = theObject.eClass();
		EStructuralFeature feature = theClass.getEStructuralFeature("name");
		if(feature != null) {
			return (String)theObject.eGet(feature);
		} else {
			return "unnamedObject";
		}
	}
}
