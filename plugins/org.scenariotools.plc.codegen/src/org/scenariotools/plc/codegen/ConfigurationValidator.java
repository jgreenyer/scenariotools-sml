package org.scenariotools.plc.codegen;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.WaitEvent;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.Transition;

import plccodegenerationconfiguration.EventPair;
import plccodegenerationconfiguration.PLCCodeGenerationConfiguration;

/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
public class ConfigurationValidator {
	private boolean valid;
	private String message;
	
	public ConfigurationValidator(PLCCodeGenerationConfiguration config) {
		valid = true;
		Set<MessageEvent> systemEvents = new HashSet<MessageEvent>();
		
		for(SMLRuntimeState state : config.getController().getStates()) {
			for(Transition transition : state.getOutgoingTransition()) {
				if(transition.getEvent() instanceof WaitEvent)
					continue; // ignore 'wait' events
				MessageEvent event = (MessageEvent)transition.getEvent();
				if(Helper.containsUnifiableEvent(config.getEventsToRemove(), event)) {
					if(state.getOutgoingTransition().size() > 1) {
						invalidate("removal of '" + event + "' not possible.");
						return;
					}
				} else if(Helper.isControllable(config.getController(), event)) {
					systemEvents.add(event);
				}
			}
		}		
	
		for(EventPair eventPair : config.getEventPairs()) {
			MessageEvent first = eventPair.getFirst();
			MessageEvent second = eventPair.getSecond();
			
			if(!Helper.isControllable(config.getController(), first)) {
				invalidate("first message event in pair " + eventPair + " is not a system event.");
				return;
			}
			
			if(first.getSendingObject() != second.getReceivingObject() || first.getReceivingObject() != second.getSendingObject()) {
				invalidate("pair " + eventPair + " is invalid (sending/receiving objects do not match).");
				return;
			}
			
			if(Helper.containsUnifiableEvent(config.getEventsToRemove(), first) || Helper.containsUnifiableEvent(config.getEventsToRemove(), second)) {
				invalidate("pair " + eventPair + " contains message event flagged for removal.");
				return;
			}
			
			remove(systemEvents, first);
			remove(systemEvents, second);
		}
		
		systemEvents.remove(null); // remove 'wait' event
		
		if(systemEvents.size() > 0) {
			MessageEvent event = systemEvents.iterator().next();
			invalidate("Found at least one system event (" + event + ") which is not part of an event pair.");
			return;
		}
	}
	
	private void invalidate(String message) {
		valid = false;
		this.message = message;
	}
	
	private static void remove(Set<MessageEvent> systemEvents, MessageEvent event) {
		for(Iterator<MessageEvent> iter = systemEvents.iterator(); iter.hasNext();) {
			MessageEvent messageEvent = iter.next();
			if(messageEvent == null)
				continue; // ignore 'wait' event
			if(messageEvent.isParameterUnifiableWith(event))
				iter.remove();
		}
	}

	public boolean isValid() {
		return valid;
	}
	
	public String getMessage() {
		return message;
	}
}
