package org.scenariotools.plc.codegen.ui.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.Alternative;
import org.scenariotools.sml.Case;
import org.scenariotools.sml.Collaboration;
import org.scenariotools.sml.Interaction;
import org.scenariotools.sml.InteractionFragment;
import org.scenariotools.sml.Loop;
import org.scenariotools.sml.ModalMessage;
import org.scenariotools.sml.Parallel;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.eclipse.swt.widgets.List;

import plccodegenerationconfiguration.EventPair;

/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
public class PLCCodeGenUIHelper {
	public static String getPairKey(EventPair pair) {
		return pair.getFirst().toString() + ", " + pair.getSecond().toString();
	}

	public static void sort(List list, Map<String, MessageEvent> eventMap, SMLRuntimeStateGraph controller) {
		java.util.List<String> items = Arrays.asList(list.getItems());
		Collections.sort(items);
		
		if(eventMap != null && controller != null) { // apply additional sorting (eventMap and controller are only non-null if this is desired)
			int front = 0;
			for(int i = 0; i < items.size(); i++) {
				if(swapToFront(eventMap.get(items.get(i)), controller)) {
					Collections.swap(items, i, front);
					front += 1;
				}
			}
		}
		
		list.setItems((String[])items.toArray());
	}

	private static boolean swapToFront(MessageEvent messageEvent, SMLRuntimeStateGraph controller) {
		boolean senderIsControllable = controller.getConfiguration().getSpecification().getControllableEClasses().contains(messageEvent.getSendingObject().eClass());
		boolean receiverIsControllable = controller.getConfiguration().getSpecification().getControllableEClasses().contains(messageEvent.getReceivingObject().eClass());
		
		return (!senderIsControllable && !receiverIsControllable) || (messageEvent.getTypedElement() instanceof EStructuralFeature);
	}

	public static void sort(java.util.List<MessageEvent> list) {
		Collections.sort(list, new Comparator<MessageEvent>() {
			@Override
			public int compare(MessageEvent e1, MessageEvent e2) {
				String n1 = e1.getTypedElement().getName();
				String n2 = e2.getTypedElement().getName();
				return n1.compareToIgnoreCase(n2);
			}
		});		
	}
	
	public static java.util.List<java.util.List<ETypedElement>> flattenScenarios(SMLRuntimeStateGraph controller) {
		java.util.List<java.util.List<ETypedElement>> result = new ArrayList<java.util.List<ETypedElement>>();
		
		for(Collaboration collaboration : controller.getConfiguration().getSpecification().getCollaborations()) {
			for(Scenario scenario : collaboration.getScenarios()) {
				result.add(flattenInteraction(scenario.getOwnedInteraction()));
			}
		}
		
		return result;
	}
	
	private static java.util.List<ETypedElement> flattenInteraction(Interaction interaction) {
		java.util.List<ETypedElement> result = new ArrayList<ETypedElement>();
		
		for (InteractionFragment fragment : interaction.getFragments()) {
			if(fragment instanceof ModalMessage) {
				result.add(((ModalMessage)fragment).getModelElement());
			} else if(fragment instanceof Interaction) {
				result.addAll(flattenInteraction((Interaction)fragment));
			} else if(fragment instanceof Loop) {
				result.addAll(flattenInteraction(((Loop)fragment).getBodyInteraction()));
			} else if(fragment instanceof Alternative) {
				Alternative alternative = (Alternative)fragment;
				for(Case theCase : alternative.getCases()) {
					result.addAll(flattenInteraction(theCase.getCaseInteraction()));
				}
			} else if(fragment instanceof Parallel) {
				Parallel parallel = (Parallel)fragment;
				for(Interaction nestedInteraction : parallel.getParallelInteraction()) {
					result.addAll(flattenInteraction(nestedInteraction));
				}
			}
		}
		
		return result;
	}
	
	public static Integer getDistance(MessageEvent event1, MessageEvent event2, java.util.List<java.util.List<ETypedElement>> scenarios) {
		int result = Integer.MAX_VALUE;
		
		ETypedElement element1 = event1.getTypedElement();
		ETypedElement element2 = event2.getTypedElement();
		
		for(java.util.List<ETypedElement> scenario : scenarios) {
			java.util.List<Integer> o1 = getOccurrences(element1, scenario);
			java.util.List<Integer> o2 = getOccurrences(element2, scenario);
			int o2size = o2.size();
			
			for(Integer i : o1) {
				int j = Collections.binarySearch(o2, i);
				if(j < 0)
					j = -(j+1);
				// j is now the index of the first entry in o2 such that o2.get(j) >= i; j == o2size iff all elements in o2 are <i
				if(j < o2size)
					result = Math.min(o2.get(j) - i, result);
			}
		}
		
		return result;
	}
	
	private static java.util.List<Integer> getOccurrences(ETypedElement element, java.util.List<ETypedElement> scenario) {
		java.util.List<Integer> result = new ArrayList<Integer>();

		for(int i = 0; i < scenario.size(); i++) {
			if(scenario.get(i) == element)
				result.add(i);
		}
		
		return result;
	}
	
	public static void sort(java.util.List<MessageEvent> list, Map<MessageEvent, Integer> distances) {
		Collections.sort(list, new Comparator<MessageEvent>() { // Collections.sort(...) is guaranteed to be stable (this is important!)
			@Override
			public int compare(MessageEvent e1, MessageEvent e2) {
				return distances.get(e1) - distances.get(e2);
			}
		});		
	}
}
