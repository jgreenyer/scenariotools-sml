package org.scenariotools.plc.codegen.ui.editor;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.part.FileEditorInput;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.WaitEvent;
import org.scenariotools.plc.codegen.ConfigurationValidator;
import org.scenariotools.plc.codegen.ui.helper.PLCCodeGenUIHelper;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.Transition;

import plccodegenerationconfiguration.EventPair;
import plccodegenerationconfiguration.PLCCodeGenerationConfiguration;
import plccodegenerationconfiguration.PlccodegenerationconfigurationFactory;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;

/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
public class PLCCodeGenConfigEditor extends EditorPart {
	private boolean dirty;
	private URI configURI;
	private PLCCodeGenerationConfiguration config;
	private Map<String, MessageEvent> eventMap;
	private Map<String, EventPair> eventPairMap;
	private Set<MessageEvent> systemEvents;
	
	private Label controllerLabel;
	private List eventsToRemove;
	private List candidateEventsToRemove;
	private List candidateEventsForPairing;
	private List eventPairs;
	private Button addEventsToRemoveButton;
	private Button removeEventsToRemoveButton;
	private Button addCustomEventsToRemoveButton;
	private Button addEventPairButton;
	private Button removeEventPairsButton;
	public PLCCodeGenConfigEditor() {
	}
	@Override
    public void createPartControl(Composite parent) {
		dirty = false;
		
		ScrolledComposite scrolledComposite = new ScrolledComposite(parent, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);
		
		Composite composite = new Composite(scrolledComposite, SWT.NONE);
		composite.setLayout(new GridLayout(1, false));
		
		Composite composite_5 = new Composite(composite, SWT.NONE);
		composite_5.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		composite_5.setLayout(new GridLayout(2, false));
		
		Label lblNewLabel = new Label(composite_5, SWT.NONE);
		lblNewLabel.setBounds(0, 0, 55, 15);
		lblNewLabel.setText("Controller:");
		
		controllerLabel = new Label(composite_5, SWT.NONE);
		GridData gd_controllerLabel = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_controllerLabel.widthHint = 113;
		controllerLabel.setLayoutData(gd_controllerLabel);
		controllerLabel.setText("<undefined>");
		
		Label lblNewLabel_1 = new Label(composite, SWT.NONE);
		GridData gd_lblNewLabel_1 = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_lblNewLabel_1.verticalIndent = 20;
		lblNewLabel_1.setLayoutData(gd_lblNewLabel_1);
		lblNewLabel_1.setText("Events to Remove:");
		
		Composite eventsToRemoveComposite = new Composite(composite, SWT.NONE);
		GridData gd_eventsToRemoveComposite = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_eventsToRemoveComposite.heightHint = 400;
		eventsToRemoveComposite.setLayoutData(gd_eventsToRemoveComposite);
		eventsToRemoveComposite.setBounds(0, 0, 64, 64);
		GridLayout gl_eventsToRemoveComposite = new GridLayout(3, false);
		eventsToRemoveComposite.setLayout(gl_eventsToRemoveComposite);
		
		eventsToRemove = new List(eventsToRemoveComposite, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI);
		eventsToRemove.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				removeEventsToRemoveButton.notifyListeners(SWT.Selection, null);
			}
		});
		GridData gd_eventsToRemove = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_eventsToRemove.widthHint = 400;
		eventsToRemove.setLayoutData(gd_eventsToRemove);
		eventsToRemove.setItems(new String[] {});
		eventsToRemove.setBounds(0, 0, 71, 68);
		
		Composite composite_2 = new Composite(eventsToRemoveComposite, SWT.NONE);
		composite_2.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		composite_2.setLayout(new GridLayout(1, false));
		
		addEventsToRemoveButton = new Button(composite_2, SWT.NONE);
		addEventsToRemoveButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(candidateEventsToRemove.getSelectionCount() == 0)
					return;
				
				setDirty(true);
				
				for(String selection : candidateEventsToRemove.getSelection()) {
					eventsToRemove.add(selection);
					candidateEventsToRemove.remove(selection);
					config.getEventsToRemove().add(eventMap.get(selection));
				}
				
				PLCCodeGenUIHelper.sort(eventsToRemove, null, null);
			}
		});
		GridData gd_addEventsToRemoveButton = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_addEventsToRemoveButton.widthHint = 100;
		addEventsToRemoveButton.setLayoutData(gd_addEventsToRemoveButton);
		addEventsToRemoveButton.setText("<< Add");
		
		removeEventsToRemoveButton = new Button(composite_2, SWT.NONE);
		removeEventsToRemoveButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(eventsToRemove.getSelectionCount() == 0)
					return;
				
				setDirty(true);
				
				for(String selection : eventsToRemove.getSelection()) {
					eventsToRemove.remove(selection);
					candidateEventsToRemove.add(selection);
					config.getEventsToRemove().remove(eventMap.get(selection));
				}
				
				PLCCodeGenUIHelper.sort(candidateEventsToRemove, eventMap, config.getController());
			}
		});
		GridData gd_removeEventsToRemoveButton = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_removeEventsToRemoveButton.widthHint = 100;
		removeEventsToRemoveButton.setLayoutData(gd_removeEventsToRemoveButton);
		removeEventsToRemoveButton.setText("Remove >>");
		
		addCustomEventsToRemoveButton = new Button(composite_2, SWT.NONE);
		GridData gd_addCustomEventsToRemoveButton = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_addCustomEventsToRemoveButton.widthHint = 100;
		addCustomEventsToRemoveButton.setLayoutData(gd_addCustomEventsToRemoveButton);
		addCustomEventsToRemoveButton.setEnabled(false);
		addCustomEventsToRemoveButton.setText("Add Custom");
		
		candidateEventsToRemove = new List(eventsToRemoveComposite, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI);
		candidateEventsToRemove.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				addEventsToRemoveButton.notifyListeners(SWT.Selection, null);
			}
		});
		GridData gd_candidateEventsToRemove = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_candidateEventsToRemove.widthHint = 400;
		candidateEventsToRemove.setLayoutData(gd_candidateEventsToRemove);
		
		Label lblNewLabel_2 = new Label(composite, SWT.NONE);
		GridData gd_lblNewLabel_2 = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_lblNewLabel_2.verticalIndent = 20;
		lblNewLabel_2.setLayoutData(gd_lblNewLabel_2);
		lblNewLabel_2.setText("Event Pairs:");
		
		Composite eventPairComposite = new Composite(composite, SWT.NONE);
		GridData gd_eventPairComposite = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_eventPairComposite.heightHint = 400;
		eventPairComposite.setLayoutData(gd_eventPairComposite);
		eventPairComposite.setLayout(new GridLayout(3, false));
		
		eventPairs = new List(eventPairComposite, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI);
		eventPairs.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				removeEventPairsButton.notifyListeners(SWT.Selection, null);
			}
		});
		GridData gd_eventPairs = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_eventPairs.widthHint = 400;
		eventPairs.setLayoutData(gd_eventPairs);
		
		Composite composite_4 = new Composite(eventPairComposite, SWT.NONE);
		composite_4.setLayout(new GridLayout(1, false));
		
		addEventPairButton = new Button(composite_4, SWT.NONE);
		addEventPairButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(candidateEventsForPairing.getSelectionCount() == 0)
					return;
				
				String firstKey = candidateEventsForPairing.getSelection()[0];
				MessageEvent first = eventMap.get(firstKey);
				
				Object result = new AddEventPairDialog(getSite().getShell(), SWT.DEFAULT, first, eventMap.values(), eventPairMap.values(), config.getController()).open();
				if(result == null)
					return;
				
				setDirty(true);
				
				String secondKey = (String)result;
				MessageEvent second = eventMap.get(secondKey);
				
				EventPair pair = PlccodegenerationconfigurationFactory.eINSTANCE.createEventPair();
				pair.setFirst(first);
				pair.setSecond(second);
				
				String pairKey = PLCCodeGenUIHelper.getPairKey(pair);
				eventPairs.add(pairKey);
				eventPairMap.put(pairKey, pair);
				config.getEventPairs().add(pair);
				
				candidateEventsForPairing.remove(firstKey);
				if(systemEvents.contains(second))
					candidateEventsForPairing.remove(secondKey);
								
				PLCCodeGenUIHelper.sort(eventPairs, null, null);
			}
		});
		GridData gd_addEventPairButton = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_addEventPairButton.widthHint = 100;
		addEventPairButton.setLayoutData(gd_addEventPairButton);
		addEventPairButton.setText("<< Add");
		
		removeEventPairsButton = new Button(composite_4, SWT.NONE);
		removeEventPairsButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(eventPairs.getSelectionCount() == 0)
					return;
				
				setDirty(true);
				
				for(String selection : eventPairs.getSelection()) {
					EventPair pair = eventPairMap.get(selection);
					
					eventPairs.remove(selection);
					eventPairMap.remove(selection);
					config.getEventPairs().remove(pair);
					
					candidateEventsForPairing.add(pair.getFirst().toString());
					if(systemEvents.contains(pair.getSecond()))
						candidateEventsForPairing.add(pair.getSecond().toString());

					PLCCodeGenUIHelper.sort(candidateEventsForPairing, null, null);
				}
			}
		});
		GridData gd_removeEventPairsButton = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_removeEventPairsButton.widthHint = 100;
		removeEventPairsButton.setLayoutData(gd_removeEventPairsButton);
		removeEventPairsButton.setText("Remove >>");
		
		candidateEventsForPairing = new List(eventPairComposite, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		candidateEventsForPairing.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				addEventPairButton.notifyListeners(SWT.Selection, null);
			}
		});
		GridData gd_candidateEventsForPairing = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_candidateEventsForPairing.widthHint = 400;
		candidateEventsForPairing.setLayoutData(gd_candidateEventsForPairing);
		scrolledComposite.setMinSize(new Point(1000, 930));
		scrolledComposite.setContent(composite);
		
		initEditor();
    }
    
	@Override
    public void init(IEditorSite site, IEditorInput input) {
		setSite(site);
		setInput(input);
    }
    
	@Override
    public void setFocus() {
		if(eventsToRemove != null)
			eventsToRemove.setFocus();
    }
    
	@Override
	public void doSave(IProgressMonitor monitor) {
		ConfigurationValidator validator = new ConfigurationValidator(config);
		if(!validator.isValid()) {
			MessageDialog.openError(getSite().getShell(), "Invalid configuration", validator.getMessage());
			return;
		}		
		
		try {
			ResourceSet rSet = new ResourceSetImpl();
			Resource res = rSet.createResource(configURI);
			
			res.getContents().add(config);
			res.save(new HashMap<Object, Object>());
			
			setDirty(false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub
	}
	
	private void setDirty(boolean dirty) {
		this.dirty = dirty;
		firePropertyChange(PROP_DIRTY);
	}
	
	@Override
	public boolean isDirty() {
		return dirty;
	}
	
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}
	
	private void initEditor() {
		IEditorInput editorInput = getEditorInput();
		setPartName(editorInput.getName()); // set name of 'tab'
		
		if(editorInput instanceof FileEditorInput) {
			// determine file to edit
			configURI = URI.createFileURI(((FileEditorInput)editorInput).getPath().toString());
			
			loadResources();
			initEventMapAndSet();
			initLists();
		}
	}
	
	private void loadResources() {
		try {
			// load file to edit
			ResourceSet rSet = new ResourceSetImpl();
			rSet.getURIConverter().getURIMap().putAll(EcorePlugin.computePlatformURIMap(true));
			Resource res = rSet.createResource(configURI);
			res.load(Collections.EMPTY_MAP);
			config = (PLCCodeGenerationConfiguration)res.getContents().get(0);
			String controllerPathString = config.getController().eResource().getURI().toFileString();
			IFile controllerFile = ResourcesPlugin.getWorkspace().getRoot().getFileForLocation(Path.fromOSString(controllerPathString));
			controllerLabel.setText("/" + controllerFile.getProjectRelativePath().toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	private void initEventMapAndSet() {
		// create map of all events occurring in the controller
		eventMap = new HashMap<String, MessageEvent>();
		systemEvents = new HashSet<MessageEvent>();
		
		for(SMLRuntimeState state : config.getController().getStates()) {
			for(Transition transition : state.getOutgoingTransition()) {
				if(transition.getEvent() instanceof WaitEvent)
					continue; // ignore 'wait' events
					
				MessageEvent event = (MessageEvent)transition.getEvent();
				String key = event.toString();
				if(!eventMap.containsKey(key)) {
					eventMap.put(key, event);
					remapEvent(event); // make sure lists in the current config to edit point to the same events
					
					SMLRuntimeState sourceState = (SMLRuntimeState)transition.getSourceState();
					if(!sourceState.getObjectSystem().isEnvironmentMessageEvent(event))
						systemEvents.add(event);
				}
			}
		}		
	}
	
	private void remapEvent(MessageEvent newEvent) {
		for(int i = 0; i < config.getEventsToRemove().size(); i++) {
			MessageEvent messageEvent = config.getEventsToRemove().get(i);
			if(messageEvent.isParameterUnifiableWith(newEvent)) {
				config.getEventsToRemove().set(i, newEvent);
				break; // assumes there is only at most one unifiable event in the list
			}
		}
		
		for(EventPair eventPair : config.getEventPairs()) {
			if(eventPair.getFirst().isParameterUnifiableWith(newEvent)) {
				eventPair.setFirst(newEvent);
				break; // assumes newEvent occurs in at most one pair and is either first or second, never both
			} else if(eventPair.getSecond().isParameterUnifiableWith(newEvent)) {
				eventPair.setSecond(newEvent);
				break; // assumes newEvent occurs in at most one pair and is either first or second, never both
			}			
		}
	}
	
	private void initLists() {
		eventPairMap = new HashMap<String, EventPair>();
		
		// update lists in GUI
		for(Entry<String, MessageEvent> entry : eventMap.entrySet()) {
			String key = entry.getKey();
			MessageEvent event = entry.getValue();
			
			if(config.getEventsToRemove().contains(event)) {
				eventsToRemove.add(key);
			} else {
				candidateEventsToRemove.add(key);
			}
			
			if(!systemEvents.contains(event))
				continue;
			
			boolean found = false;
			for (EventPair eventPair : config.getEventPairs()) {
				if(eventPair.getFirst() == event) {
					String pairKey = PLCCodeGenUIHelper.getPairKey(eventPair);
					
					eventPairs.add(pairKey);
					eventPairMap.put(pairKey, eventPair);
					
					found = true;
					break;
				} else if(eventPair.getSecond() == event) {
					found = true;
					break;
				}
			}
			
			if(!found) {
				candidateEventsForPairing.add(key);
			}
		}
		
		PLCCodeGenUIHelper.sort(eventsToRemove, null, null);
		PLCCodeGenUIHelper.sort(candidateEventsToRemove, eventMap, config.getController());
		PLCCodeGenUIHelper.sort(eventPairs, null, null);
		PLCCodeGenUIHelper.sort(candidateEventsForPairing, null, null);
	}
 }
