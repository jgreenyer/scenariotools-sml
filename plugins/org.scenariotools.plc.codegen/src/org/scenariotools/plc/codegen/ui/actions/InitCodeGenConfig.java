package org.scenariotools.plc.codegen.ui.actions;

import java.io.IOException;
import java.util.Collections;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;

import plccodegenerationconfiguration.PLCCodeGenerationConfiguration;
import plccodegenerationconfiguration.PlccodegenerationconfigurationFactory;

/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
public class InitCodeGenConfig implements IObjectActionDelegate {
	@Override
	public void run(IAction action) {
		IStructuredSelection structuredSelection = (IStructuredSelection)PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().getSelection();
		final IFile file = (IFile)structuredSelection.getFirstElement();
		
		try {
			ResourceSet rSet = new ResourceSetImpl();
			Resource res = rSet.createResource(URI.createPlatformResourceURI(file.getFullPath().toString(), true));
			res.load(Collections.EMPTY_MAP);

			PLCCodeGenerationConfiguration configuration = PlccodegenerationconfigurationFactory.eINSTANCE.createPLCCodeGenerationConfiguration();
			configuration.setController((SMLRuntimeStateGraph)res.getContents().get(0));
			
			URI configURI = URI.createPlatformResourceURI(file.getFullPath().removeFileExtension().addFileExtension("plcgenconfig").toString(), true);
			res = rSet.createResource(configURI);

			res.getContents().add(configuration);
			res.save(Collections.EMPTY_MAP);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// TODO Auto-generated method stub
	}
}
