package org.scenariotools.plc.codegen.ui.editor;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.plc.codegen.ui.helper.PLCCodeGenUIHelper;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;

import plccodegenerationconfiguration.EventPair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;

/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
public class AddEventPairDialog extends Dialog {

	protected Object result;
	protected Shell shell;
	
	private MessageEvent firstEvent;
	private Collection<MessageEvent> events;
	private Collection<EventPair> eventPairs;
	private SMLRuntimeStateGraph controller;
	
	private Label firstEventLabel;
	private List eventsList;
	private Button addButton;

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public AddEventPairDialog(Shell parent, int style, MessageEvent firstEvent, Collection<MessageEvent> events, Collection<EventPair> eventPairs, SMLRuntimeStateGraph controller) {
		super(parent, style);
		setText("Add Event Pair");
		this.firstEvent = firstEvent;
		this.events = events;
		this.eventPairs = eventPairs;
		this.controller = controller;
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		shell.setSize(651, 299);
		shell.setText(getText());
		shell.setLayout(new GridLayout(1, false));
		
		Composite composite = new Composite(shell, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite.setLayout(new GridLayout(1, false));
		
		Composite composite_1 = new Composite(composite, SWT.NONE);
		composite_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		composite_1.setLayout(new GridLayout(2, false));
		
		Label lblChooseEventTo = new Label(composite_1, SWT.NONE);
		lblChooseEventTo.setText("Choose event to pair with");
		
		firstEventLabel = new Label(composite_1, SWT.NONE);
		firstEventLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		firstEventLabel.setText("<undefined>");
		
		eventsList = new List(composite, SWT.BORDER | SWT.V_SCROLL);
		eventsList.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				addButton.notifyListeners(SWT.Selection, null);
			}
		});
		GridData gd_eventsList = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gd_eventsList.heightHint = 187;
		eventsList.setLayoutData(gd_eventsList);
		
		addButton = new Button(composite, SWT.NONE);
		addButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(eventsList.getSelectionCount() > 0)
					result = eventsList.getSelection()[0];
				shell.close();
			}
		});
		GridData gd_addButton = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		gd_addButton.widthHint = 70;
		addButton.setLayoutData(gd_addButton);
		addButton.setText("Add");

		initDialog();
	}
	
	private void initDialog() {
		firstEventLabel.setText(firstEvent.toString());
		
		java.util.List<MessageEvent> sortedEvents = new ArrayList<MessageEvent>(events.size());
		
		// filter events which cannot be a partner for pairing or are already paired
		outer:
		for(MessageEvent event : events) {
			if(event == firstEvent)
				continue;
			
			for(EventPair pair : eventPairs) {
				if(pair.getFirst() == event || pair.getSecond() == event)
					continue outer;
			}
			
			if(event.getReceivingObject() == firstEvent.getSendingObject() && event.getSendingObject() == firstEvent.getReceivingObject())
				sortedEvents.add(event);
		}
		
		// sort events alphabetically
		PLCCodeGenUIHelper.sort(sortedEvents);
		
		// sort events based on smallest "distance"
		// - events are closer if there are fewer messages in between them in a scenario
		java.util.List<java.util.List<ETypedElement>> scenarios = PLCCodeGenUIHelper.flattenScenarios(controller);
		Map<MessageEvent, Integer> distances = new HashMap<MessageEvent, Integer>();
		for(MessageEvent event : sortedEvents) {
			distances.put(event, PLCCodeGenUIHelper.getDistance(firstEvent, event, scenarios));
		}
		PLCCodeGenUIHelper.sort(sortedEvents, distances);
		
		// add sorted events to GUI list
		for(MessageEvent event : sortedEvents)
			eventsList.add(event.toString());
	}
}
