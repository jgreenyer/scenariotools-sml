/**
 */
package plccodegenerationconfiguration;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Pair</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link plccodegenerationconfiguration.EventPair#getFirst <em>First</em>}</li>
 *   <li>{@link plccodegenerationconfiguration.EventPair#getSecond <em>Second</em>}</li>
 * </ul>
 *
 * @see plccodegenerationconfiguration.PlccodegenerationconfigurationPackage#getEventPair()
 * @model
 * @generated
 */
public interface EventPair extends EObject {
	/**
	 * Returns the value of the '<em><b>First</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First</em>' reference.
	 * @see #setFirst(org.scenariotools.sml.runtime.MessageEvent)
	 * @see plccodegenerationconfiguration.PlccodegenerationconfigurationPackage#getEventPair_First()
	 * @model required="true"
	 * @generated
	 */
	org.scenariotools.sml.runtime.MessageEvent getFirst();

	/**
	 * Sets the value of the '{@link plccodegenerationconfiguration.EventPair#getFirst <em>First</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First</em>' reference.
	 * @see #getFirst()
	 * @generated
	 */
	void setFirst(org.scenariotools.sml.runtime.MessageEvent value);

	/**
	 * Returns the value of the '<em><b>Second</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Second</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Second</em>' reference.
	 * @see #setSecond(org.scenariotools.sml.runtime.MessageEvent)
	 * @see plccodegenerationconfiguration.PlccodegenerationconfigurationPackage#getEventPair_Second()
	 * @model required="true"
	 * @generated
	 */
	org.scenariotools.sml.runtime.MessageEvent getSecond();

	/**
	 * Sets the value of the '{@link plccodegenerationconfiguration.EventPair#getSecond <em>Second</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Second</em>' reference.
	 * @see #getSecond()
	 * @generated
	 */
	void setSecond(org.scenariotools.sml.runtime.MessageEvent value);

} // EventPair
