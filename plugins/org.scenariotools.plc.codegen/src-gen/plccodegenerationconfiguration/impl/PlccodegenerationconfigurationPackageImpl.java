/**
 */
package plccodegenerationconfiguration.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.scenariotools.sml.SmlPackage;
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage;
import org.scenariotools.sml.runtime.RuntimePackage;

import org.scenariotools.sml.runtime.configuration.ConfigurationPackage;
import plccodegenerationconfiguration.EventPair;
import plccodegenerationconfiguration.PLCCodeGenerationConfiguration;
import plccodegenerationconfiguration.PlccodegenerationconfigurationFactory;
import plccodegenerationconfiguration.PlccodegenerationconfigurationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PlccodegenerationconfigurationPackageImpl extends EPackageImpl implements PlccodegenerationconfigurationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass plcCodeGenerationConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventPairEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see plccodegenerationconfiguration.PlccodegenerationconfigurationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private PlccodegenerationconfigurationPackageImpl() {
		super(eNS_URI, PlccodegenerationconfigurationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link PlccodegenerationconfigurationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static PlccodegenerationconfigurationPackage init() {
		if (isInited) return (PlccodegenerationconfigurationPackage)EPackage.Registry.INSTANCE.getEPackage(PlccodegenerationconfigurationPackage.eNS_URI);

		// Obtain or create and register package
		PlccodegenerationconfigurationPackageImpl thePlccodegenerationconfigurationPackage = (PlccodegenerationconfigurationPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof PlccodegenerationconfigurationPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new PlccodegenerationconfigurationPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ConfigurationPackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		RuntimePackage.eINSTANCE.eClass();
		ScenarioExpressionsPackage.eINSTANCE.eClass();
		SmlPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		thePlccodegenerationconfigurationPackage.createPackageContents();

		// Initialize created meta-data
		thePlccodegenerationconfigurationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		thePlccodegenerationconfigurationPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(PlccodegenerationconfigurationPackage.eNS_URI, thePlccodegenerationconfigurationPackage);
		return thePlccodegenerationconfigurationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPLCCodeGenerationConfiguration() {
		return plcCodeGenerationConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPLCCodeGenerationConfiguration_Controller() {
		return (EReference)plcCodeGenerationConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPLCCodeGenerationConfiguration_EventsToRemove() {
		return (EReference)plcCodeGenerationConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPLCCodeGenerationConfiguration_EventPairs() {
		return (EReference)plcCodeGenerationConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventPair() {
		return eventPairEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventPair_First() {
		return (EReference)eventPairEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventPair_Second() {
		return (EReference)eventPairEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PlccodegenerationconfigurationFactory getPlccodegenerationconfigurationFactory() {
		return (PlccodegenerationconfigurationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		plcCodeGenerationConfigurationEClass = createEClass(PLC_CODE_GENERATION_CONFIGURATION);
		createEReference(plcCodeGenerationConfigurationEClass, PLC_CODE_GENERATION_CONFIGURATION__CONTROLLER);
		createEReference(plcCodeGenerationConfigurationEClass, PLC_CODE_GENERATION_CONFIGURATION__EVENTS_TO_REMOVE);
		createEReference(plcCodeGenerationConfigurationEClass, PLC_CODE_GENERATION_CONFIGURATION__EVENT_PAIRS);

		eventPairEClass = createEClass(EVENT_PAIR);
		createEReference(eventPairEClass, EVENT_PAIR__FIRST);
		createEReference(eventPairEClass, EVENT_PAIR__SECOND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		RuntimePackage theRuntimePackage = (RuntimePackage)EPackage.Registry.INSTANCE.getEPackage(RuntimePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(plcCodeGenerationConfigurationEClass, PLCCodeGenerationConfiguration.class, "PLCCodeGenerationConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPLCCodeGenerationConfiguration_Controller(), theRuntimePackage.getSMLRuntimeStateGraph(), null, "controller", null, 1, 1, PLCCodeGenerationConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPLCCodeGenerationConfiguration_EventsToRemove(), theRuntimePackage.getMessageEvent(), null, "eventsToRemove", null, 0, -1, PLCCodeGenerationConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPLCCodeGenerationConfiguration_EventPairs(), this.getEventPair(), null, "eventPairs", null, 0, -1, PLCCodeGenerationConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventPairEClass, EventPair.class, "EventPair", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEventPair_First(), theRuntimePackage.getMessageEvent(), null, "first", null, 1, 1, EventPair.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEventPair_Second(), theRuntimePackage.getMessageEvent(), null, "second", null, 1, 1, EventPair.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //PlccodegenerationconfigurationPackageImpl
