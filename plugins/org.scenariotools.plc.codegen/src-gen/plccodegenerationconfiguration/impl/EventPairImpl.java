/**
 */
package plccodegenerationconfiguration.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import plccodegenerationconfiguration.EventPair;
import plccodegenerationconfiguration.PlccodegenerationconfigurationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Pair</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link plccodegenerationconfiguration.impl.EventPairImpl#getFirst <em>First</em>}</li>
 *   <li>{@link plccodegenerationconfiguration.impl.EventPairImpl#getSecond <em>Second</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EventPairImpl extends MinimalEObjectImpl.Container implements EventPair {
	/**
	 * The cached value of the '{@link #getFirst() <em>First</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirst()
	 * @generated
	 * @ordered
	 */
	protected org.scenariotools.sml.runtime.MessageEvent first;

	/**
	 * The cached value of the '{@link #getSecond() <em>Second</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSecond()
	 * @generated
	 * @ordered
	 */
	protected org.scenariotools.sml.runtime.MessageEvent second;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventPairImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PlccodegenerationconfigurationPackage.Literals.EVENT_PAIR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.scenariotools.sml.runtime.MessageEvent getFirst() {
		if (first != null && first.eIsProxy()) {
			InternalEObject oldFirst = (InternalEObject)first;
			first = (org.scenariotools.sml.runtime.MessageEvent)eResolveProxy(oldFirst);
			if (first != oldFirst) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PlccodegenerationconfigurationPackage.EVENT_PAIR__FIRST, oldFirst, first));
			}
		}
		return first;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.scenariotools.sml.runtime.MessageEvent basicGetFirst() {
		return first;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirst(org.scenariotools.sml.runtime.MessageEvent newFirst) {
		org.scenariotools.sml.runtime.MessageEvent oldFirst = first;
		first = newFirst;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PlccodegenerationconfigurationPackage.EVENT_PAIR__FIRST, oldFirst, first));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.scenariotools.sml.runtime.MessageEvent getSecond() {
		if (second != null && second.eIsProxy()) {
			InternalEObject oldSecond = (InternalEObject)second;
			second = (org.scenariotools.sml.runtime.MessageEvent)eResolveProxy(oldSecond);
			if (second != oldSecond) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PlccodegenerationconfigurationPackage.EVENT_PAIR__SECOND, oldSecond, second));
			}
		}
		return second;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.scenariotools.sml.runtime.MessageEvent basicGetSecond() {
		return second;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSecond(org.scenariotools.sml.runtime.MessageEvent newSecond) {
		org.scenariotools.sml.runtime.MessageEvent oldSecond = second;
		second = newSecond;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PlccodegenerationconfigurationPackage.EVENT_PAIR__SECOND, oldSecond, second));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PlccodegenerationconfigurationPackage.EVENT_PAIR__FIRST:
				if (resolve) return getFirst();
				return basicGetFirst();
			case PlccodegenerationconfigurationPackage.EVENT_PAIR__SECOND:
				if (resolve) return getSecond();
				return basicGetSecond();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PlccodegenerationconfigurationPackage.EVENT_PAIR__FIRST:
				setFirst((org.scenariotools.sml.runtime.MessageEvent)newValue);
				return;
			case PlccodegenerationconfigurationPackage.EVENT_PAIR__SECOND:
				setSecond((org.scenariotools.sml.runtime.MessageEvent)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PlccodegenerationconfigurationPackage.EVENT_PAIR__FIRST:
				setFirst((org.scenariotools.sml.runtime.MessageEvent)null);
				return;
			case PlccodegenerationconfigurationPackage.EVENT_PAIR__SECOND:
				setSecond((org.scenariotools.sml.runtime.MessageEvent)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PlccodegenerationconfigurationPackage.EVENT_PAIR__FIRST:
				return first != null;
			case PlccodegenerationconfigurationPackage.EVENT_PAIR__SECOND:
				return second != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String toString() {
		String firstString = "<undefined>";
		if(getFirst() != null)
			firstString = getFirst().toString();
		
		String secondString = "<undefined>";
		if(getSecond() != null)
			secondString = getSecond().toString();
		
		return "[" + firstString + ", " + secondString + "]";
	}
} //EventPairImpl
