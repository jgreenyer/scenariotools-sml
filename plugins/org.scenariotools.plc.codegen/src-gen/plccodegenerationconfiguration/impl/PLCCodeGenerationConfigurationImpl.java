/**
 */
package plccodegenerationconfiguration.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;

import plccodegenerationconfiguration.EventPair;
import plccodegenerationconfiguration.PLCCodeGenerationConfiguration;
import plccodegenerationconfiguration.PlccodegenerationconfigurationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PLC Code Generation Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link plccodegenerationconfiguration.impl.PLCCodeGenerationConfigurationImpl#getController <em>Controller</em>}</li>
 *   <li>{@link plccodegenerationconfiguration.impl.PLCCodeGenerationConfigurationImpl#getEventsToRemove <em>Events To Remove</em>}</li>
 *   <li>{@link plccodegenerationconfiguration.impl.PLCCodeGenerationConfigurationImpl#getEventPairs <em>Event Pairs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PLCCodeGenerationConfigurationImpl extends MinimalEObjectImpl.Container implements PLCCodeGenerationConfiguration {
	/**
	 * The cached value of the '{@link #getController() <em>Controller</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getController()
	 * @generated
	 * @ordered
	 */
	protected SMLRuntimeStateGraph controller;

	/**
	 * The cached value of the '{@link #getEventsToRemove() <em>Events To Remove</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventsToRemove()
	 * @generated
	 * @ordered
	 */
	protected EList<org.scenariotools.sml.runtime.MessageEvent> eventsToRemove;

	/**
	 * The cached value of the '{@link #getEventPairs() <em>Event Pairs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventPairs()
	 * @generated
	 * @ordered
	 */
	protected EList<EventPair> eventPairs;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PLCCodeGenerationConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PlccodegenerationconfigurationPackage.Literals.PLC_CODE_GENERATION_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SMLRuntimeStateGraph getController() {
		if (controller != null && controller.eIsProxy()) {
			InternalEObject oldController = (InternalEObject)controller;
			controller = (SMLRuntimeStateGraph)eResolveProxy(oldController);
			if (controller != oldController) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PlccodegenerationconfigurationPackage.PLC_CODE_GENERATION_CONFIGURATION__CONTROLLER, oldController, controller));
			}
		}
		return controller;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SMLRuntimeStateGraph basicGetController() {
		return controller;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setController(SMLRuntimeStateGraph newController) {
		SMLRuntimeStateGraph oldController = controller;
		controller = newController;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PlccodegenerationconfigurationPackage.PLC_CODE_GENERATION_CONFIGURATION__CONTROLLER, oldController, controller));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<org.scenariotools.sml.runtime.MessageEvent> getEventsToRemove() {
		if (eventsToRemove == null) {
			eventsToRemove = new EObjectResolvingEList<org.scenariotools.sml.runtime.MessageEvent>(org.scenariotools.sml.runtime.MessageEvent.class, this, PlccodegenerationconfigurationPackage.PLC_CODE_GENERATION_CONFIGURATION__EVENTS_TO_REMOVE);
		}
		return eventsToRemove;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EventPair> getEventPairs() {
		if (eventPairs == null) {
			eventPairs = new EObjectContainmentEList<EventPair>(EventPair.class, this, PlccodegenerationconfigurationPackage.PLC_CODE_GENERATION_CONFIGURATION__EVENT_PAIRS);
		}
		return eventPairs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PlccodegenerationconfigurationPackage.PLC_CODE_GENERATION_CONFIGURATION__EVENT_PAIRS:
				return ((InternalEList<?>)getEventPairs()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PlccodegenerationconfigurationPackage.PLC_CODE_GENERATION_CONFIGURATION__CONTROLLER:
				if (resolve) return getController();
				return basicGetController();
			case PlccodegenerationconfigurationPackage.PLC_CODE_GENERATION_CONFIGURATION__EVENTS_TO_REMOVE:
				return getEventsToRemove();
			case PlccodegenerationconfigurationPackage.PLC_CODE_GENERATION_CONFIGURATION__EVENT_PAIRS:
				return getEventPairs();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PlccodegenerationconfigurationPackage.PLC_CODE_GENERATION_CONFIGURATION__CONTROLLER:
				setController((SMLRuntimeStateGraph)newValue);
				return;
			case PlccodegenerationconfigurationPackage.PLC_CODE_GENERATION_CONFIGURATION__EVENTS_TO_REMOVE:
				getEventsToRemove().clear();
				getEventsToRemove().addAll((Collection<? extends org.scenariotools.sml.runtime.MessageEvent>)newValue);
				return;
			case PlccodegenerationconfigurationPackage.PLC_CODE_GENERATION_CONFIGURATION__EVENT_PAIRS:
				getEventPairs().clear();
				getEventPairs().addAll((Collection<? extends EventPair>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PlccodegenerationconfigurationPackage.PLC_CODE_GENERATION_CONFIGURATION__CONTROLLER:
				setController((SMLRuntimeStateGraph)null);
				return;
			case PlccodegenerationconfigurationPackage.PLC_CODE_GENERATION_CONFIGURATION__EVENTS_TO_REMOVE:
				getEventsToRemove().clear();
				return;
			case PlccodegenerationconfigurationPackage.PLC_CODE_GENERATION_CONFIGURATION__EVENT_PAIRS:
				getEventPairs().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PlccodegenerationconfigurationPackage.PLC_CODE_GENERATION_CONFIGURATION__CONTROLLER:
				return controller != null;
			case PlccodegenerationconfigurationPackage.PLC_CODE_GENERATION_CONFIGURATION__EVENTS_TO_REMOVE:
				return eventsToRemove != null && !eventsToRemove.isEmpty();
			case PlccodegenerationconfigurationPackage.PLC_CODE_GENERATION_CONFIGURATION__EVENT_PAIRS:
				return eventPairs != null && !eventPairs.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PLCCodeGenerationConfigurationImpl
