/**
 */
package plccodegenerationconfiguration.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import plccodegenerationconfiguration.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PlccodegenerationconfigurationFactoryImpl extends EFactoryImpl implements PlccodegenerationconfigurationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static PlccodegenerationconfigurationFactory init() {
		try {
			PlccodegenerationconfigurationFactory thePlccodegenerationconfigurationFactory = (PlccodegenerationconfigurationFactory)EPackage.Registry.INSTANCE.getEFactory(PlccodegenerationconfigurationPackage.eNS_URI);
			if (thePlccodegenerationconfigurationFactory != null) {
				return thePlccodegenerationconfigurationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new PlccodegenerationconfigurationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PlccodegenerationconfigurationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case PlccodegenerationconfigurationPackage.PLC_CODE_GENERATION_CONFIGURATION: return createPLCCodeGenerationConfiguration();
			case PlccodegenerationconfigurationPackage.EVENT_PAIR: return createEventPair();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PLCCodeGenerationConfiguration createPLCCodeGenerationConfiguration() {
		PLCCodeGenerationConfigurationImpl plcCodeGenerationConfiguration = new PLCCodeGenerationConfigurationImpl();
		return plcCodeGenerationConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventPair createEventPair() {
		EventPairImpl eventPair = new EventPairImpl();
		return eventPair;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PlccodegenerationconfigurationPackage getPlccodegenerationconfigurationPackage() {
		return (PlccodegenerationconfigurationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static PlccodegenerationconfigurationPackage getPackage() {
		return PlccodegenerationconfigurationPackage.eINSTANCE;
	}

} //PlccodegenerationconfigurationFactoryImpl
