/**
 */
package plccodegenerationconfiguration;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see plccodegenerationconfiguration.PlccodegenerationconfigurationPackage
 * @generated
 */
public interface PlccodegenerationconfigurationFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PlccodegenerationconfigurationFactory eINSTANCE = plccodegenerationconfiguration.impl.PlccodegenerationconfigurationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>PLC Code Generation Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PLC Code Generation Configuration</em>'.
	 * @generated
	 */
	PLCCodeGenerationConfiguration createPLCCodeGenerationConfiguration();

	/**
	 * Returns a new object of class '<em>Event Pair</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Pair</em>'.
	 * @generated
	 */
	EventPair createEventPair();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	PlccodegenerationconfigurationPackage getPlccodegenerationconfigurationPackage();

} //PlccodegenerationconfigurationFactory
