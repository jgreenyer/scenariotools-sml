/**
 */
package plccodegenerationconfiguration;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PLC Code Generation Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link plccodegenerationconfiguration.PLCCodeGenerationConfiguration#getController <em>Controller</em>}</li>
 *   <li>{@link plccodegenerationconfiguration.PLCCodeGenerationConfiguration#getEventsToRemove <em>Events To Remove</em>}</li>
 *   <li>{@link plccodegenerationconfiguration.PLCCodeGenerationConfiguration#getEventPairs <em>Event Pairs</em>}</li>
 * </ul>
 *
 * @see plccodegenerationconfiguration.PlccodegenerationconfigurationPackage#getPLCCodeGenerationConfiguration()
 * @model
 * @generated
 */
public interface PLCCodeGenerationConfiguration extends EObject {
	/**
	 * Returns the value of the '<em><b>Controller</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controller</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controller</em>' reference.
	 * @see #setController(SMLRuntimeStateGraph)
	 * @see plccodegenerationconfiguration.PlccodegenerationconfigurationPackage#getPLCCodeGenerationConfiguration_Controller()
	 * @model required="true"
	 * @generated
	 */
	SMLRuntimeStateGraph getController();

	/**
	 * Sets the value of the '{@link plccodegenerationconfiguration.PLCCodeGenerationConfiguration#getController <em>Controller</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Controller</em>' reference.
	 * @see #getController()
	 * @generated
	 */
	void setController(SMLRuntimeStateGraph value);

	/**
	 * Returns the value of the '<em><b>Events To Remove</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.MessageEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Events To Remove</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events To Remove</em>' reference list.
	 * @see plccodegenerationconfiguration.PlccodegenerationconfigurationPackage#getPLCCodeGenerationConfiguration_EventsToRemove()
	 * @model
	 * @generated
	 */
	EList<org.scenariotools.sml.runtime.MessageEvent> getEventsToRemove();

	/**
	 * Returns the value of the '<em><b>Event Pairs</b></em>' containment reference list.
	 * The list contents are of type {@link plccodegenerationconfiguration.EventPair}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Pairs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Pairs</em>' containment reference list.
	 * @see plccodegenerationconfiguration.PlccodegenerationconfigurationPackage#getPLCCodeGenerationConfiguration_EventPairs()
	 * @model containment="true"
	 * @generated
	 */
	EList<EventPair> getEventPairs();

} // PLCCodeGenerationConfiguration
