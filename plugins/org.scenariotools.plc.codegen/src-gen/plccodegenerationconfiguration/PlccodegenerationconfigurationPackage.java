/**
 */
package plccodegenerationconfiguration;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see plccodegenerationconfiguration.PlccodegenerationconfigurationFactory
 * @model kind="package"
 * @generated
 */
public interface PlccodegenerationconfigurationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "plccodegenerationconfiguration";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.scenariotools.org/plccodegenerationconfiguration";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "plccodegenerationconfiguration";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PlccodegenerationconfigurationPackage eINSTANCE = plccodegenerationconfiguration.impl.PlccodegenerationconfigurationPackageImpl.init();

	/**
	 * The meta object id for the '{@link plccodegenerationconfiguration.impl.PLCCodeGenerationConfigurationImpl <em>PLC Code Generation Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see plccodegenerationconfiguration.impl.PLCCodeGenerationConfigurationImpl
	 * @see plccodegenerationconfiguration.impl.PlccodegenerationconfigurationPackageImpl#getPLCCodeGenerationConfiguration()
	 * @generated
	 */
	int PLC_CODE_GENERATION_CONFIGURATION = 0;

	/**
	 * The feature id for the '<em><b>Controller</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLC_CODE_GENERATION_CONFIGURATION__CONTROLLER = 0;

	/**
	 * The feature id for the '<em><b>Events To Remove</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLC_CODE_GENERATION_CONFIGURATION__EVENTS_TO_REMOVE = 1;

	/**
	 * The feature id for the '<em><b>Event Pairs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLC_CODE_GENERATION_CONFIGURATION__EVENT_PAIRS = 2;

	/**
	 * The number of structural features of the '<em>PLC Code Generation Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLC_CODE_GENERATION_CONFIGURATION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>PLC Code Generation Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLC_CODE_GENERATION_CONFIGURATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link plccodegenerationconfiguration.impl.EventPairImpl <em>Event Pair</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see plccodegenerationconfiguration.impl.EventPairImpl
	 * @see plccodegenerationconfiguration.impl.PlccodegenerationconfigurationPackageImpl#getEventPair()
	 * @generated
	 */
	int EVENT_PAIR = 1;

	/**
	 * The feature id for the '<em><b>First</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_PAIR__FIRST = 0;

	/**
	 * The feature id for the '<em><b>Second</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_PAIR__SECOND = 1;

	/**
	 * The number of structural features of the '<em>Event Pair</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_PAIR_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Event Pair</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_PAIR_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link plccodegenerationconfiguration.PLCCodeGenerationConfiguration <em>PLC Code Generation Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PLC Code Generation Configuration</em>'.
	 * @see plccodegenerationconfiguration.PLCCodeGenerationConfiguration
	 * @generated
	 */
	EClass getPLCCodeGenerationConfiguration();

	/**
	 * Returns the meta object for the reference '{@link plccodegenerationconfiguration.PLCCodeGenerationConfiguration#getController <em>Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Controller</em>'.
	 * @see plccodegenerationconfiguration.PLCCodeGenerationConfiguration#getController()
	 * @see #getPLCCodeGenerationConfiguration()
	 * @generated
	 */
	EReference getPLCCodeGenerationConfiguration_Controller();

	/**
	 * Returns the meta object for the reference list '{@link plccodegenerationconfiguration.PLCCodeGenerationConfiguration#getEventsToRemove <em>Events To Remove</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Events To Remove</em>'.
	 * @see plccodegenerationconfiguration.PLCCodeGenerationConfiguration#getEventsToRemove()
	 * @see #getPLCCodeGenerationConfiguration()
	 * @generated
	 */
	EReference getPLCCodeGenerationConfiguration_EventsToRemove();

	/**
	 * Returns the meta object for the containment reference list '{@link plccodegenerationconfiguration.PLCCodeGenerationConfiguration#getEventPairs <em>Event Pairs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Event Pairs</em>'.
	 * @see plccodegenerationconfiguration.PLCCodeGenerationConfiguration#getEventPairs()
	 * @see #getPLCCodeGenerationConfiguration()
	 * @generated
	 */
	EReference getPLCCodeGenerationConfiguration_EventPairs();

	/**
	 * Returns the meta object for class '{@link plccodegenerationconfiguration.EventPair <em>Event Pair</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Pair</em>'.
	 * @see plccodegenerationconfiguration.EventPair
	 * @generated
	 */
	EClass getEventPair();

	/**
	 * Returns the meta object for the reference '{@link plccodegenerationconfiguration.EventPair#getFirst <em>First</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>First</em>'.
	 * @see plccodegenerationconfiguration.EventPair#getFirst()
	 * @see #getEventPair()
	 * @generated
	 */
	EReference getEventPair_First();

	/**
	 * Returns the meta object for the reference '{@link plccodegenerationconfiguration.EventPair#getSecond <em>Second</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Second</em>'.
	 * @see plccodegenerationconfiguration.EventPair#getSecond()
	 * @see #getEventPair()
	 * @generated
	 */
	EReference getEventPair_Second();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PlccodegenerationconfigurationFactory getPlccodegenerationconfigurationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link plccodegenerationconfiguration.impl.PLCCodeGenerationConfigurationImpl <em>PLC Code Generation Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see plccodegenerationconfiguration.impl.PLCCodeGenerationConfigurationImpl
		 * @see plccodegenerationconfiguration.impl.PlccodegenerationconfigurationPackageImpl#getPLCCodeGenerationConfiguration()
		 * @generated
		 */
		EClass PLC_CODE_GENERATION_CONFIGURATION = eINSTANCE.getPLCCodeGenerationConfiguration();

		/**
		 * The meta object literal for the '<em><b>Controller</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PLC_CODE_GENERATION_CONFIGURATION__CONTROLLER = eINSTANCE.getPLCCodeGenerationConfiguration_Controller();

		/**
		 * The meta object literal for the '<em><b>Events To Remove</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PLC_CODE_GENERATION_CONFIGURATION__EVENTS_TO_REMOVE = eINSTANCE.getPLCCodeGenerationConfiguration_EventsToRemove();

		/**
		 * The meta object literal for the '<em><b>Event Pairs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PLC_CODE_GENERATION_CONFIGURATION__EVENT_PAIRS = eINSTANCE.getPLCCodeGenerationConfiguration_EventPairs();

		/**
		 * The meta object literal for the '{@link plccodegenerationconfiguration.impl.EventPairImpl <em>Event Pair</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see plccodegenerationconfiguration.impl.EventPairImpl
		 * @see plccodegenerationconfiguration.impl.PlccodegenerationconfigurationPackageImpl#getEventPair()
		 * @generated
		 */
		EClass EVENT_PAIR = eINSTANCE.getEventPair();

		/**
		 * The meta object literal for the '<em><b>First</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_PAIR__FIRST = eINSTANCE.getEventPair_First();

		/**
		 * The meta object literal for the '<em><b>Second</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_PAIR__SECOND = eINSTANCE.getEventPair_Second();

	}

} //PlccodegenerationconfigurationPackage
