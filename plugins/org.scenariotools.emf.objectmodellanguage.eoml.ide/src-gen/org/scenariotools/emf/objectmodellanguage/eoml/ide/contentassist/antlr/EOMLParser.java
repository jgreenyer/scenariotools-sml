/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * ScenarioTools-URL: www.scenariotools.org
 *
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete
 */
package org.scenariotools.emf.objectmodellanguage.eoml.ide.contentassist.antlr;

import com.google.inject.Inject;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.antlr.runtime.RecognitionException;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.ide.editor.contentassist.antlr.AbstractContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.FollowElement;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.scenariotools.emf.objectmodellanguage.eoml.ide.contentassist.antlr.internal.InternalEOMLParser;
import org.scenariotools.emf.objectmodellanguage.eoml.services.EOMLGrammarAccess;

public class EOMLParser extends AbstractContentAssistParser {

	@Inject
	private EOMLGrammarAccess grammarAccess;

	private Map<AbstractElement, String> nameMappings;

	@Override
	protected InternalEOMLParser createParser() {
		InternalEOMLParser result = new InternalEOMLParser(null);
		result.setGrammarAccess(grammarAccess);
		return result;
	}

	@Override
	protected String getRuleName(AbstractElement element) {
		if (nameMappings == null) {
			nameMappings = new HashMap<AbstractElement, String>() {
				private static final long serialVersionUID = 1L;
				{
					put(grammarAccess.getSObjectElementAccess().getAlternatives(), "rule__SObjectElement__Alternatives");
					put(grammarAccess.getEStringAccess().getAlternatives(), "rule__EString__Alternatives");
					put(grammarAccess.getSObjectAccess().getGroup(), "rule__SObject__Group__0");
					put(grammarAccess.getSObjectAccess().getGroup_6(), "rule__SObject__Group_6__0");
					put(grammarAccess.getSObjectAccess().getGroup_7(), "rule__SObject__Group_7__0");
					put(grammarAccess.getSOEltAttributeAccess().getGroup(), "rule__SOEltAttribute__Group__0");
					put(grammarAccess.getSOEltAttributeAccess().getGroup_3(), "rule__SOEltAttribute__Group_3__0");
					put(grammarAccess.getSOEltAttributeAccess().getGroup_3_2(), "rule__SOEltAttribute__Group_3_2__0");
					put(grammarAccess.getSOEltRefAccess().getGroup(), "rule__SOEltRef__Group__0");
					put(grammarAccess.getSOEltRefAccess().getGroup_3(), "rule__SOEltRef__Group_3__0");
					put(grammarAccess.getSOEltRefAccess().getGroup_3_2(), "rule__SOEltRef__Group_3_2__0");
					put(grammarAccess.getSOEltContaimentAccess().getGroup(), "rule__SOEltContaiment__Group__0");
					put(grammarAccess.getSOEltContaimentAccess().getGroup_3(), "rule__SOEltContaiment__Group_3__0");
					put(grammarAccess.getSOEltContaimentAccess().getGroup_3_2(), "rule__SOEltContaiment__Group_3_2__0");
					put(grammarAccess.getSAValuePrimitiveAccess().getGroup(), "rule__SAValuePrimitive__Group__0");
					put(grammarAccess.getSAValueObjectAccess().getGroup(), "rule__SAValueObject__Group__0");
					put(grammarAccess.getSValueObjectRefAccess().getGroup(), "rule__SValueObjectRef__Group__0");
					put(grammarAccess.getEStringAccess().getGroup_2(), "rule__EString__Group_2__0");
					put(grammarAccess.getSObjectAccess().getNameAssignment_2(), "rule__SObject__NameAssignment_2");
					put(grammarAccess.getSObjectAccess().getEClassAssignment_4(), "rule__SObject__EClassAssignment_4");
					put(grammarAccess.getSObjectAccess().getSobjectelementAssignment_6_1(), "rule__SObject__SobjectelementAssignment_6_1");
					put(grammarAccess.getSObjectAccess().getSobjectelementAssignment_7_1(), "rule__SObject__SobjectelementAssignment_7_1");
					put(grammarAccess.getSOEltAttributeAccess().getNameAssignment_1(), "rule__SOEltAttribute__NameAssignment_1");
					put(grammarAccess.getSOEltAttributeAccess().getSavalueAssignment_3_1(), "rule__SOEltAttribute__SavalueAssignment_3_1");
					put(grammarAccess.getSOEltAttributeAccess().getSavalueAssignment_3_2_1(), "rule__SOEltAttribute__SavalueAssignment_3_2_1");
					put(grammarAccess.getSOEltRefAccess().getNameAssignment_1(), "rule__SOEltRef__NameAssignment_1");
					put(grammarAccess.getSOEltRefAccess().getSvalueobjectAssignment_3_1(), "rule__SOEltRef__SvalueobjectAssignment_3_1");
					put(grammarAccess.getSOEltRefAccess().getSvalueobjectAssignment_3_2_1(), "rule__SOEltRef__SvalueobjectAssignment_3_2_1");
					put(grammarAccess.getSOEltContaimentAccess().getNameAssignment_1(), "rule__SOEltContaiment__NameAssignment_1");
					put(grammarAccess.getSOEltContaimentAccess().getSobjectAssignment_3_1(), "rule__SOEltContaiment__SobjectAssignment_3_1");
					put(grammarAccess.getSOEltContaimentAccess().getSobjectAssignment_3_2_1(), "rule__SOEltContaiment__SobjectAssignment_3_2_1");
					put(grammarAccess.getSAValuePrimitiveAccess().getValueAssignment_1(), "rule__SAValuePrimitive__ValueAssignment_1");
					put(grammarAccess.getSAValueObjectAccess().getNameAssignment_1(), "rule__SAValueObject__NameAssignment_1");
					put(grammarAccess.getSAValueObjectAccess().getEClassAssignment_3(), "rule__SAValueObject__EClassAssignment_3");
					put(grammarAccess.getSValueObjectRefAccess().getNameAssignment_1(), "rule__SValueObjectRef__NameAssignment_1");
					put(grammarAccess.getSValueObjectRefAccess().getEClassAssignment_3(), "rule__SValueObjectRef__EClassAssignment_3");
				}
			};
		}
		return nameMappings.get(element);
	}

	@Override
	protected Collection<FollowElement> getFollowElements(AbstractInternalContentAssistParser parser) {
		try {
			InternalEOMLParser typedParser = (InternalEOMLParser) parser;
			typedParser.entryRuleSObject();
			return typedParser.getFollowElements();
		} catch(RecognitionException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	protected String[] getInitialHiddenTokens() {
		return new String[] { "RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT" };
	}

	public EOMLGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(EOMLGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
