package org.scenariotools.emf.objectmodellanguage.eoml.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.scenariotools.emf.objectmodellanguage.eoml.services.EOMLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * ScenarioTools-URL: www.scenariotools.org
 *
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete
 */
@SuppressWarnings("all")
public class InternalEOMLParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'object'", "':'", "'{'", "'}'", "'attribute'", "'reference'", "'['", "']'", "','", "'-'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;

    // delegates
    // delegators


        public InternalEOMLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalEOMLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalEOMLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalEOML.g"; }


    	private EOMLGrammarAccess grammarAccess;

    	public void setGrammarAccess(EOMLGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleSObject"
    // InternalEOML.g:63:1: entryRuleSObject : ruleSObject EOF ;
    public final void entryRuleSObject() throws RecognitionException {
        try {
            // InternalEOML.g:64:1: ( ruleSObject EOF )
            // InternalEOML.g:65:1: ruleSObject EOF
            {
             before(grammarAccess.getSObjectRule()); 
            pushFollow(FOLLOW_1);
            ruleSObject();

            state._fsp--;

             after(grammarAccess.getSObjectRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSObject"


    // $ANTLR start "ruleSObject"
    // InternalEOML.g:72:1: ruleSObject : ( ( rule__SObject__Group__0 ) ) ;
    public final void ruleSObject() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:76:2: ( ( ( rule__SObject__Group__0 ) ) )
            // InternalEOML.g:77:2: ( ( rule__SObject__Group__0 ) )
            {
            // InternalEOML.g:77:2: ( ( rule__SObject__Group__0 ) )
            // InternalEOML.g:78:3: ( rule__SObject__Group__0 )
            {
             before(grammarAccess.getSObjectAccess().getGroup()); 
            // InternalEOML.g:79:3: ( rule__SObject__Group__0 )
            // InternalEOML.g:79:4: rule__SObject__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SObject__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSObjectAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSObject"


    // $ANTLR start "entryRuleSObjectElement"
    // InternalEOML.g:88:1: entryRuleSObjectElement : ruleSObjectElement EOF ;
    public final void entryRuleSObjectElement() throws RecognitionException {
        try {
            // InternalEOML.g:89:1: ( ruleSObjectElement EOF )
            // InternalEOML.g:90:1: ruleSObjectElement EOF
            {
             before(grammarAccess.getSObjectElementRule()); 
            pushFollow(FOLLOW_1);
            ruleSObjectElement();

            state._fsp--;

             after(grammarAccess.getSObjectElementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSObjectElement"


    // $ANTLR start "ruleSObjectElement"
    // InternalEOML.g:97:1: ruleSObjectElement : ( ( rule__SObjectElement__Alternatives ) ) ;
    public final void ruleSObjectElement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:101:2: ( ( ( rule__SObjectElement__Alternatives ) ) )
            // InternalEOML.g:102:2: ( ( rule__SObjectElement__Alternatives ) )
            {
            // InternalEOML.g:102:2: ( ( rule__SObjectElement__Alternatives ) )
            // InternalEOML.g:103:3: ( rule__SObjectElement__Alternatives )
            {
             before(grammarAccess.getSObjectElementAccess().getAlternatives()); 
            // InternalEOML.g:104:3: ( rule__SObjectElement__Alternatives )
            // InternalEOML.g:104:4: rule__SObjectElement__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__SObjectElement__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSObjectElementAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSObjectElement"


    // $ANTLR start "entryRuleSOEltAttribute"
    // InternalEOML.g:113:1: entryRuleSOEltAttribute : ruleSOEltAttribute EOF ;
    public final void entryRuleSOEltAttribute() throws RecognitionException {
        try {
            // InternalEOML.g:114:1: ( ruleSOEltAttribute EOF )
            // InternalEOML.g:115:1: ruleSOEltAttribute EOF
            {
             before(grammarAccess.getSOEltAttributeRule()); 
            pushFollow(FOLLOW_1);
            ruleSOEltAttribute();

            state._fsp--;

             after(grammarAccess.getSOEltAttributeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSOEltAttribute"


    // $ANTLR start "ruleSOEltAttribute"
    // InternalEOML.g:122:1: ruleSOEltAttribute : ( ( rule__SOEltAttribute__Group__0 ) ) ;
    public final void ruleSOEltAttribute() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:126:2: ( ( ( rule__SOEltAttribute__Group__0 ) ) )
            // InternalEOML.g:127:2: ( ( rule__SOEltAttribute__Group__0 ) )
            {
            // InternalEOML.g:127:2: ( ( rule__SOEltAttribute__Group__0 ) )
            // InternalEOML.g:128:3: ( rule__SOEltAttribute__Group__0 )
            {
             before(grammarAccess.getSOEltAttributeAccess().getGroup()); 
            // InternalEOML.g:129:3: ( rule__SOEltAttribute__Group__0 )
            // InternalEOML.g:129:4: rule__SOEltAttribute__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SOEltAttribute__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSOEltAttributeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSOEltAttribute"


    // $ANTLR start "entryRuleSOEltRef"
    // InternalEOML.g:138:1: entryRuleSOEltRef : ruleSOEltRef EOF ;
    public final void entryRuleSOEltRef() throws RecognitionException {
        try {
            // InternalEOML.g:139:1: ( ruleSOEltRef EOF )
            // InternalEOML.g:140:1: ruleSOEltRef EOF
            {
             before(grammarAccess.getSOEltRefRule()); 
            pushFollow(FOLLOW_1);
            ruleSOEltRef();

            state._fsp--;

             after(grammarAccess.getSOEltRefRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSOEltRef"


    // $ANTLR start "ruleSOEltRef"
    // InternalEOML.g:147:1: ruleSOEltRef : ( ( rule__SOEltRef__Group__0 ) ) ;
    public final void ruleSOEltRef() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:151:2: ( ( ( rule__SOEltRef__Group__0 ) ) )
            // InternalEOML.g:152:2: ( ( rule__SOEltRef__Group__0 ) )
            {
            // InternalEOML.g:152:2: ( ( rule__SOEltRef__Group__0 ) )
            // InternalEOML.g:153:3: ( rule__SOEltRef__Group__0 )
            {
             before(grammarAccess.getSOEltRefAccess().getGroup()); 
            // InternalEOML.g:154:3: ( rule__SOEltRef__Group__0 )
            // InternalEOML.g:154:4: rule__SOEltRef__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SOEltRef__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSOEltRefAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSOEltRef"


    // $ANTLR start "entryRuleSOEltContaiment"
    // InternalEOML.g:163:1: entryRuleSOEltContaiment : ruleSOEltContaiment EOF ;
    public final void entryRuleSOEltContaiment() throws RecognitionException {
        try {
            // InternalEOML.g:164:1: ( ruleSOEltContaiment EOF )
            // InternalEOML.g:165:1: ruleSOEltContaiment EOF
            {
             before(grammarAccess.getSOEltContaimentRule()); 
            pushFollow(FOLLOW_1);
            ruleSOEltContaiment();

            state._fsp--;

             after(grammarAccess.getSOEltContaimentRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSOEltContaiment"


    // $ANTLR start "ruleSOEltContaiment"
    // InternalEOML.g:172:1: ruleSOEltContaiment : ( ( rule__SOEltContaiment__Group__0 ) ) ;
    public final void ruleSOEltContaiment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:176:2: ( ( ( rule__SOEltContaiment__Group__0 ) ) )
            // InternalEOML.g:177:2: ( ( rule__SOEltContaiment__Group__0 ) )
            {
            // InternalEOML.g:177:2: ( ( rule__SOEltContaiment__Group__0 ) )
            // InternalEOML.g:178:3: ( rule__SOEltContaiment__Group__0 )
            {
             before(grammarAccess.getSOEltContaimentAccess().getGroup()); 
            // InternalEOML.g:179:3: ( rule__SOEltContaiment__Group__0 )
            // InternalEOML.g:179:4: rule__SOEltContaiment__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SOEltContaiment__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSOEltContaimentAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSOEltContaiment"


    // $ANTLR start "entryRuleSAValue"
    // InternalEOML.g:188:1: entryRuleSAValue : ruleSAValue EOF ;
    public final void entryRuleSAValue() throws RecognitionException {
        try {
            // InternalEOML.g:189:1: ( ruleSAValue EOF )
            // InternalEOML.g:190:1: ruleSAValue EOF
            {
             before(grammarAccess.getSAValueRule()); 
            pushFollow(FOLLOW_1);
            ruleSAValue();

            state._fsp--;

             after(grammarAccess.getSAValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSAValue"


    // $ANTLR start "ruleSAValue"
    // InternalEOML.g:197:1: ruleSAValue : ( ruleSAValuePrimitive ) ;
    public final void ruleSAValue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:201:2: ( ( ruleSAValuePrimitive ) )
            // InternalEOML.g:202:2: ( ruleSAValuePrimitive )
            {
            // InternalEOML.g:202:2: ( ruleSAValuePrimitive )
            // InternalEOML.g:203:3: ruleSAValuePrimitive
            {
             before(grammarAccess.getSAValueAccess().getSAValuePrimitiveParserRuleCall()); 
            pushFollow(FOLLOW_2);
            ruleSAValuePrimitive();

            state._fsp--;

             after(grammarAccess.getSAValueAccess().getSAValuePrimitiveParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSAValue"


    // $ANTLR start "entryRuleSAValuePrimitive"
    // InternalEOML.g:213:1: entryRuleSAValuePrimitive : ruleSAValuePrimitive EOF ;
    public final void entryRuleSAValuePrimitive() throws RecognitionException {
        try {
            // InternalEOML.g:214:1: ( ruleSAValuePrimitive EOF )
            // InternalEOML.g:215:1: ruleSAValuePrimitive EOF
            {
             before(grammarAccess.getSAValuePrimitiveRule()); 
            pushFollow(FOLLOW_1);
            ruleSAValuePrimitive();

            state._fsp--;

             after(grammarAccess.getSAValuePrimitiveRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSAValuePrimitive"


    // $ANTLR start "ruleSAValuePrimitive"
    // InternalEOML.g:222:1: ruleSAValuePrimitive : ( ( rule__SAValuePrimitive__Group__0 ) ) ;
    public final void ruleSAValuePrimitive() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:226:2: ( ( ( rule__SAValuePrimitive__Group__0 ) ) )
            // InternalEOML.g:227:2: ( ( rule__SAValuePrimitive__Group__0 ) )
            {
            // InternalEOML.g:227:2: ( ( rule__SAValuePrimitive__Group__0 ) )
            // InternalEOML.g:228:3: ( rule__SAValuePrimitive__Group__0 )
            {
             before(grammarAccess.getSAValuePrimitiveAccess().getGroup()); 
            // InternalEOML.g:229:3: ( rule__SAValuePrimitive__Group__0 )
            // InternalEOML.g:229:4: rule__SAValuePrimitive__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SAValuePrimitive__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSAValuePrimitiveAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSAValuePrimitive"


    // $ANTLR start "entryRuleSValueObjectRef"
    // InternalEOML.g:238:1: entryRuleSValueObjectRef : ruleSValueObjectRef EOF ;
    public final void entryRuleSValueObjectRef() throws RecognitionException {
        try {
            // InternalEOML.g:239:1: ( ruleSValueObjectRef EOF )
            // InternalEOML.g:240:1: ruleSValueObjectRef EOF
            {
             before(grammarAccess.getSValueObjectRefRule()); 
            pushFollow(FOLLOW_1);
            ruleSValueObjectRef();

            state._fsp--;

             after(grammarAccess.getSValueObjectRefRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSValueObjectRef"


    // $ANTLR start "ruleSValueObjectRef"
    // InternalEOML.g:247:1: ruleSValueObjectRef : ( ( rule__SValueObjectRef__Group__0 ) ) ;
    public final void ruleSValueObjectRef() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:251:2: ( ( ( rule__SValueObjectRef__Group__0 ) ) )
            // InternalEOML.g:252:2: ( ( rule__SValueObjectRef__Group__0 ) )
            {
            // InternalEOML.g:252:2: ( ( rule__SValueObjectRef__Group__0 ) )
            // InternalEOML.g:253:3: ( rule__SValueObjectRef__Group__0 )
            {
             before(grammarAccess.getSValueObjectRefAccess().getGroup()); 
            // InternalEOML.g:254:3: ( rule__SValueObjectRef__Group__0 )
            // InternalEOML.g:254:4: rule__SValueObjectRef__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SValueObjectRef__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSValueObjectRefAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSValueObjectRef"


    // $ANTLR start "entryRuleEString"
    // InternalEOML.g:263:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // InternalEOML.g:264:1: ( ruleEString EOF )
            // InternalEOML.g:265:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalEOML.g:272:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:276:2: ( ( ( rule__EString__Alternatives ) ) )
            // InternalEOML.g:277:2: ( ( rule__EString__Alternatives ) )
            {
            // InternalEOML.g:277:2: ( ( rule__EString__Alternatives ) )
            // InternalEOML.g:278:3: ( rule__EString__Alternatives )
            {
             before(grammarAccess.getEStringAccess().getAlternatives()); 
            // InternalEOML.g:279:3: ( rule__EString__Alternatives )
            // InternalEOML.g:279:4: rule__EString__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EString__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "rule__SObjectElement__Alternatives"
    // InternalEOML.g:287:1: rule__SObjectElement__Alternatives : ( ( ruleSOEltAttribute ) | ( ruleSOEltRef ) | ( ruleSOEltContaiment ) );
    public final void rule__SObjectElement__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:291:1: ( ( ruleSOEltAttribute ) | ( ruleSOEltRef ) | ( ruleSOEltContaiment ) )
            int alt1=3;
            alt1 = dfa1.predict(input);
            switch (alt1) {
                case 1 :
                    // InternalEOML.g:292:2: ( ruleSOEltAttribute )
                    {
                    // InternalEOML.g:292:2: ( ruleSOEltAttribute )
                    // InternalEOML.g:293:3: ruleSOEltAttribute
                    {
                     before(grammarAccess.getSObjectElementAccess().getSOEltAttributeParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleSOEltAttribute();

                    state._fsp--;

                     after(grammarAccess.getSObjectElementAccess().getSOEltAttributeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalEOML.g:298:2: ( ruleSOEltRef )
                    {
                    // InternalEOML.g:298:2: ( ruleSOEltRef )
                    // InternalEOML.g:299:3: ruleSOEltRef
                    {
                     before(grammarAccess.getSObjectElementAccess().getSOEltRefParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleSOEltRef();

                    state._fsp--;

                     after(grammarAccess.getSObjectElementAccess().getSOEltRefParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalEOML.g:304:2: ( ruleSOEltContaiment )
                    {
                    // InternalEOML.g:304:2: ( ruleSOEltContaiment )
                    // InternalEOML.g:305:3: ruleSOEltContaiment
                    {
                     before(grammarAccess.getSObjectElementAccess().getSOEltContaimentParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleSOEltContaiment();

                    state._fsp--;

                     after(grammarAccess.getSObjectElementAccess().getSOEltContaimentParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObjectElement__Alternatives"


    // $ANTLR start "rule__EString__Alternatives"
    // InternalEOML.g:314:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) | ( ( rule__EString__Group_2__0 ) ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:318:1: ( ( RULE_STRING ) | ( RULE_ID ) | ( ( rule__EString__Group_2__0 ) ) )
            int alt2=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt2=1;
                }
                break;
            case RULE_ID:
                {
                alt2=2;
                }
                break;
            case RULE_INT:
            case 20:
                {
                alt2=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalEOML.g:319:2: ( RULE_STRING )
                    {
                    // InternalEOML.g:319:2: ( RULE_STRING )
                    // InternalEOML.g:320:3: RULE_STRING
                    {
                     before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalEOML.g:325:2: ( RULE_ID )
                    {
                    // InternalEOML.g:325:2: ( RULE_ID )
                    // InternalEOML.g:326:3: RULE_ID
                    {
                     before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalEOML.g:331:2: ( ( rule__EString__Group_2__0 ) )
                    {
                    // InternalEOML.g:331:2: ( ( rule__EString__Group_2__0 ) )
                    // InternalEOML.g:332:3: ( rule__EString__Group_2__0 )
                    {
                     before(grammarAccess.getEStringAccess().getGroup_2()); 
                    // InternalEOML.g:333:3: ( rule__EString__Group_2__0 )
                    // InternalEOML.g:333:4: rule__EString__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__EString__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getEStringAccess().getGroup_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__SObject__Group__0"
    // InternalEOML.g:341:1: rule__SObject__Group__0 : rule__SObject__Group__0__Impl rule__SObject__Group__1 ;
    public final void rule__SObject__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:345:1: ( rule__SObject__Group__0__Impl rule__SObject__Group__1 )
            // InternalEOML.g:346:2: rule__SObject__Group__0__Impl rule__SObject__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__SObject__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SObject__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group__0"


    // $ANTLR start "rule__SObject__Group__0__Impl"
    // InternalEOML.g:353:1: rule__SObject__Group__0__Impl : ( () ) ;
    public final void rule__SObject__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:357:1: ( ( () ) )
            // InternalEOML.g:358:1: ( () )
            {
            // InternalEOML.g:358:1: ( () )
            // InternalEOML.g:359:2: ()
            {
             before(grammarAccess.getSObjectAccess().getSObjectAction_0()); 
            // InternalEOML.g:360:2: ()
            // InternalEOML.g:360:3: 
            {
            }

             after(grammarAccess.getSObjectAccess().getSObjectAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group__0__Impl"


    // $ANTLR start "rule__SObject__Group__1"
    // InternalEOML.g:368:1: rule__SObject__Group__1 : rule__SObject__Group__1__Impl rule__SObject__Group__2 ;
    public final void rule__SObject__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:372:1: ( rule__SObject__Group__1__Impl rule__SObject__Group__2 )
            // InternalEOML.g:373:2: rule__SObject__Group__1__Impl rule__SObject__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__SObject__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SObject__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group__1"


    // $ANTLR start "rule__SObject__Group__1__Impl"
    // InternalEOML.g:380:1: rule__SObject__Group__1__Impl : ( 'object' ) ;
    public final void rule__SObject__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:384:1: ( ( 'object' ) )
            // InternalEOML.g:385:1: ( 'object' )
            {
            // InternalEOML.g:385:1: ( 'object' )
            // InternalEOML.g:386:2: 'object'
            {
             before(grammarAccess.getSObjectAccess().getObjectKeyword_1()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getSObjectAccess().getObjectKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group__1__Impl"


    // $ANTLR start "rule__SObject__Group__2"
    // InternalEOML.g:395:1: rule__SObject__Group__2 : rule__SObject__Group__2__Impl rule__SObject__Group__3 ;
    public final void rule__SObject__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:399:1: ( rule__SObject__Group__2__Impl rule__SObject__Group__3 )
            // InternalEOML.g:400:2: rule__SObject__Group__2__Impl rule__SObject__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__SObject__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SObject__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group__2"


    // $ANTLR start "rule__SObject__Group__2__Impl"
    // InternalEOML.g:407:1: rule__SObject__Group__2__Impl : ( ( rule__SObject__NameAssignment_2 ) ) ;
    public final void rule__SObject__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:411:1: ( ( ( rule__SObject__NameAssignment_2 ) ) )
            // InternalEOML.g:412:1: ( ( rule__SObject__NameAssignment_2 ) )
            {
            // InternalEOML.g:412:1: ( ( rule__SObject__NameAssignment_2 ) )
            // InternalEOML.g:413:2: ( rule__SObject__NameAssignment_2 )
            {
             before(grammarAccess.getSObjectAccess().getNameAssignment_2()); 
            // InternalEOML.g:414:2: ( rule__SObject__NameAssignment_2 )
            // InternalEOML.g:414:3: rule__SObject__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__SObject__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getSObjectAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group__2__Impl"


    // $ANTLR start "rule__SObject__Group__3"
    // InternalEOML.g:422:1: rule__SObject__Group__3 : rule__SObject__Group__3__Impl rule__SObject__Group__4 ;
    public final void rule__SObject__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:426:1: ( rule__SObject__Group__3__Impl rule__SObject__Group__4 )
            // InternalEOML.g:427:2: rule__SObject__Group__3__Impl rule__SObject__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__SObject__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SObject__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group__3"


    // $ANTLR start "rule__SObject__Group__3__Impl"
    // InternalEOML.g:434:1: rule__SObject__Group__3__Impl : ( ':' ) ;
    public final void rule__SObject__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:438:1: ( ( ':' ) )
            // InternalEOML.g:439:1: ( ':' )
            {
            // InternalEOML.g:439:1: ( ':' )
            // InternalEOML.g:440:2: ':'
            {
             before(grammarAccess.getSObjectAccess().getColonKeyword_3()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getSObjectAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group__3__Impl"


    // $ANTLR start "rule__SObject__Group__4"
    // InternalEOML.g:449:1: rule__SObject__Group__4 : rule__SObject__Group__4__Impl rule__SObject__Group__5 ;
    public final void rule__SObject__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:453:1: ( rule__SObject__Group__4__Impl rule__SObject__Group__5 )
            // InternalEOML.g:454:2: rule__SObject__Group__4__Impl rule__SObject__Group__5
            {
            pushFollow(FOLLOW_7);
            rule__SObject__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SObject__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group__4"


    // $ANTLR start "rule__SObject__Group__4__Impl"
    // InternalEOML.g:461:1: rule__SObject__Group__4__Impl : ( ( rule__SObject__EClassAssignment_4 ) ) ;
    public final void rule__SObject__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:465:1: ( ( ( rule__SObject__EClassAssignment_4 ) ) )
            // InternalEOML.g:466:1: ( ( rule__SObject__EClassAssignment_4 ) )
            {
            // InternalEOML.g:466:1: ( ( rule__SObject__EClassAssignment_4 ) )
            // InternalEOML.g:467:2: ( rule__SObject__EClassAssignment_4 )
            {
             before(grammarAccess.getSObjectAccess().getEClassAssignment_4()); 
            // InternalEOML.g:468:2: ( rule__SObject__EClassAssignment_4 )
            // InternalEOML.g:468:3: rule__SObject__EClassAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__SObject__EClassAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getSObjectAccess().getEClassAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group__4__Impl"


    // $ANTLR start "rule__SObject__Group__5"
    // InternalEOML.g:476:1: rule__SObject__Group__5 : rule__SObject__Group__5__Impl rule__SObject__Group__6 ;
    public final void rule__SObject__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:480:1: ( rule__SObject__Group__5__Impl rule__SObject__Group__6 )
            // InternalEOML.g:481:2: rule__SObject__Group__5__Impl rule__SObject__Group__6
            {
            pushFollow(FOLLOW_8);
            rule__SObject__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SObject__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group__5"


    // $ANTLR start "rule__SObject__Group__5__Impl"
    // InternalEOML.g:488:1: rule__SObject__Group__5__Impl : ( '{' ) ;
    public final void rule__SObject__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:492:1: ( ( '{' ) )
            // InternalEOML.g:493:1: ( '{' )
            {
            // InternalEOML.g:493:1: ( '{' )
            // InternalEOML.g:494:2: '{'
            {
             before(grammarAccess.getSObjectAccess().getLeftCurlyBracketKeyword_5()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getSObjectAccess().getLeftCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group__5__Impl"


    // $ANTLR start "rule__SObject__Group__6"
    // InternalEOML.g:503:1: rule__SObject__Group__6 : rule__SObject__Group__6__Impl rule__SObject__Group__7 ;
    public final void rule__SObject__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:507:1: ( rule__SObject__Group__6__Impl rule__SObject__Group__7 )
            // InternalEOML.g:508:2: rule__SObject__Group__6__Impl rule__SObject__Group__7
            {
            pushFollow(FOLLOW_8);
            rule__SObject__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SObject__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group__6"


    // $ANTLR start "rule__SObject__Group__6__Impl"
    // InternalEOML.g:515:1: rule__SObject__Group__6__Impl : ( ( rule__SObject__Group_6__0 )* ) ;
    public final void rule__SObject__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:519:1: ( ( ( rule__SObject__Group_6__0 )* ) )
            // InternalEOML.g:520:1: ( ( rule__SObject__Group_6__0 )* )
            {
            // InternalEOML.g:520:1: ( ( rule__SObject__Group_6__0 )* )
            // InternalEOML.g:521:2: ( rule__SObject__Group_6__0 )*
            {
             before(grammarAccess.getSObjectAccess().getGroup_6()); 
            // InternalEOML.g:522:2: ( rule__SObject__Group_6__0 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==15) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalEOML.g:522:3: rule__SObject__Group_6__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__SObject__Group_6__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getSObjectAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group__6__Impl"


    // $ANTLR start "rule__SObject__Group__7"
    // InternalEOML.g:530:1: rule__SObject__Group__7 : rule__SObject__Group__7__Impl rule__SObject__Group__8 ;
    public final void rule__SObject__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:534:1: ( rule__SObject__Group__7__Impl rule__SObject__Group__8 )
            // InternalEOML.g:535:2: rule__SObject__Group__7__Impl rule__SObject__Group__8
            {
            pushFollow(FOLLOW_8);
            rule__SObject__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SObject__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group__7"


    // $ANTLR start "rule__SObject__Group__7__Impl"
    // InternalEOML.g:542:1: rule__SObject__Group__7__Impl : ( ( rule__SObject__Group_7__0 )* ) ;
    public final void rule__SObject__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:546:1: ( ( ( rule__SObject__Group_7__0 )* ) )
            // InternalEOML.g:547:1: ( ( rule__SObject__Group_7__0 )* )
            {
            // InternalEOML.g:547:1: ( ( rule__SObject__Group_7__0 )* )
            // InternalEOML.g:548:2: ( rule__SObject__Group_7__0 )*
            {
             before(grammarAccess.getSObjectAccess().getGroup_7()); 
            // InternalEOML.g:549:2: ( rule__SObject__Group_7__0 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==16) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalEOML.g:549:3: rule__SObject__Group_7__0
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__SObject__Group_7__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getSObjectAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group__7__Impl"


    // $ANTLR start "rule__SObject__Group__8"
    // InternalEOML.g:557:1: rule__SObject__Group__8 : rule__SObject__Group__8__Impl ;
    public final void rule__SObject__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:561:1: ( rule__SObject__Group__8__Impl )
            // InternalEOML.g:562:2: rule__SObject__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SObject__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group__8"


    // $ANTLR start "rule__SObject__Group__8__Impl"
    // InternalEOML.g:568:1: rule__SObject__Group__8__Impl : ( '}' ) ;
    public final void rule__SObject__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:572:1: ( ( '}' ) )
            // InternalEOML.g:573:1: ( '}' )
            {
            // InternalEOML.g:573:1: ( '}' )
            // InternalEOML.g:574:2: '}'
            {
             before(grammarAccess.getSObjectAccess().getRightCurlyBracketKeyword_8()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getSObjectAccess().getRightCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group__8__Impl"


    // $ANTLR start "rule__SObject__Group_6__0"
    // InternalEOML.g:584:1: rule__SObject__Group_6__0 : rule__SObject__Group_6__0__Impl rule__SObject__Group_6__1 ;
    public final void rule__SObject__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:588:1: ( rule__SObject__Group_6__0__Impl rule__SObject__Group_6__1 )
            // InternalEOML.g:589:2: rule__SObject__Group_6__0__Impl rule__SObject__Group_6__1
            {
            pushFollow(FOLLOW_6);
            rule__SObject__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SObject__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group_6__0"


    // $ANTLR start "rule__SObject__Group_6__0__Impl"
    // InternalEOML.g:596:1: rule__SObject__Group_6__0__Impl : ( 'attribute' ) ;
    public final void rule__SObject__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:600:1: ( ( 'attribute' ) )
            // InternalEOML.g:601:1: ( 'attribute' )
            {
            // InternalEOML.g:601:1: ( 'attribute' )
            // InternalEOML.g:602:2: 'attribute'
            {
             before(grammarAccess.getSObjectAccess().getAttributeKeyword_6_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getSObjectAccess().getAttributeKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group_6__0__Impl"


    // $ANTLR start "rule__SObject__Group_6__1"
    // InternalEOML.g:611:1: rule__SObject__Group_6__1 : rule__SObject__Group_6__1__Impl ;
    public final void rule__SObject__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:615:1: ( rule__SObject__Group_6__1__Impl )
            // InternalEOML.g:616:2: rule__SObject__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SObject__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group_6__1"


    // $ANTLR start "rule__SObject__Group_6__1__Impl"
    // InternalEOML.g:622:1: rule__SObject__Group_6__1__Impl : ( ( rule__SObject__SobjectelementAssignment_6_1 ) ) ;
    public final void rule__SObject__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:626:1: ( ( ( rule__SObject__SobjectelementAssignment_6_1 ) ) )
            // InternalEOML.g:627:1: ( ( rule__SObject__SobjectelementAssignment_6_1 ) )
            {
            // InternalEOML.g:627:1: ( ( rule__SObject__SobjectelementAssignment_6_1 ) )
            // InternalEOML.g:628:2: ( rule__SObject__SobjectelementAssignment_6_1 )
            {
             before(grammarAccess.getSObjectAccess().getSobjectelementAssignment_6_1()); 
            // InternalEOML.g:629:2: ( rule__SObject__SobjectelementAssignment_6_1 )
            // InternalEOML.g:629:3: rule__SObject__SobjectelementAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__SObject__SobjectelementAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getSObjectAccess().getSobjectelementAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group_6__1__Impl"


    // $ANTLR start "rule__SObject__Group_7__0"
    // InternalEOML.g:638:1: rule__SObject__Group_7__0 : rule__SObject__Group_7__0__Impl rule__SObject__Group_7__1 ;
    public final void rule__SObject__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:642:1: ( rule__SObject__Group_7__0__Impl rule__SObject__Group_7__1 )
            // InternalEOML.g:643:2: rule__SObject__Group_7__0__Impl rule__SObject__Group_7__1
            {
            pushFollow(FOLLOW_6);
            rule__SObject__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SObject__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group_7__0"


    // $ANTLR start "rule__SObject__Group_7__0__Impl"
    // InternalEOML.g:650:1: rule__SObject__Group_7__0__Impl : ( 'reference' ) ;
    public final void rule__SObject__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:654:1: ( ( 'reference' ) )
            // InternalEOML.g:655:1: ( 'reference' )
            {
            // InternalEOML.g:655:1: ( 'reference' )
            // InternalEOML.g:656:2: 'reference'
            {
             before(grammarAccess.getSObjectAccess().getReferenceKeyword_7_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getSObjectAccess().getReferenceKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group_7__0__Impl"


    // $ANTLR start "rule__SObject__Group_7__1"
    // InternalEOML.g:665:1: rule__SObject__Group_7__1 : rule__SObject__Group_7__1__Impl ;
    public final void rule__SObject__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:669:1: ( rule__SObject__Group_7__1__Impl )
            // InternalEOML.g:670:2: rule__SObject__Group_7__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SObject__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group_7__1"


    // $ANTLR start "rule__SObject__Group_7__1__Impl"
    // InternalEOML.g:676:1: rule__SObject__Group_7__1__Impl : ( ( rule__SObject__SobjectelementAssignment_7_1 ) ) ;
    public final void rule__SObject__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:680:1: ( ( ( rule__SObject__SobjectelementAssignment_7_1 ) ) )
            // InternalEOML.g:681:1: ( ( rule__SObject__SobjectelementAssignment_7_1 ) )
            {
            // InternalEOML.g:681:1: ( ( rule__SObject__SobjectelementAssignment_7_1 ) )
            // InternalEOML.g:682:2: ( rule__SObject__SobjectelementAssignment_7_1 )
            {
             before(grammarAccess.getSObjectAccess().getSobjectelementAssignment_7_1()); 
            // InternalEOML.g:683:2: ( rule__SObject__SobjectelementAssignment_7_1 )
            // InternalEOML.g:683:3: rule__SObject__SobjectelementAssignment_7_1
            {
            pushFollow(FOLLOW_2);
            rule__SObject__SobjectelementAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getSObjectAccess().getSobjectelementAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__Group_7__1__Impl"


    // $ANTLR start "rule__SOEltAttribute__Group__0"
    // InternalEOML.g:692:1: rule__SOEltAttribute__Group__0 : rule__SOEltAttribute__Group__0__Impl rule__SOEltAttribute__Group__1 ;
    public final void rule__SOEltAttribute__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:696:1: ( rule__SOEltAttribute__Group__0__Impl rule__SOEltAttribute__Group__1 )
            // InternalEOML.g:697:2: rule__SOEltAttribute__Group__0__Impl rule__SOEltAttribute__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__SOEltAttribute__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltAttribute__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__Group__0"


    // $ANTLR start "rule__SOEltAttribute__Group__0__Impl"
    // InternalEOML.g:704:1: rule__SOEltAttribute__Group__0__Impl : ( () ) ;
    public final void rule__SOEltAttribute__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:708:1: ( ( () ) )
            // InternalEOML.g:709:1: ( () )
            {
            // InternalEOML.g:709:1: ( () )
            // InternalEOML.g:710:2: ()
            {
             before(grammarAccess.getSOEltAttributeAccess().getSOEltAttributeAction_0()); 
            // InternalEOML.g:711:2: ()
            // InternalEOML.g:711:3: 
            {
            }

             after(grammarAccess.getSOEltAttributeAccess().getSOEltAttributeAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__Group__0__Impl"


    // $ANTLR start "rule__SOEltAttribute__Group__1"
    // InternalEOML.g:719:1: rule__SOEltAttribute__Group__1 : rule__SOEltAttribute__Group__1__Impl rule__SOEltAttribute__Group__2 ;
    public final void rule__SOEltAttribute__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:723:1: ( rule__SOEltAttribute__Group__1__Impl rule__SOEltAttribute__Group__2 )
            // InternalEOML.g:724:2: rule__SOEltAttribute__Group__1__Impl rule__SOEltAttribute__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__SOEltAttribute__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltAttribute__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__Group__1"


    // $ANTLR start "rule__SOEltAttribute__Group__1__Impl"
    // InternalEOML.g:731:1: rule__SOEltAttribute__Group__1__Impl : ( ( rule__SOEltAttribute__NameAssignment_1 ) ) ;
    public final void rule__SOEltAttribute__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:735:1: ( ( ( rule__SOEltAttribute__NameAssignment_1 ) ) )
            // InternalEOML.g:736:1: ( ( rule__SOEltAttribute__NameAssignment_1 ) )
            {
            // InternalEOML.g:736:1: ( ( rule__SOEltAttribute__NameAssignment_1 ) )
            // InternalEOML.g:737:2: ( rule__SOEltAttribute__NameAssignment_1 )
            {
             before(grammarAccess.getSOEltAttributeAccess().getNameAssignment_1()); 
            // InternalEOML.g:738:2: ( rule__SOEltAttribute__NameAssignment_1 )
            // InternalEOML.g:738:3: rule__SOEltAttribute__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__SOEltAttribute__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getSOEltAttributeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__Group__1__Impl"


    // $ANTLR start "rule__SOEltAttribute__Group__2"
    // InternalEOML.g:746:1: rule__SOEltAttribute__Group__2 : rule__SOEltAttribute__Group__2__Impl rule__SOEltAttribute__Group__3 ;
    public final void rule__SOEltAttribute__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:750:1: ( rule__SOEltAttribute__Group__2__Impl rule__SOEltAttribute__Group__3 )
            // InternalEOML.g:751:2: rule__SOEltAttribute__Group__2__Impl rule__SOEltAttribute__Group__3
            {
            pushFollow(FOLLOW_11);
            rule__SOEltAttribute__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltAttribute__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__Group__2"


    // $ANTLR start "rule__SOEltAttribute__Group__2__Impl"
    // InternalEOML.g:758:1: rule__SOEltAttribute__Group__2__Impl : ( ':' ) ;
    public final void rule__SOEltAttribute__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:762:1: ( ( ':' ) )
            // InternalEOML.g:763:1: ( ':' )
            {
            // InternalEOML.g:763:1: ( ':' )
            // InternalEOML.g:764:2: ':'
            {
             before(grammarAccess.getSOEltAttributeAccess().getColonKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getSOEltAttributeAccess().getColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__Group__2__Impl"


    // $ANTLR start "rule__SOEltAttribute__Group__3"
    // InternalEOML.g:773:1: rule__SOEltAttribute__Group__3 : rule__SOEltAttribute__Group__3__Impl ;
    public final void rule__SOEltAttribute__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:777:1: ( rule__SOEltAttribute__Group__3__Impl )
            // InternalEOML.g:778:2: rule__SOEltAttribute__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SOEltAttribute__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__Group__3"


    // $ANTLR start "rule__SOEltAttribute__Group__3__Impl"
    // InternalEOML.g:784:1: rule__SOEltAttribute__Group__3__Impl : ( ( rule__SOEltAttribute__Group_3__0 ) ) ;
    public final void rule__SOEltAttribute__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:788:1: ( ( ( rule__SOEltAttribute__Group_3__0 ) ) )
            // InternalEOML.g:789:1: ( ( rule__SOEltAttribute__Group_3__0 ) )
            {
            // InternalEOML.g:789:1: ( ( rule__SOEltAttribute__Group_3__0 ) )
            // InternalEOML.g:790:2: ( rule__SOEltAttribute__Group_3__0 )
            {
             before(grammarAccess.getSOEltAttributeAccess().getGroup_3()); 
            // InternalEOML.g:791:2: ( rule__SOEltAttribute__Group_3__0 )
            // InternalEOML.g:791:3: rule__SOEltAttribute__Group_3__0
            {
            pushFollow(FOLLOW_2);
            rule__SOEltAttribute__Group_3__0();

            state._fsp--;


            }

             after(grammarAccess.getSOEltAttributeAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__Group__3__Impl"


    // $ANTLR start "rule__SOEltAttribute__Group_3__0"
    // InternalEOML.g:800:1: rule__SOEltAttribute__Group_3__0 : rule__SOEltAttribute__Group_3__0__Impl rule__SOEltAttribute__Group_3__1 ;
    public final void rule__SOEltAttribute__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:804:1: ( rule__SOEltAttribute__Group_3__0__Impl rule__SOEltAttribute__Group_3__1 )
            // InternalEOML.g:805:2: rule__SOEltAttribute__Group_3__0__Impl rule__SOEltAttribute__Group_3__1
            {
            pushFollow(FOLLOW_4);
            rule__SOEltAttribute__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltAttribute__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__Group_3__0"


    // $ANTLR start "rule__SOEltAttribute__Group_3__0__Impl"
    // InternalEOML.g:812:1: rule__SOEltAttribute__Group_3__0__Impl : ( '[' ) ;
    public final void rule__SOEltAttribute__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:816:1: ( ( '[' ) )
            // InternalEOML.g:817:1: ( '[' )
            {
            // InternalEOML.g:817:1: ( '[' )
            // InternalEOML.g:818:2: '['
            {
             before(grammarAccess.getSOEltAttributeAccess().getLeftSquareBracketKeyword_3_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getSOEltAttributeAccess().getLeftSquareBracketKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__Group_3__0__Impl"


    // $ANTLR start "rule__SOEltAttribute__Group_3__1"
    // InternalEOML.g:827:1: rule__SOEltAttribute__Group_3__1 : rule__SOEltAttribute__Group_3__1__Impl rule__SOEltAttribute__Group_3__2 ;
    public final void rule__SOEltAttribute__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:831:1: ( rule__SOEltAttribute__Group_3__1__Impl rule__SOEltAttribute__Group_3__2 )
            // InternalEOML.g:832:2: rule__SOEltAttribute__Group_3__1__Impl rule__SOEltAttribute__Group_3__2
            {
            pushFollow(FOLLOW_12);
            rule__SOEltAttribute__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltAttribute__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__Group_3__1"


    // $ANTLR start "rule__SOEltAttribute__Group_3__1__Impl"
    // InternalEOML.g:839:1: rule__SOEltAttribute__Group_3__1__Impl : ( ( rule__SOEltAttribute__SavalueAssignment_3_1 ) ) ;
    public final void rule__SOEltAttribute__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:843:1: ( ( ( rule__SOEltAttribute__SavalueAssignment_3_1 ) ) )
            // InternalEOML.g:844:1: ( ( rule__SOEltAttribute__SavalueAssignment_3_1 ) )
            {
            // InternalEOML.g:844:1: ( ( rule__SOEltAttribute__SavalueAssignment_3_1 ) )
            // InternalEOML.g:845:2: ( rule__SOEltAttribute__SavalueAssignment_3_1 )
            {
             before(grammarAccess.getSOEltAttributeAccess().getSavalueAssignment_3_1()); 
            // InternalEOML.g:846:2: ( rule__SOEltAttribute__SavalueAssignment_3_1 )
            // InternalEOML.g:846:3: rule__SOEltAttribute__SavalueAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__SOEltAttribute__SavalueAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getSOEltAttributeAccess().getSavalueAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__Group_3__1__Impl"


    // $ANTLR start "rule__SOEltAttribute__Group_3__2"
    // InternalEOML.g:854:1: rule__SOEltAttribute__Group_3__2 : rule__SOEltAttribute__Group_3__2__Impl rule__SOEltAttribute__Group_3__3 ;
    public final void rule__SOEltAttribute__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:858:1: ( rule__SOEltAttribute__Group_3__2__Impl rule__SOEltAttribute__Group_3__3 )
            // InternalEOML.g:859:2: rule__SOEltAttribute__Group_3__2__Impl rule__SOEltAttribute__Group_3__3
            {
            pushFollow(FOLLOW_12);
            rule__SOEltAttribute__Group_3__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltAttribute__Group_3__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__Group_3__2"


    // $ANTLR start "rule__SOEltAttribute__Group_3__2__Impl"
    // InternalEOML.g:866:1: rule__SOEltAttribute__Group_3__2__Impl : ( ( rule__SOEltAttribute__Group_3_2__0 )* ) ;
    public final void rule__SOEltAttribute__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:870:1: ( ( ( rule__SOEltAttribute__Group_3_2__0 )* ) )
            // InternalEOML.g:871:1: ( ( rule__SOEltAttribute__Group_3_2__0 )* )
            {
            // InternalEOML.g:871:1: ( ( rule__SOEltAttribute__Group_3_2__0 )* )
            // InternalEOML.g:872:2: ( rule__SOEltAttribute__Group_3_2__0 )*
            {
             before(grammarAccess.getSOEltAttributeAccess().getGroup_3_2()); 
            // InternalEOML.g:873:2: ( rule__SOEltAttribute__Group_3_2__0 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==19) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalEOML.g:873:3: rule__SOEltAttribute__Group_3_2__0
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__SOEltAttribute__Group_3_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getSOEltAttributeAccess().getGroup_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__Group_3__2__Impl"


    // $ANTLR start "rule__SOEltAttribute__Group_3__3"
    // InternalEOML.g:881:1: rule__SOEltAttribute__Group_3__3 : rule__SOEltAttribute__Group_3__3__Impl ;
    public final void rule__SOEltAttribute__Group_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:885:1: ( rule__SOEltAttribute__Group_3__3__Impl )
            // InternalEOML.g:886:2: rule__SOEltAttribute__Group_3__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SOEltAttribute__Group_3__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__Group_3__3"


    // $ANTLR start "rule__SOEltAttribute__Group_3__3__Impl"
    // InternalEOML.g:892:1: rule__SOEltAttribute__Group_3__3__Impl : ( ']' ) ;
    public final void rule__SOEltAttribute__Group_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:896:1: ( ( ']' ) )
            // InternalEOML.g:897:1: ( ']' )
            {
            // InternalEOML.g:897:1: ( ']' )
            // InternalEOML.g:898:2: ']'
            {
             before(grammarAccess.getSOEltAttributeAccess().getRightSquareBracketKeyword_3_3()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getSOEltAttributeAccess().getRightSquareBracketKeyword_3_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__Group_3__3__Impl"


    // $ANTLR start "rule__SOEltAttribute__Group_3_2__0"
    // InternalEOML.g:908:1: rule__SOEltAttribute__Group_3_2__0 : rule__SOEltAttribute__Group_3_2__0__Impl rule__SOEltAttribute__Group_3_2__1 ;
    public final void rule__SOEltAttribute__Group_3_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:912:1: ( rule__SOEltAttribute__Group_3_2__0__Impl rule__SOEltAttribute__Group_3_2__1 )
            // InternalEOML.g:913:2: rule__SOEltAttribute__Group_3_2__0__Impl rule__SOEltAttribute__Group_3_2__1
            {
            pushFollow(FOLLOW_4);
            rule__SOEltAttribute__Group_3_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltAttribute__Group_3_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__Group_3_2__0"


    // $ANTLR start "rule__SOEltAttribute__Group_3_2__0__Impl"
    // InternalEOML.g:920:1: rule__SOEltAttribute__Group_3_2__0__Impl : ( ',' ) ;
    public final void rule__SOEltAttribute__Group_3_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:924:1: ( ( ',' ) )
            // InternalEOML.g:925:1: ( ',' )
            {
            // InternalEOML.g:925:1: ( ',' )
            // InternalEOML.g:926:2: ','
            {
             before(grammarAccess.getSOEltAttributeAccess().getCommaKeyword_3_2_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getSOEltAttributeAccess().getCommaKeyword_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__Group_3_2__0__Impl"


    // $ANTLR start "rule__SOEltAttribute__Group_3_2__1"
    // InternalEOML.g:935:1: rule__SOEltAttribute__Group_3_2__1 : rule__SOEltAttribute__Group_3_2__1__Impl ;
    public final void rule__SOEltAttribute__Group_3_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:939:1: ( rule__SOEltAttribute__Group_3_2__1__Impl )
            // InternalEOML.g:940:2: rule__SOEltAttribute__Group_3_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SOEltAttribute__Group_3_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__Group_3_2__1"


    // $ANTLR start "rule__SOEltAttribute__Group_3_2__1__Impl"
    // InternalEOML.g:946:1: rule__SOEltAttribute__Group_3_2__1__Impl : ( ( rule__SOEltAttribute__SavalueAssignment_3_2_1 ) ) ;
    public final void rule__SOEltAttribute__Group_3_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:950:1: ( ( ( rule__SOEltAttribute__SavalueAssignment_3_2_1 ) ) )
            // InternalEOML.g:951:1: ( ( rule__SOEltAttribute__SavalueAssignment_3_2_1 ) )
            {
            // InternalEOML.g:951:1: ( ( rule__SOEltAttribute__SavalueAssignment_3_2_1 ) )
            // InternalEOML.g:952:2: ( rule__SOEltAttribute__SavalueAssignment_3_2_1 )
            {
             before(grammarAccess.getSOEltAttributeAccess().getSavalueAssignment_3_2_1()); 
            // InternalEOML.g:953:2: ( rule__SOEltAttribute__SavalueAssignment_3_2_1 )
            // InternalEOML.g:953:3: rule__SOEltAttribute__SavalueAssignment_3_2_1
            {
            pushFollow(FOLLOW_2);
            rule__SOEltAttribute__SavalueAssignment_3_2_1();

            state._fsp--;


            }

             after(grammarAccess.getSOEltAttributeAccess().getSavalueAssignment_3_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__Group_3_2__1__Impl"


    // $ANTLR start "rule__SOEltRef__Group__0"
    // InternalEOML.g:962:1: rule__SOEltRef__Group__0 : rule__SOEltRef__Group__0__Impl rule__SOEltRef__Group__1 ;
    public final void rule__SOEltRef__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:966:1: ( rule__SOEltRef__Group__0__Impl rule__SOEltRef__Group__1 )
            // InternalEOML.g:967:2: rule__SOEltRef__Group__0__Impl rule__SOEltRef__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__SOEltRef__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltRef__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__Group__0"


    // $ANTLR start "rule__SOEltRef__Group__0__Impl"
    // InternalEOML.g:974:1: rule__SOEltRef__Group__0__Impl : ( () ) ;
    public final void rule__SOEltRef__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:978:1: ( ( () ) )
            // InternalEOML.g:979:1: ( () )
            {
            // InternalEOML.g:979:1: ( () )
            // InternalEOML.g:980:2: ()
            {
             before(grammarAccess.getSOEltRefAccess().getSOEltRefAction_0()); 
            // InternalEOML.g:981:2: ()
            // InternalEOML.g:981:3: 
            {
            }

             after(grammarAccess.getSOEltRefAccess().getSOEltRefAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__Group__0__Impl"


    // $ANTLR start "rule__SOEltRef__Group__1"
    // InternalEOML.g:989:1: rule__SOEltRef__Group__1 : rule__SOEltRef__Group__1__Impl rule__SOEltRef__Group__2 ;
    public final void rule__SOEltRef__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:993:1: ( rule__SOEltRef__Group__1__Impl rule__SOEltRef__Group__2 )
            // InternalEOML.g:994:2: rule__SOEltRef__Group__1__Impl rule__SOEltRef__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__SOEltRef__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltRef__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__Group__1"


    // $ANTLR start "rule__SOEltRef__Group__1__Impl"
    // InternalEOML.g:1001:1: rule__SOEltRef__Group__1__Impl : ( ( rule__SOEltRef__NameAssignment_1 ) ) ;
    public final void rule__SOEltRef__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1005:1: ( ( ( rule__SOEltRef__NameAssignment_1 ) ) )
            // InternalEOML.g:1006:1: ( ( rule__SOEltRef__NameAssignment_1 ) )
            {
            // InternalEOML.g:1006:1: ( ( rule__SOEltRef__NameAssignment_1 ) )
            // InternalEOML.g:1007:2: ( rule__SOEltRef__NameAssignment_1 )
            {
             before(grammarAccess.getSOEltRefAccess().getNameAssignment_1()); 
            // InternalEOML.g:1008:2: ( rule__SOEltRef__NameAssignment_1 )
            // InternalEOML.g:1008:3: rule__SOEltRef__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__SOEltRef__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getSOEltRefAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__Group__1__Impl"


    // $ANTLR start "rule__SOEltRef__Group__2"
    // InternalEOML.g:1016:1: rule__SOEltRef__Group__2 : rule__SOEltRef__Group__2__Impl rule__SOEltRef__Group__3 ;
    public final void rule__SOEltRef__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1020:1: ( rule__SOEltRef__Group__2__Impl rule__SOEltRef__Group__3 )
            // InternalEOML.g:1021:2: rule__SOEltRef__Group__2__Impl rule__SOEltRef__Group__3
            {
            pushFollow(FOLLOW_11);
            rule__SOEltRef__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltRef__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__Group__2"


    // $ANTLR start "rule__SOEltRef__Group__2__Impl"
    // InternalEOML.g:1028:1: rule__SOEltRef__Group__2__Impl : ( ':' ) ;
    public final void rule__SOEltRef__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1032:1: ( ( ':' ) )
            // InternalEOML.g:1033:1: ( ':' )
            {
            // InternalEOML.g:1033:1: ( ':' )
            // InternalEOML.g:1034:2: ':'
            {
             before(grammarAccess.getSOEltRefAccess().getColonKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getSOEltRefAccess().getColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__Group__2__Impl"


    // $ANTLR start "rule__SOEltRef__Group__3"
    // InternalEOML.g:1043:1: rule__SOEltRef__Group__3 : rule__SOEltRef__Group__3__Impl ;
    public final void rule__SOEltRef__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1047:1: ( rule__SOEltRef__Group__3__Impl )
            // InternalEOML.g:1048:2: rule__SOEltRef__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SOEltRef__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__Group__3"


    // $ANTLR start "rule__SOEltRef__Group__3__Impl"
    // InternalEOML.g:1054:1: rule__SOEltRef__Group__3__Impl : ( ( rule__SOEltRef__Group_3__0 ) ) ;
    public final void rule__SOEltRef__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1058:1: ( ( ( rule__SOEltRef__Group_3__0 ) ) )
            // InternalEOML.g:1059:1: ( ( rule__SOEltRef__Group_3__0 ) )
            {
            // InternalEOML.g:1059:1: ( ( rule__SOEltRef__Group_3__0 ) )
            // InternalEOML.g:1060:2: ( rule__SOEltRef__Group_3__0 )
            {
             before(grammarAccess.getSOEltRefAccess().getGroup_3()); 
            // InternalEOML.g:1061:2: ( rule__SOEltRef__Group_3__0 )
            // InternalEOML.g:1061:3: rule__SOEltRef__Group_3__0
            {
            pushFollow(FOLLOW_2);
            rule__SOEltRef__Group_3__0();

            state._fsp--;


            }

             after(grammarAccess.getSOEltRefAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__Group__3__Impl"


    // $ANTLR start "rule__SOEltRef__Group_3__0"
    // InternalEOML.g:1070:1: rule__SOEltRef__Group_3__0 : rule__SOEltRef__Group_3__0__Impl rule__SOEltRef__Group_3__1 ;
    public final void rule__SOEltRef__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1074:1: ( rule__SOEltRef__Group_3__0__Impl rule__SOEltRef__Group_3__1 )
            // InternalEOML.g:1075:2: rule__SOEltRef__Group_3__0__Impl rule__SOEltRef__Group_3__1
            {
            pushFollow(FOLLOW_4);
            rule__SOEltRef__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltRef__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__Group_3__0"


    // $ANTLR start "rule__SOEltRef__Group_3__0__Impl"
    // InternalEOML.g:1082:1: rule__SOEltRef__Group_3__0__Impl : ( '[' ) ;
    public final void rule__SOEltRef__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1086:1: ( ( '[' ) )
            // InternalEOML.g:1087:1: ( '[' )
            {
            // InternalEOML.g:1087:1: ( '[' )
            // InternalEOML.g:1088:2: '['
            {
             before(grammarAccess.getSOEltRefAccess().getLeftSquareBracketKeyword_3_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getSOEltRefAccess().getLeftSquareBracketKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__Group_3__0__Impl"


    // $ANTLR start "rule__SOEltRef__Group_3__1"
    // InternalEOML.g:1097:1: rule__SOEltRef__Group_3__1 : rule__SOEltRef__Group_3__1__Impl rule__SOEltRef__Group_3__2 ;
    public final void rule__SOEltRef__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1101:1: ( rule__SOEltRef__Group_3__1__Impl rule__SOEltRef__Group_3__2 )
            // InternalEOML.g:1102:2: rule__SOEltRef__Group_3__1__Impl rule__SOEltRef__Group_3__2
            {
            pushFollow(FOLLOW_12);
            rule__SOEltRef__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltRef__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__Group_3__1"


    // $ANTLR start "rule__SOEltRef__Group_3__1__Impl"
    // InternalEOML.g:1109:1: rule__SOEltRef__Group_3__1__Impl : ( ( rule__SOEltRef__SvalueobjectAssignment_3_1 ) ) ;
    public final void rule__SOEltRef__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1113:1: ( ( ( rule__SOEltRef__SvalueobjectAssignment_3_1 ) ) )
            // InternalEOML.g:1114:1: ( ( rule__SOEltRef__SvalueobjectAssignment_3_1 ) )
            {
            // InternalEOML.g:1114:1: ( ( rule__SOEltRef__SvalueobjectAssignment_3_1 ) )
            // InternalEOML.g:1115:2: ( rule__SOEltRef__SvalueobjectAssignment_3_1 )
            {
             before(grammarAccess.getSOEltRefAccess().getSvalueobjectAssignment_3_1()); 
            // InternalEOML.g:1116:2: ( rule__SOEltRef__SvalueobjectAssignment_3_1 )
            // InternalEOML.g:1116:3: rule__SOEltRef__SvalueobjectAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__SOEltRef__SvalueobjectAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getSOEltRefAccess().getSvalueobjectAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__Group_3__1__Impl"


    // $ANTLR start "rule__SOEltRef__Group_3__2"
    // InternalEOML.g:1124:1: rule__SOEltRef__Group_3__2 : rule__SOEltRef__Group_3__2__Impl rule__SOEltRef__Group_3__3 ;
    public final void rule__SOEltRef__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1128:1: ( rule__SOEltRef__Group_3__2__Impl rule__SOEltRef__Group_3__3 )
            // InternalEOML.g:1129:2: rule__SOEltRef__Group_3__2__Impl rule__SOEltRef__Group_3__3
            {
            pushFollow(FOLLOW_12);
            rule__SOEltRef__Group_3__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltRef__Group_3__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__Group_3__2"


    // $ANTLR start "rule__SOEltRef__Group_3__2__Impl"
    // InternalEOML.g:1136:1: rule__SOEltRef__Group_3__2__Impl : ( ( rule__SOEltRef__Group_3_2__0 )* ) ;
    public final void rule__SOEltRef__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1140:1: ( ( ( rule__SOEltRef__Group_3_2__0 )* ) )
            // InternalEOML.g:1141:1: ( ( rule__SOEltRef__Group_3_2__0 )* )
            {
            // InternalEOML.g:1141:1: ( ( rule__SOEltRef__Group_3_2__0 )* )
            // InternalEOML.g:1142:2: ( rule__SOEltRef__Group_3_2__0 )*
            {
             before(grammarAccess.getSOEltRefAccess().getGroup_3_2()); 
            // InternalEOML.g:1143:2: ( rule__SOEltRef__Group_3_2__0 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==19) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalEOML.g:1143:3: rule__SOEltRef__Group_3_2__0
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__SOEltRef__Group_3_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getSOEltRefAccess().getGroup_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__Group_3__2__Impl"


    // $ANTLR start "rule__SOEltRef__Group_3__3"
    // InternalEOML.g:1151:1: rule__SOEltRef__Group_3__3 : rule__SOEltRef__Group_3__3__Impl ;
    public final void rule__SOEltRef__Group_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1155:1: ( rule__SOEltRef__Group_3__3__Impl )
            // InternalEOML.g:1156:2: rule__SOEltRef__Group_3__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SOEltRef__Group_3__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__Group_3__3"


    // $ANTLR start "rule__SOEltRef__Group_3__3__Impl"
    // InternalEOML.g:1162:1: rule__SOEltRef__Group_3__3__Impl : ( ']' ) ;
    public final void rule__SOEltRef__Group_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1166:1: ( ( ']' ) )
            // InternalEOML.g:1167:1: ( ']' )
            {
            // InternalEOML.g:1167:1: ( ']' )
            // InternalEOML.g:1168:2: ']'
            {
             before(grammarAccess.getSOEltRefAccess().getRightSquareBracketKeyword_3_3()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getSOEltRefAccess().getRightSquareBracketKeyword_3_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__Group_3__3__Impl"


    // $ANTLR start "rule__SOEltRef__Group_3_2__0"
    // InternalEOML.g:1178:1: rule__SOEltRef__Group_3_2__0 : rule__SOEltRef__Group_3_2__0__Impl rule__SOEltRef__Group_3_2__1 ;
    public final void rule__SOEltRef__Group_3_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1182:1: ( rule__SOEltRef__Group_3_2__0__Impl rule__SOEltRef__Group_3_2__1 )
            // InternalEOML.g:1183:2: rule__SOEltRef__Group_3_2__0__Impl rule__SOEltRef__Group_3_2__1
            {
            pushFollow(FOLLOW_4);
            rule__SOEltRef__Group_3_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltRef__Group_3_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__Group_3_2__0"


    // $ANTLR start "rule__SOEltRef__Group_3_2__0__Impl"
    // InternalEOML.g:1190:1: rule__SOEltRef__Group_3_2__0__Impl : ( ',' ) ;
    public final void rule__SOEltRef__Group_3_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1194:1: ( ( ',' ) )
            // InternalEOML.g:1195:1: ( ',' )
            {
            // InternalEOML.g:1195:1: ( ',' )
            // InternalEOML.g:1196:2: ','
            {
             before(grammarAccess.getSOEltRefAccess().getCommaKeyword_3_2_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getSOEltRefAccess().getCommaKeyword_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__Group_3_2__0__Impl"


    // $ANTLR start "rule__SOEltRef__Group_3_2__1"
    // InternalEOML.g:1205:1: rule__SOEltRef__Group_3_2__1 : rule__SOEltRef__Group_3_2__1__Impl ;
    public final void rule__SOEltRef__Group_3_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1209:1: ( rule__SOEltRef__Group_3_2__1__Impl )
            // InternalEOML.g:1210:2: rule__SOEltRef__Group_3_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SOEltRef__Group_3_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__Group_3_2__1"


    // $ANTLR start "rule__SOEltRef__Group_3_2__1__Impl"
    // InternalEOML.g:1216:1: rule__SOEltRef__Group_3_2__1__Impl : ( ( rule__SOEltRef__SvalueobjectAssignment_3_2_1 ) ) ;
    public final void rule__SOEltRef__Group_3_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1220:1: ( ( ( rule__SOEltRef__SvalueobjectAssignment_3_2_1 ) ) )
            // InternalEOML.g:1221:1: ( ( rule__SOEltRef__SvalueobjectAssignment_3_2_1 ) )
            {
            // InternalEOML.g:1221:1: ( ( rule__SOEltRef__SvalueobjectAssignment_3_2_1 ) )
            // InternalEOML.g:1222:2: ( rule__SOEltRef__SvalueobjectAssignment_3_2_1 )
            {
             before(grammarAccess.getSOEltRefAccess().getSvalueobjectAssignment_3_2_1()); 
            // InternalEOML.g:1223:2: ( rule__SOEltRef__SvalueobjectAssignment_3_2_1 )
            // InternalEOML.g:1223:3: rule__SOEltRef__SvalueobjectAssignment_3_2_1
            {
            pushFollow(FOLLOW_2);
            rule__SOEltRef__SvalueobjectAssignment_3_2_1();

            state._fsp--;


            }

             after(grammarAccess.getSOEltRefAccess().getSvalueobjectAssignment_3_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__Group_3_2__1__Impl"


    // $ANTLR start "rule__SOEltContaiment__Group__0"
    // InternalEOML.g:1232:1: rule__SOEltContaiment__Group__0 : rule__SOEltContaiment__Group__0__Impl rule__SOEltContaiment__Group__1 ;
    public final void rule__SOEltContaiment__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1236:1: ( rule__SOEltContaiment__Group__0__Impl rule__SOEltContaiment__Group__1 )
            // InternalEOML.g:1237:2: rule__SOEltContaiment__Group__0__Impl rule__SOEltContaiment__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__SOEltContaiment__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltContaiment__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__Group__0"


    // $ANTLR start "rule__SOEltContaiment__Group__0__Impl"
    // InternalEOML.g:1244:1: rule__SOEltContaiment__Group__0__Impl : ( () ) ;
    public final void rule__SOEltContaiment__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1248:1: ( ( () ) )
            // InternalEOML.g:1249:1: ( () )
            {
            // InternalEOML.g:1249:1: ( () )
            // InternalEOML.g:1250:2: ()
            {
             before(grammarAccess.getSOEltContaimentAccess().getSOEltContaimentAction_0()); 
            // InternalEOML.g:1251:2: ()
            // InternalEOML.g:1251:3: 
            {
            }

             after(grammarAccess.getSOEltContaimentAccess().getSOEltContaimentAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__Group__0__Impl"


    // $ANTLR start "rule__SOEltContaiment__Group__1"
    // InternalEOML.g:1259:1: rule__SOEltContaiment__Group__1 : rule__SOEltContaiment__Group__1__Impl rule__SOEltContaiment__Group__2 ;
    public final void rule__SOEltContaiment__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1263:1: ( rule__SOEltContaiment__Group__1__Impl rule__SOEltContaiment__Group__2 )
            // InternalEOML.g:1264:2: rule__SOEltContaiment__Group__1__Impl rule__SOEltContaiment__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__SOEltContaiment__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltContaiment__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__Group__1"


    // $ANTLR start "rule__SOEltContaiment__Group__1__Impl"
    // InternalEOML.g:1271:1: rule__SOEltContaiment__Group__1__Impl : ( ( rule__SOEltContaiment__NameAssignment_1 ) ) ;
    public final void rule__SOEltContaiment__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1275:1: ( ( ( rule__SOEltContaiment__NameAssignment_1 ) ) )
            // InternalEOML.g:1276:1: ( ( rule__SOEltContaiment__NameAssignment_1 ) )
            {
            // InternalEOML.g:1276:1: ( ( rule__SOEltContaiment__NameAssignment_1 ) )
            // InternalEOML.g:1277:2: ( rule__SOEltContaiment__NameAssignment_1 )
            {
             before(grammarAccess.getSOEltContaimentAccess().getNameAssignment_1()); 
            // InternalEOML.g:1278:2: ( rule__SOEltContaiment__NameAssignment_1 )
            // InternalEOML.g:1278:3: rule__SOEltContaiment__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__SOEltContaiment__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getSOEltContaimentAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__Group__1__Impl"


    // $ANTLR start "rule__SOEltContaiment__Group__2"
    // InternalEOML.g:1286:1: rule__SOEltContaiment__Group__2 : rule__SOEltContaiment__Group__2__Impl rule__SOEltContaiment__Group__3 ;
    public final void rule__SOEltContaiment__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1290:1: ( rule__SOEltContaiment__Group__2__Impl rule__SOEltContaiment__Group__3 )
            // InternalEOML.g:1291:2: rule__SOEltContaiment__Group__2__Impl rule__SOEltContaiment__Group__3
            {
            pushFollow(FOLLOW_11);
            rule__SOEltContaiment__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltContaiment__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__Group__2"


    // $ANTLR start "rule__SOEltContaiment__Group__2__Impl"
    // InternalEOML.g:1298:1: rule__SOEltContaiment__Group__2__Impl : ( ':' ) ;
    public final void rule__SOEltContaiment__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1302:1: ( ( ':' ) )
            // InternalEOML.g:1303:1: ( ':' )
            {
            // InternalEOML.g:1303:1: ( ':' )
            // InternalEOML.g:1304:2: ':'
            {
             before(grammarAccess.getSOEltContaimentAccess().getColonKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getSOEltContaimentAccess().getColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__Group__2__Impl"


    // $ANTLR start "rule__SOEltContaiment__Group__3"
    // InternalEOML.g:1313:1: rule__SOEltContaiment__Group__3 : rule__SOEltContaiment__Group__3__Impl ;
    public final void rule__SOEltContaiment__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1317:1: ( rule__SOEltContaiment__Group__3__Impl )
            // InternalEOML.g:1318:2: rule__SOEltContaiment__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SOEltContaiment__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__Group__3"


    // $ANTLR start "rule__SOEltContaiment__Group__3__Impl"
    // InternalEOML.g:1324:1: rule__SOEltContaiment__Group__3__Impl : ( ( rule__SOEltContaiment__Group_3__0 ) ) ;
    public final void rule__SOEltContaiment__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1328:1: ( ( ( rule__SOEltContaiment__Group_3__0 ) ) )
            // InternalEOML.g:1329:1: ( ( rule__SOEltContaiment__Group_3__0 ) )
            {
            // InternalEOML.g:1329:1: ( ( rule__SOEltContaiment__Group_3__0 ) )
            // InternalEOML.g:1330:2: ( rule__SOEltContaiment__Group_3__0 )
            {
             before(grammarAccess.getSOEltContaimentAccess().getGroup_3()); 
            // InternalEOML.g:1331:2: ( rule__SOEltContaiment__Group_3__0 )
            // InternalEOML.g:1331:3: rule__SOEltContaiment__Group_3__0
            {
            pushFollow(FOLLOW_2);
            rule__SOEltContaiment__Group_3__0();

            state._fsp--;


            }

             after(grammarAccess.getSOEltContaimentAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__Group__3__Impl"


    // $ANTLR start "rule__SOEltContaiment__Group_3__0"
    // InternalEOML.g:1340:1: rule__SOEltContaiment__Group_3__0 : rule__SOEltContaiment__Group_3__0__Impl rule__SOEltContaiment__Group_3__1 ;
    public final void rule__SOEltContaiment__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1344:1: ( rule__SOEltContaiment__Group_3__0__Impl rule__SOEltContaiment__Group_3__1 )
            // InternalEOML.g:1345:2: rule__SOEltContaiment__Group_3__0__Impl rule__SOEltContaiment__Group_3__1
            {
            pushFollow(FOLLOW_3);
            rule__SOEltContaiment__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltContaiment__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__Group_3__0"


    // $ANTLR start "rule__SOEltContaiment__Group_3__0__Impl"
    // InternalEOML.g:1352:1: rule__SOEltContaiment__Group_3__0__Impl : ( '[' ) ;
    public final void rule__SOEltContaiment__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1356:1: ( ( '[' ) )
            // InternalEOML.g:1357:1: ( '[' )
            {
            // InternalEOML.g:1357:1: ( '[' )
            // InternalEOML.g:1358:2: '['
            {
             before(grammarAccess.getSOEltContaimentAccess().getLeftSquareBracketKeyword_3_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getSOEltContaimentAccess().getLeftSquareBracketKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__Group_3__0__Impl"


    // $ANTLR start "rule__SOEltContaiment__Group_3__1"
    // InternalEOML.g:1367:1: rule__SOEltContaiment__Group_3__1 : rule__SOEltContaiment__Group_3__1__Impl rule__SOEltContaiment__Group_3__2 ;
    public final void rule__SOEltContaiment__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1371:1: ( rule__SOEltContaiment__Group_3__1__Impl rule__SOEltContaiment__Group_3__2 )
            // InternalEOML.g:1372:2: rule__SOEltContaiment__Group_3__1__Impl rule__SOEltContaiment__Group_3__2
            {
            pushFollow(FOLLOW_12);
            rule__SOEltContaiment__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltContaiment__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__Group_3__1"


    // $ANTLR start "rule__SOEltContaiment__Group_3__1__Impl"
    // InternalEOML.g:1379:1: rule__SOEltContaiment__Group_3__1__Impl : ( ( rule__SOEltContaiment__SobjectAssignment_3_1 ) ) ;
    public final void rule__SOEltContaiment__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1383:1: ( ( ( rule__SOEltContaiment__SobjectAssignment_3_1 ) ) )
            // InternalEOML.g:1384:1: ( ( rule__SOEltContaiment__SobjectAssignment_3_1 ) )
            {
            // InternalEOML.g:1384:1: ( ( rule__SOEltContaiment__SobjectAssignment_3_1 ) )
            // InternalEOML.g:1385:2: ( rule__SOEltContaiment__SobjectAssignment_3_1 )
            {
             before(grammarAccess.getSOEltContaimentAccess().getSobjectAssignment_3_1()); 
            // InternalEOML.g:1386:2: ( rule__SOEltContaiment__SobjectAssignment_3_1 )
            // InternalEOML.g:1386:3: rule__SOEltContaiment__SobjectAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__SOEltContaiment__SobjectAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getSOEltContaimentAccess().getSobjectAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__Group_3__1__Impl"


    // $ANTLR start "rule__SOEltContaiment__Group_3__2"
    // InternalEOML.g:1394:1: rule__SOEltContaiment__Group_3__2 : rule__SOEltContaiment__Group_3__2__Impl rule__SOEltContaiment__Group_3__3 ;
    public final void rule__SOEltContaiment__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1398:1: ( rule__SOEltContaiment__Group_3__2__Impl rule__SOEltContaiment__Group_3__3 )
            // InternalEOML.g:1399:2: rule__SOEltContaiment__Group_3__2__Impl rule__SOEltContaiment__Group_3__3
            {
            pushFollow(FOLLOW_12);
            rule__SOEltContaiment__Group_3__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltContaiment__Group_3__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__Group_3__2"


    // $ANTLR start "rule__SOEltContaiment__Group_3__2__Impl"
    // InternalEOML.g:1406:1: rule__SOEltContaiment__Group_3__2__Impl : ( ( rule__SOEltContaiment__Group_3_2__0 )* ) ;
    public final void rule__SOEltContaiment__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1410:1: ( ( ( rule__SOEltContaiment__Group_3_2__0 )* ) )
            // InternalEOML.g:1411:1: ( ( rule__SOEltContaiment__Group_3_2__0 )* )
            {
            // InternalEOML.g:1411:1: ( ( rule__SOEltContaiment__Group_3_2__0 )* )
            // InternalEOML.g:1412:2: ( rule__SOEltContaiment__Group_3_2__0 )*
            {
             before(grammarAccess.getSOEltContaimentAccess().getGroup_3_2()); 
            // InternalEOML.g:1413:2: ( rule__SOEltContaiment__Group_3_2__0 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==19) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalEOML.g:1413:3: rule__SOEltContaiment__Group_3_2__0
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__SOEltContaiment__Group_3_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getSOEltContaimentAccess().getGroup_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__Group_3__2__Impl"


    // $ANTLR start "rule__SOEltContaiment__Group_3__3"
    // InternalEOML.g:1421:1: rule__SOEltContaiment__Group_3__3 : rule__SOEltContaiment__Group_3__3__Impl ;
    public final void rule__SOEltContaiment__Group_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1425:1: ( rule__SOEltContaiment__Group_3__3__Impl )
            // InternalEOML.g:1426:2: rule__SOEltContaiment__Group_3__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SOEltContaiment__Group_3__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__Group_3__3"


    // $ANTLR start "rule__SOEltContaiment__Group_3__3__Impl"
    // InternalEOML.g:1432:1: rule__SOEltContaiment__Group_3__3__Impl : ( ']' ) ;
    public final void rule__SOEltContaiment__Group_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1436:1: ( ( ']' ) )
            // InternalEOML.g:1437:1: ( ']' )
            {
            // InternalEOML.g:1437:1: ( ']' )
            // InternalEOML.g:1438:2: ']'
            {
             before(grammarAccess.getSOEltContaimentAccess().getRightSquareBracketKeyword_3_3()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getSOEltContaimentAccess().getRightSquareBracketKeyword_3_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__Group_3__3__Impl"


    // $ANTLR start "rule__SOEltContaiment__Group_3_2__0"
    // InternalEOML.g:1448:1: rule__SOEltContaiment__Group_3_2__0 : rule__SOEltContaiment__Group_3_2__0__Impl rule__SOEltContaiment__Group_3_2__1 ;
    public final void rule__SOEltContaiment__Group_3_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1452:1: ( rule__SOEltContaiment__Group_3_2__0__Impl rule__SOEltContaiment__Group_3_2__1 )
            // InternalEOML.g:1453:2: rule__SOEltContaiment__Group_3_2__0__Impl rule__SOEltContaiment__Group_3_2__1
            {
            pushFollow(FOLLOW_3);
            rule__SOEltContaiment__Group_3_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOEltContaiment__Group_3_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__Group_3_2__0"


    // $ANTLR start "rule__SOEltContaiment__Group_3_2__0__Impl"
    // InternalEOML.g:1460:1: rule__SOEltContaiment__Group_3_2__0__Impl : ( ',' ) ;
    public final void rule__SOEltContaiment__Group_3_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1464:1: ( ( ',' ) )
            // InternalEOML.g:1465:1: ( ',' )
            {
            // InternalEOML.g:1465:1: ( ',' )
            // InternalEOML.g:1466:2: ','
            {
             before(grammarAccess.getSOEltContaimentAccess().getCommaKeyword_3_2_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getSOEltContaimentAccess().getCommaKeyword_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__Group_3_2__0__Impl"


    // $ANTLR start "rule__SOEltContaiment__Group_3_2__1"
    // InternalEOML.g:1475:1: rule__SOEltContaiment__Group_3_2__1 : rule__SOEltContaiment__Group_3_2__1__Impl ;
    public final void rule__SOEltContaiment__Group_3_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1479:1: ( rule__SOEltContaiment__Group_3_2__1__Impl )
            // InternalEOML.g:1480:2: rule__SOEltContaiment__Group_3_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SOEltContaiment__Group_3_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__Group_3_2__1"


    // $ANTLR start "rule__SOEltContaiment__Group_3_2__1__Impl"
    // InternalEOML.g:1486:1: rule__SOEltContaiment__Group_3_2__1__Impl : ( ( rule__SOEltContaiment__SobjectAssignment_3_2_1 ) ) ;
    public final void rule__SOEltContaiment__Group_3_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1490:1: ( ( ( rule__SOEltContaiment__SobjectAssignment_3_2_1 ) ) )
            // InternalEOML.g:1491:1: ( ( rule__SOEltContaiment__SobjectAssignment_3_2_1 ) )
            {
            // InternalEOML.g:1491:1: ( ( rule__SOEltContaiment__SobjectAssignment_3_2_1 ) )
            // InternalEOML.g:1492:2: ( rule__SOEltContaiment__SobjectAssignment_3_2_1 )
            {
             before(grammarAccess.getSOEltContaimentAccess().getSobjectAssignment_3_2_1()); 
            // InternalEOML.g:1493:2: ( rule__SOEltContaiment__SobjectAssignment_3_2_1 )
            // InternalEOML.g:1493:3: rule__SOEltContaiment__SobjectAssignment_3_2_1
            {
            pushFollow(FOLLOW_2);
            rule__SOEltContaiment__SobjectAssignment_3_2_1();

            state._fsp--;


            }

             after(grammarAccess.getSOEltContaimentAccess().getSobjectAssignment_3_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__Group_3_2__1__Impl"


    // $ANTLR start "rule__SAValuePrimitive__Group__0"
    // InternalEOML.g:1502:1: rule__SAValuePrimitive__Group__0 : rule__SAValuePrimitive__Group__0__Impl rule__SAValuePrimitive__Group__1 ;
    public final void rule__SAValuePrimitive__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1506:1: ( rule__SAValuePrimitive__Group__0__Impl rule__SAValuePrimitive__Group__1 )
            // InternalEOML.g:1507:2: rule__SAValuePrimitive__Group__0__Impl rule__SAValuePrimitive__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__SAValuePrimitive__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SAValuePrimitive__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SAValuePrimitive__Group__0"


    // $ANTLR start "rule__SAValuePrimitive__Group__0__Impl"
    // InternalEOML.g:1514:1: rule__SAValuePrimitive__Group__0__Impl : ( () ) ;
    public final void rule__SAValuePrimitive__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1518:1: ( ( () ) )
            // InternalEOML.g:1519:1: ( () )
            {
            // InternalEOML.g:1519:1: ( () )
            // InternalEOML.g:1520:2: ()
            {
             before(grammarAccess.getSAValuePrimitiveAccess().getSValuePrimitiveAction_0()); 
            // InternalEOML.g:1521:2: ()
            // InternalEOML.g:1521:3: 
            {
            }

             after(grammarAccess.getSAValuePrimitiveAccess().getSValuePrimitiveAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SAValuePrimitive__Group__0__Impl"


    // $ANTLR start "rule__SAValuePrimitive__Group__1"
    // InternalEOML.g:1529:1: rule__SAValuePrimitive__Group__1 : rule__SAValuePrimitive__Group__1__Impl ;
    public final void rule__SAValuePrimitive__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1533:1: ( rule__SAValuePrimitive__Group__1__Impl )
            // InternalEOML.g:1534:2: rule__SAValuePrimitive__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SAValuePrimitive__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SAValuePrimitive__Group__1"


    // $ANTLR start "rule__SAValuePrimitive__Group__1__Impl"
    // InternalEOML.g:1540:1: rule__SAValuePrimitive__Group__1__Impl : ( ( rule__SAValuePrimitive__ValueAssignment_1 )? ) ;
    public final void rule__SAValuePrimitive__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1544:1: ( ( ( rule__SAValuePrimitive__ValueAssignment_1 )? ) )
            // InternalEOML.g:1545:1: ( ( rule__SAValuePrimitive__ValueAssignment_1 )? )
            {
            // InternalEOML.g:1545:1: ( ( rule__SAValuePrimitive__ValueAssignment_1 )? )
            // InternalEOML.g:1546:2: ( rule__SAValuePrimitive__ValueAssignment_1 )?
            {
             before(grammarAccess.getSAValuePrimitiveAccess().getValueAssignment_1()); 
            // InternalEOML.g:1547:2: ( rule__SAValuePrimitive__ValueAssignment_1 )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( ((LA8_0>=RULE_STRING && LA8_0<=RULE_INT)||LA8_0==20) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalEOML.g:1547:3: rule__SAValuePrimitive__ValueAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__SAValuePrimitive__ValueAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSAValuePrimitiveAccess().getValueAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SAValuePrimitive__Group__1__Impl"


    // $ANTLR start "rule__SValueObjectRef__Group__0"
    // InternalEOML.g:1556:1: rule__SValueObjectRef__Group__0 : rule__SValueObjectRef__Group__0__Impl rule__SValueObjectRef__Group__1 ;
    public final void rule__SValueObjectRef__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1560:1: ( rule__SValueObjectRef__Group__0__Impl rule__SValueObjectRef__Group__1 )
            // InternalEOML.g:1561:2: rule__SValueObjectRef__Group__0__Impl rule__SValueObjectRef__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__SValueObjectRef__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SValueObjectRef__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SValueObjectRef__Group__0"


    // $ANTLR start "rule__SValueObjectRef__Group__0__Impl"
    // InternalEOML.g:1568:1: rule__SValueObjectRef__Group__0__Impl : ( () ) ;
    public final void rule__SValueObjectRef__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1572:1: ( ( () ) )
            // InternalEOML.g:1573:1: ( () )
            {
            // InternalEOML.g:1573:1: ( () )
            // InternalEOML.g:1574:2: ()
            {
             before(grammarAccess.getSValueObjectRefAccess().getSValueObjectRefAction_0()); 
            // InternalEOML.g:1575:2: ()
            // InternalEOML.g:1575:3: 
            {
            }

             after(grammarAccess.getSValueObjectRefAccess().getSValueObjectRefAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SValueObjectRef__Group__0__Impl"


    // $ANTLR start "rule__SValueObjectRef__Group__1"
    // InternalEOML.g:1583:1: rule__SValueObjectRef__Group__1 : rule__SValueObjectRef__Group__1__Impl rule__SValueObjectRef__Group__2 ;
    public final void rule__SValueObjectRef__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1587:1: ( rule__SValueObjectRef__Group__1__Impl rule__SValueObjectRef__Group__2 )
            // InternalEOML.g:1588:2: rule__SValueObjectRef__Group__1__Impl rule__SValueObjectRef__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__SValueObjectRef__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SValueObjectRef__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SValueObjectRef__Group__1"


    // $ANTLR start "rule__SValueObjectRef__Group__1__Impl"
    // InternalEOML.g:1595:1: rule__SValueObjectRef__Group__1__Impl : ( ( rule__SValueObjectRef__NameAssignment_1 ) ) ;
    public final void rule__SValueObjectRef__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1599:1: ( ( ( rule__SValueObjectRef__NameAssignment_1 ) ) )
            // InternalEOML.g:1600:1: ( ( rule__SValueObjectRef__NameAssignment_1 ) )
            {
            // InternalEOML.g:1600:1: ( ( rule__SValueObjectRef__NameAssignment_1 ) )
            // InternalEOML.g:1601:2: ( rule__SValueObjectRef__NameAssignment_1 )
            {
             before(grammarAccess.getSValueObjectRefAccess().getNameAssignment_1()); 
            // InternalEOML.g:1602:2: ( rule__SValueObjectRef__NameAssignment_1 )
            // InternalEOML.g:1602:3: rule__SValueObjectRef__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__SValueObjectRef__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getSValueObjectRefAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SValueObjectRef__Group__1__Impl"


    // $ANTLR start "rule__SValueObjectRef__Group__2"
    // InternalEOML.g:1610:1: rule__SValueObjectRef__Group__2 : rule__SValueObjectRef__Group__2__Impl rule__SValueObjectRef__Group__3 ;
    public final void rule__SValueObjectRef__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1614:1: ( rule__SValueObjectRef__Group__2__Impl rule__SValueObjectRef__Group__3 )
            // InternalEOML.g:1615:2: rule__SValueObjectRef__Group__2__Impl rule__SValueObjectRef__Group__3
            {
            pushFollow(FOLLOW_6);
            rule__SValueObjectRef__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SValueObjectRef__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SValueObjectRef__Group__2"


    // $ANTLR start "rule__SValueObjectRef__Group__2__Impl"
    // InternalEOML.g:1622:1: rule__SValueObjectRef__Group__2__Impl : ( ':' ) ;
    public final void rule__SValueObjectRef__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1626:1: ( ( ':' ) )
            // InternalEOML.g:1627:1: ( ':' )
            {
            // InternalEOML.g:1627:1: ( ':' )
            // InternalEOML.g:1628:2: ':'
            {
             before(grammarAccess.getSValueObjectRefAccess().getColonKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getSValueObjectRefAccess().getColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SValueObjectRef__Group__2__Impl"


    // $ANTLR start "rule__SValueObjectRef__Group__3"
    // InternalEOML.g:1637:1: rule__SValueObjectRef__Group__3 : rule__SValueObjectRef__Group__3__Impl ;
    public final void rule__SValueObjectRef__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1641:1: ( rule__SValueObjectRef__Group__3__Impl )
            // InternalEOML.g:1642:2: rule__SValueObjectRef__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SValueObjectRef__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SValueObjectRef__Group__3"


    // $ANTLR start "rule__SValueObjectRef__Group__3__Impl"
    // InternalEOML.g:1648:1: rule__SValueObjectRef__Group__3__Impl : ( ( rule__SValueObjectRef__EClassAssignment_3 ) ) ;
    public final void rule__SValueObjectRef__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1652:1: ( ( ( rule__SValueObjectRef__EClassAssignment_3 ) ) )
            // InternalEOML.g:1653:1: ( ( rule__SValueObjectRef__EClassAssignment_3 ) )
            {
            // InternalEOML.g:1653:1: ( ( rule__SValueObjectRef__EClassAssignment_3 ) )
            // InternalEOML.g:1654:2: ( rule__SValueObjectRef__EClassAssignment_3 )
            {
             before(grammarAccess.getSValueObjectRefAccess().getEClassAssignment_3()); 
            // InternalEOML.g:1655:2: ( rule__SValueObjectRef__EClassAssignment_3 )
            // InternalEOML.g:1655:3: rule__SValueObjectRef__EClassAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__SValueObjectRef__EClassAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getSValueObjectRefAccess().getEClassAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SValueObjectRef__Group__3__Impl"


    // $ANTLR start "rule__EString__Group_2__0"
    // InternalEOML.g:1664:1: rule__EString__Group_2__0 : rule__EString__Group_2__0__Impl rule__EString__Group_2__1 ;
    public final void rule__EString__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1668:1: ( rule__EString__Group_2__0__Impl rule__EString__Group_2__1 )
            // InternalEOML.g:1669:2: rule__EString__Group_2__0__Impl rule__EString__Group_2__1
            {
            pushFollow(FOLLOW_4);
            rule__EString__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EString__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Group_2__0"


    // $ANTLR start "rule__EString__Group_2__0__Impl"
    // InternalEOML.g:1676:1: rule__EString__Group_2__0__Impl : ( ( '-' )? ) ;
    public final void rule__EString__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1680:1: ( ( ( '-' )? ) )
            // InternalEOML.g:1681:1: ( ( '-' )? )
            {
            // InternalEOML.g:1681:1: ( ( '-' )? )
            // InternalEOML.g:1682:2: ( '-' )?
            {
             before(grammarAccess.getEStringAccess().getHyphenMinusKeyword_2_0()); 
            // InternalEOML.g:1683:2: ( '-' )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==20) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalEOML.g:1683:3: '-'
                    {
                    match(input,20,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getEStringAccess().getHyphenMinusKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Group_2__0__Impl"


    // $ANTLR start "rule__EString__Group_2__1"
    // InternalEOML.g:1691:1: rule__EString__Group_2__1 : rule__EString__Group_2__1__Impl ;
    public final void rule__EString__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1695:1: ( rule__EString__Group_2__1__Impl )
            // InternalEOML.g:1696:2: rule__EString__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EString__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Group_2__1"


    // $ANTLR start "rule__EString__Group_2__1__Impl"
    // InternalEOML.g:1702:1: rule__EString__Group_2__1__Impl : ( RULE_INT ) ;
    public final void rule__EString__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1706:1: ( ( RULE_INT ) )
            // InternalEOML.g:1707:1: ( RULE_INT )
            {
            // InternalEOML.g:1707:1: ( RULE_INT )
            // InternalEOML.g:1708:2: RULE_INT
            {
             before(grammarAccess.getEStringAccess().getINTTerminalRuleCall_2_1()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getEStringAccess().getINTTerminalRuleCall_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Group_2__1__Impl"


    // $ANTLR start "rule__SObject__NameAssignment_2"
    // InternalEOML.g:1718:1: rule__SObject__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__SObject__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1722:1: ( ( ruleEString ) )
            // InternalEOML.g:1723:2: ( ruleEString )
            {
            // InternalEOML.g:1723:2: ( ruleEString )
            // InternalEOML.g:1724:3: ruleEString
            {
             before(grammarAccess.getSObjectAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getSObjectAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__NameAssignment_2"


    // $ANTLR start "rule__SObject__EClassAssignment_4"
    // InternalEOML.g:1733:1: rule__SObject__EClassAssignment_4 : ( RULE_ID ) ;
    public final void rule__SObject__EClassAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1737:1: ( ( RULE_ID ) )
            // InternalEOML.g:1738:2: ( RULE_ID )
            {
            // InternalEOML.g:1738:2: ( RULE_ID )
            // InternalEOML.g:1739:3: RULE_ID
            {
             before(grammarAccess.getSObjectAccess().getEClassIDTerminalRuleCall_4_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getSObjectAccess().getEClassIDTerminalRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__EClassAssignment_4"


    // $ANTLR start "rule__SObject__SobjectelementAssignment_6_1"
    // InternalEOML.g:1748:1: rule__SObject__SobjectelementAssignment_6_1 : ( ruleSObjectElement ) ;
    public final void rule__SObject__SobjectelementAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1752:1: ( ( ruleSObjectElement ) )
            // InternalEOML.g:1753:2: ( ruleSObjectElement )
            {
            // InternalEOML.g:1753:2: ( ruleSObjectElement )
            // InternalEOML.g:1754:3: ruleSObjectElement
            {
             before(grammarAccess.getSObjectAccess().getSobjectelementSObjectElementParserRuleCall_6_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSObjectElement();

            state._fsp--;

             after(grammarAccess.getSObjectAccess().getSobjectelementSObjectElementParserRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__SobjectelementAssignment_6_1"


    // $ANTLR start "rule__SObject__SobjectelementAssignment_7_1"
    // InternalEOML.g:1763:1: rule__SObject__SobjectelementAssignment_7_1 : ( ruleSObjectElement ) ;
    public final void rule__SObject__SobjectelementAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1767:1: ( ( ruleSObjectElement ) )
            // InternalEOML.g:1768:2: ( ruleSObjectElement )
            {
            // InternalEOML.g:1768:2: ( ruleSObjectElement )
            // InternalEOML.g:1769:3: ruleSObjectElement
            {
             before(grammarAccess.getSObjectAccess().getSobjectelementSObjectElementParserRuleCall_7_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSObjectElement();

            state._fsp--;

             after(grammarAccess.getSObjectAccess().getSobjectelementSObjectElementParserRuleCall_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SObject__SobjectelementAssignment_7_1"


    // $ANTLR start "rule__SOEltAttribute__NameAssignment_1"
    // InternalEOML.g:1778:1: rule__SOEltAttribute__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__SOEltAttribute__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1782:1: ( ( RULE_ID ) )
            // InternalEOML.g:1783:2: ( RULE_ID )
            {
            // InternalEOML.g:1783:2: ( RULE_ID )
            // InternalEOML.g:1784:3: RULE_ID
            {
             before(grammarAccess.getSOEltAttributeAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getSOEltAttributeAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__NameAssignment_1"


    // $ANTLR start "rule__SOEltAttribute__SavalueAssignment_3_1"
    // InternalEOML.g:1793:1: rule__SOEltAttribute__SavalueAssignment_3_1 : ( ruleSAValue ) ;
    public final void rule__SOEltAttribute__SavalueAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1797:1: ( ( ruleSAValue ) )
            // InternalEOML.g:1798:2: ( ruleSAValue )
            {
            // InternalEOML.g:1798:2: ( ruleSAValue )
            // InternalEOML.g:1799:3: ruleSAValue
            {
             before(grammarAccess.getSOEltAttributeAccess().getSavalueSAValueParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSAValue();

            state._fsp--;

             after(grammarAccess.getSOEltAttributeAccess().getSavalueSAValueParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__SavalueAssignment_3_1"


    // $ANTLR start "rule__SOEltAttribute__SavalueAssignment_3_2_1"
    // InternalEOML.g:1808:1: rule__SOEltAttribute__SavalueAssignment_3_2_1 : ( ruleSAValue ) ;
    public final void rule__SOEltAttribute__SavalueAssignment_3_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1812:1: ( ( ruleSAValue ) )
            // InternalEOML.g:1813:2: ( ruleSAValue )
            {
            // InternalEOML.g:1813:2: ( ruleSAValue )
            // InternalEOML.g:1814:3: ruleSAValue
            {
             before(grammarAccess.getSOEltAttributeAccess().getSavalueSAValueParserRuleCall_3_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSAValue();

            state._fsp--;

             after(grammarAccess.getSOEltAttributeAccess().getSavalueSAValueParserRuleCall_3_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltAttribute__SavalueAssignment_3_2_1"


    // $ANTLR start "rule__SOEltRef__NameAssignment_1"
    // InternalEOML.g:1823:1: rule__SOEltRef__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__SOEltRef__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1827:1: ( ( RULE_ID ) )
            // InternalEOML.g:1828:2: ( RULE_ID )
            {
            // InternalEOML.g:1828:2: ( RULE_ID )
            // InternalEOML.g:1829:3: RULE_ID
            {
             before(grammarAccess.getSOEltRefAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getSOEltRefAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__NameAssignment_1"


    // $ANTLR start "rule__SOEltRef__SvalueobjectAssignment_3_1"
    // InternalEOML.g:1838:1: rule__SOEltRef__SvalueobjectAssignment_3_1 : ( ruleSValueObjectRef ) ;
    public final void rule__SOEltRef__SvalueobjectAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1842:1: ( ( ruleSValueObjectRef ) )
            // InternalEOML.g:1843:2: ( ruleSValueObjectRef )
            {
            // InternalEOML.g:1843:2: ( ruleSValueObjectRef )
            // InternalEOML.g:1844:3: ruleSValueObjectRef
            {
             before(grammarAccess.getSOEltRefAccess().getSvalueobjectSValueObjectRefParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSValueObjectRef();

            state._fsp--;

             after(grammarAccess.getSOEltRefAccess().getSvalueobjectSValueObjectRefParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__SvalueobjectAssignment_3_1"


    // $ANTLR start "rule__SOEltRef__SvalueobjectAssignment_3_2_1"
    // InternalEOML.g:1853:1: rule__SOEltRef__SvalueobjectAssignment_3_2_1 : ( ruleSValueObjectRef ) ;
    public final void rule__SOEltRef__SvalueobjectAssignment_3_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1857:1: ( ( ruleSValueObjectRef ) )
            // InternalEOML.g:1858:2: ( ruleSValueObjectRef )
            {
            // InternalEOML.g:1858:2: ( ruleSValueObjectRef )
            // InternalEOML.g:1859:3: ruleSValueObjectRef
            {
             before(grammarAccess.getSOEltRefAccess().getSvalueobjectSValueObjectRefParserRuleCall_3_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSValueObjectRef();

            state._fsp--;

             after(grammarAccess.getSOEltRefAccess().getSvalueobjectSValueObjectRefParserRuleCall_3_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltRef__SvalueobjectAssignment_3_2_1"


    // $ANTLR start "rule__SOEltContaiment__NameAssignment_1"
    // InternalEOML.g:1868:1: rule__SOEltContaiment__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__SOEltContaiment__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1872:1: ( ( RULE_ID ) )
            // InternalEOML.g:1873:2: ( RULE_ID )
            {
            // InternalEOML.g:1873:2: ( RULE_ID )
            // InternalEOML.g:1874:3: RULE_ID
            {
             before(grammarAccess.getSOEltContaimentAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getSOEltContaimentAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__NameAssignment_1"


    // $ANTLR start "rule__SOEltContaiment__SobjectAssignment_3_1"
    // InternalEOML.g:1883:1: rule__SOEltContaiment__SobjectAssignment_3_1 : ( ruleSObject ) ;
    public final void rule__SOEltContaiment__SobjectAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1887:1: ( ( ruleSObject ) )
            // InternalEOML.g:1888:2: ( ruleSObject )
            {
            // InternalEOML.g:1888:2: ( ruleSObject )
            // InternalEOML.g:1889:3: ruleSObject
            {
             before(grammarAccess.getSOEltContaimentAccess().getSobjectSObjectParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSObject();

            state._fsp--;

             after(grammarAccess.getSOEltContaimentAccess().getSobjectSObjectParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__SobjectAssignment_3_1"


    // $ANTLR start "rule__SOEltContaiment__SobjectAssignment_3_2_1"
    // InternalEOML.g:1898:1: rule__SOEltContaiment__SobjectAssignment_3_2_1 : ( ruleSObject ) ;
    public final void rule__SOEltContaiment__SobjectAssignment_3_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1902:1: ( ( ruleSObject ) )
            // InternalEOML.g:1903:2: ( ruleSObject )
            {
            // InternalEOML.g:1903:2: ( ruleSObject )
            // InternalEOML.g:1904:3: ruleSObject
            {
             before(grammarAccess.getSOEltContaimentAccess().getSobjectSObjectParserRuleCall_3_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSObject();

            state._fsp--;

             after(grammarAccess.getSOEltContaimentAccess().getSobjectSObjectParserRuleCall_3_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOEltContaiment__SobjectAssignment_3_2_1"


    // $ANTLR start "rule__SAValuePrimitive__ValueAssignment_1"
    // InternalEOML.g:1913:1: rule__SAValuePrimitive__ValueAssignment_1 : ( ruleEString ) ;
    public final void rule__SAValuePrimitive__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1917:1: ( ( ruleEString ) )
            // InternalEOML.g:1918:2: ( ruleEString )
            {
            // InternalEOML.g:1918:2: ( ruleEString )
            // InternalEOML.g:1919:3: ruleEString
            {
             before(grammarAccess.getSAValuePrimitiveAccess().getValueEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getSAValuePrimitiveAccess().getValueEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SAValuePrimitive__ValueAssignment_1"


    // $ANTLR start "rule__SValueObjectRef__NameAssignment_1"
    // InternalEOML.g:1928:1: rule__SValueObjectRef__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__SValueObjectRef__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1932:1: ( ( ruleEString ) )
            // InternalEOML.g:1933:2: ( ruleEString )
            {
            // InternalEOML.g:1933:2: ( ruleEString )
            // InternalEOML.g:1934:3: ruleEString
            {
             before(grammarAccess.getSValueObjectRefAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getSValueObjectRefAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SValueObjectRef__NameAssignment_1"


    // $ANTLR start "rule__SValueObjectRef__EClassAssignment_3"
    // InternalEOML.g:1943:1: rule__SValueObjectRef__EClassAssignment_3 : ( RULE_ID ) ;
    public final void rule__SValueObjectRef__EClassAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEOML.g:1947:1: ( ( RULE_ID ) )
            // InternalEOML.g:1948:2: ( RULE_ID )
            {
            // InternalEOML.g:1948:2: ( RULE_ID )
            // InternalEOML.g:1949:3: RULE_ID
            {
             before(grammarAccess.getSValueObjectRefAccess().getEClassIDTerminalRuleCall_3_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getSValueObjectRefAccess().getEClassIDTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SValueObjectRef__EClassAssignment_3"

    // Delegated rules


    protected DFA1 dfa1 = new DFA1(this);
    static final String dfa_1s = "\13\uffff";
    static final String dfa_2s = "\1\5\1\14\1\21\1\4\2\14\1\6\1\14\3\uffff";
    static final String dfa_3s = "\1\5\1\14\1\21\1\24\2\23\1\6\1\23\3\uffff";
    static final String dfa_4s = "\10\uffff\1\1\1\3\1\2";
    static final String dfa_5s = "\13\uffff}>";
    static final String[] dfa_6s = {
            "\1\1",
            "\1\2",
            "\1\3",
            "\1\4\1\5\1\7\4\uffff\1\11\6\uffff\2\10\1\6",
            "\1\12\5\uffff\2\10",
            "\1\12\5\uffff\2\10",
            "\1\7",
            "\1\12\5\uffff\2\10",
            "",
            "",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA1 extends DFA {

        public DFA1(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 1;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "287:1: rule__SObjectElement__Alternatives : ( ( ruleSOEltAttribute ) | ( ruleSOEltRef ) | ( ruleSOEltContaiment ) );";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000100070L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x000000000001C000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x00000000000C0000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000080002L});

}