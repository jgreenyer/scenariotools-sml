/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * ScenarioTools-URL: www.scenariotools.org
 *
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete
 */
grammar InternalEOML;

options {
	superClass=AbstractInternalContentAssistParser;
}

@lexer::header {
package org.scenariotools.emf.objectmodellanguage.eoml.ide.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.Lexer;
}

@parser::header {
package org.scenariotools.emf.objectmodellanguage.eoml.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.scenariotools.emf.objectmodellanguage.eoml.services.EOMLGrammarAccess;

}
@parser::members {
	private EOMLGrammarAccess grammarAccess;

	public void setGrammarAccess(EOMLGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}

	@Override
	protected Grammar getGrammar() {
		return grammarAccess.getGrammar();
	}

	@Override
	protected String getValueForTokenName(String tokenName) {
		return tokenName;
	}
}

// Entry rule entryRuleSObject
entryRuleSObject
:
{ before(grammarAccess.getSObjectRule()); }
	 ruleSObject
{ after(grammarAccess.getSObjectRule()); } 
	 EOF 
;

// Rule SObject
ruleSObject 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getSObjectAccess().getGroup()); }
		(rule__SObject__Group__0)
		{ after(grammarAccess.getSObjectAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleSObjectElement
entryRuleSObjectElement
:
{ before(grammarAccess.getSObjectElementRule()); }
	 ruleSObjectElement
{ after(grammarAccess.getSObjectElementRule()); } 
	 EOF 
;

// Rule SObjectElement
ruleSObjectElement 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getSObjectElementAccess().getAlternatives()); }
		(rule__SObjectElement__Alternatives)
		{ after(grammarAccess.getSObjectElementAccess().getAlternatives()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleSOEltAttribute
entryRuleSOEltAttribute
:
{ before(grammarAccess.getSOEltAttributeRule()); }
	 ruleSOEltAttribute
{ after(grammarAccess.getSOEltAttributeRule()); } 
	 EOF 
;

// Rule SOEltAttribute
ruleSOEltAttribute 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getSOEltAttributeAccess().getGroup()); }
		(rule__SOEltAttribute__Group__0)
		{ after(grammarAccess.getSOEltAttributeAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleSOEltRef
entryRuleSOEltRef
:
{ before(grammarAccess.getSOEltRefRule()); }
	 ruleSOEltRef
{ after(grammarAccess.getSOEltRefRule()); } 
	 EOF 
;

// Rule SOEltRef
ruleSOEltRef 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getSOEltRefAccess().getGroup()); }
		(rule__SOEltRef__Group__0)
		{ after(grammarAccess.getSOEltRefAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleSOEltContaiment
entryRuleSOEltContaiment
:
{ before(grammarAccess.getSOEltContaimentRule()); }
	 ruleSOEltContaiment
{ after(grammarAccess.getSOEltContaimentRule()); } 
	 EOF 
;

// Rule SOEltContaiment
ruleSOEltContaiment 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getSOEltContaimentAccess().getGroup()); }
		(rule__SOEltContaiment__Group__0)
		{ after(grammarAccess.getSOEltContaimentAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleSAValue
entryRuleSAValue
:
{ before(grammarAccess.getSAValueRule()); }
	 ruleSAValue
{ after(grammarAccess.getSAValueRule()); } 
	 EOF 
;

// Rule SAValue
ruleSAValue 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getSAValueAccess().getSAValuePrimitiveParserRuleCall()); }
		ruleSAValuePrimitive
		{ after(grammarAccess.getSAValueAccess().getSAValuePrimitiveParserRuleCall()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleSAValuePrimitive
entryRuleSAValuePrimitive
:
{ before(grammarAccess.getSAValuePrimitiveRule()); }
	 ruleSAValuePrimitive
{ after(grammarAccess.getSAValuePrimitiveRule()); } 
	 EOF 
;

// Rule SAValuePrimitive
ruleSAValuePrimitive 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getSAValuePrimitiveAccess().getGroup()); }
		(rule__SAValuePrimitive__Group__0)
		{ after(grammarAccess.getSAValuePrimitiveAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleSValueObjectRef
entryRuleSValueObjectRef
:
{ before(grammarAccess.getSValueObjectRefRule()); }
	 ruleSValueObjectRef
{ after(grammarAccess.getSValueObjectRefRule()); } 
	 EOF 
;

// Rule SValueObjectRef
ruleSValueObjectRef 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getSValueObjectRefAccess().getGroup()); }
		(rule__SValueObjectRef__Group__0)
		{ after(grammarAccess.getSValueObjectRefAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleEString
entryRuleEString
:
{ before(grammarAccess.getEStringRule()); }
	 ruleEString
{ after(grammarAccess.getEStringRule()); } 
	 EOF 
;

// Rule EString
ruleEString 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getEStringAccess().getAlternatives()); }
		(rule__EString__Alternatives)
		{ after(grammarAccess.getEStringAccess().getAlternatives()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SObjectElement__Alternatives
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSObjectElementAccess().getSOEltAttributeParserRuleCall_0()); }
		ruleSOEltAttribute
		{ after(grammarAccess.getSObjectElementAccess().getSOEltAttributeParserRuleCall_0()); }
	)
	|
	(
		{ before(grammarAccess.getSObjectElementAccess().getSOEltRefParserRuleCall_1()); }
		ruleSOEltRef
		{ after(grammarAccess.getSObjectElementAccess().getSOEltRefParserRuleCall_1()); }
	)
	|
	(
		{ before(grammarAccess.getSObjectElementAccess().getSOEltContaimentParserRuleCall_2()); }
		ruleSOEltContaiment
		{ after(grammarAccess.getSObjectElementAccess().getSOEltContaimentParserRuleCall_2()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__EString__Alternatives
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); }
		RULE_STRING
		{ after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); }
	)
	|
	(
		{ before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); }
		RULE_ID
		{ after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); }
	)
	|
	(
		{ before(grammarAccess.getEStringAccess().getGroup_2()); }
		(rule__EString__Group_2__0)
		{ after(grammarAccess.getEStringAccess().getGroup_2()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SObject__Group__0__Impl
	rule__SObject__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSObjectAccess().getSObjectAction_0()); }
	()
	{ after(grammarAccess.getSObjectAccess().getSObjectAction_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SObject__Group__1__Impl
	rule__SObject__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSObjectAccess().getObjectKeyword_1()); }
	'object'
	{ after(grammarAccess.getSObjectAccess().getObjectKeyword_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SObject__Group__2__Impl
	rule__SObject__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSObjectAccess().getNameAssignment_2()); }
	(rule__SObject__NameAssignment_2)
	{ after(grammarAccess.getSObjectAccess().getNameAssignment_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SObject__Group__3__Impl
	rule__SObject__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSObjectAccess().getColonKeyword_3()); }
	':'
	{ after(grammarAccess.getSObjectAccess().getColonKeyword_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group__4
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SObject__Group__4__Impl
	rule__SObject__Group__5
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group__4__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSObjectAccess().getEClassAssignment_4()); }
	(rule__SObject__EClassAssignment_4)
	{ after(grammarAccess.getSObjectAccess().getEClassAssignment_4()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group__5
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SObject__Group__5__Impl
	rule__SObject__Group__6
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group__5__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSObjectAccess().getLeftCurlyBracketKeyword_5()); }
	'{'
	{ after(grammarAccess.getSObjectAccess().getLeftCurlyBracketKeyword_5()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group__6
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SObject__Group__6__Impl
	rule__SObject__Group__7
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group__6__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSObjectAccess().getGroup_6()); }
	(rule__SObject__Group_6__0)*
	{ after(grammarAccess.getSObjectAccess().getGroup_6()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group__7
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SObject__Group__7__Impl
	rule__SObject__Group__8
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group__7__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSObjectAccess().getGroup_7()); }
	(rule__SObject__Group_7__0)*
	{ after(grammarAccess.getSObjectAccess().getGroup_7()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group__8
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SObject__Group__8__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group__8__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSObjectAccess().getRightCurlyBracketKeyword_8()); }
	'}'
	{ after(grammarAccess.getSObjectAccess().getRightCurlyBracketKeyword_8()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__SObject__Group_6__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SObject__Group_6__0__Impl
	rule__SObject__Group_6__1
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group_6__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSObjectAccess().getAttributeKeyword_6_0()); }
	'attribute'
	{ after(grammarAccess.getSObjectAccess().getAttributeKeyword_6_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group_6__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SObject__Group_6__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group_6__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSObjectAccess().getSobjectelementAssignment_6_1()); }
	(rule__SObject__SobjectelementAssignment_6_1)
	{ after(grammarAccess.getSObjectAccess().getSobjectelementAssignment_6_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__SObject__Group_7__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SObject__Group_7__0__Impl
	rule__SObject__Group_7__1
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group_7__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSObjectAccess().getReferenceKeyword_7_0()); }
	'reference'
	{ after(grammarAccess.getSObjectAccess().getReferenceKeyword_7_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group_7__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SObject__Group_7__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__Group_7__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSObjectAccess().getSobjectelementAssignment_7_1()); }
	(rule__SObject__SobjectelementAssignment_7_1)
	{ after(grammarAccess.getSObjectAccess().getSobjectelementAssignment_7_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__SOEltAttribute__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltAttribute__Group__0__Impl
	rule__SOEltAttribute__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltAttribute__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltAttributeAccess().getSOEltAttributeAction_0()); }
	()
	{ after(grammarAccess.getSOEltAttributeAccess().getSOEltAttributeAction_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltAttribute__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltAttribute__Group__1__Impl
	rule__SOEltAttribute__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltAttribute__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltAttributeAccess().getNameAssignment_1()); }
	(rule__SOEltAttribute__NameAssignment_1)
	{ after(grammarAccess.getSOEltAttributeAccess().getNameAssignment_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltAttribute__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltAttribute__Group__2__Impl
	rule__SOEltAttribute__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltAttribute__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltAttributeAccess().getColonKeyword_2()); }
	':'
	{ after(grammarAccess.getSOEltAttributeAccess().getColonKeyword_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltAttribute__Group__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltAttribute__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltAttribute__Group__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltAttributeAccess().getGroup_3()); }
	(rule__SOEltAttribute__Group_3__0)
	{ after(grammarAccess.getSOEltAttributeAccess().getGroup_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__SOEltAttribute__Group_3__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltAttribute__Group_3__0__Impl
	rule__SOEltAttribute__Group_3__1
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltAttribute__Group_3__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltAttributeAccess().getLeftSquareBracketKeyword_3_0()); }
	'['
	{ after(grammarAccess.getSOEltAttributeAccess().getLeftSquareBracketKeyword_3_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltAttribute__Group_3__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltAttribute__Group_3__1__Impl
	rule__SOEltAttribute__Group_3__2
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltAttribute__Group_3__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltAttributeAccess().getSavalueAssignment_3_1()); }
	(rule__SOEltAttribute__SavalueAssignment_3_1)
	{ after(grammarAccess.getSOEltAttributeAccess().getSavalueAssignment_3_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltAttribute__Group_3__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltAttribute__Group_3__2__Impl
	rule__SOEltAttribute__Group_3__3
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltAttribute__Group_3__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltAttributeAccess().getGroup_3_2()); }
	(rule__SOEltAttribute__Group_3_2__0)*
	{ after(grammarAccess.getSOEltAttributeAccess().getGroup_3_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltAttribute__Group_3__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltAttribute__Group_3__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltAttribute__Group_3__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltAttributeAccess().getRightSquareBracketKeyword_3_3()); }
	']'
	{ after(grammarAccess.getSOEltAttributeAccess().getRightSquareBracketKeyword_3_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__SOEltAttribute__Group_3_2__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltAttribute__Group_3_2__0__Impl
	rule__SOEltAttribute__Group_3_2__1
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltAttribute__Group_3_2__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltAttributeAccess().getCommaKeyword_3_2_0()); }
	','
	{ after(grammarAccess.getSOEltAttributeAccess().getCommaKeyword_3_2_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltAttribute__Group_3_2__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltAttribute__Group_3_2__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltAttribute__Group_3_2__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltAttributeAccess().getSavalueAssignment_3_2_1()); }
	(rule__SOEltAttribute__SavalueAssignment_3_2_1)
	{ after(grammarAccess.getSOEltAttributeAccess().getSavalueAssignment_3_2_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__SOEltRef__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltRef__Group__0__Impl
	rule__SOEltRef__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltRef__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltRefAccess().getSOEltRefAction_0()); }
	()
	{ after(grammarAccess.getSOEltRefAccess().getSOEltRefAction_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltRef__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltRef__Group__1__Impl
	rule__SOEltRef__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltRef__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltRefAccess().getNameAssignment_1()); }
	(rule__SOEltRef__NameAssignment_1)
	{ after(grammarAccess.getSOEltRefAccess().getNameAssignment_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltRef__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltRef__Group__2__Impl
	rule__SOEltRef__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltRef__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltRefAccess().getColonKeyword_2()); }
	':'
	{ after(grammarAccess.getSOEltRefAccess().getColonKeyword_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltRef__Group__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltRef__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltRef__Group__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltRefAccess().getGroup_3()); }
	(rule__SOEltRef__Group_3__0)
	{ after(grammarAccess.getSOEltRefAccess().getGroup_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__SOEltRef__Group_3__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltRef__Group_3__0__Impl
	rule__SOEltRef__Group_3__1
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltRef__Group_3__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltRefAccess().getLeftSquareBracketKeyword_3_0()); }
	'['
	{ after(grammarAccess.getSOEltRefAccess().getLeftSquareBracketKeyword_3_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltRef__Group_3__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltRef__Group_3__1__Impl
	rule__SOEltRef__Group_3__2
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltRef__Group_3__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltRefAccess().getSvalueobjectAssignment_3_1()); }
	(rule__SOEltRef__SvalueobjectAssignment_3_1)
	{ after(grammarAccess.getSOEltRefAccess().getSvalueobjectAssignment_3_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltRef__Group_3__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltRef__Group_3__2__Impl
	rule__SOEltRef__Group_3__3
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltRef__Group_3__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltRefAccess().getGroup_3_2()); }
	(rule__SOEltRef__Group_3_2__0)*
	{ after(grammarAccess.getSOEltRefAccess().getGroup_3_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltRef__Group_3__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltRef__Group_3__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltRef__Group_3__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltRefAccess().getRightSquareBracketKeyword_3_3()); }
	']'
	{ after(grammarAccess.getSOEltRefAccess().getRightSquareBracketKeyword_3_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__SOEltRef__Group_3_2__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltRef__Group_3_2__0__Impl
	rule__SOEltRef__Group_3_2__1
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltRef__Group_3_2__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltRefAccess().getCommaKeyword_3_2_0()); }
	','
	{ after(grammarAccess.getSOEltRefAccess().getCommaKeyword_3_2_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltRef__Group_3_2__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltRef__Group_3_2__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltRef__Group_3_2__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltRefAccess().getSvalueobjectAssignment_3_2_1()); }
	(rule__SOEltRef__SvalueobjectAssignment_3_2_1)
	{ after(grammarAccess.getSOEltRefAccess().getSvalueobjectAssignment_3_2_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__SOEltContaiment__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltContaiment__Group__0__Impl
	rule__SOEltContaiment__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltContaiment__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltContaimentAccess().getSOEltContaimentAction_0()); }
	()
	{ after(grammarAccess.getSOEltContaimentAccess().getSOEltContaimentAction_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltContaiment__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltContaiment__Group__1__Impl
	rule__SOEltContaiment__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltContaiment__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltContaimentAccess().getNameAssignment_1()); }
	(rule__SOEltContaiment__NameAssignment_1)
	{ after(grammarAccess.getSOEltContaimentAccess().getNameAssignment_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltContaiment__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltContaiment__Group__2__Impl
	rule__SOEltContaiment__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltContaiment__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltContaimentAccess().getColonKeyword_2()); }
	':'
	{ after(grammarAccess.getSOEltContaimentAccess().getColonKeyword_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltContaiment__Group__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltContaiment__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltContaiment__Group__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltContaimentAccess().getGroup_3()); }
	(rule__SOEltContaiment__Group_3__0)
	{ after(grammarAccess.getSOEltContaimentAccess().getGroup_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__SOEltContaiment__Group_3__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltContaiment__Group_3__0__Impl
	rule__SOEltContaiment__Group_3__1
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltContaiment__Group_3__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltContaimentAccess().getLeftSquareBracketKeyword_3_0()); }
	'['
	{ after(grammarAccess.getSOEltContaimentAccess().getLeftSquareBracketKeyword_3_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltContaiment__Group_3__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltContaiment__Group_3__1__Impl
	rule__SOEltContaiment__Group_3__2
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltContaiment__Group_3__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltContaimentAccess().getSobjectAssignment_3_1()); }
	(rule__SOEltContaiment__SobjectAssignment_3_1)
	{ after(grammarAccess.getSOEltContaimentAccess().getSobjectAssignment_3_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltContaiment__Group_3__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltContaiment__Group_3__2__Impl
	rule__SOEltContaiment__Group_3__3
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltContaiment__Group_3__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltContaimentAccess().getGroup_3_2()); }
	(rule__SOEltContaiment__Group_3_2__0)*
	{ after(grammarAccess.getSOEltContaimentAccess().getGroup_3_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltContaiment__Group_3__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltContaiment__Group_3__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltContaiment__Group_3__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltContaimentAccess().getRightSquareBracketKeyword_3_3()); }
	']'
	{ after(grammarAccess.getSOEltContaimentAccess().getRightSquareBracketKeyword_3_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__SOEltContaiment__Group_3_2__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltContaiment__Group_3_2__0__Impl
	rule__SOEltContaiment__Group_3_2__1
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltContaiment__Group_3_2__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltContaimentAccess().getCommaKeyword_3_2_0()); }
	','
	{ after(grammarAccess.getSOEltContaimentAccess().getCommaKeyword_3_2_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltContaiment__Group_3_2__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SOEltContaiment__Group_3_2__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltContaiment__Group_3_2__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSOEltContaimentAccess().getSobjectAssignment_3_2_1()); }
	(rule__SOEltContaiment__SobjectAssignment_3_2_1)
	{ after(grammarAccess.getSOEltContaimentAccess().getSobjectAssignment_3_2_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__SAValuePrimitive__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SAValuePrimitive__Group__0__Impl
	rule__SAValuePrimitive__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__SAValuePrimitive__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSAValuePrimitiveAccess().getSValuePrimitiveAction_0()); }
	()
	{ after(grammarAccess.getSAValuePrimitiveAccess().getSValuePrimitiveAction_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SAValuePrimitive__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SAValuePrimitive__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__SAValuePrimitive__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSAValuePrimitiveAccess().getValueAssignment_1()); }
	(rule__SAValuePrimitive__ValueAssignment_1)?
	{ after(grammarAccess.getSAValuePrimitiveAccess().getValueAssignment_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__SValueObjectRef__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SValueObjectRef__Group__0__Impl
	rule__SValueObjectRef__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__SValueObjectRef__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSValueObjectRefAccess().getSValueObjectRefAction_0()); }
	()
	{ after(grammarAccess.getSValueObjectRefAccess().getSValueObjectRefAction_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SValueObjectRef__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SValueObjectRef__Group__1__Impl
	rule__SValueObjectRef__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__SValueObjectRef__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSValueObjectRefAccess().getNameAssignment_1()); }
	(rule__SValueObjectRef__NameAssignment_1)
	{ after(grammarAccess.getSValueObjectRefAccess().getNameAssignment_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SValueObjectRef__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SValueObjectRef__Group__2__Impl
	rule__SValueObjectRef__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__SValueObjectRef__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSValueObjectRefAccess().getColonKeyword_2()); }
	':'
	{ after(grammarAccess.getSValueObjectRefAccess().getColonKeyword_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__SValueObjectRef__Group__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__SValueObjectRef__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__SValueObjectRef__Group__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getSValueObjectRefAccess().getEClassAssignment_3()); }
	(rule__SValueObjectRef__EClassAssignment_3)
	{ after(grammarAccess.getSValueObjectRefAccess().getEClassAssignment_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__EString__Group_2__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__EString__Group_2__0__Impl
	rule__EString__Group_2__1
;
finally {
	restoreStackSize(stackSize);
}

rule__EString__Group_2__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getEStringAccess().getHyphenMinusKeyword_2_0()); }
	('-')?
	{ after(grammarAccess.getEStringAccess().getHyphenMinusKeyword_2_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__EString__Group_2__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__EString__Group_2__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__EString__Group_2__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getEStringAccess().getINTTerminalRuleCall_2_1()); }
	RULE_INT
	{ after(grammarAccess.getEStringAccess().getINTTerminalRuleCall_2_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__SObject__NameAssignment_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSObjectAccess().getNameEStringParserRuleCall_2_0()); }
		ruleEString
		{ after(grammarAccess.getSObjectAccess().getNameEStringParserRuleCall_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__EClassAssignment_4
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSObjectAccess().getEClassIDTerminalRuleCall_4_0()); }
		RULE_ID
		{ after(grammarAccess.getSObjectAccess().getEClassIDTerminalRuleCall_4_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__SobjectelementAssignment_6_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSObjectAccess().getSobjectelementSObjectElementParserRuleCall_6_1_0()); }
		ruleSObjectElement
		{ after(grammarAccess.getSObjectAccess().getSobjectelementSObjectElementParserRuleCall_6_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SObject__SobjectelementAssignment_7_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSObjectAccess().getSobjectelementSObjectElementParserRuleCall_7_1_0()); }
		ruleSObjectElement
		{ after(grammarAccess.getSObjectAccess().getSobjectelementSObjectElementParserRuleCall_7_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltAttribute__NameAssignment_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSOEltAttributeAccess().getNameIDTerminalRuleCall_1_0()); }
		RULE_ID
		{ after(grammarAccess.getSOEltAttributeAccess().getNameIDTerminalRuleCall_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltAttribute__SavalueAssignment_3_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSOEltAttributeAccess().getSavalueSAValueParserRuleCall_3_1_0()); }
		ruleSAValue
		{ after(grammarAccess.getSOEltAttributeAccess().getSavalueSAValueParserRuleCall_3_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltAttribute__SavalueAssignment_3_2_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSOEltAttributeAccess().getSavalueSAValueParserRuleCall_3_2_1_0()); }
		ruleSAValue
		{ after(grammarAccess.getSOEltAttributeAccess().getSavalueSAValueParserRuleCall_3_2_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltRef__NameAssignment_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSOEltRefAccess().getNameIDTerminalRuleCall_1_0()); }
		RULE_ID
		{ after(grammarAccess.getSOEltRefAccess().getNameIDTerminalRuleCall_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltRef__SvalueobjectAssignment_3_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSOEltRefAccess().getSvalueobjectSValueObjectRefParserRuleCall_3_1_0()); }
		ruleSValueObjectRef
		{ after(grammarAccess.getSOEltRefAccess().getSvalueobjectSValueObjectRefParserRuleCall_3_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltRef__SvalueobjectAssignment_3_2_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSOEltRefAccess().getSvalueobjectSValueObjectRefParserRuleCall_3_2_1_0()); }
		ruleSValueObjectRef
		{ after(grammarAccess.getSOEltRefAccess().getSvalueobjectSValueObjectRefParserRuleCall_3_2_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltContaiment__NameAssignment_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSOEltContaimentAccess().getNameIDTerminalRuleCall_1_0()); }
		RULE_ID
		{ after(grammarAccess.getSOEltContaimentAccess().getNameIDTerminalRuleCall_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltContaiment__SobjectAssignment_3_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSOEltContaimentAccess().getSobjectSObjectParserRuleCall_3_1_0()); }
		ruleSObject
		{ after(grammarAccess.getSOEltContaimentAccess().getSobjectSObjectParserRuleCall_3_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SOEltContaiment__SobjectAssignment_3_2_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSOEltContaimentAccess().getSobjectSObjectParserRuleCall_3_2_1_0()); }
		ruleSObject
		{ after(grammarAccess.getSOEltContaimentAccess().getSobjectSObjectParserRuleCall_3_2_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SAValuePrimitive__ValueAssignment_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSAValuePrimitiveAccess().getValueEStringParserRuleCall_1_0()); }
		ruleEString
		{ after(grammarAccess.getSAValuePrimitiveAccess().getValueEStringParserRuleCall_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SValueObjectRef__NameAssignment_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSValueObjectRefAccess().getNameEStringParserRuleCall_1_0()); }
		ruleEString
		{ after(grammarAccess.getSValueObjectRefAccess().getNameEStringParserRuleCall_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SValueObjectRef__EClassAssignment_3
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSValueObjectRefAccess().getEClassIDTerminalRuleCall_3_0()); }
		RULE_ID
		{ after(grammarAccess.getSValueObjectRefAccess().getEClassIDTerminalRuleCall_3_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_INT : ('0'..'9')+;

RULE_STRING : ('"' ('\\' .|~(('\\'|'"')))* '"'|'\'' ('\\' .|~(('\\'|'\'')))* '\'');

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;
