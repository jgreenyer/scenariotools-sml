/**
 * *******************************************************************************
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 * *******************************************************************************
 */
 package org.scenariotools.sml.ui.actions

import java.io.ByteArrayInputStream
import org.eclipse.core.commands.AbstractHandler
import org.eclipse.core.commands.ExecutionEvent
import org.eclipse.core.commands.ExecutionException
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IProject
import org.eclipse.core.resources.IWorkspace
import org.eclipse.core.resources.IWorkspaceRoot
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.IPath
import org.eclipse.ui.IEditorDescriptor
import org.eclipse.ui.IWorkbenchPage
import org.eclipse.ui.IWorkbenchWindow
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.dialogs.SaveAsDialog
import org.eclipse.ui.handlers.HandlerUtil
import org.eclipse.ui.part.FileEditorInput
import org.scenariotools.sml.ui.actions.converter.SelectionToEcoreConverter
import org.scenariotools.sml.ui.actions.generator.EcoreToSpecificationGenerator

class InitializeSpecificationCommandHandler extends AbstractHandler {

	val static final FILE_EXTENSION_SML = "sml"

	override Object execute(ExecutionEvent event) throws ExecutionException {
		var IWorkbenchWindow window = HandlerUtil::getActiveWorkbenchWindowChecked(event)

		// Load selected ecore resource
		var ecoreFile = SelectionToEcoreConverter.selectedEcoreFile
		val ecore = SelectionToEcoreConverter.selectedEcorePackage

		// Create path to new sml file
		var path = ecoreFile.fullPath
		path = path.removeLastSegments(1)
		path = path.append(ecore.name + "." + FILE_EXTENSION_SML)
		val IProject project = ecoreFile.project
		val IFile file = project.getFile(ecore.name + "." + FILE_EXTENSION_SML);

		// Create Dialog
		val SaveAsDialog d = new SaveAsDialog(window.getShell())
		d.originalFile = file
		d.open()
		var IPath p = d.getResult()

		// Create sml file
		try {
			if(!(FILE_EXTENSION_SML).equalsIgnoreCase(p.getFileExtension())) p = p.addFileExtension(FILE_EXTENSION_SML)
			if (p !== null) {
				val IWorkspace workspace = ResourcesPlugin.getWorkspace();
				val IWorkspaceRoot root = workspace.getRoot();
				val IFile spec = root.getFile(p)
				spec.create(
					new ByteArrayInputStream(
						EcoreToSpecificationGenerator.generateSmlFileFromEPackage(ecore).toString.getBytes()), false,
					null)
					openFile(spec)
				}
			} catch (Exception exception) {
				exception.printStackTrace
			}

			return null
		}

		def static openFile(IFile file) {
			val IWorkbenchPage page = PlatformUI.getWorkbench().activeWorkbenchWindow.activePage
			val IEditorDescriptor desc = PlatformUI.getWorkbench().getEditorRegistry().getDefaultEditor(file.getName());
			page.openEditor(new FileEditorInput(file), desc.getId());
		}

	}
	