/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.sml.ui.actions.generator

import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EPackage
import org.scenariotools.sml.Collaboration
import org.scenariotools.sml.Role
import org.scenariotools.sml.SmlFactory
import org.scenariotools.sml.Specification
import org.scenariotools.sml.expressions.scenarioExpressions.Import
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsFactory

class EcoreToSpecificationGenerator {

	public def static generateSmlFileFromEPackage(EPackage ePackage) '''
		import "«ePackage.eResource.URI.toString»"
		
		system specification «ePackage.name.toFirstUpper + "Specification"» {
		
			/*
			 * Refer to a package in the imported ecore model.
			 * Hint: Use ctrl+alt+R to rename packages and classes.
			 */
			domain «ePackage.name»
			«FOR p : ePackage.eAllContents.toList»
				«IF p instanceof EPackage»
					domain «p.name»
				«ENDIF»
			«ENDFOR»
			
			/* 
			 * Define classes of objects that are controllable
			 * or uncontrollable.
			 */
			«FOR eclass : ePackage.eAllContents.toList»
				«IF eclass instanceof EClass»
					«IF !eclass.isAbstract && !eclass.isInterface»
						define «eclass.name» as uncontrollable
					«ENDIF»
				«ENDIF»
			«ENDFOR»
			
			/*
			 * Collaborations describe how objects interact in a certain
			 * context to collectively accomplish some desired functionality.
			 */
			collaboration «ePackage.name.toFirstUpper + "Collaboration1"» {
		
				«FOR eclass : ePackage.eAllContents.toList»
					«IF eclass instanceof EClass»
						«IF !eclass.isAbstract && !eclass.isInterface»
							dynamic role «eclass.name» «eclass.name.toFirstLower»
						«ENDIF»
					«ENDIF»
				«ENDFOR»
		
			}
		
		}
	'''

	def static Specification createSpecificationFromEcorePackage(EPackage ePackage) {
		val Specification specification = SmlFactory.eINSTANCE.createSpecification

		specification.name = ePackage.name.toFirstUpper + "Specification"

		// Add import of ecore file to Specification
		val Import mainImport = ScenarioExpressionsFactory.eINSTANCE.createImport
		mainImport.importURI = ePackage.eResource.URI.toString
		specification.imports.add(mainImport)

		// Add main package to Specification
		specification.domains.add(ePackage)

		// Add sub packages to Specification
		ePackage.eAllContents.filter[c|c instanceof EPackage].forEach [ p |
			if (p instanceof EPackage) {
				specification.domains.add(p)
			}
		]

		// Add eclasses from ecore file to Specification
//		ePackage.eAllContents.filter[c|c instanceof EClass].forEach [ eclass |
//			if (eclass instanceof EClass) {
//				if (!eclass.isAbstract && !eclass.isInterface) {
//					specification.uncontrollableEClasses.add(eclass)
//				}
//			}
//		]

		val channelOptions = SmlFactory.eINSTANCE.createChannelOptions
		specification.channelOptions = channelOptions

		// Add first collaboration to Specification
		val Collaboration collaboration = SmlFactory.eINSTANCE.createCollaboration
		collaboration.name = ePackage.name.toFirstUpper + "Collaboration1"
		ePackage.eAllContents.filter[c|c instanceof EClass].forEach [ eclass |
			if (eclass instanceof EClass) {
				if (!eclass.isAbstract && !eclass.isInterface) {
					val Role role = SmlFactory.eINSTANCE.createRole
					role.name = eclass.name.toFirstLower
					role.type = eclass
					collaboration.roles.add(role)
				}
			}
		]
		specification.containedCollaborations.add(collaboration)

		return specification
	}

}
