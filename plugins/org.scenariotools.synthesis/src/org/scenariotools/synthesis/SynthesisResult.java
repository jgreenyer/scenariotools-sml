package org.scenariotools.synthesis;

import java.util.ArrayList;
import java.util.List;

import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.synthesis.ResultParser.Group;
import org.scenariotools.synthesis.ResultParser.NamedValue;
import org.scenariotools.synthesis.gr1.util.SynthesisUtil;

public class SynthesisResult {
	public static final String STATEGRAPH_GROUP_NAME = "State Graph Statistics";
	public static final String STRATEGY_GROUP_NAME = "Strategy Statistics";
	public static final String FLOAT_OUTPUT_FORMAT = "%.1f";
	
	private ResultParser stateGraphStats;
	private ResultParser strategyStats;
	private ResultParser resultObjectStats;
	public List<Group> finalResult;
	public List<List<Group>> rawData = new ArrayList<List<Group>>();
	
	public static String buildMessageForSingleRun(AbstractSynthesisAlgorithm synthAlgo, Object resultObject) throws Exception {
		List<Group> result = new ArrayList<Group>();
		
		if(synthAlgo.stateGraph != null) {
			GraphStats graphStats = new GraphStats(synthAlgo.stateGraph);
			NamedValue nv = new NamedValue();
			nv.name = "system-deterministic";
			nv.value = graphStats.systemDeterministic;
			
			List<Group> temp = ResultParser.parse(graphStats);
			temp.get(0).name = STATEGRAPH_GROUP_NAME;
			temp.get(0).values.add(nv);
			
			result.addAll(temp);
		}
		
		if(synthAlgo.strategy != null && synthAlgo.strategy instanceof SMLRuntimeStateGraph) {
			List<Group> temp = ResultParser.parse(new GraphStats((SMLRuntimeStateGraph)synthAlgo.strategy));
			temp.get(0).name = STRATEGY_GROUP_NAME;
			result.addAll(temp);
		}
		
		ResultParser.mergeInto(result, ResultParser.parse(resultObject));
		
		return buildMessage(result, synthAlgo.strategyExists);
	}
	
	public void addResultFromSingleRun(AbstractSynthesisAlgorithm synthAlgo, Object resultObject) throws Exception {
		if(synthAlgo.stateGraph != null) {
			GraphStats stats = new GraphStats(synthAlgo.stateGraph);
			if(stateGraphStats != null) {
				stateGraphStats.addData(stats);
			} else {
				stateGraphStats = new ResultParser(stats);
			}
		}
		
		if(synthAlgo.strategy != null && synthAlgo.strategy instanceof SMLRuntimeStateGraph) {
			GraphStats stats = new GraphStats((SMLRuntimeStateGraph)synthAlgo.strategy);
			if(strategyStats != null) {
				strategyStats.addData(stats);
			} else {
				strategyStats = new ResultParser(stats);
			}			
		}
		
		if(resultObjectStats != null) {
			resultObjectStats.addData(resultObject);
		} else {
			resultObjectStats = new ResultParser(resultObject);
		}
	}
	
	public String buildMessageForMultipleRuns(int numWarmUpIterations, boolean strategyExists) throws Exception {
		assert stateGraphStats == null || stateGraphStats.size() == resultObjectStats.size();
		assert strategyStats == null || strategyStats.size() == resultObjectStats.size();
		
		List<Group> result = new ArrayList<Group>();
		
		if(stateGraphStats != null) {
			// implies stateGraphStats.size() > 0
			stateGraphStats.finalize();
			stateGraphStats.finalResult.get(0).name = STATEGRAPH_GROUP_NAME;
			result.addAll(stateGraphStats.finalResult);
			for(List<Group> groups : stateGraphStats.rawData) {
				groups.get(0).name = STATEGRAPH_GROUP_NAME;
				rawData.add(groups);
			}
		}
		
		if(strategyStats != null) {
			strategyStats.finalize();
			strategyStats.finalResult.get(0).name = STRATEGY_GROUP_NAME;
			result.addAll(strategyStats.finalResult);
			for(int i = 0; i < rawData.size(); i++) { // rawData.size() == stateGraphStats.size() [== strategyStats.size()]
				List<Group> groups = strategyStats.rawData.get(i);
				groups.get(0).name = STRATEGY_GROUP_NAME;
				if(i < rawData.size())
					rawData.get(i).addAll(groups);
				else
					rawData.add(groups);
			}
		}
		
		resultObjectStats.finalize();
		ResultParser.mergeInto(result, resultObjectStats.finalResult);
		finalResult = result;
		if(rawData.isEmpty()) {
			rawData.addAll(resultObjectStats.rawData);
		} else {
			for(int i = 0; i < rawData.size(); i++) // rawData.size() == resultObjectStats.size() [== stateGraphStats.size()] [== strategyStats.size()]
				ResultParser.mergeInto(rawData.get(i), resultObjectStats.rawData.get(i));
		}
		
		return "number of iterations: " + resultObjectStats.size() + "\nnumber of warm-up iterations: " + numWarmUpIterations + "\n" + buildMessage(result, strategyExists);
	}
	
	private static String buildMessage(List<Group> groups, boolean strategyExists)  {
		StringBuffer message = new StringBuffer();
		message.append("Strategy exists: " + String.valueOf(strategyExists).toUpperCase() + "\n\n");
		
		for(Group group : groups) {
			message.append("[" + group.name + "]\n");
			for(NamedValue nv : group.values) {
				message.append(nv.name + ": ");
				if(nv.value.getClass() == Boolean.class)
					message.append(nv.value.toString().toUpperCase());
				else if(nv.value.getClass() == Float.class || nv.value.getClass() == Double.class)
					message.append(String.format(FLOAT_OUTPUT_FORMAT, nv.value));
				else
					message.append(nv.value);
				message.append("\n");
			}
			message.append("\n");
		}

		return message.toString();				
	}
	
	private static class GraphStats {
		@ResultMetaData(name="number of states", group="", position=1)
		public int numStates = 0;
		
		@ResultMetaData(name="number of transitions", group="", position=2)
		public int numTransitions = 0;

		private boolean systemDeterministic = true;
		
		private GraphStats(SMLRuntimeStateGraph graph) {
			for (SMLRuntimeState state : graph.getStates()) {
				numStates += 1;
				numTransitions += state.getOutgoingTransition().size();
				if(state.getOutgoingTransition().size() > 1 && SynthesisUtil.isControllable((SMLRuntimeState)state)) {
					systemDeterministic = false;
				}
			}
		}
	}
}
