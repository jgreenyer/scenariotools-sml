/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Mohammed Abed
 */
package org.scenariotools.synthesis;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.Writer;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.synthesis.Activator;
import org.scenariotools.synthesis.ResultParser.Group;
import org.scenariotools.synthesis.ResultParser.NamedValue;
import org.scenariotools.synthesis.ResultParser.Statistics;
import org.scenariotools.synthesis.benchmark.ui.preferencepage.PreferenceConstantsForBenchmark;

public class SynthesisJob extends Job {
	private static Logger logger = Activator.getLogManager().getLogger(SynthesisJob.class.getName());

	private Configuration configuration;
	@SuppressWarnings("rawtypes")
	private Class synthesisAlgorithmClass;
	private boolean benchmarkMode;
	
	private boolean saveResult;
	
	@SuppressWarnings("rawtypes")
	public SynthesisJob(String name, Configuration configuration, Class synthesisAlgorithmClass, boolean benchmarkMode) {
		super(name);
		this.configuration = configuration;
		this.synthesisAlgorithmClass = synthesisAlgorithmClass;
		this.benchmarkMode = benchmarkMode;
	}
	
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		try {
			if(!benchmarkMode)
				runSynthesis(monitor);
			else
				runBenchmark(monitor);
		} catch(Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		
		return Status.OK_STATUS;
	}

	private void runSynthesis(IProgressMonitor monitor) throws Exception {
		AbstractSynthesisAlgorithm synthesisAlgorithm = (AbstractSynthesisAlgorithm)synthesisAlgorithmClass.newInstance();
		Object result = synthesisAlgorithm.synthesize(configuration, monitor);

		String message = SynthesisResult.buildMessageForSingleRun(synthesisAlgorithm, result);
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				saveResult = MessageDialog.openQuestion(new Shell(), "Synthesis Result", message + "Save synthesis result?");
			}
		});
		
		if(saveResult) {
			final ResourceSet resourceSet = new ResourceSetImpl();			
		    resourceSet.getURIConverter().getURIMap().putAll(EcorePlugin.computePlatformURIMap(true));

			Map<String, Boolean> options = new HashMap<String, Boolean>();
			options.put(XMLResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE); 
			
			if(synthesisAlgorithm.stateGraph != null) {
				Resource res = resourceSet.createResource(configuration.eResource().getURI().trimFileExtension().appendFileExtension("stategraph"));
				res.getContents().add(synthesisAlgorithm.stateGraph);
				res.save(options);				
			}
			
			if(synthesisAlgorithm.strategy != null) {
				Resource res = resourceSet.createResource(configuration.eResource().getURI().trimFileExtension().appendFileExtension("strategy"));
				res.getContents().add(synthesisAlgorithm.strategy);
				res.save(options);
			}

		}
	}

	private void runBenchmark(IProgressMonitor monitor) throws Exception {
		int numIterations = Integer.valueOf(Activator.getDefault().getPreferenceStore().getString(PreferenceConstantsForBenchmark.NUMBER_OF_ITERATIONS));
		int numWarmUpIterations = Integer.valueOf(Activator.getDefault().getPreferenceStore().getString(PreferenceConstantsForBenchmark.NUMBER_OF_WARMUP_ITERATIONS));
		boolean wonLastTime = false;
		
		SynthesisResult result = new SynthesisResult();
		
		for(int iteration = 0; iteration < (numWarmUpIterations + numIterations); iteration++) {
			if(monitor.isCanceled())
				return;
			
			AbstractSynthesisAlgorithm synthesisAlgorithm = (AbstractSynthesisAlgorithm)synthesisAlgorithmClass.newInstance();
			Object resultObject = synthesisAlgorithm.synthesize(configuration, monitor);
			if(iteration >= numWarmUpIterations) {
				result.addResultFromSingleRun(synthesisAlgorithm, resultObject);
			}
			
			if(iteration == 0) {
				wonLastTime = synthesisAlgorithm.strategyExists;				
			} else {
				if(wonLastTime != synthesisAlgorithm.strategyExists) {
					logger.error("synthesis produced inconsistent results");
					return;
				}
			}
		}
		
		String message = result.buildMessageForMultipleRuns(numWarmUpIterations, wonLastTime);
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				saveResult = MessageDialog.openQuestion(new Shell(), "Benchmark Result", message + "Data format: average (minimum/median/maximum)\nSave benchmark result?");
			}
		});
		
		if(saveResult) {
			URI fileURI = configuration.eResource().getURI().trimFileExtension().appendFileExtension("csv");			
			URL fileURL = FileLocator.resolve(new URL(fileURI.toString()));
			
			Writer file = new BufferedWriter(new FileWriter(fileURL.getFile()));
			
			file.write(String.format("\"number of iterations\",\"%d\"\n", numIterations));
			file.write(String.format("\"number of warm-up iterations\",\"%d\"\n\n", numWarmUpIterations));
			file.write("\"\",\"average\",\"minimum\",\"median\",\"maximum\"\n");
			for(Group group : result.finalResult) {
				file.write(String.format("\"%s\"\n", group.name));
				for(NamedValue nv : group.values) {
					file.write(String.format("\"%s\",%s\n", nv.name, ((Statistics)nv.value).toCSVString()));
				}
			}
			file.write("\n");
			
			for(int i = 0; i < numIterations; ++i) { // numIterations == result.rawData.size()
				file.write(String.format("\"iteration index\",\"%d\"\n", i));
				for(Group group : result.rawData.get(i)) {
					file.write(String.format("\"%s\"\n", group.name));
					for(NamedValue nv : group.values) {
						file.write(String.format("\"%s\",%s\n", nv.name, nv.value));
					}
				}
				file.write("\n");
			}
			
			file.close();
		}
	}	
}
