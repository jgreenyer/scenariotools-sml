package org.scenariotools.synthesis;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.configuration.Configuration;

public abstract class AbstractSynthesisAlgorithm {
	public SMLRuntimeStateGraph stateGraph = null;
	public boolean strategyExists = false;
	public EObject strategy = null;
	
	public abstract Object synthesize(Configuration configuration, IProgressMonitor monitor);
}
