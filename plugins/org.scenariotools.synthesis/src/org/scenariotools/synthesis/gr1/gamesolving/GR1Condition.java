/**
 * Copyright (c) 2018 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     
 * Author:
 * 		Daniel Gritzner
 */
package org.scenariotools.synthesis.gr1.gamesolving;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.ParameterValue;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.Transition;
import org.scenariotools.sml.runtime.util.SMLRuntimeStateUtil;
import org.scenariotools.synthesis.gr1.util.ScenarioAndRoleBindingsKey;
import org.scenariotools.synthesis.gr1.util.SynthesisUtil;

public class GR1Condition {
	private SMLRuntimeStateGraph stateGraph;
	private Set<SMLRuntimeState> visited = new HashSet<SMLRuntimeState>();
	private int stateIndex = 1;
	
	private Map<SMLRuntimeState, Set<Event>> unexploredEnvEvents;
	private Map<SMLRuntimeState, Set<Event>> unexploredSysEvents;
	
	public static final boolean USE_PRIORITY_QUEUE = true;
	public static final boolean TERMINATE_GAME_SOLVING_EARLY = false; // only affects incremental synthesis
	private static final double INCREMENTAL_GROWTH_FACTOR = 1.5; // only affects incremental synthesis
	private static final boolean MARK_NEWLY_EXPLORED_STATES_AS_CANDIDATES = false; // only affects incremental synthesis
	private static final boolean EXPLORE_OPPONENT_EVENTS = false; // only affects incremental synthesis
	private static final boolean PERFORM_MULTIPLE_EXPLORATION_ATTEMPTS = false; // only affects incremental synthesis
		
	// collects states which assumptions must avoid/leave eventually
	private Map<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> requestedActiveAssumptionScenarios = new HashMap<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>>();
	private Set<SMLRuntimeState> assumptionSafetyViolationStates = new HashSet<SMLRuntimeState>();
	
	// collects states which guarantees must avoid/leave eventually
	private Map<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> requestedActiveGuaranteeScenarios = new HashMap<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>>();
	private Set<SMLRuntimeState> guaranteeSafetyViolationSates = new HashSet<SMLRuntimeState>();
	private Set<SMLRuntimeState> systemControlledStates = new HashSet<SMLRuntimeState>();
	
	private GenBuechiCondition[] conditions = new GenBuechiCondition[2];
	
	public GR1Condition(SMLRuntimeStateGraph stateGraph, IProgressMonitor monitor) {
		this(stateGraph, false, monitor);
	}
	
	public GR1Condition(SMLRuntimeStateGraph stateGraph, boolean incremental, IProgressMonitor monitor) {
		this.stateGraph = stateGraph;
		
		if(incremental) {
			initialExploration(monitor);
		} else {
			fullExploration(monitor);
		}

		FilteredStateGraph filteredStateGraph = new FilteredStateGraph();
		conditions[ConditionType.ASSUMPTION.value] = new GenBuechiCondition(filteredStateGraph, visited, requestedActiveAssumptionScenarios, assumptionSafetyViolationStates);
		conditions[ConditionType.GUARANTEE.value] = new GenBuechiCondition(filteredStateGraph, visited, requestedActiveGuaranteeScenarios, guaranteeSafetyViolationSates, systemControlledStates);
	}
	
	public GenBuechiCondition getCondition(ConditionType conditionType) {
		return conditions[conditionType.value];
	}
	
	private void fullExploration(IProgressMonitor monitor) {
		SMLRuntimeState startState = stateGraph.getStartState();
		visited.add(startState);

		Stack<SMLRuntimeState> stack = new Stack<SMLRuntimeState>();
		stack.push(startState);
		
		while(!stack.empty() && !monitor.isCanceled()) {
			SMLRuntimeState state = stack.pop();
			setIndex(state);

			stateGraph.generateAllSuccessors(state);
			collectNonGoalStateInformation(state);
			
			for(Transition t : state.getOutgoingTransition()) {
				SMLRuntimeState nextState = t.getTargetState();
				if(!visited.contains(nextState)) {
					visited.add(nextState);
					stack.push(nextState);
				}
			}
		}
	}
	
	private void setIndex(SMLRuntimeState state) {
		SMLRuntimeStateUtil.setPassedIndex(state, "" + stateIndex);
		stateIndex += 1;		
	}
	
	private void collectNonGoalStateInformation(SMLRuntimeState state) {
		for(ActiveScenario activeScenario : state.getActiveScenarios()) {
			if(!activeScenario.isInRequestedState()) {
				continue;				
			}
			
			Scenario scenario = activeScenario.getScenario();
			
			Map<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> theMap = null;
			switch(scenario.getKind()) {
			case ASSUMPTION:
				theMap = requestedActiveAssumptionScenarios;
				break;
			case GUARANTEE:
				theMap = requestedActiveGuaranteeScenarios;
				break;
			default:
				continue;
			}
			
			ScenarioAndRoleBindingsKey key = new ScenarioAndRoleBindingsKey(scenario, activeScenario.getRoleBindings());
			Set<SMLRuntimeState> requestedStates = theMap.get(key);
			
			if(requestedStates == null) {
				requestedStates = new HashSet<SMLRuntimeState>();
				theMap.put(key, requestedStates);
			}
			
			requestedStates.add(state);
		}
		
		if(state.isSafetyViolationOccurredInAssumptions()) {
			assumptionSafetyViolationStates.add(state);			
		}
		if(state.isSafetyViolationOccurredInGuarantees()) {
			guaranteeSafetyViolationSates.add(state);			
		}
		if(state.getOutgoingTransition().size() == 0 || SynthesisUtil.isControllable(state.getOutgoingTransition().get(0))) { // add dead lock states to make them non-goal states
			systemControlledStates.add(state);
		}
	}
	
	public GR1Condition reduce(Set<SMLRuntimeState> statesToRemove) {
		return reduce(statesToRemove, false);
	}
	
	public GR1Condition reduce(Set<SMLRuntimeState> statesToRemove, boolean createCopy) {
		GR1Condition result = (createCopy ? new GR1Condition() : this); // stateGraph, maps, and sets are undefined/empty in copy!
		
		result.conditions[0] = conditions[0].reduce(statesToRemove, createCopy);
		result.conditions[1] = conditions[1].reduce(result.conditions[0].stateGraph, createCopy);
		
		return result;
	}
	
	private GR1Condition() {}
	
	private void initialExploration(IProgressMonitor monitor) {
		SMLRuntimeState startState = stateGraph.getStartState();
		setIndex(startState);		
		visited.add(startState);
		
		unexploredEnvEvents = new HashMap<SMLRuntimeState, Set<Event>>();
		unexploredSysEvents = new HashMap<SMLRuntimeState, Set<Event>>();
		
		for(Transition t : stateGraph.generateAllSuccessors(startState)) {
			if(monitor.isCanceled()) {
				return;
			}
			
			exploreCycle(t, visited);
		}

		collectNonGoalStateInformation(startState);
	}
	
	private Transition exploreCycle(Transition t, Set<SMLRuntimeState> newStates) {
		return exploreCycle(t, newStates, null);
	}
	
	@SuppressWarnings("unused")
	private Transition exploreCycle(Transition t, Set<SMLRuntimeState> newStates, Set<SMLRuntimeState> candidates) {
		while(true) {
			SMLRuntimeState state = t.getTargetState();
			
			if(state.getEventToTransitionMap().size() > 0) {
				return t; // found a state that has been visited before
			} else if(state.getEnabledEvents().isEmpty()) {
				// found a deadlock state
				if(SMLRuntimeStateUtil.getPassedIndex(state) == null) {
					// discovered this deadlock state for the first time
					setIndex(state);
					newStates.add(state);
					collectNonGoalStateInformation(state);
				}
				
				return null; 
			}
			
			setIndex(state);
			newStates.add(state);
			if(MARK_NEWLY_EXPLORED_STATES_AS_CANDIDATES && candidates != null) {
				candidates.add(state);
			}
			
			Event event = selectEvent(state, state.getEnabledEvents()).getKey();
			Set<Event> unexploredEvents = new HashSet<Event>(state.getEnabledEvents());
			unexploredEvents.remove(event);			
			t = stateGraph.generateSuccessor(state, event);
			
			collectNonGoalStateInformation(state);

			if(!unexploredEvents.isEmpty()) {
				Map<SMLRuntimeState, Set<Event>> theMap = unexploredEnvEvents;
				if(SynthesisUtil.isControllable(state)) {
					theMap = unexploredSysEvents;
				}
				
				theMap.put(state, unexploredEvents);
			}
		}
	}
	
	private static Map.Entry<Event, Integer> selectEvent(SMLRuntimeState state, Iterable<Event> candidates) {
		if(SynthesisUtil.isControllable(state)) {
			Map<EObject, Integer> objectBindingCounts = new HashMap<EObject, Integer>();
			for(ActiveScenario scenario : state.getActiveScenarios()) {
				if(scenario.isInRequestedState()) {
					for(Entry<Role, EObject> roleBinding : scenario.getRoleBindings().getRoleBindings().entrySet()) {
						EObject key = roleBinding.getValue();
						Integer count = objectBindingCounts.get(key);
						if(count == null) {
							count = 1;
						} else {
							count += 1;
						}
						objectBindingCounts.put(key, count);
					}
				}
			}
			
			int bestScore = Integer.MIN_VALUE;
			Event bestEvent = null;
			for(Event candidate : candidates) {
				if(candidate instanceof MessageEvent) {
					MessageEvent event = (MessageEvent)candidate;
					int score = getScore(event.getSendingObject(), objectBindingCounts);
					score += getScore(event.getSendingObject(), objectBindingCounts);
					for(ParameterValue parameterValue : event.getParameterValues()) {
						score += getScore(parameterValue.getValue(), objectBindingCounts);
					}
					
					if(score > bestScore) {
						bestScore = score;
						bestEvent = candidate;
					}
				}
			}
			
			assert bestEvent != null;
			return new AbstractMap.SimpleImmutableEntry<Event, Integer>(bestEvent, bestScore);
		} else {
			return new AbstractMap.SimpleImmutableEntry<Event, Integer>(candidates.iterator().next(), 0); // return random environment event
		}
	}
	
	private static int getScore(Object object, Map<EObject, Integer> objectBindingCounts) {
		if(object == null || !(object instanceof EObject)) {
			return 0;
		}
		
		Integer score = objectBindingCounts.get((EObject)object);
		if(score == null) {
			return 0;
		} else {
			return score;
		}
	}
	
	@SuppressWarnings("unused")
	public boolean expand(Set<SMLRuntimeState> winningStates, boolean systemWins, IProgressMonitor monitor) {
		Map<SMLRuntimeState, Set<Event>> firstMap = (systemWins ? unexploredEnvEvents : unexploredSysEvents);
		if(firstMap.isEmpty()) {
			return false;
		}
		
		Set<SMLRuntimeState> candidates = (systemWins ? new HashSet<SMLRuntimeState>(winningStates) : new HashSet<SMLRuntimeState>(stateGraph.getStates()));
		if(!systemWins) {
			candidates.removeAll(winningStates);
		}
		Map<SMLRuntimeState, Set<Event>> secondMap = (systemWins ? unexploredSysEvents : unexploredEnvEvents);
		ExplorationIncrement increment = new ExplorationIncrement();
		int oldSize = stateGraph.getStates().size();		
		
		boolean performedExploration = false;
		
		do {
			performedExploration = expand(candidates, firstMap, oldSize, increment, !systemWins, monitor) || performedExploration;
			if(EXPLORE_OPPONENT_EVENTS) {
				performedExploration = expand(candidates, secondMap, oldSize, increment, systemWins, monitor) || performedExploration;	
			}
		} while(PERFORM_MULTIPLE_EXPLORATION_ATTEMPTS && !candidates.isEmpty() && !firstMap.isEmpty() && ((double)stateGraph.getStates().size())/((double)oldSize) < INCREMENTAL_GROWTH_FACTOR && !monitor.isCanceled());
		
		if(!performedExploration) {
			return false; // candidates does not intersect firstMap.keySet() => outcome of GR(1) game will not change
		}
		
		visited.addAll(increment.newStates);
		conditions[ConditionType.ASSUMPTION.value].stateGraph.reduce(Collections.emptySet()); // reset cache
		conditions[ConditionType.ASSUMPTION.value].expand(visited, increment, requestedActiveAssumptionScenarios, assumptionSafetyViolationStates, monitor);
		conditions[ConditionType.GUARANTEE.value].expand(visited, increment, requestedActiveGuaranteeScenarios, guaranteeSafetyViolationSates, systemControlledStates, monitor);
		
		return true;
	}
	
	private boolean expand(Set<SMLRuntimeState> candidates, Map<SMLRuntimeState, Set<Event>> theMap, int oldSize, ExplorationIncrement increment, boolean isSystemStep, IProgressMonitor monitor) {
		boolean performedExploration = false;

		while(!candidates.isEmpty() && !theMap.isEmpty() && ((double)stateGraph.getStates().size())/((double)oldSize) < INCREMENTAL_GROWTH_FACTOR && !monitor.isCanceled()) {
			Map.Entry<SMLRuntimeState, Event> stateAndEvent = selectStateAndEvent(candidates, theMap, isSystemStep);
			SMLRuntimeState state = stateAndEvent.getKey();
			if(state == null) {
				break;
			}
			Event event = stateAndEvent.getValue();
			
			Transition t = stateGraph.generateSuccessor(state, event);
			increment.newBoundaryOut.add(t);
			Transition t2 = exploreCycle(t, increment.newStates, candidates);
			if(t2 != null) {
				increment.newBoundaryIn.add(t2);
				if(t2 == t) {
					increment.newBoundaryIntersection.add(t);
				}
			}

			performedExploration = true;
		}
		
		return performedExploration;
	}
	
	private static Map.Entry<SMLRuntimeState, Event> selectStateAndEvent(Set<SMLRuntimeState> candidates, Map<SMLRuntimeState, Set<Event>> theMap, boolean isSystemStep) {
		SMLRuntimeState state = null;
		Event event = null;
		
		if(isSystemStep) {
			int bestScore = Integer.MIN_VALUE;
			
			for(Iterator<SMLRuntimeState> iter = candidates.iterator(); iter.hasNext();) {
				SMLRuntimeState candidate = iter.next();
				
				Set<Event> unexploredEvents = theMap.get(candidate);
				if(unexploredEvents == null) {
					iter.remove();
					continue;
				}
				
				Map.Entry<Event, Integer> bestEvent = selectEvent(candidate, unexploredEvents);
				int score = bestEvent.getValue();
				if(score > bestScore) {
					bestScore = score;
					state = candidate;
					event = bestEvent.getKey();
				}
			}
			
			if(state != null) {
				Set<Event> unexploredEvents = theMap.get(state);
				unexploredEvents.remove(event);
				if(unexploredEvents.isEmpty()) {
					theMap.remove(state);
					candidates.remove(state);
				}
			}
		} else {
			Set<Event> unexploredEvents = null;

			while(unexploredEvents == null && !candidates.isEmpty()) {
				state = candidates.iterator().next();
				unexploredEvents = theMap.get(state);
				if(unexploredEvents == null) {
					candidates.remove(state);
					state = null;
				}
			}

			if(state != null) { // implies unexploredEvents != null
				event = unexploredEvents.iterator().next(); // randomly pick unexplored event // TODO: heuristic-based selection
				unexploredEvents.remove(event);
				if(unexploredEvents.isEmpty()) {
					theMap.remove(state);
					candidates.remove(state);
				}				
			}
		}
		
		return new AbstractMap.SimpleImmutableEntry<SMLRuntimeState, Event>(state, event);
	}
	
	public enum Player {
		SYSTEM(0), ENVIRONMENT(1);
		
		public int value;
		private Player(int value) { this.value = value; }
	}
	
	public enum ConditionType {
		ASSUMPTION(0), GUARANTEE(1);
		
		public int value;
		private ConditionType(int value) { this.value = value; }
	}
	
	public static class ExplorationIncrement {
		public Set<SMLRuntimeState> newStates = new HashSet<SMLRuntimeState>(); // newly explored states in the current iteration
		public Set<Transition> newBoundaryOut = new HashSet<Transition>(); // newly explored transitions which lead out of the states that have already been explored in a previous transition
		public Set<Transition> newBoundaryIn = new HashSet<Transition>(); // newly explored transitions which lead into the states that have already been explored in a previous transition
		public Set<Transition> newBoundaryIntersection = new HashSet<Transition>(); // intersection of the other two sets
	}
}
