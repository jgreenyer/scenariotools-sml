/**
 * Copyright (c) 2018 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     
 * Author:
 * 		Daniel Gritzner
 */
package org.scenariotools.synthesis.gr1.gamesolving;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.synthesis.gr1.gamesolving.GR1Condition.ExplorationIncrement;
import org.scenariotools.synthesis.gr1.gamesolving.GR1Condition.Player;
import org.scenariotools.synthesis.gr1.util.ScenarioAndRoleBindingsKey;

public class GenBuechiCondition implements Iterable<BuechiCondition> {
	public Map<ScenarioAndRoleBindingsKey, BuechiCondition> requestedActiveScenarios = new HashMap<ScenarioAndRoleBindingsKey, BuechiCondition>();
	public BuechiCondition firstExtraCondition;
	public BuechiCondition secondExtraCondition = null;	
	
	public FilteredStateGraph stateGraph;
	
	public GenBuechiCondition(FilteredStateGraph stateGraph, Set<SMLRuntimeState> initialStates, Map<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> requestedActiveScenarios, Set<SMLRuntimeState> extraCondition) {
		this(stateGraph, initialStates, requestedActiveScenarios, extraCondition, null);
	}

	public GenBuechiCondition(FilteredStateGraph stateGraph, Set<SMLRuntimeState> initialStates, Map<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> requestedActiveScenarios, Set<SMLRuntimeState> firstExtraCondition, Set<SMLRuntimeState> secondExtraCondition) {
		this.stateGraph = stateGraph;		
		Player player = (secondExtraCondition == null ? Player.ENVIRONMENT : Player.SYSTEM);
		
		for(Entry<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> entry : requestedActiveScenarios.entrySet()) {
			this.requestedActiveScenarios.put(entry.getKey(),  new BuechiCondition(initialStates, entry.getValue(), stateGraph, player, true));
		}
		this.firstExtraCondition = new BuechiCondition(initialStates, firstExtraCondition, stateGraph, player, true); 
		if(secondExtraCondition != null) {
			this.secondExtraCondition =  new BuechiCondition(initialStates, secondExtraCondition, stateGraph, player, true);
		}
	}
	
	public GenBuechiCondition reduce(Set<SMLRuntimeState> statesToRemove) {
		return reduce(statesToRemove, false);
	}

	public GenBuechiCondition reduce(Set<SMLRuntimeState> statesToRemove, boolean createCopy) {
		FilteredStateGraph stateGraph = this.stateGraph;
		if(createCopy) {
			stateGraph = new FilteredStateGraph(this.stateGraph);
		} 
		
		stateGraph.reduce(statesToRemove);
		return reduce(stateGraph, createCopy);
	}
	
	public GenBuechiCondition reduce(FilteredStateGraph stateGraph) {
		return reduce(stateGraph, false);
	}
	
	public GenBuechiCondition reduce(FilteredStateGraph stateGraph, boolean createCopy) {
		if(createCopy) {
			GenBuechiCondition result = new GenBuechiCondition(stateGraph);
			
			for(Entry<ScenarioAndRoleBindingsKey, BuechiCondition> entry : requestedActiveScenarios.entrySet()) {
				result.requestedActiveScenarios.put(entry.getKey(), entry.getValue().reduce(result.stateGraph, true));
			}
			result.firstExtraCondition = firstExtraCondition.reduce(result.stateGraph, true);
			if(secondExtraCondition != null) {
				result.secondExtraCondition = secondExtraCondition.reduce(result.stateGraph, true);
			}
			
			return result;
		} else {
			this.stateGraph = stateGraph;
			
			for(BuechiCondition condition : requestedActiveScenarios.values()) {
				condition = condition.reduce(stateGraph);
			}
			firstExtraCondition = firstExtraCondition.reduce(stateGraph);
			if(secondExtraCondition != null) {
				secondExtraCondition = secondExtraCondition.reduce(stateGraph);
			}
			
			return this;
		}
	}

	private GenBuechiCondition(FilteredStateGraph stateGraph) {
		this.stateGraph = stateGraph;
	}
	
	public void expand(Set<SMLRuntimeState> allStates, ExplorationIncrement increment, Map<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> requestedActiveScenarios, Set<SMLRuntimeState> extraCondition, IProgressMonitor monitor) {
		expand(allStates, increment, requestedActiveScenarios, extraCondition, null, monitor);
	}

	public void expand(Set<SMLRuntimeState> allStates, ExplorationIncrement increment, Map<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> requestedActiveScenarios, Set<SMLRuntimeState> firstExtraCondition, Set<SMLRuntimeState> secondExtraCondition, IProgressMonitor monitor) {
		Player player = (secondExtraCondition == null ? Player.ENVIRONMENT : Player.SYSTEM);

		for(Entry<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> entry : requestedActiveScenarios.entrySet()) {
			if(monitor.isCanceled()) {
				return;
			}
			ScenarioAndRoleBindingsKey key = entry.getKey();
			
			BuechiCondition bc = this.requestedActiveScenarios.get(key);
			if(bc == null) {
				this.requestedActiveScenarios.put(key, new BuechiCondition(allStates, entry.getValue(), stateGraph, player, true));
			} else {
				bc.expand(increment, entry.getValue());
			}
		}
		
		this.firstExtraCondition.expand(increment, firstExtraCondition);
		if(secondExtraCondition != null) {
			this.secondExtraCondition.expand(increment, secondExtraCondition);
		}
	}

	@Override
	public Iterator<BuechiCondition> iterator() {
		if(GR1Condition.USE_PRIORITY_QUEUE) {
			return new PriorityQueueBuechiConditionIterator();
		} else {			
			return new BuechiConditionIterator();
		}
	}
	
	private class PriorityQueueBuechiConditionIterator implements Iterator<BuechiCondition> {
		private PriorityQueue<BuechiCondition> queue;
		
		private PriorityQueueBuechiConditionIterator() {
			queue = new PriorityQueue<BuechiCondition>(requestedActiveScenarios.size() + 2, new Comparator<BuechiCondition>() {
				@Override
				public int compare(BuechiCondition bc1, BuechiCondition bc2) {
					return bc1.attractorStates.size() - bc2.attractorStates.size();
				}
			});
			
			Iterator<BuechiCondition> iterator = new BuechiConditionIterator();
			while(iterator.hasNext()) {
				queue.offer(iterator.next());
			}
		}

		@Override
		public boolean hasNext() {
			return !queue.isEmpty();
		}

		@Override
		public BuechiCondition next() {
			return queue.poll();
		}
		
	}

	private class BuechiConditionIterator implements Iterator<BuechiCondition> {
		private int state = 0;
		private Iterator<BuechiCondition> mapIterator = requestedActiveScenarios.values().iterator();

		@Override
		public boolean hasNext() {
			return state < 2;
		}

		@Override
		public BuechiCondition next() {
			if(mapIterator.hasNext()) {
				return mapIterator.next();
			}
			
			switch(state) {
			case 0:
				state += (secondExtraCondition == null ? 2 : 1);
				return firstExtraCondition;
			case 1:
				state += 1;
				return secondExtraCondition;
			default:				
				return null;
			}
		}
	}	
}
