/**
 * Copyright (c) 2018 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     
 * Author:
 * 		Daniel Gritzner
 */
package org.scenariotools.synthesis.gr1.gamesolving;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.synthesis.gr1.gamesolving.GR1Condition.ConditionType;
import org.scenariotools.synthesis.gr1.gamesolving.GR1Condition.Player;

public class GameSolver {
	private static void solve(Set<SMLRuntimeState> states, GenBuechiCondition winningCondition, Player opponent, IProgressMonitor monitor) {
		gameSolvingLoop:
		while(!monitor.isCanceled()) {
			for(BuechiCondition condition : winningCondition) {
				if(condition.attractorStates.size() < states.size()) {
					BuechiCondition opponentDominion =  new BuechiCondition(states, condition.attractorStates, winningCondition.stateGraph, opponent, true);
					if(opponentDominion.attractorStates.isEmpty()) {
						return;
					}
					states.removeAll(opponentDominion.attractorStates);
					winningCondition = winningCondition.reduce(opponentDominion.attractorStates);
					continue gameSolvingLoop;
				}
			}
			
			return;
		}
	}

	public static void solve(Set<SMLRuntimeState> states, GR1Condition winningCondition, IProgressMonitor monitor) {
		solve(states, winningCondition, null, monitor);
	}
	
	public static void solve(Set<SMLRuntimeState> states, GR1Condition winningCondition, SMLRuntimeState startState, IProgressMonitor monitor) {
		gameSolvingLoop:
		while(!monitor.isCanceled()) {
			GenBuechiCondition guarantees = winningCondition.getCondition(ConditionType.GUARANTEE);
			for(BuechiCondition guarantee : guarantees) {
				if(guarantee.attractorStates.size() < states.size()) {
					Set<SMLRuntimeState> nonAttractorStates = new HashSet<SMLRuntimeState>(states);
					nonAttractorStates.removeAll(guarantee.attractorStates);
					solve(nonAttractorStates, winningCondition.getCondition(ConditionType.ASSUMPTION).reduce(guarantee.attractorStates, true), Player.SYSTEM, monitor);
					if(!nonAttractorStates.isEmpty()) {
						BuechiCondition opponentDominion = new BuechiCondition(states, nonAttractorStates, guarantees.stateGraph, Player.ENVIRONMENT);
						if(opponentDominion.attractorStates.isEmpty()) {
							return;
						}						
						states.removeAll(opponentDominion.attractorStates);
						if(startState != null && !states.contains(startState)) {
							return; // terminate early if it is clear that the system will lose
						}
						winningCondition.reduce(opponentDominion.attractorStates);
						continue gameSolvingLoop;
					}
				}
			}
			
			return;
		}
	}
}
