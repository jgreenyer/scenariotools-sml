/**
 * Copyright (c) 2018 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     
 * Author:
 * 		Daniel Gritzner
 */
package org.scenariotools.synthesis.gr1.gamesolving;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.Transition;

public class FilteredStateGraph {
	private Map<SMLRuntimeState, Set<Transition>> incomingTransitions;
	private Map<SMLRuntimeState, Set<Transition>> outgoingTransitions;
	
	public Set<SMLRuntimeState> allRemovedStates;
	public Set<SMLRuntimeState> removedStates = null; // states removed in the last call to 'reduce'
	public Set<Transition> removedIn = new HashSet<Transition>(); // transitions leading into the states removed in the last call to 'reduce'
	
	public FilteredStateGraph() {
		allRemovedStates = new HashSet<SMLRuntimeState>();
		incomingTransitions = new HashMap<SMLRuntimeState, Set<Transition>>();
		outgoingTransitions = new HashMap<SMLRuntimeState, Set<Transition>>();
	}
	
	public FilteredStateGraph(FilteredStateGraph other) {
		allRemovedStates = new HashSet<SMLRuntimeState>(other.allRemovedStates);
		incomingTransitions = new HashMap<SMLRuntimeState, Set<Transition>>(other.incomingTransitions);
		outgoingTransitions = new HashMap<SMLRuntimeState, Set<Transition>>(other.outgoingTransitions);
		if(other.removedStates != null) {
			removedStates = new HashSet<SMLRuntimeState>(other.removedStates);
		}
		removedIn.addAll(other.removedIn);
	}
	
	public FilteredStateGraph(Set<SMLRuntimeState> remainingStates, SMLRuntimeStateGraph stateGraph) {
		this();
		
		Set<SMLRuntimeState> statesToRemove = new HashSet<SMLRuntimeState>(stateGraph.getStates());
		statesToRemove.removeAll(remainingStates);

		reduce(statesToRemove);
	}
	
	public void reduce(Set<SMLRuntimeState> statesToRemove) {
		allRemovedStates.addAll(statesToRemove);
		incomingTransitions.clear();
		outgoingTransitions.clear();
		
		removedStates = new HashSet<SMLRuntimeState>(statesToRemove); // maybe this explicit copy is not necessary but removing this line needs testing
		for(SMLRuntimeState state : statesToRemove) {
			for(Transition t : state.getIncomingTransition()) {
				if(!allRemovedStates.contains(t.getSourceState())) {
					removedIn.add(t);
				}
			}
		}
	}
	
	public Set<Transition> getIncomingTransitions(SMLRuntimeState state) {
		Set<Transition> result = incomingTransitions.get(state);
		
		if(result == null) {
			result = new HashSet<Transition>();
			for(Transition t : state.getIncomingTransition()) {
				if(!allRemovedStates.contains(t.getSourceState())) {
					result.add(t);
				}
			}
			incomingTransitions.put(state, result);
		}
		
		return result;
	}
	
	public Set<Transition> getOutgoingTransitions(SMLRuntimeState state) {
		Set<Transition> result = outgoingTransitions.get(state);
		
		if(result == null) {
			result = new HashSet<Transition>();
			for(Transition t : state.getOutgoingTransition()) {
				if(!allRemovedStates.contains(t.getTargetState())) {
					result.add(t);
				}
			}
			outgoingTransitions.put(state, result);			
		}
		
		return result;		
	}
}
