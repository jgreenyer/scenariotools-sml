/**
 * Copyright (c) 2018 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     
 * Author:
 * 		Daniel Gritzner
 */
package org.scenariotools.synthesis.gr1.gamesolving;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Stack;

import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.Transition;
import org.scenariotools.synthesis.gr1.gamesolving.GR1Condition.ExplorationIncrement;
import org.scenariotools.synthesis.gr1.gamesolving.GR1Condition.Player;
import org.scenariotools.synthesis.gr1.util.SynthesisUtil;

public class BuechiCondition {
	private Set<Transition> boundary = new HashSet<Transition>();
	private FilteredStateGraph stateGraph;
	
	public Player player;
	public Set<SMLRuntimeState> goalStates;
	public Set<SMLRuntimeState> attractorStates;
	
	public BuechiCondition(Set<SMLRuntimeState> initialStates, Set<SMLRuntimeState> goalStates, FilteredStateGraph stateGraph, Player player) {
		this(initialStates, goalStates, stateGraph, player, false);
	}
	
	public BuechiCondition(Set<SMLRuntimeState> initialStates, Set<SMLRuntimeState> goalStates, FilteredStateGraph stateGraph, Player player, boolean goalStatesAreViolatingStates) {
		this.player = player;
		this.stateGraph = stateGraph;
		
		if(goalStatesAreViolatingStates) {
			this.goalStates = new HashSet<SMLRuntimeState>(initialStates);
			this.goalStates.removeAll(goalStates);			
		} else {
			this.goalStates = new HashSet<SMLRuntimeState>(goalStates);
		}
		attractorStates = new HashSet<SMLRuntimeState>(this.goalStates);
		
		addToBoundary(this.goalStates);
		growAttractorSet();
	}
	
	private void addToBoundary(Set<SMLRuntimeState> goalStates) {
		for(SMLRuntimeState goalState : goalStates) {
			for(Transition t : stateGraph.getIncomingTransitions(goalState)) {
				if(!attractorStates.contains(t.getSourceState())) {
					boundary.add(t);
				}
			}
		}		
	}

	private void growAttractorSet() {
		Set<SMLRuntimeState> newAttractors = new HashSet<SMLRuntimeState>();
		do {
			newAttractors.clear();

			for(Transition t : boundary) {
				SMLRuntimeState sourceState = t.getSourceState();
				if(isAttractor(sourceState)) {
					newAttractors.add(sourceState);
				}
			}
						
			attractorStates.addAll(newAttractors);
			for(SMLRuntimeState state : newAttractors) {
				boundary.removeAll(stateGraph.getOutgoingTransitions(state));
				for(Transition t : stateGraph.getIncomingTransitions(state)) {
					if(!attractorStates.contains(t.getSourceState())) {
						boundary.add(t);						
					}
				}
			}
		} while(!newAttractors.isEmpty());
	}
	
	private boolean isAttractor(SMLRuntimeState state) {
		if(goalStates.contains(state)) {
			return true;
		}
		
		Set<Transition> outgoingTransitions = stateGraph.getOutgoingTransitions(state);
		if(SynthesisUtil.isControllable(state) == (player == Player.SYSTEM)) {
			if(outgoingTransitions.isEmpty()) {
				return false;
			}			
			for(Transition t : outgoingTransitions) {
				if(attractorStates.contains(t.getTargetState())) {
					return true;
				}
			}
			return false;
		} else {
			for(Transition t : outgoingTransitions) {
				if(!attractorStates.contains(t.getTargetState())) {
					return false;
				}
			}
			return true;
		}					
	}
	
	public BuechiCondition reduce(FilteredStateGraph stateGraph) {
		return reduce(stateGraph, false);
	}
	
	public BuechiCondition reduce(FilteredStateGraph stateGraph, boolean createCopy) {
		BuechiCondition result = (createCopy ? new BuechiCondition(this) : this);
		result.stateGraph = stateGraph;
		
		if(stateGraph.removedStates.isEmpty()) {
			return result;
		}

		result.goalStates.removeAll(stateGraph.removedStates);		

		for(Transition t : stateGraph.removedIn) {
			if(result.attractorStates.contains(t.getSourceState())) {
				// completely redo attractor calculation as reachability might have changed
				result.attractorStates.retainAll(result.goalStates);
				result.boundary.clear();
				result.addToBoundary(result.goalStates);
				result.growAttractorSet();
				return result;
			}
		}
		
		Iterator<Transition> iterator = result.boundary.iterator();
		while(iterator.hasNext()) {
			Transition t = iterator.next();
			if(stateGraph.removedStates.contains(t.getSourceState()) || stateGraph.removedStates.contains(t.getTargetState())) {
				iterator.remove();
			}
		}
		
		result.attractorStates.removeAll(stateGraph.removedStates);
		result.growAttractorSet(); // due to the removed states there may be new attractors
		
		return result;
	}
	
	private BuechiCondition(BuechiCondition other) {
		player = other.player;
		boundary.addAll(other.boundary);
		stateGraph = other.stateGraph;
		goalStates = new HashSet<SMLRuntimeState>(other.goalStates);
		attractorStates = new HashSet<SMLRuntimeState>(other.attractorStates);
	}
	
	public void expand(ExplorationIncrement increment, Set<SMLRuntimeState> violatingStates) {
		Set<SMLRuntimeState> newGoalStates = new HashSet<SMLRuntimeState>(increment.newStates);
		newGoalStates.removeAll(violatingStates);
		
		goalStates.addAll(newGoalStates);
		attractorStates.addAll(newGoalStates);

		for(Transition t : increment.newBoundaryIntersection) {
			if(attractorStates.contains(t.getSourceState()) && attractorStates.contains(t.getTargetState())) {
				// completely recalculate as some attractors may lose their attractor status due to the newly added transition: the recursive expansion of the attractor status just won't 'travel' as far
				attractorStates.clear();
				attractorStates.addAll(goalStates);
				boundary.clear();
				addToBoundary(goalStates);
				growAttractorSet();
				return;
			}
		}
		
		addToBoundary(newGoalStates);
		for(Transition t : increment.newBoundaryIn) { // transitions leading out of 'newStates' into previously explored states 
			if(!attractorStates.contains(t.getSourceState()) && attractorStates.contains(t.getTargetState())) {
				boundary.add(t);
			}
		}
		growAttractorSet();

		for(Transition t : increment.newBoundaryOut) { // transitions leading into 'newStates' out of previously explored states
			SMLRuntimeState sourceState = t.getSourceState();
			if(attractorStates.contains(sourceState) && !attractorStates.contains(t.getTargetState())) {
				reevaluate(sourceState);
			}
		}		
	}
	
	private void reevaluate(SMLRuntimeState root) {
		Stack<SMLRuntimeState> stack = new Stack<SMLRuntimeState>();
		stack.push(root);
		
		while(!stack.empty()) {
			SMLRuntimeState state = stack.pop();
						
			if(!isAttractor(state)) {				
				attractorStates.remove(state);
				
				for(Transition t : stateGraph.getIncomingTransitions(state)) {
					if(boundary.contains(t)) {
						boundary.remove(t);						
					} else {
						SMLRuntimeState sourceState = t.getSourceState();
						if(attractorStates.contains(sourceState)) {
							stack.push(sourceState);
						}
					}
				}
				
				for(Transition t : stateGraph.getOutgoingTransitions(state)) {
					if(attractorStates.contains(t.getTargetState())) {
						boundary.add(t);
					}
				}
			}
		}
	}	
}
