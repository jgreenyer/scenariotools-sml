/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     
 * Author:
 * 		Joel Greenyer
 *		Daniel Gritzner
 */
package org.scenariotools.synthesis.gr1.strategyextraction;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.synthesis.gr1.gamesolving.BuechiCondition;
import org.scenariotools.synthesis.gr1.gamesolving.FilteredStateGraph;
import org.scenariotools.synthesis.gr1.gamesolving.GR1Condition;
import org.scenariotools.synthesis.gr1.gamesolving.GR1Condition.ConditionType;
import org.scenariotools.synthesis.gr1.gamesolving.GR1Condition.Player;
import org.scenariotools.synthesis.gr1.gamesolving.GenBuechiCondition;
import org.scenariotools.synthesis.gr1.util.GR1SynthesisToStringUtil;
import org.scenariotools.synthesis.gr1.util.ScenarioAndRoleBindingsKey;
import org.scenariotools.synthesis.gr1.util.SynthesisUtil;
import org.scenariotools.sml.runtime.Transition;

public class AttractorStrategyCalculator {
	private SMLRuntimeStateGraph stateGraph;
	private GR1Condition winningCondition;
	private Set<SMLRuntimeState> winningStates;
	private Set<SMLRuntimeState> losingStates;
	private AttractorStrategy.Factory strategyFactory;
	private Map<String, AttractorStrategy> strategies;
	
	public static Map<String, AttractorStrategy> calculate(SMLRuntimeStateGraph stateGraph, GR1Condition winningCondition, Set<SMLRuntimeState> winningStates, Set<SMLRuntimeState> losingStates, boolean strategyExists) {
		return calculate(stateGraph, winningCondition, winningStates, losingStates, strategyExists, new AttractorStrategy.Factory());
	}
	
	public static Map<String, AttractorStrategy> calculate(SMLRuntimeStateGraph stateGraph, GR1Condition winningCondition, Set<SMLRuntimeState> winningStates, Set<SMLRuntimeState> losingStates, boolean strategyExists, AttractorStrategy.Factory strategyFactory) {
		return new AttractorStrategyCalculator(stateGraph, winningCondition, winningStates, losingStates, strategyExists, strategyFactory).strategies;		
	}
	
	private AttractorStrategyCalculator(SMLRuntimeStateGraph stateGraph, GR1Condition winningCondition, Set<SMLRuntimeState> winningStates, Set<SMLRuntimeState> losingStates, boolean strategyExists, AttractorStrategy.Factory strategyFactory) {
		this.stateGraph = stateGraph;
		this.winningCondition = winningCondition;
		this.winningStates = winningStates;
		this.losingStates = losingStates;
		this.strategyFactory = strategyFactory;
		strategies = new HashMap<String, AttractorStrategy>();
		
		if(strategyExists) {
			calculateSystemAttractorStrategies();
		} else {
			calculateEnvironmentAttractorStrategies();
		}
	}
	
	private void calculateSystemAttractorStrategies() {
		GenBuechiCondition guarantees =  winningCondition.getCondition(ConditionType.GUARANTEE);
		
		// calculate strategies to fulfill guarantees 
		for (Entry<ScenarioAndRoleBindingsKey, BuechiCondition> entry : guarantees.requestedActiveScenarios.entrySet()) {
			String key = GR1SynthesisToStringUtil.calculateStringForScenarioAndRoleBindingsKey(entry.getKey());
			AttractorStrategy attractorStrategy = strategyFactory.create(entry.getValue());
			strategies.put(key, attractorStrategy);
		}

		AttractorStrategy aStrategy = strategyFactory.create(guarantees.firstExtraCondition);
		strategies.put("avoid guarantee safety violation", aStrategy);
		
		aStrategy = strategyFactory.create(guarantees.secondExtraCondition);
		strategies.put("reach env-controlled state", aStrategy);
		
		// add strategies to violate assumptions to each previously calculated strategy
		for (Entry<String, AttractorStrategy> entry : strategies.entrySet()) {
			Map<String, AttractorStrategy> temp = new HashMap<String, AttractorStrategy>();
			temp.put(entry.getKey(), entry.getValue());
			
			Set<SMLRuntimeState> remainingGraph = new HashSet<SMLRuntimeState>(winningStates);
			remainingGraph.removeAll(entry.getValue().attractorStates);
			
			fulfillConditionsWhileViolatingAtLeastOneOpponentCondition(temp, winningCondition.getCondition(ConditionType.ASSUMPTION), remainingGraph, stateGraph, Player.SYSTEM);
		}
	}

	private void calculateEnvironmentAttractorStrategies() {
		GenBuechiCondition assumptions = winningCondition.getCondition(ConditionType.ASSUMPTION);

		for (Entry<ScenarioAndRoleBindingsKey, BuechiCondition> entry : assumptions.requestedActiveScenarios.entrySet()) {
			String key = GR1SynthesisToStringUtil.calculateStringForScenarioAndRoleBindingsKey(entry.getKey());
			AttractorStrategy strategy = strategyFactory.create();
			strategy.goalStates.addAll(entry.getValue().goalStates);
			strategies.put(key, strategy);
		}
		AttractorStrategy noSafetyViolationStrategy = strategyFactory.create();
		noSafetyViolationStrategy.goalStates.addAll(assumptions.firstExtraCondition.goalStates); 
		strategies.put("avoid assumption safety violation", noSafetyViolationStrategy);

		fulfillConditionsWhileViolatingAtLeastOneOpponentCondition(strategies, winningCondition.getCondition(ConditionType.GUARANTEE), losingStates, stateGraph, Player.ENVIRONMENT);
	}
	
	private final static AttractorStrategy.Factory unoptimizedStrategyFactory = new AttractorStrategy.Factory(); // when violating opponent, optimization becomes irrelevant: we want to efficiently achieve our goals, not efficiently violate our opponent
	private static void fulfillConditionsWhileViolatingAtLeastOneOpponentCondition(Map<String, AttractorStrategy> conditionsToFulfill, GenBuechiCondition conditionsToViolate, Set<SMLRuntimeState> remainingStates, SMLRuntimeStateGraph fullStateGraph, Player player) {
		int numStates = fullStateGraph.getStates().size();
		FilteredStateGraph stateGraph = new FilteredStateGraph(remainingStates, fullStateGraph);
		Player opponent = SynthesisUtil.opponentOf(player);
		
		outer:
		while(stateGraph.allRemovedStates.size() != numStates) {
			for(BuechiCondition conditionToViolate : conditionsToViolate) {
				AttractorStrategy opponentStrategy = unoptimizedStrategyFactory.create(new BuechiCondition(null, conditionToViolate.goalStates, stateGraph, opponent));

				FilteredStateGraph subGraph = new FilteredStateGraph(stateGraph);
				subGraph.reduce(opponentStrategy.attractorStates);
				
				Set<SMLRuntimeState> allWinningSubset = null;
				Map<String, AttractorStrategy> tempStrategies = new HashMap<String, AttractorStrategy>();
	
				if(player == Player.ENVIRONMENT) {
					// find the subset in which all assumptions hold AND at the current guarantee under test is violated
					for (Entry<String, AttractorStrategy> entry : conditionsToFulfill.entrySet()) {
						String key = entry.getKey();
						
						AttractorStrategy tempStrategy = unoptimizedStrategyFactory.create(new BuechiCondition(null, entry.getValue().goalStates, subGraph, player)); 
						tempStrategies.put(key, tempStrategy);
						
						if(allWinningSubset == null) {
							allWinningSubset = new HashSet<SMLRuntimeState>(tempStrategy.attractorStates);
						} else {
							allWinningSubset.retainAll(tempStrategy.attractorStates);
						}
					}					
				} else {
					// we just want to violate the current assumption under test anyway
					allWinningSubset = new HashSet<SMLRuntimeState>(fullStateGraph.getStates());
					allWinningSubset.removeAll(subGraph.allRemovedStates);
					tempStrategies = conditionsToFulfill;
				}

				if(!allWinningSubset.isEmpty()) {
					AttractorStrategy strategyToReachCommonSubset = unoptimizedStrategyFactory.create(new BuechiCondition(null, allWinningSubset, stateGraph, player)); 
					for(SMLRuntimeState state : strategyToReachCommonSubset.attractorStates) {
						if(!SynthesisUtil.isControllable(state, player))
							continue;
						
						for (Entry<String, AttractorStrategy> entry : conditionsToFulfill.entrySet()) {
							String key = entry.getKey();
							AttractorStrategy tempStrategy = tempStrategies.get(key);
							AttractorStrategy conditionStrategy = entry.getValue();
							
//							conditionStrategy.attractorStates.addAll(tempStrategy.attractorStates);
							Transition candidate = tempStrategy.move.get(state);
							if(candidate == null)
								candidate = strategyToReachCommonSubset.move.get(state);
							conditionStrategy.move.put(state, candidate);
						}						
					}
					
					stateGraph.reduce(strategyToReachCommonSubset.attractorStates);
					continue outer;
				}
			}
			
			break; // remainingGraph can not be reduced further but it is only an unreachable subgraph anyway 
		}
	}
}
