/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     
 * Author:
 * 		Joel Greenyer
 *		Daniel Gritzner
 */
package org.scenariotools.synthesis.gr1.strategyextraction;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.synthesis.gr1.gamesolving.BuechiCondition;
import org.scenariotools.synthesis.gr1.util.SynthesisUtil;
import org.scenariotools.sml.runtime.Transition;

public final class AttractorStrategy {
	public Set<SMLRuntimeState> goalStates;
	public Set<SMLRuntimeState> attractorStates;
	
	public final Map<SMLRuntimeState, Transition> move = new HashMap<SMLRuntimeState, Transition>();

	private AttractorStrategy() {}
	
	public static class Factory {
		public AttractorStrategy create() {
			AttractorStrategy result = new AttractorStrategy();
			result.goalStates = new HashSet<SMLRuntimeState>();
			result.attractorStates = new HashSet<SMLRuntimeState>();
			return result;
		}
		
		public AttractorStrategy create(BuechiCondition goalCondition) {
			AttractorStrategy result = new AttractorStrategy();
			
			result.goalStates = goalCondition.goalStates;
			result.attractorStates = goalCondition.attractorStates;
			
			Queue<SMLRuntimeState> queue = new LinkedList<SMLRuntimeState>();
			queue.addAll(result.goalStates);
			Set<SMLRuntimeState> visited = new HashSet<SMLRuntimeState>();
			visited.addAll(result.goalStates);
			
			while(!queue.isEmpty()) {
				SMLRuntimeState state = queue.poll();
				
				for(Transition t : state.getIncomingTransition()) {
					SMLRuntimeState sourceState = t.getSourceState();
					if(result.attractorStates.contains(sourceState) && !visited.contains(sourceState)) {
						queue.add(sourceState);
						visited.add(sourceState);
						
						if(SynthesisUtil.isControllable(t, goalCondition.player)) {
							result.move.put(sourceState, t);						
						}
					}
				}
			}

			// add moves for controllable target states (might be required if all goals are fulfilled at the same time)
			outer:
			for(SMLRuntimeState state : result.goalStates) {
				if(SynthesisUtil.isControllable(state, goalCondition.player) && !result.move.containsKey(state)) {
					for (Transition t : state.getOutgoingTransition()) {
						if(result.attractorStates.contains(t.getTargetState())) {
							result.move.put(state, t);
							continue outer;
						}
					}
				}
			}
			
			return result;
		}
	}
}
