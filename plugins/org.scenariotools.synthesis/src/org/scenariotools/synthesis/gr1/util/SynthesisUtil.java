/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.synthesis.gr1.util;

import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.WaitEvent;
import org.scenariotools.synthesis.gr1.gamesolving.GR1Condition.Player;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.Transition;

public class SynthesisUtil {

	public static boolean isControllable(Transition transition) {
		Event event = transition.getEvent();
		
		if(event instanceof WaitEvent)
			return true;
		else if(event instanceof MessageEvent)
			return transition.getSourceState().getObjectSystem().isControllable(((MessageEvent) transition.getEvent()).getSendingObject());
		else
			return false;
	}
	
	public static boolean isControllable(Transition transition, Player player) {
		return isControllable(transition) == (player == Player.SYSTEM);
	}
	
	public static boolean isControllable(SMLRuntimeState state) {
		if(state.getOutgoingTransition().size() == 0)
			return false;
		
		return isControllable(state.getOutgoingTransition().get(0)); // assumes that there are no 'mixed' states with both controllable and uncontrollable transitions
	}
	
	public static boolean isControllable(SMLRuntimeState state, Player player) {
		return isControllable(state) == (player == Player.SYSTEM);		
	}
	
	public static Player playerOfState(SMLRuntimeState state) {
			if(isControllable(state.getOutgoingTransition().iterator().next()))
				return Player.SYSTEM;
			else
				return Player.ENVIRONMENT;
	}
	
	public static Player opponentOf(Player playerKind) {
		if (playerKind == Player.SYSTEM)
			return Player.ENVIRONMENT;
		else
			return Player.SYSTEM;
	}
	
}
