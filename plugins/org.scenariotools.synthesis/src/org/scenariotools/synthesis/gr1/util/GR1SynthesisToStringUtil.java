/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.synthesis.gr1.util;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.util.SMLRuntimeStateUtil;
import org.scenariotools.sml.runtime.Transition;

public class GR1SynthesisToStringUtil {
	
	public static String calculateStringForScenarioAndRoleBindingsToStatesMap(Map<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> scenarioAndRoleBindingsToNonAcceptanceStatesMap) {
		String s = "";
		for (Entry<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> scenarioAndRoleBindingsToNonAcceptanceStatesMapEntry : scenarioAndRoleBindingsToNonAcceptanceStatesMap.entrySet()) {
			ScenarioAndRoleBindingsKey key = scenarioAndRoleBindingsToNonAcceptanceStatesMapEntry.getKey();
			Set<SMLRuntimeState> value = scenarioAndRoleBindingsToNonAcceptanceStatesMapEntry.getValue();
			s += "("+ calculateStringForScenarioAndRoleBindingsKey(key) + ") -> ";
			s += concatenateStateIndicesStringForStateSet(value) + "\n ~ ";
		}
		return "( " + s + " )";
	}

	public static String calculateStringForScenarioAndRoleBindingsKey(ScenarioAndRoleBindingsKey key) {
		return key.scenario.getName() + ", " + calculateStringForActiveScenarioRoleBindings(key.activeScenarioBindings);
	}

	public static String calculateStringForActiveScenarioRoleBindings(ActiveScenarioRoleBindings activeScenarioRoleBindings) {
		return "[" + activeScenarioRoleBindings.getRoleBindings().entrySet().stream().map(entry -> calculateStringForActiveScenarioRoleBindingsEntry(entry)).collect(Collectors.joining(", ")) + "]";
	}
	
	public static String calculateStringForActiveScenarioRoleBindingsEntry(Entry<Role, EObject> entry) {
		return entry.getKey().getName() + ":" + SMLRuntimeStateUtil.getEObjectName(entry.getValue());
	}

	public static String concatenateStateIndicesStringForStateSet(Set<SMLRuntimeState> smlRuntimeStates) {
		return "{" + smlRuntimeStates.stream().map(s -> SMLRuntimeStateUtil.getPassedIndex(s))
				.collect(Collectors.joining(", ")) + "}";
	}

	public static String concatenateStateIndicesStringForStateSets(Set<Set<SMLRuntimeState>> smlRuntimeStatesSets) {
		return "{ " + smlRuntimeStatesSets.stream().map(statesSet -> concatenateStateIndicesStringForStateSet(statesSet))
				.collect(Collectors.joining(";\n -- ")) + " }";
	}
	
	public static String getTransiitionString(Transition t) {
		return SMLRuntimeStateUtil.getPassedIndex(t.getSourceState()) 
				+ "---" 
				+ t.getEvent().toString()
				+ "-->"
				+ SMLRuntimeStateUtil.getPassedIndex(t.getTargetState());
	}
}
