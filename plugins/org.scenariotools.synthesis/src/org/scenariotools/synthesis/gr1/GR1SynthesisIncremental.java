/**
 * Copyright (c) 2018 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     
 * Author:
 * 		Daniel Gritzner
 */
package org.scenariotools.synthesis.gr1;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.synthesis.AbstractSynthesisAlgorithm;
import org.scenariotools.synthesis.ResultMetaData;
import org.scenariotools.synthesis.SynthesisResult;
import org.scenariotools.synthesis.gr1.gamesolving.GR1Condition;
import org.scenariotools.synthesis.gr1.gamesolving.GameSolver;
import org.scenariotools.synthesis.gr1.strategyextraction.AttractorStrategy;
import org.scenariotools.synthesis.gr1.strategyextraction.AttractorStrategyCalculator;
import org.scenariotools.synthesis.gr1.strategyextraction.StrategyExtractor;
import org.scenariotools.synthesis.gr1.util.SMLRuntimeStateGraphStrategyInformationAnnotator;

public class GR1SynthesisIncremental extends AbstractSynthesisAlgorithm {
	public Set<SMLRuntimeState> winningStates;
	public Set<SMLRuntimeState> losingStates;

	@ResultMetaData(name="number of winning states", group=SynthesisResult.STATEGRAPH_GROUP_NAME, position=1)
	public long numWinningStates;
	
	@ResultMetaData(name="number of losing states", group=SynthesisResult.STATEGRAPH_GROUP_NAME, position=2)
	public long numLosingStates;
	
	@ResultMetaData(name="state graph exploration [ms]", group="GR(1) Synthesis Time Measurements", position=1)
	public long stateGraphExplorationTime;

	@ResultMetaData(name="GR(1) game solving [ms]", group="GR(1) Synthesis Time Measurements", position=3)
	public long gameSolvingTime = 0;

	@ResultMetaData(name="total [ms]", group="GR(1) Synthesis Time Measurements", position=4)
	public long totalTime;
	
	@ResultMetaData(name="time [ms]", group="Strategy Extraction", position=1)
	public long strategyExtractionTime = 0;
	
	@ResultMetaData(name="number of iterations", group="Incremental GR(1) Synthesis", position=1)
	public long numIncrementalIterations = 0;

	private static final String EXPLORATION_TASK_NAME = "exploring state graph and collecting goal states, iteration ";
	
	@Override
	public Object synthesize(Configuration configuration, IProgressMonitor monitor) {
		long currentTimeMillis = System.currentTimeMillis();
		monitor.beginTask("initializing state graph", IProgressMonitor.UNKNOWN);
		stateGraph = RuntimeFactory.eINSTANCE.createSMLRuntimeStateGraph();
		SMLRuntimeState startState = stateGraph.init(configuration);
		monitor.setTaskName(EXPLORATION_TASK_NAME + "1");
		GR1Condition winningCondition = new GR1Condition(stateGraph, true, monitor);
		GR1Condition winningConditionCopy = winningCondition;
		stateGraphExplorationTime = System.currentTimeMillis() - currentTimeMillis;
		
		while(!monitor.isCanceled()) {			
			numIncrementalIterations += 1;
			
			currentTimeMillis = System.currentTimeMillis();
			monitor.setTaskName("solving game, iteration " + numIncrementalIterations);
			winningStates = new HashSet<SMLRuntimeState>(stateGraph.getStates());
			winningConditionCopy = winningCondition.reduce(Collections.emptySet(), true); // create copy of winningCondition because the solver actually may reduce it 
			GameSolver.solve(winningStates, winningConditionCopy, (GR1Condition.TERMINATE_GAME_SOLVING_EARLY ? startState : null), monitor);
			strategyExists = winningStates.contains(startState);
			gameSolvingTime += System.currentTimeMillis() - currentTimeMillis;
			
			currentTimeMillis = System.currentTimeMillis();
			monitor.setTaskName(EXPLORATION_TASK_NAME + (numIncrementalIterations + 1));
			boolean hasExpanded = winningCondition.expand(winningStates, strategyExists, monitor);
			stateGraphExplorationTime += System.currentTimeMillis() - currentTimeMillis;
			
			if(!hasExpanded) {
				break;
			}
		}
		
		losingStates = new HashSet<SMLRuntimeState>(stateGraph.getStates());
		losingStates.removeAll(winningStates);
		numWinningStates = winningStates.size();
		numLosingStates = losingStates.size();
		totalTime = stateGraphExplorationTime + gameSolvingTime;
		
		if(strategyExists) {
			currentTimeMillis = System.currentTimeMillis();
			monitor.setTaskName("calculating attractor strategies");
			Map<String, AttractorStrategy> strategies = AttractorStrategyCalculator.calculate(stateGraph, winningConditionCopy, winningStates, losingStates, true);
			monitor.setTaskName("extracting memoryless strategy");
			strategy = StrategyExtractor.extractStrategy(stateGraph, strategies);
			strategyExtractionTime = System.currentTimeMillis() - currentTimeMillis;
			monitor.setTaskName("adding strategy annotations");
			SMLRuntimeStateGraphStrategyInformationAnnotator.annotateAttractorStrategyInformation(strategies, strategyExists);
		}
		monitor.setTaskName("adding state graph annotations");
		SMLRuntimeStateGraphStrategyInformationAnnotator.annotateStrategyOrCounterStrategyStates(winningStates, losingStates, strategyExists);		
		
		monitor.done();
		return this;
	}	
}
