package org.scenariotools.synthesis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.HashSet;
import java.lang.Class;
import java.lang.reflect.Field;

public class ResultParser {
	public List<Group> finalResult;
	public List<List<Group>> rawData = new ArrayList<List<Group>>();
	@SuppressWarnings("rawtypes")
	private Class typeCheck;
	private Map<String, Map<String, List<NamedValue>>> tempData = new HashMap<String, Map<String, List<NamedValue>>>();	
	
	public static List<Group> parse(Object object) throws Exception {
		return sort(_parse(object));
	}
	
	public ResultParser(Object object) throws Exception {
		typeCheck = object.getClass();
		addData(object);
	}
	
	public void addData(Object object) throws Exception {
		assert object.getClass() == typeCheck;
		Map<String, List<NamedValue>> data = _parse(object);
		rawData.add(sort(data));

		for(Map.Entry<String, List<NamedValue>> entry : data.entrySet()) {
			Map<String, List<NamedValue>> group = tempData.get(entry.getKey());
			if(group == null) {
				group = new HashMap<String, List<NamedValue>>();
				tempData.put(entry.getKey(), group);
			}

			for(NamedValue nv : entry.getValue()) {
				List<NamedValue> values = group.get(nv.name);
				if(values == null) {
					values = new ArrayList<NamedValue>();
					group.put(nv.name, values);
				}

				values.add(nv);
			}
		}		
	}
	
	public void finalize() {
		Map<String, List<NamedValue>> result = new HashMap<String, List<NamedValue>>();
		for(Map.Entry<String, Map<String, List<NamedValue>>> entry : tempData.entrySet()) {
			List<NamedValue> values = new ArrayList<NamedValue>();

			for(Map.Entry<String, List<NamedValue>> entry2 : entry.getValue().entrySet()) {				
				NamedValue nv = new NamedValue();
				nv.name = entry2.getValue().get(0).name; // == entry2.getKey() == entry2.getValue().get(i).name
				nv.value = new Statistics(entry2.getValue());
				nv.position = entry2.getValue().get(0).position;
				values.add(nv);
			}

			result.put(entry.getKey(), values);
		}

		tempData.clear();
		finalResult = sort(result);		
	}
	
	public int size() {
		return rawData.size();
	}

	public static void mergeInto(List<Group> dst, List<Group> src) {
		for(Group group : src) {
			Group dstGroup = null;
			for(Group candidate : dst) {
				if(candidate.name.equals(group.name)) {
					dstGroup = candidate;
					break;
				}
			}
			
			if(dstGroup != null) {
				dstGroup.values.addAll(group.values);
			} else {
				dst.add(group);
			}
		}
	}
	
	public static class Group {
		public String name;
		public List<NamedValue> values;
	}

	public static class NamedValue {
		public String name;
		public Object value;
		private int position;
	}

	public static class Statistics {
		private double average;
		private Number minimum;
		private String median;
		private Number maximum;

		private Statistics(List<NamedValue> _values) {
			@SuppressWarnings("rawtypes")
			Class theClass = _values.get(0).value.getClass();
			@SuppressWarnings("unchecked")
			List<Number> values = (List<Number>)(List<?>)_values.stream().map(v -> v.value).collect(Collectors.toList());
			Number sum = null;

			if(theClass == Long.class) {
				@SuppressWarnings("unchecked")
				List<Long> temp = (List<Long>)(List<?>)values;
				Collections.sort(temp);
				sum = temp.stream().reduce(0L, Long::sum);
			} else if(theClass == Double.class) {
				@SuppressWarnings("unchecked")
				List<Double> temp = (List<Double>)(List<?>)values;
				Collections.sort(temp);
				sum = temp.stream().reduce(0.0, Double::sum);
			} else {
				assert false; // unsupported type
			}

			minimum = values.get(0);
			average = sum.doubleValue() / values.size();
			maximum = values.get(values.size()-1);

			if(values.size() % 2 == 0) {
				int index = values.size() / 2;
				double a = values.get(index-1).doubleValue();
				double b = values.get(index).doubleValue();
				median = String.format(SynthesisResult.FLOAT_OUTPUT_FORMAT, (a + b) / 2.0);
			} else {
				int index = (values.size()-1) / 2;
				median = values.get(index).toString();
			}
		}

		public String toString() {
			return String.format(SynthesisResult.FLOAT_OUTPUT_FORMAT + " (%s/%s/%s)", average, toString(minimum), median, toString(maximum));
		}
		
		public String toCSVString() {
			return String.format("\"" + SynthesisResult.FLOAT_OUTPUT_FORMAT + "\",\"%s\",\"%s\",\"%s\"", average, toString(minimum), median, toString(maximum));
		}
		
		private String toString(Number n) {
			if(n.getClass() == Long.class)
				return n.toString();
			else
				return String.format(SynthesisResult.FLOAT_OUTPUT_FORMAT, n);
		}
	}

	@SuppressWarnings("rawtypes")
	private static final Set<Class> ACCEPTED_TYPES = new HashSet<Class>(Arrays.asList(byte.class, short.class, int.class, long.class, float.class, double.class));
	@SuppressWarnings("rawtypes")
	private static final Set<Class> FLOAT_TYPES = new HashSet<Class>(Arrays.asList(float.class, double.class));

	private static Map<String, List<NamedValue>> _parse(Object object) throws Exception {
		Map<String, List<NamedValue>> result = new HashMap<String, List<NamedValue>>();

		for(Field field : object.getClass().getFields()) {
			if(!ACCEPTED_TYPES.contains(field.getType()))
				continue;

			ResultMetaData metaData = (ResultMetaData)field.getAnnotation(ResultMetaData.class);
			if(metaData == null)
				continue;
			
			List<NamedValue> group = result.get(metaData.group());
			if(group == null) {
				group = new ArrayList<NamedValue>();
				result.put(metaData.group(), group);
			}

			NamedValue nv = new NamedValue();
			nv.name = metaData.name();
			Number value = (Number)field.get(object);
			if(FLOAT_TYPES.contains(field.getType()))
				nv.value = value.doubleValue();
			else
				nv.value = value.longValue();
			nv.position = metaData.position();
			
			group.add(nv);
		}

		return result;
	}

	private static List<Group> sort(Map<String, List<NamedValue>> data) {
		List<Group> result = new ArrayList<Group>(data.size());
		for(Map.Entry<String, List<NamedValue>> entry : data.entrySet()) {
			Group group = new Group();
			group.name = entry.getKey();
			group.values = entry.getValue();

			Collections.sort(group.values, new Comparator<NamedValue>() {
				public int compare(NamedValue nv1, NamedValue nv2) {
					return nv1.position - nv2.position;
				}
			});

			result.add(group);
		}

		Collections.sort(result, new Comparator<Group>() { 
			public int compare(Group g1, Group g2) {
				return g1.name.compareTo(g2.name);
			}
		});

		return result;
	}
}
