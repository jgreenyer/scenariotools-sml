/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Mohammed Abed
 */
package org.scenariotools.synthesis.benchmark.ui.preferencepage;

/**
 * @author Mohammed Abed
 * @date 20.11.2016
 * @description Constant definitions for plug-in preferences
 */
public class PreferenceConstantsForBenchmark {
	
	public static final String NUMBER_OF_ITERATIONS = "numberOfIterations";
	public static final String NUMBER_OF_WARMUP_ITERATIONS = "numberofWarmUpIterations";
}
