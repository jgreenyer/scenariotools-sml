/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Mohammed Abed
 */
package org.scenariotools.synthesis.benchmark.ui.preferencepage;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Group;
import org.scenariotools.synthesis.Activator;
import org.scenariotools.synthesis.ui.editors.SpacerFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * @author Mohammed Abed
 * @date 18.06.2016
 * @description this class serve a preference page for all benchmarking feature
 */
public class BenchmarkPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	private final String TITEL = "Benchmark Feature";
	
	private Group groupForBenchmark= null;	
	
	public BenchmarkPreferencePage() {
		super(GRID);
	}

	@Override
	protected void createFieldEditors() {
	
		setTitle(TITEL);
		addField(new SpacerFieldEditor(getFieldEditorParent()));
		
		/*
		 * creating boolean widgets to populate the enabling of benchmarking
		 */
		groupForBenchmark = new Group(getFieldEditorParent(), SWT.NONE);
		groupForBenchmark.setText("These parameters apply to all synthesis algorithms");
		GridData gridDataForBenchmark = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 2);
		gridDataForBenchmark.verticalAlignment = 20;
		groupForBenchmark.setLayoutData(gridDataForBenchmark);
		IntegerFieldEditor numberOfIterations = new IntegerFieldEditor(PreferenceConstantsForBenchmark.NUMBER_OF_ITERATIONS, "Benchmark iterations", groupForBenchmark);
		addField(numberOfIterations);
		IntegerFieldEditor numberOfWarmUpIterations = new IntegerFieldEditor(PreferenceConstantsForBenchmark.NUMBER_OF_WARMUP_ITERATIONS, "Warm-up iterations", groupForBenchmark);
		addField(numberOfWarmUpIterations);
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event)
	{
		super.propertyChange(event);
	}
	
	@Override
	protected void performDefaults() {
		super.performDefaults();
	}
	
	@Override
	public void init(IWorkbench workbench) {
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
	}
}
