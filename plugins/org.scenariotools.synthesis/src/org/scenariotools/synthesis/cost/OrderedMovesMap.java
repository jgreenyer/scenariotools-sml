package org.scenariotools.synthesis.cost;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.util.TransitionUtil;
import org.scenariotools.sml.runtime.Transition;

/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
public class OrderedMovesMap {
	private Map<SMLRuntimeState, OrderedMovesList> moves;
	
	public OrderedMovesMap() {
		moves = new HashMap<SMLRuntimeState, OrderedMovesList>();
	}
	
	public Transition getBestMove(SMLRuntimeState state) {
		OrderedMovesList list = moves.get(state);
		if(list == null)
			return state.getOutgoingTransition().iterator().next(); // return random transition
		
		boolean isControllable = true;
		Event event = state.getOutgoingTransition().get(0).getEvent();
		if(event != null) {
			MessageEvent messageEvent = (MessageEvent)event;
			isControllable = state.getObjectSystem().isControllable(messageEvent.getSendingObject());				
		}

		int index = 0;
		if(!isControllable)
			index = list.moves.size() - 1;
			
		return list.moves.get(index).t;
	}
	
	public List<Transition> getOrderedMoves(SMLRuntimeState state) {
		OrderedMovesList list = moves.get(state);
		if(list == null)
			return new ArrayList<Transition>(state.getOutgoingTransition());
		
		List<Transition> result = new ArrayList<Transition>(state.getOutgoingTransition().size());
		
		for(OrderedMovesList.ListEntry entry : list.moves) {
			result.add(entry.t);
		}
		
		if(result.size() < state.getOutgoingTransition().size()) {
			for(Transition t : state.getOutgoingTransition()) {
				if(!result.contains(t))
					result.add(t);				
			}
		}

		return result;
	}
	
	public boolean addMove(Transition t, double cost, double potentialGain) {
		SMLRuntimeState state = t.getSourceState();
		OrderedMovesList list = moves.get(state);
		if(list == null) {
			list = new OrderedMovesList();
			moves.put(state, list);
		}
		
		int index = list.contains(t);
		if(index < 0)
			return list.addNewMove(t, cost, potentialGain);
		else
			return list.replaceMove(t, cost, potentialGain, index);
	}
	
	public boolean allMovesKnown(SMLRuntimeState state) {
		OrderedMovesList list = moves.get(state);
		if(list != null) {
			return list.moves.size() == state.getOutgoingTransition().size();
		}
		
		return false;
	}
	
	public void addDebugInformation() {
		for(OrderedMovesList orderedMovesList : moves.values()) {
			for(OrderedMovesList.ListEntry entry : orderedMovesList.moves) {
				TransitionUtil.appendAttractorStrategyInformation(entry.t, "cost = " + entry.cost);				
			}
		}
	}
	
	private static class OrderedMovesList {
		private List<ListEntry> moves;
		
		private OrderedMovesList() {
			moves = new ArrayList<ListEntry>();
		}
		
		private int contains(Transition t) {
			for(int i = 0; i < moves.size(); i++) {
				if(moves.get(i).t == t)
					return i;
			}
			
			return -1;
		}
		
		private boolean addNewMove(Transition t, double cost, double potentialGain) {
			ListEntry newEntry = new ListEntry(t, cost, potentialGain);
			
			// add t to moves list such that the list is ordered by cost first, then by potentialGain and finally by order of adding the moves themselves
			for(int i = 0; i < moves.size(); i++) {
				ListEntry listEntry = moves.get(i);
				if(cost < listEntry.cost || (cost == listEntry.cost && potentialGain > listEntry.potentialGain)) {
					// add new entry s.t. the cost of all prior entries is less than or equal to 'cost' and all remaining entries have a higher cost
					moves.add(i, newEntry);
					return true;
				}
			}
			
			moves.add(newEntry); // add to end of list (most expensive move so far)
			return true;
		}
		
		private boolean replaceMove(Transition t, double cost, double potentialGain, int index) {
			boolean improved = false;
			
			ListEntry entry = moves.get(index);
			if(entry.cost > cost) {
				entry.cost = cost;
				entry.potentialGain = potentialGain;
				improved = true;
			} else if(entry.cost == cost && entry.potentialGain < potentialGain) {
				entry.potentialGain = potentialGain;
				improved = true;
			}
			
			if(!improved)
				return false;
			
			Collections.sort(moves, new Comparator<ListEntry>() {
				@Override
				public int compare(ListEntry e1, ListEntry e2) {
					return (int) Math.signum(e2.potentialGain - e1.potentialGain);
				}
			});
			
			Collections.sort(moves, new Comparator<ListEntry>() {
				@Override
				public int compare(ListEntry e1, ListEntry e2) {
					return (int) Math.signum(e1.cost - e2.cost);
				}				
			});
			
			return true;
		}
		
		private static class ListEntry {
			private Transition t;
			private double cost;
			private double potentialGain;
			
			private ListEntry(Transition t, double cost, double potentialGain) {
				this.t = t;
				this.cost = cost;
				this.potentialGain = potentialGain;
			}
		}
	}

}
