package org.scenariotools.synthesis.cost;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;
import java.util.function.Function;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.synthesis.Activator;
import org.scenariotools.synthesis.gr1.util.ScenarioAndRoleBindingsKey;
import org.scenariotools.sml.runtime.Transition;

/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
class TransitionCostCalculator {
	long gatherOptimizationSetsTime;
	long splitOptimizationSetsTime;
	long propagateCostsTime;
	
	OrderedMovesMap moves;
	
	private static Logger logger = Activator.getLogManager().getLogger(TransitionCostCalculator.class.getName());
	private SMLRuntimeStateGraph smlRuntimeStateGraph;
	private IProgressMonitor monitor;
	
	private Set<SMLRuntimeState> allGoalStates = new HashSet<SMLRuntimeState>();
	private Map<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> allCosts = new HashMap<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>>();
	
	private List<Subgraph> goals = new ArrayList<Subgraph>();
	private List<Subgraph> costs = new ArrayList<Subgraph>();
	private List<Subgraph> gains = new ArrayList<Subgraph>();
	
	static TransitionCostCalculator calculate(SMLRuntimeStateGraph smlRuntimeStateGraph, IProgressMonitor monitor) {
		return new TransitionCostCalculator(smlRuntimeStateGraph, monitor);
	}
	
	private TransitionCostCalculator(SMLRuntimeStateGraph smlRuntimeStateGraph, IProgressMonitor monitor) {
		this.smlRuntimeStateGraph = smlRuntimeStateGraph;
		this.monitor = monitor;
		
		long[] times = new long[] {0, 0, 0, 0};
		times[0] = System.currentTimeMillis();
		
		gatherOptimizationSets();
		times[1] = System.currentTimeMillis();
		
		splitOptimizationSets();
		if(logger.isDebugEnabled()) {
			Function<List<Subgraph>, String> getSizesString = l -> {
				String result = "";
				for(int i = 0; i < l.size(); i++) {
					result += l.get(i).states.size() + (i == l.size()-1 ? "" : " ");
				}
				return result;
			};
			
			logger.debug("optimization goals sizes: " + getSizesString.apply(goals));
			logger.debug("costs sizes: " + getSizesString.apply(costs));
			logger.debug("gains sizes: " + getSizesString.apply(gains));
		}
		times[2] = System.currentTimeMillis();
		
		propagateCosts();
		times[3] = System.currentTimeMillis();
		
		gatherOptimizationSetsTime = times[1] - times[0];
		splitOptimizationSetsTime = times[2] - times[1];
		propagateCostsTime = times[3] - times[2];
	}
	
	private void gatherOptimizationSets() {
		for(SMLRuntimeState _state : smlRuntimeStateGraph.getStates()) {
			if(monitor.isCanceled())
				return;
			
			SMLRuntimeState state = (SMLRuntimeState)_state;
			for(ActiveScenario activeScenario : state.getActiveScenarios()) {
				Scenario theScenario = activeScenario.getScenario();
				if(theScenario.isOptimizeCost()) {
					allGoalStates.add(state);
				} else if(theScenario.getCost() != 0.0) {
					ScenarioAndRoleBindingsKey key = new ScenarioAndRoleBindingsKey(activeScenario);
					if(allCosts.containsKey(key)) {
						allCosts.get(key).add(state);
					} else {					
						Set<SMLRuntimeState> states = new HashSet<SMLRuntimeState>();
						states.add(state);
						allCosts.put(key, states);
					}	
				}
			}
		}
	}

	private void splitOptimizationSets() {
		goals.addAll(Subgraph.split(null, allGoalStates));
		allGoalStates = null;
		
		for(Entry<ScenarioAndRoleBindingsKey, Set<SMLRuntimeState>> entry : allCosts.entrySet()) {
			if(monitor.isCanceled())
				return;
			
			ScenarioAndRoleBindingsKey key = entry.getKey();
			if(key.scenario.getCost() > 0.0) {
				costs.addAll(Subgraph.split(key, entry.getValue()));
			} else {
				gains.addAll(Subgraph.split(key, entry.getValue()));				
			}
		}
		allCosts = null;
	}
	
	private void propagateCosts() {
		moves = new OrderedMovesMap();
		
		for(Subgraph goal : goals) {
			Map<SMLRuntimeState, TransitionCost> bestMoves = new HashMap<SMLRuntimeState, TransitionCost>();
			Set<SMLRuntimeState> improved = new HashSet<SMLRuntimeState>();
			
			Stack<TransitionCost> stack = new Stack<TransitionCost>();
			for(Transition t : goal.terminatingTransitions) {
				stack.push(new TransitionCost(t, costs, gains));
			}
			
			while(!stack.empty()) {
				if(monitor.isCanceled())
					return;
				
				TransitionCost tc = stack.pop();
				Transition t = tc.t;
				boolean changed = moves.addMove(t, tc.effectiveCost, tc.remainingPotentialGain);

				SMLRuntimeState sourceState = t.getSourceState();
				if(changed && moves.getBestMove(sourceState) == t) {
					bestMoves.put(sourceState, tc);
					improved.add(sourceState);
				}
				
				if(moves.allMovesKnown(sourceState) && improved.contains(sourceState)) {
					pushToStack(stack, sourceState, bestMoves, goal);
					improved.remove(sourceState);
				}
				
				if(stack.empty() && !improved.isEmpty()) {
					for(SMLRuntimeState state : improved) {
						pushToStack(stack, state, bestMoves, goal);
					}
					improved.clear();
				}
			}
		}
	}
	
	private static void pushToStack(Stack<TransitionCost> stack, SMLRuntimeState state, Map<SMLRuntimeState, TransitionCost> bestMoves, Subgraph goal) {
		TransitionCost bestMove = bestMoves.get(state);
		for(Transition inT : state.getIncomingTransition()) {
			if(goal.states.contains(inT.getSourceState()))
				stack.push(new TransitionCost(bestMove, inT));								
		}							
	}
}
