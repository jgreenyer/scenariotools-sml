package org.scenariotools.synthesis.cost;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.Transition;
import org.scenariotools.synthesis.gr1.util.ScenarioAndRoleBindingsKey;

/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
class Subgraph {
	private ScenarioAndRoleBindingsKey key;
	Set<SMLRuntimeState> states;
	Set<Transition> initializingTransitions;
	Set<Transition> terminatingTransitions;
	
	static Set<Subgraph> split(ScenarioAndRoleBindingsKey key, Set<SMLRuntimeState> states) {
		Set<Subgraph> result = new HashSet<Subgraph>();
		
		while(!states.isEmpty()) {
			Subgraph subgraph = new Subgraph(key, states);
			result.add(subgraph);
			states.removeAll(subgraph.states);
		}
		
		return result;
	}
	
	private Subgraph(ScenarioAndRoleBindingsKey key, Set<SMLRuntimeState> states) {
		this.key = key;
		this.states = new HashSet<SMLRuntimeState>();
		initializingTransitions = new HashSet<Transition>();
		terminatingTransitions = new HashSet<Transition>();
		
		Stack<SMLRuntimeState> stack = new Stack<SMLRuntimeState>();
		stack.push(states.iterator().next()); // pick random state from 'states' as seed
		
		// grow subgraph from seed; NOTE: this approach does not support scenarios which can in one transition be terminated and re-initialized as a 'new' instance
		while(!stack.isEmpty()) {
			SMLRuntimeState state = stack.pop();
			if(this.states.contains(state)) // already processed 'state'
				continue;			
			this.states.add(state);
			
			for(Transition t : state.getIncomingTransition()) {
				SMLRuntimeState prevState = (SMLRuntimeState)t.getSourceState();
				if(states.contains(prevState)) {
					stack.push(prevState);
				} else {
					initializingTransitions.add(t);
				}
			}
			
			for(Transition t : state.getOutgoingTransition()) {
				SMLRuntimeState nextState = (SMLRuntimeState)t.getTargetState();
				if(states.contains(nextState)) {
					stack.push(nextState);					
				} else {
					terminatingTransitions.add(t);
				}
			}
		}
	}
	
	double getCost() {
		return key.scenario.getCost();
	}	
}
