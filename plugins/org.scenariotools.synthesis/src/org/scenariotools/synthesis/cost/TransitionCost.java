package org.scenariotools.synthesis.cost;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.core.runtime.Assert;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.Transition;

/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
class TransitionCost {
	Transition t;
	double effectiveCost;
	double remainingPotentialGain;
	private double cost;
	private Map<Integer, Set<Integer>> gainsToOverlappingCosts;
	private Map<Integer, Double> permanentCompensatedCosts;
	
	private List<Subgraph> costs;
	private List<Subgraph> gains;
	
	TransitionCost(Transition t, List<Subgraph> costs, List<Subgraph> gains) {
		this.t = t;
		this.costs = costs;
		this.gains = gains;
		
		gainsToOverlappingCosts = new HashMap<Integer, Set<Integer>>();
		for(int i = 0; i < gains.size(); i++) {
			Subgraph gain = gains.get(i);
			if(gain.states.contains(t.getSourceState())) {
				gainsToOverlappingCosts.put(i, new HashSet<Integer>());
			}
		}

		permanentCompensatedCosts = new HashMap<Integer, Double>();		
		
		// calculate local and future cost
		cost = 0.0;
		SMLRuntimeState state = (SMLRuntimeState)t.getTargetState();
		for(int i = 0; i < costs.size(); i++) {
			Subgraph costGraph = costs.get(i);
			if(costGraph.terminatingTransitions.contains(t) || costGraph.states.contains(state)) {
				cost += costGraph.getCost();
				for(Set<Integer> set : gainsToOverlappingCosts.values()) {
					set.add(i);
				}
				permanentCompensatedCosts.put(i, costGraph.getCost());
			}			
		}
		
		effectiveCost = cost - getPossibleCostCompensation();
	}
	
	TransitionCost(TransitionCost other, Transition t) {
		this.t = t;
		cost = other.cost;
		costs = other.costs;
		gains = other.gains;
		
		// copy tracked gains/compensated costs
		gainsToOverlappingCosts = new HashMap<Integer, Set<Integer>>();
		for(Entry<Integer, Set<Integer>> entry : other.gainsToOverlappingCosts.entrySet()) {
			gainsToOverlappingCosts.put(entry.getKey(), new HashSet<Integer>(entry.getValue()));
		}
		Assert.isTrue(gainsToOverlappingCosts.size() == other.gainsToOverlappingCosts.size());
		permanentCompensatedCosts = new HashMap<Integer, Double>();
		permanentCompensatedCosts.putAll(other.permanentCompensatedCosts);
		Assert.isTrue(permanentCompensatedCosts.size() == other.permanentCompensatedCosts.size());
		
		// reduce cost according to gains which 'end' now
		Set<Integer> toRemove = new HashSet<Integer>();
		for(Entry<Integer, Set<Integer>> entry : gainsToOverlappingCosts.entrySet()) {
			Subgraph gain = gains.get(entry.getKey());
			if(gain.initializingTransitions.contains(t)) {
				toRemove.add(entry.getKey());
				double availableGain = -gain.getCost();
				Set<Integer> overlappingCosts = new HashSet<Integer>(entry.getValue());
				
				while(!overlappingCosts.isEmpty() && availableGain > 0.0) {
					// use heuristic to find best cost to compensate
					int costIndex = getLeastOccurringIndex(overlappingCosts);
					overlappingCosts.remove(costIndex);
					double actualCost = permanentCompensatedCosts.get(costIndex);
					
					// compensate cost
					if(actualCost > availableGain) {
						permanentCompensatedCosts.put(costIndex, actualCost - availableGain);
						availableGain = 0.0;
					} else {
						availableGain -= actualCost;
						permanentCompensatedCosts.put(costIndex, 0.0);
						Assert.isTrue(permanentCompensatedCosts.containsKey(costIndex));
					}
				}
				
				cost -= (-(gain.getCost() + availableGain));
				Assert.isTrue(cost >= 0.0);
			}
		}
		for(Integer i : toRemove) {
			gainsToOverlappingCosts.remove(i);
		}

		// add local costs
		for(int i = 0; i < costs.size(); i++) {
			Subgraph costGraph = costs.get(i);
			if(costGraph.terminatingTransitions.contains(t)) {
				cost += costGraph.getCost();
				for(Set<Integer> set : gainsToOverlappingCosts.values()) {
					set.add(i);
				}
				permanentCompensatedCosts.put(i, costGraph.getCost());
			}
		}
		
		// track new gains which 'begin' now
		Set<Integer> currentlyActiveCosts = new HashSet<Integer>();
		for(int i = 0; i < costs.size(); i++) {
			if(costs.get(i).states.contains(t.getSourceState()))
				currentlyActiveCosts.add(i);
		}
		for(int i = 0; i < gains.size(); i++) {
			if(gainsToOverlappingCosts.containsKey(i))
				continue;
			
			Subgraph gain = gains.get(i);
			if(gain.terminatingTransitions.contains(t)) {				
				gainsToOverlappingCosts.put(i, new HashSet<Integer>(currentlyActiveCosts));
			}
		}		
		
		effectiveCost = cost - getPossibleCostCompensation();
	}
	
	private double getPossibleCostCompensation() {
		double result = 0.0;
		remainingPotentialGain = 0.0;
		Map<Integer, Double> compensatedCosts = new HashMap<Integer, Double>();
		compensatedCosts.putAll(permanentCompensatedCosts);
		Assert.isTrue(compensatedCosts.size() == permanentCompensatedCosts.size());
		
		for (Entry<Integer, Set<Integer>> entry : gainsToOverlappingCosts.entrySet()) {
			int gainIndex = entry.getKey();
			Subgraph gain = gains.get(gainIndex);
			double availableGain = -gain.getCost();
			Set<Integer> overlappingCosts = new HashSet<Integer>(entry.getValue());
			
			while(!overlappingCosts.isEmpty() && availableGain > 0.0) {
				// use heuristic to find best cost to compensate
				int costIndex = getLeastOccurringIndex(overlappingCosts);
				overlappingCosts.remove(costIndex);
				double actualCost = compensatedCosts.get(costIndex);
				
				// compensate cost
				if(actualCost > availableGain) {
					compensatedCosts.put(costIndex, actualCost - availableGain);
					availableGain = 0.0;
				} else {
					availableGain -= actualCost;
					compensatedCosts.put(costIndex, 0.0);
				}
			}
			
			remainingPotentialGain += availableGain;
			result += (-(gain.getCost() + availableGain));
		}
		
		return result;
	}
	
	private int getLeastOccurringIndex(Set<Integer> candidates) {
		int result = -1;
		int resultOccurrences = Integer.MAX_VALUE;
		
		for(Integer i : candidates) {
			int occurrences = 0;
			for(Set<Integer> set : gainsToOverlappingCosts.values()) {
				if(set.contains(i))
					occurrences += 1;
			}
			
			if(occurrences < resultOccurrences) {
				result = i;
				resultOccurrences = occurrences;
			}
		}
		
		Assert.isTrue(result >= 0);
		return result;
	}
}
