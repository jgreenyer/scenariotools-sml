/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     
 * Author:
 * 		Joel Greenyer
 *		Daniel Gritzner
 */
package org.scenariotools.synthesis.cost;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

//import org.apache.log4j.Logger;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.synthesis.gr1.gamesolving.BuechiCondition;
import org.scenariotools.synthesis.gr1.gamesolving.GR1Condition.Player;
//import org.scenariotools.synthesis.Activator;
import org.scenariotools.synthesis.gr1.strategyextraction.AttractorStrategy;
import org.scenariotools.synthesis.gr1.util.SynthesisUtil;
import org.scenariotools.sml.runtime.Transition;

public class OptimizedAttractorStrategyFactory extends AttractorStrategy.Factory {
//	private static Logger logger = Activator.getLogManager().getLogger(OptimizedAttractorStrategy.class.getName());

	private OrderedMovesMap moves;
	
	public OptimizedAttractorStrategyFactory(OrderedMovesMap moves) {
		this.moves = moves;
	}
	
	@Override
	public AttractorStrategy create(BuechiCondition goalCondition) {
		return new OptimizedAttractorStrategy(create(), goalCondition).attractorStrategy;
	}

	private class OptimizedAttractorStrategy {
		private AttractorStrategy attractorStrategy;
		private Set<SMLRuntimeState> remainingStates = new HashSet<SMLRuntimeState>();
		
		private OptimizedAttractorStrategy(AttractorStrategy attractorStrategy, BuechiCondition goalCondition) {
			attractorStrategy.goalStates = goalCondition.goalStates;
			attractorStrategy.attractorStates = goalCondition.attractorStates;
			
			for(SMLRuntimeState state : attractorStrategy.attractorStates) {
				if(SynthesisUtil.isControllable(state, goalCondition.player))
					remainingStates.add(state);
			}
			
			while(!remainingStates.isEmpty()) {
				SMLRuntimeState state = remainingStates.iterator().next();
				dfs(state, goalCondition.player);
			}
		}
		
		private void dfs(SMLRuntimeState initialState, Player player) {
			if(attractorStrategy.goalStates.contains(initialState)) {
				addMove(initialState, moves.getBestMove(initialState));
				return;
			}
					
			Stack<List<Transition>> stack = new Stack<List<Transition>>();
			pushToStack(stack, new ArrayList<Transition>(), initialState);

			outer:
			while(!stack.isEmpty()) {
				List<Transition> path = stack.pop();
				SMLRuntimeState last = path.get(path.size()-1).getTargetState();
				
				for(int i = 0; i < path.size(); i++) {
					if(path.get(i).getSourceState() == last)
						continue outer; // cycle detected
				}
				
				if(attractorStrategy.move.containsKey(last) || attractorStrategy.goalStates.contains(last)) {
					for(int i = 0; i < path.size(); i++) {
						Transition t = path.get(i);
						SMLRuntimeState state = t.getSourceState();
						if(SynthesisUtil.isControllable(state, player)) {
							addMove(state, t);
						}
					}
					return;
				}

				pushToStack(stack, path, last);
			}
		}
		
		private void addMove(SMLRuntimeState state, Transition t) {
			attractorStrategy.move.put(state, t);
			remainingStates.remove(state);
		}
		
		private void pushToStack(Stack<List<Transition>> stack, List<Transition> path, SMLRuntimeState state) {
			List<Transition> orderedMoves = moves.getOrderedMoves(state);
			Collections.reverse(orderedMoves); // necessary so that they are pop'ed from the stack in the proper order

			for(Transition t : orderedMoves) {
				if(!attractorStrategy.attractorStates.contains(t.getTargetState()))
					continue;
				
				List<Transition> next = new ArrayList<Transition>(path);
				next.add(t);
				stack.push(next);
			}
		}
	}
}
