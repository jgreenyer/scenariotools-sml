package org.scenariotools.synthesis.cost;

import org.eclipse.core.runtime.IProgressMonitor;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.synthesis.AbstractSynthesisAlgorithm;
import org.scenariotools.synthesis.ResultMetaData;
import org.scenariotools.synthesis.gr1.GR1Synthesis;
import org.scenariotools.synthesis.gr1.strategyextraction.AttractorStrategy;
import org.scenariotools.synthesis.gr1.strategyextraction.AttractorStrategyCalculator;
import org.scenariotools.synthesis.gr1.strategyextraction.StrategyExtractor;

/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     
 *
 * Synthesizes the most cost-optimized strategy that fulfils a given
 * specification (if possible at all).
 */
public class CostOptimizedSynthesis extends AbstractSynthesisAlgorithm {
	@ResultMetaData(name="state graph exploration [ms]", group="GR(1) Synthesis Time Measurements", position=1)
	public long stateSpaceExplorationTime;

	@ResultMetaData(name="acceptance sets calculation [ms]", group="GR(1) Synthesis Time Measurements", position=2)
	public long acceptanceSetCalculationTime;
	
	@ResultMetaData(name="GR(1) game solving [ms]", group="GR(1) Synthesis Time Measurements", position=3)
	public long gameSolvingTime;
	
	@ResultMetaData(name="total [ms]", group="GR(1) Synthesis Time Measurements", position=4)
	public long totalGR1SynthesisTime;
	
	@ResultMetaData(name="gather optimization sets time [ms]", group="Transition Cost Calculation Time Measurements", position=1)
	public long gatherOptimizationSetsTime;
	
	@ResultMetaData(name="split optimization sets time [ms]", group="Transition Cost Calculation Time Measurements", position=2)
	public long splitOptimizationSetsTime;
	
	@ResultMetaData(name="propagate costs time [ms]", group="Transition Cost Calculation Time Measurements", position=3)
	public long propagateCostsTime;
	
	@ResultMetaData(name="total transition cost calculation time [ms]", group="Transition Cost Calculation Time Measurements", position=4)
	public long totalTransitionCostCalculationTime;
	
	@ResultMetaData(name="strategy extraction time [ms]", group="Transition Cost Calculation Time Measurements", position=5)
	public long strategyExtractionTime;
	
	@ResultMetaData(name="total [ms]", group="Total Synthesis Time Measurements", position=1)
	public long totalTime;

	@Override
	public Object synthesize(Configuration configuration, IProgressMonitor monitor) {
		long[] times = new long[] {0, 0, 0, 0};
		times[0] = System.currentTimeMillis();
		
		GR1Synthesis gr1Synthesis = new GR1Synthesis();
		gr1Synthesis.synthesize(configuration, monitor); // will return 'gr1Synthesis' as result object
		stateGraph = gr1Synthesis.stateGraph;
		strategyExists = gr1Synthesis.strategyExists;
		times[1] = System.currentTimeMillis();
		
		if(!strategyExists || monitor.isCanceled()) {
			times[3] = times[1];
			return finalize(gr1Synthesis, null, times);
		}
		
		// calc costs for alternatives/transitions
		TransitionCostCalculator costCalculator = TransitionCostCalculator.calculate(stateGraph, monitor);
		times[2] = System.currentTimeMillis();
		
		// extract strategy
		AttractorStrategy.Factory strategyFactory = new OptimizedAttractorStrategyFactory(costCalculator.moves);
		strategy = StrategyExtractor.extractStrategy(stateGraph, AttractorStrategyCalculator.calculate(stateGraph, gr1Synthesis.winningCondition, gr1Synthesis.winningStates, gr1Synthesis.losingStates, true, strategyFactory));
		times[3] = System.currentTimeMillis();
		
//		costCalculator.moves.addDebugInformation();
		
		return finalize(gr1Synthesis, costCalculator, times);		
	}
	
	private Object finalize(GR1Synthesis gr1Synthesis, TransitionCostCalculator costCalculator, long[] times) {
		stateSpaceExplorationTime = gr1Synthesis.stateGraphExplorationTime;
		gameSolvingTime = gr1Synthesis.gameSolvingTime;
		totalGR1SynthesisTime = times[1] - times[0];
		
		if(costCalculator != null) {
			gatherOptimizationSetsTime = costCalculator.gatherOptimizationSetsTime;
			splitOptimizationSetsTime = costCalculator.splitOptimizationSetsTime;
			propagateCostsTime = costCalculator.propagateCostsTime;
			totalTransitionCostCalculationTime = times[2] - times[1];
			
			strategyExtractionTime = times[3] - times[2];
		}
		
		totalTime = times[3] - times[0];
		
		return this;
	}
}
