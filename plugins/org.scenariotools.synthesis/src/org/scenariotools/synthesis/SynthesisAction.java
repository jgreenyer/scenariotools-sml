/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Mohammed Abed
 */
package org.scenariotools.synthesis;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.PluginAction;
import org.osgi.framework.Bundle;
import org.scenariotools.sml.runtime.configuration.Configuration;

public class SynthesisAction implements IObjectActionDelegate {	
	private static Logger logger = Activator.getLogManager().getLogger(SynthesisAction.class.getName());

	@SuppressWarnings("restriction")
	@Override
	public void run(IAction action) {
		String algorithmClassName = action.getId();
		boolean benchmarkMode = false;
		if(algorithmClassName.endsWith("-benchmark")) {
			algorithmClassName = algorithmClassName.substring(0, algorithmClassName.length()-10);
			benchmarkMode = true;
		}
		
		try {
			Class<?> synthAlgo = null;
			if(action instanceof PluginAction) {
				Bundle bundle = Platform.getBundle(((PluginAction)action).getPluginId());
				synthAlgo = bundle.loadClass(algorithmClassName);				
			} else {
				synthAlgo = getClass().getClassLoader().loadClass(algorithmClassName);				
			}
			
			IStructuredSelection structuredSelection = (IStructuredSelection)PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().getSelection();
			IFile configurationFile = (IFile)structuredSelection.getFirstElement();

			final ResourceSet resourceSet = new ResourceSetImpl();		
		    resourceSet.getURIConverter().getURIMap().putAll(EcorePlugin.computePlatformURIMap(true));
			Resource configurationResource = resourceSet.getResource(URI.createPlatformResourceURI(configurationFile.getFullPath().toString(), true), true);
			
			Configuration configuration = (Configuration)configurationResource.getContents().get(0);

			Job job = new SynthesisJob(action.getText() + " Synthesis" + (benchmarkMode ? " (Benchmark)" : ""), configuration, synthAlgo, benchmarkMode);
			job.setUser(true);
			job.schedule();
		} catch (ClassNotFoundException e) {
			logger.error("unknown synthesis algorithm: " + algorithmClassName);
//			e.printStackTrace();
		}
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// TODO Auto-generated method stub
	}
}
