/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.statespaceexploration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.DynamicObjectContainer;
import org.scenariotools.sml.runtime.ElementContainer;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;

public class ElementContainerEvaluator {
	Map<Object, Integer> statistic = new HashMap<>();
	ElementContainer container;
	SMLRuntimeStateGraph stategraph;
	public ElementContainerEvaluator(SMLRuntimeStateGraph sg) {
		stategraph = sg;
		container = stategraph.getElementContainer();
	}
	
	public String evaluate() {
		 
		return countActiveScenarios()+"\n"+countDynamicContainers();
	}
	private String countDynamicContainers() {
		for(SMLRuntimeState s : stategraph.getStates()) {
			incrementCount(s.getDynamicObjectContainer());
		}
		int referenceCount = 0;
		for(DynamicObjectContainer c : container.getDynamicObjectContainer()) {
			referenceCount += statistic.get(c);
		}
		int actualCount = container.getDynamicObjectContainer().size();
		return "Dynamic Containers referenced: "+ referenceCount +", actual: "+actualCount+", reduction: "+reduction(referenceCount, actualCount);
	}
	private String countActiveScenarios() {
		for(SMLRuntimeState s : stategraph.getStates()) {
			putToStatistic(s.getActiveScenarios());
		}
		int referenceCount = 0;
		for(ActiveScenario as: container.getActiveScenarios()) {
			referenceCount += statistic.get(as);
		}
		int actualCount =container.getActiveScenarios().size();
		return "Active Scenarios referenced: "+referenceCount+", actual: "+actualCount+", reduction: "+reduction(referenceCount, actualCount);
	}
	private double reduction(int ref, int actual) {
		return (ref-actual)/(ref*1.0d);
	}
	private void putToStatistic(List objects) {
		for(Object o: objects)
			incrementCount(o);
	}
	private int incrementCount(Object o) {
		int count = statistic.getOrDefault(o, 0)+1;
		statistic.put(o, count);
		return count;
	}
}
