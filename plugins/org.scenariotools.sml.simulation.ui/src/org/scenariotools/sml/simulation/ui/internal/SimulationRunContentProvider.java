/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.simulation.ui.internal;

import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.simulation.SimulationRun;
import org.scenariotools.sml.simulation.observer.ISimulationRunListener;
import org.scenariotools.sml.runtime.Transition;

public class SimulationRunContentProvider {
	private SimulationRun simRun;

	public SimulationRunContentProvider(SimulationRun simRun){
		this.simRun = simRun;
	}
	
	
	public EList<SMLRuntimeState> getNodes() {
		EList<SMLRuntimeState> l = new BasicEList<SMLRuntimeState>();
		
		if(simRun == null)
			return l;
		
		l.addAll((Collection<? extends SMLRuntimeState>) simRun.getStates());
		for(Event event : simRun.getEnabeldEvents()){
			NextSMLRuntimeState next = new NextSMLRuntimeState();
			//TODO give some extra info to next
			Transition t = RuntimeFactory.eINSTANCE.createTransition();
			t.setEvent(event);
			t.setSourceState(simRun.getActualState());
			t.setTargetState(next);
			next.getIncomingTransition().add(t);
			l.add(next);
		}
		return l;

	}
	
	public void performStep(MessageEvent event){
		this.simRun.performStep(event);
	}
	
	public void addSimulationRunListener(ISimulationRunListener listener){
		this.simRun.addSimulationRunListener(listener);
	}
	
	public void removeSimulationRunListener(ISimulationRunListener listener){
		this.simRun.removeSimulationRunListener(listener);
	}
}
