/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.simulation.ui.views;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;
import org.scenariotools.sml.simulation.Manager;
import org.scenariotools.sml.simulation.SimulationRunConfiguration;
import org.scenariotools.sml.simulation.observer.ISimulationRunConfigurationListener;
import org.scenariotools.sml.simulation.ui.internal.SimulationRunConfigurationContentProvider;

public class SimulationRunConfigurationView extends ViewPart{
	
	private SimulationRunConfigurationContentProvider model;
	private ListViewer viewer;
	
	private ISimulationRunConfigurationListener simRunConfigListener = new ISimulationRunConfigurationListener() {
		@Override
		public void newSimulationRunConfiguration(SimulationRunConfiguration simulationRunConfiguration) {
			SimulationRunConfigurationView.this.viewer.setInput(SimulationRunConfigurationView.this.model.getSimulationRunConfigurations());
			SimulationRunConfigurationView.this.viewer.refresh();
		}
	};
	
	public SimulationRunConfigurationView(){
		this.model = new SimulationRunConfigurationContentProvider(Manager.getManager());
		this.model.addSimulationRunConfigurationListener(simRunConfigListener);
	}
	
	public SimulationRunConfigurationView(SimulationRunConfigurationContentProvider model){
		this.model = model;
		this.model.addSimulationRunConfigurationListener(simRunConfigListener);
	}
	
	@Override
	public void createPartControl(Composite parent) {
		this.viewer = new ListViewer(parent, SWT.SINGLE);
		

		viewer.setLabelProvider(new LabelProvider() {
			public String getText(Object element) {
				return ((SimulationRunConfiguration) element).getName();
			}
		});

		viewer.setContentProvider(new IStructuredContentProvider() {

			public Object[] getElements(Object inputElement) {
				return ((EList<SimulationRunConfiguration>) inputElement)
						.toArray();
			}

			public void dispose() {
			}

			public void inputChanged(Viewer viewer, Object oldInput,
					Object newInput) {
			}
		});

		viewer.setInput(this.model.getSimulationRunConfigurations());

		viewer.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {
				Object obj = ((IStructuredSelection) event.getSelection())
						.getFirstElement();
				if (obj instanceof SimulationRunConfiguration) {
					SimulationRunConfiguration simRunConfig = (SimulationRunConfiguration) obj;
					simRunConfig.addSimulationRun();
				}

			}
		});
		
		parent.addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				SimulationRunConfigurationView.this.close();
			}
		});
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		
	}
	
	public void close(){
		this.model.removeISimulationRunConfigurationListener(simRunConfigListener);
	}
}
