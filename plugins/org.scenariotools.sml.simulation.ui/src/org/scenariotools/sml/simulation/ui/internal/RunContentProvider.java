/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.simulation.ui.internal;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.zest.core.viewers.IGraphEntityRelationshipContentProvider;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.Transition;

public class RunContentProvider extends ArrayContentProvider implements
		IGraphEntityRelationshipContentProvider {

	@Override
	public Object[] getRelationships(Object source, Object destination) {
		EList<Transition> rels = new BasicEList<Transition>();
		if (source instanceof SMLRuntimeState && destination instanceof NextSMLRuntimeState) {
			SMLRuntimeState src = (SMLRuntimeState) source;
			NextSMLRuntimeState dest = (NextSMLRuntimeState) destination;
			for (Transition t : dest.getIncomingTransition()) {
				if (t.getSourceState() != null && t.getSourceState().equals(src)) {
					rels.add(t);
				}
			}
		}else if (source instanceof SMLRuntimeState && destination instanceof SMLRuntimeState) {
			SMLRuntimeState src = (SMLRuntimeState) source;
			SMLRuntimeState dest = (SMLRuntimeState) destination;
			for (Transition t : src.getOutgoingTransition()) {
				if (t.getTargetState() != null && t.getTargetState().equals(dest)) {
					rels.add(t);
				}
			}
		}
		return rels.toArray();
	}

}
