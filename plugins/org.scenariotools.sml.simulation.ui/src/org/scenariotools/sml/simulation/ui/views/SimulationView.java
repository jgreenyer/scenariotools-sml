/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.simulation.ui.views;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabFolder2Adapter;
import org.eclipse.swt.custom.CTabFolderEvent;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.part.ViewPart;
import org.scenariotools.sml.simulation.Manager;
import org.scenariotools.sml.simulation.SimulationRun;
import org.scenariotools.sml.simulation.SimulationRunConfiguration;
import org.scenariotools.sml.simulation.observer.INewSimulationRunListener;
import org.scenariotools.sml.simulation.observer.ISimulationRunConfigurationListener;
import org.scenariotools.sml.simulation.ui.internal.SimulationRunContentProvider;

public class SimulationView extends ViewPart{
	
	private Manager manager;
	private Composite parent;
	private EList<CTabFolder> tabFolders;
	private EList<SimulationRunView> simulationRunViews;
	//TODO private!
	public CTabFolder focusedTabFolder;
	
	private INewSimulationRunListener newSimRunListener = new INewSimulationRunListener() {
		@Override
		public void newSimulationRun(SimulationRun simulationRun) {
			CTabItem tabItem = SimulationView.this.createCTabItem(focusedTabFolder, simulationRun);		
		    focusedTabFolder.setSelection(tabItem);
		}
	};
	
	private ISimulationRunConfigurationListener simRunConfigListener = new ISimulationRunConfigurationListener() {
		@Override
		public void newSimulationRunConfiguration(SimulationRunConfiguration simulationRunConfiguration) {
			simulationRunConfiguration.addNewSimulationRunListener(SimulationView.this.newSimRunListener);
		}
	};
	
	private FocusListener focusListener = new FocusListener() {
		
		@Override
		public void focusLost(FocusEvent e) {
			// TODO Auto-generated method stub
		}
		
		@Override
		public void focusGained(FocusEvent e) {
			CTabFolder focused = (CTabFolder) e.getSource();
			SimulationView.this.focusedTabFolder = focused;
		}
	};
	
	public SimulationView(){
		this.manager = Manager.getManager();
		this.tabFolders = new BasicEList<CTabFolder>();
		this.simulationRunViews = new BasicEList<SimulationRunView>();
		manager.addNewSimulationRunConfigurationListener(simRunConfigListener);
	}
	
	@Override
	public void createPartControl(Composite parent) {

		this.parent = parent;
		// init Tabs
		CTabFolder tabFolder = createCTabFolder();

		for(SimulationRunConfiguration simRunConfig : this.manager.getSimulationRunConfigurations()){
			for(SimulationRun simRun : simRunConfig.getSimulationRuns()){
				createCTabItem(tabFolder, simRun);
			}
		}		
		
		parent.addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				// remove all listeners from the model
				SimulationView.this.manager.removeNewSimulationRunConfigurationListener(simRunConfigListener);
				for(SimulationRunConfiguration simRunConfig : SimulationView.this.manager.getSimulationRunConfigurations()){
					simRunConfig.removeNewSimulationRunListener(newSimRunListener);
				}
				for(SimulationRunView view : SimulationView.this.simulationRunViews){
					view.close();
				}
			}
		});
		
		makeActions();
		contributeToActionBars();
	}

	@Override
	public void setFocus() {
	}
	
	private CTabFolder createCTabFolder(){
		CTabFolder tabFolder = new CTabFolder(this.parent, SWT.NULL);
		this.focusedTabFolder = tabFolder;
		this.tabFolders.add(tabFolder);
		
		tabFolder.addFocusListener(focusListener);
		tabFolder.addCTabFolder2Listener(new CTabFolder2Adapter() {
			
		      public void close(CTabFolderEvent event) {
		    	  SimulationRunView view = (SimulationRunView) event.item.getData("view");
		    	  CTabFolder tabFolder = (CTabFolder) event.getSource();
		    	  SimulationView.this.removeSimulationRunView(view, tabFolder);
		      }
		});
		tabFolder.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				 CTabItem tabItem = (CTabItem) e.item;
				 ((SimulationRunView) tabItem.getData("view")).refresh();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		
		return tabFolder;
	}
	
	private CTabItem createCTabItem(CTabFolder tabFolder, SimulationRun simRun, int position){
		CTabItem tabItem = new CTabItem(tabFolder, SWT.CLOSE, position);
		tabItem.setText(simRun.getName());
	    SimulationRunView view = new SimulationRunView(new SimulationRunContentProvider(simRun), SimulationView.this);
	    this.simulationRunViews.add(view);
	    view.createPartControl(tabFolder);
	    tabItem.setControl(view.getControl());
	    tabItem.setData("view", view);
	    tabItem.setData("simRun", simRun);
	    return tabItem;
	}
	
	private CTabItem createCTabItem(CTabFolder tabFolder, SimulationRun simRun){
		return createCTabItem(tabFolder, simRun, tabFolder.getItemCount());
	}
	
	private void removeSimulationRunView(SimulationRunView view, CTabFolder tabFolder){
		if(tabFolder.getItemCount() == 1 && this.tabFolders.size() > 1){
			simulationRunViews.remove(view);
			view.close();
			this.tabFolders.remove(tabFolder);
			tabFolder.dispose();
			this.focusedTabFolder = tabFolders.get(0);
			parent.layout();
			((SimulationRunView) this.focusedTabFolder.getSelection().getData("view")).refresh();
		}else{
			simulationRunViews.remove(view);
			view.close();
		}
	}
	
	private CTabItem moveTab(CTabItem tabItem, CTabFolder tabFolder, int position){
		
		SimulationRun simRun = (SimulationRun) tabItem.getData("simRun");
		SimulationRunView view = (SimulationRunView) tabItem.getData("view");
		CTabItem movedTabItem = createCTabItem(tabFolder, simRun, position);
		removeSimulationRunView(view, tabItem.getParent());
		tabItem.dispose();
		
		return movedTabItem;
	}

	private CTabItem moveTab(CTabItem tabItem, CTabFolder tabFolder){
		return moveTab(tabItem, tabFolder, tabFolder.getItemCount());
	}
	
	private void splitTabFolder(){
		if(this.tabFolders.size() == 1 
				&& ((CTabFolder)this.tabFolders.get(0)).getItems().length > 1){
			// split
			CTabItem tabItem = ((CTabFolder)this.tabFolders.get(0)).getSelection();
			CTabFolder tabFolder = createCTabFolder();
			CTabItem movedTabItem = moveTab(tabItem, tabFolder);
			tabFolder.setSelection(movedTabItem);
	        parent.layout(true);
			((SimulationRunView) this.tabFolders.get(0).getSelection().getData("view")).refresh();
		}else if(this.tabFolders.size() == 2){
			// merge
			int folderIndexToDelete = (this.tabFolders.indexOf(focusedTabFolder) + 1) % 2;
			int position = (folderIndexToDelete == 0)? 0:this.tabFolders.get(0).getItemCount();
			CTabFolder tabFolderToDelete = this.tabFolders.get(folderIndexToDelete);
			for(CTabItem tabItem : tabFolderToDelete.getItems()){
				moveTab(tabItem, focusedTabFolder, position);
				position++;
			}
		}
	}
	
	
	// Define Actions
	//
	Action toggleSplitTabFolder;
	
	protected void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	protected void makeActions() {

		toggleSplitTabFolder = new Action(
				"Split tab group", Action.AS_PUSH_BUTTON) {
			public void run() {
				SimulationView.this.splitTabFolder();
				if(SimulationView.this.tabFolders.size() == 1){
					this.setToolTipText("Split tab group");
				}else{
					this.setToolTipText("Merge tab groups");
				}
			}
		};
		toggleSplitTabFolder.setToolTipText("Split tab group.");
//		toggleSplitTabFolder.setImageDescriptor(Activator
//				.getImageDescriptor("img/abc.png"));
	}

	protected void fillLocalPullDown(IMenuManager manager) {
		manager.add(toggleSplitTabFolder);
	}

	protected void fillLocalToolBar(IToolBarManager manager) {
		manager.add(toggleSplitTabFolder);
	}
}
