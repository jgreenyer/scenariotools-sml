/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.ui.views;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.debug.SimulationManager;
import org.scenariotools.sml.debug.UserInteractingSimulationAgent;
import org.scenariotools.sml.debug.debug.ScenarioDebugTarget;
import org.scenariotools.sml.debug.listener.IActiveSimulationAgentChangeListener;
import org.scenariotools.sml.debug.listener.IEventListChangeListener;
import org.scenariotools.sml.debug.ui.icons.ImageHelper;

public class MSDModalMessageEventSelectionView extends
		AbstractMSDModalMessageEventsView {

	public static String MODAL_MESSAGE_EVENT_SELECTION_VIEW = "org.scenariotools.sml.debug.ui.views.CondensatedMessageEventSelectionView";

	private SimulationManager currentSimulationManager = null;
	private UserInteractingSimulationAgent currentUserInteractingSimulationAgent = null;

	private Object filterObject; // stores an object (Launch,
									// ScenarioDebugTarget, ActiveMSDThread,
									// ObjectThread) that was selected last

	private ISelectionListener selectionListener = new ISelectionListener() {

		@Override
		public void selectionChanged(IWorkbenchPart part, ISelection selection) {
			if (selection instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selection)
						.getFirstElement();
				if (firstElement instanceof ScenarioDebugTarget) {
					selectionChanged((ScenarioDebugTarget) firstElement);
				}
			}
		}

		private void selectionChanged(ScenarioDebugTarget scenarioDebugTarget) {
			setFilterObject(scenarioDebugTarget);
			setCurrentSimulationManager(scenarioDebugTarget
					.getSimulationManager());
			setInput((Collection<Event>) (Collection<?>) getCurrentUserInteractingSimulationAgent()
					.getSimulationManager().getCurrentSMLRuntimeState()
					.getEnabledEvents());
		}

	};

	/**
	 * This listener is registered at a current simulation agent to be notified
	 * about a changing MSD modal message event list.
	 */
	private IEventListChangeListener msdModalMessageEventListChangeListener = new IEventListChangeListener() {
		@Override
		public void eventsChanged(Collection<Event> events) {
			setInput(events);
			MSDModalMessageEventSelectionView.this.getViewer().refresh();
		}
	};

	/**
	 * this listener is registered at a current simulation manager to be
	 * notified about a changing current simulation agent.
	 */
	private IActiveSimulationAgentChangeListener activeSimulationAgentChangeListener = new IActiveSimulationAgentChangeListener() {

		@Override
		public void activeSimulationAgentChanged(
				UserInteractingSimulationAgent userInteractingSimulationAgent) {
			setCurrentUserInteractingSimulationAgent(userInteractingSimulationAgent);
		}
	};

	/**
	 * This method is called 1. when the simulation manager notifies the view
	 * (through the {@link IActiveSimulationAgentChangeListener}) about a change
	 * of the current simulation agent. 2. if the {@link
	 * setCurrentSimulationManager(SimulationManager)} is called.
	 * 
	 * @param newUserInteractingSimulationAgent
	 */
	protected void setCurrentUserInteractingSimulationAgent(
			UserInteractingSimulationAgent newUserInteractingSimulationAgent) {
		UserInteractingSimulationAgent oldUserInteractingSimulationAgent = currentUserInteractingSimulationAgent;
		if (oldUserInteractingSimulationAgent != null)
			oldUserInteractingSimulationAgent
					.getRegisteredEventListChangeListener().remove(
							msdModalMessageEventListChangeListener);

		newUserInteractingSimulationAgent
				.getRegisteredEventListChangeListener().add(
						msdModalMessageEventListChangeListener);
		currentUserInteractingSimulationAgent = newUserInteractingSimulationAgent;
		setInput((Collection<Event>) (Collection<?>) currentUserInteractingSimulationAgent
				.getSimulationManager().getCurrentSMLRuntimeState()
				.getEnabledEvents());
	}

	protected UserInteractingSimulationAgent getCurrentUserInteractingSimulationAgent() {
		return currentUserInteractingSimulationAgent;
	}

	/**
	 * This method is called when the selection in the debug view has changed.
	 * Calling this method results in re-registering the
	 * activeSimulationAgentChangeListener to that simulation manager and
	 * setting the current user-interacting simulation agent (calling {@link
	 * setCurrentUserInteractingSimulationAgent(UserInteractingSimulationAgent
	 * newUserInteractingSimulationAgent)})
	 * 
	 * @param newSimulationManager
	 */
	protected void setCurrentSimulationManager(
			SimulationManager newSimulationManager) {
		SimulationManager oldSimulationManager = this.currentSimulationManager;

		if (oldSimulationManager != null)
			oldSimulationManager
					.getRegisteredActiveSimulationAgentChangeListener().remove(
							activeSimulationAgentChangeListener);

		newSimulationManager.getRegisteredActiveSimulationAgentChangeListener()
				.add(activeSimulationAgentChangeListener);

		currentSimulationManager = newSimulationManager;

		if (newSimulationManager.getActiveSimulationAgent() instanceof UserInteractingSimulationAgent)
			setCurrentUserInteractingSimulationAgent((UserInteractingSimulationAgent) newSimulationManager
					.getActiveSimulationAgent());

		togglePlayOutExecutionModeAction.setChecked(true);
	}

	protected SimulationManager getCurrentSimulationManager() {
		return currentSimulationManager;
	}

	public void createPartControl(Composite parent) {
		super.createPartControl(parent);
		hookSelectionListener();
	}

	protected void hookSelectionListener() {
		getSite().getWorkbenchWindow().getSelectionService()
				.addSelectionListener(selectionListener);
	}

	private void disposeSelectionListener() {
		getSite().getWorkbenchWindow().getSelectionService()
				.removeSelectionListener(selectionListener);
	}

	protected Object getFilterObject() {
		return filterObject;
	}

	protected void setFilterObject(Object filterObject) {
		this.filterObject = filterObject;
	}

	@Override
	public void setInput(Collection<Event> events) {
//		if (getFilterObject() != null) {
//
//			EList<MessageEvent> filteredMSDModalMessageEvents = new BasicEList<MessageEvent>();
//
//			if (getFilterObject() instanceof Launch) {
//				Launch launch = (Launch) getFilterObject();
//				if (launch.getDebugTarget() instanceof ScenarioDebugTarget
//						&& !launch.isTerminated()) {
//					filteredMSDModalMessageEvents.addAll(msdModalMessageEvents);
//				}
//			} else if (getFilterObject() instanceof ScenarioDebugTarget) {
//				if (!((ScenarioDebugTarget) getFilterObject()).isTerminated())
//					filteredMSDModalMessageEvents
//							.addAll(getRequirementExecutedOrEnvironmentMSDModalMessageEventsFromList(msdModalMessageEvents));
//			} else if (getFilterObject() instanceof AbstractScenarioThread
//					&& !((AbstractScenarioThread) getFilterObject()).getDebugTarget()
//							.isTerminated()) {
//				ActiveScenario activeMSD = ((AbstractScenarioThread) getFilterObject())
//						.getActiveProcess();
//				if(activeMSD !=null){
//					for (MessageEvent msdMessageEvent : msdModalMessageEvents) {
//						for (MessageEvent messageEvent : activeMSD
//								.getRequestedEvents()) {
//							filteredMSDModalMessageEvents.add(msdMessageEvent);
//						}
//					}
//				}
//			} else if (getFilterObject() instanceof ObjectThread
//					&& !((ObjectThread) getFilterObject()).getDebugTarget()
//							.isTerminated()) {
//				for (MessageEvent msdModalMessageEvent : msdModalMessageEvents) {
//					if (msdModalMessageEvent
//							.getSendingObject() == ((ObjectThread) getFilterObject())
//							.getSimulationObject()) {
//						filteredMSDModalMessageEvents.add(msdModalMessageEvent);
//					}
//				}
//			}
			super.setInput(events);
			return;
//		}
		// super.setInput(condensatedMessageEvents);
	}

	protected EList<MessageEvent> getRequirementExecutedOrEnvironmentMSDModalMessageEventsFromList(
			Collection<MessageEvent> msdModalMessageEvents) {
		EList<MessageEvent> filteredMSDModalMessageEvents = new UniqueEList<MessageEvent>();
		for (MessageEvent msdModalMessageEvent : msdModalMessageEvents) {
			//MSDModality assumptionsModality = (MSDModality) msdModalMessageEvent.getAssumptionsModality();
//			MSDModality requirementsModality = (MSDModality) msdModalMessageEvent.getRequirementsModality();
//			
//			if (requirementsModality.isMandatory() || getCurrentUserInteractingSimulationAgent().getSimulationManager().getCurrentSMLRuntimeState().isEnvironmentMessageEvent(msdModalMessageEvent.getRepresentedMessageEvent())){
//				filteredMSDModalMessageEvents.add(msdModalMessageEvent);
//				if (msdModalMessageEvent
//						.getParentSymbolicMSDModalMessageEvent() != null) {
//					filteredMSDModalMessageEvents.add(msdModalMessageEvent
//							.getParentSymbolicMSDModalMessageEvent());
//				}
//			}
		}
		return filteredMSDModalMessageEvents;
	}

	@Override
	protected void objectDoubleClicked(
			Object obj) {
		if (obj instanceof Event){
			getCurrentUserInteractingSimulationAgent().setNextEvent(((Event) obj));
		}
		Display.getCurrent().asyncExec(new Runnable() {
			@Override
			public void run() {
				MSDModalMessageEventSelectionView.this
						.getCurrentUserInteractingSimulationAgent()
						.getSimulationManager()
						.performNextStepFromSimulationAgent(
								MSDModalMessageEventSelectionView.this
										.getCurrentUserInteractingSimulationAgent());
			}
		});
	}

	protected void refreshCurrentModelMessageEventsList() {
		if (getCurrentSimulationManager() == null
				|| getCurrentSimulationManager().getCurrentSMLRuntimeState() == null)
			return;

		setInput((Collection<Event>) (Collection<?>) getCurrentSimulationManager()
				.getCurrentSMLRuntimeState()
				.getEnabledEvents());
		MSDModalMessageEventSelectionView.this.getViewer().refresh();
	}

	protected void toggleExecutionMode() {
		if (getCurrentSimulationManager() == null)
			return;

		if (togglePlayOutExecutionModeAction.isChecked()) {
			refreshCurrentModelMessageEventsList();
		} else {
			refreshCurrentModelMessageEventsList();
		}
	}

	Action togglePlayOutExecutionModeAction;

	@Override
	protected void makeActions() {
		super.makeActions();

		togglePlayOutExecutionModeAction = new Action(
				"Toggle Play-Out execution mode", Action.AS_CHECK_BOX) {
			public void run() {
				toggleExecutionMode();
			}
		};
		togglePlayOutExecutionModeAction
				.setToolTipText("Toggle between Play-Out and general execution mode (Play-Out: if there are active system events only consider these, otherwise consider all environment events; General: in every state consider all environment and system message events.)");
		togglePlayOutExecutionModeAction.setImageDescriptor(ImageHelper
				.getImageDescriptor("play-out-execution-mode.png"));

	}

	@Override
	protected void fillLocalPullDown(IMenuManager manager) {
		// TODO Auto-generated method stub
		super.fillLocalPullDown(manager);

		manager.add(togglePlayOutExecutionModeAction);
	}

	@Override
	protected void fillLocalToolBar(IToolBarManager manager) {
		super.fillLocalToolBar(manager);

		manager.add(togglePlayOutExecutionModeAction);
	}

	@Override
	public void dispose() {
		disposeSelectionListener();
		super.dispose();
	}
}
