/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.builder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.scenariotools.output.graphviz.DotOutput;
import org.scenariotools.output.graphviz.SMLRuntimeStateFormatter;
import org.scenariotools.output.graphviz.StrategyFormatter;
import org.scenariotools.output.strategy.StrategyTransformation;
import org.scenariotools.sml.Collaboration;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.configuration.Configuration;

public class ScenarioToolsBuilder extends IncrementalProjectBuilder {

	public static final String BUILDER_ID = "org.scenariotools.builder.scenariotoolsBuilder";

	private static final String MARKER_TYPE = "org.scenariotools.builder.xmlProblem";
	
	private IProgressMonitor monitor;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.internal.events.InternalBuilder#build(int,
	 * java.util.Map, org.eclipse.core.runtime.IProgressMonitor)
	 */
	protected IProject[] build(int kind, Map args, IProgressMonitor monitor) throws CoreException {
		this.monitor = monitor;
		if (kind == FULL_BUILD) {
			fullBuild(monitor);
		} else {
			IResourceDelta delta = getDelta(getProject());
			if (delta == null) {
				fullBuild(monitor);
			} else {
				incrementalBuild(delta, monitor);
			}
		}
		return null;
	}

	protected void clean(IProgressMonitor monitor) throws CoreException {
		// delete markers set and files created
		getProject().deleteMarkers(MARKER_TYPE, true, IResource.DEPTH_INFINITE);
	}

	/**
	 * 
	 * @param file
	 */
	void checkFile(IFile file) {
		if (isStategraph(file) && !monitor.isCanceled()) {
			ResourceSet resourceSet = new ResourceSetImpl();
			resourceSet.getURIConverter().getURIMap().putAll(EcorePlugin.computePlatformURIMap(true));
			final IFolder outFolder = BuilderUtil.getGenDirectory(file);
			final String strategyFileName = file.getName();
			final IFile collaborationFile = outFolder.getFile(strategyFileName + ".collaboration");
			final IFile instanceModelFile = outFolder.getFile(strategyFileName + "Instancemodel.xmi");
			final IFile runConfigurationFile = outFolder.getFile(strategyFileName + ".runconfig");

			URI strategyURI = BuilderUtil.getPlatformURIForFile(file);
			Resource strategyResource = resourceSet.createResource(strategyURI);
			try {
 				strategyResource.load(Collections.EMPTY_MAP);

 				SMLRuntimeStateGraph stategraph = (SMLRuntimeStateGraph) strategyResource.getContents().get(0);
				// output dot-graph markup. TODO: offer settings whether to
				// generate or not.
				createDotOutput(file, stategraph);
				// transform strategies into controller-scenarios.
				if (isStrategy(file)) {
					try {
//						StrategyToCollaborationTransformation t = new StrategyToCollaborationTransformation();
//						t.transform((RuntimeStateGraph) stategraph);
//						t.getCollaboration()
						StrategyTransformation t = new StrategyTransformation(BuilderUtil.getPlatformURIForFile(outFolder.getFile("dummy.txt")),stategraph);
						writeTextFile(collaborationFile, t.getOutput());
						Resource collaborationResource = resourceSet.getResource(URI.createPlatformResourceURI(collaborationFile.getFullPath().toString(),false), true);
						Configuration conf = stategraph.getConfiguration();
						Resource configResource = resourceSet.createResource(URI.createPlatformResourceURI(runConfigurationFile.getFullPath(). toString(),false));
						configResource.getContents().add(t.getConfiguration(conf,(Collaboration) collaborationResource.getContents().get(0)));
						configResource.save(Collections.EMPTY_MAP);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (IOException e) {
				// TODO logging
				e.printStackTrace();
			}

		}
	}
	
	private void createDotOutput(IFile f, SMLRuntimeStateGraph g) {
		final DotOutput dotOutput = new DotOutput();
		CharSequence content = "<error during build>.";
		if (isStrategy(f)) {
			content = dotOutput.output(g, new StrategyFormatter());
		} else {
			// default: use "verbose" output.
			content = dotOutput.output(g, new SMLRuntimeStateFormatter());
		}
		try {
			writeTextFile(BuilderUtil.getGenDirectory(f).getFile(f.getName()).getLocation().addFileExtension("dot").toFile(), content);
		} catch (IOException e) {
			// TODO logging
			e.printStackTrace();
		}
	}

	private boolean isStategraph(IFile file) {
		final String extension = file.getLocation().getFileExtension();
		return "strategy".equals(extension) || "stategraph".equals(extension);
	}

	@Deprecated
	private void writeTextFile(IFile file, CharSequence content) throws IOException {
		final File f = file.getLocation().toFile();
		writeTextFile(f, content);
	}
	private void writeTextFile(File f, CharSequence content) throws IOException{
		FileWriter writer = new FileWriter(f);
		writer.append(content);
		writer.close();
	}

	private boolean isStrategy(IFile file) {
		return "strategy".equals(file.getFileExtension());
	}

	protected void fullBuild(final IProgressMonitor monitor) throws CoreException {
		this.monitor = monitor;
		try {
			getProject().accept(new SampleResourceVisitor());
		} catch (CoreException e) {
		}
	}

	protected void incrementalBuild(IResourceDelta delta, IProgressMonitor monitor) throws CoreException {
		this.monitor = monitor;

		// the visitor does the work.
		delta.accept(new SampleDeltaVisitor());
	}

	class SampleDeltaVisitor implements IResourceDeltaVisitor {
		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.core.resources.IResourceDeltaVisitor#visit(org.eclipse.
		 * core.resources.IResourceDelta)
		 */
		public boolean visit(IResourceDelta delta) throws CoreException {
			IResource resource = delta.getResource();
			if (resource instanceof IFile) {
				IFile file = (IFile) resource;
				switch (delta.getKind()) {
				case IResourceDelta.ADDED:
					// handle added resource
					checkFile(file);
					break;
				case IResourceDelta.REMOVED:
					// handle removed resource
					break;
				case IResourceDelta.CHANGED:
					// handle changed resource
					checkFile(file);
					break;
				}
			}
			// return true to continue visiting children.
			return true;
		}
	}

	class SampleResourceVisitor implements IResourceVisitor {
		public boolean visit(IResource resource) {
			if (resource instanceof IFile)
				checkFile((IFile) resource);
			// return true to continue visiting children.
			return true;
		}
	}
}
