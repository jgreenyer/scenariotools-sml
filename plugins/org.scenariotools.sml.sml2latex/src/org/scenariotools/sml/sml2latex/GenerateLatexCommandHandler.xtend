/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.sml.sml2latex

import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.net.URL
import org.eclipse.core.commands.AbstractHandler
import org.eclipse.core.commands.ExecutionEvent
import org.eclipse.core.commands.ExecutionException
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IFolder
import org.eclipse.core.resources.IProject
import org.eclipse.core.resources.IResource
import org.eclipse.core.resources.IWorkspace
import org.eclipse.core.resources.IWorkspaceRoot
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.IPath
import org.eclipse.ui.IEditorDescriptor
import org.eclipse.ui.IWorkbenchPage
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.part.FileEditorInput
import org.scenariotools.sml.sml2latex.converter.SelectionToLatexConverter
import org.scenariotools.sml.sml2latex.generator.SmlToLatexGenerator

class GenerateLatexCommandHandler extends AbstractHandler {

	private val static final FILE_EXTENSION_LATEX = "tex"
	private val static final DESTINATION_FOLDER_NAMEFILE = "latex-gen"
	private val static final MSC_FILE_NAME = "msc.sty"
	private val static final MSC_FILE_PATH = "/mscpackage/".concat(MSC_FILE_NAME)

	override Object execute(ExecutionEvent event) throws ExecutionException {
		// Load selected sml resource
		val IFile smlFile = SelectionToLatexConverter.selectedSmlFile
		val sml = SelectionToLatexConverter.selectedSpecification

		// Create path to new tex file
		var path = smlFile.projectRelativePath
		path = path.removeLastSegments(1)
		val IProject project = smlFile.project
		val IFolder destinationFolder = project.getFolder(path).getFolder(DESTINATION_FOLDER_NAMEFILE)
		if (!destinationFolder.exists()) {
			destinationFolder.create(IResource.NONE, true, null);
		}
		val destFileName = smlFile.name.substring(0, smlFile.name.lastIndexOf("."))
		val IFile file = destinationFolder.getFile(destFileName + "." + FILE_EXTENSION_LATEX)
		var IPath p = file.fullPath

		// Create sml file and copy msc package file
		try {
			if (p !== null) {
				// create sml file
				if (!(FILE_EXTENSION_LATEX).equalsIgnoreCase(p.getFileExtension()))
					p = p.addFileExtension(FILE_EXTENSION_LATEX)
				val IWorkspace workspace = ResourcesPlugin.getWorkspace();
				val IWorkspaceRoot root = workspace.getRoot();
				val IFile outputFile = root.getFile(p)
				val is = new ByteArrayInputStream(
					SmlToLatexGenerator.generateLatexFileFromSmlSpecification(sml).toString.getBytes())
				if (outputFile.exists) {
					outputFile.setContents(is, true, false, null)
				} else {
					outputFile.create(is, true, null)
				}
				openFile(outputFile)
				// Create path for msc package file
				val IFile mscFile = destinationFolder.getFile(MSC_FILE_NAME)
				if (!mscFile.exists) {
					// load file msc package file
					val URL mscFileURL = Activator.^default.bundle.getResource(MSC_FILE_PATH)
					val InputStream mscFileIs = mscFileURL.openStream

					var ByteArrayOutputStream baos = new ByteArrayOutputStream()
					var byte[] buffer = newByteArrayOfSize(1024)
					var int len
					while ((len = mscFileIs.read(buffer)) > -1) {
						baos.write(buffer, 0, len)
					}
					baos.flush()
					val mscFileIsBais = new ByteArrayInputStream(baos.toByteArray)

					mscFile.create(mscFileIsBais, true, null)
					baos.close()
					mscFileIs.close()
				}
			}
		} catch (Exception exception) {
			exception.printStackTrace
		}
		return null
	}

	def static openFile(IFile file) {
		val IWorkbenchPage page = PlatformUI.getWorkbench().activeWorkbenchWindow.activePage
		val IEditorDescriptor desc = PlatformUI.getWorkbench().getEditorRegistry().getDefaultEditor(file.name);
		if (desc != null) {
			page.openEditor(new FileEditorInput(file), desc.getId());
		}
	}
}
