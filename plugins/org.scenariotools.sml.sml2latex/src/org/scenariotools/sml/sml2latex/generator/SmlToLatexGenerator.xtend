/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.sml.sml2latex.generator

import java.util.HashSet
import java.util.Locale
import java.util.Set
import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.common.util.BasicEMap
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.common.util.EMap
import org.eclipse.xtext.nodemodel.impl.CompositeNodeWithSemanticElement
import org.eclipse.xtext.nodemodel.impl.LeafNode
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.scenariotools.sml.Alternative
import org.scenariotools.sml.BindingExpression
import org.scenariotools.sml.Case
import org.scenariotools.sml.Collaboration
import org.scenariotools.sml.Condition
import org.scenariotools.sml.ConditionExpression
import org.scenariotools.sml.ConstraintBlock
import org.scenariotools.sml.ExpectationKind
import org.scenariotools.sml.Interaction
import org.scenariotools.sml.InteractionFragment
import org.scenariotools.sml.InterruptCondition
import org.scenariotools.sml.Loop
import org.scenariotools.sml.Message
import org.scenariotools.sml.ModalMessage
import org.scenariotools.sml.Parallel
import org.scenariotools.sml.ParameterBinding
import org.scenariotools.sml.Role
import org.scenariotools.sml.RoleBindingConstraint
import org.scenariotools.sml.Scenario
import org.scenariotools.sml.Specification
import org.scenariotools.sml.VariableFragment
import org.scenariotools.sml.ViolationCondition
import org.scenariotools.sml.WaitCondition
import org.scenariotools.sml.expressions.scenarioExpressions.TypedVariableDeclaration
import org.scenariotools.sml.expressions.scenarioExpressions.VariableAssignment
import org.scenariotools.sml.expressions.scenarioExpressions.VariableExpression
import org.scenariotools.sml.utility.SpecificationUtil
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EClass
import org.scenariotools.sml.ConditionFragment

class SmlToLatexGenerator {

	private static EMap<Collaboration, EMap<Scenario, String>> latexCodeSpecification
	private static EMap<Collaboration, EMap<Scenario, EList<Role>>> actorsSpecification
	private static EMap<Scenario, EList<String>> bindingConstraintsSpecification
	private static EMap<Collaboration, String> latexRolesCodeSpecification

	private static String NEWLINE = System.getProperty("line.separator")

	private static int MAX_BLOCK_CHILDREN_COUNT = 10

	private final static String ALT_PREFIX_ID = "a"
	private final static String LOOP_PREFIX_ID = "w"
	private final static String PARALLEL_PREFIX_ID = "p"
	private final static String CONSTRAINTS_PREFIX_ID = "c"
	private final static String INTERACTION_PREFIX_ID = "i"

	private final static double INLINE_OVERLAP_MIN_VALUE = 0.5
	private final static double INLINE_OVERLAP_DEFAULT_VALUE = 1

	private final static double CONDITION_OVERLAP_MIN_VALUE = 0
	private final static double CONDITION_OVERLAP_DEFAULT_VALUE = 0.5

	private final static double NESTED_MARGIN_LEFT_RIGHT = 0.1

	private final static String FORBIDDEN = "forbidden"
	private final static String INTERRUPT = "interrupt"
	// private final static String CONSIDER = "consider"
	// private final static String IGNORE = "ignore"
	private final static String STATIC = "static"
	private final static String DYNAMIC = "dynamic"
	private final static String ROLE = "role"

	public def static generateLatexFileFromSmlSpecification(Specification smlSpecification) {

		var Set<String> controllablesEClasses = new HashSet()
		for (controllable : smlSpecification.controllableEClasses) {
			controllablesEClasses.add(controllable.name)
		}
		var Set<String> unControllablesEClasses = new HashSet()
		for (uncontrollable : SpecificationUtil.getAllUncontrollableClasses(smlSpecification)) {
			unControllablesEClasses.add(uncontrollable.name)
		}

		latexCodeSpecification = new BasicEMap()
		actorsSpecification = new BasicEMap()
		bindingConstraintsSpecification = new BasicEMap()
		latexRolesCodeSpecification = new BasicEMap()

		val EList<Collaboration> collaborations = smlSpecification.collaborations
		for (var int iCollaboration = 0; iCollaboration < collaborations.size; iCollaboration++) {
			var EMap<Scenario, String> latexCodeCollaboration = new BasicEMap()
			var EMap<Scenario, EList<Role>> actorsCollaboration = new BasicEMap()

			val Collaboration collaboration = collaborations.get(iCollaboration)
			if (collaboration.roles != null) {
				var String rolesText = ""
				for (var int iRole = 0; iRole < collaboration.roles.size; iRole++) {
					var Role role = collaboration.roles.get(iRole)
					var String roleText = 
					'''\textbf{«IF role.static»«STATIC»«ELSE»«DYNAMIC»«ENDIF» «IF role.multiRole»multi«ENDIF» «ROLE»} «role.type.name» «role.name»\\«NEWLINE»'''
					rolesText = rolesText.concat(roleText)
				}
				latexRolesCodeSpecification.put(collaboration, rolesText)
			}

			val EList<Scenario> scenarios = collaboration.scenarios
			for (var int iScenario = 0; iScenario < scenarios.size; iScenario++) {
				val Scenario scenario = scenarios.get(iScenario)
				// retrieve bindings
				val EList<RoleBindingConstraint> bindingConstraints = scenario.roleBindings
				if (bindingConstraints != null) {
					var EList<String> listBindingScenario = new BasicEList()
					for (binding : bindingConstraints) {
						val role = binding.role
						val bindExp = binding.bindingExpression
						val String bindExpText = "\\newline\\textbf{bind} ".concat(role.name).concat(" \\textbf{to} ").
							concat(NodeModelUtils.getNode(bindExp).getText().trim())
						listBindingScenario.add(bindExpText)
					}

					bindingConstraintsSpecification.put(scenario, listBindingScenario)
				}
				// retrieve all actors from the current scenario
				var EList<Role> scenarioActors = new BasicEList()
				val modalMessages = scenario.eAllContents.filter(s|s instanceof Message)
				while (modalMessages.hasNext) {
					var Message modalMsg = modalMessages.next as Message
					if (!scenarioActors.contains(modalMsg.sender)) {
						scenarioActors.add(modalMsg.sender)
					}
					if (!scenarioActors.contains(modalMsg.receiver)) {
						scenarioActors.add(modalMsg.receiver)
					}
				}
				actorsCollaboration.put(scenario, scenarioActors)
				// retrieve latexcode from the current scenario
				val String latexScenario = SmlToLatexGenerator.convertScenarioToLatex(scenario, scenarioActors)
				latexCodeCollaboration.put(scenario, latexScenario)
			}
			actorsSpecification.put(collaboration, actorsCollaboration)
			latexCodeSpecification.put(collaboration, latexCodeCollaboration)
		}
		'''
			\documentclass[a4paper,9pt]{extarticle}
			
			\usepackage{msc}
			
			\begin{document}
			
			\title{«smlSpecification.name.replaceAll("_", "\\\\_")»}
			
			\maketitle
			
			\noindent
			\textbf{Controllable Classes:}
			«getControllableClassesText(smlSpecification)»

			\noindent
			\textbf{Non-Spontaneous Events:}
			«getNonSpontaneousEventsText(smlSpecification)»

			«FOR j : 0..< collaborations.size»
				«var Collaboration collaboration = collaborations.get(j)»
				\newpage
				\section{Collaboration: «collaboration.name.replaceAll("_", "\\\\_")»}
				
				«extractComment(collaboration)»
				
				\noindent
				«latexRolesCodeSpecification.get(collaboration)»
				
				«FOR i : 0..< collaboration.scenarios.size»
					«var Scenario scenario = collaboration.scenarios.get(i)»
					«val mscName = scenario.name.replaceAll("_", "\\\\_")»
					\subsection{«mscName»}

					«extractComment(scenario)»
					
					«val EList<String> bindings = bindingConstraintsSpecification.get(scenario)»
					«val msckeyword = scenario.kind.literal.concat(" scenario")»
					«var EList<Role> scenarioActors = actorsSpecification.get(collaboration).get(scenario)»
					«val lineCountBindings = Math.ceil((mscName.concat(msckeyword).length / (30.0 * (scenarioActors.size))))»
					«val double topDistance = (0.8 * lineCountBindings + (0.5) * bindings.size) + 0.2»
					«val String bindStr = String.join("", bindings)»
					«val String topDistanceStr = String.format(Locale.ENGLISH, "%1$,.2fcm", topDistance)»
					\begin{msc}[head top distance=«topDistanceStr», msc keyword=«msckeyword», instance distance=2.5cm, first level height=0.7cm, level height=0.5cm, label distance=0.00cm]{«mscName.concat(bindStr)»}
					«FOR actor : scenarioActors»
						«actor.compileActor»
					«ENDFOR»
					«latexCodeSpecification.get(collaboration).get(scenario)»
					\end{msc}
				«ENDFOR»
			«ENDFOR»
			\end{document}
		'''
	}
	
	def static getNonSpontaneousEventsText(Specification specification) {
		return 
		'''
		\begin{itemize}
			«FOR eOperation : specification.nonSpontaneousOperations»«NEWLINE»\item «(eOperation.eContainer as EClass).name».«eOperation.name»«ENDFOR»
		\end{itemize}
		'''
	}
	
	def static getControllableClassesText(Specification specification) {
		return 
		'''
		\begin{itemize}
			«FOR eClass : specification.controllableEClasses»«NEWLINE»\item «eClass.name»«ENDFOR»
		\end{itemize}
		'''
	}
	
	def static extractComment(EObject eObject) {
		val nodeText = NodeModelUtils.getNode(eObject).getText()
		if (nodeText.contains("/*")
			&& nodeText.indexOf("/*") < nodeText.indexOf("scenario"))
			return '''«nodeText.substring(nodeText.indexOf("/*")+2, nodeText.indexOf("*/")).replace("*", "")»
			\vspace{9pt}
			'''
	}

	private def static String getLatexFromModalMessage(ModalMessage modalMsg, EList<Role> scenarioActors,
		int indexLeftActor, int indexRightActor) {
		val Role sender = modalMsg.sender
		val Role receiver = modalMsg.receiver
		var String messType = "\\mess"

		if (modalMsg.expectationKind==ExpectationKind.NONE) {
			messType = messType.concat("*")
		}

		messType = messType.concat("[")
		if (modalMsg.isStrict) {
			messType = messType.concat("draw=red")
		} else {
			messType = messType.concat("draw=blue")
		}
		var String side = getSideSelfMessage(sender, receiver, scenarioActors, indexLeftActor, indexRightActor) // side=left|right
		if (side.length > 0) {
			messType = messType.concat(",").concat(side)
		}
		messType = messType.concat("]")
		messType = messType.concat("{")
		
		if (modalMsg.isStrict || modalMsg.expectationKind!=ExpectationKind.NONE){
			messType = messType + '''$\langle «
				IF (modalMsg.isStrict)»strict«ENDIF»«
				IF (modalMsg.isStrict && modalMsg.expectationKind!=ExpectationKind.NONE)», «ENDIF»«
				IF (modalMsg.expectationKind!=ExpectationKind.NONE)»«modalMsg.expectationKind»«ENDIF»\rangle$ '''; 
		}
		
		val String messageName = getMessageName(modalMsg)
		var String latexModalMessage = messType.concat(messageName).concat("}").concat("{").concat(sender.name).
			concat("}").concat("{").concat(receiver.name).concat("}")

		latexModalMessage = latexModalMessage.concat(getEndMessage(sender, receiver))
		return latexModalMessage
	}

	private def static String getLatexFromConstraintMessage(Message constraintsMessage, String constraintType,
		EList<Role> scenarioActors, int indexLeftActor, int indexRightActor) {
		val Role sender = constraintsMessage.sender
		val Role receiver = constraintsMessage.receiver
		var String latexConstraintMsg = "\\mess*"

		var String drawParam = ""
		if (constraintType.equalsIgnoreCase(FORBIDDEN)) {
			drawParam = "draw=red"
		}
		if (constraintType.equalsIgnoreCase(INTERRUPT)) {
			drawParam = "draw=blue"
		}

		var String side = getSideSelfMessage(sender, receiver, scenarioActors, indexLeftActor, indexRightActor) // side=left|right
		var concatParam = ""
		if (drawParam.length > 0) {
			concatParam = concatParam.concat(drawParam)

			if (side.length > 0) {
				concatParam = concatParam.concat(",").concat(side)
			}
		} else {
			concatParam = concatParam.concat(side)
		}
		if (concatParam.length > 0) {
			concatParam = "[".concat(concatParam).concat("]")
			latexConstraintMsg = latexConstraintMsg.concat(concatParam)
		}

		latexConstraintMsg = latexConstraintMsg.concat("{\\emph{").concat(constraintType).concat("} ").concat(
			getMessageName(constraintsMessage) + "}").concat("{" + sender.name + "}").concat("{" + receiver.name + "}")

		latexConstraintMsg = latexConstraintMsg.concat(getEndMessage(sender, receiver))
		return latexConstraintMsg
	}

	private def static String getSideSelfMessage(Role sender, Role receiver, EList<Role> scenarioActors,
		int indexLeftActor, int indexRightActor) {
		var String side = ""
		val boolean isSelfMessage = sender.name.equals(receiver.name)
		if (isSelfMessage) {
			side = "side="
			val k = scenarioActors.indexOf(sender)
			if ((k - indexLeftActor) > (indexRightActor - k)) {
				side = side.concat("left")
			} else {
				side = side.concat("right")
			}
		}
		return side
	}

	private def static String getEndMessage(Role sender, Role receiver) {
		var messageLatex = ""
		val boolean isSelfMessage = sender.name.equals(receiver.name)
		if (isSelfMessage) {
			messageLatex = messageLatex.concat("[1]")
		}
		messageLatex = messageLatex.concat(SmlToLatexGenerator.NEWLINE).
			concat("\\nextlevel")
		if (isSelfMessage) {
			messageLatex = messageLatex.concat("[2]")
		}
		messageLatex = messageLatex.concat(SmlToLatexGenerator.NEWLINE)
		return messageLatex
	}

	private def static String getLatexFromAlternative(Alternative altFragment, int blockLevel,
		EList<Role> scenarioActors) {
		// init
		val EList<String> actorsAndInitLatex = getInitElementsNestedBlock(altFragment, blockLevel, scenarioActors)
		val indexLeftActor = Integer.valueOf(actorsAndInitLatex.get(0))
		val indexRightActor = Integer.valueOf(actorsAndInitLatex.get(1))
		var listActorsAlt = getStringOfActorsList(scenarioActors, indexLeftActor, indexRightActor)
		var latexAlternative = actorsAndInitLatex.get(2)
		var latexAlternativeClose = actorsAndInitLatex.get(3)
		var conditionOverlap = actorsAndInitLatex.get(4)
		// end init
		val EList<Case> cases = altFragment.cases
		for (var int iCase = 0; iCase < cases.size; iCase++) {
			if (iCase > 0) {
				latexAlternative = latexAlternative.concat("\\inlineseparator{").concat(ALT_PREFIX_ID).concat("" +
					blockLevel).concat("}").concat("\\nextlevel").concat(
					SmlToLatexGenerator.NEWLINE)
			}
			val Case caseElement = cases.get(iCase)
			val Condition caseCond = caseElement.caseCondition
			var String condExpText = ""
			if (caseCond != null && caseCond.conditionExpression != null) {
				condExpText = NodeModelUtils.getNode(caseCond.conditionExpression).getText().trim()
			}
			if (!condExpText.isEmpty) {
				val skip = getNextLevelSkipParameter(condExpText, indexLeftActor, indexRightActor)
				latexAlternative = latexAlternative + ''' 
					\condition[condition overlap=«conditionOverlap»cm] {\textbf{if} $«condExpText»$}
					{«listActorsAlt»} \nextlevel[«skip»]
					'''
			}
			val Interaction caseInter = caseElement.caseInteraction
			latexAlternative = latexAlternative.concat(
				getLatexFromInteraction(caseInter, blockLevel, scenarioActors, indexLeftActor, indexRightActor))
		}
		latexAlternative = latexAlternative.concat(latexAlternativeClose)
		return latexAlternative
	}

	private def static String getNextLevelSkipParameter(String condExpText, int indexLeftActor, int indexRightActor) {
		var int lineCountAltCondition
		if (indexLeftActor != indexRightActor){
			lineCountAltCondition = Math.ceil((condExpText.length / (30.0 * (Math.abs(indexRightActor - indexLeftActor))))) as int
		}else{
			lineCountAltCondition = Math.ceil((condExpText.length / 30.0)) as int
		}
		return String.valueOf(2 + 1 * lineCountAltCondition)
	}

	private def static String getOverlapValue(int blockLevel, double defaultValue, double minValue) {
		var overlapValue = defaultValue
		if (blockLevel > 0) {
			overlapValue -=
				NESTED_MARGIN_LEFT_RIGHT * Math.floor(Math.log(blockLevel) / Math.log(MAX_BLOCK_CHILDREN_COUNT))
			if (overlapValue < minValue) {
				overlapValue = minValue
			}
		}
		return String.valueOf(overlapValue)
	}

	private def static String getLatexFromConditionFragment(ConditionFragment conditionFragment, EList<Role> scenarioActors,
		int indexLeftActor, int indexRightActor, int blockLevel) {
		val ConditionExpression conditionExp = conditionFragment.conditionExpression
		var conditionOverlap = getOverlapValue(blockLevel, CONDITION_OVERLAP_DEFAULT_VALUE, CONDITION_OVERLAP_MIN_VALUE)
		var String latexCondition = "\\condition[condition overlap=".concat(conditionOverlap).concat("cm")
		val String conditionExpText = NodeModelUtils.getNode(conditionExp).getText().trim()
		var String mainLabel = ""
		val String drawRed = ",draw=red]"
		val String drawBlue = ",draw=blue]"
		if (conditionFragment instanceof WaitCondition) {
			var sAndr = ""
			val String drawRedStart = ",draw=red"
			val String drawBlueStart = ",draw=blue"
			val String drawEnd = "]"
			if (conditionFragment.isStrict) {
				sAndr = sAndr.concat("strict")
				latexCondition = latexCondition.concat(drawRedStart)
			} else {
				latexCondition = latexCondition.concat(drawBlueStart)
			}
			if (conditionFragment.isRequested) {
				if (conditionFragment.isStrict) {
					sAndr = sAndr.concat(", ")
				}
				sAndr = sAndr.concat("eventually")
			} else {
				latexCondition = latexCondition.concat(",dashed")
			}
			latexCondition = latexCondition.concat(drawEnd)
			val waituntil = "wait until"
			if (!sAndr.isEmpty) {
				val String condType = "$\\langle ".concat(sAndr).concat("\\rangle$ ")
				mainLabel = condType.concat(waituntil)
			} else {
				mainLabel = waituntil
			}
		} else if (conditionFragment instanceof InterruptCondition) {
			latexCondition = latexCondition.concat(drawBlue)
			mainLabel = "interrupt"
		} else if (conditionFragment instanceof ViolationCondition) {
			latexCondition = latexCondition.concat(drawRed)
			mainLabel = "violation"
		}
		mainLabel = '''{\textbf{«mainLabel»} $«conditionExpText»$}'''
		val skip = getNextLevelSkipParameter(conditionExpText, indexLeftActor, indexRightActor)
		latexCondition = latexCondition.concat(mainLabel).concat("{").concat(
			getStringOfActorsList(scenarioActors, indexLeftActor, indexRightActor)).concat("}").concat("\\nextlevel[").
			concat(skip).concat("]").concat(SmlToLatexGenerator.NEWLINE)
		return latexCondition
	}

	private def static String getLatexFromVariableFragment(VariableFragment variableFragment,
		EList<Role> scenarioActors, int indexLeftActor, int indexRightActor, int blockLevel) {
		val VariableExpression varExp = variableFragment.expression
		var String variableFragmentText = ""
		if (varExp instanceof TypedVariableDeclaration) {
			val type = varExp.type
			val varName = varExp.name
			val exp = varExp.expression
			if (exp == null) {
				variableFragmentText = '''\textbf{var «type.name»} $«varName»$''' 
			}else{
				variableFragmentText = '''\textbf{var «type.name»} $«varName» = «NodeModelUtils.getNode(exp).getText().trim()»$''' 
			}
		} else if (varExp instanceof VariableAssignment) {
			val variable = varExp.variable
			val name = variable.name
			variableFragmentText = variableFragmentText.concat(name).concat(" = ")
			val exp = varExp.expression
			variableFragmentText = variableFragmentText.concat(NodeModelUtils.getNode(exp).getText().trim())
		}
		var conditionOverlap = getOverlapValue(blockLevel, CONDITION_OVERLAP_DEFAULT_VALUE, CONDITION_OVERLAP_MIN_VALUE)
		var String latexCondition = "\\condition[condition overlap=".concat(conditionOverlap).concat(
			"cm,rectangle,rounded corners=3pt]{")
		latexCondition = latexCondition.concat(variableFragmentText).concat("}")
		val skip = getNextLevelSkipParameter(variableFragmentText, indexLeftActor, indexRightActor)
		latexCondition = latexCondition.concat("{").concat(
			getStringOfActorsList(scenarioActors, indexLeftActor, indexRightActor)).concat("}").concat("\\nextlevel[").
			concat(skip).concat("]").concat(SmlToLatexGenerator.NEWLINE)
		return latexCondition
	}

	private def static EList<String> getInitElementsNestedBlock(InteractionFragment nestedBlockFragment, int blockLevel,
		EList<Role> scenarioActors) {
		var String cornerTopLeftLabel = ""
		var String idPrefix = ""
		if (nestedBlockFragment instanceof Alternative) {
			cornerTopLeftLabel = "alt"
			idPrefix = ALT_PREFIX_ID
		} else if (nestedBlockFragment instanceof Loop) {
			cornerTopLeftLabel = "while"
			idPrefix = LOOP_PREFIX_ID
		} else if (nestedBlockFragment instanceof Parallel) {
			cornerTopLeftLabel = "par"
			idPrefix = PARALLEL_PREFIX_ID
		} else if (nestedBlockFragment instanceof Interaction) {
			// cornerTopLeftLabel = ""
			idPrefix = INTERACTION_PREFIX_ID
		} else {
			throw new Exception(
				"Invalid type argument. interactFragment should be either Alternative, Loop or Parallel")
		}
		val messagesNestedBlock = nestedBlockFragment.eAllContents.filter(s|s instanceof Message)
		var int iMin = scenarioActors.size
		var int iMax = -1
		while (messagesNestedBlock.hasNext) {
			var Message message = messagesNestedBlock.next as Message
			val int positionSender = scenarioActors.indexOf(message.sender)
			val int positionReceiver = scenarioActors.indexOf(message.receiver)
			var int minLocal
			var int maxLocal
			if (positionSender < positionReceiver) {
				minLocal = positionSender
				maxLocal = positionReceiver
			} else {
				minLocal = positionReceiver
				maxLocal = positionSender
			}
			if(iMin > minLocal) iMin = minLocal
			if(iMax < maxLocal) iMax = maxLocal
		}
		val String firstPositionActor = scenarioActors.get(iMin).name
		val String secondPositionActor = scenarioActors.get(iMax).name
		var inlineOverlap = getOverlapValue(blockLevel, INLINE_OVERLAP_DEFAULT_VALUE, INLINE_OVERLAP_MIN_VALUE)
		val String latexOpenBlock = ("\\inlinestart[right inline overlap=").concat(inlineOverlap).concat(
			"cm, left inline overlap=").concat(inlineOverlap).concat("cm, label distance=1ex]{").concat(idPrefix).
			concat("" + blockLevel).concat("}").concat("{").concat(cornerTopLeftLabel).concat("}").concat("{").concat(
				firstPositionActor).concat("}").concat("{").concat(secondPositionActor).concat("}").concat(
				"\\nextlevel[2]").concat(SmlToLatexGenerator.NEWLINE)
		val String latexCloseBlock = ("\\inlineend{").concat(idPrefix).concat("" + blockLevel).concat("}").concat(
			"\\nextlevel").concat(SmlToLatexGenerator.NEWLINE)

		var conditionOverlap = getOverlapValue(blockLevel, CONDITION_OVERLAP_DEFAULT_VALUE, CONDITION_OVERLAP_MIN_VALUE)
		var EList<String> toReturn = new BasicEList()
		toReturn.add(String.valueOf(iMin))
		toReturn.add(String.valueOf(iMax))
		toReturn.add(latexOpenBlock)
		toReturn.add(latexCloseBlock)
		toReturn.add(conditionOverlap)
		return toReturn
	}

	private def static String getLatexFromInteraction(Interaction interaction, int blockLevel,
		EList<Role> scenarioActors, int indexLeftActor, int indexRightActor) {
		var latexLoop = ""
		var countChild = 1
		val EList<InteractionFragment> fragments = interaction.fragments
		for (var int iFrag = 0; iFrag < fragments.size; iFrag++) {
			var InteractionFragment fragment = fragments.get(iFrag)
			// Interaction | ModalMessage | Alternative | Condition | VariableFragment | Loop | Parallel
			if (fragment instanceof ModalMessage) {
				latexLoop = latexLoop.concat(
					getLatexFromModalMessage(fragment, scenarioActors, indexLeftActor, indexRightActor))
			} else if (fragment instanceof Alternative) {
				var blockLevelCurrent = getBlockCurrentLevel(blockLevel, countChild)
				countChild++
				latexLoop = latexLoop.concat(getLatexFromAlternative(fragment, blockLevelCurrent, scenarioActors))
			} else if (fragment instanceof ConditionFragment) {
				latexLoop = latexLoop.concat(
					getLatexFromConditionFragment(fragment, scenarioActors, indexLeftActor, indexRightActor, blockLevel))
			} else if (fragment instanceof VariableFragment) {
				latexLoop = latexLoop.concat(
					getLatexFromVariableFragment(fragment, scenarioActors, indexLeftActor, indexRightActor, blockLevel))
			} else if (fragment instanceof Loop) {
				var blockLevelCurrent = getBlockCurrentLevel(blockLevel, countChild)
				countChild++
				latexLoop = latexLoop.concat(getLatexFromLoop(fragment, blockLevelCurrent, scenarioActors))
			} else if (fragment instanceof Parallel) {
				var blockLevelCurrent = getBlockCurrentLevel(blockLevel, countChild)
				countChild++
				latexLoop = latexLoop.concat(getLatexFromParallel(fragment, blockLevelCurrent, scenarioActors))
			} else if (fragment instanceof Interaction) {
				var blockLevelCurrent = getBlockCurrentLevel(blockLevel, countChild)
				countChild++
				val EList<String> actorsAndInitLatex = getInitElementsNestedBlock(fragment, blockLevelCurrent,
					scenarioActors)
				val latexInteractionOpen = actorsAndInitLatex.get(2)
				val latexInteractionClose = actorsAndInitLatex.get(3)
				var latexInteraction = getLatexFromInteraction(fragment, blockLevelCurrent, scenarioActors,
					indexLeftActor, indexRightActor)
				latexInteraction = latexInteractionOpen.concat(latexInteraction).concat(latexInteractionClose)
				latexLoop = latexLoop.concat(latexInteraction)
			}
		}
		// add constraintblock if exists
		val ConstraintBlock constraintBlock = interaction.constraints
		if (constraintBlock != null) {
			val blockLevelCurrent = getBlockCurrentLevel(blockLevel, countChild)
			countChild++
			latexLoop = latexLoop.concat(
				getLatexFromConstraintBlock(constraintBlock, blockLevelCurrent, scenarioActors, indexLeftActor,
					indexRightActor))
		}
		return latexLoop
	}

	private def static int getBlockCurrentLevel(int blockLevel, int countChild) {
		return blockLevel * MAX_BLOCK_CHILDREN_COUNT + countChild // assume we dont have more than (MAX_BLOCK_CHILDREN_COUNT - 1) blocks inside a block
	}

	private def static String getLatexFromParallel(Parallel parallelFragment, int blockLevel,
		EList<Role> scenarioActors) {
		// init
		val EList<String> actorsAndInitLatex = getInitElementsNestedBlock(parallelFragment, blockLevel, scenarioActors)
		val indexLeftActor = Integer.valueOf(actorsAndInitLatex.get(0))
		val indexRightActor = Integer.valueOf(actorsAndInitLatex.get(1))
		var latexParallel = actorsAndInitLatex.get(2)
		var latexParallelClose = actorsAndInitLatex.get(3)
		// end init
		val EList<Interaction> interactions = parallelFragment.parallelInteraction
		for (var int iInteraction = 0; iInteraction < interactions.size; iInteraction++) {
			if (iInteraction > 0) {
				latexParallel = latexParallel.concat("\\inlineseparator[solid]{").concat(PARALLEL_PREFIX_ID).concat(
					"" + blockLevel).concat("}").concat("\\nextlevel").concat(
					SmlToLatexGenerator.NEWLINE)
			}
			val Interaction parInter = interactions.get(iInteraction)
			latexParallel = latexParallel.concat(
				getLatexFromInteraction(parInter, blockLevel, scenarioActors, indexLeftActor, indexRightActor))
		}
		latexParallel = latexParallel.concat(latexParallelClose)
		return latexParallel
	}

	private def static String convertScenarioToLatex(Scenario scenario, EList<Role> scenarioActors) {
		var int blockLevel = 0
		// var int nestedCount = 1
		val Interaction scenarioInteraction = scenario.ownedInteraction
		val int indexLeftActor = 0
		val int indexRightActor = scenarioActors.size - 1
		// each scenario muss have at least 2 actors
		val String textScenario = getLatexFromInteraction(scenarioInteraction, blockLevel, scenarioActors,
			indexLeftActor, indexRightActor)
		return textScenario
	}

	private def static String getLatexFromConstraintBlock(ConstraintBlock constraintBlock, int blockLevel,
		EList<Role> scenarioActors, int indexLeftActor, int indexRightActor) {
		// init
		var String blockType = "constraints"
		var String idPrefix = CONSTRAINTS_PREFIX_ID
		val messagesNestedBlock = constraintBlock.eAllContents.filter(s|s instanceof Message)
		var int iMin = scenarioActors.size
		var int iMax = -1
		while (messagesNestedBlock.hasNext) {
			var Message message = messagesNestedBlock.next as Message
			val int positionSender = scenarioActors.indexOf(message.sender)
			val int positionReceiver = scenarioActors.indexOf(message.receiver)
			var int minLocal
			var int maxLocal
			if (positionSender < positionReceiver) {
				minLocal = positionSender
				maxLocal = positionReceiver
			} else {
				minLocal = positionReceiver
				maxLocal = positionSender
			}
			if(iMin > minLocal) iMin = minLocal
			if(iMax < maxLocal) iMax = maxLocal
		}
		val String firstPositionActor = scenarioActors.get(iMin).name
		val String secondPositionActor = scenarioActors.get(iMax).name
		var inlineOverlap = getOverlapValue(blockLevel, INLINE_OVERLAP_DEFAULT_VALUE, INLINE_OVERLAP_MIN_VALUE)
		val String latexOpenBlock = ("\\inlinestart[right inline overlap=").concat(inlineOverlap).concat(
			"cm, left inline overlap=").concat(inlineOverlap).concat("cm, label distance=1ex]{").concat(idPrefix).
			concat("" + blockLevel).concat("}").concat("{").concat(blockType).concat("}").concat("{").concat(
				firstPositionActor).concat("}").concat("{").concat(secondPositionActor).concat("}").concat(
				"\\nextlevel[2]").concat(SmlToLatexGenerator.NEWLINE)
		val String latexCloseBlock = ("\\inlineend{").concat(idPrefix).concat("" + blockLevel).concat("}").concat(
			"\\nextlevel").concat(SmlToLatexGenerator.NEWLINE)
		var latexConstraintBlock = latexOpenBlock
		var latexConstraintClose = latexCloseBlock
		// end init
		val constraintBlockNode = NodeModelUtils.getNode(constraintBlock)
		val children = constraintBlockNode.children // children of the node: messages, keywords, whitespaces, etc
		for (var i = 0; i < children.size - 1; i++) {
			val leaf = children.get(i) // keywords: consider, interrupt, etc
			val composite = children.get(i + 1) // the actual message
			// Filter out whitespaces, etc
			if (leaf instanceof LeafNode && composite instanceof CompositeNodeWithSemanticElement) {
				// println(leaf.text + composite.text)
				val Message constraintsMessage = composite.semanticElement as Message
				val constraintType = leaf.text.trim()
				latexConstraintBlock = latexConstraintBlock.concat(
					getLatexFromConstraintMessage(constraintsMessage, constraintType, scenarioActors, indexLeftActor,
						indexRightActor))
			}
		}

		latexConstraintBlock = latexConstraintBlock.concat(latexConstraintClose)
		return latexConstraintBlock
	}

	private def static String getMessageName(Message message) {
		var String msgText = NodeModelUtils.getNode(message).getText().trim()
		msgText = msgText.substring(msgText.indexOf("->")).trim()
		var String messageName
		if (msgText.contains("(")){
			messageName = msgText.substring(msgText.indexOf(".") + 1, msgText.indexOf("(")).trim()
		}else{
			messageName = msgText.substring(msgText.indexOf(".") + 1).trim()
		}
		
 
		// getParameters
		var parameters = "("
		for (var int iParam = 0; iParam < message.parameters.size; iParam++) {
			var ParameterBinding parameter = message.parameters.get(iParam)
			if(iParam > 0) parameters = parameters.concat(",")
			val BindingExpression paramExp = parameter.bindingExpression
			val String paramExpText = NodeModelUtils.getNode(paramExp).getText().trim()
			parameters = parameters.concat(paramExpText)

		}
		parameters = parameters.concat(")")
		// end getParameters
		return messageName.concat(parameters)
	}

	private def static String getStringOfActorsList(EList<Role> scenarioActors, int fromIndex, int toIndex) {
		var String actorsList = ""
		for (var int iActor = fromIndex; iActor <= toIndex; iActor++) {
			if (iActor > fromIndex) {
				actorsList = actorsList.concat(",")
			}
			actorsList = actorsList.concat(scenarioActors.get(iActor).name)
		}
		return actorsList
	}

	private def static compileActor(Role actor) {
		'''
			\declinst[label distance=0.1cm]{«actor.name»}{}{«actor.name»:«actor.type.name»«IF(actor.isMultiRole)»\textbf{[]}«ENDIF»}
		'''
	}

	private def static String getLatexFromLoop(Loop loopFragment, int blockLevel, EList<Role> scenarioActors) {
		// init
		val EList<String> actorsAndInitLatex = getInitElementsNestedBlock(loopFragment, blockLevel, scenarioActors)
		val indexLeftActor = Integer.valueOf(actorsAndInitLatex.get(0))
		val indexRightActor = Integer.valueOf(actorsAndInitLatex.get(1))
		var listActorsLoop = getStringOfActorsList(scenarioActors, indexLeftActor, indexRightActor)
		var latexLoop = actorsAndInitLatex.get(2)
		var latexLoopClose = actorsAndInitLatex.get(3)
		var conditionOverlap = actorsAndInitLatex.get(4)
		// end init
		val Condition loopCondition = loopFragment.loopCondition
		var String condExpText = ""
		if (loopCondition != null && loopCondition.conditionExpression != null) {
			condExpText = NodeModelUtils.getNode(loopCondition.conditionExpression).getText().trim()
		}
		if (!condExpText.isEmpty) {
			val skip = getNextLevelSkipParameter(condExpText, indexLeftActor, indexRightActor)
			latexLoop = latexLoop.concat("\\condition[condition overlap=").concat(conditionOverlap).concat(
				"cm]{\\texttt{").concat(condExpText.replaceAll("&", "\\\\&")).concat("}}").concat("{").concat(
				listActorsLoop).concat("}").concat("\\nextlevel[").concat(skip).concat("]").concat(
				SmlToLatexGenerator.NEWLINE)
		}
		val Interaction loopInter = loopFragment.bodyInteraction
		val codeInteraction = getLatexFromInteraction(loopInter, blockLevel, scenarioActors, indexLeftActor,
			indexRightActor)
		latexLoop = latexLoop.concat(codeInteraction)
		latexLoop = latexLoop.concat(latexLoopClose)
		return latexLoop
	}
}
