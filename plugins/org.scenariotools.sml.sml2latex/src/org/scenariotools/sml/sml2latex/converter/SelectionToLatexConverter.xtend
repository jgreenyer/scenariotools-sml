/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.sml.sml2latex.converter

import org.eclipse.core.resources.IFile
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.jface.viewers.ISelection
import org.eclipse.jface.viewers.IStructuredSelection
import org.eclipse.ui.PlatformUI
import org.scenariotools.sml.Specification
import org.scenariotools.sml.sml2latex.exceptions.SmlFileNotFoundException

class SelectionToLatexConverter {

	private def static URI fromFile(IFile f) {
		return URI.createPlatformResourceURI(f.getFullPath().toString(), true);
	}

	private def static Resource getResource(IFile f) {
		val resourceSet = new ResourceSetImpl();
		return resourceSet.getResource(fromFile(f), true);
	}

	def static Specification getSelectedSpecification() throws SmlFileNotFoundException {
		val ISelection selection = retrieveWorkbenchSelection()

		var IFile smlFile = null

		if (selection instanceof IStructuredSelection) {
			for (Object object : selection.toArray()) {
				if (object instanceof IFile) {
					smlFile = object
				}
			}
		}

		if (smlFile == null) {
			throw new SmlFileNotFoundException()
		}

		val Specification smlspecification = getResource(smlFile).getContents().get(0) as Specification;

		return smlspecification
	}

	def static IFile getSelectedSmlFile() throws SmlFileNotFoundException {
		val ISelection selection = retrieveWorkbenchSelection()

		var IFile smlFile = null

		if (selection instanceof IStructuredSelection) {
			for (Object object : selection.toArray()) {
				if (object instanceof IFile) {
					smlFile = object
				}
			}
		}

		if (smlFile == null) {
			throw new SmlFileNotFoundException()
		}

		return smlFile
	}

	private def static ISelection retrieveWorkbenchSelection() {
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().getSelection()
	}

}
