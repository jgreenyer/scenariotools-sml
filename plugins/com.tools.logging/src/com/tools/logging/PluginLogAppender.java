/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package com.tools.logging;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Level;
import org.apache.log4j.spi.ErrorCode;
import org.apache.log4j.spi.LoggingEvent;
import org.apache.log4j.spi.ThrowableInformation;
import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.Status;

/**
 * PluginLogAppender
 * This class is a custom Log4J appender that sends Log4J events to 
 * the Eclipse plug-in log.
 * @author Manoel Marques
 */
public class PluginLogAppender extends AppenderSkeleton {

	private ILog pluginLog;
	
	/**
	 * Sets the Eclipse log instance
	 * @param log plug-in log
	 */	
	public void setLog(ILog pluginLog) {
		this.pluginLog = pluginLog;
	}
	
	/**
	 * Log event happened.
	 * Translates level to status instance codes:
	 * level > Level.ERROR - Status.ERROR
	 * level > Level.WARN - Status.WARNING
	 * level > Level.DEBUG - Status.INFO
	 * default - Status.OK
	 * @param event LoggingEvent instance
	 */	
	public void append(LoggingEvent event) {
		
		if (this.layout == null) {
			this.errorHandler.error("Missing layout for appender " + 
			       this.name,null,ErrorCode.MISSING_LAYOUT); 
			return;
		}
		
		String text = this.layout.format(event);
		
		Throwable thrown = null;
		if (this.layout.ignoresThrowable()) {
			ThrowableInformation info = event.getThrowableInformation();
			if (info != null)
				thrown = info.getThrowable(); 
		}
				
		Level level = event.getLevel();
		
		if (level.toInt() >= Level.ERROR_INT) {
            this.pluginLog.log(new Status(Status.ERROR,
                     this.pluginLog.getBundle().getSymbolicName(),
                     level.toInt(),text,thrown));
        }
		else if (level.toInt() >= Level.WARN_INT) {
            this.pluginLog.log(new Status(Status.WARNING,
                     this.pluginLog.getBundle().getSymbolicName(),
                     level.toInt(),text,thrown));
        }
		else if (level.toInt() >= Level.DEBUG_INT) { 
            // do nothing we have too much debug output,
            // it's too slow to log theese debugs to eclipse log
        }
        else{
    		this.pluginLog.log(new Status(Status.OK,
    		         this.pluginLog.getBundle().getSymbolicName(),
    				 level.toInt(),text,thrown));
        }
	}
	
	/**
	 * Closes this appender
	 */	
	public void close() {
		this.closed = true; 
	}
	
	/**
	 * Checks if this appender requires layout
	 * @return true if layout is required.
	 */	
	public boolean requiresLayout() {
		return true;
	}
}