/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package com.tools.logging;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.BundleContext;

public class LoggingPlugin extends Plugin {
	
	private static LoggingPlugin plugin;

	private ArrayList<PluginLogManager> logManagers = new ArrayList<PluginLogManager>(); 
	
	public LoggingPlugin() {
		super();
		plugin = this;
	}

	public static LoggingPlugin getDefault() {
		return plugin;
	}
	
	/**
	 * Iterates over the list of active log managers and shutdowns each one
	 * before calling the base class implementation. 
	 * @see Plugin#stop
	 */
	public void stop(BundleContext context) throws Exception {
		synchronized (this.logManagers) {
			Iterator<PluginLogManager> it = this.logManagers.iterator();
			while (it.hasNext()) {
				PluginLogManager logManager = (PluginLogManager) it.next();
				logManager.internalShutdown(); 
			}
			this.logManagers.clear(); 
		}
		super.stop(context);
	}
	
	/**
	 * Adds a log manager object to the list of active log managers
	 */	
	void addLogManager(PluginLogManager logManager) {
		synchronized (this.logManagers) {
			if (logManager != null)
				this.logManagers.add(logManager); 
		}
	}
	
	/**
	 * Removes a log manager object from the list of active log managers
	 */
	void removeLogManager(PluginLogManager logManager) {
		synchronized (this.logManagers) {
			if (logManager != null)
				this.logManagers.remove(logManager); 
		}
	}
}