/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete
 */
package org.scenariotools.emf.objectmodellanguage;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.emf.objectmodellanguage.ObjectmodellanguagePackage
 * @generated
 */
public interface ObjectmodellanguageFactory extends EFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright (c) 2016 Joel Greenyer and others.\r\nAll rights reserved. This program and the accompanying materials\r\nare made available under the terms of the Eclipse Public License v1.0\r\nwhich accompanies this distribution, and is available at\r\nhttp://www.eclipse.org/legal/epl-v10.html\r\n\r\nScenarioTools-URL: www.scenariotools.org\r\n   \r\nContributors:\r\n    ScenarioTools Team - Initial API and implementation\r\n    Eric Wete";

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ObjectmodellanguageFactory eINSTANCE = org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguageFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>SObject</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SObject</em>'.
	 * @generated
	 */
	SObject createSObject();

	/**
	 * Returns a new object of class '<em>SValue Primitive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SValue Primitive</em>'.
	 * @generated
	 */
	SValuePrimitive createSValuePrimitive();

	/**
	 * Returns a new object of class '<em>SValue Object</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SValue Object</em>'.
	 * @generated
	 */
	SValueObject createSValueObject();

	/**
	 * Returns a new object of class '<em>SO Elt Ref</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SO Elt Ref</em>'.
	 * @generated
	 */
	SOEltRef createSOEltRef();

	/**
	 * Returns a new object of class '<em>SO Elt Contaiment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SO Elt Contaiment</em>'.
	 * @generated
	 */
	SOEltContaiment createSOEltContaiment();

	/**
	 * Returns a new object of class '<em>SO Elt Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SO Elt Attribute</em>'.
	 * @generated
	 */
	SOEltAttribute createSOEltAttribute();

	/**
	 * Returns a new object of class '<em>SValue Object Ref</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SValue Object Ref</em>'.
	 * @generated
	 */
	SValueObjectRef createSValueObjectRef();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ObjectmodellanguagePackage getObjectmodellanguagePackage();

} //ObjectmodellanguageFactory
