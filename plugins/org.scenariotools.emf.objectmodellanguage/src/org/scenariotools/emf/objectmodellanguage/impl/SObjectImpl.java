/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete
 */
package org.scenariotools.emf.objectmodellanguage.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.scenariotools.emf.objectmodellanguage.ObjectmodellanguagePackage;
import org.scenariotools.emf.objectmodellanguage.SObject;
import org.scenariotools.emf.objectmodellanguage.SObjectElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SObject</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.emf.objectmodellanguage.impl.SObjectImpl#getEClass <em>EClass</em>}</li>
 *   <li>{@link org.scenariotools.emf.objectmodellanguage.impl.SObjectImpl#getSobjectelement <em>Sobjectelement</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SObjectImpl extends SNamedElementImpl implements SObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright (c) 2016 Joel Greenyer and others.\r\nAll rights reserved. This program and the accompanying materials\r\nare made available under the terms of the Eclipse Public License v1.0\r\nwhich accompanies this distribution, and is available at\r\nhttp://www.eclipse.org/legal/epl-v10.html\r\n\r\nScenarioTools-URL: www.scenariotools.org\r\n   \r\nContributors:\r\n    ScenarioTools Team - Initial API and implementation\r\n    Eric Wete";

	/**
	 * The default value of the '{@link #getEClass() <em>EClass</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEClass()
	 * @generated
	 * @ordered
	 */
	protected static final String ECLASS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEClass() <em>EClass</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEClass()
	 * @generated
	 * @ordered
	 */
	protected String eClass = ECLASS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSobjectelement() <em>Sobjectelement</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSobjectelement()
	 * @generated
	 * @ordered
	 */
	protected EList<SObjectElement> sobjectelement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SObjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ObjectmodellanguagePackage.Literals.SOBJECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEClass() {
		return eClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEClass(String newEClass) {
		String oldEClass = eClass;
		eClass = newEClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ObjectmodellanguagePackage.SOBJECT__ECLASS, oldEClass, eClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SObjectElement> getSobjectelement() {
		if (sobjectelement == null) {
			sobjectelement = new EObjectContainmentEList<SObjectElement>(SObjectElement.class, this, ObjectmodellanguagePackage.SOBJECT__SOBJECTELEMENT);
		}
		return sobjectelement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ObjectmodellanguagePackage.SOBJECT__SOBJECTELEMENT:
				return ((InternalEList<?>)getSobjectelement()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ObjectmodellanguagePackage.SOBJECT__ECLASS:
				return getEClass();
			case ObjectmodellanguagePackage.SOBJECT__SOBJECTELEMENT:
				return getSobjectelement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ObjectmodellanguagePackage.SOBJECT__ECLASS:
				setEClass((String)newValue);
				return;
			case ObjectmodellanguagePackage.SOBJECT__SOBJECTELEMENT:
				getSobjectelement().clear();
				getSobjectelement().addAll((Collection<? extends SObjectElement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ObjectmodellanguagePackage.SOBJECT__ECLASS:
				setEClass(ECLASS_EDEFAULT);
				return;
			case ObjectmodellanguagePackage.SOBJECT__SOBJECTELEMENT:
				getSobjectelement().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ObjectmodellanguagePackage.SOBJECT__ECLASS:
				return ECLASS_EDEFAULT == null ? eClass != null : !ECLASS_EDEFAULT.equals(eClass);
			case ObjectmodellanguagePackage.SOBJECT__SOBJECTELEMENT:
				return sobjectelement != null && !sobjectelement.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (eClass: ");
		result.append(eClass);
		result.append(')');
		return result.toString();
	}

} //SObjectImpl
