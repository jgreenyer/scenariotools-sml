/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete
 */
package org.scenariotools.emf.objectmodellanguage.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.scenariotools.emf.objectmodellanguage.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ObjectmodellanguageFactoryImpl extends EFactoryImpl implements ObjectmodellanguageFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright (c) 2016 Joel Greenyer and others.\r\nAll rights reserved. This program and the accompanying materials\r\nare made available under the terms of the Eclipse Public License v1.0\r\nwhich accompanies this distribution, and is available at\r\nhttp://www.eclipse.org/legal/epl-v10.html\r\n\r\nScenarioTools-URL: www.scenariotools.org\r\n   \r\nContributors:\r\n    ScenarioTools Team - Initial API and implementation\r\n    Eric Wete";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ObjectmodellanguageFactory init() {
		try {
			ObjectmodellanguageFactory theObjectmodellanguageFactory = (ObjectmodellanguageFactory)EPackage.Registry.INSTANCE.getEFactory(ObjectmodellanguagePackage.eNS_URI);
			if (theObjectmodellanguageFactory != null) {
				return theObjectmodellanguageFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ObjectmodellanguageFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectmodellanguageFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ObjectmodellanguagePackage.SOBJECT: return createSObject();
			case ObjectmodellanguagePackage.SVALUE_PRIMITIVE: return createSValuePrimitive();
			case ObjectmodellanguagePackage.SVALUE_OBJECT: return createSValueObject();
			case ObjectmodellanguagePackage.SO_ELT_REF: return createSOEltRef();
			case ObjectmodellanguagePackage.SO_ELT_CONTAIMENT: return createSOEltContaiment();
			case ObjectmodellanguagePackage.SO_ELT_ATTRIBUTE: return createSOEltAttribute();
			case ObjectmodellanguagePackage.SVALUE_OBJECT_REF: return createSValueObjectRef();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SObject createSObject() {
		SObjectImpl sObject = new SObjectImpl();
		return sObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SValuePrimitive createSValuePrimitive() {
		SValuePrimitiveImpl sValuePrimitive = new SValuePrimitiveImpl();
		return sValuePrimitive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SValueObject createSValueObject() {
		SValueObjectImpl sValueObject = new SValueObjectImpl();
		return sValueObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SOEltRef createSOEltRef() {
		SOEltRefImpl soEltRef = new SOEltRefImpl();
		return soEltRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SOEltContaiment createSOEltContaiment() {
		SOEltContaimentImpl soEltContaiment = new SOEltContaimentImpl();
		return soEltContaiment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SOEltAttribute createSOEltAttribute() {
		SOEltAttributeImpl soEltAttribute = new SOEltAttributeImpl();
		return soEltAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SValueObjectRef createSValueObjectRef() {
		SValueObjectRefImpl sValueObjectRef = new SValueObjectRefImpl();
		return sValueObjectRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectmodellanguagePackage getObjectmodellanguagePackage() {
		return (ObjectmodellanguagePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ObjectmodellanguagePackage getPackage() {
		return ObjectmodellanguagePackage.eINSTANCE;
	}

} //ObjectmodellanguageFactoryImpl
