/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete
 */
package org.scenariotools.emf.objectmodellanguage.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.scenariotools.emf.objectmodellanguage.ObjectmodellanguageFactory;
import org.scenariotools.emf.objectmodellanguage.ObjectmodellanguagePackage;
import org.scenariotools.emf.objectmodellanguage.SAValue;
import org.scenariotools.emf.objectmodellanguage.SNamedElement;
import org.scenariotools.emf.objectmodellanguage.SOEltAttribute;
import org.scenariotools.emf.objectmodellanguage.SOEltContaiment;
import org.scenariotools.emf.objectmodellanguage.SOEltRef;
import org.scenariotools.emf.objectmodellanguage.SObject;
import org.scenariotools.emf.objectmodellanguage.SObjectElement;
import org.scenariotools.emf.objectmodellanguage.SValueObject;
import org.scenariotools.emf.objectmodellanguage.SValueObjectRef;
import org.scenariotools.emf.objectmodellanguage.SValuePrimitive;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ObjectmodellanguagePackageImpl extends EPackageImpl implements ObjectmodellanguagePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright (c) 2016 Joel Greenyer and others.\r\nAll rights reserved. This program and the accompanying materials\r\nare made available under the terms of the Eclipse Public License v1.0\r\nwhich accompanies this distribution, and is available at\r\nhttp://www.eclipse.org/legal/epl-v10.html\r\n\r\nScenarioTools-URL: www.scenariotools.org\r\n   \r\nContributors:\r\n    ScenarioTools Team - Initial API and implementation\r\n    Eric Wete";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sNamedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sObjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sObjectElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass saValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sValuePrimitiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sValueObjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass soEltRefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass soEltContaimentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass soEltAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sValueObjectRefEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.scenariotools.emf.objectmodellanguage.ObjectmodellanguagePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ObjectmodellanguagePackageImpl() {
		super(eNS_URI, ObjectmodellanguageFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ObjectmodellanguagePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ObjectmodellanguagePackage init() {
		if (isInited) return (ObjectmodellanguagePackage)EPackage.Registry.INSTANCE.getEPackage(ObjectmodellanguagePackage.eNS_URI);

		// Obtain or create and register package
		ObjectmodellanguagePackageImpl theObjectmodellanguagePackage = (ObjectmodellanguagePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ObjectmodellanguagePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ObjectmodellanguagePackageImpl());

		isInited = true;

		// Create package meta-data objects
		theObjectmodellanguagePackage.createPackageContents();

		// Initialize created meta-data
		theObjectmodellanguagePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theObjectmodellanguagePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ObjectmodellanguagePackage.eNS_URI, theObjectmodellanguagePackage);
		return theObjectmodellanguagePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSNamedElement() {
		return sNamedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSNamedElement_Name() {
		return (EAttribute)sNamedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSObject() {
		return sObjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSObject_EClass() {
		return (EAttribute)sObjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSObject_Sobjectelement() {
		return (EReference)sObjectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSObjectElement() {
		return sObjectElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSAValue() {
		return saValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSValuePrimitive() {
		return sValuePrimitiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSValuePrimitive_Value() {
		return (EAttribute)sValuePrimitiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSValueObject() {
		return sValueObjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSValueObject_Name() {
		return (EAttribute)sValueObjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSValueObject_EClass() {
		return (EAttribute)sValueObjectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSOEltRef() {
		return soEltRefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSOEltRef_Svalueobject() {
		return (EReference)soEltRefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSOEltContaiment() {
		return soEltContaimentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSOEltContaiment_Sobject() {
		return (EReference)soEltContaimentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSOEltAttribute() {
		return soEltAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSOEltAttribute_Savalue() {
		return (EReference)soEltAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSValueObjectRef() {
		return sValueObjectRefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSValueObjectRef_Name() {
		return (EAttribute)sValueObjectRefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSValueObjectRef_EClass() {
		return (EAttribute)sValueObjectRefEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectmodellanguageFactory getObjectmodellanguageFactory() {
		return (ObjectmodellanguageFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		sNamedElementEClass = createEClass(SNAMED_ELEMENT);
		createEAttribute(sNamedElementEClass, SNAMED_ELEMENT__NAME);

		sObjectEClass = createEClass(SOBJECT);
		createEAttribute(sObjectEClass, SOBJECT__ECLASS);
		createEReference(sObjectEClass, SOBJECT__SOBJECTELEMENT);

		sObjectElementEClass = createEClass(SOBJECT_ELEMENT);

		saValueEClass = createEClass(SA_VALUE);

		sValuePrimitiveEClass = createEClass(SVALUE_PRIMITIVE);
		createEAttribute(sValuePrimitiveEClass, SVALUE_PRIMITIVE__VALUE);

		sValueObjectEClass = createEClass(SVALUE_OBJECT);
		createEAttribute(sValueObjectEClass, SVALUE_OBJECT__NAME);
		createEAttribute(sValueObjectEClass, SVALUE_OBJECT__ECLASS);

		soEltRefEClass = createEClass(SO_ELT_REF);
		createEReference(soEltRefEClass, SO_ELT_REF__SVALUEOBJECT);

		soEltContaimentEClass = createEClass(SO_ELT_CONTAIMENT);
		createEReference(soEltContaimentEClass, SO_ELT_CONTAIMENT__SOBJECT);

		soEltAttributeEClass = createEClass(SO_ELT_ATTRIBUTE);
		createEReference(soEltAttributeEClass, SO_ELT_ATTRIBUTE__SAVALUE);

		sValueObjectRefEClass = createEClass(SVALUE_OBJECT_REF);
		createEAttribute(sValueObjectRefEClass, SVALUE_OBJECT_REF__NAME);
		createEAttribute(sValueObjectRefEClass, SVALUE_OBJECT_REF__ECLASS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		sObjectEClass.getESuperTypes().add(this.getSNamedElement());
		sObjectElementEClass.getESuperTypes().add(this.getSNamedElement());
		sValuePrimitiveEClass.getESuperTypes().add(this.getSAValue());
		sValueObjectEClass.getESuperTypes().add(this.getSAValue());
		soEltRefEClass.getESuperTypes().add(this.getSObjectElement());
		soEltContaimentEClass.getESuperTypes().add(this.getSObjectElement());
		soEltAttributeEClass.getESuperTypes().add(this.getSObjectElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(sNamedElementEClass, SNamedElement.class, "SNamedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSNamedElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, SNamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sObjectEClass, SObject.class, "SObject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSObject_EClass(), ecorePackage.getEString(), "eClass", null, 0, 1, SObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSObject_Sobjectelement(), this.getSObjectElement(), null, "sobjectelement", null, 0, -1, SObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sObjectElementEClass, SObjectElement.class, "SObjectElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(saValueEClass, SAValue.class, "SAValue", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(sValuePrimitiveEClass, SValuePrimitive.class, "SValuePrimitive", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSValuePrimitive_Value(), ecorePackage.getEString(), "value", null, 0, 1, SValuePrimitive.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sValueObjectEClass, SValueObject.class, "SValueObject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSValueObject_Name(), ecorePackage.getEString(), "name", null, 0, 1, SValueObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSValueObject_EClass(), ecorePackage.getEString(), "eClass", null, 0, 1, SValueObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(soEltRefEClass, SOEltRef.class, "SOEltRef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSOEltRef_Svalueobject(), this.getSValueObjectRef(), null, "svalueobject", null, 0, -1, SOEltRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(soEltContaimentEClass, SOEltContaiment.class, "SOEltContaiment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSOEltContaiment_Sobject(), this.getSObject(), null, "sobject", null, 0, -1, SOEltContaiment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(soEltAttributeEClass, SOEltAttribute.class, "SOEltAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSOEltAttribute_Savalue(), this.getSAValue(), null, "savalue", null, 0, -1, SOEltAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sValueObjectRefEClass, SValueObjectRef.class, "SValueObjectRef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSValueObjectRef_Name(), ecorePackage.getEString(), "name", null, 0, 1, SValueObjectRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSValueObjectRef_EClass(), ecorePackage.getEString(), "eClass", null, 0, 1, SValueObjectRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //ObjectmodellanguagePackageImpl
