/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete
 */
package org.scenariotools.emf.objectmodellanguage.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.scenariotools.emf.objectmodellanguage.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.scenariotools.emf.objectmodellanguage.ObjectmodellanguagePackage
 * @generated
 */
public class ObjectmodellanguageSwitch<T> extends Switch<T> {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright (c) 2016 Joel Greenyer and others.\r\nAll rights reserved. This program and the accompanying materials\r\nare made available under the terms of the Eclipse Public License v1.0\r\nwhich accompanies this distribution, and is available at\r\nhttp://www.eclipse.org/legal/epl-v10.html\r\n\r\nScenarioTools-URL: www.scenariotools.org\r\n   \r\nContributors:\r\n    ScenarioTools Team - Initial API and implementation\r\n    Eric Wete";

	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ObjectmodellanguagePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectmodellanguageSwitch() {
		if (modelPackage == null) {
			modelPackage = ObjectmodellanguagePackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ObjectmodellanguagePackage.SNAMED_ELEMENT: {
				SNamedElement sNamedElement = (SNamedElement)theEObject;
				T result = caseSNamedElement(sNamedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ObjectmodellanguagePackage.SOBJECT: {
				SObject sObject = (SObject)theEObject;
				T result = caseSObject(sObject);
				if (result == null) result = caseSNamedElement(sObject);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ObjectmodellanguagePackage.SOBJECT_ELEMENT: {
				SObjectElement sObjectElement = (SObjectElement)theEObject;
				T result = caseSObjectElement(sObjectElement);
				if (result == null) result = caseSNamedElement(sObjectElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ObjectmodellanguagePackage.SA_VALUE: {
				SAValue saValue = (SAValue)theEObject;
				T result = caseSAValue(saValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ObjectmodellanguagePackage.SVALUE_PRIMITIVE: {
				SValuePrimitive sValuePrimitive = (SValuePrimitive)theEObject;
				T result = caseSValuePrimitive(sValuePrimitive);
				if (result == null) result = caseSAValue(sValuePrimitive);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ObjectmodellanguagePackage.SVALUE_OBJECT: {
				SValueObject sValueObject = (SValueObject)theEObject;
				T result = caseSValueObject(sValueObject);
				if (result == null) result = caseSAValue(sValueObject);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ObjectmodellanguagePackage.SO_ELT_REF: {
				SOEltRef soEltRef = (SOEltRef)theEObject;
				T result = caseSOEltRef(soEltRef);
				if (result == null) result = caseSObjectElement(soEltRef);
				if (result == null) result = caseSNamedElement(soEltRef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ObjectmodellanguagePackage.SO_ELT_CONTAIMENT: {
				SOEltContaiment soEltContaiment = (SOEltContaiment)theEObject;
				T result = caseSOEltContaiment(soEltContaiment);
				if (result == null) result = caseSObjectElement(soEltContaiment);
				if (result == null) result = caseSNamedElement(soEltContaiment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ObjectmodellanguagePackage.SO_ELT_ATTRIBUTE: {
				SOEltAttribute soEltAttribute = (SOEltAttribute)theEObject;
				T result = caseSOEltAttribute(soEltAttribute);
				if (result == null) result = caseSObjectElement(soEltAttribute);
				if (result == null) result = caseSNamedElement(soEltAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ObjectmodellanguagePackage.SVALUE_OBJECT_REF: {
				SValueObjectRef sValueObjectRef = (SValueObjectRef)theEObject;
				T result = caseSValueObjectRef(sValueObjectRef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SNamed Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SNamed Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSNamedElement(SNamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSObject(SObject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SObject Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SObject Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSObjectElement(SObjectElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SA Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SA Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSAValue(SAValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SValue Primitive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SValue Primitive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSValuePrimitive(SValuePrimitive object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SValue Object</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SValue Object</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSValueObject(SValueObject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SO Elt Ref</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SO Elt Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSOEltRef(SOEltRef object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SO Elt Contaiment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SO Elt Contaiment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSOEltContaiment(SOEltContaiment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SO Elt Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SO Elt Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSOEltAttribute(SOEltAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SValue Object Ref</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SValue Object Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSValueObjectRef(SValueObjectRef object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ObjectmodellanguageSwitch
