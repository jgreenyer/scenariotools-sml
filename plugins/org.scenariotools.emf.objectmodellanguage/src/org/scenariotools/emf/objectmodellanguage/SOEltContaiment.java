/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete
 */
package org.scenariotools.emf.objectmodellanguage;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SO Elt Contaiment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.emf.objectmodellanguage.SOEltContaiment#getSobject <em>Sobject</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.emf.objectmodellanguage.ObjectmodellanguagePackage#getSOEltContaiment()
 * @model
 * @generated
 */
public interface SOEltContaiment extends SObjectElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright (c) 2016 Joel Greenyer and others.\r\nAll rights reserved. This program and the accompanying materials\r\nare made available under the terms of the Eclipse Public License v1.0\r\nwhich accompanies this distribution, and is available at\r\nhttp://www.eclipse.org/legal/epl-v10.html\r\n\r\nScenarioTools-URL: www.scenariotools.org\r\n   \r\nContributors:\r\n    ScenarioTools Team - Initial API and implementation\r\n    Eric Wete";

	/**
	 * Returns the value of the '<em><b>Sobject</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.emf.objectmodellanguage.SObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sobject</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sobject</em>' containment reference list.
	 * @see org.scenariotools.emf.objectmodellanguage.ObjectmodellanguagePackage#getSOEltContaiment_Sobject()
	 * @model containment="true"
	 * @generated
	 */
	EList<SObject> getSobject();

} // SOEltContaiment
