/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete
 */
package org.scenariotools.emf.objectmodellanguage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.scenariotools.emf.objectmodellanguage.ObjectmodellanguageFactory
 * @model kind="package"
 * @generated
 */
public interface ObjectmodellanguagePackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright (c) 2016 Joel Greenyer and others.\r\nAll rights reserved. This program and the accompanying materials\r\nare made available under the terms of the Eclipse Public License v1.0\r\nwhich accompanies this distribution, and is available at\r\nhttp://www.eclipse.org/legal/epl-v10.html\r\n\r\nScenarioTools-URL: www.scenariotools.org\r\n   \r\nContributors:\r\n    ScenarioTools Team - Initial API and implementation\r\n    Eric Wete";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "objectmodellanguage";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.scenariotools.org/emf/objectmodellanguage";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "objectmodellanguage";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ObjectmodellanguagePackage eINSTANCE = org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.emf.objectmodellanguage.impl.SNamedElementImpl <em>SNamed Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.emf.objectmodellanguage.impl.SNamedElementImpl
	 * @see org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl#getSNamedElement()
	 * @generated
	 */
	int SNAMED_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SNAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>SNamed Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SNAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>SNamed Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SNAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.emf.objectmodellanguage.impl.SObjectImpl <em>SObject</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.emf.objectmodellanguage.impl.SObjectImpl
	 * @see org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl#getSObject()
	 * @generated
	 */
	int SOBJECT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOBJECT__NAME = SNAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>EClass</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOBJECT__ECLASS = SNAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sobjectelement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOBJECT__SOBJECTELEMENT = SNAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>SObject</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOBJECT_FEATURE_COUNT = SNAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>SObject</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOBJECT_OPERATION_COUNT = SNAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.emf.objectmodellanguage.impl.SObjectElementImpl <em>SObject Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.emf.objectmodellanguage.impl.SObjectElementImpl
	 * @see org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl#getSObjectElement()
	 * @generated
	 */
	int SOBJECT_ELEMENT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOBJECT_ELEMENT__NAME = SNAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>SObject Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOBJECT_ELEMENT_FEATURE_COUNT = SNAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>SObject Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOBJECT_ELEMENT_OPERATION_COUNT = SNAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.emf.objectmodellanguage.impl.SAValueImpl <em>SA Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.emf.objectmodellanguage.impl.SAValueImpl
	 * @see org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl#getSAValue()
	 * @generated
	 */
	int SA_VALUE = 3;

	/**
	 * The number of structural features of the '<em>SA Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SA_VALUE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>SA Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SA_VALUE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.emf.objectmodellanguage.impl.SValuePrimitiveImpl <em>SValue Primitive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.emf.objectmodellanguage.impl.SValuePrimitiveImpl
	 * @see org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl#getSValuePrimitive()
	 * @generated
	 */
	int SVALUE_PRIMITIVE = 4;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SVALUE_PRIMITIVE__VALUE = SA_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>SValue Primitive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SVALUE_PRIMITIVE_FEATURE_COUNT = SA_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>SValue Primitive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SVALUE_PRIMITIVE_OPERATION_COUNT = SA_VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.emf.objectmodellanguage.impl.SValueObjectImpl <em>SValue Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.emf.objectmodellanguage.impl.SValueObjectImpl
	 * @see org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl#getSValueObject()
	 * @generated
	 */
	int SVALUE_OBJECT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SVALUE_OBJECT__NAME = SA_VALUE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>EClass</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SVALUE_OBJECT__ECLASS = SA_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>SValue Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SVALUE_OBJECT_FEATURE_COUNT = SA_VALUE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>SValue Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SVALUE_OBJECT_OPERATION_COUNT = SA_VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.emf.objectmodellanguage.impl.SOEltRefImpl <em>SO Elt Ref</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.emf.objectmodellanguage.impl.SOEltRefImpl
	 * @see org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl#getSOEltRef()
	 * @generated
	 */
	int SO_ELT_REF = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SO_ELT_REF__NAME = SOBJECT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Svalueobject</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SO_ELT_REF__SVALUEOBJECT = SOBJECT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>SO Elt Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SO_ELT_REF_FEATURE_COUNT = SOBJECT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>SO Elt Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SO_ELT_REF_OPERATION_COUNT = SOBJECT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.emf.objectmodellanguage.impl.SOEltContaimentImpl <em>SO Elt Contaiment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.emf.objectmodellanguage.impl.SOEltContaimentImpl
	 * @see org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl#getSOEltContaiment()
	 * @generated
	 */
	int SO_ELT_CONTAIMENT = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SO_ELT_CONTAIMENT__NAME = SOBJECT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Sobject</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SO_ELT_CONTAIMENT__SOBJECT = SOBJECT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>SO Elt Contaiment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SO_ELT_CONTAIMENT_FEATURE_COUNT = SOBJECT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>SO Elt Contaiment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SO_ELT_CONTAIMENT_OPERATION_COUNT = SOBJECT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.emf.objectmodellanguage.impl.SOEltAttributeImpl <em>SO Elt Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.emf.objectmodellanguage.impl.SOEltAttributeImpl
	 * @see org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl#getSOEltAttribute()
	 * @generated
	 */
	int SO_ELT_ATTRIBUTE = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SO_ELT_ATTRIBUTE__NAME = SOBJECT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Savalue</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SO_ELT_ATTRIBUTE__SAVALUE = SOBJECT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>SO Elt Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SO_ELT_ATTRIBUTE_FEATURE_COUNT = SOBJECT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>SO Elt Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SO_ELT_ATTRIBUTE_OPERATION_COUNT = SOBJECT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.emf.objectmodellanguage.impl.SValueObjectRefImpl <em>SValue Object Ref</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.emf.objectmodellanguage.impl.SValueObjectRefImpl
	 * @see org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl#getSValueObjectRef()
	 * @generated
	 */
	int SVALUE_OBJECT_REF = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SVALUE_OBJECT_REF__NAME = 0;

	/**
	 * The feature id for the '<em><b>EClass</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SVALUE_OBJECT_REF__ECLASS = 1;

	/**
	 * The number of structural features of the '<em>SValue Object Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SVALUE_OBJECT_REF_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>SValue Object Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SVALUE_OBJECT_REF_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link org.scenariotools.emf.objectmodellanguage.SNamedElement <em>SNamed Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SNamed Element</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SNamedElement
	 * @generated
	 */
	EClass getSNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.emf.objectmodellanguage.SNamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SNamedElement#getName()
	 * @see #getSNamedElement()
	 * @generated
	 */
	EAttribute getSNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.emf.objectmodellanguage.SObject <em>SObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SObject</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SObject
	 * @generated
	 */
	EClass getSObject();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.emf.objectmodellanguage.SObject#getEClass <em>EClass</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>EClass</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SObject#getEClass()
	 * @see #getSObject()
	 * @generated
	 */
	EAttribute getSObject_EClass();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.emf.objectmodellanguage.SObject#getSobjectelement <em>Sobjectelement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sobjectelement</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SObject#getSobjectelement()
	 * @see #getSObject()
	 * @generated
	 */
	EReference getSObject_Sobjectelement();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.emf.objectmodellanguage.SObjectElement <em>SObject Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SObject Element</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SObjectElement
	 * @generated
	 */
	EClass getSObjectElement();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.emf.objectmodellanguage.SAValue <em>SA Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SA Value</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SAValue
	 * @generated
	 */
	EClass getSAValue();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.emf.objectmodellanguage.SValuePrimitive <em>SValue Primitive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SValue Primitive</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SValuePrimitive
	 * @generated
	 */
	EClass getSValuePrimitive();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.emf.objectmodellanguage.SValuePrimitive#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SValuePrimitive#getValue()
	 * @see #getSValuePrimitive()
	 * @generated
	 */
	EAttribute getSValuePrimitive_Value();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.emf.objectmodellanguage.SValueObject <em>SValue Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SValue Object</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SValueObject
	 * @generated
	 */
	EClass getSValueObject();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.emf.objectmodellanguage.SValueObject#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SValueObject#getName()
	 * @see #getSValueObject()
	 * @generated
	 */
	EAttribute getSValueObject_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.emf.objectmodellanguage.SValueObject#getEClass <em>EClass</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>EClass</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SValueObject#getEClass()
	 * @see #getSValueObject()
	 * @generated
	 */
	EAttribute getSValueObject_EClass();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.emf.objectmodellanguage.SOEltRef <em>SO Elt Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SO Elt Ref</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SOEltRef
	 * @generated
	 */
	EClass getSOEltRef();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.emf.objectmodellanguage.SOEltRef#getSvalueobject <em>Svalueobject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Svalueobject</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SOEltRef#getSvalueobject()
	 * @see #getSOEltRef()
	 * @generated
	 */
	EReference getSOEltRef_Svalueobject();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.emf.objectmodellanguage.SOEltContaiment <em>SO Elt Contaiment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SO Elt Contaiment</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SOEltContaiment
	 * @generated
	 */
	EClass getSOEltContaiment();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.emf.objectmodellanguage.SOEltContaiment#getSobject <em>Sobject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sobject</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SOEltContaiment#getSobject()
	 * @see #getSOEltContaiment()
	 * @generated
	 */
	EReference getSOEltContaiment_Sobject();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.emf.objectmodellanguage.SOEltAttribute <em>SO Elt Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SO Elt Attribute</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SOEltAttribute
	 * @generated
	 */
	EClass getSOEltAttribute();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.emf.objectmodellanguage.SOEltAttribute#getSavalue <em>Savalue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Savalue</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SOEltAttribute#getSavalue()
	 * @see #getSOEltAttribute()
	 * @generated
	 */
	EReference getSOEltAttribute_Savalue();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.emf.objectmodellanguage.SValueObjectRef <em>SValue Object Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SValue Object Ref</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SValueObjectRef
	 * @generated
	 */
	EClass getSValueObjectRef();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.emf.objectmodellanguage.SValueObjectRef#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SValueObjectRef#getName()
	 * @see #getSValueObjectRef()
	 * @generated
	 */
	EAttribute getSValueObjectRef_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.emf.objectmodellanguage.SValueObjectRef#getEClass <em>EClass</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>EClass</em>'.
	 * @see org.scenariotools.emf.objectmodellanguage.SValueObjectRef#getEClass()
	 * @see #getSValueObjectRef()
	 * @generated
	 */
	EAttribute getSValueObjectRef_EClass();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ObjectmodellanguageFactory getObjectmodellanguageFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.emf.objectmodellanguage.impl.SNamedElementImpl <em>SNamed Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.emf.objectmodellanguage.impl.SNamedElementImpl
		 * @see org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl#getSNamedElement()
		 * @generated
		 */
		EClass SNAMED_ELEMENT = eINSTANCE.getSNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SNAMED_ELEMENT__NAME = eINSTANCE.getSNamedElement_Name();

		/**
		 * The meta object literal for the '{@link org.scenariotools.emf.objectmodellanguage.impl.SObjectImpl <em>SObject</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.emf.objectmodellanguage.impl.SObjectImpl
		 * @see org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl#getSObject()
		 * @generated
		 */
		EClass SOBJECT = eINSTANCE.getSObject();

		/**
		 * The meta object literal for the '<em><b>EClass</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOBJECT__ECLASS = eINSTANCE.getSObject_EClass();

		/**
		 * The meta object literal for the '<em><b>Sobjectelement</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOBJECT__SOBJECTELEMENT = eINSTANCE.getSObject_Sobjectelement();

		/**
		 * The meta object literal for the '{@link org.scenariotools.emf.objectmodellanguage.impl.SObjectElementImpl <em>SObject Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.emf.objectmodellanguage.impl.SObjectElementImpl
		 * @see org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl#getSObjectElement()
		 * @generated
		 */
		EClass SOBJECT_ELEMENT = eINSTANCE.getSObjectElement();

		/**
		 * The meta object literal for the '{@link org.scenariotools.emf.objectmodellanguage.impl.SAValueImpl <em>SA Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.emf.objectmodellanguage.impl.SAValueImpl
		 * @see org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl#getSAValue()
		 * @generated
		 */
		EClass SA_VALUE = eINSTANCE.getSAValue();

		/**
		 * The meta object literal for the '{@link org.scenariotools.emf.objectmodellanguage.impl.SValuePrimitiveImpl <em>SValue Primitive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.emf.objectmodellanguage.impl.SValuePrimitiveImpl
		 * @see org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl#getSValuePrimitive()
		 * @generated
		 */
		EClass SVALUE_PRIMITIVE = eINSTANCE.getSValuePrimitive();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SVALUE_PRIMITIVE__VALUE = eINSTANCE.getSValuePrimitive_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.emf.objectmodellanguage.impl.SValueObjectImpl <em>SValue Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.emf.objectmodellanguage.impl.SValueObjectImpl
		 * @see org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl#getSValueObject()
		 * @generated
		 */
		EClass SVALUE_OBJECT = eINSTANCE.getSValueObject();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SVALUE_OBJECT__NAME = eINSTANCE.getSValueObject_Name();

		/**
		 * The meta object literal for the '<em><b>EClass</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SVALUE_OBJECT__ECLASS = eINSTANCE.getSValueObject_EClass();

		/**
		 * The meta object literal for the '{@link org.scenariotools.emf.objectmodellanguage.impl.SOEltRefImpl <em>SO Elt Ref</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.emf.objectmodellanguage.impl.SOEltRefImpl
		 * @see org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl#getSOEltRef()
		 * @generated
		 */
		EClass SO_ELT_REF = eINSTANCE.getSOEltRef();

		/**
		 * The meta object literal for the '<em><b>Svalueobject</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SO_ELT_REF__SVALUEOBJECT = eINSTANCE.getSOEltRef_Svalueobject();

		/**
		 * The meta object literal for the '{@link org.scenariotools.emf.objectmodellanguage.impl.SOEltContaimentImpl <em>SO Elt Contaiment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.emf.objectmodellanguage.impl.SOEltContaimentImpl
		 * @see org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl#getSOEltContaiment()
		 * @generated
		 */
		EClass SO_ELT_CONTAIMENT = eINSTANCE.getSOEltContaiment();

		/**
		 * The meta object literal for the '<em><b>Sobject</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SO_ELT_CONTAIMENT__SOBJECT = eINSTANCE.getSOEltContaiment_Sobject();

		/**
		 * The meta object literal for the '{@link org.scenariotools.emf.objectmodellanguage.impl.SOEltAttributeImpl <em>SO Elt Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.emf.objectmodellanguage.impl.SOEltAttributeImpl
		 * @see org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl#getSOEltAttribute()
		 * @generated
		 */
		EClass SO_ELT_ATTRIBUTE = eINSTANCE.getSOEltAttribute();

		/**
		 * The meta object literal for the '<em><b>Savalue</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SO_ELT_ATTRIBUTE__SAVALUE = eINSTANCE.getSOEltAttribute_Savalue();

		/**
		 * The meta object literal for the '{@link org.scenariotools.emf.objectmodellanguage.impl.SValueObjectRefImpl <em>SValue Object Ref</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.emf.objectmodellanguage.impl.SValueObjectRefImpl
		 * @see org.scenariotools.emf.objectmodellanguage.impl.ObjectmodellanguagePackageImpl#getSValueObjectRef()
		 * @generated
		 */
		EClass SVALUE_OBJECT_REF = eINSTANCE.getSValueObjectRef();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SVALUE_OBJECT_REF__NAME = eINSTANCE.getSValueObjectRef_Name();

		/**
		 * The meta object literal for the '<em><b>EClass</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SVALUE_OBJECT_REF__ECLASS = eINSTANCE.getSValueObjectRef_EClass();

	}

} //ObjectmodellanguagePackage
