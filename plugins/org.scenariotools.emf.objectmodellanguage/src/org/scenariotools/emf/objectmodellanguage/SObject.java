/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete
 */
package org.scenariotools.emf.objectmodellanguage;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SObject</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.emf.objectmodellanguage.SObject#getEClass <em>EClass</em>}</li>
 *   <li>{@link org.scenariotools.emf.objectmodellanguage.SObject#getSobjectelement <em>Sobjectelement</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.emf.objectmodellanguage.ObjectmodellanguagePackage#getSObject()
 * @model
 * @generated
 */
public interface SObject extends SNamedElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright (c) 2016 Joel Greenyer and others.\r\nAll rights reserved. This program and the accompanying materials\r\nare made available under the terms of the Eclipse Public License v1.0\r\nwhich accompanies this distribution, and is available at\r\nhttp://www.eclipse.org/legal/epl-v10.html\r\n\r\nScenarioTools-URL: www.scenariotools.org\r\n   \r\nContributors:\r\n    ScenarioTools Team - Initial API and implementation\r\n    Eric Wete";

	/**
	 * Returns the value of the '<em><b>EClass</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EClass</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EClass</em>' attribute.
	 * @see #setEClass(String)
	 * @see org.scenariotools.emf.objectmodellanguage.ObjectmodellanguagePackage#getSObject_EClass()
	 * @model
	 * @generated
	 */
	String getEClass();

	/**
	 * Sets the value of the '{@link org.scenariotools.emf.objectmodellanguage.SObject#getEClass <em>EClass</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EClass</em>' attribute.
	 * @see #getEClass()
	 * @generated
	 */
	void setEClass(String value);

	/**
	 * Returns the value of the '<em><b>Sobjectelement</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.emf.objectmodellanguage.SObjectElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sobjectelement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sobjectelement</em>' containment reference list.
	 * @see org.scenariotools.emf.objectmodellanguage.ObjectmodellanguagePackage#getSObject_Sobjectelement()
	 * @model containment="true"
	 * @generated
	 */
	EList<SObjectElement> getSobjectelement();

} // SObject
