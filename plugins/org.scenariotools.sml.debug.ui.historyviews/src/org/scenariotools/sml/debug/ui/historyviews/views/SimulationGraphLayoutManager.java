/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.ui.historyviews.views;

import org.eclipse.jface.action.Action;
import org.eclipse.zest.core.viewers.GraphViewer;
import org.eclipse.zest.layouts.LayoutAlgorithm;
import org.eclipse.zest.layouts.LayoutStyles;
import org.eclipse.zest.layouts.algorithms.RadialLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.TreeLayoutAlgorithm;
import org.scenariotools.sml.debug.ui.historyviews.Activator;

public class SimulationGraphLayoutManager {
	
	private GraphViewer viewer;
	
	public SimulationGraphLayoutManager(GraphViewer viewer) {
		this.viewer = viewer;
	}

	private LayoutAlgorithm setLayout(int layoutNum) {
		LayoutAlgorithm layout;
		switch (layoutNum) {
			case 0:	layout = new ScenarioToolsSpringLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
					((ScenarioToolsSpringLayoutAlgorithm) layout).setRandom(false);
			//		((SpringLayoutAlgorithm) layout).setSpringLength(1);
			//		((SpringLayoutAlgorithm) layout).setSpringMove(1);
			//		((SpringLayoutAlgorithm) layout).setSpringStrain(1.0d);
			//		((SpringLayoutAlgorithm) layout).setBounds(1000, 1000, 1000, 1000);
					break;	
			case 1:	layout = new TreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
					break;
			case 2: layout = new RadialLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
					break;
			default:layout = new TreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
					break;
		}
		
		// layout = new GridLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);// many intersections
		// layout = new HorizontalTreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
		// layout = new RadialLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);// not so good

		return layout;

	}
	
	private String getLayoutName(int layoutNum) {
		String layout;
		switch (layoutNum) {
			case 0:	layout = "Spring Layout";
					break;
			case 1:	layout = "Tree Layout";
					break;
			case 2: layout = "Radial Layout";
					break;
			default:layout = "Tree Layout";
					break;
		}
		return layout;
	}

	public void applyLayout() {
		LayoutAlgorithm layout = setLayout(1);
		viewer.setLayoutAlgorithm(layout, true);
		viewer.applyLayout();
	}
	
	protected int currentLayout = 1;
	protected final int numLayouts = 3;
	
	protected void changeLayout(){
		currentLayout++;
		viewer.setLayoutAlgorithm(setLayout(currentLayout % numLayouts));
		viewer.applyLayout();
		changeLayout.setToolTipText("Change Layout. \nCurrent: " 
										+ getLayoutName(currentLayout % numLayouts) 
										+ "\nNext: " 
										+ getLayoutName((currentLayout+1) % numLayouts));
	}
	Action changeLayout;
	
	public Action getChangeLayoutAction(){
		changeLayout = new Action("Change Layout.", Action.AS_PUSH_BUTTON){
			public void run(){
				SimulationGraphLayoutManager.this.changeLayout();
			}
		};
		changeLayout.setToolTipText("Change Layout. \nCurrent: " 
				+ getLayoutName(currentLayout) 
				+ "\nNext: " 
				+ getLayoutName(currentLayout+1));
		changeLayout.setImageDescriptor(Activator.getImageDescriptor("img/change-layout.png"));
		return changeLayout;
	}
}
