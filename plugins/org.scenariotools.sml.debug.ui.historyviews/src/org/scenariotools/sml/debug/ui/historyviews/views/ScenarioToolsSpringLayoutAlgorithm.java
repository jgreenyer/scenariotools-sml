/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.ui.historyviews.views;

import org.eclipse.zest.layouts.algorithms.SpringLayoutAlgorithm;

class ScenarioToolsSpringLayoutAlgorithm extends SpringLayoutAlgorithm {
	
	public ScenarioToolsSpringLayoutAlgorithm(int s) {
		super(s);
	}

	/**
	 * Minimum distance considered between nodes
	 */
	protected static final double MIN_DISTANCE = 10.0d;
}