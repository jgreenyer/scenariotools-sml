/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.ui.historyviews.views;

import java.util.Collection;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.widgets.Display;
import org.scenariotools.sml.debug.ui.historyviews.Activator;
import org.scenariotools.sml.debug.ui.historyviews.ViewContentProvider.ViewContentProvider;
import org.scenariotools.sml.runtime.SMLRuntimeState;

public class ViewContentManager {

	private Action toggleShowSubGraph;
	private Action toggleShowEnabledMessagesOnCurrentState;
	private Action toggleShowEnabledMessagesOnAllStates;
	private Action increaseSubGraph;
	private Action decreaseSubGraph;
	private Action displaySizeOfSubGraph;

	private boolean showSubGraph;
	private int numAncestors;
	private boolean showEnabledMessagesOnCurrentState;
	private boolean showEnabledMessagesOnAllStates;
	
	private ViewContentProvider viewContentProvider;
	private SimulationGraphView simulationGraphView;
	
	public ViewContentManager(SimulationGraphView simulationGraphView) {
		this.simulationGraphView = simulationGraphView;
		this.viewContentProvider = ViewContentProvider.Factory.getFullGraph(simulationGraphView);
		this.showSubGraph = false;
		this.numAncestors = 5;
		this.showEnabledMessagesOnCurrentState = false;
		this.showEnabledMessagesOnAllStates = false;
	}
	
	private void toggleShowSubGraph() {
		showSubGraph = !showSubGraph;
		refreshViewContentProvider();
	}
	
	private void increaseSubGraph() {	
		numAncestors++;
		refreshShowSubGraphDescription();
		refreshViewContentProvider();
	}

	private void decreaseSubGraph() {
		if(numAncestors > 1){
			numAncestors--;
			refreshShowSubGraphDescription();
			refreshViewContentProvider();
		}
	}

	private void refreshShowSubGraphDescription() {
		toggleShowSubGraph.setDescription("Show only successors and antecessors up to a depth of " + numAncestors +".");
		toggleShowSubGraph.setToolTipText("Show only successors and antecessors up to a depth of " + numAncestors +".");
		displaySizeOfSubGraph.setText("#" + Integer.toString(numAncestors));
	}
	
	private void toggleShowEnabledMessagesOnCurrentState() {
		showEnabledMessagesOnCurrentState = !showEnabledMessagesOnCurrentState;
		refreshViewContentProvider();
	}
	
	private void toggleShowEnabledMessagesOnAllStates() {
		showEnabledMessagesOnAllStates = !showEnabledMessagesOnAllStates;
		refreshViewContentProvider();
	}
	
	public Action getShowSubGraphAction(){
		toggleShowSubGraph = new Action("Show only successors and antecessors up to a depth of x.", 
				Action.AS_CHECK_BOX) {
			public void run() {
				toggleShowSubGraph();
			}
		};
		toggleShowSubGraph
				.setToolTipText("Show only successors and antecessors up to a depth of " + numAncestors +".");
		toggleShowSubGraph.setImageDescriptor(Activator.getImageDescriptor("img/subtree.png"));
		
		return toggleShowSubGraph;
	}
	
	public Action getIncreaseSubGraphAction(){
		increaseSubGraph = new Action("Increase number of successors and antecessors.", 
				Action.AS_PUSH_BUTTON) {
			public void run() {
				increaseSubGraph();
			}
		};
		increaseSubGraph.setToolTipText("Increase number of successors and antecessor.");
		increaseSubGraph.setImageDescriptor(Activator.getImageDescriptor("img/subtreeSizePlus.png"));
		
		return increaseSubGraph;
	}

	public Action getDecreaseSubGraphAction(){
		decreaseSubGraph = new Action("Decrease number of successors and antecessor.", 
				Action.AS_PUSH_BUTTON) {
			public void run() {
				decreaseSubGraph();
			}
		};
		decreaseSubGraph.setToolTipText("Decrease number of successors and antecessor.");
		decreaseSubGraph.setImageDescriptor(Activator.getImageDescriptor("img/subtreeSizeMinus.png"));
		
		return decreaseSubGraph;
	}
	
	public Action getDisplaySizeOfSubGraphAction(){
		displaySizeOfSubGraph = new Action("Number of successors and antecessor.", Action.AS_PUSH_BUTTON){};
		displaySizeOfSubGraph.setToolTipText("Number of successors and antecessor.");
		displaySizeOfSubGraph.setText("#" + Integer.toString(numAncestors));
		displaySizeOfSubGraph.setEnabled(false);
		
		return displaySizeOfSubGraph;
	}
	
	public Action getShowEnabledMessagesOnCurrentStateAction(){
		toggleShowEnabledMessagesOnCurrentState = new Action("Show enabled messages on current state.", 
				Action.AS_CHECK_BOX) {
			public void run() {
				toggleShowEnabledMessagesOnCurrentState();
			}
		};
		toggleShowEnabledMessagesOnCurrentState
				.setToolTipText("Show enabled messages on current state.");
		toggleShowEnabledMessagesOnCurrentState.setImageDescriptor(Activator.getImageDescriptor("img/subtreeEnabledMessages.png"));
		
		return toggleShowEnabledMessagesOnCurrentState;
	}
	
	public Action getShowEnabledMessagesOnAllStatesAction(){
		toggleShowEnabledMessagesOnAllStates = new Action("Show enabled messages on all states.", 
				Action.AS_CHECK_BOX) {
			public void run() {
				toggleShowEnabledMessagesOnAllStates();
			}
		};
		toggleShowEnabledMessagesOnAllStates
				.setToolTipText("Show enabled messages on all states.");
		toggleShowEnabledMessagesOnAllStates.setImageDescriptor(Activator.getImageDescriptor("img/subtreeEnabledMessagesAll.png"));
		
		return toggleShowEnabledMessagesOnAllStates;
	}
	
	public Collection<SMLRuntimeState> getNodes(){
		return viewContentProvider.getNodes();
	}
	
	private void refreshCurrentSimulationGraph(){
		simulationGraphView.refreshCurrentSimulationGraph();
	}
	
	protected void refreshViewContentProvider(){
		
		if (showSubGraph()){
			viewContentProvider = ViewContentProvider.Factory.getSubGraph(simulationGraphView, numAncestors);
		} else if(showFullGraph()){
			viewContentProvider = ViewContentProvider.Factory.getFullGraph(simulationGraphView);
			refreshCurrentSimulationGraph();
			//Workaround, without the graph concentrate the nodes sometimes after toggle display mode
			Display.getCurrent().asyncExec(new Runnable() {
				@Override
				public void run() {
					ViewContentManager.this.refreshCurrentSimulationGraph();
				}
			});
		}else if(showExtendActualStateOnFullGraph()){
			viewContentProvider = ViewContentProvider.Factory.getExtendActualStateOnFullGraph(simulationGraphView);
		}else if(showExtendAllStatesOnFullGraph()){
			viewContentProvider = ViewContentProvider.Factory.getExtendAllStatesOnFullGraph(simulationGraphView);
		}else if(showExtendActualStateOnSubGraph()){
			viewContentProvider = ViewContentProvider.Factory.getExtendActualStateOnSubGraph(simulationGraphView, numAncestors);
		}else if(showExtendAllStatesOnSubGraph()){
			viewContentProvider = ViewContentProvider.Factory.getExtendAllStatesOnSubGraph(simulationGraphView, numAncestors);
		}
		refreshCurrentSimulationGraph();
	}

	private boolean showSubGraph() {
		return showSubGraph && !showEnabledMessagesOnCurrentState && !showEnabledMessagesOnAllStates;
	}
	
	private boolean showFullGraph() {
		return !showSubGraph && !showEnabledMessagesOnCurrentState && !showEnabledMessagesOnAllStates;
	}

	private boolean showExtendActualStateOnFullGraph() {
		return !showSubGraph && showEnabledMessagesOnCurrentState && !showEnabledMessagesOnAllStates;
	}

	private boolean showExtendAllStatesOnFullGraph() {
		return !showSubGraph && showEnabledMessagesOnAllStates;
	}

	private boolean showExtendActualStateOnSubGraph() {
		return showSubGraph && showEnabledMessagesOnCurrentState && !showEnabledMessagesOnAllStates;
	}

	private boolean showExtendAllStatesOnSubGraph() {
		return showSubGraph && showEnabledMessagesOnAllStates;
	}
}
