/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.ui.historyviews.ViewContentProvider;

import java.util.Collection;

import org.scenariotools.sml.debug.ui.historyviews.views.SimulationGraphView;
import org.scenariotools.sml.runtime.SMLRuntimeState;


public interface ViewContentProvider {

	Collection<SMLRuntimeState> getNodes();

	SMLRuntimeState getCurrentState();

	public static class Factory{
		
		public static ViewContentProvider getFullGraph(SimulationGraphView simulationGraphView){
			return new FullGraphContentProvider(simulationGraphView);
		}
		
		public static ViewContentProvider getSubGraph(SimulationGraphView simulationGraphView){
			return new SubGraphContentProvider(simulationGraphView);
		}
		
		public static ViewContentProvider getSubGraph(SimulationGraphView simulationGraphView, int size){
			return new SubGraphContentProvider(simulationGraphView, size);
		}
		
		public static ViewContentProvider getExtendActualStateOnFullGraph(SimulationGraphView simulationGraphView){
			ViewContentProvider baseViewContentProvider = new FullGraphContentProvider(simulationGraphView);
			return new ExtendActualStateWithEnabledMessageContentProvider(baseViewContentProvider);
		}
		
		public static ViewContentProvider getExtendActualStateOnSubGraph(SimulationGraphView simulationGraphView){
			ViewContentProvider baseViewContentProvider = new SubGraphContentProvider(simulationGraphView);
			return new ExtendActualStateWithEnabledMessageContentProvider(baseViewContentProvider);
		}
		
		public static ViewContentProvider getExtendActualStateOnSubGraph(SimulationGraphView simulationGraphView, int size){
			ViewContentProvider baseViewContentProvider = new SubGraphContentProvider(simulationGraphView, size);
			return new ExtendActualStateWithEnabledMessageContentProvider(baseViewContentProvider);
		}
		
		public static ViewContentProvider getExtendAllStatesOnFullGraph(SimulationGraphView simulationGraphView){
			ViewContentProvider baseViewContentProvider = new FullGraphContentProvider(simulationGraphView);
			return new ExtendAllStatesWithEnabledMessageContentProvider(baseViewContentProvider);
		}
		
		public static ViewContentProvider getExtendAllStatesOnSubGraph(SimulationGraphView simulationGraphView){
			ViewContentProvider baseViewContentProvider = new SubGraphContentProvider(simulationGraphView);
			return new ExtendAllStatesWithEnabledMessageContentProvider(baseViewContentProvider);
		}
		
		public static ViewContentProvider getExtendAllStatesOnSubGraph(SimulationGraphView simulationGraphView, int size){
			ViewContentProvider baseViewContentProvider = new SubGraphContentProvider(simulationGraphView, size);
			return new ExtendAllStatesWithEnabledMessageContentProvider(baseViewContentProvider);
		}
	}
}	