/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.ui.historyviews.ViewContentProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.debug.ui.historyviews.model.EnabledMessageTransition;
import org.scenariotools.sml.debug.ui.historyviews.model.NextSMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.Transition;

public abstract class ExtendEnabledMessageContentProvider implements ViewContentProvider {

	protected ViewContentProvider baseViewContentProvider;
	
	//cache for zest optimization
	private Map<CacheKey, NextSMLRuntimeState> eventToNextStateMap;
	
	protected ExtendEnabledMessageContentProvider(ViewContentProvider baseViewContentProvider){
		this.baseViewContentProvider = baseViewContentProvider;
		this.eventToNextStateMap = new HashMap<CacheKey, NextSMLRuntimeState>();
	}
	
	protected Collection<SMLRuntimeState> extendStateWithNextSMLRuntimeState(SMLRuntimeState state){
		Collection<SMLRuntimeState> result = new ArrayList<SMLRuntimeState>();
		
		if(state == null) return result;
		
		for(Event event : state.getEnabledEvents()){
			if(eventIsAlreadyExplored(state, event))continue;
				
			NextSMLRuntimeState next = getTransitionAndNextSMLRuntimeState(event, state);
			result.add(next);
		}
		
		return result;
	}

	private boolean eventIsAlreadyExplored(SMLRuntimeState state, Event event) {
		return state.getEventToTransitionMap().containsKey(event);
	}

	private NextSMLRuntimeState getTransitionAndNextSMLRuntimeState(Event event, SMLRuntimeState state) {
		// return cache if existing
		CacheKey cacheKey = new CacheKey(state, event);
		if(eventToNextStateMap.containsKey(cacheKey)) return eventToNextStateMap.get(cacheKey);
		
		NextSMLRuntimeState next = new NextSMLRuntimeState();
		// TG: use own Transition class to not extend the runtime model
		Transition t = new EnabledMessageTransition();
		t.setEvent(event);
		t.setSourceState(state);
		t.setTargetState(next);
		next.getIncomingTransition().add(t);
		
		// add to cache
		eventToNextStateMap.put(cacheKey, next);
		
		return next;
	}

	@Override
	public SMLRuntimeState getCurrentState() {
		return baseViewContentProvider.getCurrentState();
	}

	private class CacheKey{
		private SMLRuntimeState state;
		private Event event;
		public CacheKey(SMLRuntimeState state, Event event){
			this.state = state;
			this.event = event;
		}
		@Override
		public int hashCode() {
			return state.hashCode() + event.hashCode();
		}
		
		@Override
		public boolean equals(Object obj) {
			return hashCode() == obj.hashCode();
		}
	}
}
