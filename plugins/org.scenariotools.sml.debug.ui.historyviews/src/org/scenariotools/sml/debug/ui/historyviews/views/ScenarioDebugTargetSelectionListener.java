/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.ui.historyviews.views;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.scenariotools.sml.debug.debug.ScenarioDebugTarget;

final class ScenarioDebugTargetSelectionListener implements ISelectionListener {
	
	private final SimulationGraphView simulationGraphView;
	private ScenarioDebugTarget filterObject;

	ScenarioDebugTargetSelectionListener(SimulationGraphView simulationGraphView) {
		this.simulationGraphView = simulationGraphView;
	}

	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			Object firstElement = ((IStructuredSelection) selection).getFirstElement();			
			if (firstElement instanceof ScenarioDebugTarget) {
				if(!firstElement.equals(filterObject))
					selectionChanged((ScenarioDebugTarget) firstElement);
			}
		}
	}

	private void selectionChanged(ScenarioDebugTarget scenarioDebugTarget) {
		filterObject = scenarioDebugTarget;
		simulationGraphView.setCurrentSimulationManager(scenarioDebugTarget.getSimulationManager());
		simulationGraphView.refreshCurrentSimulationGraph();
		simulationGraphView.getViewer().refresh();
	}
}