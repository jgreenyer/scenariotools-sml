/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.ui.historyviews.views;

import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.debug.SimulationAgent;
import org.scenariotools.sml.debug.ui.historyviews.model.EnabledMessageTransition;
import org.scenariotools.sml.debug.ui.historyviews.model.NextSMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeState;

final class ChangeActiveStateDoubleClickListener implements IDoubleClickListener {
	/**
	 * 
	 */
	private final SimulationGraphView simulationGraphView;

	/**
	 * @param simulationGraphView
	 */
	ChangeActiveStateDoubleClickListener(SimulationGraphView simulationGraphView) {
		this.simulationGraphView = simulationGraphView;
	}

	@Override
	public void doubleClick(DoubleClickEvent clickEvent) {
		Object obj = ((IStructuredSelection) clickEvent.getSelection()).getFirstElement();
		
		if (obj instanceof NextSMLRuntimeState) {
			// Perform step with event. Set the source state of the event as current state before.
			NextSMLRuntimeState state = (NextSMLRuntimeState) ((IStructuredSelection) clickEvent
					.getSelection()).getFirstElement();

			if(state.getIncomingTransition().size() != 1){
				throw new RuntimeException("NextSMLRuntimeState has wrong count transitions!"); //TG: Should not happen
			}
			
			EnabledMessageTransition nextEventTransition = (EnabledMessageTransition) state.getIncomingTransition().get(0);
			final Event event = nextEventTransition.getEvent();
			
			
			this.simulationGraphView.getCurrentHistoryAgent().setNextState((SMLRuntimeState) nextEventTransition.getSourceState());

			Display.getCurrent().asyncExec(new Runnable() {
				@Override
				public void run() {
					simulationGraphView
							.getCurrentHistoryAgent()
							.getSimulationManager()
							.loadStateFromHistoryAgent(
									simulationGraphView
											.getCurrentHistoryAgent());
					SimulationAgent simulationAgent = simulationGraphView.getCurrentSimulationManager().getActiveSimulationAgent();
					simulationAgent.setNextEvent(event);
					simulationGraphView.getCurrentSimulationManager().performNextStepFromSimulationAgent(simulationAgent);
				}
			});
			
		}else if (obj instanceof SMLRuntimeState) {
			// Change to current state of the simulation
			SMLRuntimeState state = (SMLRuntimeState) ((IStructuredSelection) clickEvent
					.getSelection()).getFirstElement();
			this.simulationGraphView.getCurrentHistoryAgent().setNextState(state);

			Display.getCurrent().asyncExec(new Runnable() {
				@Override
				public void run() {
					simulationGraphView
							.getCurrentHistoryAgent()
							.getSimulationManager()
							.loadStateFromHistoryAgent(
									simulationGraphView
											.getCurrentHistoryAgent());
				}
			});

		}

	}
}