/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.ui.historyviews.views;

import org.scenariotools.sml.debug.listener.ISimulationTraceChangeListener;

final class SimulationTraceChangeListener implements ISimulationTraceChangeListener {

	private final SimulationGraphView simulationGraphView;

	SimulationTraceChangeListener(SimulationGraphView simulationGraphView) {
		this.simulationGraphView = simulationGraphView;
	}

	@Override
	public void simulationTraceChanged() {
		this.simulationGraphView.refreshCurrentSimulationGraph();		
	}
}