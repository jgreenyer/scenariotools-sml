/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.advancedagents.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.scenariotools.sml.debug.advancedagents.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AdvancedagentsFactoryImpl extends EFactoryImpl implements AdvancedagentsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AdvancedagentsFactory init() {
		try {
			AdvancedagentsFactory theAdvancedagentsFactory = (AdvancedagentsFactory)EPackage.Registry.INSTANCE.getEFactory(AdvancedagentsPackage.eNS_URI);
			if (theAdvancedagentsFactory != null) {
				return theAdvancedagentsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new AdvancedagentsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdvancedagentsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case AdvancedagentsPackage.RANDOM_ENVIRONMENT_SIMULATION_AGENT: return createRandomEnvironmentSimulationAgent();
			case AdvancedagentsPackage.RANDOM_SYSTEM_SIMULATION_AGENT: return createRandomSystemSimulationAgent();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RandomEnvironmentSimulationAgent createRandomEnvironmentSimulationAgent() {
		RandomEnvironmentSimulationAgentImpl randomEnvironmentSimulationAgent = new RandomEnvironmentSimulationAgentImpl();
		return randomEnvironmentSimulationAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RandomSystemSimulationAgent createRandomSystemSimulationAgent() {
		RandomSystemSimulationAgentImpl randomSystemSimulationAgent = new RandomSystemSimulationAgentImpl();
		return randomSystemSimulationAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdvancedagentsPackage getAdvancedagentsPackage() {
		return (AdvancedagentsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static AdvancedagentsPackage getPackage() {
		return AdvancedagentsPackage.eINSTANCE;
	}

} //AdvancedagentsFactoryImpl
