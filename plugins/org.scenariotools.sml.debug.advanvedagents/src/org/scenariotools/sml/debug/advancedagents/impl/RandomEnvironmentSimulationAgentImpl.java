/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.advancedagents.impl;

import org.eclipse.emf.ecore.EClass;
import org.scenariotools.sml.debug.advancedagents.AdvancedagentsPackage;
import org.scenariotools.sml.debug.advancedagents.RandomEnvironmentSimulationAgent;
import org.scenariotools.sml.debug.impl.SimulationAgentImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Random Environment Simulation Agent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class RandomEnvironmentSimulationAgentImpl extends SimulationAgentImpl implements RandomEnvironmentSimulationAgent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RandomEnvironmentSimulationAgentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdvancedagentsPackage.Literals.RANDOM_ENVIRONMENT_SIMULATION_AGENT;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void init(){
//		// Check the actual state. Can the environment perform a step.
//				for (ModalMessageEvent e : this.getSimulationManager()
//						.getCurrentMSDRuntimeState()
//						.getMessageEventToModalMessageEventMap().values()) {
//					if (this.getSimulationManager().getCurrentMSDRuntimeState()
//							.isEnvironmentMessageEvent(e.getRepresentedMessageEvent())
//							&& ((MSDModalMessageEvent)e).getSafetyViolatingInActiveProcess().isEmpty()) {
//						this.setNextEvent(e.getRepresentedMessageEvent());
//						System.out
//								.println("RandomEnvironmentSimulationAgent chooses a step: "
//										+ e.getRepresentedMessageEvent()
//												.getMessageName());
//						break;
//					}
//				}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void stepMade() {
//		if(!getSimulationManager().getActiveSimulationAgent().equals(this)){
//			return;
//		}
//		for (IMSDModalMessageEventListChangeListener msdModalMessageEventListChangeListener : getRegisteredMSDModalEventListChangeListener()) {
//			msdModalMessageEventListChangeListener.msdModalMessageEventsChanged((Collection<MSDModalMessageEvent>)(Collection<?>)getSimulationManager().getCurrentMSDRuntimeState().getMessageEventToModalMessageEventMap().values());
//		}
//		// Check the actual state. Can the environment perform a step.
//		List<ModalMessageEvent> noViolatingMessages = new ArrayList<ModalMessageEvent>();
//		for(ModalMessageEvent e : this.getSimulationManager().getCurrentMSDRuntimeState().getMessageEventToModalMessageEventMap().values()){
//			if(this.getSimulationManager().getCurrentMSDRuntimeState().isEnvironmentMessageEvent(e.getRepresentedMessageEvent()) 
//					&& !e.getAssumptionsModality().isSafetyViolating()
//					&& !e.getRequirementsModality().isSafetyViolating()
//					&& ((MSDModalMessageEvent)e).getSafetyViolatingInActiveProcess().isEmpty() 
//					&& !e.getRepresentedMessageEvent().equals(getNextEvent())){
//				
//				noViolatingMessages.add(e);
//			}
//		}
//		
//		if(noViolatingMessages.isEmpty()){
//			System.out.println("RandomEnvironmentimulationAgent doesnt choose: Now we produce a BUG!");
//		}else{
//			int randomEventIndex = (int) Math.floor(Math.random() * noViolatingMessages.size());
//			this.setNextEvent(noViolatingMessages.get(randomEventIndex).getRepresentedMessageEvent());
//			System.out.println("RandomEnvironmentimulationAgent chooses a step: " 
//						+ noViolatingMessages.get(randomEventIndex).getRepresentedMessageEvent().getMessageName());
//		}
	}
} //RandomEnvironmentSimulationAgentImpl
