/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtext.linking.ILinkingDiagnosticMessageProvider;
import org.eclipse.xtext.ui.editor.model.edit.ITextEditComposer;
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfiguration;
import org.eclipse.xtext.ide.editor.syntaxcoloring.AbstractAntlrTokenToAttributeIdMapper;
import org.eclipse.xtext.ide.editor.syntaxcoloring.DefaultSemanticHighlightingCalculator;
import org.scenariotools.sml.collaboration.ui.CollaborationAntlrTokenToAttributeIdMapper;
import org.scenariotools.sml.collaboration.ui.CollaborationHighlightingConfiguration;
import org.scenariotools.sml.collaboration.ui.CollaborationSemanticHighlightingCalculator;
import org.scenariotools.sml.ui.linking.SMLLinkingDiagnosticMessageProvider;
import org.scenariotools.sml.ui.quickfix.SMLTextEditComposer;

import com.google.inject.Binder;

/**
 * Use this class to register components to be used within the IDE.
 */
public class SMLUiModule extends org.scenariotools.sml.ui.AbstractSMLUiModule {
	public SMLUiModule(AbstractUIPlugin plugin) {
		super(plugin);
	}
	
	/*
	 * This adds a custom LinkingDiagnosticMessageProvider that extends the
	 * error messages of missing references.
	 */
	public Class<? extends ILinkingDiagnosticMessageProvider> bindILinkingDiagnosticMessageProvider() {
		return SMLLinkingDiagnosticMessageProvider.class;
	}

	/*
	 * This adds a custom TextEditComposer that enables auto formatting after
	 * using quickfixes.
	 */
	public Class<? extends ITextEditComposer> bindITextEditComposer() {
		return SMLTextEditComposer.class;
	}
	
	public Class<? extends AbstractAntlrTokenToAttributeIdMapper> bindAbstractAntlrTokenToAttributeIdMapper() {
		return CollaborationAntlrTokenToAttributeIdMapper.class;
	}

	public Class<? extends IHighlightingConfiguration> bindILexicalHighlightingConfiguration() {
		return CollaborationHighlightingConfiguration.class;
	}
	
	public void configure(Binder binder) {
		super.configure(binder);
		binder.bind(DefaultSemanticHighlightingCalculator.class).to(CollaborationSemanticHighlightingCalculator.class);
	}
}
