/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.sml.ui.linking

import org.eclipse.xtext.diagnostics.DiagnosticMessage
import org.scenariotools.sml.collaboration.linking.CollaborationLinkingDiagnosticMessageProvider

class SMLLinkingDiagnosticMessageProvider extends CollaborationLinkingDiagnosticMessageProvider {

	override DiagnosticMessage getUnresolvedProxyMessage(ILinkingDiagnosticContext context) {
			
		val superMessage = super.getUnresolvedProxyMessage(context);
		var newMessage = superMessage

		
		return newMessage

	}

}
