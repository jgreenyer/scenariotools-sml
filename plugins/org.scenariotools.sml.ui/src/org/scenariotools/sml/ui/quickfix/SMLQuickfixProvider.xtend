/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.ui.quickfix

import org.scenariotools.sml.collaboration.ui.quickfix.CollaborationQuickfixProvider
import org.scenariotools.sml.validation.SMLIssueCodes
import org.eclipse.xtext.validation.Issue
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor
import org.eclipse.xtext.ui.editor.quickfix.Fix
import org.eclipse.xtext.ui.editor.model.edit.ISemanticModification
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext
import org.scenariotools.sml.StringRanges
import org.scenariotools.sml.IntegerRanges
import org.scenariotools.sml.EnumRanges

/**
 * Custom quickfixes.
 * 
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#quick-fixes
 */
class SMLQuickfixProvider extends CollaborationQuickfixProvider {

	@Fix(SMLIssueCodes::RANGES_VALUE_IS_DUPLICATE)
	def fixDuplicateRangesDefinition(Issue issue, IssueResolutionAcceptor acceptor) {
		acceptor.accept(
			issue,
			'Remove value',
			'Remove value.',
			null,
			new ISemanticModification() {
				override apply(EObject element, IModificationContext context) throws Exception {
					if (element instanceof IntegerRanges) {
						element.values.remove(Integer.parseInt(issue.data.get(0)))
					} else if (element instanceof StringRanges) {
						element.values.remove(Integer.parseInt(issue.data.get(0)))
					} else if (element instanceof EnumRanges) {
						element.values.remove(Integer.parseInt(issue.data.get(0)))
					}
				}
			}
		)
	}
	
	@Fix(SMLIssueCodes::RANGES_VALUE_CONTAINED_IN_INTERVAL)
	def fixRangesValueContainedInInterval(Issue issue, IssueResolutionAcceptor acceptor) {
		acceptor.accept(
			issue,
			'Remove value',
			'Remove value.',
			null,
			new ISemanticModification() {
				override apply(EObject element, IModificationContext context) throws Exception {
					val integerRanges = element as IntegerRanges
					integerRanges.values.remove(Integer.parseInt(issue.data.get(0)))
				}
			}
		)
	}

}
