/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.ui.contentassist.antlr;

import java.util.Collection;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.RecognitionException;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.ui.editor.contentassist.antlr.AbstractContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.FollowElement;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;

import com.google.inject.Inject;

import org.scenariotools.sml.services.SMLGrammarAccess;

public class SMLParser extends AbstractContentAssistParser {
	
	@Inject
	private SMLGrammarAccess grammarAccess;
	
	private Map<AbstractElement, String> nameMappings;
	
	@Override
	protected org.scenariotools.sml.ui.contentassist.antlr.internal.InternalSMLParser createParser() {
		org.scenariotools.sml.ui.contentassist.antlr.internal.InternalSMLParser result = new org.scenariotools.sml.ui.contentassist.antlr.internal.InternalSMLParser(null);
		result.setGrammarAccess(grammarAccess);
		return result;
	}
	
	@Override
	protected String getRuleName(AbstractElement element) {
		if (nameMappings == null) {
			nameMappings = new HashMap<AbstractElement, String>() {
				private static final long serialVersionUID = 1L;
				{
					put(grammarAccess.getSpecificationAccess().getAlternatives_10(), "rule__Specification__Alternatives_10");
					put(grammarAccess.getAbstractRangesAccess().getAlternatives_1(), "rule__AbstractRanges__Alternatives_1");
					put(grammarAccess.getIntegerRangesAccess().getAlternatives(), "rule__IntegerRanges__Alternatives");
					put(grammarAccess.getIntegerRangesAccess().getMinAlternatives_0_0_0_0(), "rule__IntegerRanges__MinAlternatives_0_0_0_0");
					put(grammarAccess.getIntegerRangesAccess().getMaxAlternatives_0_0_2_0(), "rule__IntegerRanges__MaxAlternatives_0_0_2_0");
					put(grammarAccess.getIntegerRangesAccess().getValuesAlternatives_0_1_1_0_0(), "rule__IntegerRanges__ValuesAlternatives_0_1_1_0_0");
					put(grammarAccess.getIntegerRangesAccess().getValuesAlternatives_0_1_1_1_1_0(), "rule__IntegerRanges__ValuesAlternatives_0_1_1_1_1_0");
					put(grammarAccess.getIntegerRangesAccess().getValuesAlternatives_1_0_0(), "rule__IntegerRanges__ValuesAlternatives_1_0_0");
					put(grammarAccess.getIntegerRangesAccess().getValuesAlternatives_1_1_1_0(), "rule__IntegerRanges__ValuesAlternatives_1_1_1_0");
					put(grammarAccess.getRoleAccess().getAlternatives_0(), "rule__Role__Alternatives_0");
					put(grammarAccess.getScenarioAccess().getAlternatives_4(), "rule__Scenario__Alternatives_4");
					put(grammarAccess.getInteractionFragmentAccess().getAlternatives(), "rule__InteractionFragment__Alternatives");
					put(grammarAccess.getVariableFragmentAccess().getExpressionAlternatives_0(), "rule__VariableFragment__ExpressionAlternatives_0");
					put(grammarAccess.getParameterExpressionAccess().getAlternatives(), "rule__ParameterExpression__Alternatives");
					put(grammarAccess.getTimedConditionFragmentAccess().getAlternatives(), "rule__TimedConditionFragment__Alternatives");
					put(grammarAccess.getConditionFragmentAccess().getAlternatives(), "rule__ConditionFragment__Alternatives");
					put(grammarAccess.getConstraintBlockAccess().getAlternatives_3(), "rule__ConstraintBlock__Alternatives_3");
					put(grammarAccess.getExpressionOrRegionAccess().getAlternatives(), "rule__ExpressionOrRegion__Alternatives");
					put(grammarAccess.getExpressionAndVariablesAccess().getAlternatives(), "rule__ExpressionAndVariables__Alternatives");
					put(grammarAccess.getVariableExpressionAccess().getAlternatives(), "rule__VariableExpression__Alternatives");
					put(grammarAccess.getRelationExpressionAccess().getOperatorAlternatives_1_1_0(), "rule__RelationExpression__OperatorAlternatives_1_1_0");
					put(grammarAccess.getTimedExpressionAccess().getOperatorAlternatives_1_0(), "rule__TimedExpression__OperatorAlternatives_1_0");
					put(grammarAccess.getAdditionExpressionAccess().getOperatorAlternatives_1_1_0(), "rule__AdditionExpression__OperatorAlternatives_1_1_0");
					put(grammarAccess.getMultiplicationExpressionAccess().getOperatorAlternatives_1_1_0(), "rule__MultiplicationExpression__OperatorAlternatives_1_1_0");
					put(grammarAccess.getNegatedExpressionAccess().getAlternatives(), "rule__NegatedExpression__Alternatives");
					put(grammarAccess.getNegatedExpressionAccess().getOperatorAlternatives_0_1_0(), "rule__NegatedExpression__OperatorAlternatives_0_1_0");
					put(grammarAccess.getBasicExpressionAccess().getAlternatives(), "rule__BasicExpression__Alternatives");
					put(grammarAccess.getValueAccess().getAlternatives(), "rule__Value__Alternatives");
					put(grammarAccess.getIntegerValueAccess().getValueAlternatives_0(), "rule__IntegerValue__ValueAlternatives_0");
					put(grammarAccess.getFeatureAccessAccess().getAlternatives_2(), "rule__FeatureAccess__Alternatives_2");
					put(grammarAccess.getScenarioKindAccess().getAlternatives(), "rule__ScenarioKind__Alternatives");
					put(grammarAccess.getExpectationKindAccess().getAlternatives(), "rule__ExpectationKind__Alternatives");
					put(grammarAccess.getCollectionOperationAccess().getAlternatives(), "rule__CollectionOperation__Alternatives");
					put(grammarAccess.getCollectionModificationAccess().getAlternatives(), "rule__CollectionModification__Alternatives");
					put(grammarAccess.getSpecificationAccess().getGroup(), "rule__Specification__Group__0");
					put(grammarAccess.getSpecificationAccess().getGroup_4(), "rule__Specification__Group_4__0");
					put(grammarAccess.getSpecificationAccess().getGroup_4_0(), "rule__Specification__Group_4_0__0");
					put(grammarAccess.getSpecificationAccess().getGroup_5(), "rule__Specification__Group_5__0");
					put(grammarAccess.getSpecificationAccess().getGroup_6(), "rule__Specification__Group_6__0");
					put(grammarAccess.getSpecificationAccess().getGroup_8(), "rule__Specification__Group_8__0");
					put(grammarAccess.getSpecificationAccess().getGroup_8_2(), "rule__Specification__Group_8_2__0");
					put(grammarAccess.getSpecificationAccess().getGroup_9(), "rule__Specification__Group_9__0");
					put(grammarAccess.getSpecificationAccess().getGroup_9_2(), "rule__Specification__Group_9_2__0");
					put(grammarAccess.getSpecificationAccess().getGroup_9_2_1(), "rule__Specification__Group_9_2_1__0");
					put(grammarAccess.getSpecificationAccess().getGroup_10_1(), "rule__Specification__Group_10_1__0");
					put(grammarAccess.getFQNENUMAccess().getGroup(), "rule__FQNENUM__Group__0");
					put(grammarAccess.getFQNENUMAccess().getGroup_1(), "rule__FQNENUM__Group_1__0");
					put(grammarAccess.getChannelOptionsAccess().getGroup(), "rule__ChannelOptions__Group__0");
					put(grammarAccess.getChannelOptionsAccess().getGroup_1(), "rule__ChannelOptions__Group_1__0");
					put(grammarAccess.getMessageChannelAccess().getGroup(), "rule__MessageChannel__Group__0");
					put(grammarAccess.getNestedCollaborationAccess().getGroup(), "rule__NestedCollaboration__Group__0");
					put(grammarAccess.getEventParameterRangesAccess().getGroup(), "rule__EventParameterRanges__Group__0");
					put(grammarAccess.getEventParameterRangesAccess().getGroup_3(), "rule__EventParameterRanges__Group_3__0");
					put(grammarAccess.getRangesForParameterAccess().getGroup(), "rule__RangesForParameter__Group__0");
					put(grammarAccess.getAbstractRangesAccess().getGroup(), "rule__AbstractRanges__Group__0");
					put(grammarAccess.getIntegerRangesAccess().getGroup_0(), "rule__IntegerRanges__Group_0__0");
					put(grammarAccess.getIntegerRangesAccess().getGroup_0_0(), "rule__IntegerRanges__Group_0_0__0");
					put(grammarAccess.getIntegerRangesAccess().getGroup_0_1(), "rule__IntegerRanges__Group_0_1__0");
					put(grammarAccess.getIntegerRangesAccess().getGroup_0_1_1(), "rule__IntegerRanges__Group_0_1_1__0");
					put(grammarAccess.getIntegerRangesAccess().getGroup_0_1_1_1(), "rule__IntegerRanges__Group_0_1_1_1__0");
					put(grammarAccess.getIntegerRangesAccess().getGroup_1(), "rule__IntegerRanges__Group_1__0");
					put(grammarAccess.getIntegerRangesAccess().getGroup_1_1(), "rule__IntegerRanges__Group_1_1__0");
					put(grammarAccess.getStringRangesAccess().getGroup(), "rule__StringRanges__Group__0");
					put(grammarAccess.getStringRangesAccess().getGroup_1(), "rule__StringRanges__Group_1__0");
					put(grammarAccess.getEnumRangesAccess().getGroup(), "rule__EnumRanges__Group__0");
					put(grammarAccess.getEnumRangesAccess().getGroup_1(), "rule__EnumRanges__Group_1__0");
					put(grammarAccess.getCollaborationAccess().getGroup(), "rule__Collaboration__Group__0");
					put(grammarAccess.getCollaborationAccess().getGroup_1(), "rule__Collaboration__Group_1__0");
					put(grammarAccess.getCollaborationAccess().getGroup_2(), "rule__Collaboration__Group_2__0");
					put(grammarAccess.getFQNAccess().getGroup(), "rule__FQN__Group__0");
					put(grammarAccess.getFQNAccess().getGroup_1(), "rule__FQN__Group_1__0");
					put(grammarAccess.getRoleAccess().getGroup(), "rule__Role__Group__0");
					put(grammarAccess.getRoleAccess().getGroup_0_1(), "rule__Role__Group_0_1__0");
					put(grammarAccess.getScenarioAccess().getGroup(), "rule__Scenario__Group__0");
					put(grammarAccess.getScenarioAccess().getGroup_4_0(), "rule__Scenario__Group_4_0__0");
					put(grammarAccess.getScenarioAccess().getGroup_4_1(), "rule__Scenario__Group_4_1__0");
					put(grammarAccess.getScenarioAccess().getGroup_5(), "rule__Scenario__Group_5__0");
					put(grammarAccess.getScenarioAccess().getGroup_5_2(), "rule__Scenario__Group_5_2__0");
					put(grammarAccess.getScenarioAccess().getGroup_6(), "rule__Scenario__Group_6__0");
					put(grammarAccess.getRoleBindingConstraintAccess().getGroup(), "rule__RoleBindingConstraint__Group__0");
					put(grammarAccess.getInteractionAccess().getGroup(), "rule__Interaction__Group__0");
					put(grammarAccess.getModalMessageAccess().getGroup(), "rule__ModalMessage__Group__0");
					put(grammarAccess.getModalMessageAccess().getGroup_1(), "rule__ModalMessage__Group_1__0");
					put(grammarAccess.getModalMessageAccess().getGroup_7(), "rule__ModalMessage__Group_7__0");
					put(grammarAccess.getModalMessageAccess().getGroup_8(), "rule__ModalMessage__Group_8__0");
					put(grammarAccess.getModalMessageAccess().getGroup_8_1(), "rule__ModalMessage__Group_8_1__0");
					put(grammarAccess.getModalMessageAccess().getGroup_8_1_1(), "rule__ModalMessage__Group_8_1_1__0");
					put(grammarAccess.getWildcardParameterExpressionAccess().getGroup(), "rule__WildcardParameterExpression__Group__0");
					put(grammarAccess.getVariableBindingParameterExpressionAccess().getGroup(), "rule__VariableBindingParameterExpression__Group__0");
					put(grammarAccess.getAlternativeAccess().getGroup(), "rule__Alternative__Group__0");
					put(grammarAccess.getAlternativeAccess().getGroup_3(), "rule__Alternative__Group_3__0");
					put(grammarAccess.getCaseAccess().getGroup(), "rule__Case__Group__0");
					put(grammarAccess.getLoopAccess().getGroup(), "rule__Loop__Group__0");
					put(grammarAccess.getParallelAccess().getGroup(), "rule__Parallel__Group__0");
					put(grammarAccess.getParallelAccess().getGroup_3(), "rule__Parallel__Group_3__0");
					put(grammarAccess.getWaitConditionAccess().getGroup(), "rule__WaitCondition__Group__0");
					put(grammarAccess.getInterruptConditionAccess().getGroup(), "rule__InterruptCondition__Group__0");
					put(grammarAccess.getViolationConditionAccess().getGroup(), "rule__ViolationCondition__Group__0");
					put(grammarAccess.getTimedWaitConditionAccess().getGroup(), "rule__TimedWaitCondition__Group__0");
					put(grammarAccess.getTimedViolationConditionAccess().getGroup(), "rule__TimedViolationCondition__Group__0");
					put(grammarAccess.getTimedInterruptConditionAccess().getGroup(), "rule__TimedInterruptCondition__Group__0");
					put(grammarAccess.getConditionAccess().getGroup(), "rule__Condition__Group__0");
					put(grammarAccess.getConstraintBlockAccess().getGroup(), "rule__ConstraintBlock__Group__0");
					put(grammarAccess.getConstraintBlockAccess().getGroup_3_0(), "rule__ConstraintBlock__Group_3_0__0");
					put(grammarAccess.getConstraintBlockAccess().getGroup_3_1(), "rule__ConstraintBlock__Group_3_1__0");
					put(grammarAccess.getConstraintBlockAccess().getGroup_3_2(), "rule__ConstraintBlock__Group_3_2__0");
					put(grammarAccess.getConstraintBlockAccess().getGroup_3_3(), "rule__ConstraintBlock__Group_3_3__0");
					put(grammarAccess.getConstraintMessageAccess().getGroup(), "rule__ConstraintMessage__Group__0");
					put(grammarAccess.getConstraintMessageAccess().getGroup_5(), "rule__ConstraintMessage__Group_5__0");
					put(grammarAccess.getConstraintMessageAccess().getGroup_7(), "rule__ConstraintMessage__Group_7__0");
					put(grammarAccess.getConstraintMessageAccess().getGroup_7_1(), "rule__ConstraintMessage__Group_7_1__0");
					put(grammarAccess.getDocumentAccess().getGroup(), "rule__Document__Group__0");
					put(grammarAccess.getDocumentAccess().getGroup_1(), "rule__Document__Group_1__0");
					put(grammarAccess.getImportAccess().getGroup(), "rule__Import__Group__0");
					put(grammarAccess.getExpressionRegionAccess().getGroup(), "rule__ExpressionRegion__Group__0");
					put(grammarAccess.getExpressionRegionAccess().getGroup_2(), "rule__ExpressionRegion__Group_2__0");
					put(grammarAccess.getVariableDeclarationAccess().getGroup(), "rule__VariableDeclaration__Group__0");
					put(grammarAccess.getVariableAssignmentAccess().getGroup(), "rule__VariableAssignment__Group__0");
					put(grammarAccess.getTypedVariableDeclarationAccess().getGroup(), "rule__TypedVariableDeclaration__Group__0");
					put(grammarAccess.getTypedVariableDeclarationAccess().getGroup_3(), "rule__TypedVariableDeclaration__Group_3__0");
					put(grammarAccess.getClockDeclarationAccess().getGroup(), "rule__ClockDeclaration__Group__0");
					put(grammarAccess.getClockDeclarationAccess().getGroup_3(), "rule__ClockDeclaration__Group_3__0");
					put(grammarAccess.getClockAccess().getGroup(), "rule__Clock__Group__0");
					put(grammarAccess.getClockAssignmentAccess().getGroup(), "rule__ClockAssignment__Group__0");
					put(grammarAccess.getClockAssignmentAccess().getGroup_2(), "rule__ClockAssignment__Group_2__0");
					put(grammarAccess.getImplicationExpressionAccess().getGroup(), "rule__ImplicationExpression__Group__0");
					put(grammarAccess.getImplicationExpressionAccess().getGroup_1(), "rule__ImplicationExpression__Group_1__0");
					put(grammarAccess.getDisjunctionExpressionAccess().getGroup(), "rule__DisjunctionExpression__Group__0");
					put(grammarAccess.getDisjunctionExpressionAccess().getGroup_1(), "rule__DisjunctionExpression__Group_1__0");
					put(grammarAccess.getConjunctionExpressionAccess().getGroup(), "rule__ConjunctionExpression__Group__0");
					put(grammarAccess.getConjunctionExpressionAccess().getGroup_1(), "rule__ConjunctionExpression__Group_1__0");
					put(grammarAccess.getRelationExpressionAccess().getGroup(), "rule__RelationExpression__Group__0");
					put(grammarAccess.getRelationExpressionAccess().getGroup_1(), "rule__RelationExpression__Group_1__0");
					put(grammarAccess.getTimedExpressionAccess().getGroup(), "rule__TimedExpression__Group__0");
					put(grammarAccess.getAdditionExpressionAccess().getGroup(), "rule__AdditionExpression__Group__0");
					put(grammarAccess.getAdditionExpressionAccess().getGroup_1(), "rule__AdditionExpression__Group_1__0");
					put(grammarAccess.getMultiplicationExpressionAccess().getGroup(), "rule__MultiplicationExpression__Group__0");
					put(grammarAccess.getMultiplicationExpressionAccess().getGroup_1(), "rule__MultiplicationExpression__Group_1__0");
					put(grammarAccess.getNegatedExpressionAccess().getGroup_0(), "rule__NegatedExpression__Group_0__0");
					put(grammarAccess.getBasicExpressionAccess().getGroup_1(), "rule__BasicExpression__Group_1__0");
					put(grammarAccess.getEnumValueAccess().getGroup(), "rule__EnumValue__Group__0");
					put(grammarAccess.getNullValueAccess().getGroup(), "rule__NullValue__Group__0");
					put(grammarAccess.getCollectionAccessAccess().getGroup(), "rule__CollectionAccess__Group__0");
					put(grammarAccess.getFeatureAccessAccess().getGroup(), "rule__FeatureAccess__Group__0");
					put(grammarAccess.getFeatureAccessAccess().getGroup_2_0(), "rule__FeatureAccess__Group_2_0__0");
					put(grammarAccess.getFeatureAccessAccess().getGroup_2_0_1(), "rule__FeatureAccess__Group_2_0_1__0");
					put(grammarAccess.getFeatureAccessAccess().getGroup_2_1(), "rule__FeatureAccess__Group_2_1__0");
					put(grammarAccess.getFeatureAccessAccess().getGroup_2_1_2(), "rule__FeatureAccess__Group_2_1_2__0");
					put(grammarAccess.getFeatureAccessAccess().getGroup_2_1_2_1(), "rule__FeatureAccess__Group_2_1_2_1__0");
					put(grammarAccess.getSpecificationAccess().getImportsAssignment_0(), "rule__Specification__ImportsAssignment_0");
					put(grammarAccess.getSpecificationAccess().getNameAssignment_2(), "rule__Specification__NameAssignment_2");
					put(grammarAccess.getSpecificationAccess().getDomainsAssignment_4_0_1(), "rule__Specification__DomainsAssignment_4_0_1");
					put(grammarAccess.getSpecificationAccess().getContextsAssignment_5_1(), "rule__Specification__ContextsAssignment_5_1");
					put(grammarAccess.getSpecificationAccess().getControllableEClassesAssignment_6_2(), "rule__Specification__ControllableEClassesAssignment_6_2");
					put(grammarAccess.getSpecificationAccess().getChannelOptionsAssignment_7(), "rule__Specification__ChannelOptionsAssignment_7");
					put(grammarAccess.getSpecificationAccess().getNonSpontaneousOperationsAssignment_8_2_0(), "rule__Specification__NonSpontaneousOperationsAssignment_8_2_0");
					put(grammarAccess.getSpecificationAccess().getNonSpontaneousOperationsAssignment_8_2_1(), "rule__Specification__NonSpontaneousOperationsAssignment_8_2_1");
					put(grammarAccess.getSpecificationAccess().getEventParameterRangesAssignment_9_2_0(), "rule__Specification__EventParameterRangesAssignment_9_2_0");
					put(grammarAccess.getSpecificationAccess().getEventParameterRangesAssignment_9_2_1_1(), "rule__Specification__EventParameterRangesAssignment_9_2_1_1");
					put(grammarAccess.getSpecificationAccess().getContainedCollaborationsAssignment_10_0(), "rule__Specification__ContainedCollaborationsAssignment_10_0");
					put(grammarAccess.getSpecificationAccess().getIncludedCollaborationsAssignment_10_1_1(), "rule__Specification__IncludedCollaborationsAssignment_10_1_1");
					put(grammarAccess.getChannelOptionsAccess().getAllMessagesRequireLinksAssignment_1_2(), "rule__ChannelOptions__AllMessagesRequireLinksAssignment_1_2");
					put(grammarAccess.getChannelOptionsAccess().getMessageChannelsAssignment_1_3(), "rule__ChannelOptions__MessageChannelsAssignment_1_3");
					put(grammarAccess.getMessageChannelAccess().getEventAssignment_0(), "rule__MessageChannel__EventAssignment_0");
					put(grammarAccess.getMessageChannelAccess().getChannelFeatureAssignment_2(), "rule__MessageChannel__ChannelFeatureAssignment_2");
					put(grammarAccess.getNestedCollaborationAccess().getNameAssignment_1(), "rule__NestedCollaboration__NameAssignment_1");
					put(grammarAccess.getNestedCollaborationAccess().getRolesAssignment_3(), "rule__NestedCollaboration__RolesAssignment_3");
					put(grammarAccess.getNestedCollaborationAccess().getScenariosAssignment_4(), "rule__NestedCollaboration__ScenariosAssignment_4");
					put(grammarAccess.getEventParameterRangesAccess().getEventAssignment_0(), "rule__EventParameterRanges__EventAssignment_0");
					put(grammarAccess.getEventParameterRangesAccess().getRangesForParameterAssignment_2(), "rule__EventParameterRanges__RangesForParameterAssignment_2");
					put(grammarAccess.getEventParameterRangesAccess().getRangesForParameterAssignment_3_1(), "rule__EventParameterRanges__RangesForParameterAssignment_3_1");
					put(grammarAccess.getRangesForParameterAccess().getParameterAssignment_0(), "rule__RangesForParameter__ParameterAssignment_0");
					put(grammarAccess.getRangesForParameterAccess().getRangesAssignment_2(), "rule__RangesForParameter__RangesAssignment_2");
					put(grammarAccess.getIntegerRangesAccess().getMinAssignment_0_0_0(), "rule__IntegerRanges__MinAssignment_0_0_0");
					put(grammarAccess.getIntegerRangesAccess().getMaxAssignment_0_0_2(), "rule__IntegerRanges__MaxAssignment_0_0_2");
					put(grammarAccess.getIntegerRangesAccess().getValuesAssignment_0_1_1_0(), "rule__IntegerRanges__ValuesAssignment_0_1_1_0");
					put(grammarAccess.getIntegerRangesAccess().getValuesAssignment_0_1_1_1_1(), "rule__IntegerRanges__ValuesAssignment_0_1_1_1_1");
					put(grammarAccess.getIntegerRangesAccess().getValuesAssignment_1_0(), "rule__IntegerRanges__ValuesAssignment_1_0");
					put(grammarAccess.getIntegerRangesAccess().getValuesAssignment_1_1_1(), "rule__IntegerRanges__ValuesAssignment_1_1_1");
					put(grammarAccess.getStringRangesAccess().getValuesAssignment_0(), "rule__StringRanges__ValuesAssignment_0");
					put(grammarAccess.getStringRangesAccess().getValuesAssignment_1_1(), "rule__StringRanges__ValuesAssignment_1_1");
					put(grammarAccess.getEnumRangesAccess().getValuesAssignment_0(), "rule__EnumRanges__ValuesAssignment_0");
					put(grammarAccess.getEnumRangesAccess().getValuesAssignment_1_1(), "rule__EnumRanges__ValuesAssignment_1_1");
					put(grammarAccess.getCollaborationAccess().getImportsAssignment_0(), "rule__Collaboration__ImportsAssignment_0");
					put(grammarAccess.getCollaborationAccess().getDomainsAssignment_1_1(), "rule__Collaboration__DomainsAssignment_1_1");
					put(grammarAccess.getCollaborationAccess().getContextsAssignment_2_1(), "rule__Collaboration__ContextsAssignment_2_1");
					put(grammarAccess.getCollaborationAccess().getNameAssignment_4(), "rule__Collaboration__NameAssignment_4");
					put(grammarAccess.getCollaborationAccess().getRolesAssignment_6(), "rule__Collaboration__RolesAssignment_6");
					put(grammarAccess.getCollaborationAccess().getScenariosAssignment_7(), "rule__Collaboration__ScenariosAssignment_7");
					put(grammarAccess.getRoleAccess().getStaticAssignment_0_0(), "rule__Role__StaticAssignment_0_0");
					put(grammarAccess.getRoleAccess().getMultiRoleAssignment_0_1_1(), "rule__Role__MultiRoleAssignment_0_1_1");
					put(grammarAccess.getRoleAccess().getTypeAssignment_2(), "rule__Role__TypeAssignment_2");
					put(grammarAccess.getRoleAccess().getNameAssignment_3(), "rule__Role__NameAssignment_3");
					put(grammarAccess.getScenarioAccess().getSingularAssignment_0(), "rule__Scenario__SingularAssignment_0");
					put(grammarAccess.getScenarioAccess().getKindAssignment_1(), "rule__Scenario__KindAssignment_1");
					put(grammarAccess.getScenarioAccess().getNameAssignment_3(), "rule__Scenario__NameAssignment_3");
					put(grammarAccess.getScenarioAccess().getOptimizeCostAssignment_4_0_0(), "rule__Scenario__OptimizeCostAssignment_4_0_0");
					put(grammarAccess.getScenarioAccess().getCostAssignment_4_1_2(), "rule__Scenario__CostAssignment_4_1_2");
					put(grammarAccess.getScenarioAccess().getContextsAssignment_5_1(), "rule__Scenario__ContextsAssignment_5_1");
					put(grammarAccess.getScenarioAccess().getContextsAssignment_5_2_1(), "rule__Scenario__ContextsAssignment_5_2_1");
					put(grammarAccess.getScenarioAccess().getRoleBindingsAssignment_6_2(), "rule__Scenario__RoleBindingsAssignment_6_2");
					put(grammarAccess.getScenarioAccess().getOwnedInteractionAssignment_7(), "rule__Scenario__OwnedInteractionAssignment_7");
					put(grammarAccess.getRoleBindingConstraintAccess().getRoleAssignment_0(), "rule__RoleBindingConstraint__RoleAssignment_0");
					put(grammarAccess.getRoleBindingConstraintAccess().getBindingExpressionAssignment_2(), "rule__RoleBindingConstraint__BindingExpressionAssignment_2");
					put(grammarAccess.getFeatureAccessBindingExpressionAccess().getFeatureaccessAssignment(), "rule__FeatureAccessBindingExpression__FeatureaccessAssignment");
					put(grammarAccess.getVariableFragmentAccess().getExpressionAssignment(), "rule__VariableFragment__ExpressionAssignment");
					put(grammarAccess.getInteractionAccess().getFragmentsAssignment_2(), "rule__Interaction__FragmentsAssignment_2");
					put(grammarAccess.getInteractionAccess().getConstraintsAssignment_4(), "rule__Interaction__ConstraintsAssignment_4");
					put(grammarAccess.getModalMessageAccess().getStrictAssignment_0(), "rule__ModalMessage__StrictAssignment_0");
					put(grammarAccess.getModalMessageAccess().getMonitoredAssignment_1_0(), "rule__ModalMessage__MonitoredAssignment_1_0");
					put(grammarAccess.getModalMessageAccess().getExpectationKindAssignment_1_1(), "rule__ModalMessage__ExpectationKindAssignment_1_1");
					put(grammarAccess.getModalMessageAccess().getSenderAssignment_2(), "rule__ModalMessage__SenderAssignment_2");
					put(grammarAccess.getModalMessageAccess().getReceiverAssignment_4(), "rule__ModalMessage__ReceiverAssignment_4");
					put(grammarAccess.getModalMessageAccess().getModelElementAssignment_6(), "rule__ModalMessage__ModelElementAssignment_6");
					put(grammarAccess.getModalMessageAccess().getCollectionModificationAssignment_7_1(), "rule__ModalMessage__CollectionModificationAssignment_7_1");
					put(grammarAccess.getModalMessageAccess().getParametersAssignment_8_1_0(), "rule__ModalMessage__ParametersAssignment_8_1_0");
					put(grammarAccess.getModalMessageAccess().getParametersAssignment_8_1_1_1(), "rule__ModalMessage__ParametersAssignment_8_1_1_1");
					put(grammarAccess.getParameterBindingAccess().getBindingExpressionAssignment(), "rule__ParameterBinding__BindingExpressionAssignment");
					put(grammarAccess.getValueParameterExpressionAccess().getValueAssignment(), "rule__ValueParameterExpression__ValueAssignment");
					put(grammarAccess.getVariableBindingParameterExpressionAccess().getVariableAssignment_1(), "rule__VariableBindingParameterExpression__VariableAssignment_1");
					put(grammarAccess.getAlternativeAccess().getCasesAssignment_2(), "rule__Alternative__CasesAssignment_2");
					put(grammarAccess.getAlternativeAccess().getCasesAssignment_3_1(), "rule__Alternative__CasesAssignment_3_1");
					put(grammarAccess.getCaseAccess().getCaseConditionAssignment_1(), "rule__Case__CaseConditionAssignment_1");
					put(grammarAccess.getCaseAccess().getCaseInteractionAssignment_2(), "rule__Case__CaseInteractionAssignment_2");
					put(grammarAccess.getLoopAccess().getLoopConditionAssignment_1(), "rule__Loop__LoopConditionAssignment_1");
					put(grammarAccess.getLoopAccess().getBodyInteractionAssignment_2(), "rule__Loop__BodyInteractionAssignment_2");
					put(grammarAccess.getParallelAccess().getParallelInteractionAssignment_2(), "rule__Parallel__ParallelInteractionAssignment_2");
					put(grammarAccess.getParallelAccess().getParallelInteractionAssignment_3_1(), "rule__Parallel__ParallelInteractionAssignment_3_1");
					put(grammarAccess.getWaitConditionAccess().getStrictAssignment_1(), "rule__WaitCondition__StrictAssignment_1");
					put(grammarAccess.getWaitConditionAccess().getRequestedAssignment_2(), "rule__WaitCondition__RequestedAssignment_2");
					put(grammarAccess.getWaitConditionAccess().getConditionExpressionAssignment_4(), "rule__WaitCondition__ConditionExpressionAssignment_4");
					put(grammarAccess.getInterruptConditionAccess().getConditionExpressionAssignment_2(), "rule__InterruptCondition__ConditionExpressionAssignment_2");
					put(grammarAccess.getViolationConditionAccess().getConditionExpressionAssignment_2(), "rule__ViolationCondition__ConditionExpressionAssignment_2");
					put(grammarAccess.getTimedWaitConditionAccess().getStrictAssignment_2(), "rule__TimedWaitCondition__StrictAssignment_2");
					put(grammarAccess.getTimedWaitConditionAccess().getRequestedAssignment_3(), "rule__TimedWaitCondition__RequestedAssignment_3");
					put(grammarAccess.getTimedWaitConditionAccess().getTimedConditionExpressionAssignment_5(), "rule__TimedWaitCondition__TimedConditionExpressionAssignment_5");
					put(grammarAccess.getTimedViolationConditionAccess().getTimedConditionExpressionAssignment_3(), "rule__TimedViolationCondition__TimedConditionExpressionAssignment_3");
					put(grammarAccess.getTimedInterruptConditionAccess().getTimedConditionExpressionAssignment_3(), "rule__TimedInterruptCondition__TimedConditionExpressionAssignment_3");
					put(grammarAccess.getConditionAccess().getConditionExpressionAssignment_1(), "rule__Condition__ConditionExpressionAssignment_1");
					put(grammarAccess.getConditionExpressionAccess().getExpressionAssignment(), "rule__ConditionExpression__ExpressionAssignment");
					put(grammarAccess.getConstraintBlockAccess().getConsiderAssignment_3_0_1(), "rule__ConstraintBlock__ConsiderAssignment_3_0_1");
					put(grammarAccess.getConstraintBlockAccess().getIgnoreAssignment_3_1_1(), "rule__ConstraintBlock__IgnoreAssignment_3_1_1");
					put(grammarAccess.getConstraintBlockAccess().getForbiddenAssignment_3_2_1(), "rule__ConstraintBlock__ForbiddenAssignment_3_2_1");
					put(grammarAccess.getConstraintBlockAccess().getInterruptAssignment_3_3_1(), "rule__ConstraintBlock__InterruptAssignment_3_3_1");
					put(grammarAccess.getConstraintMessageAccess().getSenderAssignment_0(), "rule__ConstraintMessage__SenderAssignment_0");
					put(grammarAccess.getConstraintMessageAccess().getReceiverAssignment_2(), "rule__ConstraintMessage__ReceiverAssignment_2");
					put(grammarAccess.getConstraintMessageAccess().getModelElementAssignment_4(), "rule__ConstraintMessage__ModelElementAssignment_4");
					put(grammarAccess.getConstraintMessageAccess().getCollectionModificationAssignment_5_1(), "rule__ConstraintMessage__CollectionModificationAssignment_5_1");
					put(grammarAccess.getConstraintMessageAccess().getParametersAssignment_7_0(), "rule__ConstraintMessage__ParametersAssignment_7_0");
					put(grammarAccess.getConstraintMessageAccess().getParametersAssignment_7_1_1(), "rule__ConstraintMessage__ParametersAssignment_7_1_1");
					put(grammarAccess.getDocumentAccess().getImportsAssignment_0(), "rule__Document__ImportsAssignment_0");
					put(grammarAccess.getDocumentAccess().getDomainsAssignment_1_1(), "rule__Document__DomainsAssignment_1_1");
					put(grammarAccess.getDocumentAccess().getExpressionsAssignment_2(), "rule__Document__ExpressionsAssignment_2");
					put(grammarAccess.getImportAccess().getImportURIAssignment_1(), "rule__Import__ImportURIAssignment_1");
					put(grammarAccess.getExpressionRegionAccess().getExpressionsAssignment_2_0(), "rule__ExpressionRegion__ExpressionsAssignment_2_0");
					put(grammarAccess.getVariableDeclarationAccess().getNameAssignment_1(), "rule__VariableDeclaration__NameAssignment_1");
					put(grammarAccess.getVariableDeclarationAccess().getExpressionAssignment_3(), "rule__VariableDeclaration__ExpressionAssignment_3");
					put(grammarAccess.getVariableAssignmentAccess().getVariableAssignment_0(), "rule__VariableAssignment__VariableAssignment_0");
					put(grammarAccess.getVariableAssignmentAccess().getExpressionAssignment_2(), "rule__VariableAssignment__ExpressionAssignment_2");
					put(grammarAccess.getTypedVariableDeclarationAccess().getTypeAssignment_1(), "rule__TypedVariableDeclaration__TypeAssignment_1");
					put(grammarAccess.getTypedVariableDeclarationAccess().getNameAssignment_2(), "rule__TypedVariableDeclaration__NameAssignment_2");
					put(grammarAccess.getTypedVariableDeclarationAccess().getExpressionAssignment_3_1(), "rule__TypedVariableDeclaration__ExpressionAssignment_3_1");
					put(grammarAccess.getClockDeclarationAccess().getNameAssignment_2(), "rule__ClockDeclaration__NameAssignment_2");
					put(grammarAccess.getClockDeclarationAccess().getExpressionAssignment_3_1(), "rule__ClockDeclaration__ExpressionAssignment_3_1");
					put(grammarAccess.getClockAccess().getNameAssignment_0(), "rule__Clock__NameAssignment_0");
					put(grammarAccess.getClockAccess().getLeftIncludedAssignment_1(), "rule__Clock__LeftIncludedAssignment_1");
					put(grammarAccess.getClockAccess().getRightIncludedAssignment_2(), "rule__Clock__RightIncludedAssignment_2");
					put(grammarAccess.getClockAccess().getLeftValueAssignment_3(), "rule__Clock__LeftValueAssignment_3");
					put(grammarAccess.getClockAccess().getRightValueAssignment_4(), "rule__Clock__RightValueAssignment_4");
					put(grammarAccess.getClockAssignmentAccess().getVariableAssignment_1(), "rule__ClockAssignment__VariableAssignment_1");
					put(grammarAccess.getClockAssignmentAccess().getExpressionAssignment_2_1(), "rule__ClockAssignment__ExpressionAssignment_2_1");
					put(grammarAccess.getImplicationExpressionAccess().getOperatorAssignment_1_1(), "rule__ImplicationExpression__OperatorAssignment_1_1");
					put(grammarAccess.getImplicationExpressionAccess().getRightAssignment_1_2(), "rule__ImplicationExpression__RightAssignment_1_2");
					put(grammarAccess.getDisjunctionExpressionAccess().getOperatorAssignment_1_1(), "rule__DisjunctionExpression__OperatorAssignment_1_1");
					put(grammarAccess.getDisjunctionExpressionAccess().getRightAssignment_1_2(), "rule__DisjunctionExpression__RightAssignment_1_2");
					put(grammarAccess.getConjunctionExpressionAccess().getOperatorAssignment_1_1(), "rule__ConjunctionExpression__OperatorAssignment_1_1");
					put(grammarAccess.getConjunctionExpressionAccess().getRightAssignment_1_2(), "rule__ConjunctionExpression__RightAssignment_1_2");
					put(grammarAccess.getRelationExpressionAccess().getOperatorAssignment_1_1(), "rule__RelationExpression__OperatorAssignment_1_1");
					put(grammarAccess.getRelationExpressionAccess().getRightAssignment_1_2(), "rule__RelationExpression__RightAssignment_1_2");
					put(grammarAccess.getTimedExpressionAccess().getClockAssignment_0(), "rule__TimedExpression__ClockAssignment_0");
					put(grammarAccess.getTimedExpressionAccess().getOperatorAssignment_1(), "rule__TimedExpression__OperatorAssignment_1");
					put(grammarAccess.getTimedExpressionAccess().getValueAssignment_2(), "rule__TimedExpression__ValueAssignment_2");
					put(grammarAccess.getAdditionExpressionAccess().getOperatorAssignment_1_1(), "rule__AdditionExpression__OperatorAssignment_1_1");
					put(grammarAccess.getAdditionExpressionAccess().getRightAssignment_1_2(), "rule__AdditionExpression__RightAssignment_1_2");
					put(grammarAccess.getMultiplicationExpressionAccess().getOperatorAssignment_1_1(), "rule__MultiplicationExpression__OperatorAssignment_1_1");
					put(grammarAccess.getMultiplicationExpressionAccess().getRightAssignment_1_2(), "rule__MultiplicationExpression__RightAssignment_1_2");
					put(grammarAccess.getNegatedExpressionAccess().getOperatorAssignment_0_1(), "rule__NegatedExpression__OperatorAssignment_0_1");
					put(grammarAccess.getNegatedExpressionAccess().getOperandAssignment_0_2(), "rule__NegatedExpression__OperandAssignment_0_2");
					put(grammarAccess.getIntegerValueAccess().getValueAssignment(), "rule__IntegerValue__ValueAssignment");
					put(grammarAccess.getBooleanValueAccess().getValueAssignment(), "rule__BooleanValue__ValueAssignment");
					put(grammarAccess.getStringValueAccess().getValueAssignment(), "rule__StringValue__ValueAssignment");
					put(grammarAccess.getEnumValueAccess().getTypeAssignment_0(), "rule__EnumValue__TypeAssignment_0");
					put(grammarAccess.getEnumValueAccess().getValueAssignment_2(), "rule__EnumValue__ValueAssignment_2");
					put(grammarAccess.getVariableValueAccess().getValueAssignment(), "rule__VariableValue__ValueAssignment");
					put(grammarAccess.getCollectionAccessAccess().getCollectionOperationAssignment_0(), "rule__CollectionAccess__CollectionOperationAssignment_0");
					put(grammarAccess.getCollectionAccessAccess().getParameterAssignment_2(), "rule__CollectionAccess__ParameterAssignment_2");
					put(grammarAccess.getFeatureAccessAccess().getTargetAssignment_0(), "rule__FeatureAccess__TargetAssignment_0");
					put(grammarAccess.getFeatureAccessAccess().getValueAssignment_2_0_0(), "rule__FeatureAccess__ValueAssignment_2_0_0");
					put(grammarAccess.getFeatureAccessAccess().getCollectionAccessAssignment_2_0_1_1(), "rule__FeatureAccess__CollectionAccessAssignment_2_0_1_1");
					put(grammarAccess.getFeatureAccessAccess().getValueAssignment_2_1_0(), "rule__FeatureAccess__ValueAssignment_2_1_0");
					put(grammarAccess.getFeatureAccessAccess().getParametersAssignment_2_1_2_0(), "rule__FeatureAccess__ParametersAssignment_2_1_2_0");
					put(grammarAccess.getFeatureAccessAccess().getParametersAssignment_2_1_2_1_1(), "rule__FeatureAccess__ParametersAssignment_2_1_2_1_1");
					put(grammarAccess.getStructuralFeatureValueAccess().getValueAssignment(), "rule__StructuralFeatureValue__ValueAssignment");
					put(grammarAccess.getOperationValueAccess().getValueAssignment(), "rule__OperationValue__ValueAssignment");
				}
			};
		}
		return nameMappings.get(element);
	}
	
	@Override
	protected Collection<FollowElement> getFollowElements(AbstractInternalContentAssistParser parser) {
		try {
			org.scenariotools.sml.ui.contentassist.antlr.internal.InternalSMLParser typedParser = (org.scenariotools.sml.ui.contentassist.antlr.internal.InternalSMLParser) parser;
			typedParser.entryRuleSpecification();
			return typedParser.getFollowElements();
		} catch(RecognitionException ex) {
			throw new RuntimeException(ex);
		}		
	}
	
	@Override
	protected String[] getInitialHiddenTokens() {
		return new String[] { "RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT" };
	}
	
	public SMLGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}
	
	public void setGrammarAccess(SMLGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
