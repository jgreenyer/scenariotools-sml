/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.sml.expressions.utility

import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.ecore.EcorePackage
import org.scenariotools.sml.expressions.scenarioExpressions.CollectionAccess
import org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation
import org.scenariotools.sml.expressions.scenarioExpressions.Expression
import org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess
import org.scenariotools.sml.expressions.scenarioExpressions.StructuralFeatureValue
import org.eclipse.emf.ecore.ETypedElement
import org.scenariotools.sml.expressions.scenarioExpressions.OperationValue

class CollectionUtil {

	static def String getNeededCollectionParameterMultiplicity(CollectionAccess collectionAccess) {
		switch collectionAccess.collectionOperation {
			case CollectionOperation.ANY: return CollectionMultiplicityTypes.NONE
			case CollectionOperation.CONTAINS: return CollectionMultiplicityTypes.SINGLE
			case CollectionOperation.CONTAINS_ALL: return CollectionMultiplicityTypes.MULTIPLE
			case CollectionOperation.FIRST: return CollectionMultiplicityTypes.NONE
			case CollectionOperation.GET: return CollectionMultiplicityTypes.SINGLE
			case CollectionOperation.IS_EMPTY: return CollectionMultiplicityTypes.NONE
			case CollectionOperation.LAST: return CollectionMultiplicityTypes.NONE
			case CollectionOperation.SIZE: return CollectionMultiplicityTypes.NONE
		}
		return CollectionMultiplicityTypes.NONE
	}

	static def EClassifier getNeededCollectionParameterType(CollectionAccess collectionAccess) {
		val fa = collectionAccess.eContainer as FeatureAccess
		val value = fa.value as StructuralFeatureValue
		val feature = value.value as EStructuralFeature
		val type = feature.EType
		switch collectionAccess.collectionOperation {
			case CollectionOperation.ANY: return null
			case CollectionOperation.CONTAINS: return type
			case CollectionOperation.CONTAINS_ALL: return type
			case CollectionOperation.FIRST: return null
			case CollectionOperation.GET: return EcorePackage.Literals.EINT
			case CollectionOperation.IS_EMPTY: return null
			case CollectionOperation.LAST: return null
			case CollectionOperation.SIZE: return null
		}
		return null
	}

	static def EClassifier getCollectionReturnType(CollectionAccess collectionAccess) {
		val fa = collectionAccess.eContainer as FeatureAccess
		val value = fa.value as StructuralFeatureValue
		val feature = value.value as EStructuralFeature
		val type = feature.EType
		switch collectionAccess.collectionOperation {
			case CollectionOperation.ANY: return type
			case CollectionOperation.CONTAINS: return EcorePackage.Literals.EBOOLEAN
			case CollectionOperation.CONTAINS_ALL: return EcorePackage.Literals.EBOOLEAN
			case CollectionOperation.FIRST: return type
			case CollectionOperation.GET: return type
			case CollectionOperation.IS_EMPTY: return EcorePackage.Literals.EBOOLEAN
			case CollectionOperation.LAST: return type
			case CollectionOperation.SIZE: return EcorePackage.Literals.EINT
		}
		return null
	}

	static def String getMultiplicityOfExpression(Expression expression) {
		if (expression instanceof FeatureAccess) {
			val fa = expression as FeatureAccess
			var ETypedElement feature = null
			val v = fa.value
			if(v instanceof StructuralFeatureValue)
				feature = v.value as ETypedElement				
			else if(v instanceof OperationValue)
				feature = v.value as ETypedElement
			else
				return CollectionMultiplicityTypes.SINGLE
			val collectionAccess = fa.collectionAccess as CollectionAccess
			if (feature.upperBound == 1) {
				return CollectionMultiplicityTypes.SINGLE
			} else if (collectionAccess == null) {
				return CollectionMultiplicityTypes.MULTIPLE
			} else {
				return CollectionMultiplicityTypes.SINGLE
			}
		} else {
			return CollectionMultiplicityTypes.SINGLE
		}
	}

	static def boolean isCollection(EStructuralFeature feature) {
		if (feature == null)
			return false
		return feature.upperBound != 1
	}

	static def boolean isList(EStructuralFeature feature) {
		if (feature == null)
			return false
		return isCollection(feature) && feature.ordered
	}

	static def boolean isSet(EStructuralFeature feature) {
		if (feature == null)
			return false
		return isCollection(feature) && !feature.ordered
	}

	static def boolean isCollectionOperation(String operation) {
		return CollectionOperation.get(operation) != null
	}

	static def boolean isCollectionListOperation(String operation) {
		return isListCollectionOperation(CollectionOperation.get(operation))
	}

	static def boolean isCollectionSetOperation(String operation) {
		return isSetCollectionOperation(CollectionOperation.get(operation))
	}

	static def isSetCollectionOperation(CollectionOperation operation) {
		switch (operation) {
			case CollectionOperation.ANY: return true
			case CollectionOperation.CONTAINS: return true
			case CollectionOperation.CONTAINS_ALL: return true
			case CollectionOperation.FIRST: return false
			case CollectionOperation.GET: return false
			case CollectionOperation.IS_EMPTY: return true
			case CollectionOperation.LAST: return false
			case CollectionOperation.SIZE: return true
		}
		return false
	}

	static def isListCollectionOperation(CollectionOperation operation) {
		switch (operation) {
			case CollectionOperation.ANY: return true
			case CollectionOperation.CONTAINS: return true
			case CollectionOperation.CONTAINS_ALL: return true
			case CollectionOperation.FIRST: return true
			case CollectionOperation.GET: return true
			case CollectionOperation.IS_EMPTY: return true
			case CollectionOperation.LAST: return true
			case CollectionOperation.SIZE: return true
		}
		return false
	}

}
