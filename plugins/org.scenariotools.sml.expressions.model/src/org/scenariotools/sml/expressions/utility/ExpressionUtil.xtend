/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.sml.expressions.utility

import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EcorePackage
import org.scenariotools.sml.expressions.scenarioExpressions.BinaryOperationExpression
import org.scenariotools.sml.expressions.scenarioExpressions.Expression
import org.scenariotools.sml.expressions.scenarioExpressions.ExpressionOrRegion
import org.scenariotools.sml.expressions.scenarioExpressions.ExpressionRegion
import org.scenariotools.sml.expressions.scenarioExpressions.TypedVariable
import org.scenariotools.sml.expressions.scenarioExpressions.UnaryOperationExpression
import org.scenariotools.sml.expressions.scenarioExpressions.Value
import org.scenariotools.sml.expressions.scenarioExpressions.Variable
import org.scenariotools.sml.expressions.scenarioExpressions.VariableDeclaration
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage
import org.eclipse.emf.ecore.EcorePackage.Literals

class ExpressionUtil {

	def static ExpressionRegion getContainingExpressionRegion(EObject obj) {
		var region = obj.eContainer
		while (!(region instanceof ExpressionRegion)) {
			region = region.eContainer
			if (region == null)
				return null
		}
		return region as ExpressionRegion
	}

	def static EList<Variable> getRelevantVariablesFor(EObject obj) {
		val scope = new BasicEList<Variable>()
		var containingRegion = getContainingExpressionRegion(obj)
		// Not contained in interaction -> Return empty scope
		if (containingRegion == null) {
			return scope
		}
		for (ExpressionOrRegion  exp : containingRegion.expressions) {
			// Input Object reached -> Stop iteration, because after this object are only non declared variables!
			if (exp == obj || exp.eAllContents.toList.contains(obj)) {
				// Recursively search for variables in upper interaction
				scope.addAll(getRelevantVariablesFor(containingRegion))
				return scope
			}
			// Detect Variable
			if (exp instanceof TypedVariable) {
				scope.add(exp)
			}
		}
		return scope
	}

	def static EClassifier getVariableType(Variable variable) {
		if (variable instanceof TypedVariable)
			return variable.type
		if (variable instanceof VariableDeclaration)
			return getExpressionType(variable.expression)
		return null
	}

	public static final String TYPE_INTEGER = "EInt"
	public static final String TYPE_STRING = "EString"
	public static final String TYPE_BOOLEAN = "EBoolean"
	public static final String TYPE_UNDEFINED = "Undefined"

	def static EClassifier getExpressionType(Expression expression) {
		if (expression instanceof Value) {
			return ValueUtil.getExpressionType(expression)
		} else if (expression instanceof UnaryOperationExpression) {
			return expression.operand.expressionType
		} else if (expression instanceof BinaryOperationExpression) {
			val typeLeft = expression.left.expressionType
			val typeRight = expression.right.expressionType
			if (expression.operator.isBooleanOperator || expression.operator.isEquationOperator) {
				return EcorePackage.Literals.EBOOLEAN
			} else if (expression.operator.isIntegerOperator && typeLeft == typeRight &&
				typeLeft == EcorePackage.Literals.EINT) {
				return EcorePackage.Literals.EINT
			} else if (expression.operator.isStringOperator && typeLeft == typeRight &&
				typeLeft == EcorePackage.Literals.ESTRING) {
				return EcorePackage.Literals.ESTRING
			} else
				return EcorePackage.Literals.EOBJECT
		}
		return EcorePackage.Literals.EOBJECT
	}

	def static boolean isBooleanExpression(Expression expression) {
		return "boolean".equals(expression.expressionType.instanceClassName)
	}

	def static boolean isIntegerExpression(Expression expression) {
		return "int".equals(expression.expressionType.instanceClassName)
	}

	def static boolean isStringExpression(Expression expression) {
		if (expression.expressionType == EcorePackage.Literals.ESTRING)
			return true
		return false
	}
	
	def static boolean isClockExpression(Expression expression) {
		if (expression.expressionType == ScenarioExpressionsPackage.Literals.CLOCK_DECLARATION)
			return true
		return false
	}

	def static boolean isBooleanOperator(String string) { // '==' | '!=' | '<' | '>' | '<=' | '>='
		if (string.equals("=>") || string.equals("&&") || string.equals("||") || string.equals("!"))
			return true
		return false
	}

	def static boolean isIntegerOperator(String string) {
		if (string.equals("+") || string.equals("-") || string.equals("*") || string.equals("/"))
			return true
		return false
	}

	def static boolean isStringOperator(String string) {
		if (string.equals("+"))
			return true
		return false
	}

	def static boolean isEquationOperator(String string) { // '==' | '!=' | '<' | '>' | '<=' | '>=' || '!'
		if (string.equals("==") || string.equals("!=") || string.equals("<") || string.equals(">") ||
			string.equals("<=") || string.equals(">=") || string.equals("!"))
			return true
		return false
	}
}
