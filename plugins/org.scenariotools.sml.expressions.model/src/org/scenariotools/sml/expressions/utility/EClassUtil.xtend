/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.sml.expressions.utility

import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.ecore.EReference
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EModelElement

class EClassUtil {
	
	def static EList<EStructuralFeature> retrieveEStructuralFeaturesOf(EClass eclass) {
		val elements = new BasicEList<EStructuralFeature>()
		elements.addAll(eclass.EStructuralFeatures)

		eclass.EAllSuperTypes.forEach [ s |
			elements.addAll(s.EStructuralFeatures)
		]

		return elements
	}

	def static EList<EReference> retrieveAllEReferencesOf(EClass eclass) {
		val elements = new BasicEList<EReference>()
		elements.addAll(eclass.EReferences)

		eclass.EAllSuperTypes.forEach [ s |
			elements.addAll(s.EReferences)
		]

		return elements
	}
	
	def static EList<EModelElement> retrieveAllModelElementsOf(EClass eclass) {
		val elements = new BasicEList<EModelElement>()

		elements.addAll(eclass.getEAllOperations)
		elements.addAll(eclass.getEStructuralFeatures)

		eclass.getEAllSuperTypes.forEach [ s |
			elements.addAll(s.getEAllOperations)
			elements.addAll(s.getEStructuralFeatures)
		]

		return elements
	}
	
	
	
}
