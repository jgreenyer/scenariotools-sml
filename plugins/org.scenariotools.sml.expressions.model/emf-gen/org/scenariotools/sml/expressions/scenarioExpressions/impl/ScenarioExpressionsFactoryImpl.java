/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.expressions.scenarioExpressions.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.scenariotools.sml.expressions.scenarioExpressions.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ScenarioExpressionsFactoryImpl extends EFactoryImpl implements ScenarioExpressionsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ScenarioExpressionsFactory init() {
		try {
			ScenarioExpressionsFactory theScenarioExpressionsFactory = (ScenarioExpressionsFactory)EPackage.Registry.INSTANCE.getEFactory(ScenarioExpressionsPackage.eNS_URI);
			if (theScenarioExpressionsFactory != null) {
				return theScenarioExpressionsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ScenarioExpressionsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScenarioExpressionsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ScenarioExpressionsPackage.DOCUMENT: return createDocument();
			case ScenarioExpressionsPackage.IMPORT: return createImport();
			case ScenarioExpressionsPackage.EXPRESSION_REGION: return createExpressionRegion();
			case ScenarioExpressionsPackage.UNARY_OPERATION_EXPRESSION: return createUnaryOperationExpression();
			case ScenarioExpressionsPackage.BINARY_OPERATION_EXPRESSION: return createBinaryOperationExpression();
			case ScenarioExpressionsPackage.VARIABLE_DECLARATION: return createVariableDeclaration();
			case ScenarioExpressionsPackage.TYPED_VARIABLE_DECLARATION: return createTypedVariableDeclaration();
			case ScenarioExpressionsPackage.VARIABLE_ASSIGNMENT: return createVariableAssignment();
			case ScenarioExpressionsPackage.INTEGER_VALUE: return createIntegerValue();
			case ScenarioExpressionsPackage.BOOLEAN_VALUE: return createBooleanValue();
			case ScenarioExpressionsPackage.STRING_VALUE: return createStringValue();
			case ScenarioExpressionsPackage.ENUM_VALUE: return createEnumValue();
			case ScenarioExpressionsPackage.NULL_VALUE: return createNullValue();
			case ScenarioExpressionsPackage.VARIABLE_VALUE: return createVariableValue();
			case ScenarioExpressionsPackage.FEATURE_ACCESS: return createFeatureAccess();
			case ScenarioExpressionsPackage.COLLECTION_ACCESS: return createCollectionAccess();
			case ScenarioExpressionsPackage.SUB_FEATURE_ACCESS: return createSubFeatureAccess();
			case ScenarioExpressionsPackage.STRUCTURAL_FEATURE_VALUE: return createStructuralFeatureValue();
			case ScenarioExpressionsPackage.OPERATION_VALUE: return createOperationValue();
			case ScenarioExpressionsPackage.CLOCK_DECLARATION: return createClockDeclaration();
			case ScenarioExpressionsPackage.TIMED_EXPRESSION: return createTimedExpression();
			case ScenarioExpressionsPackage.CLOCK_ASSIGNMENT: return createClockAssignment();
			case ScenarioExpressionsPackage.CLOCK: return createClock();
			case ScenarioExpressionsPackage.CLOCK_EXPRESSION: return createClockExpression();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ScenarioExpressionsPackage.COLLECTION_OPERATION:
				return createCollectionOperationFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ScenarioExpressionsPackage.COLLECTION_OPERATION:
				return convertCollectionOperationToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Document createDocument() {
		DocumentImpl document = new DocumentImpl();
		return document;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Import createImport() {
		ImportImpl import_ = new ImportImpl();
		return import_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionRegion createExpressionRegion() {
		ExpressionRegionImpl expressionRegion = new ExpressionRegionImpl();
		return expressionRegion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnaryOperationExpression createUnaryOperationExpression() {
		UnaryOperationExpressionImpl unaryOperationExpression = new UnaryOperationExpressionImpl();
		return unaryOperationExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryOperationExpression createBinaryOperationExpression() {
		BinaryOperationExpressionImpl binaryOperationExpression = new BinaryOperationExpressionImpl();
		return binaryOperationExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableDeclaration createVariableDeclaration() {
		VariableDeclarationImpl variableDeclaration = new VariableDeclarationImpl();
		return variableDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypedVariableDeclaration createTypedVariableDeclaration() {
		TypedVariableDeclarationImpl typedVariableDeclaration = new TypedVariableDeclarationImpl();
		return typedVariableDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableAssignment createVariableAssignment() {
		VariableAssignmentImpl variableAssignment = new VariableAssignmentImpl();
		return variableAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerValue createIntegerValue() {
		IntegerValueImpl integerValue = new IntegerValueImpl();
		return integerValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanValue createBooleanValue() {
		BooleanValueImpl booleanValue = new BooleanValueImpl();
		return booleanValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringValue createStringValue() {
		StringValueImpl stringValue = new StringValueImpl();
		return stringValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumValue createEnumValue() {
		EnumValueImpl enumValue = new EnumValueImpl();
		return enumValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullValue createNullValue() {
		NullValueImpl nullValue = new NullValueImpl();
		return nullValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableValue createVariableValue() {
		VariableValueImpl variableValue = new VariableValueImpl();
		return variableValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureAccess createFeatureAccess() {
		FeatureAccessImpl featureAccess = new FeatureAccessImpl();
		return featureAccess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CollectionAccess createCollectionAccess() {
		CollectionAccessImpl collectionAccess = new CollectionAccessImpl();
		return collectionAccess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubFeatureAccess createSubFeatureAccess() {
		SubFeatureAccessImpl subFeatureAccess = new SubFeatureAccessImpl();
		return subFeatureAccess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StructuralFeatureValue createStructuralFeatureValue() {
		StructuralFeatureValueImpl structuralFeatureValue = new StructuralFeatureValueImpl();
		return structuralFeatureValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationValue createOperationValue() {
		OperationValueImpl operationValue = new OperationValueImpl();
		return operationValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClockDeclaration createClockDeclaration() {
		ClockDeclarationImpl clockDeclaration = new ClockDeclarationImpl();
		return clockDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedExpression createTimedExpression() {
		TimedExpressionImpl timedExpression = new TimedExpressionImpl();
		return timedExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClockExpression createClockExpression() {
		ClockExpressionImpl clockExpression = new ClockExpressionImpl();
		return clockExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClockAssignment createClockAssignment() {
		ClockAssignmentImpl clockAssignment = new ClockAssignmentImpl();
		return clockAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Clock createClock() {
		ClockImpl clock = new ClockImpl();
		return clock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CollectionOperation createCollectionOperationFromString(EDataType eDataType, String initialValue) {
		CollectionOperation result = CollectionOperation.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCollectionOperationToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScenarioExpressionsPackage getScenarioExpressionsPackage() {
		return (ScenarioExpressionsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ScenarioExpressionsPackage getPackage() {
		return ScenarioExpressionsPackage.eINSTANCE;
	}

} //ScenarioExpressionsFactoryImpl
