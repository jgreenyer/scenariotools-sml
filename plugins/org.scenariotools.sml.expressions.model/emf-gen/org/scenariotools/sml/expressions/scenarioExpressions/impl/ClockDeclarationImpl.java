/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.expressions.scenarioExpressions.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.scenariotools.sml.expressions.scenarioExpressions.Clock;
import org.scenariotools.sml.expressions.scenarioExpressions.ClockDeclaration;
import org.scenariotools.sml.expressions.scenarioExpressions.Expression;
import org.scenariotools.sml.expressions.scenarioExpressions.ExpressionAndVariables;
import org.scenariotools.sml.expressions.scenarioExpressions.ExpressionOrRegion;
import org.scenariotools.sml.expressions.scenarioExpressions.NamedElement;
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage;
import org.scenariotools.sml.expressions.scenarioExpressions.TypedVariable;
import org.scenariotools.sml.expressions.scenarioExpressions.VariableExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Clock Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.ClockDeclarationImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.ClockDeclarationImpl#getLeftValue <em>Left Value</em>}</li>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.ClockDeclarationImpl#getRightValue <em>Right Value</em>}</li>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.ClockDeclarationImpl#isLeftIncluded <em>Left Included</em>}</li>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.ClockDeclarationImpl#isRightIncluded <em>Right Included</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ClockDeclarationImpl extends VariableExpressionImpl implements ClockDeclaration {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getLeftValue() <em>Left Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeftValue()
	 * @generated
	 * @ordered
	 */
	protected static final int LEFT_VALUE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLeftValue() <em>Left Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeftValue()
	 * @generated
	 * @ordered
	 */
	protected int leftValue = LEFT_VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getRightValue() <em>Right Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightValue()
	 * @generated
	 * @ordered
	 */
	protected static final int RIGHT_VALUE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getRightValue() <em>Right Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightValue()
	 * @generated
	 * @ordered
	 */
	protected int rightValue = RIGHT_VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #isLeftIncluded() <em>Left Included</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLeftIncluded()
	 * @generated
	 * @ordered
	 */
	protected static final boolean LEFT_INCLUDED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isLeftIncluded() <em>Left Included</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLeftIncluded()
	 * @generated
	 * @ordered
	 */
	protected boolean leftIncluded = LEFT_INCLUDED_EDEFAULT;

	/**
	 * The default value of the '{@link #isRightIncluded() <em>Right Included</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRightIncluded()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RIGHT_INCLUDED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isRightIncluded() <em>Right Included</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRightIncluded()
	 * @generated
	 * @ordered
	 */
	protected boolean rightIncluded = RIGHT_INCLUDED_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClockDeclarationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScenarioExpressionsPackage.Literals.CLOCK_DECLARATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenarioExpressionsPackage.CLOCK_DECLARATION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLeftValue() {
		return leftValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLeftValue(int newLeftValue) {
		int oldLeftValue = leftValue;
		leftValue = newLeftValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenarioExpressionsPackage.CLOCK_DECLARATION__LEFT_VALUE, oldLeftValue, leftValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getRightValue() {
		return rightValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRightValue(int newRightValue) {
		int oldRightValue = rightValue;
		rightValue = newRightValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenarioExpressionsPackage.CLOCK_DECLARATION__RIGHT_VALUE, oldRightValue, rightValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isLeftIncluded() {
		return leftIncluded;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLeftIncluded(boolean newLeftIncluded) {
		boolean oldLeftIncluded = leftIncluded;
		leftIncluded = newLeftIncluded;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenarioExpressionsPackage.CLOCK_DECLARATION__LEFT_INCLUDED, oldLeftIncluded, leftIncluded));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRightIncluded() {
		return rightIncluded;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRightIncluded(boolean newRightIncluded) {
		boolean oldRightIncluded = rightIncluded;
		rightIncluded = newRightIncluded;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenarioExpressionsPackage.CLOCK_DECLARATION__RIGHT_INCLUDED, oldRightIncluded, rightIncluded));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScenarioExpressionsPackage.CLOCK_DECLARATION__NAME:
				return getName();
			case ScenarioExpressionsPackage.CLOCK_DECLARATION__LEFT_VALUE:
				return getLeftValue();
			case ScenarioExpressionsPackage.CLOCK_DECLARATION__RIGHT_VALUE:
				return getRightValue();
			case ScenarioExpressionsPackage.CLOCK_DECLARATION__LEFT_INCLUDED:
				return isLeftIncluded();
			case ScenarioExpressionsPackage.CLOCK_DECLARATION__RIGHT_INCLUDED:
				return isRightIncluded();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScenarioExpressionsPackage.CLOCK_DECLARATION__NAME:
				setName((String)newValue);
				return;
			case ScenarioExpressionsPackage.CLOCK_DECLARATION__LEFT_VALUE:
				setLeftValue((Integer)newValue);
				return;
			case ScenarioExpressionsPackage.CLOCK_DECLARATION__RIGHT_VALUE:
				setRightValue((Integer)newValue);
				return;
			case ScenarioExpressionsPackage.CLOCK_DECLARATION__LEFT_INCLUDED:
				setLeftIncluded((Boolean)newValue);
				return;
			case ScenarioExpressionsPackage.CLOCK_DECLARATION__RIGHT_INCLUDED:
				setRightIncluded((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScenarioExpressionsPackage.CLOCK_DECLARATION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ScenarioExpressionsPackage.CLOCK_DECLARATION__LEFT_VALUE:
				setLeftValue(LEFT_VALUE_EDEFAULT);
				return;
			case ScenarioExpressionsPackage.CLOCK_DECLARATION__RIGHT_VALUE:
				setRightValue(RIGHT_VALUE_EDEFAULT);
				return;
			case ScenarioExpressionsPackage.CLOCK_DECLARATION__LEFT_INCLUDED:
				setLeftIncluded(LEFT_INCLUDED_EDEFAULT);
				return;
			case ScenarioExpressionsPackage.CLOCK_DECLARATION__RIGHT_INCLUDED:
				setRightIncluded(RIGHT_INCLUDED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScenarioExpressionsPackage.CLOCK_DECLARATION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ScenarioExpressionsPackage.CLOCK_DECLARATION__LEFT_VALUE:
				return leftValue != LEFT_VALUE_EDEFAULT;
			case ScenarioExpressionsPackage.CLOCK_DECLARATION__RIGHT_VALUE:
				return rightValue != RIGHT_VALUE_EDEFAULT;
			case ScenarioExpressionsPackage.CLOCK_DECLARATION__LEFT_INCLUDED:
				return leftIncluded != LEFT_INCLUDED_EDEFAULT;
			case ScenarioExpressionsPackage.CLOCK_DECLARATION__RIGHT_INCLUDED:
				return rightIncluded != RIGHT_INCLUDED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (derivedFeatureID) {
				case ScenarioExpressionsPackage.CLOCK_DECLARATION__NAME: return ScenarioExpressionsPackage.NAMED_ELEMENT__NAME;
				default: return -1;
			}
		}
		if (baseClass == Clock.class) {
			switch (derivedFeatureID) {
				case ScenarioExpressionsPackage.CLOCK_DECLARATION__LEFT_VALUE: return ScenarioExpressionsPackage.CLOCK__LEFT_VALUE;
				case ScenarioExpressionsPackage.CLOCK_DECLARATION__RIGHT_VALUE: return ScenarioExpressionsPackage.CLOCK__RIGHT_VALUE;
				case ScenarioExpressionsPackage.CLOCK_DECLARATION__LEFT_INCLUDED: return ScenarioExpressionsPackage.CLOCK__LEFT_INCLUDED;
				case ScenarioExpressionsPackage.CLOCK_DECLARATION__RIGHT_INCLUDED: return ScenarioExpressionsPackage.CLOCK__RIGHT_INCLUDED;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (baseFeatureID) {
				case ScenarioExpressionsPackage.NAMED_ELEMENT__NAME: return ScenarioExpressionsPackage.CLOCK_DECLARATION__NAME;
				default: return -1;
			}
		}
		if (baseClass == Clock.class) {
			switch (baseFeatureID) {
				case ScenarioExpressionsPackage.CLOCK__LEFT_VALUE: return ScenarioExpressionsPackage.CLOCK_DECLARATION__LEFT_VALUE;
				case ScenarioExpressionsPackage.CLOCK__RIGHT_VALUE: return ScenarioExpressionsPackage.CLOCK_DECLARATION__RIGHT_VALUE;
				case ScenarioExpressionsPackage.CLOCK__LEFT_INCLUDED: return ScenarioExpressionsPackage.CLOCK_DECLARATION__LEFT_INCLUDED;
				case ScenarioExpressionsPackage.CLOCK__RIGHT_INCLUDED: return ScenarioExpressionsPackage.CLOCK_DECLARATION__RIGHT_INCLUDED;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", leftValue: ");
		result.append(leftValue);
		result.append(", rightValue: ");
		result.append(rightValue);
		result.append(", leftIncluded: ");
		result.append(leftIncluded);
		result.append(", rightIncluded: ");
		result.append(rightIncluded);
		result.append(')');
		return result.toString();
	}

} //ClockDeclarationImpl
