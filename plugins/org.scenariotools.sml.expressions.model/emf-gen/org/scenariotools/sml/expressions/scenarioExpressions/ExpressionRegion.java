/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.expressions.scenarioExpressions;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression Region</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.ExpressionRegion#getExpressions <em>Expressions</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getExpressionRegion()
 * @model
 * @generated
 */
public interface ExpressionRegion extends ExpressionOrRegion {
	/**
	 * Returns the value of the '<em><b>Expressions</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.expressions.scenarioExpressions.ExpressionOrRegion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expressions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expressions</em>' containment reference list.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getExpressionRegion_Expressions()
	 * @model containment="true"
	 * @generated
	 */
	EList<ExpressionOrRegion> getExpressions();

} // ExpressionRegion
