/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.expressions.scenarioExpressions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Clock</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.Clock#getLeftValue <em>Left Value</em>}</li>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.Clock#getRightValue <em>Right Value</em>}</li>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.Clock#isLeftIncluded <em>Left Included</em>}</li>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.Clock#isRightIncluded <em>Right Included</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getClock()
 * @model
 * @generated
 */
public interface Clock extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Left Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Value</em>' attribute.
	 * @see #setLeftValue(int)
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getClock_LeftValue()
	 * @model
	 * @generated
	 */
	int getLeftValue();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.expressions.scenarioExpressions.Clock#getLeftValue <em>Left Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left Value</em>' attribute.
	 * @see #getLeftValue()
	 * @generated
	 */
	void setLeftValue(int value);

	/**
	 * Returns the value of the '<em><b>Right Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Value</em>' attribute.
	 * @see #setRightValue(int)
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getClock_RightValue()
	 * @model
	 * @generated
	 */
	int getRightValue();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.expressions.scenarioExpressions.Clock#getRightValue <em>Right Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right Value</em>' attribute.
	 * @see #getRightValue()
	 * @generated
	 */
	void setRightValue(int value);

	/**
	 * Returns the value of the '<em><b>Left Included</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left Included</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Included</em>' attribute.
	 * @see #setLeftIncluded(boolean)
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getClock_LeftIncluded()
	 * @model
	 * @generated
	 */
	boolean isLeftIncluded();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.expressions.scenarioExpressions.Clock#isLeftIncluded <em>Left Included</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left Included</em>' attribute.
	 * @see #isLeftIncluded()
	 * @generated
	 */
	void setLeftIncluded(boolean value);

	/**
	 * Returns the value of the '<em><b>Right Included</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right Included</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Included</em>' attribute.
	 * @see #setRightIncluded(boolean)
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getClock_RightIncluded()
	 * @model
	 * @generated
	 */
	boolean isRightIncluded();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.expressions.scenarioExpressions.Clock#isRightIncluded <em>Right Included</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right Included</em>' attribute.
	 * @see #isRightIncluded()
	 * @generated
	 */
	void setRightIncluded(boolean value);

} // Clock
