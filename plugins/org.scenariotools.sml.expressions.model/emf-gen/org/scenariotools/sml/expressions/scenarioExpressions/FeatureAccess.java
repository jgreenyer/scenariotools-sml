/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.expressions.scenarioExpressions;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Access</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess#getTarget <em>Target</em>}</li>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess#getValue <em>Value</em>}</li>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess#getCollectionAccess <em>Collection Access</em>}</li>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getFeatureAccess()
 * @model
 * @generated
 */
public interface FeatureAccess extends Value {
	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(EObject)
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getFeatureAccess_Target()
	 * @model
	 * @generated
	 */
	EObject getTarget();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(EObject value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(Value)
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getFeatureAccess_Value()
	 * @model containment="true"
	 * @generated
	 */
	Value getValue();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Value value);

	/**
	 * Returns the value of the '<em><b>Collection Access</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collection Access</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collection Access</em>' containment reference.
	 * @see #setCollectionAccess(CollectionAccess)
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getFeatureAccess_CollectionAccess()
	 * @model containment="true"
	 * @generated
	 */
	CollectionAccess getCollectionAccess();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess#getCollectionAccess <em>Collection Access</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Collection Access</em>' containment reference.
	 * @see #getCollectionAccess()
	 * @generated
	 */
	void setCollectionAccess(CollectionAccess value);

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.expressions.scenarioExpressions.Expression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getFeatureAccess_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<Expression> getParameters();

} // FeatureAccess
