/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.expressions.scenarioExpressions.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.scenariotools.sml.expressions.scenarioExpressions.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage
 * @generated
 */
public class ScenarioExpressionsSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ScenarioExpressionsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScenarioExpressionsSwitch() {
		if (modelPackage == null) {
			modelPackage = ScenarioExpressionsPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ScenarioExpressionsPackage.DOCUMENT: {
				Document document = (Document)theEObject;
				T result = caseDocument(document);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.NAMED_ELEMENT: {
				NamedElement namedElement = (NamedElement)theEObject;
				T result = caseNamedElement(namedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.IMPORT: {
				Import import_ = (Import)theEObject;
				T result = caseImport(import_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.EXPRESSION_OR_REGION: {
				ExpressionOrRegion expressionOrRegion = (ExpressionOrRegion)theEObject;
				T result = caseExpressionOrRegion(expressionOrRegion);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.ABSTRACT_EXPRESSION: {
				AbstractExpression abstractExpression = (AbstractExpression)theEObject;
				T result = caseAbstractExpression(abstractExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.EXPRESSION_REGION: {
				ExpressionRegion expressionRegion = (ExpressionRegion)theEObject;
				T result = caseExpressionRegion(expressionRegion);
				if (result == null) result = caseExpressionOrRegion(expressionRegion);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.EXPRESSION_AND_VARIABLES: {
				ExpressionAndVariables expressionAndVariables = (ExpressionAndVariables)theEObject;
				T result = caseExpressionAndVariables(expressionAndVariables);
				if (result == null) result = caseExpressionOrRegion(expressionAndVariables);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.VARIABLE_EXPRESSION: {
				VariableExpression variableExpression = (VariableExpression)theEObject;
				T result = caseVariableExpression(variableExpression);
				if (result == null) result = caseExpression(variableExpression);
				if (result == null) result = caseExpressionAndVariables(variableExpression);
				if (result == null) result = caseExpressionOrRegion(variableExpression);
				if (result == null) result = caseAbstractExpression(variableExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.EXPRESSION: {
				Expression expression = (Expression)theEObject;
				T result = caseExpression(expression);
				if (result == null) result = caseExpressionAndVariables(expression);
				if (result == null) result = caseAbstractExpression(expression);
				if (result == null) result = caseExpressionOrRegion(expression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.OPERATION_EXPRESSION: {
				OperationExpression operationExpression = (OperationExpression)theEObject;
				T result = caseOperationExpression(operationExpression);
				if (result == null) result = caseExpression(operationExpression);
				if (result == null) result = caseExpressionAndVariables(operationExpression);
				if (result == null) result = caseAbstractExpression(operationExpression);
				if (result == null) result = caseExpressionOrRegion(operationExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.UNARY_OPERATION_EXPRESSION: {
				UnaryOperationExpression unaryOperationExpression = (UnaryOperationExpression)theEObject;
				T result = caseUnaryOperationExpression(unaryOperationExpression);
				if (result == null) result = caseOperationExpression(unaryOperationExpression);
				if (result == null) result = caseExpression(unaryOperationExpression);
				if (result == null) result = caseExpressionAndVariables(unaryOperationExpression);
				if (result == null) result = caseAbstractExpression(unaryOperationExpression);
				if (result == null) result = caseExpressionOrRegion(unaryOperationExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.BINARY_OPERATION_EXPRESSION: {
				BinaryOperationExpression binaryOperationExpression = (BinaryOperationExpression)theEObject;
				T result = caseBinaryOperationExpression(binaryOperationExpression);
				if (result == null) result = caseOperationExpression(binaryOperationExpression);
				if (result == null) result = caseExpression(binaryOperationExpression);
				if (result == null) result = caseExpressionAndVariables(binaryOperationExpression);
				if (result == null) result = caseAbstractExpression(binaryOperationExpression);
				if (result == null) result = caseExpressionOrRegion(binaryOperationExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.VARIABLE: {
				Variable variable = (Variable)theEObject;
				T result = caseVariable(variable);
				if (result == null) result = caseAbstractExpression(variable);
				if (result == null) result = caseNamedElement(variable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.TYPED_VARIABLE: {
				TypedVariable typedVariable = (TypedVariable)theEObject;
				T result = caseTypedVariable(typedVariable);
				if (result == null) result = caseVariable(typedVariable);
				if (result == null) result = caseAbstractExpression(typedVariable);
				if (result == null) result = caseNamedElement(typedVariable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.VARIABLE_DECLARATION: {
				VariableDeclaration variableDeclaration = (VariableDeclaration)theEObject;
				T result = caseVariableDeclaration(variableDeclaration);
				if (result == null) result = caseVariableExpression(variableDeclaration);
				if (result == null) result = caseVariable(variableDeclaration);
				if (result == null) result = caseExpression(variableDeclaration);
				if (result == null) result = caseNamedElement(variableDeclaration);
				if (result == null) result = caseExpressionAndVariables(variableDeclaration);
				if (result == null) result = caseExpressionOrRegion(variableDeclaration);
				if (result == null) result = caseAbstractExpression(variableDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.TYPED_VARIABLE_DECLARATION: {
				TypedVariableDeclaration typedVariableDeclaration = (TypedVariableDeclaration)theEObject;
				T result = caseTypedVariableDeclaration(typedVariableDeclaration);
				if (result == null) result = caseVariableDeclaration(typedVariableDeclaration);
				if (result == null) result = caseTypedVariable(typedVariableDeclaration);
				if (result == null) result = caseVariableExpression(typedVariableDeclaration);
				if (result == null) result = caseVariable(typedVariableDeclaration);
				if (result == null) result = caseExpression(typedVariableDeclaration);
				if (result == null) result = caseNamedElement(typedVariableDeclaration);
				if (result == null) result = caseExpressionAndVariables(typedVariableDeclaration);
				if (result == null) result = caseExpressionOrRegion(typedVariableDeclaration);
				if (result == null) result = caseAbstractExpression(typedVariableDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.VARIABLE_ASSIGNMENT: {
				VariableAssignment variableAssignment = (VariableAssignment)theEObject;
				T result = caseVariableAssignment(variableAssignment);
				if (result == null) result = caseVariableExpression(variableAssignment);
				if (result == null) result = caseExpression(variableAssignment);
				if (result == null) result = caseExpressionAndVariables(variableAssignment);
				if (result == null) result = caseExpressionOrRegion(variableAssignment);
				if (result == null) result = caseAbstractExpression(variableAssignment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.VALUE: {
				Value value = (Value)theEObject;
				T result = caseValue(value);
				if (result == null) result = caseExpression(value);
				if (result == null) result = caseExpressionAndVariables(value);
				if (result == null) result = caseAbstractExpression(value);
				if (result == null) result = caseExpressionOrRegion(value);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.STATIC_VALUE: {
				StaticValue staticValue = (StaticValue)theEObject;
				T result = caseStaticValue(staticValue);
				if (result == null) result = caseValue(staticValue);
				if (result == null) result = caseExpression(staticValue);
				if (result == null) result = caseExpressionAndVariables(staticValue);
				if (result == null) result = caseAbstractExpression(staticValue);
				if (result == null) result = caseExpressionOrRegion(staticValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.INTEGER_VALUE: {
				IntegerValue integerValue = (IntegerValue)theEObject;
				T result = caseIntegerValue(integerValue);
				if (result == null) result = caseStaticValue(integerValue);
				if (result == null) result = caseValue(integerValue);
				if (result == null) result = caseExpression(integerValue);
				if (result == null) result = caseExpressionAndVariables(integerValue);
				if (result == null) result = caseAbstractExpression(integerValue);
				if (result == null) result = caseExpressionOrRegion(integerValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.BOOLEAN_VALUE: {
				BooleanValue booleanValue = (BooleanValue)theEObject;
				T result = caseBooleanValue(booleanValue);
				if (result == null) result = caseStaticValue(booleanValue);
				if (result == null) result = caseValue(booleanValue);
				if (result == null) result = caseExpression(booleanValue);
				if (result == null) result = caseExpressionAndVariables(booleanValue);
				if (result == null) result = caseAbstractExpression(booleanValue);
				if (result == null) result = caseExpressionOrRegion(booleanValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.STRING_VALUE: {
				StringValue stringValue = (StringValue)theEObject;
				T result = caseStringValue(stringValue);
				if (result == null) result = caseStaticValue(stringValue);
				if (result == null) result = caseValue(stringValue);
				if (result == null) result = caseExpression(stringValue);
				if (result == null) result = caseExpressionAndVariables(stringValue);
				if (result == null) result = caseAbstractExpression(stringValue);
				if (result == null) result = caseExpressionOrRegion(stringValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.ENUM_VALUE: {
				EnumValue enumValue = (EnumValue)theEObject;
				T result = caseEnumValue(enumValue);
				if (result == null) result = caseStaticValue(enumValue);
				if (result == null) result = caseValue(enumValue);
				if (result == null) result = caseExpression(enumValue);
				if (result == null) result = caseExpressionAndVariables(enumValue);
				if (result == null) result = caseAbstractExpression(enumValue);
				if (result == null) result = caseExpressionOrRegion(enumValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.NULL_VALUE: {
				NullValue nullValue = (NullValue)theEObject;
				T result = caseNullValue(nullValue);
				if (result == null) result = caseStaticValue(nullValue);
				if (result == null) result = caseValue(nullValue);
				if (result == null) result = caseExpression(nullValue);
				if (result == null) result = caseExpressionAndVariables(nullValue);
				if (result == null) result = caseAbstractExpression(nullValue);
				if (result == null) result = caseExpressionOrRegion(nullValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.VARIABLE_VALUE: {
				VariableValue variableValue = (VariableValue)theEObject;
				T result = caseVariableValue(variableValue);
				if (result == null) result = caseValue(variableValue);
				if (result == null) result = caseExpression(variableValue);
				if (result == null) result = caseExpressionAndVariables(variableValue);
				if (result == null) result = caseAbstractExpression(variableValue);
				if (result == null) result = caseExpressionOrRegion(variableValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.FEATURE_ACCESS: {
				FeatureAccess featureAccess = (FeatureAccess)theEObject;
				T result = caseFeatureAccess(featureAccess);
				if (result == null) result = caseValue(featureAccess);
				if (result == null) result = caseExpression(featureAccess);
				if (result == null) result = caseExpressionAndVariables(featureAccess);
				if (result == null) result = caseAbstractExpression(featureAccess);
				if (result == null) result = caseExpressionOrRegion(featureAccess);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.COLLECTION_ACCESS: {
				CollectionAccess collectionAccess = (CollectionAccess)theEObject;
				T result = caseCollectionAccess(collectionAccess);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.SUB_FEATURE_ACCESS: {
				SubFeatureAccess subFeatureAccess = (SubFeatureAccess)theEObject;
				T result = caseSubFeatureAccess(subFeatureAccess);
				if (result == null) result = caseValue(subFeatureAccess);
				if (result == null) result = caseExpression(subFeatureAccess);
				if (result == null) result = caseExpressionAndVariables(subFeatureAccess);
				if (result == null) result = caseAbstractExpression(subFeatureAccess);
				if (result == null) result = caseExpressionOrRegion(subFeatureAccess);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.STRUCTURAL_FEATURE_VALUE: {
				StructuralFeatureValue structuralFeatureValue = (StructuralFeatureValue)theEObject;
				T result = caseStructuralFeatureValue(structuralFeatureValue);
				if (result == null) result = caseValue(structuralFeatureValue);
				if (result == null) result = caseExpression(structuralFeatureValue);
				if (result == null) result = caseExpressionAndVariables(structuralFeatureValue);
				if (result == null) result = caseAbstractExpression(structuralFeatureValue);
				if (result == null) result = caseExpressionOrRegion(structuralFeatureValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.OPERATION_VALUE: {
				OperationValue operationValue = (OperationValue)theEObject;
				T result = caseOperationValue(operationValue);
				if (result == null) result = caseValue(operationValue);
				if (result == null) result = caseExpression(operationValue);
				if (result == null) result = caseExpressionAndVariables(operationValue);
				if (result == null) result = caseAbstractExpression(operationValue);
				if (result == null) result = caseExpressionOrRegion(operationValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.CLOCK_DECLARATION: {
				ClockDeclaration clockDeclaration = (ClockDeclaration)theEObject;
				T result = caseClockDeclaration(clockDeclaration);
				if (result == null) result = caseVariableExpression(clockDeclaration);
				if (result == null) result = caseClock(clockDeclaration);
				if (result == null) result = caseExpression(clockDeclaration);
				if (result == null) result = caseNamedElement(clockDeclaration);
				if (result == null) result = caseExpressionAndVariables(clockDeclaration);
				if (result == null) result = caseExpressionOrRegion(clockDeclaration);
				if (result == null) result = caseAbstractExpression(clockDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.TIMED_EXPRESSION: {
				TimedExpression timedExpression = (TimedExpression)theEObject;
				T result = caseTimedExpression(timedExpression);
				if (result == null) result = caseOperationExpression(timedExpression);
				if (result == null) result = caseExpression(timedExpression);
				if (result == null) result = caseExpressionAndVariables(timedExpression);
				if (result == null) result = caseAbstractExpression(timedExpression);
				if (result == null) result = caseExpressionOrRegion(timedExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.CLOCK_ASSIGNMENT: {
				ClockAssignment clockAssignment = (ClockAssignment)theEObject;
				T result = caseClockAssignment(clockAssignment);
				if (result == null) result = caseVariableExpression(clockAssignment);
				if (result == null) result = caseExpression(clockAssignment);
				if (result == null) result = caseExpressionAndVariables(clockAssignment);
				if (result == null) result = caseExpressionOrRegion(clockAssignment);
				if (result == null) result = caseAbstractExpression(clockAssignment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.CLOCK: {
				Clock clock = (Clock)theEObject;
				T result = caseClock(clock);
				if (result == null) result = caseNamedElement(clock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScenarioExpressionsPackage.CLOCK_EXPRESSION: {
				ClockExpression clockExpression = (ClockExpression)theEObject;
				T result = caseClockExpression(clockExpression);
				if (result == null) result = caseExpression(clockExpression);
				if (result == null) result = caseExpressionAndVariables(clockExpression);
				if (result == null) result = caseAbstractExpression(clockExpression);
				if (result == null) result = caseExpressionOrRegion(clockExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Document</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Document</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDocument(Document object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Import</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Import</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImport(Import object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expression Or Region</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expression Or Region</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpressionOrRegion(ExpressionOrRegion object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractExpression(AbstractExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expression Region</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expression Region</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpressionRegion(ExpressionRegion object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expression And Variables</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expression And Variables</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpressionAndVariables(ExpressionAndVariables object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariableExpression(VariableExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpression(Expression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationExpression(OperationExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unary Operation Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unary Operation Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnaryOperationExpression(UnaryOperationExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Operation Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Operation Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinaryOperationExpression(BinaryOperationExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariable(Variable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Typed Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Typed Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypedVariable(TypedVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariableDeclaration(VariableDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Typed Variable Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Typed Variable Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypedVariableDeclaration(TypedVariableDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable Assignment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariableAssignment(VariableAssignment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseValue(Value object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Static Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Static Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStaticValue(StaticValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Integer Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Integer Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIntegerValue(IntegerValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBooleanValue(BooleanValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringValue(StringValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enum Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enum Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumValue(EnumValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Null Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Null Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNullValue(NullValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariableValue(VariableValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Access</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Access</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureAccess(FeatureAccess object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Collection Access</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Collection Access</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCollectionAccess(CollectionAccess object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sub Feature Access</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sub Feature Access</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubFeatureAccess(SubFeatureAccess object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Structural Feature Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Structural Feature Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStructuralFeatureValue(StructuralFeatureValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationValue(OperationValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Clock Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Clock Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClockDeclaration(ClockDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Timed Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Timed Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimedExpression(TimedExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Clock Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Clock Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClockExpression(ClockExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Clock Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Clock Assignment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClockAssignment(ClockAssignment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Clock</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Clock</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClock(Clock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ScenarioExpressionsSwitch
