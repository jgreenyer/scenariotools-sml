/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.expressions.scenarioExpressions.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.sml.expressions.scenarioExpressions.CollectionAccess;
import org.scenariotools.sml.expressions.scenarioExpressions.Expression;
import org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess;
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage;
import org.scenariotools.sml.expressions.scenarioExpressions.Value;
import org.scenariotools.sml.expressions.scenarioExpressions.Variable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature Access</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.FeatureAccessImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.FeatureAccessImpl#getValue <em>Value</em>}</li>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.FeatureAccessImpl#getCollectionAccess <em>Collection Access</em>}</li>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.FeatureAccessImpl#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FeatureAccessImpl extends ValueImpl implements FeatureAccess {
	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected EObject target;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected Value value;

	/**
	 * The cached value of the '{@link #getCollectionAccess() <em>Collection Access</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollectionAccess()
	 * @generated
	 * @ordered
	 */
	protected CollectionAccess collectionAccess;

	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<Expression> parameters;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureAccessImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScenarioExpressionsPackage.Literals.FEATURE_ACCESS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getTarget() {
		if (target != null && target.eIsProxy()) {
			InternalEObject oldTarget = (InternalEObject)target;
			target = eResolveProxy(oldTarget);
			if (target != oldTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ScenarioExpressionsPackage.FEATURE_ACCESS__TARGET, oldTarget, target));
			}
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(EObject newTarget) {
		EObject oldTarget = target;
		target = newTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenarioExpressionsPackage.FEATURE_ACCESS__TARGET, oldTarget, target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValue(Value newValue, NotificationChain msgs) {
		Value oldValue = value;
		value = newValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScenarioExpressionsPackage.FEATURE_ACCESS__VALUE, oldValue, newValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(Value newValue) {
		if (newValue != value) {
			NotificationChain msgs = null;
			if (value != null)
				msgs = ((InternalEObject)value).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScenarioExpressionsPackage.FEATURE_ACCESS__VALUE, null, msgs);
			if (newValue != null)
				msgs = ((InternalEObject)newValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScenarioExpressionsPackage.FEATURE_ACCESS__VALUE, null, msgs);
			msgs = basicSetValue(newValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenarioExpressionsPackage.FEATURE_ACCESS__VALUE, newValue, newValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CollectionAccess getCollectionAccess() {
		return collectionAccess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCollectionAccess(CollectionAccess newCollectionAccess, NotificationChain msgs) {
		CollectionAccess oldCollectionAccess = collectionAccess;
		collectionAccess = newCollectionAccess;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScenarioExpressionsPackage.FEATURE_ACCESS__COLLECTION_ACCESS, oldCollectionAccess, newCollectionAccess);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCollectionAccess(CollectionAccess newCollectionAccess) {
		if (newCollectionAccess != collectionAccess) {
			NotificationChain msgs = null;
			if (collectionAccess != null)
				msgs = ((InternalEObject)collectionAccess).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScenarioExpressionsPackage.FEATURE_ACCESS__COLLECTION_ACCESS, null, msgs);
			if (newCollectionAccess != null)
				msgs = ((InternalEObject)newCollectionAccess).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScenarioExpressionsPackage.FEATURE_ACCESS__COLLECTION_ACCESS, null, msgs);
			msgs = basicSetCollectionAccess(newCollectionAccess, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenarioExpressionsPackage.FEATURE_ACCESS__COLLECTION_ACCESS, newCollectionAccess, newCollectionAccess));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Expression> getParameters() {
		if (parameters == null) {
			parameters = new EObjectContainmentEList<Expression>(Expression.class, this, ScenarioExpressionsPackage.FEATURE_ACCESS__PARAMETERS);
		}
		return parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScenarioExpressionsPackage.FEATURE_ACCESS__VALUE:
				return basicSetValue(null, msgs);
			case ScenarioExpressionsPackage.FEATURE_ACCESS__COLLECTION_ACCESS:
				return basicSetCollectionAccess(null, msgs);
			case ScenarioExpressionsPackage.FEATURE_ACCESS__PARAMETERS:
				return ((InternalEList<?>)getParameters()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScenarioExpressionsPackage.FEATURE_ACCESS__TARGET:
				if (resolve) return getTarget();
				return basicGetTarget();
			case ScenarioExpressionsPackage.FEATURE_ACCESS__VALUE:
				return getValue();
			case ScenarioExpressionsPackage.FEATURE_ACCESS__COLLECTION_ACCESS:
				return getCollectionAccess();
			case ScenarioExpressionsPackage.FEATURE_ACCESS__PARAMETERS:
				return getParameters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScenarioExpressionsPackage.FEATURE_ACCESS__TARGET:
				setTarget((EObject)newValue);
				return;
			case ScenarioExpressionsPackage.FEATURE_ACCESS__VALUE:
				setValue((Value)newValue);
				return;
			case ScenarioExpressionsPackage.FEATURE_ACCESS__COLLECTION_ACCESS:
				setCollectionAccess((CollectionAccess)newValue);
				return;
			case ScenarioExpressionsPackage.FEATURE_ACCESS__PARAMETERS:
				getParameters().clear();
				getParameters().addAll((Collection<? extends Expression>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScenarioExpressionsPackage.FEATURE_ACCESS__TARGET:
				setTarget((EObject)null);
				return;
			case ScenarioExpressionsPackage.FEATURE_ACCESS__VALUE:
				setValue((Value)null);
				return;
			case ScenarioExpressionsPackage.FEATURE_ACCESS__COLLECTION_ACCESS:
				setCollectionAccess((CollectionAccess)null);
				return;
			case ScenarioExpressionsPackage.FEATURE_ACCESS__PARAMETERS:
				getParameters().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScenarioExpressionsPackage.FEATURE_ACCESS__TARGET:
				return target != null;
			case ScenarioExpressionsPackage.FEATURE_ACCESS__VALUE:
				return value != null;
			case ScenarioExpressionsPackage.FEATURE_ACCESS__COLLECTION_ACCESS:
				return collectionAccess != null;
			case ScenarioExpressionsPackage.FEATURE_ACCESS__PARAMETERS:
				return parameters != null && !parameters.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //FeatureAccessImpl
