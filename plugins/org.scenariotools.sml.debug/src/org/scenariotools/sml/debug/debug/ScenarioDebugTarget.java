/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.debug;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarkerDelta;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IMemoryBlock;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.IThread;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.scenariotools.sml.Collaboration;
import org.scenariotools.sml.debug.HistoryAgent;
import org.scenariotools.sml.debug.SimulationAgent;
import org.scenariotools.sml.debug.DebugFactory;
import org.scenariotools.sml.debug.SimulationManager;
import org.scenariotools.sml.debug.listener.IStepPerformedListener;
import org.scenariotools.sml.debug.plugin.Activator;
import org.scenariotools.sml.debug.util.SMLEnabledFragmentMarker;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.runtime.util.SMLRuntimeStateUtil;

public class ScenarioDebugTarget extends ScenarioDebugElement implements
		IDebugTarget {

	private static Logger logger = Activator.getLogManager().getLogger(
			ScenarioDebugTarget.class.getName());
	
	protected ILaunch launch;
	protected IFile smlFile;
	protected EList<IFile> collaborationFiles;
	protected SMLEnabledFragmentMarker smlEnabledFragmentMarker;
	
	protected SimulationManager simulationManager;
	
	protected List<AbstractScenarioThread> activeMSDThreads;
	protected List<ObjectThread> systemObjectThreads;
	protected List<ObjectThread> environmentObjectThreads;
	
	protected IStepPerformedListener stepPerformedListener = new IStepPerformedListener() {
		@Override
		public void stepPerformed() {
			fireChangeEvent(DebugEvent.CONTENT);
		}
	};

	
	protected SimulationManager createSimulationManager(){
		return DebugFactory.eINSTANCE.createSimulationManager();
	}

	protected SMLRuntimeStateGraph createMSDRuntimeStateGraph(){
		return RuntimeFactory.eINSTANCE.createSMLRuntimeStateGraph();
	}

	public ScenarioDebugTarget(ILaunch launch, Configuration scenarioRunConfiguration,
			String configuredEnvironmentSimulationAgentNsURI,
			String configuredEnvironmentSimulationAgentEClassName,
			String configuredSystemSimulationAgentNsURI,
			String configuredSystemSimulationAgentEClassName,
			String delayMilliseconds, 
			boolean startInPauseMode) {
		super(null);
		this.launch = launch;
		try {
			this.smlFile = getSMLFile(scenarioRunConfiguration);
			this.collaborationFiles = getCollaborationFiles(scenarioRunConfiguration);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error("Connot load SML files!", e);
		}
		this.smlEnabledFragmentMarker = new SMLEnabledFragmentMarker(this);
		
		logger.debug("Creating runtime manager");
		
		setSimulationManager(createSimulationManager());
		
		getSimulationManager().setScenarioRunConfiguration(scenarioRunConfiguration);
		
		getSimulationManager().setPause(startInPauseMode);
		
		getSimulationManager().setStepDelayMilliseconds(Integer.parseInt(delayMilliseconds));

		SimulationAgent environmentSimulationAgent = loadSimulationAgent(configuredEnvironmentSimulationAgentNsURI, configuredEnvironmentSimulationAgentEClassName);
		environmentSimulationAgent.setSimulationManager(getSimulationManager());
		getSimulationManager().getRegisteredEnvironmentSimulationAgent().add(environmentSimulationAgent);
		getSimulationManager().setCurrentEnvironmentSimulationAgent(environmentSimulationAgent);

		SimulationAgent systemSimulationAgent = loadSimulationAgent(configuredSystemSimulationAgentNsURI, configuredSystemSimulationAgentEClassName);
		systemSimulationAgent.setSimulationManager(getSimulationManager());
		getSimulationManager().getRegisteredSystemSimulationAgent().add(systemSimulationAgent);
		getSimulationManager().setCurrentSystemSimulationAgent(systemSimulationAgent);
		
		SMLRuntimeStateGraph msdRuntimeStateGraph = createMSDRuntimeStateGraph();
		SMLRuntimeState msdRuntimeState = msdRuntimeStateGraph.init(scenarioRunConfiguration);
		
		getSimulationManager().setSmlRuntimeStateGraph(msdRuntimeStateGraph);
		getSimulationManager().setCurrentSMLRuntimeState(msdRuntimeState);
		SMLRuntimeStateUtil.setPassedIndex(getSimulationManager().getCurrentSMLRuntimeState(), "0");
		
		HistoryAgent historyAgent = DebugFactory.eINSTANCE.createHistoryAgent();
		historyAgent.setSimulationManager(getSimulationManager());
		getSimulationManager().getRegisteredHistoryAgent().add(historyAgent);
		getSimulationManager().setCurrentHistoryAgent(historyAgent);
		
		logger.debug("init current EnvironmentSimulationAgent");
		getSimulationManager().getCurrentEnvironmentSimulationAgent().init();
		
		logger.debug("init current SystemSimulationAgent");
		getSimulationManager().getCurrentSystemSimulationAgent().init();
		
		getSimulationManager().getCurrentHistoryAgent().init();
		
		getSimulationManager().getRegisteredStepPerformedListener().add(stepPerformedListener);
		
		getSimulationManager().setActiveSimulationAgent(environmentSimulationAgent);
		
		logger.debug("Runtime manager is initialized");

	}
	
	
	protected SimulationAgent loadSimulationAgent(String SimulationAgentNsURI, String SimulationAgentEClassName){
		EPackage environmentSimulationAgentPackage = EPackage.Registry.INSTANCE.getEPackage(SimulationAgentNsURI);
		if (environmentSimulationAgentPackage != null) {
			EClass environmentSimulationAgentClass = (EClass) environmentSimulationAgentPackage.getEClassifier(SimulationAgentEClassName);
			if (environmentSimulationAgentClass == null)
				logger.error("The specified class \"" + SimulationAgentEClassName + "\" cannot be found");
			EFactory eFactory = EPackage.Registry.INSTANCE.getEFactory(SimulationAgentNsURI); 
			EObject eObject = eFactory.create(environmentSimulationAgentClass);
			if (eObject != null && eObject instanceof SimulationAgent)
				return (SimulationAgent) eObject;
			else{
				logger.error("Cannot create a runtime agent object from eClass \"" + environmentSimulationAgentClass + "\" from EFactory " + eFactory);
				return null;
			}
		}else{
			logger.error("The specified package with nsURI \"" + SimulationAgentNsURI
					+ "\" cannot be found");
			return null;
		}

	}
	
	/**
	 * Retrieve the source file from an {@link ILaunchConfiguration}.
	 * 
	 * @param configuration
	 *            configuration to use
	 * @return source file or <code>null</code>
	 * @throws CoreException
	 */
	public IFile getSourceFile(final EObject element) throws CoreException {
		Resource resource = element.eResource();
		String platformString = resource.getURI().toPlatformString(true);
		return  ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(platformString));		
	}
	
	private IFile getSMLFile(Configuration smlRunConfig) throws CoreException{
		return getSourceFile(smlRunConfig.getSpecification());
	}
	
	private EList<IFile> getCollaborationFiles(Configuration smlRunConfig){
		EList<IFile> sourceFiles = new BasicEList<IFile>();
		// Included collaborations have their own resource.
		for(Collaboration col : smlRunConfig.getConsideredCollaborations()){
			if(smlRunConfig.getSpecification().getIncludedCollaborations().contains(col))
				continue;
			Resource colResource = col.eResource();
			String colPlatformString = colResource.getURI().toPlatformString(true);
			IFile colFile = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(colPlatformString));		
			sourceFiles.add(colFile);
		}
		return sourceFiles;
	}

	
	public SimulationManager getSimulationManager() {
		return simulationManager;
	}

	public SMLEnabledFragmentMarker getSDEnabledFragmentMarker(){
		return this.smlEnabledFragmentMarker;
	}
	
	public void setSimulationManager(SimulationManager SimulationManager) {
		this.simulationManager = SimulationManager;
	}

	public EList<IFile> getFiles(){
		EList<IFile> result = new BasicEList<IFile>();
		result.add(this.smlFile);
		result.addAll(collaborationFiles);
		return result;
	}
	
	public IFile getSMLFile(){
		return this.smlFile;
	}
	
	@Override
	public ILaunch getLaunch() {
		return launch;
	}
	
	@Override
	public boolean canTerminate() {
		return !getSimulationManager().isTerminated();
	}

	@Override
	public boolean isTerminated() {
		return getSimulationManager().isTerminated();
	}

	@Override
	public void terminate() throws DebugException {
		getSimulationManager().terminate();
		this.getSDEnabledFragmentMarker().removeAll();
		fireEvent(new DebugEvent(getDebugTarget(), DebugEvent.TERMINATE));
	}

	@Override
	public boolean canResume() {
		return getSimulationManager().isPause() && !getSimulationManager().isTerminated();
	}

	@Override
	public boolean canSuspend() {
		return !getSimulationManager().isPause() && !getSimulationManager().isTerminated();
	}

	@Override
	public boolean isSuspended() {
		return getSimulationManager().isPause();
	}

	@Override
	public void resume() throws DebugException {
		getSimulationManager().setPause(false);
		fireEvent(new DebugEvent(this, DebugEvent.RESUME));
		fireChangeEvent(DebugEvent.CONTENT);
	}

	@Override
	public void suspend() throws DebugException {
		getSimulationManager().setPause(true);
		fireEvent(new DebugEvent(this, DebugEvent.SUSPEND));
		fireChangeEvent(DebugEvent.CONTENT);
	}
	
	@Override
	public void breakpointAdded(IBreakpoint breakpoint) {
		// TODO Auto-generated method stub

	}

	@Override
	public void breakpointRemoved(IBreakpoint breakpoint, IMarkerDelta delta) {
		// TODO Auto-generated method stub

	}

	@Override
	public void breakpointChanged(IBreakpoint breakpoint, IMarkerDelta delta) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean canDisconnect() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void disconnect() throws DebugException {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isDisconnected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean supportsStorageRetrieval() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public IMemoryBlock getMemoryBlock(long startAddress, long length)
			throws DebugException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IProcess getProcess() {
		// TODO Auto-generated method stub
		return null;
	}

	protected List<AbstractScenarioThread> getActiveMSDThreads(){
		if (activeMSDThreads == null)
			activeMSDThreads = new ArrayList<AbstractScenarioThread>();
		return activeMSDThreads;
	}
	
	protected List<ObjectThread> getSystemObjectThreads(){
		if (systemObjectThreads == null)
			systemObjectThreads = new ArrayList<ObjectThread>();
		return systemObjectThreads;
	}

	protected List<ObjectThread> getEnvironmentObjectThreads(){
		if (environmentObjectThreads == null)
			environmentObjectThreads = new ArrayList<ObjectThread>();
		return environmentObjectThreads;
	}

	@Override
	public IThread[] getThreads() throws DebugException {
		refreshActiveMSDThreads();
		refreshEnvironmentObectThreads();
		refreshSystemObjectThreads();
		
		List<IThread> threadList = new ArrayList<IThread>();
		threadList.addAll(getActiveMSDThreads());
		threadList.addAll(getEnvironmentObjectThreads());
		threadList.addAll(getSystemObjectThreads());
		
		return threadList.toArray(new IThread[] {});
	}
	
	protected void refreshSystemObjectThreads(){
		// 1. remove orphaned ObectThreads
		EList<EObject> eObjectList = getSimulationManager().getCurrentSMLRuntimeState().getObjectSystem().getControllableObjects();
		EList<EObject> eObjectsWithoutObjectThreads = new BasicEList<EObject>(eObjectList);
		Iterator<ObjectThread> objectThreadIterator =  getSystemObjectThreads().iterator();
		while (objectThreadIterator.hasNext()) {
			ObjectThread objectThread = (ObjectThread) objectThreadIterator
					.next();
			if(!eObjectList.contains(objectThread.getSimulationObject())){
				objectThreadIterator.remove();
			}else{
				objectThread.setDynamicSimulationObject(getDynamicSimulationObject(objectThread.getSimulationObject()));
				eObjectsWithoutObjectThreads.remove(objectThread.getSimulationObject());
			}
			
		}
		
		// 2. create new Objecthreads
		for (EObject eObject : eObjectsWithoutObjectThreads) {
			getSystemObjectThreads().add(new ObjectThread(this, eObject, getDynamicSimulationObject(eObject), true));
		}
	}
	
	private EObject getDynamicSimulationObject(EObject staticSimulationObject){
		return getSimulationManager().getCurrentSMLRuntimeState().getDynamicObjectContainer().getStaticEObjectToDynamicEObjectMap().get(staticSimulationObject);
	}
	
	protected void refreshEnvironmentObectThreads(){
		// 1. remove orphaned ObectThreads
		EList<EObject> eObjectList = getSimulationManager().getCurrentSMLRuntimeState().getObjectSystem().getUncontrollableObjects();
		EList<EObject> eObjectsWithoutObjectThreads = new BasicEList<EObject>(eObjectList);
		Iterator<ObjectThread> objectThreadIterator =  getEnvironmentObjectThreads().iterator();
		while (objectThreadIterator.hasNext()) {
			ObjectThread objectThread = (ObjectThread) objectThreadIterator
					.next();
			if(!eObjectList.contains(objectThread.getSimulationObject())){
				objectThreadIterator.remove();
			}else{
				objectThread.setDynamicSimulationObject(getDynamicSimulationObject(objectThread.getSimulationObject()));
				eObjectsWithoutObjectThreads.remove(objectThread.getSimulationObject());
			}
			
		}
		
		// 2. create new Objecthreads
		for (EObject eObject : eObjectsWithoutObjectThreads) {
			getEnvironmentObjectThreads().add(new ObjectThread(this, eObject, getDynamicSimulationObject(eObject), false));
		}
	}

	
	protected void refreshActiveMSDThreads(){
		// 1. remove orphaned ActiveMSDThreads
		EList<ActiveScenario> currentActiveProcessList = getSimulationManager().getCurrentSMLRuntimeState().getActiveScenarios();
		EList<ActiveScenario> cutsWithoutActiveMSDThreadList = new BasicEList<ActiveScenario>(currentActiveProcessList);
		Iterator<AbstractScenarioThread> activeMSDThreadIterator =  getActiveMSDThreads().iterator();
		while (activeMSDThreadIterator.hasNext()) {
			AbstractScenarioThread activeMSDThread = activeMSDThreadIterator
					.next();
			if(activeMSDThread instanceof ActiveMSDThread){
				if(!currentActiveProcessList.contains(((ActiveMSDThread)activeMSDThread).getActiveProcess())){
					activeMSDThread.remove();
					activeMSDThreadIterator.remove();
				}else{
					cutsWithoutActiveMSDThreadList.remove(((ActiveMSDThread)activeMSDThread).getActiveProcess());
				}
			}
		}
		
		// 2. create new ActiveMSDThreads
		for (ActiveScenario activeProcess : cutsWithoutActiveMSDThreadList) {
			getActiveMSDThreads().add(new ActiveMSDThread(this, (ActiveScenario)activeProcess));
		}
	}


	@Override
	public boolean hasThreads() throws DebugException {
		return true;
	}

	@Override
	public String getName() throws DebugException {
		SMLRuntimeState state=getSimulationManager().getCurrentSMLRuntimeState();
		String violationsString=(state.isSafetyViolationOccurredInGuarantees()?"[guarantee violated] ":"")
				//+(state.isSafetyViolationOccurredInRequirements()?"[requirements violated] ":"")
				+(state.isSafetyViolationOccurredInAssumptions()?"[assumptions violated] ":"");

		return violationsString 
				+ "Object System: " 
				+ getSimulationRootObjectsName() 
				+ " executed with SML system specification: " 
				+ getSpecificationName();
	}

	protected String getSpecificationName(){
		return getSimulationManager().getSmlRuntimeStateGraph().getConfiguration().getSpecification().getName();
	}
	
	private String getSimulationRootObjectsName(){
		try {
			EList<EObject> simulationObjects = new BasicEList<EObject>();
			simulationObjects.addAll(getSimulationManager().getCurrentSMLRuntimeState().getObjectSystem().getUncontrollableObjects());
			simulationObjects.addAll(getSimulationManager().getCurrentSMLRuntimeState().getObjectSystem().getControllableObjects());
			String numberOfObjectsString = "(" + (getSimulationManager().getCurrentSMLRuntimeState().getObjectSystem().getUncontrollableObjects().size() + getSimulationManager().getCurrentSMLRuntimeState().getObjectSystem().getControllableObjects().size()) + " objects)";
			return numberOfObjectsString + " " + simulationObjects.toString();
		} catch (NullPointerException e) {
			return "<no simulation objects>";
		}
	}
	
	@Override
	public ScenarioDebugTarget getDebugTarget() {
		return this;
	}
	
	@Override
	public boolean supportsBreakpoint(IBreakpoint breakpoint) {
		// TODO Auto-generated method stub
		return false;
	}
}
