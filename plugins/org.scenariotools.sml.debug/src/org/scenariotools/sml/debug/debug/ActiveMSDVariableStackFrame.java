/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.debug;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.core.resources.IFile;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IVariable;
import org.scenariotools.sml.expressions.scenarioExpressions.TypedVariableDeclaration;
import org.scenariotools.sml.expressions.scenarioExpressions.Variable;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;

public class ActiveMSDVariableStackFrame extends AbstractActiveMSDStackFrame{

	private List<ActiveMSDVariable> variables;

	public ActiveMSDVariableStackFrame(IDebugTarget target, ActiveScenario activeMSD,
			AbstractScenarioThread activeMSDThread) {
		super(target, activeMSD, activeMSDThread);
		// TODO Auto-generated constructor stub
	}

	@Override
	public IVariable[] getVariables() throws DebugException {

		refreshVariables();
		
		return variables.toArray(new ActiveMSDVariable[]{});
	}
	
	@Override
	public IFile getSourceFile(){
		return ((ScenarioDebugTarget)getDebugTarget()).getSMLFile();
	}
	
	protected void getNestedVariales(ActivePart activeInteraction){
		if(activeInteraction.getEnabledNestedActiveInteractions().isEmpty()){
			for(Entry<Variable, Object> entry : activeInteraction.getVariableMap().entrySet()){
				variables.add(new ActiveMSDVariable(getDebugTarget(), ((TypedVariableDeclaration)entry.getKey()).getName(), entry.getValue()));
			}
		}else{
			for(Entry<Variable, Object> entry : activeInteraction.getVariableMap().entrySet()){
				variables.add(new ActiveMSDVariable(getDebugTarget(), ((TypedVariableDeclaration)entry.getKey()).getName(), entry.getValue()));
			}
			for(ActivePart nestedActiveInteraction : activeInteraction.getEnabledNestedActiveInteractions()){
				getNestedVariales(nestedActiveInteraction);
			}
		}
	}
	
	protected void refreshVariables() throws DebugException{
		
		variables = new ArrayList<ActiveMSDVariable>();
		
		getNestedVariales(getActiveMSD().getMainActiveInteraction());
		
//		// 1. remove orphaned Variables
//		
//		EList<String> currentActiveMSDVariables = new BasicEList<String>();
//		currentActiveMSDVariables.addAll(getActiveMSD().getVariableValuations().getVariableNameToEObjectValueMap().keySet());
//		currentActiveMSDVariables.addAll(getActiveMSD().getVariableValuations().getVariableNameToStringValueMap().keySet());
//		currentActiveMSDVariables.addAll(getActiveMSD().getVariableValuations().getVariableNameToIntegerValueMap().keySet());
//		currentActiveMSDVariables.addAll(getActiveMSD().getVariableValuations().getVariableNameToBooleanValueMap().keySet());
//		
//				
//		EList<String> currentActiveMSDVariablesWithoutRepresentation = new BasicEList<String>(currentActiveMSDVariables);
//		Iterator<ActiveMSDVariable> activeMSDVariableIterator = variables.iterator();
//		
//		while (activeMSDVariableIterator.hasNext()) {
//			ActiveMSDVariable activeMSDVariable = activeMSDVariableIterator.next();
//			boolean activeMSDVariableForVariableExists = false;
//			for (String varName : currentActiveMSDVariables) {
//				if (getBindingForVariableName(varName) == null) continue;
//				if (activeMSDVariable.getName() == varName){
//					activeMSDVariableForVariableExists = true;
//					break;
//				}
//			}
//			if (!activeMSDVariableForVariableExists){
//				activeMSDVariableIterator.remove();
//			}else{
//				currentActiveMSDVariablesWithoutRepresentation.remove(activeMSDVariable.getName());
//			}
//		}
//
//		// 2. create new Variables
//		
//		for (String varName : currentActiveMSDVariablesWithoutRepresentation) {
//			Object bindingForVariable = getBindingForVariableName(varName);
//			if (bindingForVariable == null) continue;
//			variables.add(new ActiveMSDVariable(getDebugTarget(), varName, bindingForVariable));
//		}
	}
	
	private Object getBindingForVariableName(String varName){
//		if (getActiveMSD().getVariableValuations().getVariableNameToEObjectValueMap().get(varName) != null) return getActiveMSD().getVariableValuations().getVariableNameToEObjectValueMap().get(varName); 
//		else if (getActiveMSD().getVariableValuations().getVariableNameToStringValueMap().get(varName) != null) return getActiveMSD().getVariableValuations().getVariableNameToStringValueMap().get(varName); 
//		else if (getActiveMSD().getVariableValuations().getVariableNameToIntegerValueMap().get(varName) != null) return getActiveMSD().getVariableValuations().getVariableNameToIntegerValueMap().get(varName); 
//		else if (getActiveMSD().getVariableValuations().getVariableNameToBooleanValueMap().get(varName) != null) return getActiveMSD().getVariableValuations().getVariableNameToBooleanValueMap().get(varName); 
//		else 
		return null;
	}

	
	@Override
	public boolean hasVariables() throws DebugException {
		refreshVariables();
		return !variables.isEmpty();
	}

	@Override
	public String getName() throws DebugException {
		return "Scenario Variables";
	}

	

}
