/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.debug;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IVariable;
import org.eclipse.xtext.nodemodel.ICompositeNode;
import org.scenariotools.sml.InteractionFragment;
import org.scenariotools.sml.runtime.ActiveScenario;

public class EnabledFragmentStackFrame extends AbstractActiveMSDStackFrame{

	private ICompositeNode node;
	private InteractionFragment interactionFragment;
	
	public EnabledFragmentStackFrame(IDebugTarget target,
			ActiveScenario activeMSD, AbstractScenarioThread activeMSDThread, ICompositeNode node, InteractionFragment fragment) {
		super(target, activeMSD, activeMSDThread);
		this.interactionFragment = fragment;
		this.setNode(node);
	}

	@Override
	public IVariable[] getVariables() throws DebugException {
		// TODO Auto-generated method stub
		return new IVariable[0];
	}

	@Override
	public boolean hasVariables() throws DebugException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getName() throws DebugException {
		return "Enabled Fragment";
	}

	@Override
	public IFile getSourceFile() throws CoreException {
		return ((ScenarioDebugTarget)getDebugTarget()).getSourceFile(interactionFragment);
	}

	@Override
	public int getLineNumber() throws DebugException {
//		return NodeModelUtils.getNode(getActiveMSD().getMainActiveInteraction().getEnabledFragment()).getStartLine();
		return node.getStartLine();
	}
	
	public void setNode(ICompositeNode node){
		this.node = node;
		((ScenarioDebugTarget)getDebugTarget()).getSDEnabledFragmentMarker().update(this, node);
	}
	
	public void remove(){
		((ScenarioDebugTarget)getDebugTarget()).getSDEnabledFragmentMarker().remove(this);
	}
}
