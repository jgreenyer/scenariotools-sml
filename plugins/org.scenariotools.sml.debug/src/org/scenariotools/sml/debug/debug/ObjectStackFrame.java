/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.debug;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IRegisterGroup;
import org.eclipse.debug.core.model.IStackFrame;
import org.eclipse.debug.core.model.IThread;
import org.eclipse.debug.core.model.IVariable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

public class ObjectStackFrame extends ScenarioDebugElement implements IStackFrame {

	ObjectThread objectThread;
	private EObject staticSimulationObject;
	private EObject dynamicSimulationObject;
	private List<ObjectVariable> objectVariables;
	
	public ObjectStackFrame(IDebugTarget target, ObjectThread objectThread, EObject simulationObject, EObject dynamicSimulationObject) {
		super(target);
		this.objectThread = objectThread;
		this.staticSimulationObject = simulationObject;
		this.dynamicSimulationObject = dynamicSimulationObject;
		initObjectVariables();
	}
	
	private void initObjectVariables() {
		objectVariables = new ArrayList<ObjectVariable>();
		for (EStructuralFeature eStructuralFeature : dynamicSimulationObject.eClass().getEAllStructuralFeatures()) {
			objectVariables.add(new ObjectVariable(getDebugTarget(), dynamicSimulationObject, eStructuralFeature));
		}
	}

	public IFile getSourceFile(){
		return ((ScenarioDebugTarget)getDebugTarget()).getSMLFile();
	}
	
	@Override
	public boolean canStepInto() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canStepOver() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canStepReturn() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isStepping() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void stepInto() throws DebugException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stepOver() throws DebugException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stepReturn() throws DebugException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean canResume() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canSuspend() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSuspended() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void resume() throws DebugException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void suspend() throws DebugException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean canTerminate() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isTerminated() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void terminate() throws DebugException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IThread getThread() {
		return objectThread;
	}

	@Override
	public IVariable[] getVariables() throws DebugException {
		return objectVariables.toArray(new IVariable[]{});
	}

	@Override
	public boolean hasVariables() throws DebugException {
		return !objectVariables.isEmpty();
	}

	@Override
	public int getLineNumber() throws DebugException {
		return -1;
	}

	@Override
	public int getCharStart() throws DebugException {
		return -1;
	}

	@Override
	public int getCharEnd() throws DebugException {
		return -1;
	}

	@Override
	public String getName() throws DebugException {
		return "Feature values";
	}

	@Override
	public IRegisterGroup[] getRegisterGroups() throws DebugException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasRegisterGroups() throws DebugException {
		// TODO Auto-generated method stub
		return false;
	}

	public void setDynamicSimulationObject(EObject dynamicSimulationObject) {
		this.dynamicSimulationObject = dynamicSimulationObject;
		initObjectVariables();
		
	}

}
