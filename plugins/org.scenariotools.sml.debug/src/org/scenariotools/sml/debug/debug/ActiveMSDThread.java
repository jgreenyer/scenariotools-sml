/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.debug;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IStackFrame;
import org.eclipse.xtext.nodemodel.ICompositeNode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.scenariotools.sml.InteractionFragment;
import org.scenariotools.sml.ScenarioKind;
import org.scenariotools.sml.debug.plugin.Activator;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;

public class ActiveMSDThread extends AbstractScenarioThread {

	private static Logger logger = Activator.getLogManager().getLogger(
			ActiveMSDThread.class.getName());

	protected ActiveScenario activeMSD;
	protected List<EnabledFragmentStackFrame> enabledFragmentStackFrames;
	protected ActiveMSMLifelineBindingStackFrame activeMSMLifelineBindingStackFrame;
	protected ActiveMSDVariableStackFrame activeMSDVariableStackFrame;

	
	public ActiveMSDThread(IDebugTarget target, ActiveScenario activeMSD) {
		super(target);
		if (activeMSD != null)
			this.activeMSD = activeMSD;
			enabledFragmentStackFrames = new ArrayList<EnabledFragmentStackFrame>();
	}
	
	public ActiveScenario getActiveProcess(){
		return activeMSD;
	}
	
	private List<NodeFragment> getEnabledFragments(){
		List<NodeFragment> nodes = getEnabledFragments(activeMSD.getMainActiveInteraction());
		return nodes;
		
	}
	
	private List<NodeFragment> getEnabledFragments(ActivePart activeInteraction){
		if(activeInteraction.getEnabledNestedActiveInteractions().isEmpty()){
			List<NodeFragment> l = new ArrayList<NodeFragment>();
			l.add(new NodeFragment(NodeModelUtils.getNode(activeInteraction.getInteractionFragment()), activeInteraction.getInteractionFragment()));
			return l;
		}else{
			List<NodeFragment> l = new ArrayList<NodeFragment>();
			for(ActivePart nestedActiveInteraction : activeInteraction.getEnabledNestedActiveInteractions()){
				l.addAll(getEnabledFragments(nestedActiveInteraction));
			}
			return l;
		}
	}
	
	private class NodeFragment{
		public ICompositeNode node;
		public InteractionFragment fragment;
		public NodeFragment(ICompositeNode node, InteractionFragment fragment){
			this.node = node;
			this.fragment = fragment;
		}
	}
	
	@Override
	public boolean hasStackFrames() throws DebugException {
		// FIXME Work around to get the marker on the screen.
		// The marker are part of the EnabledFragmentStackFrame.
		// Next problem: This function is only called if the debug view is visible.
		// Only visible parts are refreshed... 
		// TODO use DebugEvents for refreshing the markers(DebugEvent.SUSPEND)  
		getStackFrames();
		return true;
	}
	
	@Override
	public IStackFrame[] getStackFrames() throws DebugException {
		List<NodeFragment> nodes = getEnabledFragments();
		if(nodes.size() < enabledFragmentStackFrames.size()){
			for(int i = enabledFragmentStackFrames.size(); i > nodes.size(); i--){
				enabledFragmentStackFrames.get(i-1).remove();
				enabledFragmentStackFrames.remove(i-1);
			}
		}
		for(int i = 0; i < nodes.size(); i++){
			ICompositeNode node = nodes.get(i).node;
			if(i < enabledFragmentStackFrames.size()){
				enabledFragmentStackFrames.get(i).setNode(node);
			}else{
				enabledFragmentStackFrames.add(new EnabledFragmentStackFrame(getDebugTarget(), getActiveProcess(), this, node, nodes.get(i).fragment));
			}
		}
		if (activeMSMLifelineBindingStackFrame == null)
			activeMSMLifelineBindingStackFrame = new ActiveMSMLifelineBindingStackFrame(getDebugTarget(), activeMSD, this);
		if (activeMSDVariableStackFrame == null)
			activeMSDVariableStackFrame = new ActiveMSDVariableStackFrame(getDebugTarget(), activeMSD, this);
		
		List<IStackFrame> stackFrames = new ArrayList<IStackFrame>();
		stackFrames.addAll(enabledFragmentStackFrames);
		stackFrames.add(activeMSMLifelineBindingStackFrame);
		stackFrames.add(activeMSDVariableStackFrame);
		return stackFrames.toArray(new IStackFrame[stackFrames.size()]);
	}

	@Override
	public IStackFrame getTopStackFrame() throws DebugException {
		return enabledFragmentStackFrames.isEmpty() ? null : enabledFragmentStackFrames.get(0);
	}
	
	@Override
	public String getName() throws DebugException {
		String name = "";
		
		if(getActiveProcess().getScenario().getKind() == ScenarioKind.GUARANTEE)
			name += "guarantee scenario ";
		else
			name +=	"assumption scenario: ";
		
		name += getActiveProcess().getScenario().getName();

		return name;
	}
	
	@Override
	public void remove(){
		for(EnabledFragmentStackFrame frame :enabledFragmentStackFrames){
			frame.remove();
		}
	}
}
