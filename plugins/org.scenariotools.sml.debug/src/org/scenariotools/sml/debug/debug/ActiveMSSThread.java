/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.debug;

import org.apache.log4j.Logger;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IStackFrame;
import org.scenariotools.sml.debug.plugin.Activator;
import org.scenariotools.sml.runtime.ActiveScenario;

public class ActiveMSSThread extends AbstractScenarioThread {

	private static Logger logger = Activator.getLogManager().getLogger(
			ActiveMSSThread.class.getName());

	private ActiveScenario activeMSS;
	private ActiveMSDVariableStackFrame activeMSDVariableStackFrame;

	public ActiveMSSThread(IDebugTarget target, ActiveScenario activeMSS) {
		super(target);
		if (activeMSS != null)
			this.activeMSS = activeMSS;
	}

	public ActiveScenario getActiveProcess() {
		return activeMSS;
	}

	@Override
	public boolean hasStackFrames() throws DebugException {
		return true;
	}

	@Override
	public IStackFrame[] getStackFrames() throws DebugException {
		if (activeMSDVariableStackFrame == null)
			activeMSDVariableStackFrame = new ActiveMSDVariableStackFrame(getDebugTarget(), activeMSS, this);
		return new IStackFrame[] { activeMSDVariableStackFrame };
	}

	@Override
	public String getName() throws DebugException {
//		String bindingString = "";
//
//		String returnString = "active ";
//		if (RuntimeUtil.Helper.isEnvironmentAssumptionProcess(activeMSS)) {
//			returnString += "assumption ";
//		} else {
//			returnString += "requirement ";
//		}
//
//		String cutString = "";
//		cutString += " (";
//		cutString += activeMSS.getCurrentState().getUmlState();
//		cutString += ")";
//
//		return returnString + "MSS: "
//				+ getActiveProcess().getStateMachine().getName() + " "
//				+ bindingString + cutString;
		return "MSS";
	}

}
