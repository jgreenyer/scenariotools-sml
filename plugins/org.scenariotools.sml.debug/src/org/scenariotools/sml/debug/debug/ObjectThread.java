/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.debug;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IStackFrame;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

public class ObjectThread extends AbstractScenarioThread {

	private EObject simulationObject;
	private EObject dynamicSymulationObject;
	private boolean isSystemObject;
	private ObjectStackFrame objectStackFrame;
	
	public ObjectThread(IDebugTarget target, EObject simulationObject, EObject dynamicSimulationObject, boolean isSystemObject) {
		super(target);
		this.simulationObject = simulationObject;
		this.dynamicSymulationObject = dynamicSimulationObject;
		this.isSystemObject = isSystemObject;
		this.objectStackFrame = new ObjectStackFrame(target, this, simulationObject, dynamicSymulationObject);
	}
	
	public EObject getSimulationObject(){
		return simulationObject;
	}
	
	@Override
	public String getName() throws DebugException {
		EStructuralFeature nameFeature = getSimulationObject().eClass().getEStructuralFeature("name");
		StringBuilder objectString = new StringBuilder();
		
		if (isSystemObject){
			objectString.append("Sys Object: ");
		}else{
			objectString.append("Env Object: ");
		}
		if(nameFeature != null && getSimulationObject().eIsSet(nameFeature)){
			objectString.append(getSimulationObject().eGet(nameFeature));
		}else{
			objectString.append(getSimulationObject().toString());
		}
		objectString.append(", Type: ");
		objectString.append(getSimulationObject().eClass().getName());
		objectString.append(", ECoreID: ");
		objectString.append(Integer.toHexString(System.identityHashCode(getSimulationObject())));
		
		return objectString.toString();
	}
	
	@Override
	public IStackFrame[] getStackFrames() throws DebugException {
		return new IStackFrame[]{objectStackFrame};
	}
	
	@Override
	public boolean hasStackFrames() throws DebugException {
		return true;
	}

	public void setDynamicSimulationObject(EObject dynamicSimulationObject) {
		this.dynamicSymulationObject = dynamicSimulationObject;
		this.objectStackFrame.setDynamicSimulationObject(dynamicSimulationObject);
		
	}
}
