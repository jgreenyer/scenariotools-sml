/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.debug.DebugPackage;
import org.scenariotools.sml.debug.SimulationAgent;
import org.scenariotools.sml.debug.SimulationManager;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Agent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.debug.impl.SimulationAgentImpl#getSimulationManager <em>Simulation Manager</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.impl.SimulationAgentImpl#getNextEvent <em>Next Event</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class SimulationAgentImpl extends EObjectImpl implements SimulationAgent {
	/**
	 * The cached value of the '{@link #getSimulationManager() <em>Simulation Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimulationManager()
	 * @generated
	 * @ordered
	 */
	protected SimulationManager simulationManager;

	/**
	 * The cached value of the '{@link #getNextEvent() <em>Next Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNextEvent()
	 * @generated
	 * @ordered
	 */
	protected Event nextEvent;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimulationAgentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DebugPackage.Literals.SIMULATION_AGENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationManager getSimulationManager() {
		if (simulationManager != null && simulationManager.eIsProxy()) {
			InternalEObject oldSimulationManager = (InternalEObject)simulationManager;
			simulationManager = (SimulationManager)eResolveProxy(oldSimulationManager);
			if (simulationManager != oldSimulationManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DebugPackage.SIMULATION_AGENT__SIMULATION_MANAGER, oldSimulationManager, simulationManager));
			}
		}
		return simulationManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationManager basicGetSimulationManager() {
		return simulationManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSimulationManager(SimulationManager newSimulationManager) {
		SimulationManager oldSimulationManager = simulationManager;
		simulationManager = newSimulationManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DebugPackage.SIMULATION_AGENT__SIMULATION_MANAGER, oldSimulationManager, simulationManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Event getNextEvent() {
		if (nextEvent != null && nextEvent.eIsProxy()) {
			InternalEObject oldNextEvent = (InternalEObject)nextEvent;
			nextEvent = (Event)eResolveProxy(oldNextEvent);
			if (nextEvent != oldNextEvent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DebugPackage.SIMULATION_AGENT__NEXT_EVENT, oldNextEvent, nextEvent));
			}
		}
		return nextEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Event basicGetNextEvent() {
		return nextEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNextEvent(Event newNextEvent) {
		Event oldNextEvent = nextEvent;
		nextEvent = newNextEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DebugPackage.SIMULATION_AGENT__NEXT_EVENT, oldNextEvent, nextEvent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void init(){
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void terminate() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void stepMade() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DebugPackage.SIMULATION_AGENT__SIMULATION_MANAGER:
				if (resolve) return getSimulationManager();
				return basicGetSimulationManager();
			case DebugPackage.SIMULATION_AGENT__NEXT_EVENT:
				if (resolve) return getNextEvent();
				return basicGetNextEvent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DebugPackage.SIMULATION_AGENT__SIMULATION_MANAGER:
				setSimulationManager((SimulationManager)newValue);
				return;
			case DebugPackage.SIMULATION_AGENT__NEXT_EVENT:
				setNextEvent((Event)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DebugPackage.SIMULATION_AGENT__SIMULATION_MANAGER:
				setSimulationManager((SimulationManager)null);
				return;
			case DebugPackage.SIMULATION_AGENT__NEXT_EVENT:
				setNextEvent((Event)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DebugPackage.SIMULATION_AGENT__SIMULATION_MANAGER:
				return simulationManager != null;
			case DebugPackage.SIMULATION_AGENT__NEXT_EVENT:
				return nextEvent != null;
		}
		return super.eIsSet(featureID);
	}

} //SimulationAgentImpl
