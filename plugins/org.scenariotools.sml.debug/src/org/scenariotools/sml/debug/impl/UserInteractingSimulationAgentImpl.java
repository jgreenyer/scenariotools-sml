/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.debug.DebugPackage;
import org.scenariotools.sml.debug.UserInteractingSimulationAgent;
import org.scenariotools.sml.debug.listener.IEventListChangeListener;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>User Interacting Simulation Agent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.debug.impl.UserInteractingSimulationAgentImpl#getRegisteredEventListChangeListener <em>Registered Event List Change Listener</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UserInteractingSimulationAgentImpl extends SimulationAgentImpl implements UserInteractingSimulationAgent {
	/**
	 * The cached value of the '{@link #getRegisteredEventListChangeListener() <em>Registered Event List Change Listener</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegisteredEventListChangeListener()
	 * @generated
	 * @ordered
	 */
	protected EList<IEventListChangeListener> registeredEventListChangeListener;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UserInteractingSimulationAgentImpl() {
		super();
	}
	
	
	@Override
	public void stepMade() {
		for (IEventListChangeListener eventListChangeListener : getRegisteredEventListChangeListener()) {
			eventListChangeListener.eventsChanged((Collection<Event>)(Collection<?>)getSimulationManager().getCurrentSMLRuntimeState().getEnabledEvents());
		}
	}

	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DebugPackage.Literals.USER_INTERACTING_SIMULATION_AGENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IEventListChangeListener> getRegisteredEventListChangeListener() {
		if (registeredEventListChangeListener == null) {
			registeredEventListChangeListener = new EDataTypeUniqueEList<IEventListChangeListener>(IEventListChangeListener.class, this, DebugPackage.USER_INTERACTING_SIMULATION_AGENT__REGISTERED_EVENT_LIST_CHANGE_LISTENER);
		}
		return registeredEventListChangeListener;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DebugPackage.USER_INTERACTING_SIMULATION_AGENT__REGISTERED_EVENT_LIST_CHANGE_LISTENER:
				return getRegisteredEventListChangeListener();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DebugPackage.USER_INTERACTING_SIMULATION_AGENT__REGISTERED_EVENT_LIST_CHANGE_LISTENER:
				getRegisteredEventListChangeListener().clear();
				getRegisteredEventListChangeListener().addAll((Collection<? extends IEventListChangeListener>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DebugPackage.USER_INTERACTING_SIMULATION_AGENT__REGISTERED_EVENT_LIST_CHANGE_LISTENER:
				getRegisteredEventListChangeListener().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DebugPackage.USER_INTERACTING_SIMULATION_AGENT__REGISTERED_EVENT_LIST_CHANGE_LISTENER:
				return registeredEventListChangeListener != null && !registeredEventListChangeListener.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (registeredEventListChangeListener: ");
		result.append(registeredEventListChangeListener);
		result.append(')');
		return result.toString();
	}

} //UserInteractingSimulationAgentImpl
