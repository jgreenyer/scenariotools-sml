/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.scenariotools.sml.debug.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DebugFactoryImpl extends EFactoryImpl implements DebugFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DebugFactory init() {
		try {
			DebugFactory theDebugFactory = (DebugFactory)EPackage.Registry.INSTANCE.getEFactory(DebugPackage.eNS_URI);
			if (theDebugFactory != null) {
				return theDebugFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DebugFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DebugFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DebugPackage.SIMULATION_MANAGER: return createSimulationManager();
			case DebugPackage.DEFAULT_SYSTEM_SIMULATION_AGENT: return createDefaultSystemSimulationAgent();
			case DebugPackage.DEFAULT_ENVIRONMENT_SIMULATION_AGENT: return createDefaultEnvironmentSimulationAgent();
			case DebugPackage.USER_INTERACTING_SIMULATION_AGENT: return createUserInteractingSimulationAgent();
			case DebugPackage.HISTORY_AGENT: return createHistoryAgent();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationManager createSimulationManager() {
		SimulationManagerImpl simulationManager = new SimulationManagerImpl();
		return simulationManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultSystemSimulationAgent createDefaultSystemSimulationAgent() {
		DefaultSystemSimulationAgentImpl defaultSystemSimulationAgent = new DefaultSystemSimulationAgentImpl();
		return defaultSystemSimulationAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultEnvironmentSimulationAgent createDefaultEnvironmentSimulationAgent() {
		DefaultEnvironmentSimulationAgentImpl defaultEnvironmentSimulationAgent = new DefaultEnvironmentSimulationAgentImpl();
		return defaultEnvironmentSimulationAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserInteractingSimulationAgent createUserInteractingSimulationAgent() {
		UserInteractingSimulationAgentImpl userInteractingSimulationAgent = new UserInteractingSimulationAgentImpl();
		return userInteractingSimulationAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HistoryAgent createHistoryAgent() {
		HistoryAgentImpl historyAgent = new HistoryAgentImpl();
		return historyAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DebugPackage getDebugPackage() {
		return (DebugPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DebugPackage getPackage() {
		return DebugPackage.eINSTANCE;
	}

} //DebugFactoryImpl
