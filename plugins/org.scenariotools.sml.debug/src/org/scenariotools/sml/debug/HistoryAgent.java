/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.sml.debug.listener.ISimulationTraceChangeListener;
import org.scenariotools.sml.runtime.SMLRuntimeState;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>History Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.debug.HistoryAgent#getNextState <em>Next State</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.HistoryAgent#getSimulationManager <em>Simulation Manager</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.HistoryAgent#getRegisteredSimulationTraceChangeListener <em>Registered Simulation Trace Change Listener</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.debug.DebugPackage#getHistoryAgent()
 * @model
 * @generated
 */
public interface HistoryAgent extends EObject {
	/**
	 * Returns the value of the '<em><b>Next State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next State</em>' reference.
	 * @see #setNextState(SMLRuntimeState)
	 * @see org.scenariotools.sml.debug.DebugPackage#getHistoryAgent_NextState()
	 * @model
	 * @generated
	 */
	SMLRuntimeState getNextState();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.debug.HistoryAgent#getNextState <em>Next State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Next State</em>' reference.
	 * @see #getNextState()
	 * @generated
	 */
	void setNextState(SMLRuntimeState value);

	/**
	 * Returns the value of the '<em><b>Simulation Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simulation Manager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simulation Manager</em>' reference.
	 * @see #setSimulationManager(SimulationManager)
	 * @see org.scenariotools.sml.debug.DebugPackage#getHistoryAgent_SimulationManager()
	 * @model
	 * @generated
	 */
	SimulationManager getSimulationManager();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.debug.HistoryAgent#getSimulationManager <em>Simulation Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Simulation Manager</em>' reference.
	 * @see #getSimulationManager()
	 * @generated
	 */
	void setSimulationManager(SimulationManager value);

	/**
	 * Returns the value of the '<em><b>Registered Simulation Trace Change Listener</b></em>' attribute list.
	 * The list contents are of type {@link org.scenariotools.sml.debug.listener.ISimulationTraceChangeListener}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Registered Simulation Trace Change Listener</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registered Simulation Trace Change Listener</em>' attribute list.
	 * @see org.scenariotools.sml.debug.DebugPackage#getHistoryAgent_RegisteredSimulationTraceChangeListener()
	 * @model dataType="org.scenariotools.sml.debug.ISimulationTraceChangeListener" transient="true"
	 * @generated
	 */
	EList<ISimulationTraceChangeListener> getRegisteredSimulationTraceChangeListener();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void init();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void terminate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void stepMade();

} // HistoryAgent
