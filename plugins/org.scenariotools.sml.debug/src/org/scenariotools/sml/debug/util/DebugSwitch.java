/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.scenariotools.sml.debug.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.scenariotools.sml.debug.DebugPackage
 * @generated
 */
public class DebugSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DebugPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DebugSwitch() {
		if (modelPackage == null) {
			modelPackage = DebugPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case DebugPackage.SIMULATION_MANAGER: {
				SimulationManager simulationManager = (SimulationManager)theEObject;
				T result = caseSimulationManager(simulationManager);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DebugPackage.SIMULATION_AGENT: {
				SimulationAgent simulationAgent = (SimulationAgent)theEObject;
				T result = caseSimulationAgent(simulationAgent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DebugPackage.DEFAULT_SYSTEM_SIMULATION_AGENT: {
				DefaultSystemSimulationAgent defaultSystemSimulationAgent = (DefaultSystemSimulationAgent)theEObject;
				T result = caseDefaultSystemSimulationAgent(defaultSystemSimulationAgent);
				if (result == null) result = caseUserInteractingSimulationAgent(defaultSystemSimulationAgent);
				if (result == null) result = caseSimulationAgent(defaultSystemSimulationAgent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DebugPackage.DEFAULT_ENVIRONMENT_SIMULATION_AGENT: {
				DefaultEnvironmentSimulationAgent defaultEnvironmentSimulationAgent = (DefaultEnvironmentSimulationAgent)theEObject;
				T result = caseDefaultEnvironmentSimulationAgent(defaultEnvironmentSimulationAgent);
				if (result == null) result = caseUserInteractingSimulationAgent(defaultEnvironmentSimulationAgent);
				if (result == null) result = caseSimulationAgent(defaultEnvironmentSimulationAgent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DebugPackage.USER_INTERACTING_SIMULATION_AGENT: {
				UserInteractingSimulationAgent userInteractingSimulationAgent = (UserInteractingSimulationAgent)theEObject;
				T result = caseUserInteractingSimulationAgent(userInteractingSimulationAgent);
				if (result == null) result = caseSimulationAgent(userInteractingSimulationAgent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DebugPackage.HISTORY_AGENT: {
				HistoryAgent historyAgent = (HistoryAgent)theEObject;
				T result = caseHistoryAgent(historyAgent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simulation Manager</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simulation Manager</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimulationManager(SimulationManager object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simulation Agent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simulation Agent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimulationAgent(SimulationAgent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Default System Simulation Agent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Default System Simulation Agent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefaultSystemSimulationAgent(DefaultSystemSimulationAgent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Default Environment Simulation Agent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Default Environment Simulation Agent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefaultEnvironmentSimulationAgent(DefaultEnvironmentSimulationAgent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>User Interacting Simulation Agent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>User Interacting Simulation Agent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUserInteractingSimulationAgent(UserInteractingSimulationAgent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>History Agent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>History Agent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHistoryAgent(HistoryAgent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //DebugSwitch
