/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug.util;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.xtext.nodemodel.ICompositeNode;
import org.scenariotools.sml.debug.debug.EnabledFragmentStackFrame;
import org.scenariotools.sml.debug.debug.ScenarioDebugTarget;

public class SMLEnabledFragmentMarker {

	private ScenarioDebugTarget scenarioDebugTarget;
	private Map<EnabledFragmentStackFrame, IMarker> frameToMarkerMap; 

	public SMLEnabledFragmentMarker(ScenarioDebugTarget scenarioDebugTarget) {
		this.scenarioDebugTarget = scenarioDebugTarget;
		this.frameToMarkerMap = new HashMap<EnabledFragmentStackFrame, IMarker>();
		removeAll();
	}

	private IMarker createMarker(ICompositeNode node, IFile sourceFile) {
		IMarker marker = null;
		try {
			marker = sourceFile
						.createMarker("org.scenariotools.sml.debug.fragmentmarker");
			marker.setAttribute(IMarker.CHAR_START, node.getOffset());
			marker.setAttribute(IMarker.CHAR_END, node.getEndOffset());
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return marker;
	}


	public void remove(EnabledFragmentStackFrame enabledFragmentStackFrame) {
		if(frameToMarkerMap.containsKey(enabledFragmentStackFrame)){
			try {
				frameToMarkerMap.get(enabledFragmentStackFrame).delete();
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		frameToMarkerMap.remove(enabledFragmentStackFrame);
	}

	public void removeAll(){
		try {
			for(IFile file : scenarioDebugTarget.getFiles()){
				file.deleteMarkers("org.scenariotools.sml.debug.fragmentmarker", false, 1);
			}
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void update(EnabledFragmentStackFrame enabledFragmentStackFrame, ICompositeNode node) {
		if(!scenarioDebugTarget.isTerminated()){
			if(frameToMarkerMap.containsKey(enabledFragmentStackFrame)){
				IMarker marker = frameToMarkerMap.get(enabledFragmentStackFrame);
				if(marker.getAttribute(IMarker.CHAR_START, -1) == node.getOffset() && marker.getAttribute(IMarker.CHAR_END, -1) == node.getEndOffset()){
					// node is not new
				}else{
					try {
						frameToMarkerMap.put(enabledFragmentStackFrame, createMarker(node, enabledFragmentStackFrame.getSourceFile()));
					} catch (CoreException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}else{
				try {
					frameToMarkerMap.put(enabledFragmentStackFrame, createMarker(node, enabledFragmentStackFrame.getSourceFile()));
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}	
		
	}
}
