/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Default System Simulation Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.debug.DebugPackage#getDefaultSystemSimulationAgent()
 * @model
 * @generated
 */
public interface DefaultSystemSimulationAgent extends UserInteractingSimulationAgent {
} // DefaultSystemSimulationAgent
