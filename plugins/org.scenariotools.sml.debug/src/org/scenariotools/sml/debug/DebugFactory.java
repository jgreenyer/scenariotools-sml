/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.sml.debug.DebugPackage
 * @generated
 */
public interface DebugFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DebugFactory eINSTANCE = org.scenariotools.sml.debug.impl.DebugFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Simulation Manager</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simulation Manager</em>'.
	 * @generated
	 */
	SimulationManager createSimulationManager();

	/**
	 * Returns a new object of class '<em>Default System Simulation Agent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Default System Simulation Agent</em>'.
	 * @generated
	 */
	DefaultSystemSimulationAgent createDefaultSystemSimulationAgent();

	/**
	 * Returns a new object of class '<em>Default Environment Simulation Agent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Default Environment Simulation Agent</em>'.
	 * @generated
	 */
	DefaultEnvironmentSimulationAgent createDefaultEnvironmentSimulationAgent();

	/**
	 * Returns a new object of class '<em>User Interacting Simulation Agent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>User Interacting Simulation Agent</em>'.
	 * @generated
	 */
	UserInteractingSimulationAgent createUserInteractingSimulationAgent();

	/**
	 * Returns a new object of class '<em>History Agent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>History Agent</em>'.
	 * @generated
	 */
	HistoryAgent createHistoryAgent();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DebugPackage getDebugPackage();

} //DebugFactory
