/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.debug;

import org.eclipse.emf.common.util.EList;
import org.scenariotools.sml.debug.listener.IEventListChangeListener;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User Interacting Simulation Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.debug.UserInteractingSimulationAgent#getRegisteredEventListChangeListener <em>Registered Event List Change Listener</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.debug.DebugPackage#getUserInteractingSimulationAgent()
 * @model
 * @generated
 */
public interface UserInteractingSimulationAgent extends SimulationAgent {
	/**
	 * Returns the value of the '<em><b>Registered Event List Change Listener</b></em>' attribute list.
	 * The list contents are of type {@link org.scenariotools.sml.debug.listener.IEventListChangeListener}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Registered Event List Change Listener</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registered Event List Change Listener</em>' attribute list.
	 * @see org.scenariotools.sml.debug.DebugPackage#getUserInteractingSimulationAgent_RegisteredEventListChangeListener()
	 * @model dataType="org.scenariotools.sml.debug.IEventListChangeListener" transient="true"
	 * @generated
	 */
	EList<IEventListChangeListener> getRegisteredEventListChangeListener();

} // UserInteractingSimulationAgent
