/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * ScenarioTools-URL: www.scenariotools.org
 *
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete
 */
package org.scenariotools.emf.objectmodellanguage.eoml.serializer;

import com.google.inject.Inject;
import java.util.Set;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Parameter;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.serializer.ISerializationContext;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;
import org.scenariotools.emf.objectmodellanguage.ObjectmodellanguagePackage;
import org.scenariotools.emf.objectmodellanguage.SOEltAttribute;
import org.scenariotools.emf.objectmodellanguage.SOEltContaiment;
import org.scenariotools.emf.objectmodellanguage.SOEltRef;
import org.scenariotools.emf.objectmodellanguage.SObject;
import org.scenariotools.emf.objectmodellanguage.SValueObject;
import org.scenariotools.emf.objectmodellanguage.SValueObjectRef;
import org.scenariotools.emf.objectmodellanguage.SValuePrimitive;
import org.scenariotools.emf.objectmodellanguage.eoml.services.EOMLGrammarAccess;

@SuppressWarnings("all")
public class EOMLSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private EOMLGrammarAccess grammarAccess;
	
	@Override
	public void sequence(ISerializationContext context, EObject semanticObject) {
		EPackage epackage = semanticObject.eClass().getEPackage();
		ParserRule rule = context.getParserRule();
		Action action = context.getAssignedAction();
		Set<Parameter> parameters = context.getEnabledBooleanParameters();
		if (epackage == ObjectmodellanguagePackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case ObjectmodellanguagePackage.SO_ELT_ATTRIBUTE:
				sequence_SOEltAttribute(context, (SOEltAttribute) semanticObject); 
				return; 
			case ObjectmodellanguagePackage.SO_ELT_CONTAIMENT:
				sequence_SOEltContaiment(context, (SOEltContaiment) semanticObject); 
				return; 
			case ObjectmodellanguagePackage.SO_ELT_REF:
				sequence_SOEltRef(context, (SOEltRef) semanticObject); 
				return; 
			case ObjectmodellanguagePackage.SOBJECT:
				sequence_SObject(context, (SObject) semanticObject); 
				return; 
			case ObjectmodellanguagePackage.SVALUE_OBJECT:
				sequence_SAValueObject(context, (SValueObject) semanticObject); 
				return; 
			case ObjectmodellanguagePackage.SVALUE_OBJECT_REF:
				sequence_SValueObjectRef(context, (SValueObjectRef) semanticObject); 
				return; 
			case ObjectmodellanguagePackage.SVALUE_PRIMITIVE:
				sequence_SAValuePrimitive(context, (SValuePrimitive) semanticObject); 
				return; 
			}
		if (errorAcceptor != null)
			errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Contexts:
	 *     SAValueObject returns SValueObject
	 *
	 * Constraint:
	 *     (name=EString eClass=ID)
	 */
	protected void sequence_SAValueObject(ISerializationContext context, SValueObject semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, ObjectmodellanguagePackage.Literals.SVALUE_OBJECT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ObjectmodellanguagePackage.Literals.SVALUE_OBJECT__NAME));
			if (transientValues.isValueTransient(semanticObject, ObjectmodellanguagePackage.Literals.SVALUE_OBJECT__ECLASS) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ObjectmodellanguagePackage.Literals.SVALUE_OBJECT__ECLASS));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getSAValueObjectAccess().getNameEStringParserRuleCall_1_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getSAValueObjectAccess().getEClassIDTerminalRuleCall_3_0(), semanticObject.getEClass());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     SAValue returns SValuePrimitive
	 *     SAValuePrimitive returns SValuePrimitive
	 *
	 * Constraint:
	 *     value=EString?
	 */
	protected void sequence_SAValuePrimitive(ISerializationContext context, SValuePrimitive semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     SObjectElement returns SOEltAttribute
	 *     SOEltAttribute returns SOEltAttribute
	 *
	 * Constraint:
	 *     (name=ID savalue+=SAValue savalue+=SAValue*)
	 */
	protected void sequence_SOEltAttribute(ISerializationContext context, SOEltAttribute semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     SObjectElement returns SOEltContaiment
	 *     SOEltContaiment returns SOEltContaiment
	 *
	 * Constraint:
	 *     (name=ID sobject+=SObject sobject+=SObject*)
	 */
	protected void sequence_SOEltContaiment(ISerializationContext context, SOEltContaiment semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     SObjectElement returns SOEltRef
	 *     SOEltRef returns SOEltRef
	 *
	 * Constraint:
	 *     (name=ID svalueobject+=SValueObjectRef svalueobject+=SValueObjectRef*)
	 */
	protected void sequence_SOEltRef(ISerializationContext context, SOEltRef semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     SObject returns SObject
	 *
	 * Constraint:
	 *     (name=EString eClass=ID sobjectelement+=SObjectElement* sobjectelement+=SObjectElement*)
	 */
	protected void sequence_SObject(ISerializationContext context, SObject semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     SValueObjectRef returns SValueObjectRef
	 *
	 * Constraint:
	 *     (name=EString eClass=ID)
	 */
	protected void sequence_SValueObjectRef(ISerializationContext context, SValueObjectRef semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, ObjectmodellanguagePackage.Literals.SVALUE_OBJECT_REF__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ObjectmodellanguagePackage.Literals.SVALUE_OBJECT_REF__NAME));
			if (transientValues.isValueTransient(semanticObject, ObjectmodellanguagePackage.Literals.SVALUE_OBJECT_REF__ECLASS) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ObjectmodellanguagePackage.Literals.SVALUE_OBJECT_REF__ECLASS));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getSValueObjectRefAccess().getNameEStringParserRuleCall_1_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getSValueObjectRefAccess().getEClassIDTerminalRuleCall_3_0(), semanticObject.getEClass());
		feeder.finish();
	}
	
	
}
