/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * ScenarioTools-URL: www.scenariotools.org
 *
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete
 */
package org.scenariotools.emf.objectmodellanguage.eoml.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.List;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Alternatives;
import org.eclipse.xtext.Assignment;
import org.eclipse.xtext.Grammar;
import org.eclipse.xtext.GrammarUtil;
import org.eclipse.xtext.Group;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.TerminalRule;
import org.eclipse.xtext.common.services.TerminalsGrammarAccess;
import org.eclipse.xtext.service.AbstractElementFinder.AbstractGrammarElementFinder;
import org.eclipse.xtext.service.GrammarProvider;

@Singleton
public class EOMLGrammarAccess extends AbstractGrammarElementFinder {
	
	public class SObjectElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.emf.objectmodellanguage.eoml.EOML.SObject");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cSObjectAction_0 = (Action)cGroup.eContents().get(0);
		private final Keyword cObjectKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cNameAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cNameEStringParserRuleCall_2_0 = (RuleCall)cNameAssignment_2.eContents().get(0);
		private final Keyword cColonKeyword_3 = (Keyword)cGroup.eContents().get(3);
		private final Assignment cEClassAssignment_4 = (Assignment)cGroup.eContents().get(4);
		private final RuleCall cEClassIDTerminalRuleCall_4_0 = (RuleCall)cEClassAssignment_4.eContents().get(0);
		private final Keyword cLeftCurlyBracketKeyword_5 = (Keyword)cGroup.eContents().get(5);
		private final Group cGroup_6 = (Group)cGroup.eContents().get(6);
		private final Keyword cAttributeKeyword_6_0 = (Keyword)cGroup_6.eContents().get(0);
		private final Assignment cSobjectelementAssignment_6_1 = (Assignment)cGroup_6.eContents().get(1);
		private final RuleCall cSobjectelementSObjectElementParserRuleCall_6_1_0 = (RuleCall)cSobjectelementAssignment_6_1.eContents().get(0);
		private final Group cGroup_7 = (Group)cGroup.eContents().get(7);
		private final Keyword cReferenceKeyword_7_0 = (Keyword)cGroup_7.eContents().get(0);
		private final Assignment cSobjectelementAssignment_7_1 = (Assignment)cGroup_7.eContents().get(1);
		private final RuleCall cSobjectelementSObjectElementParserRuleCall_7_1_0 = (RuleCall)cSobjectelementAssignment_7_1.eContents().get(0);
		private final Keyword cRightCurlyBracketKeyword_8 = (Keyword)cGroup.eContents().get(8);
		
		//SObject:
		//	{SObject}
		//	'object' name=EString ':' eClass=ID '{' ('attribute' sobjectelement+=SObjectElement)* ('reference'
		//	sobjectelement+=SObjectElement)*
		//	'}';
		@Override public ParserRule getRule() { return rule; }
		
		//{SObject} 'object' name=EString ':' eClass=ID '{' ('attribute' sobjectelement+=SObjectElement)* ('reference'
		//sobjectelement+=SObjectElement)* '}'
		public Group getGroup() { return cGroup; }
		
		//{SObject}
		public Action getSObjectAction_0() { return cSObjectAction_0; }
		
		//'object'
		public Keyword getObjectKeyword_1() { return cObjectKeyword_1; }
		
		//name=EString
		public Assignment getNameAssignment_2() { return cNameAssignment_2; }
		
		//EString
		public RuleCall getNameEStringParserRuleCall_2_0() { return cNameEStringParserRuleCall_2_0; }
		
		//':'
		public Keyword getColonKeyword_3() { return cColonKeyword_3; }
		
		//eClass=ID
		public Assignment getEClassAssignment_4() { return cEClassAssignment_4; }
		
		//ID
		public RuleCall getEClassIDTerminalRuleCall_4_0() { return cEClassIDTerminalRuleCall_4_0; }
		
		//'{'
		public Keyword getLeftCurlyBracketKeyword_5() { return cLeftCurlyBracketKeyword_5; }
		
		//('attribute' sobjectelement+=SObjectElement)*
		public Group getGroup_6() { return cGroup_6; }
		
		//'attribute'
		public Keyword getAttributeKeyword_6_0() { return cAttributeKeyword_6_0; }
		
		//sobjectelement+=SObjectElement
		public Assignment getSobjectelementAssignment_6_1() { return cSobjectelementAssignment_6_1; }
		
		//SObjectElement
		public RuleCall getSobjectelementSObjectElementParserRuleCall_6_1_0() { return cSobjectelementSObjectElementParserRuleCall_6_1_0; }
		
		//('reference' sobjectelement+=SObjectElement)*
		public Group getGroup_7() { return cGroup_7; }
		
		//'reference'
		public Keyword getReferenceKeyword_7_0() { return cReferenceKeyword_7_0; }
		
		//sobjectelement+=SObjectElement
		public Assignment getSobjectelementAssignment_7_1() { return cSobjectelementAssignment_7_1; }
		
		//SObjectElement
		public RuleCall getSobjectelementSObjectElementParserRuleCall_7_1_0() { return cSobjectelementSObjectElementParserRuleCall_7_1_0; }
		
		//'}'
		public Keyword getRightCurlyBracketKeyword_8() { return cRightCurlyBracketKeyword_8; }
	}
	public class SObjectElementElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.emf.objectmodellanguage.eoml.EOML.SObjectElement");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cSOEltAttributeParserRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final RuleCall cSOEltRefParserRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		private final RuleCall cSOEltContaimentParserRuleCall_2 = (RuleCall)cAlternatives.eContents().get(2);
		
		//SObjectElement:
		//	SOEltAttribute | SOEltRef | SOEltContaiment;
		@Override public ParserRule getRule() { return rule; }
		
		//SOEltAttribute | SOEltRef | SOEltContaiment
		public Alternatives getAlternatives() { return cAlternatives; }
		
		//SOEltAttribute
		public RuleCall getSOEltAttributeParserRuleCall_0() { return cSOEltAttributeParserRuleCall_0; }
		
		//SOEltRef
		public RuleCall getSOEltRefParserRuleCall_1() { return cSOEltRefParserRuleCall_1; }
		
		//SOEltContaiment
		public RuleCall getSOEltContaimentParserRuleCall_2() { return cSOEltContaimentParserRuleCall_2; }
	}
	public class SOEltAttributeElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.emf.objectmodellanguage.eoml.EOML.SOEltAttribute");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cSOEltAttributeAction_0 = (Action)cGroup.eContents().get(0);
		private final Assignment cNameAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cNameIDTerminalRuleCall_1_0 = (RuleCall)cNameAssignment_1.eContents().get(0);
		private final Keyword cColonKeyword_2 = (Keyword)cGroup.eContents().get(2);
		private final Group cGroup_3 = (Group)cGroup.eContents().get(3);
		private final Keyword cLeftSquareBracketKeyword_3_0 = (Keyword)cGroup_3.eContents().get(0);
		private final Assignment cSavalueAssignment_3_1 = (Assignment)cGroup_3.eContents().get(1);
		private final RuleCall cSavalueSAValueParserRuleCall_3_1_0 = (RuleCall)cSavalueAssignment_3_1.eContents().get(0);
		private final Group cGroup_3_2 = (Group)cGroup_3.eContents().get(2);
		private final Keyword cCommaKeyword_3_2_0 = (Keyword)cGroup_3_2.eContents().get(0);
		private final Assignment cSavalueAssignment_3_2_1 = (Assignment)cGroup_3_2.eContents().get(1);
		private final RuleCall cSavalueSAValueParserRuleCall_3_2_1_0 = (RuleCall)cSavalueAssignment_3_2_1.eContents().get(0);
		private final Keyword cRightSquareBracketKeyword_3_3 = (Keyword)cGroup_3.eContents().get(3);
		
		//SOEltAttribute:
		//	{SOEltAttribute} name=ID ':' ("[" savalue+=SAValue ("," savalue+=SAValue)* "]");
		@Override public ParserRule getRule() { return rule; }
		
		//{SOEltAttribute} name=ID ':' ("[" savalue+=SAValue ("," savalue+=SAValue)* "]")
		public Group getGroup() { return cGroup; }
		
		//{SOEltAttribute}
		public Action getSOEltAttributeAction_0() { return cSOEltAttributeAction_0; }
		
		//name=ID
		public Assignment getNameAssignment_1() { return cNameAssignment_1; }
		
		//ID
		public RuleCall getNameIDTerminalRuleCall_1_0() { return cNameIDTerminalRuleCall_1_0; }
		
		//':'
		public Keyword getColonKeyword_2() { return cColonKeyword_2; }
		
		//("[" savalue+=SAValue ("," savalue+=SAValue)* "]")
		public Group getGroup_3() { return cGroup_3; }
		
		//"["
		public Keyword getLeftSquareBracketKeyword_3_0() { return cLeftSquareBracketKeyword_3_0; }
		
		//savalue+=SAValue
		public Assignment getSavalueAssignment_3_1() { return cSavalueAssignment_3_1; }
		
		//SAValue
		public RuleCall getSavalueSAValueParserRuleCall_3_1_0() { return cSavalueSAValueParserRuleCall_3_1_0; }
		
		//("," savalue+=SAValue)*
		public Group getGroup_3_2() { return cGroup_3_2; }
		
		//","
		public Keyword getCommaKeyword_3_2_0() { return cCommaKeyword_3_2_0; }
		
		//savalue+=SAValue
		public Assignment getSavalueAssignment_3_2_1() { return cSavalueAssignment_3_2_1; }
		
		//SAValue
		public RuleCall getSavalueSAValueParserRuleCall_3_2_1_0() { return cSavalueSAValueParserRuleCall_3_2_1_0; }
		
		//"]"
		public Keyword getRightSquareBracketKeyword_3_3() { return cRightSquareBracketKeyword_3_3; }
	}
	public class SOEltRefElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.emf.objectmodellanguage.eoml.EOML.SOEltRef");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cSOEltRefAction_0 = (Action)cGroup.eContents().get(0);
		private final Assignment cNameAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cNameIDTerminalRuleCall_1_0 = (RuleCall)cNameAssignment_1.eContents().get(0);
		private final Keyword cColonKeyword_2 = (Keyword)cGroup.eContents().get(2);
		private final Group cGroup_3 = (Group)cGroup.eContents().get(3);
		private final Keyword cLeftSquareBracketKeyword_3_0 = (Keyword)cGroup_3.eContents().get(0);
		private final Assignment cSvalueobjectAssignment_3_1 = (Assignment)cGroup_3.eContents().get(1);
		private final RuleCall cSvalueobjectSValueObjectRefParserRuleCall_3_1_0 = (RuleCall)cSvalueobjectAssignment_3_1.eContents().get(0);
		private final Group cGroup_3_2 = (Group)cGroup_3.eContents().get(2);
		private final Keyword cCommaKeyword_3_2_0 = (Keyword)cGroup_3_2.eContents().get(0);
		private final Assignment cSvalueobjectAssignment_3_2_1 = (Assignment)cGroup_3_2.eContents().get(1);
		private final RuleCall cSvalueobjectSValueObjectRefParserRuleCall_3_2_1_0 = (RuleCall)cSvalueobjectAssignment_3_2_1.eContents().get(0);
		private final Keyword cRightSquareBracketKeyword_3_3 = (Keyword)cGroup_3.eContents().get(3);
		
		//SOEltRef:
		//	{SOEltRef} name=ID ':' ("[" svalueobject+=SValueObjectRef ("," svalueobject+=SValueObjectRef)* "]");
		@Override public ParserRule getRule() { return rule; }
		
		//{SOEltRef} name=ID ':' ("[" svalueobject+=SValueObjectRef ("," svalueobject+=SValueObjectRef)* "]")
		public Group getGroup() { return cGroup; }
		
		//{SOEltRef}
		public Action getSOEltRefAction_0() { return cSOEltRefAction_0; }
		
		//name=ID
		public Assignment getNameAssignment_1() { return cNameAssignment_1; }
		
		//ID
		public RuleCall getNameIDTerminalRuleCall_1_0() { return cNameIDTerminalRuleCall_1_0; }
		
		//':'
		public Keyword getColonKeyword_2() { return cColonKeyword_2; }
		
		//("[" svalueobject+=SValueObjectRef ("," svalueobject+=SValueObjectRef)* "]")
		public Group getGroup_3() { return cGroup_3; }
		
		//"["
		public Keyword getLeftSquareBracketKeyword_3_0() { return cLeftSquareBracketKeyword_3_0; }
		
		//svalueobject+=SValueObjectRef
		public Assignment getSvalueobjectAssignment_3_1() { return cSvalueobjectAssignment_3_1; }
		
		//SValueObjectRef
		public RuleCall getSvalueobjectSValueObjectRefParserRuleCall_3_1_0() { return cSvalueobjectSValueObjectRefParserRuleCall_3_1_0; }
		
		//("," svalueobject+=SValueObjectRef)*
		public Group getGroup_3_2() { return cGroup_3_2; }
		
		//","
		public Keyword getCommaKeyword_3_2_0() { return cCommaKeyword_3_2_0; }
		
		//svalueobject+=SValueObjectRef
		public Assignment getSvalueobjectAssignment_3_2_1() { return cSvalueobjectAssignment_3_2_1; }
		
		//SValueObjectRef
		public RuleCall getSvalueobjectSValueObjectRefParserRuleCall_3_2_1_0() { return cSvalueobjectSValueObjectRefParserRuleCall_3_2_1_0; }
		
		//"]"
		public Keyword getRightSquareBracketKeyword_3_3() { return cRightSquareBracketKeyword_3_3; }
	}
	public class SOEltContaimentElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.emf.objectmodellanguage.eoml.EOML.SOEltContaiment");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cSOEltContaimentAction_0 = (Action)cGroup.eContents().get(0);
		private final Assignment cNameAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cNameIDTerminalRuleCall_1_0 = (RuleCall)cNameAssignment_1.eContents().get(0);
		private final Keyword cColonKeyword_2 = (Keyword)cGroup.eContents().get(2);
		private final Group cGroup_3 = (Group)cGroup.eContents().get(3);
		private final Keyword cLeftSquareBracketKeyword_3_0 = (Keyword)cGroup_3.eContents().get(0);
		private final Assignment cSobjectAssignment_3_1 = (Assignment)cGroup_3.eContents().get(1);
		private final RuleCall cSobjectSObjectParserRuleCall_3_1_0 = (RuleCall)cSobjectAssignment_3_1.eContents().get(0);
		private final Group cGroup_3_2 = (Group)cGroup_3.eContents().get(2);
		private final Keyword cCommaKeyword_3_2_0 = (Keyword)cGroup_3_2.eContents().get(0);
		private final Assignment cSobjectAssignment_3_2_1 = (Assignment)cGroup_3_2.eContents().get(1);
		private final RuleCall cSobjectSObjectParserRuleCall_3_2_1_0 = (RuleCall)cSobjectAssignment_3_2_1.eContents().get(0);
		private final Keyword cRightSquareBracketKeyword_3_3 = (Keyword)cGroup_3.eContents().get(3);
		
		//SOEltContaiment:
		//	{SOEltContaiment} name=ID ':' ("[" sobject+=SObject ("," sobject+=SObject)* "]");
		@Override public ParserRule getRule() { return rule; }
		
		//{SOEltContaiment} name=ID ':' ("[" sobject+=SObject ("," sobject+=SObject)* "]")
		public Group getGroup() { return cGroup; }
		
		//{SOEltContaiment}
		public Action getSOEltContaimentAction_0() { return cSOEltContaimentAction_0; }
		
		//name=ID
		public Assignment getNameAssignment_1() { return cNameAssignment_1; }
		
		//ID
		public RuleCall getNameIDTerminalRuleCall_1_0() { return cNameIDTerminalRuleCall_1_0; }
		
		//':'
		public Keyword getColonKeyword_2() { return cColonKeyword_2; }
		
		//("[" sobject+=SObject ("," sobject+=SObject)* "]")
		public Group getGroup_3() { return cGroup_3; }
		
		//"["
		public Keyword getLeftSquareBracketKeyword_3_0() { return cLeftSquareBracketKeyword_3_0; }
		
		//sobject+=SObject
		public Assignment getSobjectAssignment_3_1() { return cSobjectAssignment_3_1; }
		
		//SObject
		public RuleCall getSobjectSObjectParserRuleCall_3_1_0() { return cSobjectSObjectParserRuleCall_3_1_0; }
		
		//("," sobject+=SObject)*
		public Group getGroup_3_2() { return cGroup_3_2; }
		
		//","
		public Keyword getCommaKeyword_3_2_0() { return cCommaKeyword_3_2_0; }
		
		//sobject+=SObject
		public Assignment getSobjectAssignment_3_2_1() { return cSobjectAssignment_3_2_1; }
		
		//SObject
		public RuleCall getSobjectSObjectParserRuleCall_3_2_1_0() { return cSobjectSObjectParserRuleCall_3_2_1_0; }
		
		//"]"
		public Keyword getRightSquareBracketKeyword_3_3() { return cRightSquareBracketKeyword_3_3; }
	}
	public class SAValueElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.emf.objectmodellanguage.eoml.EOML.SAValue");
		private final RuleCall cSAValuePrimitiveParserRuleCall = (RuleCall)rule.eContents().get(1);
		
		/// *SAValue returns SAValue:
		//	SAValueObject | SAValuePrimitive;* / SAValue:
		//	SAValuePrimitive;
		@Override public ParserRule getRule() { return rule; }
		
		//SAValuePrimitive
		public RuleCall getSAValuePrimitiveParserRuleCall() { return cSAValuePrimitiveParserRuleCall; }
	}
	public class SAValuePrimitiveElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.emf.objectmodellanguage.eoml.EOML.SAValuePrimitive");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cSValuePrimitiveAction_0 = (Action)cGroup.eContents().get(0);
		private final Assignment cValueAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cValueEStringParserRuleCall_1_0 = (RuleCall)cValueAssignment_1.eContents().get(0);
		
		//SAValuePrimitive SValuePrimitive:
		//	{SValuePrimitive} value=EString?
		@Override public ParserRule getRule() { return rule; }
		
		//{SValuePrimitive} value=EString?
		public Group getGroup() { return cGroup; }
		
		//{SValuePrimitive}
		public Action getSValuePrimitiveAction_0() { return cSValuePrimitiveAction_0; }
		
		//value=EString?
		public Assignment getValueAssignment_1() { return cValueAssignment_1; }
		
		//EString
		public RuleCall getValueEStringParserRuleCall_1_0() { return cValueEStringParserRuleCall_1_0; }
	}
	public class SAValueObjectElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.emf.objectmodellanguage.eoml.EOML.SAValueObject");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cSValueObjectAction_0 = (Action)cGroup.eContents().get(0);
		private final Assignment cNameAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cNameEStringParserRuleCall_1_0 = (RuleCall)cNameAssignment_1.eContents().get(0);
		private final Keyword cColonKeyword_2 = (Keyword)cGroup.eContents().get(2);
		private final Assignment cEClassAssignment_3 = (Assignment)cGroup.eContents().get(3);
		private final RuleCall cEClassIDTerminalRuleCall_3_0 = (RuleCall)cEClassAssignment_3.eContents().get(0);
		
		//SAValueObject SValueObject:
		//	{SValueObject} name=EString ':' eClass=ID
		@Override public ParserRule getRule() { return rule; }
		
		//{SValueObject} name=EString ':' eClass=ID
		public Group getGroup() { return cGroup; }
		
		//{SValueObject}
		public Action getSValueObjectAction_0() { return cSValueObjectAction_0; }
		
		//name=EString
		public Assignment getNameAssignment_1() { return cNameAssignment_1; }
		
		//EString
		public RuleCall getNameEStringParserRuleCall_1_0() { return cNameEStringParserRuleCall_1_0; }
		
		//':'
		public Keyword getColonKeyword_2() { return cColonKeyword_2; }
		
		//eClass=ID
		public Assignment getEClassAssignment_3() { return cEClassAssignment_3; }
		
		//ID
		public RuleCall getEClassIDTerminalRuleCall_3_0() { return cEClassIDTerminalRuleCall_3_0; }
	}
	public class SValueObjectRefElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.emf.objectmodellanguage.eoml.EOML.SValueObjectRef");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cSValueObjectRefAction_0 = (Action)cGroup.eContents().get(0);
		private final Assignment cNameAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cNameEStringParserRuleCall_1_0 = (RuleCall)cNameAssignment_1.eContents().get(0);
		private final Keyword cColonKeyword_2 = (Keyword)cGroup.eContents().get(2);
		private final Assignment cEClassAssignment_3 = (Assignment)cGroup.eContents().get(3);
		private final RuleCall cEClassIDTerminalRuleCall_3_0 = (RuleCall)cEClassAssignment_3.eContents().get(0);
		
		//SValueObjectRef:
		//	{SValueObjectRef} name=EString ':' eClass=ID;
		@Override public ParserRule getRule() { return rule; }
		
		//{SValueObjectRef} name=EString ':' eClass=ID
		public Group getGroup() { return cGroup; }
		
		//{SValueObjectRef}
		public Action getSValueObjectRefAction_0() { return cSValueObjectRefAction_0; }
		
		//name=EString
		public Assignment getNameAssignment_1() { return cNameAssignment_1; }
		
		//EString
		public RuleCall getNameEStringParserRuleCall_1_0() { return cNameEStringParserRuleCall_1_0; }
		
		//':'
		public Keyword getColonKeyword_2() { return cColonKeyword_2; }
		
		//eClass=ID
		public Assignment getEClassAssignment_3() { return cEClassAssignment_3; }
		
		//ID
		public RuleCall getEClassIDTerminalRuleCall_3_0() { return cEClassIDTerminalRuleCall_3_0; }
	}
	public class EStringElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.emf.objectmodellanguage.eoml.EOML.EString");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cSTRINGTerminalRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final RuleCall cIDTerminalRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		private final Group cGroup_2 = (Group)cAlternatives.eContents().get(2);
		private final Keyword cHyphenMinusKeyword_2_0 = (Keyword)cGroup_2.eContents().get(0);
		private final RuleCall cINTTerminalRuleCall_2_1 = (RuleCall)cGroup_2.eContents().get(1);
		
		//EString:
		//	STRING | ID | '-'? INT;
		@Override public ParserRule getRule() { return rule; }
		
		//STRING | ID | '-'? INT
		public Alternatives getAlternatives() { return cAlternatives; }
		
		//STRING
		public RuleCall getSTRINGTerminalRuleCall_0() { return cSTRINGTerminalRuleCall_0; }
		
		//ID
		public RuleCall getIDTerminalRuleCall_1() { return cIDTerminalRuleCall_1; }
		
		//'-'? INT
		public Group getGroup_2() { return cGroup_2; }
		
		//'-'?
		public Keyword getHyphenMinusKeyword_2_0() { return cHyphenMinusKeyword_2_0; }
		
		//INT
		public RuleCall getINTTerminalRuleCall_2_1() { return cINTTerminalRuleCall_2_1; }
	}
	
	
	private final SObjectElements pSObject;
	private final SObjectElementElements pSObjectElement;
	private final SOEltAttributeElements pSOEltAttribute;
	private final SOEltRefElements pSOEltRef;
	private final SOEltContaimentElements pSOEltContaiment;
	private final SAValueElements pSAValue;
	private final SAValuePrimitiveElements pSAValuePrimitive;
	private final SAValueObjectElements pSAValueObject;
	private final SValueObjectRefElements pSValueObjectRef;
	private final EStringElements pEString;
	
	private final Grammar grammar;
	
	private final TerminalsGrammarAccess gaTerminals;

	@Inject
	public EOMLGrammarAccess(GrammarProvider grammarProvider,
			TerminalsGrammarAccess gaTerminals) {
		this.grammar = internalFindGrammar(grammarProvider);
		this.gaTerminals = gaTerminals;
		this.pSObject = new SObjectElements();
		this.pSObjectElement = new SObjectElementElements();
		this.pSOEltAttribute = new SOEltAttributeElements();
		this.pSOEltRef = new SOEltRefElements();
		this.pSOEltContaiment = new SOEltContaimentElements();
		this.pSAValue = new SAValueElements();
		this.pSAValuePrimitive = new SAValuePrimitiveElements();
		this.pSAValueObject = new SAValueObjectElements();
		this.pSValueObjectRef = new SValueObjectRefElements();
		this.pEString = new EStringElements();
	}
	
	protected Grammar internalFindGrammar(GrammarProvider grammarProvider) {
		Grammar grammar = grammarProvider.getGrammar(this);
		while (grammar != null) {
			if ("org.scenariotools.emf.objectmodellanguage.eoml.EOML".equals(grammar.getName())) {
				return grammar;
			}
			List<Grammar> grammars = grammar.getUsedGrammars();
			if (!grammars.isEmpty()) {
				grammar = grammars.iterator().next();
			} else {
				return null;
			}
		}
		return grammar;
	}
	
	@Override
	public Grammar getGrammar() {
		return grammar;
	}
	
	
	public TerminalsGrammarAccess getTerminalsGrammarAccess() {
		return gaTerminals;
	}

	
	//SObject:
	//	{SObject}
	//	'object' name=EString ':' eClass=ID '{' ('attribute' sobjectelement+=SObjectElement)* ('reference'
	//	sobjectelement+=SObjectElement)*
	//	'}';
	public SObjectElements getSObjectAccess() {
		return pSObject;
	}
	
	public ParserRule getSObjectRule() {
		return getSObjectAccess().getRule();
	}
	
	//SObjectElement:
	//	SOEltAttribute | SOEltRef | SOEltContaiment;
	public SObjectElementElements getSObjectElementAccess() {
		return pSObjectElement;
	}
	
	public ParserRule getSObjectElementRule() {
		return getSObjectElementAccess().getRule();
	}
	
	//SOEltAttribute:
	//	{SOEltAttribute} name=ID ':' ("[" savalue+=SAValue ("," savalue+=SAValue)* "]");
	public SOEltAttributeElements getSOEltAttributeAccess() {
		return pSOEltAttribute;
	}
	
	public ParserRule getSOEltAttributeRule() {
		return getSOEltAttributeAccess().getRule();
	}
	
	//SOEltRef:
	//	{SOEltRef} name=ID ':' ("[" svalueobject+=SValueObjectRef ("," svalueobject+=SValueObjectRef)* "]");
	public SOEltRefElements getSOEltRefAccess() {
		return pSOEltRef;
	}
	
	public ParserRule getSOEltRefRule() {
		return getSOEltRefAccess().getRule();
	}
	
	//SOEltContaiment:
	//	{SOEltContaiment} name=ID ':' ("[" sobject+=SObject ("," sobject+=SObject)* "]");
	public SOEltContaimentElements getSOEltContaimentAccess() {
		return pSOEltContaiment;
	}
	
	public ParserRule getSOEltContaimentRule() {
		return getSOEltContaimentAccess().getRule();
	}
	
	/// *SAValue returns SAValue:
	//	SAValueObject | SAValuePrimitive;* / SAValue:
	//	SAValuePrimitive;
	public SAValueElements getSAValueAccess() {
		return pSAValue;
	}
	
	public ParserRule getSAValueRule() {
		return getSAValueAccess().getRule();
	}
	
	//SAValuePrimitive SValuePrimitive:
	//	{SValuePrimitive} value=EString?
	public SAValuePrimitiveElements getSAValuePrimitiveAccess() {
		return pSAValuePrimitive;
	}
	
	public ParserRule getSAValuePrimitiveRule() {
		return getSAValuePrimitiveAccess().getRule();
	}
	
	//SAValueObject SValueObject:
	//	{SValueObject} name=EString ':' eClass=ID
	public SAValueObjectElements getSAValueObjectAccess() {
		return pSAValueObject;
	}
	
	public ParserRule getSAValueObjectRule() {
		return getSAValueObjectAccess().getRule();
	}
	
	//SValueObjectRef:
	//	{SValueObjectRef} name=EString ':' eClass=ID;
	public SValueObjectRefElements getSValueObjectRefAccess() {
		return pSValueObjectRef;
	}
	
	public ParserRule getSValueObjectRefRule() {
		return getSValueObjectRefAccess().getRule();
	}
	
	//EString:
	//	STRING | ID | '-'? INT;
	public EStringElements getEStringAccess() {
		return pEString;
	}
	
	public ParserRule getEStringRule() {
		return getEStringAccess().getRule();
	}
	
	//terminal ID:
	//	'^'? ('a'..'z' | 'A'..'Z' | '_') ('a'..'z' | 'A'..'Z' | '_' | '0'..'9')*;
	public TerminalRule getIDRule() {
		return gaTerminals.getIDRule();
	}
	
	//terminal INT returns ecore::EInt:
	//	'0'..'9'+;
	public TerminalRule getINTRule() {
		return gaTerminals.getINTRule();
	}
	
	//terminal STRING:
	//	'"' ('\\' . | !('\\' | '"'))* '"' | "'" ('\\' . | !('\\' | "'"))* "'";
	public TerminalRule getSTRINGRule() {
		return gaTerminals.getSTRINGRule();
	}
	
	//terminal ML_COMMENT:
	//	'/ *'->'* /';
	public TerminalRule getML_COMMENTRule() {
		return gaTerminals.getML_COMMENTRule();
	}
	
	//terminal SL_COMMENT:
	//	'//' !('\n' | '\r')* ('\r'? '\n')?;
	public TerminalRule getSL_COMMENTRule() {
		return gaTerminals.getSL_COMMENTRule();
	}
	
	//terminal WS:
	//	' ' | '\t' | '\r' | '\n'+;
	public TerminalRule getWSRule() {
		return gaTerminals.getWSRule();
	}
	
	//terminal ANY_OTHER:
	//	.;
	public TerminalRule getANY_OTHERRule() {
		return gaTerminals.getANY_OTHERRule();
	}
}
