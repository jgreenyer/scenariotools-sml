package org.scenariotools.emf.objectmodellanguage.eoml.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.scenariotools.emf.objectmodellanguage.eoml.services.EOMLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * ScenarioTools-URL: www.scenariotools.org
 *
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete
 */
@SuppressWarnings("all")
public class InternalEOMLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'object'", "':'", "'{'", "'attribute'", "'reference'", "'}'", "'['", "','", "']'", "'-'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;

    // delegates
    // delegators


        public InternalEOMLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalEOMLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalEOMLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalEOML.g"; }



     	private EOMLGrammarAccess grammarAccess;

        public InternalEOMLParser(TokenStream input, EOMLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "SObject";
       	}

       	@Override
       	protected EOMLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleSObject"
    // InternalEOML.g:74:1: entryRuleSObject returns [EObject current=null] : iv_ruleSObject= ruleSObject EOF ;
    public final EObject entryRuleSObject() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSObject = null;


        try {
            // InternalEOML.g:74:48: (iv_ruleSObject= ruleSObject EOF )
            // InternalEOML.g:75:2: iv_ruleSObject= ruleSObject EOF
            {
             newCompositeNode(grammarAccess.getSObjectRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSObject=ruleSObject();

            state._fsp--;

             current =iv_ruleSObject; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSObject"


    // $ANTLR start "ruleSObject"
    // InternalEOML.g:81:1: ruleSObject returns [EObject current=null] : ( () otherlv_1= 'object' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( (lv_eClass_4_0= RULE_ID ) ) otherlv_5= '{' (otherlv_6= 'attribute' ( (lv_sobjectelement_7_0= ruleSObjectElement ) ) )* (otherlv_8= 'reference' ( (lv_sobjectelement_9_0= ruleSObjectElement ) ) )* otherlv_10= '}' ) ;
    public final EObject ruleSObject() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_eClass_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_sobjectelement_7_0 = null;

        EObject lv_sobjectelement_9_0 = null;



        	enterRule();

        try {
            // InternalEOML.g:87:2: ( ( () otherlv_1= 'object' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( (lv_eClass_4_0= RULE_ID ) ) otherlv_5= '{' (otherlv_6= 'attribute' ( (lv_sobjectelement_7_0= ruleSObjectElement ) ) )* (otherlv_8= 'reference' ( (lv_sobjectelement_9_0= ruleSObjectElement ) ) )* otherlv_10= '}' ) )
            // InternalEOML.g:88:2: ( () otherlv_1= 'object' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( (lv_eClass_4_0= RULE_ID ) ) otherlv_5= '{' (otherlv_6= 'attribute' ( (lv_sobjectelement_7_0= ruleSObjectElement ) ) )* (otherlv_8= 'reference' ( (lv_sobjectelement_9_0= ruleSObjectElement ) ) )* otherlv_10= '}' )
            {
            // InternalEOML.g:88:2: ( () otherlv_1= 'object' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( (lv_eClass_4_0= RULE_ID ) ) otherlv_5= '{' (otherlv_6= 'attribute' ( (lv_sobjectelement_7_0= ruleSObjectElement ) ) )* (otherlv_8= 'reference' ( (lv_sobjectelement_9_0= ruleSObjectElement ) ) )* otherlv_10= '}' )
            // InternalEOML.g:89:3: () otherlv_1= 'object' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( (lv_eClass_4_0= RULE_ID ) ) otherlv_5= '{' (otherlv_6= 'attribute' ( (lv_sobjectelement_7_0= ruleSObjectElement ) ) )* (otherlv_8= 'reference' ( (lv_sobjectelement_9_0= ruleSObjectElement ) ) )* otherlv_10= '}'
            {
            // InternalEOML.g:89:3: ()
            // InternalEOML.g:90:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getSObjectAccess().getSObjectAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getSObjectAccess().getObjectKeyword_1());
            		
            // InternalEOML.g:100:3: ( (lv_name_2_0= ruleEString ) )
            // InternalEOML.g:101:4: (lv_name_2_0= ruleEString )
            {
            // InternalEOML.g:101:4: (lv_name_2_0= ruleEString )
            // InternalEOML.g:102:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getSObjectAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSObjectRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.scenariotools.emf.objectmodellanguage.eoml.EOML.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_3, grammarAccess.getSObjectAccess().getColonKeyword_3());
            		
            // InternalEOML.g:123:3: ( (lv_eClass_4_0= RULE_ID ) )
            // InternalEOML.g:124:4: (lv_eClass_4_0= RULE_ID )
            {
            // InternalEOML.g:124:4: (lv_eClass_4_0= RULE_ID )
            // InternalEOML.g:125:5: lv_eClass_4_0= RULE_ID
            {
            lv_eClass_4_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_eClass_4_0, grammarAccess.getSObjectAccess().getEClassIDTerminalRuleCall_4_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSObjectRule());
            					}
            					setWithLastConsumed(
            						current,
            						"eClass",
            						lv_eClass_4_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_5=(Token)match(input,13,FOLLOW_7); 

            			newLeafNode(otherlv_5, grammarAccess.getSObjectAccess().getLeftCurlyBracketKeyword_5());
            		
            // InternalEOML.g:145:3: (otherlv_6= 'attribute' ( (lv_sobjectelement_7_0= ruleSObjectElement ) ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==14) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalEOML.g:146:4: otherlv_6= 'attribute' ( (lv_sobjectelement_7_0= ruleSObjectElement ) )
            	    {
            	    otherlv_6=(Token)match(input,14,FOLLOW_5); 

            	    				newLeafNode(otherlv_6, grammarAccess.getSObjectAccess().getAttributeKeyword_6_0());
            	    			
            	    // InternalEOML.g:150:4: ( (lv_sobjectelement_7_0= ruleSObjectElement ) )
            	    // InternalEOML.g:151:5: (lv_sobjectelement_7_0= ruleSObjectElement )
            	    {
            	    // InternalEOML.g:151:5: (lv_sobjectelement_7_0= ruleSObjectElement )
            	    // InternalEOML.g:152:6: lv_sobjectelement_7_0= ruleSObjectElement
            	    {

            	    						newCompositeNode(grammarAccess.getSObjectAccess().getSobjectelementSObjectElementParserRuleCall_6_1_0());
            	    					
            	    pushFollow(FOLLOW_7);
            	    lv_sobjectelement_7_0=ruleSObjectElement();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getSObjectRule());
            	    						}
            	    						add(
            	    							current,
            	    							"sobjectelement",
            	    							lv_sobjectelement_7_0,
            	    							"org.scenariotools.emf.objectmodellanguage.eoml.EOML.SObjectElement");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalEOML.g:170:3: (otherlv_8= 'reference' ( (lv_sobjectelement_9_0= ruleSObjectElement ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==15) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalEOML.g:171:4: otherlv_8= 'reference' ( (lv_sobjectelement_9_0= ruleSObjectElement ) )
            	    {
            	    otherlv_8=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_8, grammarAccess.getSObjectAccess().getReferenceKeyword_7_0());
            	    			
            	    // InternalEOML.g:175:4: ( (lv_sobjectelement_9_0= ruleSObjectElement ) )
            	    // InternalEOML.g:176:5: (lv_sobjectelement_9_0= ruleSObjectElement )
            	    {
            	    // InternalEOML.g:176:5: (lv_sobjectelement_9_0= ruleSObjectElement )
            	    // InternalEOML.g:177:6: lv_sobjectelement_9_0= ruleSObjectElement
            	    {

            	    						newCompositeNode(grammarAccess.getSObjectAccess().getSobjectelementSObjectElementParserRuleCall_7_1_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_sobjectelement_9_0=ruleSObjectElement();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getSObjectRule());
            	    						}
            	    						add(
            	    							current,
            	    							"sobjectelement",
            	    							lv_sobjectelement_9_0,
            	    							"org.scenariotools.emf.objectmodellanguage.eoml.EOML.SObjectElement");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            otherlv_10=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getSObjectAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSObject"


    // $ANTLR start "entryRuleSObjectElement"
    // InternalEOML.g:203:1: entryRuleSObjectElement returns [EObject current=null] : iv_ruleSObjectElement= ruleSObjectElement EOF ;
    public final EObject entryRuleSObjectElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSObjectElement = null;


        try {
            // InternalEOML.g:203:55: (iv_ruleSObjectElement= ruleSObjectElement EOF )
            // InternalEOML.g:204:2: iv_ruleSObjectElement= ruleSObjectElement EOF
            {
             newCompositeNode(grammarAccess.getSObjectElementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSObjectElement=ruleSObjectElement();

            state._fsp--;

             current =iv_ruleSObjectElement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSObjectElement"


    // $ANTLR start "ruleSObjectElement"
    // InternalEOML.g:210:1: ruleSObjectElement returns [EObject current=null] : (this_SOEltAttribute_0= ruleSOEltAttribute | this_SOEltRef_1= ruleSOEltRef | this_SOEltContaiment_2= ruleSOEltContaiment ) ;
    public final EObject ruleSObjectElement() throws RecognitionException {
        EObject current = null;

        EObject this_SOEltAttribute_0 = null;

        EObject this_SOEltRef_1 = null;

        EObject this_SOEltContaiment_2 = null;



        	enterRule();

        try {
            // InternalEOML.g:216:2: ( (this_SOEltAttribute_0= ruleSOEltAttribute | this_SOEltRef_1= ruleSOEltRef | this_SOEltContaiment_2= ruleSOEltContaiment ) )
            // InternalEOML.g:217:2: (this_SOEltAttribute_0= ruleSOEltAttribute | this_SOEltRef_1= ruleSOEltRef | this_SOEltContaiment_2= ruleSOEltContaiment )
            {
            // InternalEOML.g:217:2: (this_SOEltAttribute_0= ruleSOEltAttribute | this_SOEltRef_1= ruleSOEltRef | this_SOEltContaiment_2= ruleSOEltContaiment )
            int alt3=3;
            alt3 = dfa3.predict(input);
            switch (alt3) {
                case 1 :
                    // InternalEOML.g:218:3: this_SOEltAttribute_0= ruleSOEltAttribute
                    {

                    			newCompositeNode(grammarAccess.getSObjectElementAccess().getSOEltAttributeParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_SOEltAttribute_0=ruleSOEltAttribute();

                    state._fsp--;


                    			current = this_SOEltAttribute_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalEOML.g:227:3: this_SOEltRef_1= ruleSOEltRef
                    {

                    			newCompositeNode(grammarAccess.getSObjectElementAccess().getSOEltRefParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_SOEltRef_1=ruleSOEltRef();

                    state._fsp--;


                    			current = this_SOEltRef_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalEOML.g:236:3: this_SOEltContaiment_2= ruleSOEltContaiment
                    {

                    			newCompositeNode(grammarAccess.getSObjectElementAccess().getSOEltContaimentParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_SOEltContaiment_2=ruleSOEltContaiment();

                    state._fsp--;


                    			current = this_SOEltContaiment_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSObjectElement"


    // $ANTLR start "entryRuleSOEltAttribute"
    // InternalEOML.g:248:1: entryRuleSOEltAttribute returns [EObject current=null] : iv_ruleSOEltAttribute= ruleSOEltAttribute EOF ;
    public final EObject entryRuleSOEltAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSOEltAttribute = null;


        try {
            // InternalEOML.g:248:55: (iv_ruleSOEltAttribute= ruleSOEltAttribute EOF )
            // InternalEOML.g:249:2: iv_ruleSOEltAttribute= ruleSOEltAttribute EOF
            {
             newCompositeNode(grammarAccess.getSOEltAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSOEltAttribute=ruleSOEltAttribute();

            state._fsp--;

             current =iv_ruleSOEltAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSOEltAttribute"


    // $ANTLR start "ruleSOEltAttribute"
    // InternalEOML.g:255:1: ruleSOEltAttribute returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' (otherlv_3= '[' ( (lv_savalue_4_0= ruleSAValue ) ) (otherlv_5= ',' ( (lv_savalue_6_0= ruleSAValue ) ) )* otherlv_7= ']' ) ) ;
    public final EObject ruleSOEltAttribute() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_savalue_4_0 = null;

        EObject lv_savalue_6_0 = null;



        	enterRule();

        try {
            // InternalEOML.g:261:2: ( ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' (otherlv_3= '[' ( (lv_savalue_4_0= ruleSAValue ) ) (otherlv_5= ',' ( (lv_savalue_6_0= ruleSAValue ) ) )* otherlv_7= ']' ) ) )
            // InternalEOML.g:262:2: ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' (otherlv_3= '[' ( (lv_savalue_4_0= ruleSAValue ) ) (otherlv_5= ',' ( (lv_savalue_6_0= ruleSAValue ) ) )* otherlv_7= ']' ) )
            {
            // InternalEOML.g:262:2: ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' (otherlv_3= '[' ( (lv_savalue_4_0= ruleSAValue ) ) (otherlv_5= ',' ( (lv_savalue_6_0= ruleSAValue ) ) )* otherlv_7= ']' ) )
            // InternalEOML.g:263:3: () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' (otherlv_3= '[' ( (lv_savalue_4_0= ruleSAValue ) ) (otherlv_5= ',' ( (lv_savalue_6_0= ruleSAValue ) ) )* otherlv_7= ']' )
            {
            // InternalEOML.g:263:3: ()
            // InternalEOML.g:264:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getSOEltAttributeAccess().getSOEltAttributeAction_0(),
            					current);
            			

            }

            // InternalEOML.g:270:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalEOML.g:271:4: (lv_name_1_0= RULE_ID )
            {
            // InternalEOML.g:271:4: (lv_name_1_0= RULE_ID )
            // InternalEOML.g:272:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_1_0, grammarAccess.getSOEltAttributeAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSOEltAttributeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_9); 

            			newLeafNode(otherlv_2, grammarAccess.getSOEltAttributeAccess().getColonKeyword_2());
            		
            // InternalEOML.g:292:3: (otherlv_3= '[' ( (lv_savalue_4_0= ruleSAValue ) ) (otherlv_5= ',' ( (lv_savalue_6_0= ruleSAValue ) ) )* otherlv_7= ']' )
            // InternalEOML.g:293:4: otherlv_3= '[' ( (lv_savalue_4_0= ruleSAValue ) ) (otherlv_5= ',' ( (lv_savalue_6_0= ruleSAValue ) ) )* otherlv_7= ']'
            {
            otherlv_3=(Token)match(input,17,FOLLOW_3); 

            				newLeafNode(otherlv_3, grammarAccess.getSOEltAttributeAccess().getLeftSquareBracketKeyword_3_0());
            			
            // InternalEOML.g:297:4: ( (lv_savalue_4_0= ruleSAValue ) )
            // InternalEOML.g:298:5: (lv_savalue_4_0= ruleSAValue )
            {
            // InternalEOML.g:298:5: (lv_savalue_4_0= ruleSAValue )
            // InternalEOML.g:299:6: lv_savalue_4_0= ruleSAValue
            {

            						newCompositeNode(grammarAccess.getSOEltAttributeAccess().getSavalueSAValueParserRuleCall_3_1_0());
            					
            pushFollow(FOLLOW_10);
            lv_savalue_4_0=ruleSAValue();

            state._fsp--;


            						if (current==null) {
            							current = createModelElementForParent(grammarAccess.getSOEltAttributeRule());
            						}
            						add(
            							current,
            							"savalue",
            							lv_savalue_4_0,
            							"org.scenariotools.emf.objectmodellanguage.eoml.EOML.SAValue");
            						afterParserOrEnumRuleCall();
            					

            }


            }

            // InternalEOML.g:316:4: (otherlv_5= ',' ( (lv_savalue_6_0= ruleSAValue ) ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==18) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalEOML.g:317:5: otherlv_5= ',' ( (lv_savalue_6_0= ruleSAValue ) )
            	    {
            	    otherlv_5=(Token)match(input,18,FOLLOW_3); 

            	    					newLeafNode(otherlv_5, grammarAccess.getSOEltAttributeAccess().getCommaKeyword_3_2_0());
            	    				
            	    // InternalEOML.g:321:5: ( (lv_savalue_6_0= ruleSAValue ) )
            	    // InternalEOML.g:322:6: (lv_savalue_6_0= ruleSAValue )
            	    {
            	    // InternalEOML.g:322:6: (lv_savalue_6_0= ruleSAValue )
            	    // InternalEOML.g:323:7: lv_savalue_6_0= ruleSAValue
            	    {

            	    							newCompositeNode(grammarAccess.getSOEltAttributeAccess().getSavalueSAValueParserRuleCall_3_2_1_0());
            	    						
            	    pushFollow(FOLLOW_10);
            	    lv_savalue_6_0=ruleSAValue();

            	    state._fsp--;


            	    							if (current==null) {
            	    								current = createModelElementForParent(grammarAccess.getSOEltAttributeRule());
            	    							}
            	    							add(
            	    								current,
            	    								"savalue",
            	    								lv_savalue_6_0,
            	    								"org.scenariotools.emf.objectmodellanguage.eoml.EOML.SAValue");
            	    							afterParserOrEnumRuleCall();
            	    						

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_7=(Token)match(input,19,FOLLOW_2); 

            				newLeafNode(otherlv_7, grammarAccess.getSOEltAttributeAccess().getRightSquareBracketKeyword_3_3());
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSOEltAttribute"


    // $ANTLR start "entryRuleSOEltRef"
    // InternalEOML.g:350:1: entryRuleSOEltRef returns [EObject current=null] : iv_ruleSOEltRef= ruleSOEltRef EOF ;
    public final EObject entryRuleSOEltRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSOEltRef = null;


        try {
            // InternalEOML.g:350:49: (iv_ruleSOEltRef= ruleSOEltRef EOF )
            // InternalEOML.g:351:2: iv_ruleSOEltRef= ruleSOEltRef EOF
            {
             newCompositeNode(grammarAccess.getSOEltRefRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSOEltRef=ruleSOEltRef();

            state._fsp--;

             current =iv_ruleSOEltRef; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSOEltRef"


    // $ANTLR start "ruleSOEltRef"
    // InternalEOML.g:357:1: ruleSOEltRef returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' (otherlv_3= '[' ( (lv_svalueobject_4_0= ruleSValueObjectRef ) ) (otherlv_5= ',' ( (lv_svalueobject_6_0= ruleSValueObjectRef ) ) )* otherlv_7= ']' ) ) ;
    public final EObject ruleSOEltRef() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_svalueobject_4_0 = null;

        EObject lv_svalueobject_6_0 = null;



        	enterRule();

        try {
            // InternalEOML.g:363:2: ( ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' (otherlv_3= '[' ( (lv_svalueobject_4_0= ruleSValueObjectRef ) ) (otherlv_5= ',' ( (lv_svalueobject_6_0= ruleSValueObjectRef ) ) )* otherlv_7= ']' ) ) )
            // InternalEOML.g:364:2: ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' (otherlv_3= '[' ( (lv_svalueobject_4_0= ruleSValueObjectRef ) ) (otherlv_5= ',' ( (lv_svalueobject_6_0= ruleSValueObjectRef ) ) )* otherlv_7= ']' ) )
            {
            // InternalEOML.g:364:2: ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' (otherlv_3= '[' ( (lv_svalueobject_4_0= ruleSValueObjectRef ) ) (otherlv_5= ',' ( (lv_svalueobject_6_0= ruleSValueObjectRef ) ) )* otherlv_7= ']' ) )
            // InternalEOML.g:365:3: () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' (otherlv_3= '[' ( (lv_svalueobject_4_0= ruleSValueObjectRef ) ) (otherlv_5= ',' ( (lv_svalueobject_6_0= ruleSValueObjectRef ) ) )* otherlv_7= ']' )
            {
            // InternalEOML.g:365:3: ()
            // InternalEOML.g:366:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getSOEltRefAccess().getSOEltRefAction_0(),
            					current);
            			

            }

            // InternalEOML.g:372:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalEOML.g:373:4: (lv_name_1_0= RULE_ID )
            {
            // InternalEOML.g:373:4: (lv_name_1_0= RULE_ID )
            // InternalEOML.g:374:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_1_0, grammarAccess.getSOEltRefAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSOEltRefRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_9); 

            			newLeafNode(otherlv_2, grammarAccess.getSOEltRefAccess().getColonKeyword_2());
            		
            // InternalEOML.g:394:3: (otherlv_3= '[' ( (lv_svalueobject_4_0= ruleSValueObjectRef ) ) (otherlv_5= ',' ( (lv_svalueobject_6_0= ruleSValueObjectRef ) ) )* otherlv_7= ']' )
            // InternalEOML.g:395:4: otherlv_3= '[' ( (lv_svalueobject_4_0= ruleSValueObjectRef ) ) (otherlv_5= ',' ( (lv_svalueobject_6_0= ruleSValueObjectRef ) ) )* otherlv_7= ']'
            {
            otherlv_3=(Token)match(input,17,FOLLOW_3); 

            				newLeafNode(otherlv_3, grammarAccess.getSOEltRefAccess().getLeftSquareBracketKeyword_3_0());
            			
            // InternalEOML.g:399:4: ( (lv_svalueobject_4_0= ruleSValueObjectRef ) )
            // InternalEOML.g:400:5: (lv_svalueobject_4_0= ruleSValueObjectRef )
            {
            // InternalEOML.g:400:5: (lv_svalueobject_4_0= ruleSValueObjectRef )
            // InternalEOML.g:401:6: lv_svalueobject_4_0= ruleSValueObjectRef
            {

            						newCompositeNode(grammarAccess.getSOEltRefAccess().getSvalueobjectSValueObjectRefParserRuleCall_3_1_0());
            					
            pushFollow(FOLLOW_10);
            lv_svalueobject_4_0=ruleSValueObjectRef();

            state._fsp--;


            						if (current==null) {
            							current = createModelElementForParent(grammarAccess.getSOEltRefRule());
            						}
            						add(
            							current,
            							"svalueobject",
            							lv_svalueobject_4_0,
            							"org.scenariotools.emf.objectmodellanguage.eoml.EOML.SValueObjectRef");
            						afterParserOrEnumRuleCall();
            					

            }


            }

            // InternalEOML.g:418:4: (otherlv_5= ',' ( (lv_svalueobject_6_0= ruleSValueObjectRef ) ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==18) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalEOML.g:419:5: otherlv_5= ',' ( (lv_svalueobject_6_0= ruleSValueObjectRef ) )
            	    {
            	    otherlv_5=(Token)match(input,18,FOLLOW_3); 

            	    					newLeafNode(otherlv_5, grammarAccess.getSOEltRefAccess().getCommaKeyword_3_2_0());
            	    				
            	    // InternalEOML.g:423:5: ( (lv_svalueobject_6_0= ruleSValueObjectRef ) )
            	    // InternalEOML.g:424:6: (lv_svalueobject_6_0= ruleSValueObjectRef )
            	    {
            	    // InternalEOML.g:424:6: (lv_svalueobject_6_0= ruleSValueObjectRef )
            	    // InternalEOML.g:425:7: lv_svalueobject_6_0= ruleSValueObjectRef
            	    {

            	    							newCompositeNode(grammarAccess.getSOEltRefAccess().getSvalueobjectSValueObjectRefParserRuleCall_3_2_1_0());
            	    						
            	    pushFollow(FOLLOW_10);
            	    lv_svalueobject_6_0=ruleSValueObjectRef();

            	    state._fsp--;


            	    							if (current==null) {
            	    								current = createModelElementForParent(grammarAccess.getSOEltRefRule());
            	    							}
            	    							add(
            	    								current,
            	    								"svalueobject",
            	    								lv_svalueobject_6_0,
            	    								"org.scenariotools.emf.objectmodellanguage.eoml.EOML.SValueObjectRef");
            	    							afterParserOrEnumRuleCall();
            	    						

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            otherlv_7=(Token)match(input,19,FOLLOW_2); 

            				newLeafNode(otherlv_7, grammarAccess.getSOEltRefAccess().getRightSquareBracketKeyword_3_3());
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSOEltRef"


    // $ANTLR start "entryRuleSOEltContaiment"
    // InternalEOML.g:452:1: entryRuleSOEltContaiment returns [EObject current=null] : iv_ruleSOEltContaiment= ruleSOEltContaiment EOF ;
    public final EObject entryRuleSOEltContaiment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSOEltContaiment = null;


        try {
            // InternalEOML.g:452:56: (iv_ruleSOEltContaiment= ruleSOEltContaiment EOF )
            // InternalEOML.g:453:2: iv_ruleSOEltContaiment= ruleSOEltContaiment EOF
            {
             newCompositeNode(grammarAccess.getSOEltContaimentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSOEltContaiment=ruleSOEltContaiment();

            state._fsp--;

             current =iv_ruleSOEltContaiment; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSOEltContaiment"


    // $ANTLR start "ruleSOEltContaiment"
    // InternalEOML.g:459:1: ruleSOEltContaiment returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' (otherlv_3= '[' ( (lv_sobject_4_0= ruleSObject ) ) (otherlv_5= ',' ( (lv_sobject_6_0= ruleSObject ) ) )* otherlv_7= ']' ) ) ;
    public final EObject ruleSOEltContaiment() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_sobject_4_0 = null;

        EObject lv_sobject_6_0 = null;



        	enterRule();

        try {
            // InternalEOML.g:465:2: ( ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' (otherlv_3= '[' ( (lv_sobject_4_0= ruleSObject ) ) (otherlv_5= ',' ( (lv_sobject_6_0= ruleSObject ) ) )* otherlv_7= ']' ) ) )
            // InternalEOML.g:466:2: ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' (otherlv_3= '[' ( (lv_sobject_4_0= ruleSObject ) ) (otherlv_5= ',' ( (lv_sobject_6_0= ruleSObject ) ) )* otherlv_7= ']' ) )
            {
            // InternalEOML.g:466:2: ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' (otherlv_3= '[' ( (lv_sobject_4_0= ruleSObject ) ) (otherlv_5= ',' ( (lv_sobject_6_0= ruleSObject ) ) )* otherlv_7= ']' ) )
            // InternalEOML.g:467:3: () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' (otherlv_3= '[' ( (lv_sobject_4_0= ruleSObject ) ) (otherlv_5= ',' ( (lv_sobject_6_0= ruleSObject ) ) )* otherlv_7= ']' )
            {
            // InternalEOML.g:467:3: ()
            // InternalEOML.g:468:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getSOEltContaimentAccess().getSOEltContaimentAction_0(),
            					current);
            			

            }

            // InternalEOML.g:474:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalEOML.g:475:4: (lv_name_1_0= RULE_ID )
            {
            // InternalEOML.g:475:4: (lv_name_1_0= RULE_ID )
            // InternalEOML.g:476:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_1_0, grammarAccess.getSOEltContaimentAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSOEltContaimentRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_9); 

            			newLeafNode(otherlv_2, grammarAccess.getSOEltContaimentAccess().getColonKeyword_2());
            		
            // InternalEOML.g:496:3: (otherlv_3= '[' ( (lv_sobject_4_0= ruleSObject ) ) (otherlv_5= ',' ( (lv_sobject_6_0= ruleSObject ) ) )* otherlv_7= ']' )
            // InternalEOML.g:497:4: otherlv_3= '[' ( (lv_sobject_4_0= ruleSObject ) ) (otherlv_5= ',' ( (lv_sobject_6_0= ruleSObject ) ) )* otherlv_7= ']'
            {
            otherlv_3=(Token)match(input,17,FOLLOW_11); 

            				newLeafNode(otherlv_3, grammarAccess.getSOEltContaimentAccess().getLeftSquareBracketKeyword_3_0());
            			
            // InternalEOML.g:501:4: ( (lv_sobject_4_0= ruleSObject ) )
            // InternalEOML.g:502:5: (lv_sobject_4_0= ruleSObject )
            {
            // InternalEOML.g:502:5: (lv_sobject_4_0= ruleSObject )
            // InternalEOML.g:503:6: lv_sobject_4_0= ruleSObject
            {

            						newCompositeNode(grammarAccess.getSOEltContaimentAccess().getSobjectSObjectParserRuleCall_3_1_0());
            					
            pushFollow(FOLLOW_10);
            lv_sobject_4_0=ruleSObject();

            state._fsp--;


            						if (current==null) {
            							current = createModelElementForParent(grammarAccess.getSOEltContaimentRule());
            						}
            						add(
            							current,
            							"sobject",
            							lv_sobject_4_0,
            							"org.scenariotools.emf.objectmodellanguage.eoml.EOML.SObject");
            						afterParserOrEnumRuleCall();
            					

            }


            }

            // InternalEOML.g:520:4: (otherlv_5= ',' ( (lv_sobject_6_0= ruleSObject ) ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==18) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalEOML.g:521:5: otherlv_5= ',' ( (lv_sobject_6_0= ruleSObject ) )
            	    {
            	    otherlv_5=(Token)match(input,18,FOLLOW_11); 

            	    					newLeafNode(otherlv_5, grammarAccess.getSOEltContaimentAccess().getCommaKeyword_3_2_0());
            	    				
            	    // InternalEOML.g:525:5: ( (lv_sobject_6_0= ruleSObject ) )
            	    // InternalEOML.g:526:6: (lv_sobject_6_0= ruleSObject )
            	    {
            	    // InternalEOML.g:526:6: (lv_sobject_6_0= ruleSObject )
            	    // InternalEOML.g:527:7: lv_sobject_6_0= ruleSObject
            	    {

            	    							newCompositeNode(grammarAccess.getSOEltContaimentAccess().getSobjectSObjectParserRuleCall_3_2_1_0());
            	    						
            	    pushFollow(FOLLOW_10);
            	    lv_sobject_6_0=ruleSObject();

            	    state._fsp--;


            	    							if (current==null) {
            	    								current = createModelElementForParent(grammarAccess.getSOEltContaimentRule());
            	    							}
            	    							add(
            	    								current,
            	    								"sobject",
            	    								lv_sobject_6_0,
            	    								"org.scenariotools.emf.objectmodellanguage.eoml.EOML.SObject");
            	    							afterParserOrEnumRuleCall();
            	    						

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            otherlv_7=(Token)match(input,19,FOLLOW_2); 

            				newLeafNode(otherlv_7, grammarAccess.getSOEltContaimentAccess().getRightSquareBracketKeyword_3_3());
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSOEltContaiment"


    // $ANTLR start "entryRuleSAValue"
    // InternalEOML.g:554:1: entryRuleSAValue returns [EObject current=null] : iv_ruleSAValue= ruleSAValue EOF ;
    public final EObject entryRuleSAValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSAValue = null;


        try {
            // InternalEOML.g:554:48: (iv_ruleSAValue= ruleSAValue EOF )
            // InternalEOML.g:555:2: iv_ruleSAValue= ruleSAValue EOF
            {
             newCompositeNode(grammarAccess.getSAValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSAValue=ruleSAValue();

            state._fsp--;

             current =iv_ruleSAValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSAValue"


    // $ANTLR start "ruleSAValue"
    // InternalEOML.g:561:1: ruleSAValue returns [EObject current=null] : this_SAValuePrimitive_0= ruleSAValuePrimitive ;
    public final EObject ruleSAValue() throws RecognitionException {
        EObject current = null;

        EObject this_SAValuePrimitive_0 = null;



        	enterRule();

        try {
            // InternalEOML.g:567:2: (this_SAValuePrimitive_0= ruleSAValuePrimitive )
            // InternalEOML.g:568:2: this_SAValuePrimitive_0= ruleSAValuePrimitive
            {

            		newCompositeNode(grammarAccess.getSAValueAccess().getSAValuePrimitiveParserRuleCall());
            	
            pushFollow(FOLLOW_2);
            this_SAValuePrimitive_0=ruleSAValuePrimitive();

            state._fsp--;


            		current = this_SAValuePrimitive_0;
            		afterParserOrEnumRuleCall();
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSAValue"


    // $ANTLR start "entryRuleSAValuePrimitive"
    // InternalEOML.g:579:1: entryRuleSAValuePrimitive returns [EObject current=null] : iv_ruleSAValuePrimitive= ruleSAValuePrimitive EOF ;
    public final EObject entryRuleSAValuePrimitive() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSAValuePrimitive = null;


        try {
            // InternalEOML.g:579:57: (iv_ruleSAValuePrimitive= ruleSAValuePrimitive EOF )
            // InternalEOML.g:580:2: iv_ruleSAValuePrimitive= ruleSAValuePrimitive EOF
            {
             newCompositeNode(grammarAccess.getSAValuePrimitiveRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSAValuePrimitive=ruleSAValuePrimitive();

            state._fsp--;

             current =iv_ruleSAValuePrimitive; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSAValuePrimitive"


    // $ANTLR start "ruleSAValuePrimitive"
    // InternalEOML.g:586:1: ruleSAValuePrimitive returns [EObject current=null] : ( () ( (lv_value_1_0= ruleEString ) )? ) ;
    public final EObject ruleSAValuePrimitive() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_value_1_0 = null;



        	enterRule();

        try {
            // InternalEOML.g:592:2: ( ( () ( (lv_value_1_0= ruleEString ) )? ) )
            // InternalEOML.g:593:2: ( () ( (lv_value_1_0= ruleEString ) )? )
            {
            // InternalEOML.g:593:2: ( () ( (lv_value_1_0= ruleEString ) )? )
            // InternalEOML.g:594:3: () ( (lv_value_1_0= ruleEString ) )?
            {
            // InternalEOML.g:594:3: ()
            // InternalEOML.g:595:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getSAValuePrimitiveAccess().getSValuePrimitiveAction_0(),
            					current);
            			

            }

            // InternalEOML.g:601:3: ( (lv_value_1_0= ruleEString ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( ((LA7_0>=RULE_ID && LA7_0<=RULE_INT)||LA7_0==20) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalEOML.g:602:4: (lv_value_1_0= ruleEString )
                    {
                    // InternalEOML.g:602:4: (lv_value_1_0= ruleEString )
                    // InternalEOML.g:603:5: lv_value_1_0= ruleEString
                    {

                    					newCompositeNode(grammarAccess.getSAValuePrimitiveAccess().getValueEStringParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_value_1_0=ruleEString();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getSAValuePrimitiveRule());
                    					}
                    					set(
                    						current,
                    						"value",
                    						lv_value_1_0,
                    						"org.scenariotools.emf.objectmodellanguage.eoml.EOML.EString");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSAValuePrimitive"


    // $ANTLR start "entryRuleSValueObjectRef"
    // InternalEOML.g:624:1: entryRuleSValueObjectRef returns [EObject current=null] : iv_ruleSValueObjectRef= ruleSValueObjectRef EOF ;
    public final EObject entryRuleSValueObjectRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSValueObjectRef = null;


        try {
            // InternalEOML.g:624:56: (iv_ruleSValueObjectRef= ruleSValueObjectRef EOF )
            // InternalEOML.g:625:2: iv_ruleSValueObjectRef= ruleSValueObjectRef EOF
            {
             newCompositeNode(grammarAccess.getSValueObjectRefRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSValueObjectRef=ruleSValueObjectRef();

            state._fsp--;

             current =iv_ruleSValueObjectRef; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSValueObjectRef"


    // $ANTLR start "ruleSValueObjectRef"
    // InternalEOML.g:631:1: ruleSValueObjectRef returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( (lv_eClass_3_0= RULE_ID ) ) ) ;
    public final EObject ruleSValueObjectRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token lv_eClass_3_0=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalEOML.g:637:2: ( ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( (lv_eClass_3_0= RULE_ID ) ) ) )
            // InternalEOML.g:638:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( (lv_eClass_3_0= RULE_ID ) ) )
            {
            // InternalEOML.g:638:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( (lv_eClass_3_0= RULE_ID ) ) )
            // InternalEOML.g:639:3: () ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( (lv_eClass_3_0= RULE_ID ) )
            {
            // InternalEOML.g:639:3: ()
            // InternalEOML.g:640:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getSValueObjectRefAccess().getSValueObjectRefAction_0(),
            					current);
            			

            }

            // InternalEOML.g:646:3: ( (lv_name_1_0= ruleEString ) )
            // InternalEOML.g:647:4: (lv_name_1_0= ruleEString )
            {
            // InternalEOML.g:647:4: (lv_name_1_0= ruleEString )
            // InternalEOML.g:648:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getSValueObjectRefAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSValueObjectRefRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.scenariotools.emf.objectmodellanguage.eoml.EOML.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getSValueObjectRefAccess().getColonKeyword_2());
            		
            // InternalEOML.g:669:3: ( (lv_eClass_3_0= RULE_ID ) )
            // InternalEOML.g:670:4: (lv_eClass_3_0= RULE_ID )
            {
            // InternalEOML.g:670:4: (lv_eClass_3_0= RULE_ID )
            // InternalEOML.g:671:5: lv_eClass_3_0= RULE_ID
            {
            lv_eClass_3_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_eClass_3_0, grammarAccess.getSValueObjectRefAccess().getEClassIDTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSValueObjectRefRule());
            					}
            					setWithLastConsumed(
            						current,
            						"eClass",
            						lv_eClass_3_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSValueObjectRef"


    // $ANTLR start "entryRuleEString"
    // InternalEOML.g:691:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalEOML.g:691:47: (iv_ruleEString= ruleEString EOF )
            // InternalEOML.g:692:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalEOML.g:698:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID | ( (kw= '-' )? this_INT_3= RULE_INT ) ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;
        Token kw=null;
        Token this_INT_3=null;


        	enterRule();

        try {
            // InternalEOML.g:704:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID | ( (kw= '-' )? this_INT_3= RULE_INT ) ) )
            // InternalEOML.g:705:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID | ( (kw= '-' )? this_INT_3= RULE_INT ) )
            {
            // InternalEOML.g:705:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID | ( (kw= '-' )? this_INT_3= RULE_INT ) )
            int alt9=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt9=1;
                }
                break;
            case RULE_ID:
                {
                alt9=2;
                }
                break;
            case RULE_INT:
            case 20:
                {
                alt9=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalEOML.g:706:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalEOML.g:714:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalEOML.g:722:3: ( (kw= '-' )? this_INT_3= RULE_INT )
                    {
                    // InternalEOML.g:722:3: ( (kw= '-' )? this_INT_3= RULE_INT )
                    // InternalEOML.g:723:4: (kw= '-' )? this_INT_3= RULE_INT
                    {
                    // InternalEOML.g:723:4: (kw= '-' )?
                    int alt8=2;
                    int LA8_0 = input.LA(1);

                    if ( (LA8_0==20) ) {
                        alt8=1;
                    }
                    switch (alt8) {
                        case 1 :
                            // InternalEOML.g:724:5: kw= '-'
                            {
                            kw=(Token)match(input,20,FOLLOW_12); 

                            					current.merge(kw);
                            					newLeafNode(kw, grammarAccess.getEStringAccess().getHyphenMinusKeyword_2_0());
                            				

                            }
                            break;

                    }

                    this_INT_3=(Token)match(input,RULE_INT,FOLLOW_2); 

                    				current.merge(this_INT_3);
                    			

                    				newLeafNode(this_INT_3, grammarAccess.getEStringAccess().getINTTerminalRuleCall_2_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"

    // Delegated rules


    protected DFA3 dfa3 = new DFA3(this);
    static final String dfa_1s = "\13\uffff";
    static final String dfa_2s = "\1\4\1\14\1\21\1\4\2\14\1\6\1\14\3\uffff";
    static final String dfa_3s = "\1\4\1\14\1\21\1\24\2\23\1\6\1\23\3\uffff";
    static final String dfa_4s = "\10\uffff\1\3\1\1\1\2";
    static final String dfa_5s = "\13\uffff}>";
    static final String[] dfa_6s = {
            "\1\1",
            "\1\2",
            "\1\3",
            "\1\5\1\4\1\7\4\uffff\1\10\6\uffff\2\11\1\6",
            "\1\12\5\uffff\2\11",
            "\1\12\5\uffff\2\11",
            "\1\7",
            "\1\12\5\uffff\2\11",
            "",
            "",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA3 extends DFA {

        public DFA3(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 3;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "217:2: (this_SOEltAttribute_0= ruleSOEltAttribute | this_SOEltRef_1= ruleSOEltRef | this_SOEltContaiment_2= ruleSOEltContaiment )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000100070L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x000000000001C000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x00000000000C0000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000040L});

}