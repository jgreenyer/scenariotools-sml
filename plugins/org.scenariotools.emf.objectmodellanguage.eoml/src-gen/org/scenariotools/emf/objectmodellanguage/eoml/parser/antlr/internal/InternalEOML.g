/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * ScenarioTools-URL: www.scenariotools.org
 *
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete
 */
grammar InternalEOML;

options {
	superClass=AbstractInternalAntlrParser;
}

@lexer::header {
package org.scenariotools.emf.objectmodellanguage.eoml.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;
}

@parser::header {
package org.scenariotools.emf.objectmodellanguage.eoml.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.scenariotools.emf.objectmodellanguage.eoml.services.EOMLGrammarAccess;

}

@parser::members {

 	private EOMLGrammarAccess grammarAccess;

    public InternalEOMLParser(TokenStream input, EOMLGrammarAccess grammarAccess) {
        this(input);
        this.grammarAccess = grammarAccess;
        registerRules(grammarAccess.getGrammar());
    }

    @Override
    protected String getFirstRuleName() {
    	return "SObject";
   	}

   	@Override
   	protected EOMLGrammarAccess getGrammarAccess() {
   		return grammarAccess;
   	}

}

@rulecatch {
    catch (RecognitionException re) {
        recover(input,re);
        appendSkippedTokens();
    }
}

// Entry rule entryRuleSObject
entryRuleSObject returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getSObjectRule()); }
	iv_ruleSObject=ruleSObject
	{ $current=$iv_ruleSObject.current; }
	EOF;

// Rule SObject
ruleSObject returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			{
				$current = forceCreateModelElement(
					grammarAccess.getSObjectAccess().getSObjectAction_0(),
					$current);
			}
		)
		otherlv_1='object'
		{
			newLeafNode(otherlv_1, grammarAccess.getSObjectAccess().getObjectKeyword_1());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getSObjectAccess().getNameEStringParserRuleCall_2_0());
				}
				lv_name_2_0=ruleEString
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getSObjectRule());
					}
					set(
						$current,
						"name",
						lv_name_2_0,
						"org.scenariotools.emf.objectmodellanguage.eoml.EOML.EString");
					afterParserOrEnumRuleCall();
				}
			)
		)
		otherlv_3=':'
		{
			newLeafNode(otherlv_3, grammarAccess.getSObjectAccess().getColonKeyword_3());
		}
		(
			(
				lv_eClass_4_0=RULE_ID
				{
					newLeafNode(lv_eClass_4_0, grammarAccess.getSObjectAccess().getEClassIDTerminalRuleCall_4_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getSObjectRule());
					}
					setWithLastConsumed(
						$current,
						"eClass",
						lv_eClass_4_0,
						"org.eclipse.xtext.common.Terminals.ID");
				}
			)
		)
		otherlv_5='{'
		{
			newLeafNode(otherlv_5, grammarAccess.getSObjectAccess().getLeftCurlyBracketKeyword_5());
		}
		(
			otherlv_6='attribute'
			{
				newLeafNode(otherlv_6, grammarAccess.getSObjectAccess().getAttributeKeyword_6_0());
			}
			(
				(
					{
						newCompositeNode(grammarAccess.getSObjectAccess().getSobjectelementSObjectElementParserRuleCall_6_1_0());
					}
					lv_sobjectelement_7_0=ruleSObjectElement
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getSObjectRule());
						}
						add(
							$current,
							"sobjectelement",
							lv_sobjectelement_7_0,
							"org.scenariotools.emf.objectmodellanguage.eoml.EOML.SObjectElement");
						afterParserOrEnumRuleCall();
					}
				)
			)
		)*
		(
			otherlv_8='reference'
			{
				newLeafNode(otherlv_8, grammarAccess.getSObjectAccess().getReferenceKeyword_7_0());
			}
			(
				(
					{
						newCompositeNode(grammarAccess.getSObjectAccess().getSobjectelementSObjectElementParserRuleCall_7_1_0());
					}
					lv_sobjectelement_9_0=ruleSObjectElement
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getSObjectRule());
						}
						add(
							$current,
							"sobjectelement",
							lv_sobjectelement_9_0,
							"org.scenariotools.emf.objectmodellanguage.eoml.EOML.SObjectElement");
						afterParserOrEnumRuleCall();
					}
				)
			)
		)*
		otherlv_10='}'
		{
			newLeafNode(otherlv_10, grammarAccess.getSObjectAccess().getRightCurlyBracketKeyword_8());
		}
	)
;

// Entry rule entryRuleSObjectElement
entryRuleSObjectElement returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getSObjectElementRule()); }
	iv_ruleSObjectElement=ruleSObjectElement
	{ $current=$iv_ruleSObjectElement.current; }
	EOF;

// Rule SObjectElement
ruleSObjectElement returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		{
			newCompositeNode(grammarAccess.getSObjectElementAccess().getSOEltAttributeParserRuleCall_0());
		}
		this_SOEltAttribute_0=ruleSOEltAttribute
		{
			$current = $this_SOEltAttribute_0.current;
			afterParserOrEnumRuleCall();
		}
		    |
		{
			newCompositeNode(grammarAccess.getSObjectElementAccess().getSOEltRefParserRuleCall_1());
		}
		this_SOEltRef_1=ruleSOEltRef
		{
			$current = $this_SOEltRef_1.current;
			afterParserOrEnumRuleCall();
		}
		    |
		{
			newCompositeNode(grammarAccess.getSObjectElementAccess().getSOEltContaimentParserRuleCall_2());
		}
		this_SOEltContaiment_2=ruleSOEltContaiment
		{
			$current = $this_SOEltContaiment_2.current;
			afterParserOrEnumRuleCall();
		}
	)
;

// Entry rule entryRuleSOEltAttribute
entryRuleSOEltAttribute returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getSOEltAttributeRule()); }
	iv_ruleSOEltAttribute=ruleSOEltAttribute
	{ $current=$iv_ruleSOEltAttribute.current; }
	EOF;

// Rule SOEltAttribute
ruleSOEltAttribute returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			{
				$current = forceCreateModelElement(
					grammarAccess.getSOEltAttributeAccess().getSOEltAttributeAction_0(),
					$current);
			}
		)
		(
			(
				lv_name_1_0=RULE_ID
				{
					newLeafNode(lv_name_1_0, grammarAccess.getSOEltAttributeAccess().getNameIDTerminalRuleCall_1_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getSOEltAttributeRule());
					}
					setWithLastConsumed(
						$current,
						"name",
						lv_name_1_0,
						"org.eclipse.xtext.common.Terminals.ID");
				}
			)
		)
		otherlv_2=':'
		{
			newLeafNode(otherlv_2, grammarAccess.getSOEltAttributeAccess().getColonKeyword_2());
		}
		(
			otherlv_3='['
			{
				newLeafNode(otherlv_3, grammarAccess.getSOEltAttributeAccess().getLeftSquareBracketKeyword_3_0());
			}
			(
				(
					{
						newCompositeNode(grammarAccess.getSOEltAttributeAccess().getSavalueSAValueParserRuleCall_3_1_0());
					}
					lv_savalue_4_0=ruleSAValue
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getSOEltAttributeRule());
						}
						add(
							$current,
							"savalue",
							lv_savalue_4_0,
							"org.scenariotools.emf.objectmodellanguage.eoml.EOML.SAValue");
						afterParserOrEnumRuleCall();
					}
				)
			)
			(
				otherlv_5=','
				{
					newLeafNode(otherlv_5, grammarAccess.getSOEltAttributeAccess().getCommaKeyword_3_2_0());
				}
				(
					(
						{
							newCompositeNode(grammarAccess.getSOEltAttributeAccess().getSavalueSAValueParserRuleCall_3_2_1_0());
						}
						lv_savalue_6_0=ruleSAValue
						{
							if ($current==null) {
								$current = createModelElementForParent(grammarAccess.getSOEltAttributeRule());
							}
							add(
								$current,
								"savalue",
								lv_savalue_6_0,
								"org.scenariotools.emf.objectmodellanguage.eoml.EOML.SAValue");
							afterParserOrEnumRuleCall();
						}
					)
				)
			)*
			otherlv_7=']'
			{
				newLeafNode(otherlv_7, grammarAccess.getSOEltAttributeAccess().getRightSquareBracketKeyword_3_3());
			}
		)
	)
;

// Entry rule entryRuleSOEltRef
entryRuleSOEltRef returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getSOEltRefRule()); }
	iv_ruleSOEltRef=ruleSOEltRef
	{ $current=$iv_ruleSOEltRef.current; }
	EOF;

// Rule SOEltRef
ruleSOEltRef returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			{
				$current = forceCreateModelElement(
					grammarAccess.getSOEltRefAccess().getSOEltRefAction_0(),
					$current);
			}
		)
		(
			(
				lv_name_1_0=RULE_ID
				{
					newLeafNode(lv_name_1_0, grammarAccess.getSOEltRefAccess().getNameIDTerminalRuleCall_1_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getSOEltRefRule());
					}
					setWithLastConsumed(
						$current,
						"name",
						lv_name_1_0,
						"org.eclipse.xtext.common.Terminals.ID");
				}
			)
		)
		otherlv_2=':'
		{
			newLeafNode(otherlv_2, grammarAccess.getSOEltRefAccess().getColonKeyword_2());
		}
		(
			otherlv_3='['
			{
				newLeafNode(otherlv_3, grammarAccess.getSOEltRefAccess().getLeftSquareBracketKeyword_3_0());
			}
			(
				(
					{
						newCompositeNode(grammarAccess.getSOEltRefAccess().getSvalueobjectSValueObjectRefParserRuleCall_3_1_0());
					}
					lv_svalueobject_4_0=ruleSValueObjectRef
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getSOEltRefRule());
						}
						add(
							$current,
							"svalueobject",
							lv_svalueobject_4_0,
							"org.scenariotools.emf.objectmodellanguage.eoml.EOML.SValueObjectRef");
						afterParserOrEnumRuleCall();
					}
				)
			)
			(
				otherlv_5=','
				{
					newLeafNode(otherlv_5, grammarAccess.getSOEltRefAccess().getCommaKeyword_3_2_0());
				}
				(
					(
						{
							newCompositeNode(grammarAccess.getSOEltRefAccess().getSvalueobjectSValueObjectRefParserRuleCall_3_2_1_0());
						}
						lv_svalueobject_6_0=ruleSValueObjectRef
						{
							if ($current==null) {
								$current = createModelElementForParent(grammarAccess.getSOEltRefRule());
							}
							add(
								$current,
								"svalueobject",
								lv_svalueobject_6_0,
								"org.scenariotools.emf.objectmodellanguage.eoml.EOML.SValueObjectRef");
							afterParserOrEnumRuleCall();
						}
					)
				)
			)*
			otherlv_7=']'
			{
				newLeafNode(otherlv_7, grammarAccess.getSOEltRefAccess().getRightSquareBracketKeyword_3_3());
			}
		)
	)
;

// Entry rule entryRuleSOEltContaiment
entryRuleSOEltContaiment returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getSOEltContaimentRule()); }
	iv_ruleSOEltContaiment=ruleSOEltContaiment
	{ $current=$iv_ruleSOEltContaiment.current; }
	EOF;

// Rule SOEltContaiment
ruleSOEltContaiment returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			{
				$current = forceCreateModelElement(
					grammarAccess.getSOEltContaimentAccess().getSOEltContaimentAction_0(),
					$current);
			}
		)
		(
			(
				lv_name_1_0=RULE_ID
				{
					newLeafNode(lv_name_1_0, grammarAccess.getSOEltContaimentAccess().getNameIDTerminalRuleCall_1_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getSOEltContaimentRule());
					}
					setWithLastConsumed(
						$current,
						"name",
						lv_name_1_0,
						"org.eclipse.xtext.common.Terminals.ID");
				}
			)
		)
		otherlv_2=':'
		{
			newLeafNode(otherlv_2, grammarAccess.getSOEltContaimentAccess().getColonKeyword_2());
		}
		(
			otherlv_3='['
			{
				newLeafNode(otherlv_3, grammarAccess.getSOEltContaimentAccess().getLeftSquareBracketKeyword_3_0());
			}
			(
				(
					{
						newCompositeNode(grammarAccess.getSOEltContaimentAccess().getSobjectSObjectParserRuleCall_3_1_0());
					}
					lv_sobject_4_0=ruleSObject
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getSOEltContaimentRule());
						}
						add(
							$current,
							"sobject",
							lv_sobject_4_0,
							"org.scenariotools.emf.objectmodellanguage.eoml.EOML.SObject");
						afterParserOrEnumRuleCall();
					}
				)
			)
			(
				otherlv_5=','
				{
					newLeafNode(otherlv_5, grammarAccess.getSOEltContaimentAccess().getCommaKeyword_3_2_0());
				}
				(
					(
						{
							newCompositeNode(grammarAccess.getSOEltContaimentAccess().getSobjectSObjectParserRuleCall_3_2_1_0());
						}
						lv_sobject_6_0=ruleSObject
						{
							if ($current==null) {
								$current = createModelElementForParent(grammarAccess.getSOEltContaimentRule());
							}
							add(
								$current,
								"sobject",
								lv_sobject_6_0,
								"org.scenariotools.emf.objectmodellanguage.eoml.EOML.SObject");
							afterParserOrEnumRuleCall();
						}
					)
				)
			)*
			otherlv_7=']'
			{
				newLeafNode(otherlv_7, grammarAccess.getSOEltContaimentAccess().getRightSquareBracketKeyword_3_3());
			}
		)
	)
;

// Entry rule entryRuleSAValue
entryRuleSAValue returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getSAValueRule()); }
	iv_ruleSAValue=ruleSAValue
	{ $current=$iv_ruleSAValue.current; }
	EOF;

// Rule SAValue
ruleSAValue returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	{
		newCompositeNode(grammarAccess.getSAValueAccess().getSAValuePrimitiveParserRuleCall());
	}
	this_SAValuePrimitive_0=ruleSAValuePrimitive
	{
		$current = $this_SAValuePrimitive_0.current;
		afterParserOrEnumRuleCall();
	}
;

// Entry rule entryRuleSAValuePrimitive
entryRuleSAValuePrimitive returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getSAValuePrimitiveRule()); }
	iv_ruleSAValuePrimitive=ruleSAValuePrimitive
	{ $current=$iv_ruleSAValuePrimitive.current; }
	EOF;

// Rule SAValuePrimitive
ruleSAValuePrimitive returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			{
				$current = forceCreateModelElement(
					grammarAccess.getSAValuePrimitiveAccess().getSValuePrimitiveAction_0(),
					$current);
			}
		)
		(
			(
				{
					newCompositeNode(grammarAccess.getSAValuePrimitiveAccess().getValueEStringParserRuleCall_1_0());
				}
				lv_value_1_0=ruleEString
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getSAValuePrimitiveRule());
					}
					set(
						$current,
						"value",
						lv_value_1_0,
						"org.scenariotools.emf.objectmodellanguage.eoml.EOML.EString");
					afterParserOrEnumRuleCall();
				}
			)
		)?
	)
;

// Entry rule entryRuleSValueObjectRef
entryRuleSValueObjectRef returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getSValueObjectRefRule()); }
	iv_ruleSValueObjectRef=ruleSValueObjectRef
	{ $current=$iv_ruleSValueObjectRef.current; }
	EOF;

// Rule SValueObjectRef
ruleSValueObjectRef returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			{
				$current = forceCreateModelElement(
					grammarAccess.getSValueObjectRefAccess().getSValueObjectRefAction_0(),
					$current);
			}
		)
		(
			(
				{
					newCompositeNode(grammarAccess.getSValueObjectRefAccess().getNameEStringParserRuleCall_1_0());
				}
				lv_name_1_0=ruleEString
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getSValueObjectRefRule());
					}
					set(
						$current,
						"name",
						lv_name_1_0,
						"org.scenariotools.emf.objectmodellanguage.eoml.EOML.EString");
					afterParserOrEnumRuleCall();
				}
			)
		)
		otherlv_2=':'
		{
			newLeafNode(otherlv_2, grammarAccess.getSValueObjectRefAccess().getColonKeyword_2());
		}
		(
			(
				lv_eClass_3_0=RULE_ID
				{
					newLeafNode(lv_eClass_3_0, grammarAccess.getSValueObjectRefAccess().getEClassIDTerminalRuleCall_3_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getSValueObjectRefRule());
					}
					setWithLastConsumed(
						$current,
						"eClass",
						lv_eClass_3_0,
						"org.eclipse.xtext.common.Terminals.ID");
				}
			)
		)
	)
;

// Entry rule entryRuleEString
entryRuleEString returns [String current=null]:
	{ newCompositeNode(grammarAccess.getEStringRule()); }
	iv_ruleEString=ruleEString
	{ $current=$iv_ruleEString.current.getText(); }
	EOF;

// Rule EString
ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		this_STRING_0=RULE_STRING
		{
			$current.merge(this_STRING_0);
		}
		{
			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
		}
		    |
		this_ID_1=RULE_ID
		{
			$current.merge(this_ID_1);
		}
		{
			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
		}
		    |
		(
			(
				kw='-'
				{
					$current.merge(kw);
					newLeafNode(kw, grammarAccess.getEStringAccess().getHyphenMinusKeyword_2_0());
				}
			)?
			this_INT_3=RULE_INT
			{
				$current.merge(this_INT_3);
			}
			{
				newLeafNode(this_INT_3, grammarAccess.getEStringAccess().getINTTerminalRuleCall_2_1());
			}
		)
	)
;

RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_INT : ('0'..'9')+;

RULE_STRING : ('"' ('\\' .|~(('\\'|'"')))* '"'|'\'' ('\\' .|~(('\\'|'\'')))* '\'');

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;
