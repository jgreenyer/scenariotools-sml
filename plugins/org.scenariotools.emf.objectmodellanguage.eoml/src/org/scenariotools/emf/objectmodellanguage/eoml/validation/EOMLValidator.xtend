/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * ScenarioTools-URL: www.scenariotools.org
 *
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete
 */
package org.scenariotools.emf.objectmodellanguage.eoml.validation


/**
 * This class contains custom validation rules. 
 *
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
class EOMLValidator extends AbstractEOMLValidator {
	
//	public static val INVALID_NAME = 'invalidName'
//
//	@Check
//	def checkGreetingStartsWithCapital(Greeting greeting) {
//		if (!Character.isUpperCase(greeting.name.charAt(0))) {
//			warning('Name should start with a capital', 
//					EOMLPackage.Literals.GREETING__NAME,
//					INVALID_NAME)
//		}
//	}
	
}
