/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 * 
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete 
 */
package org.scenariotools.emf.objectmodellanguage.eoml.util

import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.IPath
import org.eclipse.core.runtime.Path
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.ui.IEditorInput
import org.eclipse.ui.IEditorPart
import org.eclipse.ui.IWorkbench
import org.eclipse.ui.IWorkbenchPage
import org.eclipse.ui.IWorkbenchWindow
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.part.FileEditorInput

/** 
 * @author Eric Wete
 * @date 10.11.2016
 * @description This is the Objectsystem's utilities to generate textual representation and Code of Objectsystem. 
 * 
 */
public class OsmlGeneratorUtils {
	val static String PROPERTY_NAME = "name"
	val static String NO_NAME = "NO_NAME"
	val static String NO_VALUE = "NO_VALUE"
	val static String NO_REF_NAME = "NO_REF_NAME"
	val static String NEW_LINE = "\n"
	val static String ESCAPED_NEW_LINE = "\\n"
	val static String QUOTE = '"'
	val static String STATE_GRAPH_FILE_EXT = ".stategraph"

	/**
	 * Get UML String of the EObject.
	 * @param rootObject the EObject.
	 */
	def static String generateStringUMLFromRootObject(EObject rootObject) {
		'''
			/*
				@startuml
				«rootObject.generateStringUMLFromObject»
				«rootObject.generateStringUMLReferencesFromObject»
				@enduml
			*/
		'''
	}

	/**
	 * Get DOT String of the EObject.
	 * @param rootObject the EObject.
	 */
	def static String generateStringDOTFromRootObject(EObject rootObject) {
		'''
			digraph {
				ratio=auto
				node [shape=record,color="#000000",style=filled,fillcolor="#fffdeb",fontcolor=black,fontsize=10,fontstyle=plain,fontname="helvetica"]
				edge [color="#000000",fontcolor=black,fontsize=10,fontstyle=plain,fontname="helvetica"]
				«rootObject.generateStringDOTFromObject»
				«rootObject.generateStringDOTReferencesFromObject»
			}
		'''
	}

	private def static String generateStringUMLReferencesFromObject(EObject rootObject) {
		'''
			«FOR eStru : rootObject.eClass.getEAllReferences.filter[r|r.isContainment]»
				«IF (rootObject.eGet(eStru)!=null)»
					«eStru.generateStringUMLFromReference(rootObject)»
				«ENDIF»
			«ENDFOR»
			«FOR eStru : rootObject.eClass.getEAllReferences.filter[r|!r.isContainment]»
				«IF (rootObject.eGet(eStru)!=null)»
					«eStru.generateStringUMLFromReference(rootObject)»
				«ENDIF»
			«ENDFOR»
		'''
	}

	private def static String generateStringDOTReferencesFromObject(EObject rootObject) {
		'''
			«FOR eStru : rootObject.eClass.getEAllReferences.filter[r|r.isContainment]»
				«IF (rootObject.eGet(eStru)!=null)»
					«eStru.generateStringDOTFromReference(rootObject)»
				«ENDIF»
			«ENDFOR»
			«FOR eStru : rootObject.eClass.getEAllReferences.filter[r|!r.isContainment]»
				«IF (rootObject.eGet(eStru)!=null)»
					«eStru.generateStringDOTFromReference(rootObject)»
				«ENDIF»
			«ENDFOR»
		'''
	}

	private def static generateStringUMLFromReference(EReference eStru, EObject object) {
		var String refName = eStru.name
		if (refName.isNullOrEmpty || refName.trim.isNullOrEmpty) {
			refName = NO_REF_NAME
		}
		val refValue = object.eGet(eStru)
		'''
			«IF refValue != null»
				«IF (refValue instanceof EList)»
					«FOR refValueCurrent : refValue»
						«IF refValueCurrent instanceof EObject»
							«IF eStru.isContainment»
								«refValueCurrent.generateStringUMLFromObject»
								«object.objectId»*--> "«refName»" «refValueCurrent.objectId»
								«refValueCurrent.generateStringUMLReferencesFromObject»
							«ELSE»
								«object.objectId»--> "«refName»" «refValueCurrent.objectId»
							«ENDIF»
						«ELSE»
							//"#B#"
						«ENDIF»
					«ENDFOR»
				«ELSEIF (refValue instanceof EObject)»
					«IF refValue instanceof EObject»
						«IF eStru.isContainment»
							«refValue.generateStringUMLFromObject»
							«object.objectId»*--> "«refName»" «refValue.objectId»
							«refValue.generateStringUMLReferencesFromObject»
						«ELSE»
							«object.objectId»--> "«refName»" «refValue.objectId»
						«ENDIF»
					«ELSE»
						//"*B*"
					«ENDIF»
				«ENDIF»
			«ELSE»
				reference «refName» : [NO_REF_VALUE]
			«ENDIF»
		'''
	}

	private def static generateStringDOTFromReference(EReference eStru, EObject object) {
		var String refName = eStru.name
		if (refName.isNullOrEmpty || refName.trim.isNullOrEmpty) {
			refName = NO_REF_NAME
		}
		val refValue = object.eGet(eStru)
		'''
			«IF refValue != null»
				«IF (refValue instanceof EList)»
					«FOR refValueCurrent : refValue»
						«IF refValueCurrent instanceof EObject»
							«IF eStru.isContainment»
								«refValueCurrent.generateStringDOTFromObject»
								«object.objectId» -> «refValueCurrent.objectId» [label = "«refName»", dir = both, arrowtail = diamond]
								«refValueCurrent.generateStringDOTReferencesFromObject»
							«ELSE»
								«object.objectId» -> «refValueCurrent.objectId» [label = "«refName»"]
							«ENDIF»
						«ELSE»
							//"#B#"
						«ENDIF»
					«ENDFOR»
				«ELSEIF (refValue instanceof EObject)»
					«IF refValue instanceof EObject»
						«IF eStru.isContainment»
							«refValue.generateStringDOTFromObject»
							«object.objectId» -> «refValue.objectId» [label = "«refName»", dir = both, arrowtail = diamond]
							«refValue.generateStringDOTReferencesFromObject»
						«ELSE»
							«object.objectId» -> «refValue.objectId» [label = "«refName»"]
						«ENDIF»
					«ELSE»
						//"*B*"
					«ENDIF»
				«ENDIF»
			«ELSE»
				reference «refName» : [NO_REF_VALUE]
			«ENDIF»
		'''
	}

	private def static String getObjectId(EObject object) {
		var String attrValue = null
		for (eAttr : object.eClass().getEAllAttributes().filter[a|a.name.equalsIgnoreCase(PROPERTY_NAME)]) {
			attrValue = object.eGet(eAttr) as String
			if (attrValue.isNullOrEmpty || attrValue.trim.isNullOrEmpty) {
				attrValue = NO_NAME
			}
		}
		if (attrValue == null) {
			attrValue = NO_REF_NAME
		}
		return attrValue
	}

	private def static String generateStringUMLFromObject(EObject object) {
		var String eClassName = object.eClass().getName()
		var String attrValue = null
		for (eAttr : object.eClass().getEAllAttributes().filter[a|a.name.equalsIgnoreCase(PROPERTY_NAME)]) {
			attrValue = object.eGet(eAttr) as String
			if (attrValue.isNullOrEmpty || attrValue.trim.isNullOrEmpty) {
				attrValue = NO_NAME
			}
		}
		if (attrValue == null) {
			attrValue = NO_REF_NAME
		}
		'''
			object «attrValue» {
				eClass = «eClassName»
				«FOR eAttr : object.eClass.getEAllAttributes.filter[a|!(a.name.equalsIgnoreCase(PROPERTY_NAME))]»
					«IF (object.eGet(eAttr)!=null)»
						«eAttr.generateStringUMLFromAttribute(object)»
					«ENDIF»
				«ENDFOR»
			}
		'''
	}

	private def static String generateStringDOTFromObject(EObject object) {
		var String eClassName = object.eClass().getName()
		var String attrValue = null
		for (eAttr : object.eClass().getEAllAttributes().filter[a|a.name.equalsIgnoreCase(PROPERTY_NAME)]) {
			attrValue = object.eGet(eAttr) as String
			if (attrValue.isNullOrEmpty || attrValue.trim.isNullOrEmpty) {
				attrValue = NO_NAME
			}
		}
		if (attrValue == null) {
			attrValue = NO_REF_NAME
		}
		var before = attrValue.concat("[label = ").concat(QUOTE).concat("{").concat(attrValue).concat(":").concat(
			eClassName).concat(" | ")
		var after = "}".concat(QUOTE).concat("]")
		var isFirst = true
		for (eAttr : object.eClass.getEAllAttributes.filter[a|!(a.name.equalsIgnoreCase(PROPERTY_NAME))]) {
			if (object.eGet(eAttr) != null) {
				if (!isFirst) {
					before = before.concat(OsmlGeneratorUtils.ESCAPED_NEW_LINE)
				} else {
					isFirst = false
				}
				before = before.concat(eAttr.generateStringDOTFromAttribute(object).toString.trim)
			}
		}
		before = before.concat(after)
		'''«before»'''
	}

	private def static generateStringUMLFromAttribute(EAttribute eAttr, EObject object) {
		val value = object.eGet(eAttr)
		var attrValue = NO_VALUE
		if (value != null) {
			attrValue = value.toString.trim
		}
		'''
			«eAttr.name» = «attrValue»
		'''
	}

	private def static generateStringDOTFromAttribute(EAttribute eAttr, EObject object) {
		val value = object.eGet(eAttr)
		var attrValue = NO_VALUE
		if (value != null) {
			attrValue = value.toString.trim
		}
		'''
			«eAttr.name» = «attrValue»
		'''
	}

	/**
	 * Get String (human readable) of the EObject for textual representation.
	 * @param object the EObject
	 */
	def static String generateStringFromObject(EObject object) {
		var String eClassName = object.eClass().getName()
		var String attrValue = null
		for (eAttr : object.eClass().getEAllAttributes().filter[a|a.name.equalsIgnoreCase(PROPERTY_NAME)]) {
			attrValue = object.eGet(eAttr) as String
			if (attrValue.isNullOrEmpty || attrValue.trim.isNullOrEmpty) {
				attrValue = NO_NAME
			}
		}
		if (attrValue == null) {
			attrValue = NO_REF_NAME
		}
		'''
			object «attrValue» : «eClassName» {
				«FOR eAttr : object.eClass.getEAllAttributes.filter[a|!(a.name.equalsIgnoreCase(PROPERTY_NAME))]»
					«IF (object.eGet(eAttr)!=null)»
						«eAttr.generateStringFromAttribute(object)»
					«ENDIF»
				«ENDFOR»
				«FOR eStru : object.eClass.getEAllReferences.filter[r|r.isContainment]»
					«IF (object.eGet(eStru)!=null)»
						«eStru.generateStringFromReference(object)»
					«ENDIF»
				«ENDFOR»
				«FOR eStru : object.eClass.getEAllReferences.filter[r|!r.isContainment]»
					«IF (object.eGet(eStru)!=null)»
						«eStru.generateStringFromReference(object)»
					«ENDIF»
				«ENDFOR»
			}
		'''
	}

	private def static generateStringFromAttribute(EAttribute eAttr, EObject object) {
		val value = object.eGet(eAttr)
		var attrValue = NO_VALUE
		if (value != null) {
			attrValue = value.toString.trim
		}
		'''
			attribute «eAttr.name» : [«attrValue»]
		'''
	}

	private def static generateStringFromReference(EReference eStru, EObject object) {
		var String refName = eStru.name
		if (refName.isNullOrEmpty || refName.trim.isNullOrEmpty) {
			refName = NO_REF_NAME
		}
		val refValue = object.eGet(
			eStru)
		'''
			«IF refValue != null»
				«IF (refValue instanceof EList)»
					«FOR refValueCurrent : refValue BEFORE "reference ".concat(refName).concat(" : [").concat(NEW_LINE) SEPARATOR "," AFTER "]" »
						«IF refValueCurrent instanceof EObject»
							«IF eStru.isContainment»
								«refValueCurrent.generateStringFromObject»
							«ELSE»
								«refValueCurrent.generateStringFromObjectReference»
							«ENDIF»
						«ELSE»
							//"#B#"
						«ENDIF»
					«ENDFOR»
				«ELSEIF (refValue instanceof EObject)»
					reference «refName» :[
					«IF refValue instanceof EObject»
						«IF eStru.isContainment»
							«refValue.generateStringFromObject»
						«ELSE»
							«refValue.generateStringFromObjectReference»
						«ENDIF»
					«ELSE»
						//"*B*"
					«ENDIF»
					]
				«ENDIF»
			«ELSE»
				reference «refName» : [NO_REF_VALUE]
			«ENDIF»
		'''
	}

	private def static String generateStringFromObjectReference(EObject refValue) {
		var String refValEClassName = refValue.eClass().getName()
		var String attrValue = null
		for (eAttr : refValue.eClass().getEAllAttributes().filter[a|a.name.equalsIgnoreCase(PROPERTY_NAME)]) {
			attrValue = refValue.eGet(eAttr) as String
			if (attrValue.isNullOrEmpty || attrValue.trim.isNullOrEmpty) {
				attrValue = NO_NAME
			}
		}
		if (attrValue == null) {
			attrValue = NO_REF_NAME
		}
		'''
			«attrValue» : «refValEClassName»
		'''
	}

	/**
	 * Get the current stateGraph file path.
	 */
	private def static IPath getCurrentStateGraphFileIPath() {
		val IWorkbench wb = PlatformUI.workbench
		if (wb == null) {
			return null
		}
		val IWorkbenchWindow window = wb.activeWorkbenchWindow
		if (window == null) {
			return null
		}
		for (page : window.pages) {
			for (editorRef : page.editorReferences) {
				val editorPart = editorRef.getEditor(false)
				val input = editorPart.editorInput
				if (input instanceof FileEditorInput) {
					val path = input.path
					if (path.toString.endsWith(STATE_GRAPH_FILE_EXT)) {
						return path
					}
				}
			}
		}
		val IWorkbenchPage page = window.activePage
		if (page == null) {
			return null
		}
		val IEditorPart editor = page.activeEditor
		if (editor == null) {
			return null
		}
		val IEditorInput input = editor.editorInput
		if (input == null) {
			return null
		}
		val IPath path = (input as FileEditorInput).path
		return path
	}

	/**
	 * Get the stateGraph file path of the rootElement passed as input.
	 * @param rootElt the EObject rootElt.
	 */
	def static IPath getCurrentStateGraphFileIPath(EObject rootElt) {
		val URI resourceURI = rootElt.eResource().getURI()
		val IPath path = new Path(resourceURI.toPlatformString(true))
		val IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(path)
		val IPath pathToCompareWith = file.getRawLocation()
		if (pathToCompareWith == null) {
			return getCurrentStateGraphFileIPath
		}
		return pathToCompareWith
	}
}
