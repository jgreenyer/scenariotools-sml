/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * ScenarioTools-URL: www.scenariotools.org
 *
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete
 */
package org.scenariotools.emf.objectmodellanguage.eoml


/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
class EOMLStandaloneSetup extends EOMLStandaloneSetupGenerated {

	def static void doSetup() {
		new EOMLStandaloneSetup().createInjectorAndDoEMFRegistration()
	}
}
