/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.sml.Message;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.ActiveConstraint#getConstraintMessageEvent <em>Constraint Message Event</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActiveConstraint#getActiveMessageParameters <em>Active Message Parameters</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActiveConstraint#getMessage <em>Message</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveConstraint()
 * @model abstract="true"
 * @generated
 */
public interface ActiveConstraint extends EObject {
	/**
	 * Returns the value of the '<em><b>Constraint Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraint Message Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraint Message Event</em>' reference.
	 * @see #setConstraintMessageEvent(MessageEvent)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveConstraint_ConstraintMessageEvent()
	 * @model
	 * @generated
	 */
	MessageEvent getConstraintMessageEvent();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.ActiveConstraint#getConstraintMessageEvent <em>Constraint Message Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constraint Message Event</em>' reference.
	 * @see #getConstraintMessageEvent()
	 * @generated
	 */
	void setConstraintMessageEvent(MessageEvent value);

	/**
	 * Returns the value of the '<em><b>Active Message Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.ActiveMessageParameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Message Parameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Message Parameters</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveConstraint_ActiveMessageParameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<ActiveMessageParameter> getActiveMessageParameters();

	/**
	 * Returns the value of the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message</em>' reference.
	 * @see #setMessage(Message)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveConstraint_Message()
	 * @model
	 * @generated
	 */
	Message getMessage();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.ActiveConstraint#getMessage <em>Message</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message</em>' reference.
	 * @see #getMessage()
	 * @generated
	 */
	void setMessage(Message value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void init(ActiveScenarioRoleBindings roleBindings, ActivePart parentActivePart, ActiveScenario activeScenario);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void updateConstraintEvent(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void addToParentSpecificConstraintList(ActiveInteraction parant, MessageEvent constraintMessage);

} // ActiveConstraint
