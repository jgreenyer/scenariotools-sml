/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multi Active Scenario Initializations</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.MultiActiveScenarioInitializations#getActiveScenarioToActiveScenarioProgressMap <em>Active Scenario To Active Scenario Progress Map</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getMultiActiveScenarioInitializations()
 * @model
 * @generated
 */
public interface MultiActiveScenarioInitializations extends EObject {
	/**
	 * Returns the value of the '<em><b>Active Scenario To Active Scenario Progress Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.sml.runtime.ActiveScenario},
	 * and the value is of type {@link org.scenariotools.sml.runtime.ActiveScenarioProgress},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Scenario To Active Scenario Progress Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Scenario To Active Scenario Progress Map</em>' map.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getMultiActiveScenarioInitializations_ActiveScenarioToActiveScenarioProgressMap()
	 * @model mapType="org.scenariotools.sml.runtime.ActiveScenarioToActiveScenarioProgressMapEntry&lt;org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.ActiveScenarioProgress&gt;"
	 * @generated
	 */
	EMap<ActiveScenario, ActiveScenarioProgress> getActiveScenarioToActiveScenarioProgressMap();

} // MultiActiveScenarioInitializations
