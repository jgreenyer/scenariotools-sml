/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.ETypedElement;
import org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * represents a message sent from a sending object to a receiving object at runtime.
 * If the referenced operation specifies a (Boolean, Integer, String, EObject) parameter,
 * the message can carry a corresponding value. Note that only operations/messages
 * with at most one parameter are supported.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.MessageEvent#getSendingObject <em>Sending Object</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.MessageEvent#getReceivingObject <em>Receiving Object</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.MessageEvent#getMessageName <em>Message Name</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.MessageEvent#isConcrete <em>Concrete</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.MessageEvent#isParameterized <em>Parameterized</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.MessageEvent#getTypedElement <em>Typed Element</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.MessageEvent#getParameterValues <em>Parameter Values</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.MessageEvent#getCollectionOperation <em>Collection Operation</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getMessageEvent()
 * @model
 * @generated
 */
public interface MessageEvent extends Event {
	/**
	 * Returns the value of the '<em><b>Sending Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sending Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sending Object</em>' reference.
	 * @see #setSendingObject(EObject)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getMessageEvent_SendingObject()
	 * @model
	 * @generated
	 */
	EObject getSendingObject();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.MessageEvent#getSendingObject <em>Sending Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sending Object</em>' reference.
	 * @see #getSendingObject()
	 * @generated
	 */
	void setSendingObject(EObject value);

	/**
	 * Returns the value of the '<em><b>Receiving Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Receiving Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Receiving Object</em>' reference.
	 * @see #setReceivingObject(EObject)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getMessageEvent_ReceivingObject()
	 * @model
	 * @generated
	 */
	EObject getReceivingObject();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.MessageEvent#getReceivingObject <em>Receiving Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Receiving Object</em>' reference.
	 * @see #getReceivingObject()
	 * @generated
	 */
	void setReceivingObject(EObject value);

	/**
	 * Returns the value of the '<em><b>Message Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Name</em>' attribute.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getMessageEvent_MessageName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getMessageName();

	/**
	 * Returns the value of the '<em><b>Concrete</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Is false if the referenced operation specifies a parameter, but the message does specify a value.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Concrete</em>' attribute.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getMessageEvent_Concrete()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	boolean isConcrete();

	/**
	 * Returns the value of the '<em><b>Parameterized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * true if the referenced operation specifies a parameter
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Parameterized</em>' attribute.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getMessageEvent_Parameterized()
	 * @model transient="true" changeable="false" volatile="true"
	 * @generated
	 */
	boolean isParameterized();

	/**
	 * Returns the value of the '<em><b>Typed Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Typed Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Typed Element</em>' reference.
	 * @see #setTypedElement(ETypedElement)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getMessageEvent_TypedElement()
	 * @model
	 * @generated
	 */
	ETypedElement getTypedElement();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.MessageEvent#getTypedElement <em>Typed Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Typed Element</em>' reference.
	 * @see #getTypedElement()
	 * @generated
	 */
	void setTypedElement(ETypedElement value);

	/**
	 * Returns the value of the '<em><b>Parameter Values</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.ParameterValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Values</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Values</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getMessageEvent_ParameterValues()
	 * @model containment="true"
	 * @generated
	 */
	EList<ParameterValue> getParameterValues();

	/**
	 * Returns the value of the '<em><b>Collection Operation</b></em>' attribute.
	 * The default value is <code>"isEmpty"</code>.
	 * The literals are from the enumeration {@link org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collection Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collection Operation</em>' attribute.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation
	 * @see #setCollectionOperation(CollectionOperation)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getMessageEvent_CollectionOperation()
	 * @model default="isEmpty"
	 * @generated
	 */
	CollectionOperation getCollectionOperation();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.MessageEvent#getCollectionOperation <em>Collection Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Collection Operation</em>' attribute.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation
	 * @see #getCollectionOperation()
	 * @generated
	 */
	void setCollectionOperation(CollectionOperation value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean eq(MessageEvent messageEvent);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isMessageUnifiableWith(MessageEvent messageEvent);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isParameterUnifiableWith(MessageEvent messageEvent);

} // MessageEvent
