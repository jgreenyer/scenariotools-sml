/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.util;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.ScenarioKind;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.Transition;
import org.scenariotools.sml.runtime.WaitEvent;

public class SMLRuntimeStateUtil {
	
	public static void markTransitionsAsViolating(SMLRuntimeState sourceState, SMLRuntimeState targetState) {
		for (Transition t : sourceState.getOutgoingTransition()) {
			SMLRuntimeState target = t.getTargetState();
			if (target.equals(targetState))
				setTransitionLeadsToGuaranteeSafetyViolation(t, Boolean.TRUE);
		}
	}
	
	public static void setTransitionLeadsToGuaranteeSafetyViolation(Transition t, Boolean b){
		t.getStringToBooleanAnnotationMap().put("leadToGuaranteeSafetyViolation", b);
	}
	public static boolean isTransitionLeadsToGuaranteeSafetyViolation(Transition t){
		Boolean b = t.getStringToBooleanAnnotationMap().get("leadToGuaranteeSafetyViolation");
		return b != null && b.booleanValue();
	}

	public static void setTransitionFromParent(SMLRuntimeState child, Transition transitionToParent){
		child.getStringToEObjectAnnotationMap().put("TransitionFromParent", transitionToParent);		
	}
	public static Transition getTransitionFromParent(SMLRuntimeState child){
		EObject transitionFromParent = child.getStringToEObjectAnnotationMap().get("TransitionFromParent");
		if (transitionFromParent == null || !(transitionFromParent instanceof Transition))
			return null;
		else
			return (Transition) transitionFromParent;
	}

	public static void setDistance(SMLRuntimeState state, int i) {
		state.getStringToStringAnnotationMap().put("distanceFromRoot", Integer.toString(i));
	}
	public static int getDistance(SMLRuntimeState state) {
		String distanceString = state.getStringToStringAnnotationMap().get("distanceFromRoot");
		if(distanceString == null){
			return -1;
		}
		int returnValue;
		try {
			returnValue = Integer.parseInt(distanceString);
		} catch (NumberFormatException e) {
			returnValue = -1;
		}
		return returnValue;
	}
	
	public static void setDistanceToNearestSafetyViolation(SMLRuntimeState state, int i) {
		state.getStringToStringAnnotationMap().put("distanceToNearestSafetyViolation", Integer.toString(i));
	}
	public static int getDistanceToNearestSafetyViolation(SMLRuntimeState state) {
		try {
			String distanceString = state.getStringToStringAnnotationMap().get("distanceToNearestSafetyViolation");
			int returnValue;
			returnValue = Integer.parseInt(distanceString);
			return returnValue;
		} catch (NumberFormatException e) {
			return -1;
		} catch (NullPointerException e) {
			return -1;
		}
	}
	
	public static boolean isSpanningTreeTransition(Transition t){
		Boolean isSpanningTreeTransition = t.getStringToBooleanAnnotationMap().get("spanningtree");
		return (isSpanningTreeTransition != null && isSpanningTreeTransition.booleanValue());
	}

	public static void setSpanningTreeTransition(Transition t, Boolean isSpanningTreeTransition){
		t.getStringToBooleanAnnotationMap().put("spanningtree", isSpanningTreeTransition);
	}

	public static boolean isStrategyState(SMLRuntimeState state){
		Boolean isStrategyState = state.getStringToBooleanAnnotationMap().get("strategy");
		return (isStrategyState != null && isStrategyState.booleanValue());
	}

	public static void setStrategyState(SMLRuntimeState state, Boolean isStrategyState){
		state.getStringToBooleanAnnotationMap().put("strategy", isStrategyState);
	}

	public static boolean isCounterStrategyState(SMLRuntimeState state){
		Boolean isCounterStrategyState = state.getStringToBooleanAnnotationMap().get("counterstrategy");
		return (isCounterStrategyState != null && isCounterStrategyState.booleanValue());
	}

	public static void setCounterStrategyState(SMLRuntimeState state, Boolean isCounterStrategyState){
		state.getStringToBooleanAnnotationMap().put("counterstrategy", isCounterStrategyState);
	}

	
	public static void setGoal(SMLRuntimeState state, Boolean goal){
		state.getStringToBooleanAnnotationMap().put("goal", goal);
	}
	
	public static void setWin(SMLRuntimeState state, Boolean win){
		state.getStringToBooleanAnnotationMap().put("win", win);
	}

	public static void setLose(SMLRuntimeState state, Boolean lose){
		state.getStringToBooleanAnnotationMap().put("lose", lose);
	}

	public static boolean isGoal(SMLRuntimeState state){
		Boolean isGoal = state.getStringToBooleanAnnotationMap().get("goal");
		return (isGoal != null && isGoal.booleanValue());
	}

	public static boolean isWin(SMLRuntimeState state){
		Boolean isWin = state.getStringToBooleanAnnotationMap().get("win");
		return (isWin != null && isWin.booleanValue());
	}

	public static boolean isLose(SMLRuntimeState state){
		Boolean isLose = state.getStringToBooleanAnnotationMap().get("lose");
		return (isLose != null && isLose.booleanValue());
	}

	public static void setPassedIndex(SMLRuntimeState state, String indexString){
		state.getStringToStringAnnotationMap().put("passedIndex", indexString);
	}

	public static String getPassedIndex(SMLRuntimeState state){
		return state.getStringToStringAnnotationMap().get("passedIndex");
	}

	public static void setIterationInWhichStateWasExplored(SMLRuntimeState state, String iterationString){
		state.getStringToStringAnnotationMap().put("iterationInWhichStateWasExplored", iterationString);
	}
	public static String getIterationInWhichStateWasExplored(SMLRuntimeState state){
		return state.getStringToStringAnnotationMap().get("iterationInWhichStateWasExplored");
	}

	/**
	 * Usees breadth-first search to calculate for each state the
	 * min/max distance to the next safety violating state.
	 * @param stateGraph 
	 */
	public static void annotateStrategyStates(SMLRuntimeStateGraph stateGraph, Set<SMLRuntimeState> strategyStates, boolean strategyExists) {
		
		// mark states of the strategy/counterstrategy.
		for (SMLRuntimeState state : strategyStates) {
			if(strategyExists){
				SMLRuntimeStateUtil.setStrategyState(state, Boolean.TRUE);
			}else{
				SMLRuntimeStateUtil.setCounterStrategyState(state, Boolean.TRUE);
			}
		}

		
		Set<SMLRuntimeState> safetyViolatingStates = new HashSet<SMLRuntimeState>();
		
		Queue<SMLRuntimeState> queue = new LinkedList<SMLRuntimeState>(); 
		
		SMLRuntimeState root = stateGraph.getStartState();
		SMLRuntimeStateUtil.setDistance(root, 0);
		queue.add(root);
		
		while(!queue.isEmpty()){
			SMLRuntimeState current = queue.poll();
			for (Transition t : current.getOutgoingTransition()) {
				SMLRuntimeState target = t.getTargetState();
				if (SMLRuntimeStateUtil.getDistance(target)<0){
					SMLRuntimeStateUtil.setDistance(target,SMLRuntimeStateUtil.getDistance(current)+1);
					SMLRuntimeStateUtil.setTransitionFromParent(target, t);
					SMLRuntimeStateUtil.setSpanningTreeTransition(t, Boolean.TRUE);
					if (strategyStates.contains(target)) // only explore further if it is part of the (counter)strategy.
						queue.add(target);
					if((false//target.isSafetyViolationOccurredInRequirements()
							|| target.isSafetyViolationOccurredInGuarantees()
							|| target.getOutgoingTransition().isEmpty() && getPassedIndex(target) != null
							)){
						safetyViolatingStates.add(target);
						//System.out.println("safetyViolatingState: " + getPassedIndex(target));
					}
				}
			}
		}
		
		for (SMLRuntimeState violatingState : safetyViolatingStates) {
			SMLRuntimeStateUtil.setDistanceToNearestSafetyViolation(violatingState, 0);
			
			SMLRuntimeState currentState = violatingState;

			while (currentState != null){
				Transition transitionFromParent = SMLRuntimeStateUtil.getTransitionFromParent(currentState);
				if (transitionFromParent != null){
					SMLRuntimeState parentState = transitionFromParent.getSourceState();
					int currentDistanceToNearestSafetyViolation = SMLRuntimeStateUtil.getDistanceToNearestSafetyViolation(currentState);
					int parentDistanceToNearestSafetyViolation = SMLRuntimeStateUtil.getDistanceToNearestSafetyViolation(parentState);
					if (parentDistanceToNearestSafetyViolation < 0 
							|| parentDistanceToNearestSafetyViolation > currentDistanceToNearestSafetyViolation + 1){
						SMLRuntimeStateUtil.setDistanceToNearestSafetyViolation(parentState, currentDistanceToNearestSafetyViolation + 1 );
						SMLRuntimeStateUtil.markTransitionsAsViolating(parentState, currentState);
						currentState = parentState;
					}else
						currentState = null;
				}else
					currentState = null;
			}
		}
		
	}
	
	public static void setDeadlockString(SMLRuntimeState state, String s){
		state.getStringToStringAnnotationMap().put("deadlock", s);
	}

	public static String getDeadlockString(SMLRuntimeState state){
		return state.getStringToStringAnnotationMap().get("deadlock");
	}

	public static void setSystemStepBlockedButAssumptionRequestedEventsString(SMLRuntimeState state, String s){
		state.getStringToStringAnnotationMap().put("SystemStepBlockedButAssumptionRequestedEvents", s);
	}

	public static String getSystemStepBlockedButAssumptionRequestedEventsString(SMLRuntimeState state){
		return state.getStringToStringAnnotationMap().get("SystemStepBlockedButAssumptionRequestedEvents");
	}

	public static void setSystemStepBlockedButAssumptionRequestedEvents(SMLRuntimeState state, Boolean isSystemStepBlocked){
		state.getStringToBooleanAnnotationMap().put("SystemStepBlockedButAssumptionRequestedEvents", isSystemStepBlocked);
	}

	public static void unsetSystemStepBlockedButAssumptionRequestedEvents(SMLRuntimeState state){
		state.getStringToBooleanAnnotationMap().remove("SystemStepBlockedButAssumptionRequestedEvents");
	}

	public static boolean isSystemStepBlockedButAssumptionRequestedEvents(SMLRuntimeState state){
		Boolean isSystemStepBlocked = state.getStringToBooleanAnnotationMap().get("SystemStepBlockedButAssumptionRequestedEvents");
		return (isSystemStepBlocked != null && isSystemStepBlocked.booleanValue());
	}

	
	public static String getBlockedGuaranteeScenarioInformation(SMLRuntimeState smlState){
		String annotationString = "";
		for (ActiveScenario activeScenario : smlState.getActiveScenarios()) {
			if(activeScenario.getScenario().getKind() == ScenarioKind.ASSUMPTION) 
				continue;
			for (MessageEvent messageEvent : activeScenario.getRequestedEvents()) {
				for (ActiveScenario activeScenario2 : smlState.getActiveScenarios()) {
					if(activeScenario2.getScenario().getKind() == ScenarioKind.ASSUMPTION) 
						continue;
					if(activeScenario2.isBlocked(messageEvent))
						annotationString += 
						"\n" + messageEvent.getMessageName().replaceAll("->", "_to_")
						+ "\n     REQUESTED by " + activeScenario.getScenario().getName() 
						+ "\n     BLOCKED   by " + activeScenario2.getScenario().getName();
				}
			}
		}
		return annotationString;
	}
	
	public static String getEObjectName(EObject eObject) {
		if (eObject == null)
			return "<null>";
		for (EAttribute eAttribute : eObject.eClass().getEAllAttributes()) {
			if ("name".equals(eAttribute.getName())
					&& eAttribute.getEType().getInstanceClass() == String.class) {
				return (String) eObject.eGet(eAttribute);
			}
		}
		return eObject.hashCode() + "";
	}
	
	public static Set<Transition> getControllableOutgoingTransitions(SMLRuntimeState state) {
		Set<Transition> result = new HashSet<Transition>();
		
		SMLObjectSystem objectSystem = state.getObjectSystem();
		
		for(Transition t : state.getOutgoingTransition()) {
			Event event = t.getEvent();
			if(!(event instanceof MessageEvent))
				continue;
			
			MessageEvent messageEvent = (MessageEvent)event;
			if(objectSystem.isControllable(messageEvent.getSendingObject()))
				result.add(t);
		}
		
		return result;
	}
	
	public static Set<Transition> getUncontrollableOutgoingTransitions(SMLRuntimeState state) {
		Set<Transition> result = new HashSet<Transition>(state.getOutgoingTransition());
		result.removeAll(getControllableOutgoingTransitions(state));
		
		return result;
	}	
	
	public static boolean isControllable(Transition transition) {
		Event event = transition.getEvent();
		
		if(event instanceof WaitEvent)
			return true;
		else if(event instanceof MessageEvent)
			return transition.getSourceState().getObjectSystem().isControllable(((MessageEvent) transition.getEvent()).getSendingObject());
		else
			return false;
	}
}
