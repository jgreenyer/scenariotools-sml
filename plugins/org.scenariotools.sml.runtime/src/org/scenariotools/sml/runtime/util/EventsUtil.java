/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.util;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;


public class EventsUtil {

	public static final EObject nullEObject = EcoreFactory.eINSTANCE.createEObject();
	
	public static String getName(EObject eObject){
		if(eObject != null){
			EClass eClass = eObject.eClass();
			EStructuralFeature nameFeature = eClass.getEStructuralFeature("name");
			if (nameFeature != null && "EString".equals(nameFeature.getEType().getName()))
				return (String) eObject.eGet(nameFeature);
			else
				return eObject.hashCode() + "";				
		}
		return "<null>";
	}
	
//	public static Type umlPrimitiveStringType;
//	public static Type umlPrimitiveIntegerType;
//	public static Type umlPrimitiveBooleanType;
//	
//	public static Type getUMLPrimitiveStringType(){
//		if (umlPrimitiveStringType == null){
//			umlPrimitiveStringType = getUMLPrimitiveType("String");
//		}
//		return umlPrimitiveStringType;
//	}
//
//	public static Type getUMLPrimitiveIntegerType(){
//		if (umlPrimitiveIntegerType == null){
//			umlPrimitiveIntegerType = getUMLPrimitiveType("Integer");
//		}
//		return umlPrimitiveIntegerType;
//	}
//
//	public static Type getUMLPrimitiveBooleanType(){
//		if (umlPrimitiveBooleanType == null){
//			umlPrimitiveBooleanType = getUMLPrimitiveType("Boolean");
//		}
//		return umlPrimitiveBooleanType;
//	}
	
	public static boolean isStringType(EClassifier type){
		return "EString".equals(type.getName());
//		return (type.equals(getUMLPrimitiveStringType()));
	}
	public static boolean isIntegerType(EClassifier type){
		return "EInteger".equals(type.getName()) || "EInt".equals(type.getName());
//		System.out.println(type);
//		System.out.println(getUMLPrimitiveIntegerType());
//		return (type.equals(getUMLPrimitiveIntegerType()));
	}
	public static boolean isBooleanType(EClassifier type){
		return "EBoolean".equals(type.getName());
//		return (type.equals(getUMLPrimitiveBooleanType()));
	}
	
}
