package org.scenariotools.sml.runtime.util;

import org.scenariotools.sml.runtime.Transition;

public class TransitionUtil {
	
	public static void setPassedIndex(Transition transition, String indexString){
		transition.getStringToStringAnnotationMap().put("passedIndex", indexString);
	}

	public static String getPassedIndex(Transition transition){
		return transition.getStringToStringAnnotationMap().get("passedIndex");
	}

	
	public static void setIterationInWhichTransitionWasExplored(Transition transition, String iterationString){
		transition.getStringToStringAnnotationMap().put("iterationInWhichStateWasExplored", iterationString);
	}
	public static String getIterationInWhichTransitionWasExplored(Transition transition){
		return transition.getStringToStringAnnotationMap().get("iterationInWhichStateWasExplored");
	}

	private static String attractorStrategyKeyString = "attractorStrategy";
	public static void appendAttractorStrategyInformation(Transition transition, String string) {
		String existingEntry = transition.getStringToStringAnnotationMap().get(attractorStrategyKeyString);
		if (existingEntry == null) {
			transition.getStringToStringAnnotationMap().put(attractorStrategyKeyString, string);
		}else {
			transition.getStringToStringAnnotationMap().put(attractorStrategyKeyString, existingEntry + string);
		}
	}
	public static String getAttractorStrategyInformation(Transition transition){
		return transition.getStringToStringAnnotationMap().get(attractorStrategyKeyString);
	}

	
	private static String isStrategyTransitionKeyString = "isStrategyTransition";
	public static void setStrategyTransition(Transition transition, boolean isStrategyTransition){
		transition.getStringToBooleanAnnotationMap().put(isStrategyTransitionKeyString, new Boolean(isStrategyTransition));
	}
	public static boolean isStrategyTransition(Transition transition){
		Boolean result = transition.getStringToBooleanAnnotationMap().get(isStrategyTransitionKeyString);
		if (result == null)
			return false;
		else return result;
	}
	
	private static String isCounterStrategyTransitionKeyString = "isCounterStrategyTransition";
	public static void setCounterStrategyTransition(Transition transition, boolean isStrategyTransition){
		transition.getStringToBooleanAnnotationMap().put(isCounterStrategyTransitionKeyString, new Boolean(isStrategyTransition));
	}
	public static boolean isCounterStrategyTransition(Transition transition){
		Boolean result = transition.getStringToBooleanAnnotationMap().get(isCounterStrategyTransitionKeyString);
		if (result == null)
			return false;
		else return result;
	}
	
	private static String isControllableTransitionKeyString = "isControllableTransition";
	public static void setControllableTransition(Transition transition, boolean isControllable){
		transition.getStringToBooleanAnnotationMap().put(isControllableTransitionKeyString, new Boolean(isControllable));
	}
	public static boolean isControllableTransition(Transition transition){
		Boolean result = transition.getStringToBooleanAnnotationMap().get(isControllableTransitionKeyString);
		if (result == null)
			return false;
		else return result;
	}
	public static boolean isSetControllableTransition(Transition transition){
		return transition.getStringToBooleanAnnotationMap().get(isControllableTransitionKeyString) != null;
	}
	
	

}
