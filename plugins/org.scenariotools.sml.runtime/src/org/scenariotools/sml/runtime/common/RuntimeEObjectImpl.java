/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.common;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;

public class RuntimeEObjectImpl extends EObjectImpl {

	  /*
	   * Javadoc copied from interface.
	   */
	  @Override
	  public EList<Adapter> eAdapters()
	  {
	    if (eAdapters == null)
	    {
	      eAdapters =  new RuntimeEAdapterList<Adapter>(this);
	    }
	    return eAdapters;
	  }
	  
	  public static class RuntimeEAdapterList<E extends Object & Adapter> extends EAdapterList{

		public RuntimeEAdapterList(Notifier arg0) {
			super(arg0);
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public boolean add(Object object) {
			if (object instanceof ECrossReferenceAdapter)
				return false;
			return super.add(object);
		}
		
		@Override
		public void add(int index, Object object) {
			if (object instanceof ECrossReferenceAdapter)
				return;
			super.add(index, object);
		}
		
//		@Override
//		public boolean addAll(Collection collection) {
//			for (Object object : collection) {
//				if (object instanceof ECrossReferenceAdapter)
//					System.out.println("fff");
//			}
//			// TODO Auto-generated method stub
//			return super.addAll(collection);
//		}
//		
//		@Override
//		public boolean addAll(int index, Collection collection) {
//			for (Object object : collection) {
//				if (object instanceof ECrossReferenceAdapter)
//					System.out.println("fff");
//			}
//			return super.addAll(index, collection);
//		}
//		
//		@Override
//		public boolean addAllUnique(Collection collection) {
//			for (Object object : collection) {
//				if (object instanceof ECrossReferenceAdapter)
//					System.out.println("fff");
//			}
//			return super.addAllUnique(collection);
//		}
//		
//		@Override
//		public boolean addAllUnique(int index, Collection collection) {
//			for (Object object : collection) {
//				if (object instanceof ECrossReferenceAdapter)
//					System.out.println("fff");
//			}
//			return super.addAllUnique(index, collection);
//		}
//		
//		@Override
//		public boolean addAllUnique(int index, Object[] objects, int start,
//				int end) {
//			for (int i = 0; i < objects.length; i++) {
//				Object object = objects[i];
//				if (object instanceof ECrossReferenceAdapter)
//					System.out.println("fff");
//				
//			}
//
//			return super.addAllUnique(index, objects, start, end);
//		}
//		
//		@Override
//		public boolean addAllUnique(Object[] objects, int start, int end) {
//			for (int i = 0; i < objects.length; i++) {
//				Object object = objects[i];
//				if (object instanceof ECrossReferenceAdapter)
//					System.out.println("fff");
//				
//			}
//			return super.addAllUnique(objects, start, end);
//		}
//		
//		@Override
//		public void addListener(Listener listener) {
//			if (listener instanceof ECrossReferenceAdapter)
//				return;
//			super.addListener(listener);
//		}
//		
//		@Override
//		public void addUnique(Object object) {
//			if (object instanceof ECrossReferenceAdapter)
//				return;
//			super.addUnique(object);
//		}
//		
//		@Override
//		public void addUnique(int index, Object object) {
//			if (object instanceof ECrossReferenceAdapter)
//				return;
//			super.addUnique(index, object);
//		}
				
	  }
	
}
