/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.StringParameterValue#getStringParameterValue <em>String Parameter Value</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getStringParameterValue()
 * @model
 * @generated
 */
public interface StringParameterValue extends ParameterValue {
	/**
	 * Returns the value of the '<em><b>String Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String Parameter Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String Parameter Value</em>' attribute.
	 * @see #isSetStringParameterValue()
	 * @see #unsetStringParameterValue()
	 * @see #setStringParameterValue(String)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getStringParameterValue_StringParameterValue()
	 * @model unsettable="true"
	 * @generated
	 */
	String getStringParameterValue();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.StringParameterValue#getStringParameterValue <em>String Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>String Parameter Value</em>' attribute.
	 * @see #isSetStringParameterValue()
	 * @see #unsetStringParameterValue()
	 * @see #getStringParameterValue()
	 * @generated
	 */
	void setStringParameterValue(String value);

	/**
	 * Unsets the value of the '{@link org.scenariotools.sml.runtime.StringParameterValue#getStringParameterValue <em>String Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetStringParameterValue()
	 * @see #getStringParameterValue()
	 * @see #setStringParameterValue(String)
	 * @generated
	 */
	void unsetStringParameterValue();

	/**
	 * Returns whether the value of the '{@link org.scenariotools.sml.runtime.StringParameterValue#getStringParameterValue <em>String Parameter Value</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>String Parameter Value</em>' attribute is set.
	 * @see #unsetStringParameterValue()
	 * @see #getStringParameterValue()
	 * @see #setStringParameterValue(String)
	 * @generated
	 */
	boolean isSetStringParameterValue();

} // StringParameterValue
