/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;

import org.scenariotools.sml.VariableBindingParameterExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Message Parameter With Bind To Var</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithBindToVar#getParameter <em>Parameter</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveMessageParameterWithBindToVar()
 * @model
 * @generated
 */
public interface ActiveMessageParameterWithBindToVar extends ActiveMessageParameter {
	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' reference.
	 * @see #setParameter(VariableBindingParameterExpression)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveMessageParameterWithBindToVar_Parameter()
	 * @model
	 * @generated
	 */
	VariableBindingParameterExpression getParameter();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithBindToVar#getParameter <em>Parameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter</em>' reference.
	 * @see #getParameter()
	 * @generated
	 */
	void setParameter(VariableBindingParameterExpression value);

} // ActiveMessageParameterWithBindToVar
