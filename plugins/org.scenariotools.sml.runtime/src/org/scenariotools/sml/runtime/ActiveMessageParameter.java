/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Message Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveMessageParameter()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ActiveMessageParameter extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void init(ParameterValue parameterValue);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void update(ParameterValue parameterValue, ActivePart parent, ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean hasSideEffectsOnUnification();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean executeSideEffectsOnUnification(ParameterValue parameterValueFromOccuredMessage, ParameterValue parameterValue, ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState);

} // ActiveMessageParameter
