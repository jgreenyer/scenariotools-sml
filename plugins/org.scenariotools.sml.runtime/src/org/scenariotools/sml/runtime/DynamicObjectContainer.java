/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dynamic Object Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.DynamicObjectContainer#getStaticEObjectToDynamicEObjectMap <em>Static EObject To Dynamic EObject Map</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.DynamicObjectContainer#getRootObjects <em>Root Objects</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getDynamicObjectContainer()
 * @model
 * @generated
 */
public interface DynamicObjectContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Static EObject To Dynamic EObject Map</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.ecore.EObject},
	 * and the value is of type {@link org.eclipse.emf.ecore.EObject},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Static EObject To Dynamic EObject Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Static EObject To Dynamic EObject Map</em>' map.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getDynamicObjectContainer_StaticEObjectToDynamicEObjectMap()
	 * @model mapType="org.scenariotools.sml.runtime.StaticEObjectToDynamicEObjectMapEntry&lt;org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EObject&gt;"
	 * @generated
	 */
	EMap<EObject, EObject> getStaticEObjectToDynamicEObjectMap();

	/**
	 * Returns the value of the '<em><b>Root Objects</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Objects</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Objects</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getDynamicObjectContainer_RootObjects()
	 * @model containment="true"
	 * @generated
	 */
	EList<EObject> getRootObjects();

} // DynamicObjectContainer
