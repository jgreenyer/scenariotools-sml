/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.sml.runtime.keywrapper.ActiveInteractionKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.ActiveScenarioRoleBindingsKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.ObjectSystemKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.StateKeyWrapper;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * <p>Container for model objects shared by multiple runtime states and provides methods for recognizing duplicate objects.
 * Clients should not directly manipulate the exposed lists. Instead, they should invoke the various getXXX operations.<p>
 * 
 * <p>Some objects do not change regularly during state updates. For instance,  DynamicObjectSystemContainer objects only change, if a message event occurs, that has side effects.
 * When a statespace contains only a few transitions with side-effects-causing message events, it makes sense to store only the distinct DynamicObjectContainer instances.</p>
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.ElementContainer#getActiveScenarios <em>Active Scenarios</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ElementContainer#getActiveInteractions <em>Active Interactions</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ElementContainer#getObjectSystems <em>Object Systems</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ElementContainer#getActiveScenarioRoleBindings <em>Active Scenario Role Bindings</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ElementContainer#getDynamicObjectContainer <em>Dynamic Object Container</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ElementContainer#getActiveInteractionKeyWrapperToActiveInteractionMap <em>Active Interaction Key Wrapper To Active Interaction Map</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ElementContainer#getActiveScenarioKeyWrapperToActiveScenarioMap <em>Active Scenario Key Wrapper To Active Scenario Map</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ElementContainer#getObjectSystemKeyWrapperToObjectSystemMap <em>Object System Key Wrapper To Object System Map</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ElementContainer#getObjectSystemKeyWrapperToDynamicObjectContainerMap <em>Object System Key Wrapper To Dynamic Object Container Map</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ElementContainer#getStateKeyWrapperToStateMap <em>State Key Wrapper To State Map</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ElementContainer#getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap <em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ElementContainer#isEnabled <em>Enabled</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ElementContainer#getWaitEvent <em>Wait Event</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getElementContainer()
 * @model
 * @generated
 */
public interface ElementContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Active Scenarios</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.ActiveScenario}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Scenarios</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Scenarios</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getElementContainer_ActiveScenarios()
	 * @model containment="true"
	 * @generated
	 */
	EList<ActiveScenario> getActiveScenarios();

	/**
	 * Returns the value of the '<em><b>Active Interactions</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.ActivePart}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Interactions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Interactions</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getElementContainer_ActiveInteractions()
	 * @model containment="true"
	 * @generated
	 */
	EList<ActivePart> getActiveInteractions();

	/**
	 * Returns the value of the '<em><b>Object Systems</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.SMLObjectSystem}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Systems</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Systems</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getElementContainer_ObjectSystems()
	 * @model containment="true"
	 * @generated
	 */
	EList<SMLObjectSystem> getObjectSystems();

	/**
	 * Returns the value of the '<em><b>Active Scenario Role Bindings</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.ActiveScenarioRoleBindings}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Scenario Role Bindings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Scenario Role Bindings</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getElementContainer_ActiveScenarioRoleBindings()
	 * @model containment="true"
	 * @generated
	 */
	EList<ActiveScenarioRoleBindings> getActiveScenarioRoleBindings();

	/**
	 * Returns the value of the '<em><b>Dynamic Object Container</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.DynamicObjectContainer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dynamic Object Container</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dynamic Object Container</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getElementContainer_DynamicObjectContainer()
	 * @model containment="true"
	 * @generated
	 */
	EList<DynamicObjectContainer> getDynamicObjectContainer();

	/**
	 * Returns the value of the '<em><b>Active Interaction Key Wrapper To Active Interaction Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.sml.runtime.keywrapper.ActiveInteractionKeyWrapper},
	 * and the value is of type {@link org.scenariotools.sml.runtime.ActivePart},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Interaction Key Wrapper To Active Interaction Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Interaction Key Wrapper To Active Interaction Map</em>' map.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getElementContainer_ActiveInteractionKeyWrapperToActiveInteractionMap()
	 * @model mapType="org.scenariotools.sml.runtime.ActiveInteractionKeyWrapperToActiveInteractionMapEntry&lt;org.scenariotools.sml.runtime.ActiveInteractionKeyWrapper, org.scenariotools.sml.runtime.ActivePart&gt;" transient="true"
	 * @generated
	 */
	EMap<ActiveInteractionKeyWrapper, ActivePart> getActiveInteractionKeyWrapperToActiveInteractionMap();

	/**
	 * Returns the value of the '<em><b>Active Scenario Key Wrapper To Active Scenario Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper},
	 * and the value is of type {@link org.scenariotools.sml.runtime.ActiveScenario},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Scenario Key Wrapper To Active Scenario Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Scenario Key Wrapper To Active Scenario Map</em>' map.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getElementContainer_ActiveScenarioKeyWrapperToActiveScenarioMap()
	 * @model mapType="org.scenariotools.sml.runtime.ActiveScenarioKeyWrapperToActiveScenarioMapEntry&lt;org.scenariotools.sml.runtime.ActiveScenarioKeyWrapper, org.scenariotools.sml.runtime.ActiveScenario&gt;" transient="true"
	 * @generated
	 */
	EMap<ActiveScenarioKeyWrapper, ActiveScenario> getActiveScenarioKeyWrapperToActiveScenarioMap();

	/**
	 * Returns the value of the '<em><b>Object System Key Wrapper To Object System Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.sml.runtime.keywrapper.ObjectSystemKeyWrapper},
	 * and the value is of type {@link org.scenariotools.sml.runtime.SMLObjectSystem},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object System Key Wrapper To Object System Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object System Key Wrapper To Object System Map</em>' map.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getElementContainer_ObjectSystemKeyWrapperToObjectSystemMap()
	 * @model mapType="org.scenariotools.sml.runtime.ObjectSystemKeyWrapperToObjectSystemMapEntry&lt;org.scenariotools.sml.runtime.ObjectSystemKeyWrapper, org.scenariotools.sml.runtime.SMLObjectSystem&gt;" transient="true"
	 * @generated
	 */
	EMap<ObjectSystemKeyWrapper, SMLObjectSystem> getObjectSystemKeyWrapperToObjectSystemMap();

	/**
	 * Returns the value of the '<em><b>Object System Key Wrapper To Dynamic Object Container Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.sml.runtime.keywrapper.ObjectSystemKeyWrapper},
	 * and the value is of type {@link org.scenariotools.sml.runtime.DynamicObjectContainer},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object System Key Wrapper To Dynamic Object Container Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object System Key Wrapper To Dynamic Object Container Map</em>' map.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getElementContainer_ObjectSystemKeyWrapperToDynamicObjectContainerMap()
	 * @model mapType="org.scenariotools.sml.runtime.ObjectSystemKeyWrapperToDynamicObjectContainerMapEntry&lt;org.scenariotools.sml.runtime.ObjectSystemKeyWrapper, org.scenariotools.sml.runtime.DynamicObjectContainer&gt;" transient="true"
	 * @generated
	 */
	EMap<ObjectSystemKeyWrapper, DynamicObjectContainer> getObjectSystemKeyWrapperToDynamicObjectContainerMap();

	/**
	 * Returns the value of the '<em><b>State Key Wrapper To State Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.sml.runtime.keywrapper.StateKeyWrapper},
	 * and the value is of type {@link org.scenariotools.sml.runtime.SMLRuntimeState},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State Key Wrapper To State Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State Key Wrapper To State Map</em>' map.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getElementContainer_StateKeyWrapperToStateMap()
	 * @model mapType="org.scenariotools.sml.runtime.StateKeyWrapperToStateMapEntry&lt;org.scenariotools.sml.runtime.StateKeyWrapper, org.scenariotools.sml.runtime.SMLRuntimeState&gt;" transient="true"
	 * @generated
	 */
	EMap<StateKeyWrapper, SMLRuntimeState> getStateKeyWrapperToStateMap();

	/**
	 * Returns the value of the '<em><b>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.sml.runtime.keywrapper.ActiveScenarioRoleBindingsKeyWrapper},
	 * and the value is of type {@link org.scenariotools.sml.runtime.ActiveScenarioRoleBindings},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map</em>' map.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getElementContainer_ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap()
	 * @model mapType="org.scenariotools.sml.runtime.ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry&lt;org.scenariotools.sml.runtime.ActiveScenarioRoleBindingsKeyWrapper, org.scenariotools.sml.runtime.ActiveScenarioRoleBindings&gt;" transient="true"
	 * @generated
	 */
	EMap<ActiveScenarioRoleBindingsKeyWrapper, ActiveScenarioRoleBindings> getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap();

	/**
	 * Returns the value of the '<em><b>Enabled</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enabled</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enabled</em>' attribute.
	 * @see #setEnabled(boolean)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getElementContainer_Enabled()
	 * @model default="true"
	 * @generated
	 */
	boolean isEnabled();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.ElementContainer#isEnabled <em>Enabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enabled</em>' attribute.
	 * @see #isEnabled()
	 * @generated
	 */
	void setEnabled(boolean value);

	/**
	 * Returns the value of the '<em><b>Wait Event</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wait Event</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wait Event</em>' containment reference.
	 * @see #setWaitEvent(WaitEvent)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getElementContainer_WaitEvent()
	 * @model containment="true"
	 * @generated
	 */
	WaitEvent getWaitEvent();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.ElementContainer#getWaitEvent <em>Wait Event</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wait Event</em>' containment reference.
	 * @see #getWaitEvent()
	 * @generated
	 */
	void setWaitEvent(WaitEvent value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	ActiveScenario getActiveScenario(ActiveScenario activeScenario);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	SMLObjectSystem getObjectSystem(SMLObjectSystem smlObjectSystem);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	SMLRuntimeState getSMLRuntimeState(SMLRuntimeState smlRuntimeState);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	ActiveScenarioRoleBindings getActiveScenarioRoleBindings(ActiveScenarioRoleBindings activeScenarioRoleBindings);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	DynamicObjectContainer getDynamicObjectContainer(DynamicObjectContainer dynamicObjectContainer, SMLObjectSystem objectSystem);

} // ElementContainer



