/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.Transition#getSourceState <em>Source State</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.Transition#getTargetState <em>Target State</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.Transition#getEvent <em>Event</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.Transition#getLabel <em>Label</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getTransition()
 * @model
 * @generated
 */
public interface Transition extends AnnotatableElement {
	/**
	 * Returns the value of the '<em><b>Source State</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getOutgoingTransition <em>Outgoing Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source State</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source State</em>' container reference.
	 * @see #setSourceState(SMLRuntimeState)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getTransition_SourceState()
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#getOutgoingTransition
	 * @model opposite="outgoingTransition" transient="false"
	 * @generated
	 */
	SMLRuntimeState getSourceState();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.Transition#getSourceState <em>Source State</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source State</em>' container reference.
	 * @see #getSourceState()
	 * @generated
	 */
	void setSourceState(SMLRuntimeState value);

	/**
	 * Returns the value of the '<em><b>Target State</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getIncomingTransition <em>Incoming Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target State</em>' reference.
	 * @see #setTargetState(SMLRuntimeState)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getTransition_TargetState()
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#getIncomingTransition
	 * @model opposite="incomingTransition"
	 * @generated
	 */
	SMLRuntimeState getTargetState();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.Transition#getTargetState <em>Target State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target State</em>' reference.
	 * @see #getTargetState()
	 * @generated
	 */
	void setTargetState(SMLRuntimeState value);

	/**
	 * Returns the value of the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event</em>' reference.
	 * @see #setEvent(Event)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getTransition_Event()
	 * @model
	 * @generated
	 */
	Event getEvent();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.Transition#getEvent <em>Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event</em>' reference.
	 * @see #getEvent()
	 * @generated
	 */
	void setEvent(Event value);

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getTransition_Label()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getLabel();

} // Transition
