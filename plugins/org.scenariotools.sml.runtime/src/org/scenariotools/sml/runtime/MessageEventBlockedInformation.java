/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message Event Blocked Information</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.MessageEventBlockedInformation#getDescription <em>Description</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.MessageEventBlockedInformation#getMessageEventString <em>Message Event String</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getMessageEventBlockedInformation()
 * @model
 * @generated
 */
public interface MessageEventBlockedInformation extends EObject {
	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getMessageEventBlockedInformation_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.MessageEventBlockedInformation#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Message Event String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Event String</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Event String</em>' attribute.
	 * @see #setMessageEventString(String)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getMessageEventBlockedInformation_MessageEventString()
	 * @model
	 * @generated
	 */
	String getMessageEventString();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.MessageEventBlockedInformation#getMessageEventString <em>Message Event String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Event String</em>' attribute.
	 * @see #getMessageEventString()
	 * @generated
	 */
	void setMessageEventString(String value);

} // MessageEventBlockedInformation
