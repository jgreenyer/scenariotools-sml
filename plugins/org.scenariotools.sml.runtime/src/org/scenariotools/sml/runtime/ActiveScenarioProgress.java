/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Active Scenario Progress</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveScenarioProgress()
 * @model
 * @generated
 */
public enum ActiveScenarioProgress implements Enumerator {
	/**
	 * The '<em><b>PROGRESS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PROGRESS_VALUE
	 * @generated
	 * @ordered
	 */
	PROGRESS(0, "PROGRESS", "PROGRESS"),

	/**
	 * The '<em><b>NO PROGRESS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NO_PROGRESS_VALUE
	 * @generated
	 * @ordered
	 */
	NO_PROGRESS(1, "NO_PROGRESS", "NO_PROGRESS"),

	/**
	 * The '<em><b>INTERACTION END</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTERACTION_END_VALUE
	 * @generated
	 * @ordered
	 */
	INTERACTION_END(2, "INTERACTION_END", "INTERACTION_END"),

	/**
	 * The '<em><b>COLD VIOLATION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COLD_VIOLATION_VALUE
	 * @generated
	 * @ordered
	 */
	COLD_VIOLATION(3, "COLD_VIOLATION", "COLD_VIOLATION"),

	/**
	 * The '<em><b>SAFETY VIOLATION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAFETY_VIOLATION_VALUE
	 * @generated
	 * @ordered
	 */
	SAFETY_VIOLATION(4, "SAFETY_VIOLATION", "SAFETY_VIOLATION"), /**
	 * The '<em><b>CONTINUE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONTINUE_VALUE
	 * @generated
	 * @ordered
	 */
	CONTINUE(5, "CONTINUE", "CONTINUE"), /**
	 * The '<em><b>STEP OVER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STEP_OVER_VALUE
	 * @generated
	 * @ordered
	 */
	STEP_OVER(6, "STEP_OVER", "STEP_OVER"), /**
	 * The '<em><b>MESSAGE PROGRESSED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MESSAGE_PROGRESSED_VALUE
	 * @generated
	 * @ordered
	 */
	MESSAGE_PROGRESSED(7, "MESSAGE_PROGRESSED", "MESSAGE_PROGRESSED");

	/**
	 * The '<em><b>PROGRESS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PROGRESS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PROGRESS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PROGRESS_VALUE = 0;

	/**
	 * The '<em><b>NO PROGRESS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NO PROGRESS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NO_PROGRESS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NO_PROGRESS_VALUE = 1;

	/**
	 * The '<em><b>INTERACTION END</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>INTERACTION END</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTERACTION_END
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int INTERACTION_END_VALUE = 2;

	/**
	 * The '<em><b>COLD VIOLATION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>COLD VIOLATION</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #COLD_VIOLATION
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int COLD_VIOLATION_VALUE = 3;

	/**
	 * The '<em><b>SAFETY VIOLATION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SAFETY VIOLATION</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SAFETY_VIOLATION
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SAFETY_VIOLATION_VALUE = 4;

	/**
	 * The '<em><b>CONTINUE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CONTINUE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONTINUE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CONTINUE_VALUE = 5;

	/**
	 * The '<em><b>STEP OVER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>STEP OVER</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STEP_OVER
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int STEP_OVER_VALUE = 6;

	/**
	 * The '<em><b>MESSAGE PROGRESSED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MESSAGE PROGRESSED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MESSAGE_PROGRESSED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MESSAGE_PROGRESSED_VALUE = 7;

	/**
	 * An array of all the '<em><b>Active Scenario Progress</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ActiveScenarioProgress[] VALUES_ARRAY =
		new ActiveScenarioProgress[] {
			PROGRESS,
			NO_PROGRESS,
			INTERACTION_END,
			COLD_VIOLATION,
			SAFETY_VIOLATION,
			CONTINUE,
			STEP_OVER,
			MESSAGE_PROGRESSED,
		};

	/**
	 * A public read-only list of all the '<em><b>Active Scenario Progress</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ActiveScenarioProgress> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Active Scenario Progress</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ActiveScenarioProgress get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ActiveScenarioProgress result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Active Scenario Progress</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ActiveScenarioProgress getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ActiveScenarioProgress result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Active Scenario Progress</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ActiveScenarioProgress get(int value) {
		switch (value) {
			case PROGRESS_VALUE: return PROGRESS;
			case NO_PROGRESS_VALUE: return NO_PROGRESS;
			case INTERACTION_END_VALUE: return INTERACTION_END;
			case COLD_VIOLATION_VALUE: return COLD_VIOLATION;
			case SAFETY_VIOLATION_VALUE: return SAFETY_VIOLATION;
			case CONTINUE_VALUE: return CONTINUE;
			case STEP_OVER_VALUE: return STEP_OVER;
			case MESSAGE_PROGRESSED_VALUE: return MESSAGE_PROGRESSED;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ActiveScenarioProgress(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ActiveScenarioProgress
