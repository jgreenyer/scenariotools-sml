/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.ecore.EObject;

import org.scenariotools.sml.expressions.scenarioExpressions.Variable;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Provides values for variables occurring in expressions.
 * <!-- end-model-doc -->
 *
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getContext()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Context extends EObject {
	/**
	 * <!-- begin-user-doc --> Provides the value of a variable, or the constant
	 * {@link Context#UNDEFINED UNDEFINED}. <!--
	 * end-user-doc -->
	 * @model
	 * @generated
	 */
	Object getValue(Variable variable);
	/**
	 * Value of any variable that is not defined in a context.
	 */
	static final Object UNDEFINED = new Object() {
		@Override
		public String toString() {
			return "UNDEFINED";
		}
	};
} // Context
