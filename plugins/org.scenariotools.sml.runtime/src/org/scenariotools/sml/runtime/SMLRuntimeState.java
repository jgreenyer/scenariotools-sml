/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.scenariotools.sml.Scenario;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SML Runtime State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * <p>An SMLRuntimeState is constituted by the active copies of SML scenarios
 * as well as a {@link #getDynamicObjectContainer copy of the object system}, 
 * that is updated through message event side effects.</p>
 * 
 * <p>Some objects that constitute a runtime state may be re-used across several instances 
 * in order to save memory ({@link org.scenariotools.sml.runtime.ElementContainer see also}). Client code should consider that invoking operations on 
 * a runtime state instance can change those constituents and thereby invalidate other
 * state instances.  </p>
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeState#getActiveScenarios <em>Active Scenarios</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeState#getDynamicObjectContainer <em>Dynamic Object Container</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeState#getComputedInitializingMessageEvents <em>Computed Initializing Message Events</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeState#getTerminatedExistentialScenariosFromLastPerformStep <em>Terminated Existential Scenarios From Last Perform Step</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeState#getMessageEventBlockedInformation <em>Message Event Blocked Information</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeState#getObjectSystem <em>Object System</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeState#isSafetyViolationOccurredInGuarantees <em>Safety Violation Occurred In Guarantees</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeState#isSafetyViolationOccurredInAssumptions <em>Safety Violation Occurred In Assumptions</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeState#getEventToTransitionMap <em>Event To Transition Map</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeState#getEnabledEvents <em>Enabled Events</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeState#getOutgoingTransition <em>Outgoing Transition</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeState#getIncomingTransition <em>Incoming Transition</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeState#getStateGraph <em>State Graph</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeState#isSystemChoseToWait <em>System Chose To Wait</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeState()
 * @model
 * @generated
 */
public interface SMLRuntimeState extends AnnotatableElement {
	/**
	 * Returns the value of the '<em><b>Active Scenarios</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.ActiveScenario}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Scenarios</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * List of active scenarios that constitute this runtime state. 
	 * This list is ordered by activation, i.e. instances at lower indices have been
	 * created earlier. Instances are removed or added during invocations of {@link #performStep}.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Active Scenarios</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeState_ActiveScenarios()
	 * @model
	 * @generated
	 */
	EList<ActiveScenario> getActiveScenarios();

	/**
	 * Returns the value of the '<em><b>Dynamic Object Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dynamic Object Container</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Holds a copy of the object system state that is affected by invocation of the {@link #performStep} operation.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Dynamic Object Container</em>' reference.
	 * @see #setDynamicObjectContainer(DynamicObjectContainer)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeState_DynamicObjectContainer()
	 * @model
	 * @generated
	 */
	DynamicObjectContainer getDynamicObjectContainer();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getDynamicObjectContainer <em>Dynamic Object Container</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dynamic Object Container</em>' reference.
	 * @see #getDynamicObjectContainer()
	 * @generated
	 */
	void setDynamicObjectContainer(DynamicObjectContainer value);

	/**
	 * Returns the value of the '<em><b>Computed Initializing Message Events</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.MessageEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Computed Initializing Message Events</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * A list of message events that lead to activation of scenarios.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Computed Initializing Message Events</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeState_ComputedInitializingMessageEvents()
	 * @model containment="true"
	 * @generated
	 */
	EList<MessageEvent> getComputedInitializingMessageEvents();

	/**
	 * Returns the value of the '<em><b>Terminated Existential Scenarios From Last Perform Step</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.Scenario}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terminated Existential Scenarios From Last Perform Step</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * A list of scenarios that were completed during the last invocation of {@link #performStep}.
	 * Calling {@link performStep} more than once on an instance will cause this list to be reset.
	 * During construction of a statespace, the {@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#generateSuccessor} operation will annotate transitions with this list.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Terminated Existential Scenarios From Last Perform Step</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeState_TerminatedExistentialScenariosFromLastPerformStep()
	 * @model transient="true"
	 * @generated
	 */
	EList<Scenario> getTerminatedExistentialScenariosFromLastPerformStep();

	/**
	 * Returns the value of the '<em><b>Message Event Blocked Information</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.MessageEventBlockedInformation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Event Blocked Information</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Event Blocked Information</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeState_MessageEventBlockedInformation()
	 * @model containment="true"
	 * @generated
	 */
	EList<MessageEventBlockedInformation> getMessageEventBlockedInformation();

	/**
	 * Returns the value of the '<em><b>Object System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object System</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object System</em>' reference.
	 * @see #setObjectSystem(SMLObjectSystem)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeState_ObjectSystem()
	 * @model
	 * @generated
	 */
	SMLObjectSystem getObjectSystem();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getObjectSystem <em>Object System</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object System</em>' reference.
	 * @see #getObjectSystem()
	 * @generated
	 */
	void setObjectSystem(SMLObjectSystem value);

	/**
	 * Returns the value of the '<em><b>Safety Violation Occurred In Guarantees</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Safety Violation Occurred In Guarantees</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Safety Violation Occurred In Guarantees</em>' attribute.
	 * @see #setSafetyViolationOccurredInGuarantees(boolean)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeState_SafetyViolationOccurredInGuarantees()
	 * @model
	 * @generated
	 */
	boolean isSafetyViolationOccurredInGuarantees();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.SMLRuntimeState#isSafetyViolationOccurredInGuarantees <em>Safety Violation Occurred In Guarantees</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Safety Violation Occurred In Guarantees</em>' attribute.
	 * @see #isSafetyViolationOccurredInGuarantees()
	 * @generated
	 */
	void setSafetyViolationOccurredInGuarantees(boolean value);

	/**
	 * Returns the value of the '<em><b>Safety Violation Occurred In Assumptions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Safety Violation Occurred In Assumptions</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Safety Violation Occurred In Assumptions</em>' attribute.
	 * @see #setSafetyViolationOccurredInAssumptions(boolean)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeState_SafetyViolationOccurredInAssumptions()
	 * @model
	 * @generated
	 */
	boolean isSafetyViolationOccurredInAssumptions();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.SMLRuntimeState#isSafetyViolationOccurredInAssumptions <em>Safety Violation Occurred In Assumptions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Safety Violation Occurred In Assumptions</em>' attribute.
	 * @see #isSafetyViolationOccurredInAssumptions()
	 * @generated
	 */
	void setSafetyViolationOccurredInAssumptions(boolean value);

	/**
	 * Returns the value of the '<em><b>Event To Transition Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.sml.runtime.Event},
	 * and the value is of type {@link org.scenariotools.sml.runtime.Transition},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event To Transition Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event To Transition Map</em>' map.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeState_EventToTransitionMap()
	 * @model mapType="org.scenariotools.sml.runtime.EventToTransitionMapEntry&lt;org.scenariotools.sml.runtime.Event, org.scenariotools.sml.runtime.Transition&gt;"
	 * @generated
	 */
	EMap<Event, Transition> getEventToTransitionMap();

	/**
	 * Returns the value of the '<em><b>Enabled Events</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enabled Events</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enabled Events</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeState_EnabledEvents()
	 * @model
	 * @generated
	 */
	EList<Event> getEnabledEvents();

	/**
	 * Returns the value of the '<em><b>Outgoing Transition</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.Transition}.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.sml.runtime.Transition#getSourceState <em>Source State</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing Transition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing Transition</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeState_OutgoingTransition()
	 * @see org.scenariotools.sml.runtime.Transition#getSourceState
	 * @model opposite="sourceState" containment="true"
	 * @generated
	 */
	EList<Transition> getOutgoingTransition();

	/**
	 * Returns the value of the '<em><b>Incoming Transition</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.Transition}.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.sml.runtime.Transition#getTargetState <em>Target State</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming Transition</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming Transition</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeState_IncomingTransition()
	 * @see org.scenariotools.sml.runtime.Transition#getTargetState
	 * @model opposite="targetState"
	 * @generated
	 */
	EList<Transition> getIncomingTransition();

	/**
	 * Returns the value of the '<em><b>State Graph</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getStates <em>States</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State Graph</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State Graph</em>' container reference.
	 * @see #setStateGraph(SMLRuntimeStateGraph)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeState_StateGraph()
	 * @see org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getStates
	 * @model opposite="states" transient="false"
	 * @generated
	 */
	SMLRuntimeStateGraph getStateGraph();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getStateGraph <em>State Graph</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State Graph</em>' container reference.
	 * @see #getStateGraph()
	 * @generated
	 */
	void setStateGraph(SMLRuntimeStateGraph value);

	/**
	 * Returns the value of the '<em><b>System Chose To Wait</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System Chose To Wait</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Chose To Wait</em>' attribute.
	 * @see #setSystemChoseToWait(boolean)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeState_SystemChoseToWait()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isSystemChoseToWait();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.SMLRuntimeState#isSystemChoseToWait <em>System Chose To Wait</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Chose To Wait</em>' attribute.
	 * @see #isSystemChoseToWait()
	 * @generated
	 */
	void setSystemChoseToWait(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * <p>Sets this state's object system to the given object system and
	 * updates the enabled message event list of this state.</p>
	 * 
	 * <p>This operation has to be called only once for the initial state of an SMLRuntimeStateGraph instance.</p>
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void init(SMLObjectSystem objectSystem);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Updates the list of message events that are enabled in this runtime state instance.
	 * The result depends on the object system and the list of active scenarios.
	 * A detailed explanation can be found under <a href="https://bitbucket.org/jgreenyer/scenariotools-sml/wiki/MessageEvent%20Computation">https://bitbucket.org/jgreenyer/scenariotools-sml/wiki/MessageEvent%20Computation</a>.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void updateEnabledMessageEvents();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void performStep(Event event);

} // SMLRuntimeState
