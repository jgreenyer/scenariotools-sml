/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic.helper;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.runtime.SMLObjectSystem;

public class RoleHelper {


	public static boolean canRoleBindToEObject(Role role, EObject eObject,
			SMLObjectSystem objectSystem) {
		
		if (role.isStatic()){
			return ((SMLObjectSystem)objectSystem).getStaticRoleBindings().get(role) == eObject; 
		}else{
			// TODO roles that send/receive first messages but are constrained (possible?)
			return ((EClass) role.getType()).isSuperTypeOf(eObject.eClass());
		}
		
		//return false;
	}
	
}
