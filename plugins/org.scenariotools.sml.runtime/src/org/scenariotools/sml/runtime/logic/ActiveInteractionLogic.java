/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic;

import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.ActiveConstraint;
import org.scenariotools.sml.runtime.ActiveInteraction;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.impl.ActivePartImpl;

public abstract class ActiveInteractionLogic extends ActivePartImpl implements ActiveInteraction {

	@Override
	public void init(ActiveScenarioRoleBindings roleBindings,
			ActivePart parentActivePart, 
			ActiveScenario activeScenario) {

		super.init(roleBindings, parentActivePart, activeScenario);
		
		// reset
		getVariableMap().clear();
		getEObjectVariableMap().clear();
		// initialize nested
		for (ActivePart nestedActivePart : this.getNestedActiveInteractions()) {
			nestedActivePart.init(roleBindings, this,	activeScenario);
		}
		
		if(parentActivePart != null){
			parentActivePart.getCoveredEvents().addAll(this.getCoveredEvents());
		}
		
		// initialize Interaction Constraints
		for(ActiveConstraint activeConstraint : this.getActiveConstraints()){
			activeConstraint.init(roleBindings, this, activeScenario);
		}
	}

	private boolean isPerformStep = true;
	
	@Override
	public ActiveScenarioProgress performStep(MessageEvent event,
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {

		isPerformStep = true;
		return performStepInternal(event, activeScenario, smlRuntimeState);
	}
	
	@Override
	public ActiveScenarioProgress postPerformStep(MessageEvent event, 
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		
		isPerformStep = false;
		return performStepInternal(event, activeScenario, smlRuntimeState);
	}
	

	private ActiveScenarioProgress performStepInternal(MessageEvent event, 
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		
		ActivePart nested = this.getEnabledNestedActiveInteractions().get(0);
		final ActiveScenarioProgress progress = performStepWRTState(nested, event, activeScenario, smlRuntimeState);
		switch (progress) {
		case COLD_VIOLATION:
		case NO_PROGRESS:
		case SAFETY_VIOLATION:
			return progress;
		case PROGRESS:
			return progress;
		case MESSAGE_PROGRESSED:
			return progress;
		case INTERACTION_END:
			ActiveScenarioProgress enableProgress = enableNextElement(event, activeScenario, smlRuntimeState);		
			if(enableProgress == ActiveScenarioProgress.CONTINUE){
				enableProgress = performStepWRTState(this, event, activeScenario, smlRuntimeState);
			}
			return enableProgress;	
		case CONTINUE:
			ActiveScenarioProgress continueProgress = enableNextElement(event, activeScenario, smlRuntimeState);	
			if(continueProgress == ActiveScenarioProgress.PROGRESS){
				return this.performStep(event, activeScenario, smlRuntimeState);
			}
		default:
			throw new RuntimeException("Unexpected value for progress: " + progress);

		}
	}
	
	private ActiveScenarioProgress performStepWRTState(ActivePart activePart, MessageEvent event,
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		
		if(isPerformStep)
			return activePart.performStep(event, activeScenario, smlRuntimeState);
		else
			return activePart.postPerformStep(event, activeScenario, smlRuntimeState);
	}
	
	@Override
	public void updateMessageEvents(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		// update nested elements
		super.updateMessageEvents(activeScenario, smlRuntimeState);
		// update constraints
		for(ActiveConstraint activeParameter : this.getActiveConstraints()){
			activeParameter.updateConstraintEvent(activeScenario, smlRuntimeState);
		}
	}
	
	/**
	 * Enable the first fragment of the interaction.
	 */
	@Override
	public ActiveScenarioProgress enable(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		// first element becomes enabled
		ActivePart enabled = this.getNestedActiveInteractions().get(0);
		this.getEnabledNestedActiveInteractions().add(enabled);
		return enabled.enable(activeScenario, smlRuntimeState);
	}
}
