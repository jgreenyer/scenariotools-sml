/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.ParameterValue;
import org.scenariotools.sml.Collaboration;
import org.scenariotools.sml.Message;
import org.scenariotools.sml.MessageChannel;
import org.scenariotools.sml.ModalMessage;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.runtime.DynamicObjectContainer;
import org.scenariotools.sml.runtime.MessageEventExtensionInterface;
import org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator;
import org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.runtime.configuration.RoleAssignment;
import org.scenariotools.sml.runtime.configuration.RoleBindings;
import org.scenariotools.sml.runtime.logic.helper.ConfigurationObjectCopier;
import org.scenariotools.sml.runtime.logic.helper.EObjectHelper;
import org.scenariotools.sml.runtime.logic.helper.MessageEventHelper;
import org.scenariotools.sml.runtime.plugin.Activator;
import org.scenariotools.sml.utility.ScenarioUtil;
import org.scenariotools.sml.utility.SpecificationUtil;

public abstract class SMLObjectSystemLogic extends MinimalEObjectImpl.Container implements SMLObjectSystem {

	protected static Logger logger = Activator.getLogManager().getLogger(SMLObjectSystemLogic.class.getName());

	/**
	 * Gathers all objects that are simulated during execution in a
	 * {@link DynamicObjectContainer}, creates a list of initializing message
	 * events, creates a list of {@link MessageEventSideEffectsExecutor} objects
	 * and populates a role-to-eobject map, that maps static roles to their
	 * actors.
	 * 
	 */

	public void init(Configuration runConfiguration, SMLRuntimeState smlRuntimeState) {
		initMessageEventExtensions(runConfiguration, getMessageEventSideEffectsExecutor(), "org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor");
		initMessageEventExtensions(runConfiguration, getMessageEventIsIndependentEvaluators(), "org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator");
		this.setRunconfiguration(runConfiguration);

		logger.debug("get all objects from the runConfiguration.");
		
		//TODO black magic? Why we doing this? ConfigurationObjectCopier?
		ConfigurationObjectCopier copier = new ConfigurationObjectCopier(runConfiguration);
		Collection<EObject> allObjects = new HashSet<>(); 
		for(EObject root: runConfiguration.getInstanceModelRootObjects()){
			allObjects.add(root); 
			getRootObjects().add(root);
			root.eAllContents().forEachRemaining(o -> allObjects.add(o));
		}
		for (EObject o : allObjects) {
			markControllabilityStatus(o);
		}
		getObjects().addAll(allObjects);

		initializeStaticRoleBindingsMap(runConfiguration);
		
		initializeDynamicObjectSystem(smlRuntimeState);
	}

	/**
	 * Asks all registered {@link MessageEventSideEffectsExecutor} objects to
	 * execute side effects on the given object container.
	 */
	public void executeSideEffects(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer) {

		// invoke plugged-in MessageEventSideEffectsExecutors:
		for (MessageEventSideEffectsExecutor messageEventSideEffectsExecutor : getMessageEventSideEffectsExecutor()) {
			messageEventSideEffectsExecutor.executeSideEffects(messageEvent, dynamicObjectContainer);
		}

		ETypedElement typedElement = messageEvent.getTypedElement();
		if (typedElement instanceof EStructuralFeature) {
			
			EStructuralFeature feature = (EStructuralFeature) typedElement;
			EObject staticReceiver = messageEvent.getReceivingObject();
			EObject dynamicReceiver = dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(staticReceiver);
			Object parameterValue = messageEvent.getParameterValues().isEmpty()? null: messageEvent.getParameterValues().get(0).getValue();
			
			if(feature.isMany()){
				executeCollectionModification(messageEvent, dynamicObjectContainer);
			} else {
				// if parameter Value refers to a static receiver -> replace it.
				EObject dynamicEobject = dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap()
						.get(parameterValue);
				if (dynamicEobject != null) {
					parameterValue = dynamicEobject;
				}
				dynamicReceiver.eSet(feature, parameterValue);
			}
		}
		
		if(typedElement instanceof EOperation){
			invokeDoExecuteOperationForMessageEvent(messageEvent, dynamicObjectContainer);
		}

		// All objects must be contained by an other object or the ObjectSystem
		for (EObject eObject : this.getObjects()) {
			if (eObject.eContainer() == null && eObject.eResource()==null) {
				logger.error("NO Container! Computing side effects results in inconsistent ObjectSystem state! Object: "
						+ eObject + " has no container.");
				// this.getRootObjects().add(eObject); // Don't change the
				// objects in the static (SML)ObjectSystem.
//				dynamicObjectContainer.getRootObjects().add(eObject);
				logger.debug("Add object to ObjectSystem containment list.");
			}
		}
	}

	private void invokeDoExecuteOperationForMessageEvent(MessageEvent messageEvent,
			DynamicObjectContainer dynamicObjectContainer) {

		EObject staticReceiver = messageEvent.getReceivingObject();
		EObject dynamicReceiver = dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(staticReceiver);
		
		assert(messageEvent.getTypedElement() instanceof EOperation);
		EOperation doExecuteEOperation = (EOperation) messageEvent.getTypedElement();
		

		if (doExecuteEOperation.getEType() != null){
			logger.info("Do-execute operation found " + doExecuteEOperation.getName() + " should be VOID, instean it returns " + doExecuteEOperation.getEType());
		}
		
		EList<Object> parameterList = new BasicEList<Object>();
		for (ParameterValue parameterValue : messageEvent.getParameterValues()) {
			Object valueObject = parameterValue.getValue(); 
			if (valueObject instanceof EObject)
				parameterList.add(dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(valueObject));
			else
				parameterList.add(valueObject);
				
		}
		
		try {
			dynamicReceiver.eInvoke(doExecuteEOperation, parameterList);
			logger.debug("Invoked the DO-execute operation " + doExecuteEOperation.getName());
		} catch (InvocationTargetException e1) {
			// TODO Auto-generated catch block
			logger.error("There was a problem invoking the DO-execute operation " + doExecuteEOperation.getName(), e1);
			return;
		} catch (UnsupportedOperationException e) {
			logger.debug("DO-execute operation not implemented, UnsupportedOperationException for " + doExecuteEOperation.getName());
			return;
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void executeCollectionModification(MessageEvent messageEvent,
			DynamicObjectContainer dynamicObjectContainer) {
		final EStructuralFeature feature = (EStructuralFeature) messageEvent.getTypedElement();
		final EObject dynamicReceiver = dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap()
				.get(messageEvent.getReceivingObject());
		final Object parameterValue = messageEvent.getParameterValues().isEmpty() ? null
				: messageEvent.getParameterValues().get(0).getValue();
		final EList values = (EList) dynamicReceiver.eGet(feature);
		// we need a fresh list (see eSet() javadoc), using the same would
		// result in
		// undefined behavior.
		final BasicEList l = new BasicEList<>(values);

		switch (messageEvent.getCollectionOperation()) {
		case ADD:
			if (feature instanceof EReference) {
				l.add(dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(parameterValue));
			} else {
				l.add(parameterValue);
			}
			break;
		case ADD_TO_FRONT:
			if (feature instanceof EReference)
				l.add(0, dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(parameterValue));
			else
				l.add(0, parameterValue);
			break;
		case CLEAR:
			l.clear();
			break;
		case REMOVE:
			if (feature instanceof EStructuralFeature)
				l.remove(dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(parameterValue));
			else
				l.remove(parameterValue);
			break;
		// the following operations have no side effects	
		case ANY:
		case CONTAINS:
		case CONTAINS_ALL:
		case FIRST:
		case GET:
		case IS_EMPTY:
		case LAST:
		case SIZE:
			return;
		default:
			throw new IllegalArgumentException("Collection operation not supported by the runtime. "+messageEvent.getCollectionOperation());
		}
		dynamicReceiver.eSet(feature, l);
	}

	
	public boolean isIndependent(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer) {
		for (MessageEventIsIndependentEvaluator evaluator : getMessageEventIsIndependentEvaluators()) {
			if(evaluator.isIndependent(messageEvent, dynamicObjectContainer))
				return true;
		}
		return false;
	}
	

	/**
	 * Ask plugged-in MessageEventSideEffectsExecutors. Return false if one
	 * MessageEventSideEffectsExecutor votes for FALSE.
	 */
	public boolean canExecuteSideEffects(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer) {

		for (MessageEventSideEffectsExecutor messageEventSideEffectsExecutor : getMessageEventSideEffectsExecutor()) {
			if (!messageEventSideEffectsExecutor.canExecuteSideEffects(messageEvent, dynamicObjectContainer))
				return false;
		}
		
		if(messageEvent.getTypedElement() instanceof EOperation){
			return invokeCanExecuteOperationForMessageEvent(messageEvent, dynamicObjectContainer);
		}else
			return true;
	}

	private boolean invokeCanExecuteOperationForMessageEvent(MessageEvent messageEvent,
			DynamicObjectContainer dynamicObjectContainer) {
		EObject staticReceiver = messageEvent.getReceivingObject();
		EObject dynamicReceiver = dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(staticReceiver);
		assert(messageEvent.getTypedElement() instanceof EOperation);
		EOperation eOperation = (EOperation) messageEvent.getTypedElement();
		EOperation canExecuteEOperation = 
				dynamicReceiver.eClass().getEAllOperations().stream().filter(eOp -> ("CAN_" + eOperation.getName()).equals(eOp.getName())).findAny().orElse(null);
		
		if (canExecuteEOperation == null){
			logger.debug("No CAN-execute operation found for message event " + messageEvent);
			return true;
		}

		if (!canExecuteEOperation.getEType().getName().equals("EBoolean")){
			logger.error("CAN-execute operation found " + canExecuteEOperation.getName() + " must return EBoolean, instean it returns " + canExecuteEOperation.getEType());
			return false;
		}
		
		EList<Object> parameterList = new BasicEList<Object>();
		for (ParameterValue parameterValue : messageEvent.getParameterValues()) {
			Object valueObject = parameterValue.getValue();
			if (valueObject instanceof EObject)
				parameterList.add(dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(valueObject));
			else
				parameterList.add(valueObject);
		}
		
		Object result = null; 
		
		try {
			result = dynamicReceiver.eInvoke(canExecuteEOperation, parameterList);
		} catch (InvocationTargetException e1) {
			// TODO Auto-generated catch block
			logger.error("There was a problem invoking the CAN-execute operation " + canExecuteEOperation.getName(), e1);
			return false;
		} catch (UnsupportedOperationException e) {
			logger.debug("CAN-execute operation not implemented, UnsupportedOperationException for " + canExecuteEOperation.getName());
			return true;
		}
		
		if (result == null){
			logger.error("There was a problem invoking the CAN-execute operation -- result is NULL " + canExecuteEOperation.getName());
			return false;
		}
		if (!(result instanceof Boolean)){
			logger.error("There was a problem invoking the CAN-execute operation -- result is not Boolean " + canExecuteEOperation.getName());
			return false;
		}
		
		logger.debug("invoking the CAN-execute operation " + canExecuteEOperation.getName() + " returns " + result);
		
		return (Boolean) result;

	}

	/**
	 */
	public boolean isNonSpontaneousMessageEvent(MessageEvent messageEvent) {
		return this.getSpecification().getNonSpontaneousOperations().contains(messageEvent.getTypedElement());
	}

	
	/**
	 * Populates the bindings map for static roles with values from the given copier.  
	 */
	private void initializeStaticRoleBindingsMap(Configuration config) {
		for(RoleBindings bindings: config.getStaticRoleBindings()){
			if(config.getIgnoredCollaborations().contains(bindings.getCollaboration()))
				continue;
			for(RoleAssignment assignment : bindings.getBindings()) {
				final Role role = assignment.getRole();
				final EObject object = assignment.getObject();
				// TODO change multiplicity of map value.
				if (null == this.getStaticRoleBindings().get(role)) {
					this.getStaticRoleBindings().put(role, object);
				}
				if (object == null) {
					logger.error("Creating static role binding failed! Role: " + role.getName());
					continue;
				}
			}
		}
		for(Collaboration collab : config.getConsideredCollaborations())
			for(Role role : collab.getRoles()) {
				if(!role.isStatic())
					continue;

				EObject boundObject = this.getStaticRoleBindings().get(role);
				if(boundObject == null)
					logger.warn("No object bound to static role '" + role.getName() + "' in collaboration '" + collab.getName() + "'");
			}		
		
//		final Map<Role, EObject> mapFromCopier = copier.getStaticRoleBindingsMap();
//		for (Role role : mapFromCopier.keySet()) {
//			final EObject object = mapFromCopier.get(role);
//			
			
//		}
	}

	// /**
	// * Copies objects from instance model(s) referenced by
	// * <code>runConfiguration</code> and adds the copies to the object system.
	// *
	// * @param runConfiguration
	// * @return
	// */
	// private Map<EObject, EObject> addInstanceModelObjects(Configuration
	// runConfiguration) {
	// // Copy all objects from models referenced by the run configuration.
	// Copier copier = new Copier(true);
	// List<Object> toCopy = new ArrayList<>();
	// List<EObject> rootObjects =
	// runConfiguration.getInstanceModelRootObjects();
	// for (EObject root : rootObjects) {
	// toCopy.add(root);
	// for (Iterator<EObject> it = root.eAllContents(); it.hasNext();) {
	// EObject o = it.next();
	// toCopy.add(o);
	// }
	// }
	// // toCopy.addAll(runConfiguration.getStaticRoleBindings());
	// Collection<Object> copies = copier.copyAll(toCopy);
	// copier.copyReferences();
	//
	// // Put all copied objects into different lists.
	// for (EObject rootObject : rootObjects) {
	// final EObject rootCopy = copier.get(rootObject);
	// // a root object in a resource is also a root object in the object
	// // system.
	// getRootObjects().add(rootCopy);
	// getObjects().add(rootCopy);
	// // populate controllable/uncontrollable lists.
	// markControllabilityStatus(rootCopy);
	// for (Iterator<EObject> it = rootCopy.eAllContents(); it.hasNext();) {
	// EObject contained = it.next();
	// getObjects().add(contained);
	// markControllabilityStatus(contained);
	// }
	// }
	// return copier;
	// }

	/**
	 * Adds the given object to the controllable or uncontrollable object list,
	 * depending on its eclass. If an object has a class that is neither
	 * declared as controllable or uncontrollable, calling this method has no
	 * effect.
	 * 
	 * @param o
	 */
	private void markControllabilityStatus(EObject o) {
		EList<EClass> controllableEClasses = getSpecification().getControllableEClasses();
		if(controllableEClasses.contains(o.eClass())
				|| !Collections.disjoint(controllableEClasses, o.eClass().getEAllSuperTypes())){
			getControllableObjects().add(o);
			return;
		}
		EList<EClass> uncontrollableEClasses = SpecificationUtil.getAllUncontrollableClasses(getSpecification()); 
		if(uncontrollableEClasses.contains(o.eClass())
				|| !Collections.disjoint(uncontrollableEClasses, o.eClass().getEAllSuperTypes())){
			getUncontrollableObjects().add(o);
			return;
		}
	}

	/**
	 * Returns <code>true</code> if <code>eClass</code> is controllable
	 * according to the specification associated with this object system.
	 * 
	 * @param eClass
	 * @return
	 */
	private boolean isControllableEClass(EClass eClass) {
		return getSpecification().getControllableEClasses().contains(eClass) 
				|| !Collections.disjoint(getSpecification().getControllableEClasses(), eClass.getEAllSuperTypes());
	}
	
	private <T extends MessageEventExtensionInterface> void initMessageEventExtensions(Configuration runConfiguration, EList<T> extensions, String extensionPointClass) {
		if (Platform.isRunning() && extensions.isEmpty()) {
			IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(extensionPointClass);
			for (IConfigurationElement configurationElement : config) {
				EPackage processingPolicyPackage = EPackage.Registry.INSTANCE.getEPackage(configurationElement.getAttribute("nsURI"));
				if (processingPolicyPackage == null) {
					logger.error("The specified package with nsURI \"" + configurationElement.getAttribute("nsURI") + "\" cannot be found");
					return;
				}
				
				EClass processingPolicyClass = (EClass) processingPolicyPackage.getEClassifier(configurationElement.getAttribute("eClass"));
				if (processingPolicyClass == null) {
					logger.error("The specified class \"" + configurationElement.getAttribute("eClass") + "\" cannot be found");
					return;
				}

				EObject eObj = EPackage.Registry.INSTANCE.getEFactory(configurationElement.getAttribute("nsURI")).create(processingPolicyClass);
				if (eObj == null)
					return;
				
				try {
					@SuppressWarnings("unchecked")
					T extension = (T) eObj;
					extension.init(runConfiguration);
					extensions.add(extension);
				}
				catch (ClassCastException e) {
					logger.error("The provided class " + configurationElement.getAttribute("nsURI") + "."
							+ configurationElement.getAttribute("eClass")
							+ " does not extend " + extensionPointClass + ".");
					
				}
			}
		}
	}

	/**
	 * Computes initializing environment message events by iterating over all first messages of all scenarios of all collaborations
	 * and creating message events for these messages. 
	 */
	public EList<MessageEvent> getInitializingEnvironmentMessageEvents(DynamicObjectContainer dynamicObjectProvider) {
		
		EList<MessageEvent> initializingEnvironmentMessageEvents = new UniqueEList<MessageEvent>();
		
		for (Collaboration collaboration : getRunconfiguration().getConsideredCollaborations()) {
			for (Scenario scenario : collaboration.getScenarios()) {
				EList<ModalMessage> firstMessages = ScenarioUtil
						.getInitializingMessages(scenario);
				for (Message firstMessage : firstMessages) {
					logger.debug("Initializing message of scenario " + scenario.getName() + " is: " + firstMessage);
					if (!isControllableEClass((EClass) firstMessage.getSender().getType())) // if it is an environment message
						initializingEnvironmentMessageEvents.addAll(getInitializingEventsForMessage(firstMessage, scenario,dynamicObjectProvider));
				}
			}
		}
		
		
		
		return initializingEnvironmentMessageEvents;
	}

	/**
	 * Gets all possible message events that are unifiable with
	 * <code>firstMessage</code> to the initializing message events list.
	 * 
	 * @param firstMessage
	 * @param dynamicObjectProvider 
	 */
	private EList<MessageEvent> getInitializingEventsForMessage(Message firstMessage, Scenario scenario, DynamicObjectContainer dynamicObjectProvider) {

		EList<MessageEvent> initializingMessageEvents = new UniqueEList<MessageEvent>();
		final EList<EObject> objectsThatCanBindToSendingRole = objectsThatCanBindToSendingRole(firstMessage.getSender());
		for (EObject sendingEObject : objectsThatCanBindToSendingRole) {
			for (EObject receivingEObject : objectsThatCanBindToReceivingRole(sendingEObject, firstMessage,dynamicObjectProvider)) {
				MessageEvent messageEvent = MessageEventHelper.createMessageEvent(sendingEObject, receivingEObject,
						firstMessage);

				logger.debug("Created initializing message event: " + messageEvent);
				initializingMessageEvents.add(messageEvent);
			}
		}
		return initializingMessageEvents;
	}
	
	private List<EObject> objectsThatCanBindToReceivingRole(EObject sendingEObject, Message firstMessage, DynamicObjectContainer dynamicObjectProvider) {
			final List<MessageChannel> channels = getSpecification().getChannelOptions().getChannelsForEvent(firstMessage.getModelElement()); 
			if(channels.isEmpty()) {
				return objectsThatCanBindToSendingRole(firstMessage.getReceiver());
			} else {
				final List<EObject> allReferencedObjects = new UniqueEList<>();
				sendingEObject = dynamicObjectProvider.getStaticEObjectToDynamicEObjectMap().get(sendingEObject);
				
				for (MessageChannel channel : channels) {
					final List<EObject> dynamicReceivers = EObjectHelper.getAllReferencedObjects(sendingEObject,
							(EReference) channel.getChannelFeature());
				for (EObject dynamicReceiver : dynamicReceivers) {
					dynamicObjectProvider.getStaticEObjectToDynamicEObjectMap().entrySet().forEach(entry -> {
						if (dynamicReceiver == entry.getValue())
							allReferencedObjects.add(entry.getKey());
					});
				}
				}
				
				return allReferencedObjects;
			}
	}

	private EList<EObject> objectsThatCanBindToSendingRole(Role role){
		final EList<EObject> objectsThatCanBindToRole = new BasicEList<EObject>();
		if (role.isStatic()) {
			assert(getStaticRoleBindings().keySet().contains(role));
			EObject object = getStaticRoleBindings().get(role);
			objectsThatCanBindToRole.add(object);
		} else {
			// dynamic role, may be played by all objects that have the role's
			// type or a subtype.
			for (EObject potentialSender : getObjects())
				if (((EClass) role.getType()).isSuperTypeOf(potentialSender.eClass()))
					objectsThatCanBindToRole.add(potentialSender);
		}
		return objectsThatCanBindToRole;
	}


	/**
	 * Maps initializing MessageEvents to the respective Scenarios.
	 */
	public EList<Scenario> getScenariosForInitMessageEvent(MessageEvent initMessageEvent) {
		
		EList<Scenario> getScenariosForInitMessageEvent = new BasicEList<Scenario>();
		
		for (Collaboration collaboration : getRunconfiguration().getConsideredCollaborations()) {
			for (Scenario scenario : collaboration.getScenarios()) {
				EList<ModalMessage> firstMessages = ScenarioUtil.getInitializingMessages(scenario);
				for (Message firstMessage : firstMessages) {
					if(firstMessage.getModelElement().equals(initMessageEvent.getTypedElement())
							&& (firstMessage.getSender().getType().equals(initMessageEvent.getSendingObject().eClass()) 
									|| initMessageEvent.getSendingObject().eClass().getEAllSuperTypes().contains(firstMessage.getSender().getType()))
							&& (firstMessage.getReceiver().getType().equals(initMessageEvent.getReceivingObject().eClass())
									|| initMessageEvent.getReceivingObject().eClass().getEAllSuperTypes().contains(firstMessage.getReceiver().getType()))
							){
						getScenariosForInitMessageEvent.add(scenario);
					}
				}
			}
		}
		
		return getScenariosForInitMessageEvent;
	}

	/**
	 * Initialize the Dynamic Object System and set up the mapping between
	 * static and dynamic objects. Initialize the
	 * <code>SMLRuntimeState.getDynamicObjectContainer().getStaticEObjectToDynamicEObjectMap()</code>
	 * and the
	 * <code>SMLRuntimeState.getDynamicObjectContainer().getRootObjects()</code>
	 * .
	 * 
	 * @param smlRuntimeState
	 */
	private void initializeDynamicObjectSystem(SMLRuntimeState smlRuntimeState) {
		// add evolving objects
		final Copier copier = new Copier();
		final EList<EObject> objectsToCopy = new BasicEList<EObject>();
		objectsToCopy.addAll(this.getRootObjects());
		copier.copyAll(objectsToCopy);
		copier.copyReferences();

		// set up mapping: static -> dynamic objects
		// for(EObject e : objectDefinitionToInstanceMap.values()){
		// use objects reference instead => referenced instance models are
		// respected.
		for (EObject e : getObjects()) {
			EObject copiedEObject = (EObject) copier.get(e);
			smlRuntimeState.getDynamicObjectContainer().getStaticEObjectToDynamicEObjectMap().put(e, copiedEObject);
		}
		// add root objects
		for (EObject e : this.getRootObjects()) {
			EObject copiedEObject = (EObject) copier.get(e);
			smlRuntimeState.getDynamicObjectContainer().getRootObjects().add(copiedEObject);
		}
	}
}
