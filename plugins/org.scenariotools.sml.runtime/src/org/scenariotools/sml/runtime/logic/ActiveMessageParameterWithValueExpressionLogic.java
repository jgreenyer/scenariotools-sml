/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic;

import java.util.Collection;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.ParameterValue;
import org.scenariotools.sml.ValueParameterExpression;
import org.scenariotools.sml.runtime.ActiveMessageParameterWithValueExpression;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.logic.helper.BasicInterpreter;

public abstract class ActiveMessageParameterWithValueExpressionLogic extends MinimalEObjectImpl.Container implements ActiveMessageParameterWithValueExpression {

	@Override
	public void init(ParameterValue parameterValue) {
		// This parameter type is lazy initialized. It will be initialized by the ActiveModalMessage if it gets enabled. 
	}

	@Override
	public void update(ParameterValue parameterValue, 
			ActivePart parent,
			ActiveScenario activeScenario, 
			SMLRuntimeState smlRuntimeState) {
		
		final BasicInterpreter interpreter = new BasicInterpreter(smlRuntimeState.getDynamicObjectContainer());
		final ValueParameterExpression expr = this.getParameter();
		
		// Joel: We have to recalculate the coveredMessageEvents based on the value expression.

		// First, get one (the first) of the events that correspond to the parent modal message.
		// (These could have been created during the building of the active scenario or by prior
		// calls of this method)
		MessageEvent coveredMessageEvent = parent.getCoveredEvents().get(0);
		
		// Clear all previously calculated events, in order to reset corresponding message events
		// that may have been created by previous calls of this message.
		parent.getCoveredEvents().clear();
		
		// re-evaluate the value expression
		Object valueCollection = interpreter.evaluate(expr.getValue(), parent, activeScenario);

		// if the result is a collection, create message events accordingly 
		if(valueCollection instanceof Collection<?>){
			boolean reuseFirstCoveredMessageEvent = true;
			
			if (((Collection) valueCollection).isEmpty()){
				throw new UnsupportedOperationException("Currently the case is not handled where a parameter value expression evaluates to an empty list.");
			}
			
			for (EObject valueElement : (Collection<EObject>)valueCollection) {
				int parameterIndex = coveredMessageEvent.getParameterValues().indexOf(parameterValue);
				if (parameterIndex < 0) 
					continue;
				
				
				if (reuseFirstCoveredMessageEvent){
					// reuse the first message event and avoid creating a copy for it, for efficiency reasons.
					reuseFirstCoveredMessageEvent = false;
					coveredMessageEvent.getParameterValues().get(parameterIndex).setValue(valueElement);
					parent.getCoveredEvents().add(coveredMessageEvent);
				}else{
					// copy previous message event and only change the parameter value. 
					MessageEvent coveredMessageEventCopy = EcoreUtil.copy(coveredMessageEvent);
					coveredMessageEventCopy.getParameterValues().get(parameterIndex).setValue(valueElement);
					parent.getCoveredEvents().add(coveredMessageEventCopy);
					
					for(MessageEvent otherEvent : activeScenario.getAlphabet())
						if(otherEvent.isParameterUnifiableWith(coveredMessageEventCopy))
							return;
					
					activeScenario.getAlphabet().add(coveredMessageEventCopy);
				}
				
			}
		}else{
			// if it is a single value, update it.
			parameterValue.setValue(interpreter.evaluate(expr.getValue(), parent, activeScenario));				
			parent.getCoveredEvents().add(coveredMessageEvent);
		}
	}

	@Override
	public boolean executeSideEffectsOnUnification(ParameterValue parameterValueFromOccuredMessage, 
			ParameterValue parameterValue,
			ActiveScenario activeScenario, 
			SMLRuntimeState smlRuntimeState) {
		
		return true;
	}

	/**
	 * This ParameterType has no side effects by convention
	 */
	@Override
	public boolean hasSideEffectsOnUnification() {
		return false;
	}
}
