/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic.ParameterRanges;

import java.util.Iterator;

import org.eclipse.emf.common.util.BasicEList;

public class EmptyParameterValues<T> implements ParameterValues<T> {
	
	protected EmptyParameterValues() {
	}

	@Override
	public boolean contains(T value) {
		return false;
	}

	@Override
	public boolean isNull() {
		return true;
	}
	
	public String toSting() {
		return "[]";
	}

	@Override
	public Iterator<T> iterator() {
		return new BasicEList<T>().iterator();
	}
}