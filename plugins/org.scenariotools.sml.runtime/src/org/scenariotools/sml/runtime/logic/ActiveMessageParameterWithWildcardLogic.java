/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.scenariotools.sml.runtime.ParameterValue;
import org.scenariotools.sml.runtime.ActiveMessageParameterWithWildcard;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.SMLRuntimeState;

public abstract class ActiveMessageParameterWithWildcardLogic extends MinimalEObjectImpl.Container implements ActiveMessageParameterWithWildcard {

	@Override
	public void init(ParameterValue parameterValue) {
		parameterValue.setWildcardParameter(true);
	}

	@Override
	public void update(ParameterValue parameterValue, 
			ActivePart parent,
			ActiveScenario activeScenario, 
			SMLRuntimeState smlRuntimeState) {
		
		parameterValue.setWildcardParameter(true);
	}

	@Override
	public boolean executeSideEffectsOnUnification(ParameterValue parameterValueFromOccuredMessage, 
			ParameterValue parameterValue,
			ActiveScenario activeScenario, 
			SMLRuntimeState smlRuntimeState) {
		
		return false;
	}

	@Override
	public boolean hasSideEffectsOnUnification() {
		return false;
	}
}
