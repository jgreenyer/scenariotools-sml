/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic;

import java.util.Iterator;

import org.eclipse.emf.common.util.EList;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.ActiveAlternative;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.ViolationKind;
import org.scenariotools.sml.runtime.impl.ActivePartImpl;

public abstract class ActiveAlternativeLogic extends ActivePartImpl implements ActiveAlternative {

	@Override
	public void init(ActiveScenarioRoleBindings roleBindings, 
			ActivePart parentActivePart,
			ActiveScenario activeScenario) {

		super.init(roleBindings, parentActivePart, activeScenario);

		// initialize nested
		for (ActivePart nestedActivePart : this.getNestedActiveInteractions()) {
			nestedActivePart.init(roleBindings, this, activeScenario);
		}

		parentActivePart.getCoveredEvents().addAll(this.getCoveredEvents());
	}

	private boolean isPerformStep = true;
	
	@Override
	public ActiveScenarioProgress performStep(MessageEvent event,
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {

		isPerformStep = true;
		return performStepInternal(event, activeScenario, smlRuntimeState);
	}
	
	@Override
	public ActiveScenarioProgress postPerformStep(MessageEvent event, 
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		
		isPerformStep = false;
		return performStepInternal(event, activeScenario, smlRuntimeState);
	}
	

	private ActiveScenarioProgress performStepInternal(MessageEvent event, 
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {	

		// Check case conditions
		// TODO May have side effects
		if(this.getEnabledNestedActiveInteractions().isEmpty()){
			this.enable(activeScenario, smlRuntimeState);
		}
		
		ActivePart takenCase = null;
		ActiveScenarioProgress result = ActiveScenarioProgress.NO_PROGRESS;
		EList<ActivePart> enabledNestedInteractions = this.getEnabledNestedActiveInteractions();
		cases: for (Iterator<ActivePart> it = enabledNestedInteractions.iterator(); it.hasNext();) {
			ActivePart nestedActiveInteraction = it.next();
			ActiveScenarioProgress caseProgress = performStepWRTState(nestedActiveInteraction, event, activeScenario,
					smlRuntimeState);
			switch (caseProgress) {
			case COLD_VIOLATION:
				if (result == ActiveScenarioProgress.NO_PROGRESS)
					result = ActiveScenarioProgress.COLD_VIOLATION;
				break;
			case INTERACTION_END:
				result = ActiveScenarioProgress.INTERACTION_END;
				break cases;
			case MESSAGE_PROGRESSED:
				result = caseProgress;		
				takenCase = nestedActiveInteraction;
				break cases;
			case NO_PROGRESS:
				break;
			case PROGRESS:
				takenCase = nestedActiveInteraction;
				result = ActiveScenarioProgress.PROGRESS;
				// TODO test for correctness
				// take first case as default. Don't test others because we cannot undo the side effects 
				// on variables and object system.
				break cases;
			case SAFETY_VIOLATION:
				if (result != ActiveScenarioProgress.PROGRESS) // TODO check: can result ever be PROGRESS (-> break cases)
					result = ActiveScenarioProgress.SAFETY_VIOLATION;
				break;
			default:
				throw new RuntimeException("Illegal value for progress");

			}
		}

		if ((result == ActiveScenarioProgress.MESSAGE_PROGRESSED 
				|| result == ActiveScenarioProgress.PROGRESS)
				&& enabledNestedInteractions.size() > 1) {
			
			enabledNestedInteractions.clear();
			enabledNestedInteractions.add(takenCase);
		}
		if (result == ActiveScenarioProgress.INTERACTION_END) {
			enabledNestedInteractions.clear();
			return ActiveScenarioProgress.INTERACTION_END;
			// result = ActiveScenarioProgress.PROGRESS;
		}
		
		// check for violation because no branch of the alternative is activated
		if(result == ActiveScenarioProgress.NO_PROGRESS && enabledNestedInteractions.isEmpty()){
			// TODO I assume a strict cut to 
			ViolationKind vk = this.isViolatingInInteraction(event, true);
			switch (vk) {
			case COLD:
				return ActiveScenarioProgress.COLD_VIOLATION;
			case SAFETY:
				return ActiveScenarioProgress.SAFETY_VIOLATION;
			default:
				return ActiveScenarioProgress.NO_PROGRESS;
			}
		}
		return result;
	}
	
	private ActiveScenarioProgress performStepWRTState(ActivePart activePart, MessageEvent event,
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		
		if(isPerformStep)
			return activePart.performStep(event, activeScenario, smlRuntimeState);
		else
			return activePart.postPerformStep(event, activeScenario, smlRuntimeState);
	}
	
	@Override
	public ActiveScenarioProgress enable(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		
		boolean skipAlternative = true;
		
		for (ActivePart nestedActivePart : this.getNestedActiveInteractions()) {
			
			ActivePart activeCase = nestedActivePart;
			ActiveScenarioProgress caseProgress = activeCase.enable(activeScenario, smlRuntimeState);
			if(caseProgress != ActiveScenarioProgress.STEP_OVER) {
				this.getEnabledNestedActiveInteractions().add(activeCase);
				if(caseProgress == ActiveScenarioProgress.CONTINUE){
					activeCase.performStep(null, activeScenario, smlRuntimeState);
				}
				skipAlternative = false;
			}	
		}
		return skipAlternative ? ActiveScenarioProgress.STEP_OVER:ActiveScenarioProgress.NO_PROGRESS ;

	}
}