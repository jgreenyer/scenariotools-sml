/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.scenariotools.sml.runtime.EObjectParameterValue;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.ParameterValue;
import org.scenariotools.sml.BindingExpression;
import org.scenariotools.sml.ChannelOptions;
import org.scenariotools.sml.Collaboration;
import org.scenariotools.sml.FeatureAccessBindingExpression;
import org.scenariotools.sml.Message;
import org.scenariotools.sml.MessageChannel;
import org.scenariotools.sml.ModalMessage;
import org.scenariotools.sml.ParameterBinding;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.RoleBindingConstraint;
import org.scenariotools.sml.VariableBindingParameterExpression;
import org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation;
import org.scenariotools.sml.expressions.scenarioExpressions.Expression;
import org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess;
import org.scenariotools.sml.expressions.scenarioExpressions.OperationValue;
import org.scenariotools.sml.expressions.scenarioExpressions.StructuralFeatureValue;
import org.scenariotools.sml.expressions.scenarioExpressions.Value;
import org.scenariotools.sml.expressions.scenarioExpressions.Variable;
import org.scenariotools.sml.expressions.scenarioExpressions.VariableValue;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.BlockedType;
import org.scenariotools.sml.runtime.Context;
import org.scenariotools.sml.runtime.DynamicObjectContainer;
import org.scenariotools.sml.runtime.MultiActiveScenarioInitializations;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.logic.helper.ActiveScenarioHelper;
import org.scenariotools.sml.runtime.logic.helper.BasicInterpreter;
import org.scenariotools.sml.runtime.logic.helper.DynamicObjectContainerHelper;
import org.scenariotools.sml.runtime.logic.helper.EObjectHelper;
import org.scenariotools.sml.runtime.logic.helper.ExpressionEvaluationException;
import org.scenariotools.sml.runtime.plugin.Activator;
import org.scenariotools.sml.utility.ScenarioUtil;

public abstract class ActiveScenarioLogic extends MinimalEObjectImpl.Container implements ActiveScenario{
	
	protected static Logger logger = Activator.getLogManager().getLogger(
			ActiveScenarioLogic.class.getName());
	
	public EList<MessageEvent> getRequestedEvents() {
		return this.getMainActiveInteraction().getRequestedEvents();
	}
	
	@Override
	public boolean isInRequestedState() {
		return getMainActiveInteraction().isInRequestedState();
	}
	
	@Override
	public boolean isInStrictState() {
		return getMainActiveInteraction().isInStrictState();
	}

	public Object getValue(Variable variable) {
		if (!(variable instanceof Role)) {
			return Context.UNDEFINED;
		}
		EObject result = this.getRoleBindings().getRoleBindings().get(variable);
		return result==null ? Context.UNDEFINED : result;
	}
	
	public ActiveScenarioProgress performStep(MessageEvent event, SMLRuntimeState smlRuntimeState) {
		if(this.isSafetyViolationOccurred()){
			logger.error("This line should be never reached");
			return ActiveScenarioProgress.SAFETY_VIOLATION;
		}
		if(logger.isDebugEnabled())
			logger.debug("Calling performStep on active scenario " + this.getScenario().getName()	+ " with event " + event);
		
		ActiveScenarioProgress activeScenarioProgress = ActiveScenarioProgress.SAFETY_VIOLATION;
		if(messageSentWithoutChannelExisting(event,smlRuntimeState))
			return ActiveScenarioProgress.SAFETY_VIOLATION;
		try{
			activeScenarioProgress = this.getMainActiveInteraction().performStep(event, this, smlRuntimeState);
		} catch (ExpressionEvaluationException e){
			logger.debug(e);
		}
		
		if(activeScenarioProgress == ActiveScenarioProgress.MESSAGE_PROGRESSED){
			try{
				activeScenarioProgress = this.getMainActiveInteraction().postPerformStep(event, this, smlRuntimeState);
			} catch (ExpressionEvaluationException e){
				logger.error(e);
			}
		}
		
		if(activeScenarioProgress == ActiveScenarioProgress.PROGRESS){
			getMainActiveInteraction().updateMessageEvents(this, smlRuntimeState);
		}else if(activeScenarioProgress == ActiveScenarioProgress.SAFETY_VIOLATION){
			this.setSafetyViolationOccurred(true);
		}
		
		if(logger.isDebugEnabled()){
			if(activeScenarioProgress == ActiveScenarioProgress.COLD_VIOLATION){
				logger.debug("COLD_VIOLATION occured!");
			}else if(activeScenarioProgress == ActiveScenarioProgress.SAFETY_VIOLATION){
				logger.debug("SAFETY_VIOLATION occured!");
			}else if(activeScenarioProgress == ActiveScenarioProgress.PROGRESS){
				logger.debug("PROGRESS!");
			}
		}
		
		return activeScenarioProgress;
	}

	private boolean messageSentWithoutChannelExisting(MessageEvent event, SMLRuntimeState smlRuntimeState) {
		ChannelOptions options = ((SMLObjectSystem) smlRuntimeState.getObjectSystem()).getSpecification().getChannelOptions();
		if(options == null)
			return false;
		
		final EObject dynamicSender = smlRuntimeState.getDynamicObjectContainer()
				.getStaticEObjectToDynamicEObjectMap().get(event.getSendingObject());
		final EObject dynamicReceiver = smlRuntimeState.getDynamicObjectContainer().getStaticEObjectToDynamicEObjectMap()
				.get(event.getReceivingObject());
		
		EList<MessageChannel> channels = options.getChannelsForEvent(event.getTypedElement());
		for (MessageChannel channel : channels) {
			if (EObjectHelper.getAllReferencedObjects(dynamicSender, (EReference) channel.getChannelFeature())
					.contains(dynamicReceiver))
				return false;
		}
		return !channels.isEmpty();
		
	}

	public MultiActiveScenarioInitializations init(SMLObjectSystem objectSystem, DynamicObjectContainer dynamicObjectContainer, SMLRuntimeState smlRuntimeState, MessageEvent messageEvent) {
		
		MultiActiveScenarioInitializations multiActiveScenarioInitializations = RuntimeFactory.eINSTANCE.createMultiActiveScenarioInitializations();
		multiActiveScenarioInitializations.getActiveScenarioToActiveScenarioProgressMap().put(this, null);
		
		for (EClass eclass: this.getScenario().getContexts()) {
				EObject contextEObject = eclass.getEPackage().getEFactoryInstance().create(eclass);
				try{
					EObject sender = dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(messageEvent.getSendingObject());
					EObjectHelper.setEReferenceByName(contextEObject, "sender", sender);
				}catch (IllegalArgumentException e) {
					logger.info("Cannot set sender object to context helper object.");
				}
				try{					
					EObject receiver = dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(messageEvent.getReceivingObject());
					EObjectHelper.setEReferenceByName(contextEObject, "receiver", receiver);
				}catch (IllegalArgumentException e) {
					logger.info("Cannot set receiver object to context helper object.");
				}
				for (ParameterValue parameterValue : messageEvent.getParameterValues()) {
					if (parameterValue.isUnset()) continue;
					if (parameterValue instanceof EObjectParameterValue){
						EObject staticObject = ((EObjectParameterValue) parameterValue).getEObjectParameterValue();
						EObject dynamicObject = dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(staticObject);
						try{
							if(parameterValue.getEParameter() == null){ // this happens when message is a set-message
								EObjectHelper.setEReferenceByName(contextEObject, parameterValue.getStrucFeatureOrEOp().getName(), dynamicObject);
							}else{ // regular message parameter
								EObjectHelper.setEReferenceByName(contextEObject, parameterValue.getEParameter().getName(), dynamicObject);
							}							
						}catch (IllegalArgumentException e) {
							logger.info("Cannot set parameter value to context helper object:" + parameterValue);
						}
					}
				}
				try{
					if(!EObjectHelper.getBooleanEAttributeByName(contextEObject, "matches")){
//						return ActiveScenarioProgress.COLD_VIOLATION;
						multiActiveScenarioInitializations.getActiveScenarioToActiveScenarioProgressMap().put(
								this, 
								ActiveScenarioProgress.COLD_VIOLATION);
						return multiActiveScenarioInitializations;
					}
				}catch (IllegalArgumentException e) {
					logger.debug("Could not find any 'matches' method with return type boolean for EClass " + eclass.getName());
				}
				
				getContextHelperClassInstances().put(eclass, contextEObject);

		}
		
		this.setRoleBindings(RuntimeFactory.eINSTANCE.createActiveScenarioRoleBindings());
		
		// 3) Create static role bindings
		for (Role role : ((Collaboration) this.getScenario().eContainer()).getRoles()) {
			if (role.isStatic()) {
				this
						.getRoleBindings()
						.getRoleBindings()
						.put(role, objectSystem.getStaticRoleBindings().get(role));
			}
			
		}

		EList<ModalMessage> firstMessage = ScenarioUtil.getInitializingMessages(this.getScenario());
		for(Message m : firstMessage){
			Role firstSender = m.getSender();
			Role firstReceiver = m.getReceiver();
			if(!(m.getModelElement() == messageEvent.getTypedElement() && (m.getModelElement().getUpperBound()!=-1 || m.getCollectionModification()==messageEvent.getCollectionOperation())))
				continue;
			if(!firstSender.isStatic()) {
				getRoleBindings().getRoleBindings().put(firstSender, messageEvent.getSendingObject());
			}
			if(!firstReceiver.isStatic()) {
				getRoleBindings().getRoleBindings().put(firstReceiver, messageEvent.getReceivingObject());
			}
			
			for(int i=0;i<m.getParameters().size();i++){
				ParameterBinding binding = m.getParameters().get(i);
				if (binding.getBindingExpression() instanceof VariableBindingParameterExpression) {
					Variable variable = ((VariableBindingParameterExpression)binding.getBindingExpression()).getVariable().getValue();
					if(variable instanceof Role) {
						Role role = (Role) variable;
						EObject eObjectToBind = (EObject) messageEvent.getParameterValues().get(i).getValue();
						if (role.getType().isInstance(eObjectToBind)) {
							getRoleBindings().getRoleBindings().put(role, eObjectToBind);
						}else {
							multiActiveScenarioInitializations.getActiveScenarioToActiveScenarioProgressMap().put(
									this, 
									ActiveScenarioProgress.COLD_VIOLATION);
						}
					}
				}
			}
		}
		
		// 4) Create dynamic role bindings
		EList<RoleBindingConstraint> roleBindingConstraints = new BasicEList<RoleBindingConstraint>();
		roleBindingConstraints.addAll(this.getScenario().getRoleBindings());
		
		RoleBindingConstraint roleBindingConstraint = null;
		while ((roleBindingConstraint = getNextFulfillableRoleBinding(roleBindingConstraints)) != null) {
			// JOEL:
			// for each active scenario in the multiActiveScenarioInitializations
			// 1. remove this active scenario from the map
			// 2. re-add the first active scenario for the first object to be bound to its role 
			// 3. re-add further copies of the active scenario for each further objects to be bound to its role
			Set<ActiveScenario> initializedActiveScenariosSoFar = getInitializedActiveScenariosSoFar(
					multiActiveScenarioInitializations);
			
			for (ActiveScenario activeScenario : initializedActiveScenariosSoFar) {
				multiActiveScenarioInitializations.getActiveScenarioToActiveScenarioProgressMap().removeKey(activeScenario);

				EList<EObject> eObjectForRoleList = evaluateRoleBindingConstraint(
						roleBindingConstraint, activeScenario, smlRuntimeState.getDynamicObjectContainer());

				if (eObjectForRoleList.isEmpty()) {
					// TODO query the scenario here, after language has been updated. JOEL: What does that mean? 
					// -- We debated how a scenario should behave when a role binding cannot be assigned (null or empty collection)
					// the idea was to specify whether dynamic bindings are 'strict'  or not.
					// 'strict': safety violation, 'non-strict': interrupt.
					// Implementing the non-strict semantics for now.
					boolean strictBindings = false;
					if(strictBindings) {
						multiActiveScenarioInitializations.getActiveScenarioToActiveScenarioProgressMap().put(
								activeScenario, 
								ActiveScenarioProgress.SAFETY_VIOLATION);
					}else{
						multiActiveScenarioInitializations.getActiveScenarioToActiveScenarioProgressMap().put(
								activeScenario, 
								ActiveScenarioProgress.COLD_VIOLATION);
					}
				}

				if(eObjectForRoleList.size() > 1 && !roleBindingConstraint.getRole().isMultiRole()) {
					throw new RuntimeException("Cannot bind a non-multi role to a collection of elements. Role: " 
							+ roleBindingConstraint.getRole().getName() 
							+ " bound during the activation of scenario: " 
							+ this.getScenario().getName());
				}
				
				for (int i=0; i < eObjectForRoleList.size(); i++){
					
					EObject eObjectForRole = eObjectForRoleList.get(i);
					Role role = roleBindingConstraint.getRole();
					
					if(eObjectForRole == null 
							|| eObjectForRole.eClass().equals(role.getType())
							|| eObjectForRole.eClass().getEAllSuperTypes().contains(role.getType())){
						// original active scenario can be updated and re-added for the first binding
						// for all further bindings (many roles), a copy must be created.
						if(i > 0) 
							activeScenario = ActiveScenarioHelper.createCopyOfActiveScenario(activeScenario);
						
						activeScenario
							.getRoleBindings()
							.getRoleBindings()
							.put(role, eObjectForRole);
						
						if (logger.isDebugEnabled()) {
								logger.debug("Binding dynamic role: " + role + " to EObject: " + EObjectHelper.getName(eObjectForRole));
						}
						
						// just collect them for now in the keyset of the map. Assignment of active scenario progress happens below.
						multiActiveScenarioInitializations.getActiveScenarioToActiveScenarioProgressMap().put(activeScenario, null);
					}
					
				}
			}
		}

		if(roleBindingConstraints.size() > 0) {
			String roleNames = roleBindingConstraints.get(0).getRole().getName();
			for(int i = 1; i < roleBindingConstraints.size(); ++i)
				roleNames = roleNames + ", " + roleBindingConstraints.get(i).getRole().getName();

			logger.warn("could not bind all roles in scenario '" + getScenario().getName() + "': " + roleNames);
		}
		
		
		// 5) Iterate over all active scenarios and continue with their initialization.
		for(Map.Entry<ActiveScenario, ActiveScenarioProgress> multiActiveScenarioInitializationsEntry :
				multiActiveScenarioInitializations.getActiveScenarioToActiveScenarioProgressMap()) {

			ActiveScenario activeScenario = multiActiveScenarioInitializationsEntry.getKey();
			ActiveScenarioProgress activeScenarioProgress = multiActiveScenarioInitializationsEntry.getValue();

			if(activeScenarioProgress == ActiveScenarioProgress.COLD_VIOLATION
					|| activeScenarioProgress == ActiveScenarioProgress.SAFETY_VIOLATION)
				continue;
			
			// Initialize scenario body
			activeScenario.getMainActiveInteraction().init(activeScenario.getRoleBindings(), null, activeScenario);
			
			ActiveScenarioProgress progress = ActiveScenarioProgress.SAFETY_VIOLATION;
			// Enable scenario's first element
			try {
				progress = activeScenario.getMainActiveInteraction().enable(activeScenario, smlRuntimeState);
			} catch(ExpressionEvaluationException e) {
				logger.info("An exception occured during evaluation of expression: "+e.getLocalizedMessage());
			}
			
			// The enabled element needs to be continued.
			// Thats happens if the first element in a scenario is a var or a condition.
			while(progress == ActiveScenarioProgress.CONTINUE){
				// Process vars, conditions and so on (no messages).
				progress = activeScenario.performStep(messageEvent, smlRuntimeState);
			}

			if(progress == ActiveScenarioProgress.SAFETY_VIOLATION){
				multiActiveScenarioInitializationsEntry.setValue(ActiveScenarioProgress.SAFETY_VIOLATION);
			}else{

//				JOEL: This seems obsolete. It will be triggered anyways by 
//				activeScenario.performStep(), which is called subsequently in 
//				ActiveScenarioHelper.createActiveScenario
//				// Updates enabled MessageEvents
//				activeScenario.getMainActiveInteraction().updateMessageEvents(activeScenario, smlRuntimeState);

				if(logger.isDebugEnabled()){
					logger.debug("Initialized scenario " + activeScenario.getScenario().getName()+".");
				}
				multiActiveScenarioInitializationsEntry.setValue(ActiveScenarioProgress.PROGRESS);
			}
			
		}
		
		return multiActiveScenarioInitializations;
		
	}

	/**
	 * Returns all active scenarios in the {@code multiActiveScenarioInitializations} map
	 * for which no safety violations or interrupts were assigned yet.
	 * @param multiActiveScenarioInitializations
	 * @return
	 */
	private Set<ActiveScenario> getInitializedActiveScenariosSoFar(
			MultiActiveScenarioInitializations multiActiveScenarioInitializations) {
		Set<ActiveScenario> initializedActiveScenariosSoFar = new HashSet<ActiveScenario>();
		for (Map.Entry<ActiveScenario, ActiveScenarioProgress> activeScenarioToActiveScenarioProgressMapEntry : multiActiveScenarioInitializations.getActiveScenarioToActiveScenarioProgressMap().entrySet()) {
			if (activeScenarioToActiveScenarioProgressMapEntry.getValue() != ActiveScenarioProgress.COLD_VIOLATION
					&& activeScenarioToActiveScenarioProgressMapEntry.getValue() != ActiveScenarioProgress.SAFETY_VIOLATION)
				initializedActiveScenariosSoFar.add(activeScenarioToActiveScenarioProgressMapEntry.getKey());
		}
		return initializedActiveScenariosSoFar;
	}
	
	/**
	 * Return next role binding constraint which can be fulfilled, due already having bound all
	 * roles requires for evaluation.
	 */
	private RoleBindingConstraint getNextFulfillableRoleBinding(EList<RoleBindingConstraint> roleBindingConstraints) {
		RoleBindingConstraint result = null;
		EMap<Role, EObject> roleBindings = getRoleBindings().getRoleBindings();
		
		for(RoleBindingConstraint roleBindingConstraint : roleBindingConstraints) {
			BindingExpression bindingExpression = roleBindingConstraint.getBindingExpression();
			if(bindingExpression instanceof FeatureAccessBindingExpression) {
				FeatureAccess fa = ((FeatureAccessBindingExpression)bindingExpression).getFeatureaccess();
				EObject target = fa.getTarget();
				if(roleBindings.containsKey(target) || target instanceof EClass) { // is target of feature access already bound?
					boolean unboundParameter = false;
					for(Expression expr : fa.getParameters()) { // are all parameters, if they are variables, already bound?
						if(expr instanceof VariableValue) {
							Variable var = ((VariableValue)expr).getValue();
							if(var instanceof Role && !roleBindings.containsKey(var)) {
								unboundParameter = true;
								break;
							}
						}
					}
					
					if(!unboundParameter) {
						// can evaluate the roleBindingConstraint, all dependencies are already bound
						result = roleBindingConstraint;
						break;
					}
				}
			}
		}
		
		if(result != null)
			roleBindingConstraints.remove(result);

		return result;
	}

	@SuppressWarnings("unchecked")
	private static EList<EObject> evaluateRoleBindingConstraint(RoleBindingConstraint roleBindingConstraint, ActiveScenario activeScenario, DynamicObjectContainer dynamicObjectContainer){
		EList<EObject> result = new BasicEList<EObject>();						
		
		BindingExpression bindingExpression = roleBindingConstraint.getBindingExpression();
		boolean isFeatureBindingExpression = bindingExpression instanceof FeatureAccessBindingExpression;
		
		if(isFeatureBindingExpression){
			EList<EObject> resultDynamic = new BasicEList<EObject>();									
			FeatureAccess fa = ((FeatureAccessBindingExpression)bindingExpression).getFeatureaccess();

			EObject referencedObject = getReferencedObject(fa, activeScenario, dynamicObjectContainer);
			
			if (referencedObject == null)
				return result;
			
			Value featureValue = fa.getValue();
			
			if(featureValue instanceof StructuralFeatureValue) {
				EStructuralFeature structualFeature = ((StructuralFeatureValue)featureValue).getValue();
				
				if(structualFeature.isMany()) {
					EList<EObject> list = (EList<EObject>)referencedObject.eGet(structualFeature);
					if(!list.isEmpty()) {
						if(fa.getCollectionAccess() != null) {
							CollectionOperation collectionOperation = fa.getCollectionAccess().getCollectionOperation();
							if(collectionOperation == CollectionOperation.FIRST) {
								resultDynamic.add(list.get(0));
							} else if (collectionOperation == CollectionOperation.LAST) {
								resultDynamic.add(list.get(list.size()-1));
							} else {
								throw new UnsupportedOperationException("Collection operation " + collectionOperation +  " is not yet supported at this point.");
							} 
						}else {
							resultDynamic = list;
						}
					} else {
						resultDynamic.add(null);
					}
				} else {
					resultDynamic.add((EObject)referencedObject.eGet(structualFeature));
				}				
			} else if(featureValue instanceof OperationValue) {
				BasicInterpreter interpreter = new BasicInterpreter(dynamicObjectContainer);
				EOperation operation = ((OperationValue)featureValue).getValue();
				EList<Expression> parameters = fa.getParameters();
				
				EList<Object> parameterValues = new BasicEList<Object>(parameters.size());
				for(int i = 0; i < parameters.size(); i++) {
					Object value = interpreter.toDynamicRecursive(interpreter.evaluate(parameters.get(i), activeScenario));
					parameterValues.add(value);
				}

				Object invocationResult = null;
				try {
					invocationResult = referencedObject.eInvoke(operation, parameterValues);
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
				
				if(invocationResult == null)
					resultDynamic.add(null);
				else if(invocationResult instanceof EObject)
					resultDynamic.add((EObject)invocationResult);
				else
					resultDynamic.addAll((Collection<? extends EObject>)invocationResult);
			} else {
				throw new UnsupportedOperationException("Unsupported FeatureAccess value type: " + featureValue);
			}
			
			for(EObject dynamicObject : resultDynamic) {
				if(dynamicObject == null) {
					logger.info("Object is NULL! Result of evaluating role binding is NULL."
							+ " Scenario: " + activeScenario.getScenario().getName()
							+ " Role: " + roleBindingConstraint.getRole()
							+ " EObject from dynamic ObjSys: " + dynamicObject
							+ " FeatureAccess.value: " + fa.getValue());
					// FIXME TODO the user specification is inconsistent if a scenario wants to send something to or from this role!
					// We want to allow binding expressions that result to null. Consider the following example:
					//	/*
					//	 * When a car approaches an obstacle on the blocked lane, an according event will occur
					//	 */
					//	assumption scenario ApproachingObstacleOnBlockedLaneAssumption
					//	with dynamic bindings [
					//		bind currentArea to car.inArea
					//		bind nextArea to currentArea.next
					//		bind obstacle to nextArea.obstacle
					//	]{
					//		message env->car.carMovesToNextArea()
					//		interrupt if [obstacle == null]
					//		message strict requested env->car.setApproachingObstacle(obstacle)			
					//	} constraints [
					//		forbidden message env->car.carMovesToNextArea()
					//		forbidden message env->car.carMovesToNextAreaOnOvertakingLane()
					//	]
					//continue;
				}
				
				result.add(DynamicObjectContainerHelper.getStaticObjectFromDynamicObject(dynamicObject, dynamicObjectContainer));
			}				
		} else {
			throw new UnsupportedOperationException("Method currently only supports dynamic role bindings on simple feature accesses.");
		}
		
		return result;
	}

	private static EObject getReferencedObject(FeatureAccess fa, ActiveScenario activeScenario, DynamicObjectContainer dynamicObjectContainer) {
		EObject target = fa.getTarget();
		
		if(target instanceof EClass) {
			EObject contextHelperEObjectStatic = activeScenario.getContextHelperClassInstances().get(target);
			return createDynamicCopyOf(contextHelperEObjectStatic, dynamicObjectContainer);
		} else if (target instanceof EObject) {
			EObject eObjectBoundToSourceRole = activeScenario.getRoleBindings().getRoleBindings().get(target);
			return dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(eObjectBoundToSourceRole);
		} else {
			throw new UnsupportedOperationException("Feature binding expression must either refer to a role or a helper class.");			
		}
	}

	private static EObject createDynamicCopyOf(EObject contextHelperEObjectStatic,
			DynamicObjectContainer dynamicObjectContainer) {
		
		EObject contextHelperEObjectDynamic = EcoreUtil.copy(contextHelperEObjectStatic);
		for (EReference eReference : contextHelperEObjectStatic.eClass().getEAllReferences()) {
			if (eReference.isDerived()) continue;
			if (eReference.isMany()){
				for (EObject referencedObjectStatic : (EList<EObject>) contextHelperEObjectStatic.eGet(eReference)) {
					((EList<EObject>)contextHelperEObjectDynamic.eGet(eReference)).add(dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(referencedObjectStatic));
				}
			}else{
				contextHelperEObjectDynamic.eSet(eReference, dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(contextHelperEObjectStatic.eGet(eReference)));
			}
		}
		
		return contextHelperEObjectDynamic;
	}

	public boolean isBlocked(MessageEvent MessageEvent) {
		ActivePart activeInteraction = this.getMainActiveInteraction();
		
		BlockedType blockedType = activeInteraction.isBlocked(MessageEvent , this.isInStrictState());
		switch(blockedType) {
			case ENABLED:
				return false;
			case CUT_NOT_STRICT:
				return false;
			case FORBIDDEN:
				return true;
			case IGNORE:
				return false;
			case CUT_STRICT:
				return false;
			case STRICT_CONSIDERED:
				return true;
			case BLOCKED:
				return true;
			case INTERRUPTED:
				return false;
			default:
				throw new IllegalArgumentException("BlockedType not supported!");
		}		
	}
}
