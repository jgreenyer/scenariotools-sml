/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic;

import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.WaitCondition;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.ActiveWaitCondition;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.impl.ActivePartImpl;

public abstract class ActiveWaitConditionLogic extends ActivePartImpl implements ActiveWaitCondition {
	
	@Override
	public void init(ActiveScenarioRoleBindings roleBindings, 
			ActivePart parentActivePart,
			ActiveScenario activeScenario) {
		
		super.init(roleBindings, parentActivePart, activeScenario);
	}

	/**
	 * Returns {@link ActiveScenarioProgress#INTERACTION_END INTERACTION_END}, if the condition
	 *         evaluates to <code>true</code>, {@link ActiveScenarioProgress#NO_PROGRESS
	 *         NO_PROGRESS} otherwise.
	 */
	@Override
	public ActiveScenarioProgress performStep(MessageEvent event, ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {

		boolean conditionFulfilled = isWaitConditionFulfilled(activeScenario, smlRuntimeState);
		
		if (conditionFulfilled)
			return ActiveScenarioProgress.INTERACTION_END;
		else{
			return computeProgressFromComputedViolationKind(event, activeScenario);
		}
	}
	
	@Override
	public ActiveScenarioProgress postPerformStep(MessageEvent event, ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		
		boolean conditionFulfilled = isWaitConditionFulfilled(activeScenario, smlRuntimeState);
		
		if (conditionFulfilled)
			return ActiveScenarioProgress.INTERACTION_END;
		else{
			return ActiveScenarioProgress.NO_PROGRESS;
		}
	}

	private boolean isWaitConditionFulfilled(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		return evaluateConditionExpression(
				((WaitCondition) this.getInteractionFragment()).getConditionExpression(), 
				smlRuntimeState,
				activeScenario, 
				this);
	}
	
	@Override
	public ActiveScenarioProgress enable(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		return ActiveScenarioProgress.CONTINUE;
	}
	
	@Override
	public boolean isInRequestedState() {
		return ((WaitCondition) this.getInteractionFragment()).isRequested();
	}
	
	@Override
	public boolean isInStrictState() {
		return ((WaitCondition) this.getInteractionFragment()).isStrict();
	}
}
