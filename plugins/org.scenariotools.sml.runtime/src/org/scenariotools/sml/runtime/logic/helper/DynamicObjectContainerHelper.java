package org.scenariotools.sml.runtime.logic.helper;

import java.util.Collection;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.scenariotools.sml.runtime.DynamicObjectContainer;

public class DynamicObjectContainerHelper {
	
	public static EObject getStaticObjectFromDynamicObject(EObject dynamicEObject, DynamicObjectContainer dynamicObjectContainer){
		for(Entry<EObject, EObject> entry : dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap()){
			if(entry.getValue() == dynamicEObject){
				return entry.getKey();
			}
		}
		return null;
	}

	public static EList<EObject> getStaticObjectEListFromDynamicObjectEList(EList<EObject> dynamicEObjectEList, DynamicObjectContainer dynamicObjectContainer){
		EList<EObject> resultStaticEObjectList = new BasicEList<EObject>(); 
		
		for(EObject dynamicObject : dynamicEObjectEList){
			resultStaticEObjectList.add(getStaticObjectFromDynamicObject(dynamicObject, dynamicObjectContainer));
		}
		return resultStaticEObjectList;
	}

	
	public static EObject getDynamicHelperClassInstanceFromStatic(EObject eObjectWithReferencesToStaticObjects, DynamicObjectContainer dynamicObjectContainer){
		EObject eObjectWithReferencesToDynamicObjects = EcoreUtil.copy(eObjectWithReferencesToStaticObjects);
		
		for (EReference eReference : eObjectWithReferencesToStaticObjects.eClass().getEAllReferences()) {
			if (!eReference.isChangeable()) continue;
			if(eReference.isMany()){
				eObjectWithReferencesToDynamicObjects.eUnset(eReference);
				for (EObject eObject : (Collection<EObject>)eObjectWithReferencesToStaticObjects.eGet(eReference)) {
					EObject staticObject = (EObject) eObjectWithReferencesToStaticObjects.eGet(eReference);
					EObject dynamicObject = dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(staticObject);
					((Collection<EObject>)eObjectWithReferencesToDynamicObjects.eGet(eReference)).add(dynamicObject);
				}
			}else{
				EObject staticObject = (EObject) eObjectWithReferencesToStaticObjects.eGet(eReference);
				EObject dynamicObject = dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(staticObject);
				eObjectWithReferencesToDynamicObjects.eSet(
						eReference, dynamicObject);
						
			}
			
		}
		
		return eObjectWithReferencesToDynamicObjects;
	}
	
}
