/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic;

import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.ActiveConstraintInterrupt;
import org.scenariotools.sml.runtime.ActiveInteraction;
import org.scenariotools.sml.runtime.impl.ActiveConstraintImpl;

public class ActiveConstraintInterruptLogic extends ActiveConstraintImpl implements ActiveConstraintInterrupt {

	@Override
	public void addToParentSpecificConstraintList(ActiveInteraction parant,
			MessageEvent constraintMessage) {
		parant.getInterruptingEvents().add(constraintMessage);
	}
}
