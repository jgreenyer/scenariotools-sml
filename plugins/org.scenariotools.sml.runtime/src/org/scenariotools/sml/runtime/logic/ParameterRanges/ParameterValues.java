/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic.ParameterRanges;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

public interface ParameterValues<T> extends Iterable<T>{
	
	public boolean contains(T value);
	
	public default boolean isNull(){ 
		return false; 
	}

	public static class Factory{
		
		public static ParameterValues<Integer> getIntegerParameterValues(EList<IntegerParameterRange> ranges, EList<Integer> values){
			return new IntegerParameterValues(ranges, values);
		}
		
		public static <T> ParameterValues<T> getListParameterValues(EList<T> ranges){
			return new ListParameterValues<T>(ranges);
		}
		
		public static <T> ParameterValues<T> getEmptyParameterValues(){
			return new EmptyParameterValues<T>();
		}
		
		public static IntegerParameterRange getIntegerParameterRange(int lowerBound, int upperBound){
			return new IntegerParameterRange(lowerBound, upperBound);
		}
		
		public static ParameterValues<Boolean> getBooleanParameterValues(){
			EList<Boolean> list = new BasicEList<Boolean>();
			list.add(true);
			list.add(false);
			return new ListParameterValues<Boolean>(list);
		}
	}	
}
