/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic.scenario;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.EList;
import org.scenariotools.sml.Alternative;
import org.scenariotools.sml.BindingExpression;
import org.scenariotools.sml.Case;
import org.scenariotools.sml.ValueParameterExpression;
import org.scenariotools.sml.Interaction;
import org.scenariotools.sml.InteractionFragment;
import org.scenariotools.sml.InterruptCondition;
import org.scenariotools.sml.Loop;
import org.scenariotools.sml.Message;
import org.scenariotools.sml.ModalMessage;
import org.scenariotools.sml.Parallel;
import org.scenariotools.sml.ParameterBinding;
import org.scenariotools.sml.WildcardParameterExpression;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.VariableBindingParameterExpression;
import org.scenariotools.sml.VariableFragment;
import org.scenariotools.sml.ViolationCondition;
import org.scenariotools.sml.WaitCondition;
import org.scenariotools.sml.runtime.ActiveAlternative;
import org.scenariotools.sml.runtime.ActiveCase;
import org.scenariotools.sml.runtime.ActiveConstraint;
import org.scenariotools.sml.runtime.ActiveInteraction;
import org.scenariotools.sml.runtime.ActiveInterruptCondition;
import org.scenariotools.sml.runtime.ActiveLoop;
import org.scenariotools.sml.runtime.ActiveMessageParameter;
import org.scenariotools.sml.runtime.ActiveMessageParameterWithBindToVar;
import org.scenariotools.sml.runtime.ActiveMessageParameterWithValueExpression;
import org.scenariotools.sml.runtime.ActiveMessageParameterWithWildcard;
import org.scenariotools.sml.runtime.ActiveModalMessage;
import org.scenariotools.sml.runtime.ActiveParallel;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.ActiveVariableFragment;
import org.scenariotools.sml.runtime.ActiveViolationCondition;
import org.scenariotools.sml.runtime.ActiveWaitCondition;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.plugin.Activator;

public class ScenarioBuilder {
	
	protected static Logger logger = Activator.getLogManager().getLogger(
			ScenarioBuilder.class.getName());

	public ActiveScenario buildScenario(Scenario scenario){
		
		ActiveScenario activeScenario =  RuntimeFactory.eINSTANCE.createActiveScenario();
		ActiveScenarioRoleBindings roleBindings = RuntimeFactory.eINSTANCE.createActiveScenarioRoleBindings();
		
		activeScenario.setRoleBindings(roleBindings);
		activeScenario.setScenario(scenario);
		
		ActiveInteraction ownedActiveInteraction = 
				buildActiveInteraction(scenario.getOwnedInteraction());
		
		activeScenario.setMainActiveInteraction((ActivePart) ownedActiveInteraction);
		
		
		if(logger.isDebugEnabled()){
			logger.debug("Initialized scenario " + scenario.getName()+".");
		}
		
		return activeScenario;
	}
	
	/**
	 * @param parent
	 * @param interactionFragments
	 * @return
	 */
	private boolean addChildInteractionFragment(ActivePart parent, InteractionFragment interactionFragment){
		
		if(interactionFragment instanceof ModalMessage){
			
			ModalMessage modalMessage = (ModalMessage) interactionFragment;
			ActiveModalMessage activeModalMessage = buildActiveModalMessage(modalMessage);
			parent.getNestedActiveInteractions().add(activeModalMessage);
			
		}else if(interactionFragment instanceof Alternative){
			
			Alternative alternative = (Alternative) interactionFragment;
			ActiveAlternative activeAlternative = buildActiveAlternative(alternative);
			parent.getNestedActiveInteractions().add(activeAlternative);
			
		}else if(interactionFragment instanceof Parallel){
			
			Parallel parallel = (Parallel) interactionFragment;
			ActiveParallel activeParallel = buildActiveParallel(parallel);
			parent.getNestedActiveInteractions().add(activeParallel);
			
		}else if(interactionFragment instanceof Interaction){
			
			Interaction interaction = (Interaction) interactionFragment;
			ActiveInteraction activeInteraction = buildActiveInteraction(interaction);
			parent.getNestedActiveInteractions().add((ActivePart) activeInteraction);
			
		}else if(interactionFragment instanceof Loop){
			
			Loop loop = (Loop) interactionFragment;
			ActiveLoop activeLoop = buildActiveLoop(loop);
			parent.getNestedActiveInteractions().add(activeLoop);
			
		}else if(interactionFragment instanceof VariableFragment){
			
			VariableFragment variableFragment = (VariableFragment) interactionFragment;
			ActiveVariableFragment activeVariableFragment = buildActiveVariableFragment(variableFragment);
			parent.getNestedActiveInteractions().add(activeVariableFragment);
			
		}else if(interactionFragment instanceof InterruptCondition){
			
			InterruptCondition interruptCondition = (InterruptCondition) interactionFragment;
			ActiveInterruptCondition activeInterruptCondition = buildActiveInterruptCondition(interruptCondition);
			parent.getNestedActiveInteractions().add(activeInterruptCondition);
			
		}else if(interactionFragment instanceof ViolationCondition){
			
			ViolationCondition violationCondition = (ViolationCondition) interactionFragment;
			ActiveViolationCondition activeViolationCondition = buildActiveViolationCondition(violationCondition); 
			parent.getNestedActiveInteractions().add(activeViolationCondition);
			
		}else if(interactionFragment instanceof WaitCondition) {

			WaitCondition waitCondition = (WaitCondition) interactionFragment;
			ActiveWaitCondition activeWaitCondition = buildActiveWaitCondition(waitCondition);
			parent.getNestedActiveInteractions().add(activeWaitCondition);

		}else{
			throw new RuntimeException("Unsupported type in ScenarioBuilder: " + interactionFragment.toString());
		}
	
		return true;
	
	}
	
	/**
	 * @param parent
	 * @param interactionFragments
	 * @return
	 */
	private boolean addChildInteractionFragments(ActivePart parent, EList<InteractionFragment> interactionFragments){
		
		for(InteractionFragment interactionFragment : interactionFragments){
			addChildInteractionFragment(parent, interactionFragment);
		}
		return true;
	}
	
	private void addParameter(EList<ActiveMessageParameter> parameterList, Message message){
		for(ParameterBinding parameterBinding : message.getParameters()){
			BindingExpression bindingExpression = parameterBinding.getBindingExpression();
			ActiveMessageParameter activeMessageParameter = null;
			if(bindingExpression instanceof ValueParameterExpression){
				
				ValueParameterExpression valueParameterExpression = (ValueParameterExpression) bindingExpression;
				activeMessageParameter = buildActiveMessageParameterWithValueExpression(valueParameterExpression);
				
			}else if(bindingExpression instanceof WildcardParameterExpression){
				
				WildcardParameterExpression wildcardParameterExpression = (WildcardParameterExpression) bindingExpression;
				activeMessageParameter = buildActiveMessageParameterWithWildcard(wildcardParameterExpression);
				
			}else if(bindingExpression instanceof VariableBindingParameterExpression){
				
				VariableBindingParameterExpression variableBindingParameterExrpession = (VariableBindingParameterExpression) bindingExpression;
				activeMessageParameter = buildActiveMessageParameterWithBindToVar(variableBindingParameterExrpession);
				
			}else{
				throw new RuntimeException("Unsupported parameter type in ScenarioBuilder: " + bindingExpression.toString());
			}
			
			parameterList.add(activeMessageParameter);
		}
	}

	private ActiveInteraction buildActiveInteraction(Interaction interaction){
		ActiveInteraction activeInteraction = RuntimeFactory.eINSTANCE.createActiveInteraction();
		activeInteraction.setInteractionFragment(interaction);
		
		addChildInteractionFragments((ActivePart) activeInteraction, interaction.getFragments());
		
		if (interaction.getConstraints() != null) {
			for (Message forbiddenMessage : interaction.getConstraints().getForbidden()) {
				ActiveConstraint activeConstraint = RuntimeFactory.eINSTANCE.createActiveConstraintForbidden();
				activeConstraint.setMessage(forbiddenMessage);
				addParameter(activeConstraint.getActiveMessageParameters(), forbiddenMessage);
				activeInteraction.getActiveConstraints().add(activeConstraint);
			}
			for (Message ignoreMessage : interaction.getConstraints().getIgnore()) {
				ActiveConstraint activeConstraint = RuntimeFactory.eINSTANCE.createActiveConstraintIgnore();
				activeConstraint.setMessage(ignoreMessage);
				addParameter(activeConstraint.getActiveMessageParameters(), ignoreMessage);
				activeInteraction.getActiveConstraints().add(activeConstraint);
			}
			for (Message considerMessage : interaction.getConstraints().getConsider()) {
				ActiveConstraint activeConstraint = RuntimeFactory.eINSTANCE.createActiveConstraintConsider();
				activeConstraint.setMessage(considerMessage);
				addParameter(activeConstraint.getActiveMessageParameters(), considerMessage);
				activeInteraction.getActiveConstraints().add(activeConstraint);
			}
			for (Message interruptMessage : interaction.getConstraints().getInterrupt()) {
				ActiveConstraint activeConstraint = RuntimeFactory.eINSTANCE.createActiveConstraintInterrupt();
				activeConstraint.setMessage(interruptMessage);
				addParameter(activeConstraint.getActiveMessageParameters(), interruptMessage);
				activeInteraction.getActiveConstraints().add(activeConstraint);
			}
		}
		
		return activeInteraction;
	}
	
	private ActiveWaitCondition buildActiveWaitCondition(WaitCondition waitCondition) { 
		ActiveWaitCondition activeWaitCondition = RuntimeFactory.eINSTANCE.createActiveWaitCondition();
		activeWaitCondition.setInteractionFragment(waitCondition);
		return activeWaitCondition;
	}
	
	private ActiveViolationCondition buildActiveViolationCondition(ViolationCondition violationCondition) {
		
		ActiveViolationCondition activeViolationCondition = RuntimeFactory.eINSTANCE.createActiveViolationCondition();
		activeViolationCondition.setInteractionFragment(violationCondition);
		return activeViolationCondition;
	}
	
	private ActiveInterruptCondition buildActiveInterruptCondition(InterruptCondition interruptCondition) {
		ActiveInterruptCondition activeInterruptCondition = RuntimeFactory.eINSTANCE.createActiveInterruptCondition();
		activeInterruptCondition.setInteractionFragment(interruptCondition);
		return activeInterruptCondition;
	}

	private ActiveVariableFragment buildActiveVariableFragment(VariableFragment variableFragment) {
		ActiveVariableFragment activeVariableFragment = RuntimeFactory.eINSTANCE.createActiveVariableFragment();
		activeVariableFragment.setInteractionFragment(variableFragment);
		return activeVariableFragment;
	}

	/**
	 * @param loop
	 * @return
	 */
	private ActiveLoop buildActiveLoop(Loop loop) {
		ActiveLoop activeLoop = RuntimeFactory.eINSTANCE.createActiveLoop();
		activeLoop.setInteractionFragment(loop);
		
		addChildInteractionFragment(activeLoop, loop.getBodyInteraction());
		
		return activeLoop;
	}

	private ActiveParallel buildActiveParallel(Parallel parallel) {
		ActiveParallel activeParallel = RuntimeFactory.eINSTANCE.createActiveParallel();
		activeParallel.setInteractionFragment(parallel);
		
		for(Interaction nestedInteraction : parallel.getParallelInteraction()){
			addChildInteractionFragment(activeParallel, nestedInteraction);
		}
		
		return activeParallel;
	}

	private ActiveAlternative buildActiveAlternative(Alternative alternative) {
		
		ActiveAlternative activeAlternative = RuntimeFactory.eINSTANCE.createActiveAlternative();
		activeAlternative.setInteractionFragment(alternative);
		
		for (Case altCase : alternative.getCases()) {

			ActiveCase activeCase = RuntimeFactory.eINSTANCE.createActiveCase();
			activeAlternative.getNestedActiveInteractions().add(activeCase);
			activeCase.setCase(altCase);
			addChildInteractionFragment(activeCase, altCase.getCaseInteraction());
			
		}
		
		return activeAlternative;
	}

	private ActiveModalMessage buildActiveModalMessage(ModalMessage modalMessage) {

		ActiveModalMessage activeModalMessage = RuntimeFactory.eINSTANCE.createActiveModalMessage();
		activeModalMessage.setInteractionFragment(modalMessage);
		
		// Parameter 
		addParameter(activeModalMessage.getActiveMessageParameters(), (Message) modalMessage);
		
		return activeModalMessage;
	}
	
	private ActiveMessageParameterWithValueExpression buildActiveMessageParameterWithValueExpression(ValueParameterExpression valueParameterExpression){
		ActiveMessageParameterWithValueExpression activeParamWithExpression = RuntimeFactory.eINSTANCE.createActiveMessageParameterWithValueExpression();
		activeParamWithExpression.setParameter(valueParameterExpression);
		return activeParamWithExpression;
	}
	
	private ActiveMessageParameterWithWildcard buildActiveMessageParameterWithWildcard(WildcardParameterExpression wildcardParameterExpression){
		ActiveMessageParameterWithWildcard activeMessageParameterWithWildcard = RuntimeFactory.eINSTANCE.createActiveMessageParameterWithWildcard();
		activeMessageParameterWithWildcard.setParameter(wildcardParameterExpression);
		return activeMessageParameterWithWildcard;
	}
	
	private ActiveMessageParameterWithBindToVar buildActiveMessageParameterWithBindToVar(VariableBindingParameterExpression variableBindingParameterExpression){
		ActiveMessageParameterWithBindToVar activeMessageParameterWithBindToVar = RuntimeFactory.eINSTANCE.createActiveMessageParameterWithBindToVar();
		activeMessageParameterWithBindToVar.setParameter(variableBindingParameterExpression);
		return activeMessageParameterWithBindToVar;
	}
}
