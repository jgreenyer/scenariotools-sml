/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     Nils Glade, ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic.helper;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.ExpectationKind;
import org.scenariotools.sml.ModalMessage;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.ScenarioKind;
import org.scenariotools.sml.runtime.ActiveModalMessage;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.MultiActiveScenarioInitializations;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.emf.custom.MessageEventUniqueEList;
import org.scenariotools.sml.runtime.logic.SMLObjectSystemLogic;
import org.scenariotools.sml.runtime.logic.scenario.ScenarioBuilder;
import org.scenariotools.sml.runtime.util.RuntimeSwitch;

/**
 * Returns a collection of pairs of message events enabled in a scenario and the
 * execution kind of the modal message that enabled the event. This class never
 * returns null.
 */
public class EventGatheringVisitor {

	
	final EList<MessageEvent> listeningEnvironmentEvents = new MessageEventUniqueEList<>();
	Set<ExpectationKind> systemExpectationKind = EnumSet.noneOf(ExpectationKind.class);
	Set<ExpectationKind> environmentExpectationKind = EnumSet.noneOf(ExpectationKind.class);
	Map<ExpectationKind, List<MessageEvent>> systemEventsByExpectationKind = new EnumMap<>(ExpectationKind.class);
	private Map<ExpectationKind, List<MessageEvent>> environmentEventsByExpectationKind = new EnumMap<>(
			ExpectationKind.class);
	EList<MessageEvent> initializingMessageEvents;
	final SMLRuntimeState runtimeState;

	public EventGatheringVisitor(SMLRuntimeState state) {
		this.runtimeState = state;
		for (ExpectationKind x : ExpectationKind.values()) {
			systemEventsByExpectationKind.put(x, new MessageEventUniqueEList<>());
			environmentEventsByExpectationKind.put(x, new MessageEventUniqueEList<>());
		}
	}
	/**
	 * returns the subset of message events whose sending object is controllable or uncontrollable.
	 * @param controllable
	 * @param events
	 * @return
	 */
	private Collection<MessageEvent> select(boolean controllable, Collection<MessageEvent> events) {
		List<MessageEvent> result = new MessageEventUniqueEList<MessageEvent>();
		for (MessageEvent m : events)
			if (runtimeState.getObjectSystem().isControllable(m.getSendingObject()) == controllable) {
				result.add(m);
			}
		return result;
	}

	public void visitAll() {
		// First, collect all relevant message events from active copies.
		for (ActiveScenario scenario : runtimeState.getActiveScenarios()) {
			// collect all environment events for unassumed behavior.
			listeningEnvironmentEvents.addAll(select(false, scenario.getAlphabet()));
			if (scenario.getScenario().getKind() == ScenarioKind.GUARANTEE) {
				gatherSystemEvents(scenario);
			} else if (scenario.getScenario().getKind() == ScenarioKind.ASSUMPTION) {
				gatherEnvironmentEvents(scenario);
			}

		}
		// TODO: Is this correct? (regarding different parameter values)
		getListeningEnvironmentEvents().removeIf(
				e -> ((SMLObjectSystem) runtimeState.getObjectSystem()).isNonSpontaneousMessageEvent(e));
		getInitializingEnvironmentEvents().removeIf(
				e -> ((SMLObjectSystem) runtimeState.getObjectSystem()).isNonSpontaneousMessageEvent(e));
	
		// Then, remove all message events that are blocked by guarantee or
		// assumption scenarios.
		for (ActiveScenario scenario : runtimeState.getActiveScenarios()) {
			if (scenario.getScenario().getKind() == ScenarioKind.GUARANTEE) {
				for (ExpectationKind k : systemEventsByExpectationKind.keySet()) {
					if (k != ExpectationKind.NONE) {
						filterCollection(systemEventsByExpectationKind.get(k), scenario);
					}
				}
			} else if (scenario.getScenario().getKind() == ScenarioKind.ASSUMPTION) {
				for (ExpectationKind k : environmentEventsByExpectationKind.keySet()) {
						filterCollection(environmentEventsByExpectationKind.get(k), scenario);
				}
					// assumption scenarios also block spontaneously occuring
					// and initializing message events.
					filterCollection(getListeningEnvironmentEvents(), scenario);
					filterCollection(getInitializingEnvironmentEvents(), scenario);
					
			}

		}
		listeningEnvironmentEvents.addAll(environmentEventsByExpectationKind.get(ExpectationKind.NONE));
	}

	public void visitAssumptionScenariosOnly() {
		// First, collect all relevant message events from active copies.
		for (ActiveScenario scenario : runtimeState.getActiveScenarios()) {
			if (scenario.getScenario().getKind() == ScenarioKind.ASSUMPTION) {
				// collect all environment events for unassumed behavior.
				listeningEnvironmentEvents.addAll(select(false, scenario.getAlphabet()));
				if (scenario.getScenario().getKind() == ScenarioKind.GUARANTEE) {
					gatherSystemEvents(scenario);
				} else if (scenario.getScenario().getKind() == ScenarioKind.ASSUMPTION) {
					gatherEnvironmentEvents(scenario);
				}

			}
		}
		getListeningEnvironmentEvents().removeIf(
				e -> ((SMLObjectSystem) runtimeState.getObjectSystem()).isNonSpontaneousMessageEvent(e));
		getInitializingEnvironmentEvents().removeIf(
				e -> ((SMLObjectSystem) runtimeState.getObjectSystem()).isNonSpontaneousMessageEvent(e));
		// Then, remove all message events that are blocked by guarantee or
		// assumption scenarios.
		for (ActiveScenario scenario : runtimeState.getActiveScenarios()) {
			if (scenario.getScenario().getKind() == ScenarioKind.GUARANTEE) {
				continue;
			} else if (scenario.getScenario().getKind() == ScenarioKind.ASSUMPTION) {
				for (ExpectationKind k : environmentEventsByExpectationKind.keySet()) {
					filterCollection(environmentEventsByExpectationKind.get(k), scenario);
				}
				// assumption scenarios also block spontaneously occuring
				// and initializing message events.
				filterCollection(getListeningEnvironmentEvents(), scenario);
				filterCollection(getInitializingEnvironmentEvents(), scenario);
			}
		}
		listeningEnvironmentEvents.addAll(environmentEventsByExpectationKind.get(ExpectationKind.NONE));
	}
	private void gatherEnvironmentEvents(ActiveScenario scenario) {
		RuntimeSwitch<Void> fragmentSwitch = new RuntimeSwitch<Void>() {

			@Override
			public Void caseActivePart(ActivePart object) {
				for (ActivePart nested : object.getEnabledNestedActiveInteractions())
					doSwitch(nested);
				return null;
			}

			@Override
			public Void caseActiveModalMessage(ActiveModalMessage object) {
				ModalMessage message = (ModalMessage) object.getInteractionFragment();
				if(runtimeState.getObjectSystem().isControllable(object.getEnabledEvents().get(0).getSendingObject()))
					return null;
				environmentExpectationKind.add(message.getExpectationKind());
//				boolean isSpontaneous = ((SMLObjectSystem) runtimeState.getObjectSystem())
//						.isNonSpontaneousMessageEvent(object.getEnabledEvents().get(0));
				// message events that are non-spontaneous && monitored have to
				// be requested somewhere else.
				// (monitored <=> ExpectationKind != NONE).
				if (/*isSpontaneous || */!message.isMonitored()) {
					environmentEventsByExpectationKind.get(message.getExpectationKind()).addAll(object.getEnabledEvents());
				}
				return null;
			}

		};
		fragmentSwitch.doSwitch(scenario.getMainActiveInteraction());
	}

	private void gatherSystemEvents(ActiveScenario scenario) {
		RuntimeSwitch<Void> fragmentSwitch = new RuntimeSwitch<Void>() {
			@Override
			public Void caseActivePart(ActivePart object) {
				for (ActivePart nested : object.getEnabledNestedActiveInteractions())
					doSwitch(nested);
				return null;
			}

			@Override
			public Void caseActiveModalMessage(ActiveModalMessage object) {
				ModalMessage message = (ModalMessage) object.getInteractionFragment();
				systemExpectationKind.add(message.getExpectationKind());
				if (message.getExpectationKind() != ExpectationKind.NONE && !message.isMonitored()) {
					systemEventsByExpectationKind.get(message.getExpectationKind()).addAll(object.getEnabledEvents());
				}
				return null;
			}

		};
		fragmentSwitch.doSwitch(scenario.getMainActiveInteraction());
	}

	private void filterCollection(Collection<MessageEvent> collection, ActiveScenario filter) {
		for (Iterator<MessageEvent> it = collection.iterator(); it.hasNext();) {
			MessageEvent event = it.next();
			if (filter.isBlocked(event)) {
				it.remove();
				// TODO store information.
			}
		}
	}

	public List<MessageEvent> getMessageEvents(boolean fromSystem, ExpectationKind k) {
		if (fromSystem) {
			return systemEventsByExpectationKind.getOrDefault(k, Collections.emptyList());
		} else {
			return environmentEventsByExpectationKind.getOrDefault(k, Collections.emptyList());
		}

	}

	/**
	 * 
	 * @return True if some system message event is expected to be sent in
	 *         committed execution kind.
	 */
	public boolean isCommittedSystemEventsExist() {
		return systemExpectationKind.contains(ExpectationKind.COMMITTED);
	}

	/**
	 *
	 * @return All committed message events that are sent by system objects and
	 *         not blocked by guarantee scenarios.
	 */
	public List<MessageEvent> getCommittedSystemEvents() {
		return systemEventsByExpectationKind.get(ExpectationKind.COMMITTED);
	}

	/**
	 * 
	 * @return True if some system message event is expected to be sent in
	 *         urgent execution kind.
	 */
	public boolean isUrgentSystemEventsExist() {
		return systemExpectationKind.contains(ExpectationKind.URGENT);
	}

	/**
	 *
	 * @return All urgent message events that are sent by system objects and not
	 *         blocked by guarantee scenarios.
	 */
	public List<MessageEvent> getUrgentSystemEvents() {
		return systemEventsByExpectationKind.get(ExpectationKind.URGENT);
	}

	/**
	 *
	 * @return All requested message events that are sent by system objects and
	 *         not blocked by guarantee scenarios.
	 */
	public List<MessageEvent> getRequestedSystemEvents() {
		return systemEventsByExpectationKind.get(ExpectationKind.REQUESTED);
	}

	/**
	 * 
	 * @return All eventually message events that are sent by environment
	 *         objects and not blocked by assumption scenarios.
	 */
	public List<MessageEvent> getEventuallySystemEvents() {
		return systemEventsByExpectationKind.get(ExpectationKind.EVENTUALLY);
	}

	/**
	 * 
	 * @return True if some system message event is expected to be sent in
	 *         requested execution kind.
	 */
	public boolean isRequestedSystemEventsExist() {
		return systemExpectationKind.contains(ExpectationKind.REQUESTED);
	}

	/**
	 * 
	 * @return True if some environment message event is expected to be sent in
	 *         committed execution kind.
	 */
	public boolean isCommittedEnvironmentEventsExist() {
		return environmentExpectationKind.contains(ExpectationKind.COMMITTED);
	}

	/**
	 * 
	 * @return All committed message events that are sent by environment objects
	 *         and not blocked by assumption scenarios.
	 */
	public List<MessageEvent> getCommittedEnvironmentEvents() {
		return environmentEventsByExpectationKind.get(ExpectationKind.COMMITTED);
	}
	
	/**
	 * 
	 * @return All urgent message events that are sent by environment objects
	 *         and not blocked by assumption scenarios.
	 */
	public Collection<? extends MessageEvent> getUrgentEnvironmentEvents() {
		return environmentEventsByExpectationKind.get(ExpectationKind.URGENT);
	}
	
	/**
	 * 
	 * @return All requested message events that are sent by environment objects
	 *         and not blocked by assumption scenarios.
	 */
	public Collection<? extends MessageEvent> getRequestedEnvironmentEvents() {
		return environmentEventsByExpectationKind.get(ExpectationKind.REQUESTED);
	}
	/**
	 * 
	 * @return All eventually message events that are sent by environment
	 *         objects and not blocked by assumption scenarios.
	 */
	public List<MessageEvent> getEventuallyEnvironmentEvents() {
		return environmentEventsByExpectationKind.get(ExpectationKind.EVENTUALLY);
	}
	
	/**
	 * 
	 * @return Environment message events that are not expected, but affect active copies. This collection includes spontaneously occurring events as well as
	 * non-spontaneous events that are enabled in assumption scenarios.
	 */
	public Collection<MessageEvent> getListeningEnvironmentEvents() {
		return listeningEnvironmentEvents;
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////
	////// from SMLRuntimeStateLogic
	//////////////////////////////////////////////////////////////////////////////////////////

	
	private void addAllConcreteInitializingEnvironmentMessageEvents(EList<MessageEvent> enabledEventsList) {
		// 1) add all possible initializing environment events
		for (MessageEvent messageEvent : ((SMLObjectSystem) runtimeState.getObjectSystem())
				.getInitializingEnvironmentMessageEvents(runtimeState.getDynamicObjectContainer())) {
			// if the message event has parameter -> update values
			if (messageEvent.isParameterized()) {
				// only concrete message events can be send
//				if (messageEvent.isConcrete()) {
//					enabledEventsList.add(messageEvent);
//					//continue;
//				} else {
					// compute parameter values
					EList<MessageEvent> initMessages = computeConcreteMessageEvents(messageEvent);
					// computed init messages need to be contained.
					// TODO Memory use: only add these messages to the
					// containment list
					// that are not removed in further steps.
					runtimeState.getComputedInitializingMessageEvents().addAll(initMessages);
					enabledEventsList.addAll(initMessages);
//				}
			} else {
				// no parameter
				runtimeState.getComputedInitializingMessageEvents().add(messageEvent);
				enabledEventsList.add(messageEvent);
			}
		}
	}

	/**
	 * Computes init MessagesEvents with parameter form SML Scenarios.
	 * 
	 * @param messageEvent
	 * @return
	 */
	private EList<MessageEvent> computeConcreteMessageEvents(MessageEvent messageEvent) {
		EList<MessageEvent> enabledEventsList = new MessageEventUniqueEList<MessageEvent>();

		EList<Scenario> scenarioList = ((SMLObjectSystemLogic) runtimeState.getObjectSystem())
				.getScenariosForInitMessageEvent(messageEvent);

		for (Scenario scenario : scenarioList) {
			//////////
			// From ActiveScenarioHelper.createActiveScenario(...)
			/////////

			// 1) Create active scenario
			// TODO do this a other way eINSTANCE etc...
			ScenarioBuilder scenarioBuilder = new ScenarioBuilder();
			ActiveScenario activeScenario = scenarioBuilder.buildScenario(scenario);
			MultiActiveScenarioInitializations multiActiveScenarioInitializations = activeScenario.init((SMLObjectSystem) runtimeState.getObjectSystem(),
					runtimeState.getDynamicObjectContainer(), runtimeState, messageEvent);
			
			for (Map.Entry<ActiveScenario, ActiveScenarioProgress> activeScenarioToActiveScenarioProgressMapEntry : multiActiveScenarioInitializations.getActiveScenarioToActiveScenarioProgressMap().entrySet()) {
				ActiveScenarioProgress progress =  activeScenarioToActiveScenarioProgressMapEntry.getValue();
				activeScenario = activeScenarioToActiveScenarioProgressMapEntry.getKey();
				
				// TODO discuss this. COMMENT Joel: I think safety violations during this stage must be ignored, i.e., code is good.
				if (progress == ActiveScenarioProgress.SAFETY_VIOLATION)
					continue;

				//////////
				// END From ActiveScenarioHelper.createActiveScenario(...)
				/////////

				EList<MessageEvent> initMessages = getEnabledMessages(activeScenario.getMainActiveInteraction());

				enabledEventsList.addAll(initMessages);

			}
			
		}
		return enabledEventsList;
	}

	// TODO part of proof of concept: first message with parameter
	private EList<MessageEvent> getEnabledMessages(ActivePart activeInteraction) {
		EList<MessageEvent> enabledMessages = new MessageEventUniqueEList<MessageEvent>();
		if (activeInteraction.getEnabledNestedActiveInteractions().isEmpty()) {
			enabledMessages.addAll(activeInteraction.getEnabledEvents());
		} else {
			for (ActivePart nestedPart : activeInteraction.getEnabledNestedActiveInteractions()) {
				enabledMessages.addAll(getEnabledMessages(nestedPart));
			}
		}
		return enabledMessages;
	}

	public List<MessageEvent> getInitializingEnvironmentEvents() {
		if(initializingMessageEvents == null){
		initializingMessageEvents = new MessageEventUniqueEList<>();
		addAllConcreteInitializingEnvironmentMessageEvents(initializingMessageEvents);
		}
		return initializingMessageEvents;
	}
}
