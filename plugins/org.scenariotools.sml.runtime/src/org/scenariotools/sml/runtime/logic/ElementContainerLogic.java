/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.DynamicObjectContainer;
import org.scenariotools.sml.runtime.ElementContainer;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.ActiveScenarioRoleBindingsKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.ObjectSystemKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.StateKeyWrapper;

public abstract class ElementContainerLogic extends MinimalEObjectImpl.Container implements ElementContainer{
	
	/**
	 */
	public ActiveScenario getActiveScenario(ActiveScenario activeScenario) {
		if(!this.isEnabled())
			return activeScenario;
		
		activeScenario.setRoleBindings(this.getActiveScenarioRoleBindings(activeScenario.getRoleBindings()));
		
		ActiveScenarioKeyWrapper keyWrapper = new ActiveScenarioKeyWrapper(activeScenario);
		if(this.getActiveScenarioKeyWrapperToActiveScenarioMap().containsKey(keyWrapper)){
			return this.getActiveScenarioKeyWrapperToActiveScenarioMap().get(keyWrapper);
		}else{
			this.getActiveScenarioKeyWrapperToActiveScenarioMap().put(keyWrapper, activeScenario);
			this.getActiveScenarios().add(activeScenario);
			return activeScenario;
		}
	}

	/**
	 */
	public SMLObjectSystem getObjectSystem(SMLObjectSystem smlObjectSystem) {
		if(!this.isEnabled())
			return smlObjectSystem;
		
		ObjectSystemKeyWrapper keyWrapper = new ObjectSystemKeyWrapper(smlObjectSystem.getRootObjects());
		if(this.getObjectSystemKeyWrapperToObjectSystemMap().containsKey(keyWrapper)){
			return this.getObjectSystemKeyWrapperToObjectSystemMap().get(keyWrapper);
		}else{
			this.getObjectSystemKeyWrapperToObjectSystemMap().put(keyWrapper, smlObjectSystem);
			this.getObjectSystems().add(smlObjectSystem);
			return smlObjectSystem;
		}
	}

	/**
	 * Find existing state that can be used as target state of current step.
	 * If no equal existing state exists, this method will return the given 
	 * smlRuntimeState.
	 */
	public SMLRuntimeState getSMLRuntimeState(SMLRuntimeState smlRuntimeState) {
		if(!this.isEnabled())
			return smlRuntimeState;
		
		// handle Scenarios
		for (int i = 0; i < smlRuntimeState.getActiveScenarios().size(); i++) {
			smlRuntimeState.getActiveScenarios().set(
					i,
					this.getActiveScenario(smlRuntimeState
							.getActiveScenarios().get(i)));
		}
		
		// handle dynamic object system
		smlRuntimeState.setDynamicObjectContainer(this
				.getDynamicObjectContainer(smlRuntimeState
						.getDynamicObjectContainer(),smlRuntimeState.getObjectSystem()));

		StateKeyWrapper keyWrapper = new StateKeyWrapper(smlRuntimeState);
		if(this.getStateKeyWrapperToStateMap().containsKey(keyWrapper)){
			return this.getStateKeyWrapperToStateMap().get(keyWrapper);
		}else{
			this.getStateKeyWrapperToStateMap().put(keyWrapper, smlRuntimeState);
			return smlRuntimeState;
		}
	}

	/**
	 */
	public ActiveScenarioRoleBindings getActiveScenarioRoleBindings(ActiveScenarioRoleBindings activeScenarioRoleBindings) {
		if(!this.isEnabled())
			return activeScenarioRoleBindings;

		ActiveScenarioRoleBindingsKeyWrapper keyWrapper = new ActiveScenarioRoleBindingsKeyWrapper(activeScenarioRoleBindings);
		if(this.getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap().containsKey(keyWrapper)){
			return this.getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap().get(keyWrapper);
		}else{
			this.getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap().put(keyWrapper, activeScenarioRoleBindings);
			this.getActiveScenarioRoleBindings().add(activeScenarioRoleBindings);
			return activeScenarioRoleBindings;
		}
	}

	/**
	 */
	
	public DynamicObjectContainer getDynamicObjectContainer(DynamicObjectContainer dynamicObjectContainer, SMLObjectSystem objectSystem) {
		if (!this.isEnabled())
			return dynamicObjectContainer;
		
		EList<EObject> objects = new BasicEList<EObject>();
		if(dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().isEmpty()){
			//nothing to do
		}else{
			//We need to keep the order from the SMLObjectSystem::rootObjects 
            SMLObjectSystem smlObjectSystem = (SMLObjectSystem) objectSystem;
            EList<EObject> staticRootObjects = smlObjectSystem.getRootObjects();
            for(EObject staticEObject: staticRootObjects){
                objects.add(dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(staticEObject));
            }
        }
        ObjectSystemKeyWrapper keyWrapper = new ObjectSystemKeyWrapper(objects);
		
		if (this.getObjectSystemKeyWrapperToDynamicObjectContainerMap().containsKey(keyWrapper)) {
			return this.getObjectSystemKeyWrapperToDynamicObjectContainerMap().get(keyWrapper);
		} else {
			this.getObjectSystemKeyWrapperToDynamicObjectContainerMap().put(keyWrapper, dynamicObjectContainer);
			this.getDynamicObjectContainer().add(dynamicObjectContainer);
			return dynamicObjectContainer;
		}
	}


}
