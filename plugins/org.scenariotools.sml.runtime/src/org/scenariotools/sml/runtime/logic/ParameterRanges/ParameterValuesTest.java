/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic.ParameterRanges;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;

public class ParameterValuesTest {
	public static void main(String[] args) {
		EList<IntegerParameterRange> ranges = new BasicEList<IntegerParameterRange>();
		ranges.add(ParameterValues.Factory.getIntegerParameterRange(0, 10));
		ranges.add(ParameterValues.Factory.getIntegerParameterRange(12, 12));
		ranges.add(ParameterValues.Factory.getIntegerParameterRange(1000, 1007));
		
		EList<Integer> values = new BasicEList<Integer>();
		values.add(-3);
		values.add(-33);
		values.add(77);
		values.add(777);
		values.add(455);
		values.add(3333);
		
		ParameterValues<Integer> pvInteger = ParameterValues.Factory.getIntegerParameterValues(ranges, values);
		
		for(Integer i : pvInteger){
			System.out.println(i);
		}
		
		ParameterValues<Integer> pvIntegerNull1 = ParameterValues.Factory.getIntegerParameterValues(null, values);
		for(Integer i : pvIntegerNull1){
			System.out.println(i);
		}
		
		ParameterValues<Integer> pvIntegerNull2 = ParameterValues.Factory.getIntegerParameterValues(ranges, null);
		for(Integer i : pvIntegerNull2){
			System.out.println(i);
		}
		
		ParameterValues<Integer> pvIntegerNull3 = ParameterValues.Factory.getIntegerParameterValues(null, null);
		for(Integer i : pvIntegerNull3){
			System.out.println(i);
		}
		
		EList<String> strings = new BasicEList<String>();
		strings.add("Hallo ");
		strings.add("Timo");
		strings.add("!\n");
		strings.add("Was ");
		strings.add("geht ");
		strings.add("ab?\n");
		ParameterValues<String> pvString = ParameterValues.Factory.getListParameterValues(strings);
		
		for(String i : pvString){
			System.out.print(i);
		}
		
		
		EList<ActiveScenarioProgress> enums = new BasicEList<ActiveScenarioProgress>();
		enums.add(ActiveScenarioProgress.CONTINUE);
		enums.add(ActiveScenarioProgress.NO_PROGRESS);
		enums.add(ActiveScenarioProgress.SAFETY_VIOLATION);
		enums.add(ActiveScenarioProgress.COLD_VIOLATION);
		enums.add(ActiveScenarioProgress.STEP_OVER);
		ParameterValues<ActiveScenarioProgress> pvEnum = ParameterValues.Factory.getListParameterValues(enums);
		
		for(ActiveScenarioProgress i : pvEnum){
			System.out.println(i);
		}
		
		Assert.isTrue(pvInteger.contains(1));
		Assert.isTrue(pvInteger.contains(5));
		Assert.isTrue(pvInteger.contains(777));
		Assert.isTrue(pvInteger.contains(1001));
		Assert.isTrue(!pvInteger.contains(-1111));
		Assert.isTrue(!pvInteger.contains(-1));
		
	}
}
