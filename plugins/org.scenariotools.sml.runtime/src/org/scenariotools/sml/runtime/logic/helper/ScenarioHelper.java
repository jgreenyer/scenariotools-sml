/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic.helper;

import org.scenariotools.sml.Role;
import org.scenariotools.sml.Specification;

// should be part of Scenario?
public class ScenarioHelper {

	/**
	 * Note: This function would be superfluous, if role had a derived attribute
	 * <i>controllable</i>.
	 * 
	 * @param s
	 *            Specification that contains the usecase where <i>r</i> is
	 *            defined.
	 * @param r
	 *            A role that has been defined in a usecase.
	 * @return
	 */
	public static boolean isControllableRole(Specification s, Role r) {
		return s.getControllableEClasses().contains(r.getType());
	}

	/**
	 * Note: This function would be superfluous, if role had a derived attribute
	 * <i>uncontrollable</i>.
	 * 
	 * @param s
	 *            Specification that contains the usecase where <i>r</i> is
	 *            defined.
	 * @param r
	 *            A role that has been defined in a usecase.
	 * @return
	 */
	public static boolean isUncontrollableRole(Specification s, Role r) {
		return !isControllableRole(s, r);
	}
}
