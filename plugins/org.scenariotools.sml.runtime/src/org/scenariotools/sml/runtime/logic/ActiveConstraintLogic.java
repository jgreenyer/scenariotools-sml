/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.ParameterValue;
import org.scenariotools.sml.Message;
import org.scenariotools.sml.runtime.ActiveConstraint;
import org.scenariotools.sml.runtime.ActiveInteraction;
import org.scenariotools.sml.runtime.ActiveMessageParameter;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.logic.helper.MessageEventHelper;

public abstract class ActiveConstraintLogic  extends MinimalEObjectImpl.Container implements ActiveConstraint {

	/** 
	 * Updates Constraint parameter
	 */
	@Override
	public void updateConstraintEvent(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		
		if (this.getConstraintMessageEvent().isParameterized()) {
			// update parameters
			for (int i = 0; i < getActiveMessageParameters().size(); i++) {
				ActiveMessageParameter activeMessageParameter = getActiveMessageParameters().get(i);
				ParameterValue parameterValue = this.getConstraintMessageEvent().getParameterValues().get(i);
				// this.eContainer should ever be an ActiveInteraction
				// TODO change eContainer to parent...
				activeMessageParameter.update(parameterValue, (ActivePart) this.eContainer, activeScenario, smlRuntimeState);
			}
		}

	}
	
	@Override
	public void init(ActiveScenarioRoleBindings roleBindings,
			ActivePart parentActivePart, 
			ActiveScenario activeScenario) {
		
		Message forbiddenMessage = this.getMessage();
		EObject receiver = roleBindings.getRoleBindings().get(forbiddenMessage.getReceiver());
		EObject sender = roleBindings.getRoleBindings().get(forbiddenMessage.getSender());
		MessageEvent forbiddenEvent = MessageEventHelper
				.createMessageEventInActiveScenario(sender,
						receiver,
						forbiddenMessage,
						activeScenario);
		// add constraint to the specific Constraint list in the containing Interaction
		this.addToParentSpecificConstraintList((ActiveInteraction) parentActivePart, forbiddenEvent);
		
		this.setConstraintMessageEvent(forbiddenEvent);
	}
}
