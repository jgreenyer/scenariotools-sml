/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic;

import java.util.HashSet;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.ETypedElement;
import org.scenariotools.sml.runtime.EEnumParameterValue;
import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.runtime.IntegerParameterValue;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.ParameterValue;
import org.scenariotools.sml.runtime.StringParameterValue;
import org.scenariotools.sml.runtime.WaitEvent;
import org.scenariotools.sml.runtime.impl.AnnotatableElementImpl;
import org.scenariotools.sml.Collaboration;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.ScenarioKind;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.MessageEventBlockedInformation;
import org.scenariotools.sml.runtime.ParameterRangesProvider;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.emf.custom.MessageEventUniqueEList;
import org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper;
import org.scenariotools.sml.runtime.logic.ParameterRanges.ParameterValues;
import org.scenariotools.sml.runtime.logic.helper.ActiveScenarioHelper;
import org.scenariotools.sml.runtime.logic.helper.EventGatheringVisitor;
import org.scenariotools.sml.runtime.logic.helper.MessageEventHelper;
import org.scenariotools.sml.runtime.plugin.Activator;
import org.scenariotools.sml.runtime.util.SMLRuntimeStateUtil;

public abstract class SMLRuntimeStateLogic extends AnnotatableElementImpl implements SMLRuntimeState {

	protected static Logger logger = Activator.getLogManager().getLogger(
			SMLRuntimeStateLogic.class.getName());

	
	/**
	 * Set all initializing environment messages to enabled list.
	 */
	public void init(SMLObjectSystem objectSystem) {
		logger.debug("set ObjectSystem");
		this.setObjectSystem(objectSystem);
		// set all initializing environment messages
		updateEnabledMessageEvents();
	}

	
	/**
	 */
	public void updateEnabledMessageEvents() {
		this.getEnabledEvents().clear();
		this.getComputedInitializingMessageEvents().clear();
		SMLRuntimeStateUtil.unsetSystemStepBlockedButAssumptionRequestedEvents(this);

		
		if(isSafetyViolationOccurredInAssumptions()){
			// system won, just add environment events that, when executed, will lead
			// to self-transitions.
			// TODO check if this is necessary, or if just adding one event/self-transition
			// would be sufficient.
			EList<Event> enabledEventsList = new MessageEventUniqueEList<Event>();
			enabledEventsList.addAll(new EventGatheringVisitor(this).getInitializingEnvironmentEvents());
			// TODO check, if this is needed.
			removeMessagesForWhichSideEffectsCannotBeExecuted(enabledEventsList);
			
			this.getEnabledEvents().addAll(enabledEventsList);
			
			if(logger.isDebugEnabled()){
				logger.debug(this.getEnabledEvents());
			}
			return;
		} else if (isSafetyViolationOccurredInGuarantees()) {
			/* Guarantees were violated, we continue the execution
			 * in order to check, whether subsequently the environment is
			 * able to avoid assumption violations.
			 * (If yes, finally the system loses,
			 *  if no, the system wins, i.e. the environment could force
			 *  a guarantee violation only at the cost of violating the
			 *  assumptions later on.)
			 *  
			 *  When a safety violation occurred in the guarantees, we do not
			 *  enable system events.
			 */
			EList<Event> enabledEventsList = new MessageEventUniqueEList<Event>();
			// here: add all spontaneous events and enabled non-spontaneous from
			// the assumptions to the enabled list.
			EventGatheringVisitor g = new EventGatheringVisitor(this);
			g.visitAssumptionScenariosOnly();
			if(g.isCommittedEnvironmentEventsExist()){
				// At this point, the environment must violate the assumptions.
				if(g.getCommittedEnvironmentEvents().isEmpty()){
					setSafetyViolationOccurredInAssumptions(true);					
				}else {
					enabledEventsList.addAll(g.getCommittedEnvironmentEvents());
				}
			}else{
				enabledEventsList.addAll(g.getInitializingEnvironmentEvents());
				enabledEventsList.addAll(g.getUrgentEnvironmentEvents());
				enabledEventsList.addAll(g.getRequestedEnvironmentEvents());
				enabledEventsList.addAll(g.getEventuallyEnvironmentEvents());
				enabledEventsList.addAll(g.getListeningEnvironmentEvents());
			}
			removeMessagesForWhichSideEffectsCannotBeExecuted(enabledEventsList);
			removeNonConcreteMessages(enabledEventsList);

			this.getEnabledEvents().addAll(enabledEventsList);

			if(logger.isDebugEnabled()) {
				logger.debug(this.getEnabledEvents());
			}
			return;	
		}else {
			// No safety violations occurred in assumptions or guarantees (regular update).
			
			EList<Event> enabledEvents = new MessageEventUniqueEList<>();
			EventGatheringVisitor visitor = new EventGatheringVisitor(this);
			/*
			 * 1. If committed events exist (system OR environment events, 
			 *  monitored or non-monitored in assumption or guarantee scenarios)
			 * 	There is an assumption safety violation if all
			 *  WHICH EVENT TO ENABLE: 
			 *  If there are committed system events (monitored or non-monitored) 
			 *  in guarantee scenarios, then we enable the subset of those system events for which
			 *  the following conditions hold:
			 *  	- they correspond to enabled non-monitored messages in any guarantee scenario
			 *  	- they are not blocked by any guarantee scenario.
			 *  
			 *  If no system events are enabled as described above and 
			 *  if there are committed (monitored or non-monitored) environment events in
			 *  assumption scenario, then we enable the subset of those environment events
			 *  where for each event the following conditions hold: 
			 *  	- they must not be blocked by any assumption scenario.
			 *  	- they must be spontaneous or non-spontaneous but enabled in at least one
			 *  	  assumption scenario. 
			 * 
			 *  It is a guarantee safety violation, if all committed events in guarantee scenarios
			 *  but there are no enabled events as described above. 
			 *  It is an assumption safety violation, if there are committed events in assumption scenarios
			 *  but there are no enabled events as described above. 
			 * 
			 */
			visitor.visitAll();
			if (visitor.isCommittedSystemEventsExist()) {
				if (visitor.getCommittedSystemEvents().isEmpty()) {
					setSafetyViolationOccurredInGuarantees(true);
				} else {
					enabledEvents.addAll(visitor.getCommittedSystemEvents());
				}
			} else if (visitor.isUrgentSystemEventsExist()) {
				if (visitor.getUrgentSystemEvents().isEmpty() && visitor.getRequestedSystemEvents().isEmpty()
						&& visitor.getEventuallySystemEvents().isEmpty()) {
					setSafetyViolationOccurredInGuarantees(true);
				} else {
					enabledEvents.addAll(visitor.getUrgentSystemEvents());
					enabledEvents.addAll(visitor.getRequestedSystemEvents());
					enabledEvents.addAll(visitor.getEventuallySystemEvents());
				}
			} else if (visitor.isRequestedSystemEventsExist()) {
				if (!visitor.getRequestedSystemEvents().isEmpty()) {
					enabledEvents.addAll(visitor.getRequestedSystemEvents());
					enabledEvents.addAll(visitor.getEventuallySystemEvents());
				}
			} else if(!visitor.getEventuallySystemEvents().isEmpty() && !isSystemChoseToWait()) {
				enabledEvents.addAll(visitor.getEventuallySystemEvents());
				enabledEvents.add(getStateGraph().getElementContainer().getWaitEvent());
			} else if (visitor.isCommittedEnvironmentEventsExist()) {
				if (visitor.getCommittedEnvironmentEvents().isEmpty()) {
					setSafetyViolationOccurredInAssumptions(true);
				} else {
					enabledEvents.addAll(visitor.getCommittedEnvironmentEvents());
				}
			} else {
				enabledEvents.addAll(visitor.getUrgentEnvironmentEvents());
				enabledEvents.addAll(visitor.getRequestedEnvironmentEvents());
				enabledEvents.addAll(visitor.getEventuallyEnvironmentEvents());
				enabledEvents.addAll(visitor.getInitializingEnvironmentEvents());
				enabledEvents.addAll(visitor.getListeningEnvironmentEvents());
			}
			
			mergeSystemMessagesWithWildcardParameter(enabledEvents);
			addConcreteMessagesForNonConcrete(enabledEvents);
			removeNonConcreteMessages(enabledEvents);
			
			// important to invoke this after removeNonConcreteMessages(enabledEvents), 
			// since otherwise certain messages may slip past when their parameter values determine
			// whether they can be executed or not.
			removeMessagesForWhichSideEffectsCannotBeExecuted(enabledEvents);
			
			checkIfMessageParameterAreInParameterRangesAndRemoveMessageIfNot(enabledEvents);
			getEnabledEvents().addAll(enabledEvents);
		}
		
		if(logger.isDebugEnabled()){
			logger.debug(this.getEnabledEvents());
		}
	}
	/**
	 * Removes all system events that have wildcard parameter values. 
	 * @param enabledEventsList
	 */
	private void mergeSystemMessagesWithWildcardParameter(EList<Event> enabledEventsList) {
		if(enabledEventsList.isEmpty())return;
		
		EList<Event> candidates = new BasicEList<Event>();
		addAllSystemEvents(candidates, enabledEventsList);

		if(candidates.isEmpty())return;
		
		while(!candidates.isEmpty()){
			EList<MessageEvent> matches = new BasicEList<MessageEvent>(); 
			
			MessageEvent first = (MessageEvent)candidates.remove(candidates.size() -1);
			matches.add(first);
			
			for(Iterator<Event> iterator = candidates.iterator(); iterator.hasNext();) {
				MessageEvent message = (MessageEvent)iterator.next();
				
				if(message.isMessageUnifiableWith(first)){
					matches.add(message);
					iterator.remove();
				}
			}
			if(matches.size() >= 2){
				ParameterRangesProvider provider = RuntimeFactory.eINSTANCE.createParameterRangesProvider();
				provider.init(matches);
			
				EList<MessageEvent> merged = MessageEventHelper.getMergedConcreteMessageEvents(matches.get(0), provider, (SMLObjectSystem) getObjectSystem());
				enabledEventsList.removeAll(matches);
				enabledEventsList.addAll(merged);
				getComputedInitializingMessageEvents().addAll(merged);
				logger.debug("Remove Messages for merging: " + matches);
				logger.debug("Add merged Messages: " + merged);
			}
		}
	}


	@SuppressWarnings("unchecked")
	private void checkIfMessageParameterAreInParameterRangesAndRemoveMessageIfNot(EList<Event> enabledEventsList) {
		if(enabledEventsList.isEmpty())return;
		int countMessagesBefore = enabledEventsList.size();
		
		ParameterRangesProvider parameterRangesProvider = ((SMLRuntimeStateGraph)this.getStateGraph()).getParameterRangesProvider();
		
		message:
		for(Iterator<Event> iterator = enabledEventsList.iterator(); iterator.hasNext();) {
			Event event = iterator.next();
			if(!(event instanceof MessageEvent))
				continue;
			
			MessageEvent message = (MessageEvent)event;
			
			if(message.isParameterized()){
				for(ParameterValue parameterValue : message.getParameterValues()){
					if(parameterValue.isWildcardParameter())
						continue;

					ETypedElement parameterType = parameterValue.getStrucFeatureOrEOp();
					if(!parameterRangesProvider.containsParameterValues(parameterType))
						continue;
						
					if(parameterValue instanceof IntegerParameterValue){
						ParameterValues<Integer> pv = (ParameterValues<Integer>) parameterRangesProvider.getParameterValues(parameterType, (SMLObjectSystem) getObjectSystem());
						if(!pv.contains((Integer)parameterValue.getValue())){
							logger.debug("Message Parameter is not in Parameter Ranges: " + parameterValue + " from " + message + " isn't in " + pv.toString());
							iterator.remove();
							continue message;
						}
					}else if(parameterValue instanceof StringParameterValue){
						ParameterValues<String> pv = (ParameterValues<String>) parameterRangesProvider.getParameterValues(parameterType, (SMLObjectSystem) getObjectSystem());
						if(!pv.contains((String)parameterValue.getValue())){
							logger.debug("Message Parameter is not in Parameter Ranges: " + parameterValue + " from " + message + " isn't in " + pv.toString());
							iterator.remove();
							continue message;
						}
					}else if(parameterValue instanceof EEnumParameterValue){
						ParameterValues<EEnumLiteral> pv = (ParameterValues<EEnumLiteral>) parameterRangesProvider.getParameterValues(parameterType, (SMLObjectSystem) getObjectSystem());
						if(!pv.contains((EEnumLiteral)parameterValue.getValue())){
							logger.debug("Message Parameter is not in Parameter Ranges: " + parameterValue + " from " + message + " isn't in " + pv.toString());
							iterator.remove();
							continue message;
						}
					}
					
				}
			}
		}
		if(enabledEventsList.isEmpty()){
			setSafetyViolationOccurredInGuarantees(true);
			//setSafetyViolationOccurredInRequirements(true);
			setSafetyViolationOccurredInAssumptions(true);
			logger.debug("All " + countMessagesBefore + " messages were removed by parameter ranges check.");
		}
	}


	/**
	 * remove all messages that are not concrete (all messages that have wildcard parameter)
	 */
	private void removeNonConcreteMessages(EList<Event> enabledEventsList) {
		for (Iterator<Event> iterator = enabledEventsList.iterator(); iterator.hasNext();) {
			Event enabledEvent = iterator.next();
			if(!(enabledEvent instanceof MessageEvent))
				continue;
			
			MessageEvent enabledMessageEvent = (MessageEvent)enabledEvent;
			
			if(!enabledMessageEvent.isConcrete()){	
				iterator.remove();
			}
		}
	}

	
	private void addConcreteMessagesForNonConcrete(EList<Event> enabledEventsList) {
		EList<MessageEvent> result = new BasicEList<MessageEvent>();
		for (Iterator<Event> iterator = enabledEventsList.iterator(); iterator.hasNext();) {
			Event enabledEvent = iterator.next();
			if(!(enabledEvent instanceof MessageEvent))
				continue;
			
			MessageEvent enabledMessageEvent = (MessageEvent)enabledEvent;
			
			if(!enabledMessageEvent.isConcrete()){	
				result.addAll(MessageEventHelper
							.getAllConcreteMessageEvents(enabledMessageEvent, 
									((SMLRuntimeStateGraph)this.eContainer).getParameterRangesProvider(),
									(SMLObjectSystem)getObjectSystem()));
			}
		}
		getComputedInitializingMessageEvents().addAll(result);
		enabledEventsList.addAll(result);
	}
	/**
	 * remove all messages on that side effects cannot executed 
	 * @param enabledEventsList
	 */
	private void removeMessagesForWhichSideEffectsCannotBeExecuted(EList<Event> enabledEventsList) {
		for (Iterator<Event> iterator = enabledEventsList.iterator(); iterator.hasNext();) {
			Event enabledEvent = iterator.next();
			if(!(enabledEvent instanceof MessageEvent))
				continue;
				
			MessageEvent enabledMessageEvent = (MessageEvent)enabledEvent;
			
			if (!((SMLObjectSystem)getObjectSystem()).canExecuteSideEffects(enabledMessageEvent, this.getDynamicObjectContainer())){
				iterator.remove();
				storeMessageEventBlockedInformation(enabledMessageEvent, "Execution prohibited by a plugged-in MessageEventSideEffectsExecutor.");
			}
		}
	}


	/**
	 * Decide if it is a environment step or a system step.
	 * It's a environment step if no system message is requested by any guarantee scenario.
	 * 
	 * @param guaranteeRequestedEventsList
	 * @return
	 */
//	private boolean isEnvironmentStep(EList<MessageEvent> guaranteeRequestedEventsList) {
//		for (MessageEvent messageEvent : guaranteeRequestedEventsList) {
//			if (this.getObjectSystem().isControllable(messageEvent.getSendingObject())) {
//				return false;
//			}
//		}
//		return true;
//	}

	/**
	 * Collects MessageEvents (the Alphabet) form all active scenarios expect violated scenarios.
	 * 
	 * @return
	 */
//	private EList<MessageEvent> getListeningEvents() {
//		EList<MessageEvent> result = new MessageEventUniqueEList<MessageEvent>();
//		
//		for (ActiveScenario activeScenario : this.getActiveScenarios()) {
//			if(activeScenario.isSafetyViolationOccurred())continue;
//
//			result.addAll(activeScenario.getAlphabet());
//		}
//		
//		return result;
//	}

	/**
	 * Collects MessageEvents (the Alphabet) form all active assumption scenarios expect violated scenarios.
	 * 
	 * @return
	 */
//	private EList<MessageEvent> getListeningEventsFromAssumptions() {
//		EList<MessageEvent> result = new MessageEventUniqueEList<MessageEvent>();
//		
//		for (ActiveScenario activeScenario : this.getActiveScenarios()) {
//			if(activeScenario.isSafetyViolationOccurred())continue;
//			if(activeScenario.getScenario().getKind() != ScenarioKind.ASSUMPTION)continue;
//
//			result.addAll(activeScenario.getAlphabet());
//		}
//		
//		return result;
//	}
	

//	private void updateEnabledMessageEventsForSystemStep(EList<MessageEvent> guaranteeRequestedEventsList,
//			EList<MessageEvent> enabledEventsList) {
//		
//		// 1) add all system events that are requested in a guarantee scenario
////		addAllSystemEvents(enabledEventsList, guaranteeRequestedEventsList);
//
//		// 2) remove all events that are parameter unifiable with events
//		// blocked in requirement scenarios and guarantee scenarios 
//		// and not cause a violation in assumption scenarios
//		for (Iterator<MessageEvent> iterator = enabledEventsList.iterator(); iterator
//				.hasNext();) {
//			MessageEvent enabledMessageEvent = iterator.next();
//			boolean violationInGuarantees = false;
//			boolean violationInAssumption = false;
//			boolean violationInRequirement = false;
//			for (ActiveScenario activeScenario : this.getActiveScenarios()) {
//				
//				if(activeScenario.getScenario().getKind() == ScenarioKind.GUARANTEE 
//						&& !violationInGuarantees 
//						&& activeScenario.isBlocked(enabledMessageEvent)){
//					violationInGuarantees = true;
//					
//					storeMessageEventBlockedByActiveScenarioInformation(enabledMessageEvent, activeScenario);
//					
//				}else if(activeScenario.getScenario().getKind() == ScenarioKind.ASSUMPTION 
//						&& !violationInAssumption
//						&& activeScenario.isBlocked(enabledMessageEvent)){
//					violationInAssumption = true;
//					
//					storeMessageEventBlockedByActiveScenarioInformation(enabledMessageEvent, activeScenario);
//
//					break;
//					
//				}
//			}
//			// First check all assumptions before decide. System wins if a violation occurs in 
//			// a assumption scenario whatever violations occur else. 
//			if ((violationInGuarantees || violationInRequirement) && !violationInAssumption) {
//				iterator.remove();
//			}
//		}
//	}
	/**
	 * Adds all message events with controllable sender objects to enabledEventsList.
	 * @param enabledEventsList
	 * @param guaranteeRequestedEventsList
	 */
	private void addAllSystemEvents(EList<Event> enabledEventsList,
			EList<Event> guaranteeRequestedEventsList) {
		for (Event event : guaranteeRequestedEventsList) {
			if (this.getObjectSystem().isControllable(event)) {
				enabledEventsList.add(event);
			}
		}
	}

    private boolean assumptionsActiveWithEnabledRequestedEvents() {
        for (ActiveScenario activeScenario : getActiveScenarios()) {
            if(activeScenario.getScenario().getKind() == ScenarioKind.ASSUMPTION
                    && !activeScenario.getRequestedEvents().isEmpty())
                return true;
        }
        return false;
    }
	
	


	/**
	 */
	public void performStep(Event event) {
		getTerminatedExistentialScenariosFromLastPerformStep().clear();

        // do not change state when the environment has lost 
		if(isSafetyViolationOccurredInAssumptions())
			return;
		
		// When a safety violation occurred in the guarantee, it should not be that system events are executed.
		assert(!(isSafetyViolationOccurredInGuarantees() && getObjectSystem().isControllable(event)));
		
        // do not change state when the system has lost 
        if((isSafetyViolationOccurredInGuarantees() 
        		|| false)//safetyViolationOccurredInRequirements) 
        		&& !isSafetyViolationOccurredInAssumptions()
        		&& !assumptionsActiveWithEnabledRequestedEvents()) 
            return;
		
		logger.debug("Calling performStep with event " + event);

		if(event instanceof WaitEvent) {
			setSystemChoseToWait(true);
			return;
		}
		
		MessageEvent messageEvent = (MessageEvent)event;
		handleSideEffectOnObjectSystem(messageEvent);

		// 2) Progress active scenarios
		boolean systemCauseSafetyViolationInLastStep = this.isSafetyViolationOccurredInGuarantees(); 
														//|| isSafetyViolationOccurredInRequirements();
		for (Iterator<ActiveScenario> it = this.getActiveScenarios().iterator(); it.hasNext();) {
			ActiveScenario activeScenario = it.next();
			
			// If safety violation occurred in guarantee scenario before ->
			// don't process
			if (systemCauseSafetyViolationInLastStep
					&& (activeScenario.getScenario().getKind() == ScenarioKind.GUARANTEE)) {
				continue;
			}
			// perform step
			
			ActiveScenarioProgress activeScenarioProgress = activeScenario.performStep(messageEvent, this);
			
			// add regular terminated existential scenarios to terminatedExistentialScenariosFromLastPerformStep list
			if(activeScenario.getScenario().getKind() == ScenarioKind.EXISTENTIAL
					&& activeScenarioProgress == ActiveScenarioProgress.INTERACTION_END){
				getTerminatedExistentialScenariosFromLastPerformStep().add(activeScenario.getScenario());
			}
			
			// handle violations and scenario termination
			if (activeScenarioProgress == ActiveScenarioProgress.COLD_VIOLATION
					|| activeScenarioProgress == ActiveScenarioProgress.INTERACTION_END)
				it.remove();
			
			// set violation state in RuntimeState
			if (activeScenarioProgress == ActiveScenarioProgress.SAFETY_VIOLATION) {
				ActiveScenarioHelper.flagSafetyViolation(this, activeScenario);
			}
		}
		
		activateScenarios(messageEvent);
		removeDuplicateActiveScenarios();
	}

	/**
	 * Removes equal copies of active scenarios from this state. 
	 * @see ActiveScenarioHelper#areEqualActiveScenarios(ActiveScenario, ActiveScenario)
	 * @see ActiveScenarioKeyWrapper
	 */
	private void removeDuplicateActiveScenarios() {
		// cache constructed keywrappers in a set. Compared to calling the "areEqual" function for
		// all scenario pairs, this should be more efficient.
		final Set<ActiveScenarioKeyWrapper> wrapperSet = new HashSet<>();
		for (ListIterator<ActiveScenario> it = getActiveScenarios().listIterator(); it.hasNext();) {
			final ActiveScenario activeScenario = it.next();
			final ActiveScenarioKeyWrapper wrapper = new ActiveScenarioKeyWrapper(activeScenario);
			final boolean duplicateFound = !wrapperSet.add(wrapper);
			if (duplicateFound) {
				it.remove();
				logger.debug("Found duplicate scenario: "+activeScenario);
			}

		}
	}


	private void handleSideEffectOnObjectSystem(MessageEvent messageEvent) {
		// 1) handle side effect on object system
//		if (((SMLObjectSystem) this.getObjectSystem())
//				.canExecuteSideEffects(messageEvent, this.getDynamicObjectContainer())) {
		
		((SMLObjectSystem) this.getObjectSystem())
				.executeSideEffects(messageEvent, this.getDynamicObjectContainer());
//		} else {
//			this.setSafetyViolationOccurredInGuarantees(true);
//		}
	}

	/**
	 * Create an active scenario for each scenario beginning with a message
	 * that is unifiable with the given message event. The active scenario is
	 * added to the list of active scenarios, unless the list contains a similar
	 * (see helper method) instance already and the scenario is singular.
	 * 
	 * @see ActiveScenarioHelper#areEqualActiveScenarios(ActiveScenario, ActiveScenario)
	 * @param messageEvent
	 */
	private void activateScenarios(MessageEvent messageEvent) {
//		final EList<Collaboration> collaborations = ((SMLRuntimeStateGraph) this.getStateGraph()).getConfiguration()
//				.getSpecification().getCollaborations(); 
		final EList<Collaboration> collaborations = ((SMLRuntimeStateGraph) this.getStateGraph()).getConfiguration().getConsideredCollaborations();
		for (Collaboration collaboration : collaborations) {
			scenario: 
			for (Scenario scenario : collaboration.getScenarios()) {

				if(this.isSafetyViolationOccurredInGuarantees() && scenario.getKind() == ScenarioKind.GUARANTEE) continue;
				
				EList<ActiveScenario> activeScenarios = ActiveScenarioHelper.createActiveScenario(this, scenario,
						messageEvent);
				for(ActiveScenario activated : activeScenarios) {
					for(ActiveScenario existing: getActiveScenarios()) {
						if(ActiveScenarioHelper.areSimilarActiveScenarios(activated, existing)) {
							logger.debug("found similar scenario "+scenario.getName());
							if(existing.getScenario().isSingular()){
								continue scenario;
							}
						}
					}
					this.getActiveScenarios().add(activated);
				}
			}
		}
	}

	protected void storeMessageEventBlockedByActiveScenarioInformation(MessageEvent enabledMessageEvent, ActiveScenario activeScenario){
		
		logger.debug(enabledMessageEvent + " is blocked by " + activeScenario.getScenario().getName());
		//System.out.println(enabledMessageEvent + " is blocked by " + activeScenario.getScenario().getName());
		
		MessageEventBlockedInformation messageEventBlockedInformation 
			= RuntimeFactory.eINSTANCE.createMessageEventBlockedInformation();
		messageEventBlockedInformation.setMessageEventString(enabledMessageEvent.getMessageName());
		String scenarioKindAbbreviation = (activeScenario.getScenario().getKind() == ScenarioKind.ASSUMPTION)? "A"
				: (activeScenario.getScenario().getKind() == ScenarioKind.GUARANTEE)? "R" : "S";
		messageEventBlockedInformation.setDescription("BLOCKED By " + scenarioKindAbbreviation + " " + activeScenario.getScenario().getName());
		getMessageEventBlockedInformation().add(messageEventBlockedInformation);
	}

	protected void storeMessageEventBlockedInformation(MessageEvent enabledMessageEvent, String description){
		
		logger.debug("The message event " + enabledMessageEvent + " is blocked. Description " + description);
		//System.out.println("The message event " + enabledMessageEvent + " is blocked. Description " + description);
		
		MessageEventBlockedInformation messageEventBlockedInformation 
			= RuntimeFactory.eINSTANCE.createMessageEventBlockedInformation();
		messageEventBlockedInformation.setMessageEventString(enabledMessageEvent.getMessageName());
		messageEventBlockedInformation.setDescription(description);
		getMessageEventBlockedInformation().add(messageEventBlockedInformation);
	}

}
