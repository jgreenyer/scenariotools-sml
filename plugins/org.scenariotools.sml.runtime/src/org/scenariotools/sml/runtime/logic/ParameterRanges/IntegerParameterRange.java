/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic.ParameterRanges;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class IntegerParameterRange implements Iterable<Integer>{
	private int lowerBound;
	private int upperBound;
	
	protected IntegerParameterRange(int lowerBound, int upperBound){
		if (lowerBound > upperBound) {
		      throw new IllegalArgumentException("min must be <= max " + lowerBound + " <= " + upperBound);
		}
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
	}
	
	public boolean contains(Integer value) {
		return value >= lowerBound && value <= upperBound;
	}
	
	public int getUpperBound() {
		return upperBound;
	}
	
	public int getLowerBound() {
		return lowerBound;
	}
	
	
	public String toString() {
		return lowerBound + ".." + upperBound;
	}
	
	@Override
	public Iterator<Integer> iterator() {
		return new IntegerParameterRangeIterator(lowerBound, upperBound);
	}
	
	private static final class IntegerParameterRangeIterator implements Iterator<Integer> {
		private int cursor;
		private final int end;

		public IntegerParameterRangeIterator(int start, int end) {
			this.cursor = start;
			this.end = end;
		}

		public boolean hasNext() {
			return this.cursor <= end;
		}

		public Integer next() {
			if (!this.hasNext()) {
				throw new NoSuchElementException();
			}
			return cursor++;			
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}