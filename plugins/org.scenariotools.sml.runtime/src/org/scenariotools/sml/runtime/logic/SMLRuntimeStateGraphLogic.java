/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.runtime.plugin.Activator;
import org.scenariotools.sml.runtime.Transition;

public abstract class SMLRuntimeStateGraphLogic extends MinimalEObjectImpl.Container implements SMLRuntimeStateGraph {
	
	protected static Logger logger = Activator.getLogManager().getLogger(SMLRuntimeStateGraphLogic.class.getName());
	
	/**
	 */
	public SMLRuntimeState init(Configuration config) {
		
		logger.debug("create and set start state");
		logger.debug("create ElementContainer and set scenarios");

//		EObjectHelper.initialize(configuration);
	
		this.setConfiguration(config);
		
		this.setParameterRangesProvider(RuntimeFactory.eINSTANCE.createParameterRangesProvider());
		this.getParameterRangesProvider().init(config);
		
		this.setElementContainer(RuntimeFactory.eINSTANCE.createElementContainer());
//		this.getElementContainer().setEnabled(false);
		getElementContainer().setWaitEvent(RuntimeFactory.eINSTANCE.createWaitEvent());
		
		SMLRuntimeState initialState = RuntimeFactory.eINSTANCE.createSMLRuntimeState();
		this.getStates().add(initialState);
		this.setStartState(initialState);
		
		initialState.setDynamicObjectContainer(RuntimeFactory.eINSTANCE.createDynamicObjectContainer());
		
		logger.debug("create and init ObjectSystem");
		
		SMLObjectSystem objectSystem = RuntimeFactory.eINSTANCE.createSMLObjectSystem();
		objectSystem.init(config, initialState);
		
		logger.debug("setup all initializing ModalMessageEvents");
		
		//setupInitializingModalMessageEvents(stategraph, stategraph.getInitializingEnvironmentModalMessageEvent(), stategraph.getInitializingSystemModalMessageEvent());
		
		logger.debug("init start state");
		
		initialState.init(this.getElementContainer().getObjectSystem(objectSystem));
		
		addStateToElementContainer(this, initialState);
		
		return initialState;
	}

	/**
	 */
	public Transition generateSuccessor(SMLRuntimeState state, Event event) {
		
		Transition transition = state.getEventToTransitionMap().get(event);
		if (transition != null)
			return transition;

		// copy state
		SMLRuntimeState smlRuntimeStateCopy = copyState(state, this);

		smlRuntimeStateCopy.performStep(event);
		
		EList<Scenario> terminatedExistentialScenarios = new BasicEList<Scenario>();
		terminatedExistentialScenarios.addAll(smlRuntimeStateCopy.getTerminatedExistentialScenariosFromLastPerformStep());
		
		SMLRuntimeState successorSMLRuntimeState = addStateToElementContainer(this, smlRuntimeStateCopy);		
		if(successorSMLRuntimeState == smlRuntimeStateCopy)
			successorSMLRuntimeState.updateEnabledMessageEvents();
		
 		transition = RuntimeFactory.eINSTANCE.createTransition();
		transition.setEvent(event);
		transition.setSourceState(state);
		transition.setTargetState(successorSMLRuntimeState);
		
		for(Scenario scenario : terminatedExistentialScenarios){
			transition.getStringToBooleanAnnotationMap().put(scenario.getName(), true);
		}

		state.getEventToTransitionMap().put(event, transition);

		return transition;
	}

	/**
	 */
	public EList<Transition> generateAllSuccessors(SMLRuntimeState state) {
		EList<Transition> allTransitions = new BasicEList<Transition>();
		for (Event event : state.getEnabledEvents()) {
//			if (messageEvent.isConcrete())
			Transition t = this.generateSuccessor(state, event);
			allTransitions.add(t);
		}
		return allTransitions;
	}
	
	
	
	private static SMLRuntimeState addStateToElementContainer(SMLRuntimeStateGraph stategraph, SMLRuntimeState state) {
		
		SMLRuntimeState registeredState = stategraph.getElementContainer().getSMLRuntimeState(state);
		if(registeredState == state){
			return state;
		}else{
			state.setStateGraph(null);
			state.getIncomingTransition().clear();
			state.getOutgoingTransition().clear();
			return registeredState;
		}
	}
	
	protected static SMLRuntimeState copyState(SMLRuntimeState state, SMLRuntimeStateGraph containingStateGraph){
		// Containment references are copied by default
		// Containment references are i.e.: all ActiveParts
		//
		Copier copier = new Copier();
		
		EList<EObject> objectsToCopy = new BasicEList<EObject>();
		// add SMLRuntimeState
		objectsToCopy.add(state);
		// add dynamic ObjectSystem
		objectsToCopy.add(state.getDynamicObjectContainer());
		// add all ActiveScenarios
		objectsToCopy.addAll(state.getActiveScenarios());

		copier.copyAll(objectsToCopy);
		copier.copyReferences();
		
		SMLRuntimeState copiedState = (SMLRuntimeState) copier.get(state);
		copiedState.getOutgoingTransition().clear();
		// FIXME symptom, or cause of the problem?
		copiedState.getIncomingTransition().clear();
		copiedState.getEventToTransitionMap().clear();
		copiedState.getStringToBooleanAnnotationMap().clear();
		copiedState.getStringToStringAnnotationMap().clear();
		copiedState.getStringToEObjectAnnotationMap().clear();
		
		copiedState.setSystemChoseToWait(false);
		copiedState.getMessageEventBlockedInformation().clear();
		
		copiedState.setStateGraph(containingStateGraph);

		return copiedState;
	}
}
