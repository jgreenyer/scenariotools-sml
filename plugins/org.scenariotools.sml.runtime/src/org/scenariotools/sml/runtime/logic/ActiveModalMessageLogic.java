/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.ParameterValue;
import org.scenariotools.sml.ExpectationKind;
import org.scenariotools.sml.ModalMessage;
import org.scenariotools.sml.runtime.ActiveMessageParameter;
import org.scenariotools.sml.runtime.ActiveModalMessage;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.emf.custom.MessageEventUniqueEList;
import org.scenariotools.sml.runtime.impl.ActivePartImpl;
import org.scenariotools.sml.runtime.logic.helper.MessageEventHelper;

public abstract class ActiveModalMessageLogic extends ActivePartImpl implements ActiveModalMessage {
	/**
	 * Add a message event to the parent active part's 
	 * {@link ActivePart#getCoveredEvents() coveredEvents} list and to the {@link ActiveScenario#getAlphabet() alphabet} of the containing active scenario as well.
 	 * The message event is computed from the modal message and the parent scenario's role binding map.
 	 * @param interaction not used
 	 * @param roleBindings the role bindings of the <code>activeScenario</code> parameter. Should not be necessary to provide?
 	 * @param parentActivePart the parent active part 
 	 * @param smlRuntimeState not used
 	 * 
 	 * 
	 */
	@Override
	public void init(ActiveScenarioRoleBindings roleBindings, 
			ActivePart parentActivePart,
			ActiveScenario activeScenario) {
		
		super.init(roleBindings, parentActivePart, activeScenario);
		
		// protect for double initialization
		if(this.getCoveredEvents().isEmpty()){
			ModalMessage message = (ModalMessage) this.getInteractionFragment();
	
			EObject sender = roleBindings.getRoleBindings().get(message.getSender());
			EObject receiver = roleBindings.getRoleBindings().get(message.getReceiver());
	
			// TODO Refactor this static call
			MessageEvent messageEvent = MessageEventHelper.createMessageEventInActiveScenario(sender,
					receiver, message, activeScenario);
	
			// initialize parameters
			for (int i = 0; i < getActiveMessageParameters().size(); i++) {
				ActiveMessageParameter activeMessageParameter = getActiveMessageParameters().get(i);
				ParameterValue parameterValue = messageEvent.getParameterValues().get(i);
				activeMessageParameter.init(parameterValue);
			}
			
			this.getCoveredEvents().add(messageEvent);
			parentActivePart.getCoveredEvents().addAll(this.getCoveredEvents());
		}
	}

	/**
	 * Computes the unification result of <code>event</code> with the event that
	 * corresponds to the {@link ModalMessage}. Depending on the former's value,
	 * returns:
	 * <ul>
	 * <li><del>{@link ActiveScenarioProgress#PROGRESS PROGRESS}</del>
	 * {@link ActiveScenarioProgress#INTERACTION_END INTERACTION_END} if the
	 * events could be unified</li>
	 * <li>{@link ActiveScenarioProgress#NO_PROGRESS NO_PROGRESS} if
	 * <code>event</code> is not unifiable with any event of the current
	 * scenario</li>
	 * <li>{@link ActiveScenarioProgress#COLD_VIOLATION COLD}/
	 * {@link ActiveScenarioProgress#SAFETY_VIOLATION SAFETY_VIOLATION} if
	 * <code>event</code> lead to a cold or safety violation</li>
	 * </ul>
	 */
	@Override
	public ActiveScenarioProgress performStep(MessageEvent event, ActiveScenario activeScenario,
			SMLRuntimeState smlRuntimeState) {

		for (MessageEvent enabledEvent : getCoveredEvents()) {
			if (event.isParameterUnifiableWith(enabledEvent)) {
				
				// check for side effects and execute them
				for (int i = 0; i < getActiveMessageParameters().size(); i++) {
					ActiveMessageParameter activeMessageParameter = getActiveMessageParameters().get(i);
					if(activeMessageParameter.hasSideEffectsOnUnification()){
						ParameterValue parameterValue = enabledEvent.getParameterValues().get(i);
						ParameterValue parameterValueFromOccuredMessage = event.getParameterValues().get(i);
						activeMessageParameter.executeSideEffectsOnUnification(parameterValueFromOccuredMessage, parameterValue, activeScenario, smlRuntimeState);
					}
				}
				this.getEnabledEvents().clear();
				return ActiveScenarioProgress.MESSAGE_PROGRESSED;
			}
		}
		
		// check how (and if) step event is violating this scenario.
		return computeProgressFromComputedViolationKind(event, activeScenario);
	}
	
	@Override
	public ActiveScenarioProgress postPerformStep(MessageEvent event, ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		// if enabled events list is not empty then the message was not parameter unifiable.
		if(this.getEnabledEvents().isEmpty()){
			return ActiveScenarioProgress.INTERACTION_END;
		}else{
			return ActiveScenarioProgress.NO_PROGRESS;
		}
	}
	
	/**
	 * If the message is requested currently, return the message event corresponding to this modal message. Otherwise return an empty list. 
	 */
	@Override
	public EList<MessageEvent> getRequestedEvents() {
		// Requested events are only in enabled active interactions with no
		// enabled nested active interactions.
		// These active interactions always have only one enabled fragment and
		// only one enabled MessageEvent.
		// So if the fragment is requested the MessageEvent is requested.
		// If the fragment is a condition, there are no requested events.

		EList<MessageEvent> requestedMessageEvents = new MessageEventUniqueEList<MessageEvent>();

		ModalMessage enabledMessage = (ModalMessage) this.getInteractionFragment();

		if (enabledMessage.getExpectationKind() != ExpectationKind.NONE) { // TODO: Review correctness of enabledMessage.getExpectationKind != ExpectationKind.NONE. Was isRequested()
			requestedMessageEvents.addAll(this.getCoveredEvents());
		}

		return requestedMessageEvents;
	}
	
	@Override
	public boolean isInRequestedState() {
		return ((ModalMessage) this.getInteractionFragment()).getExpectationKind() != ExpectationKind.NONE;  // TODO: Review correctness of enabledMessage.getExpectationKind != ExpectationKind.NONE. Was isRequested()
	}
	
	@Override
	public boolean isInStrictState() {
		return ((ModalMessage) this.getInteractionFragment()).isStrict();
	}
	
	/**
	 * Updates the parameter values for the event corresponding to this modal message.
	 * @param smlRuntimeState state whose dynamic object container is used for updating the parameter values. Should be replaced if possible.
	 */
	@Override
	public void updateMessageEvents(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		
		// retrieve the enabled event
		MessageEvent enabledEvent = this.getCoveredEvents().get(0);
			
		if (enabledEvent.isParameterized()) {		
			// update parameters
			for (int i = 0; i < getActiveMessageParameters().size(); i++) {
				ActiveMessageParameter activeMessageParameter = getActiveMessageParameters().get(i);
				ParameterValue parameterValue = enabledEvent.getParameterValues().get(i);
				activeMessageParameter.update(parameterValue, this, activeScenario, smlRuntimeState);
			}		
		}
	}

	@Override
	public ActiveScenarioProgress enable(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		// update MessageEvent and especially initialize parameter values
		this.updateMessageEvents(activeScenario, smlRuntimeState);
		this.getEnabledEvents().addAll(getCoveredEvents());
		// nothing else to do for parent
		return ActiveScenarioProgress.NO_PROGRESS;
	}
}
