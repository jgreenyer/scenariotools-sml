/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic;

import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcorePackage;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.VariableFragment;
import org.scenariotools.sml.expressions.scenarioExpressions.Expression;
import org.scenariotools.sml.expressions.scenarioExpressions.TypedVariable;
import org.scenariotools.sml.expressions.scenarioExpressions.VariableAssignment;
import org.scenariotools.sml.expressions.scenarioExpressions.VariableDeclaration;
import org.scenariotools.sml.expressions.scenarioExpressions.VariableExpression;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.ActiveVariableFragment;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.impl.ActivePartImpl;
import org.scenariotools.sml.runtime.plugin.Activator;

public abstract class ActiveVariableFragmentLogic extends ActivePartImpl implements ActiveVariableFragment {

	protected static Logger logger = Activator.getLogManager().getLogger(
			ActiveVariableFragmentLogic.class.getName());
	
	@Override
	public void init(ActiveScenarioRoleBindings roleBindings,
			ActivePart parentActivePart, 
			ActiveScenario activeScenario) {
		
		super.init(roleBindings, parentActivePart, activeScenario);
	}
	
	@Override
	public ActiveScenarioProgress enable(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState){
		return ActiveScenarioProgress.CONTINUE;
	}
	
	@Override
	public ActiveScenarioProgress performStep(MessageEvent event,
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
	
		VariableFragment variableFragment = (VariableFragment) this.getInteractionFragment();
		VariableExpression variableExpr = variableFragment.getExpression();

		if (variableExpr instanceof VariableDeclaration) {
			handleVariableDeclaration(activeScenario, smlRuntimeState, variableExpr);
		} else if (variableExpr instanceof VariableAssignment) {
			handleVariableAssignment(activeScenario, smlRuntimeState, variableExpr);
		}
		
		return ActiveScenarioProgress.INTERACTION_END;
	}

	private void handleVariableDeclaration(ActiveScenario activeScenario, 
			SMLRuntimeState smlRuntimeState, VariableExpression variableExpr) {
		
		VariableDeclaration variableDeclaration = (VariableDeclaration) variableExpr;
		Expression expr = variableDeclaration.getExpression();
		Object value = evaluateExpression(expr, smlRuntimeState, this, activeScenario);
		
		updateVariable(getParentActiveInteraction(), variableDeclaration, value);
		
		if(logger.isDebugEnabled()){
			if(value == null){
				logger.debug("defined variable " + variableDeclaration.getName());
			}else{
				logger.debug("defined variable " + variableDeclaration.getName()
					+ " with value \"" + value.toString() + "\"");
			}
		}
	}
	
	private void handleVariableAssignment(ActiveScenario activeScenario, 
			SMLRuntimeState smlRuntimeState, VariableExpression variableExpr) {
		
		VariableAssignment va = (VariableAssignment) variableExpr;
		VariableDeclaration v = va.getVariable();
		Object value = evaluateExpression(va.getExpression(), smlRuntimeState, this, activeScenario);
		
		// put the variable where it is declared
		ActivePart activeInteractionWithVar = this;
		while (activeInteractionWithVar.getParentActiveInteraction() != null
				&& activeInteractionWithVar.getVariableMap().get(v) == null) {
			activeInteractionWithVar = activeInteractionWithVar.getParentActiveInteraction();
		}
		updateVariable(activeInteractionWithVar, v, value);
		
		if(logger.isDebugEnabled()){
			logger.debug("assigned to variable "
					+ ((VariableDeclaration) v).getName() 
					+ "  the value \""
					+ value.toString() 
					+ "\"");
		}
	}

	private void updateVariable(ActivePart activeInteractionWithVar, VariableDeclaration v, Object value) {
		if (isEObject(v)) {
			activeInteractionWithVar.getEObjectVariableMap().put(v, (EObject) value);
		} else {
			activeInteractionWithVar.getVariableMap().put(v, value);
		}
	}
	
	private boolean isEObject(VariableDeclaration variableDeclaration) {
		EClassifier t = (((TypedVariable) variableDeclaration).getType());
		return (t != EcorePackage.Literals.EINT 
				&& t != EcorePackage.Literals.ESTRING 
				&& t != EcorePackage.Literals.EBOOLEAN)
				|| EcorePackage.Literals.EENUM.isInstance(t);
	}

	@Override
	public ActiveScenarioProgress postPerformStep(MessageEvent event, 
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		
		return performStep(event, activeScenario, smlRuntimeState);
	}
}
