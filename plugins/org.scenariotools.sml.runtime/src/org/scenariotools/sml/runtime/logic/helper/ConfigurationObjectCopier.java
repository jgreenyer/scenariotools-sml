/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.runtime.configuration.RoleAssignment;
import org.scenariotools.sml.runtime.configuration.RoleBindings;

/**
 * Makes defensive copy of instance model objects and provides a static role
 * bindings map that refers to the copies.
 * 
 * @author Nils
 *
 */
public class ConfigurationObjectCopier {
	private final Copier copier;
	private final List<EObject> objectCopies;
	private final List<EObject> rootCopies;
	private final Configuration configuration;
	private final Map<Role, EObject> staticRoleBindingsMap;

	public ConfigurationObjectCopier(Configuration c) {
		copier = new Copier(true);
		objectCopies = new ArrayList<EObject>();
		rootCopies = new ArrayList<EObject>();
		staticRoleBindingsMap = new HashMap<Role, EObject>();
		this.configuration = c;
		copy();
		copyRoleBindings();

	}

	private void copyRoleBindings() {
		for (RoleBindings bindingsContainer : configuration.getStaticRoleBindings()) {
			for (RoleAssignment assignment : bindingsContainer.getBindings()) {
				final EObject copy = copier.get(assignment.getObject());
				staticRoleBindingsMap.put(assignment.getRole(), copy);
			}
		}
	}

	/**
	 * Populates the copier map, the object and root-object copies
	 */
	@SuppressWarnings("unused")
	private void copy() {
		List<EObject> allCopiedObjects = new ArrayList<EObject>();
		List<EObject> rootObjects = configuration.getInstanceModelRootObjects();
		for (EObject root : rootObjects) {
			allCopiedObjects.add(root);
//			for (Iterator<EObject> contentIterator = root.eAllContents(); contentIterator.hasNext();) {
//				EObject o = contentIterator.next();
//				allCopiedObjects.add(o);
//			}
		} ;
		Object copyResult = copier.copyAll(allCopiedObjects);
		copier.copyReferences();
		objectCopies.addAll(copier.values());
		for (EObject root : rootObjects) {
			rootCopies.add(copier.get(root));
		}
	}

	/**
	 * Returns an unmodifiable list of copied objects from the instance model.
	 * 
	 * @return
	 */
	public final List<EObject> getObjectCopies() {
		return Collections.unmodifiableList(objectCopies);
	}

	/**
	 * Returns an unmodifiable list of copied root objects from the instance
	 * models.
	 */
	public final List<EObject> getRootObjectCopies() {
		return Collections.unmodifiableList(rootCopies);
	}

	/**
	 * Returns an unmodifiable map from static roles to the objects that play
	 * them.
	 * 
	 * @return
	 */
	public final Map<Role, EObject> getStaticRoleBindingsMap() {
		return Collections.unmodifiableMap(staticRoleBindingsMap);
	}
}
