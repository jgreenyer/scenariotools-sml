/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic.helper;

import java.util.Iterator;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.ParameterValue;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.Message;
import org.scenariotools.sml.ParameterBinding;
import org.scenariotools.sml.WildcardParameterExpression;
import org.scenariotools.sml.VariableBindingParameterExpression;
import org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ParameterRangesProvider;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.logic.ParameterRanges.ParameterValues;
import org.scenariotools.sml.runtime.plugin.Activator;

public class MessageEventHelper {

	protected static Logger logger = Activator.getLogManager().getLogger(MessageEventHelper.class.getName());

	public static MessageEvent createCollectionOperationMessageEvent(MessageEvent messageEvent,
			EStructuralFeature feature, CollectionOperation op) {
		messageEvent.setCollectionOperation(op);
		switch (op) {
		case ADD:
		case ADD_TO_FRONT:
		case REMOVE:
			ParameterValue v = createParameterValue(feature.getEType());
			v.setEParameter(null);
			v.setValue(null);
			v.setStrucFeatureOrEOp(feature);
			messageEvent.getParameterValues().add(v);
		default:
		}
		return messageEvent;
	}

	public static MessageEvent createSetFeatureMessageEvent(MessageEvent messageEvent, EStructuralFeature feature) {
		ParameterValue val = createParameterValue(feature.getEType());
		val.setEParameter(null);
		val.setStrucFeatureOrEOp(feature);
		messageEvent.getParameterValues().add(val);
		return messageEvent;
	}

	public static MessageEvent createOperationMessageEvent(MessageEvent event, EOperation operation) {
		for (EParameter parameter : operation.getEParameters()) {
			ParameterValue val = createParameterValue(parameter.getEType());
			val.setEParameter(parameter);
			val.setStrucFeatureOrEOp(parameter);
			event.getParameterValues().add(val);
		}
		return event;
	}

	public static MessageEvent createOperationMessageEvent(MessageEvent event, Message message) {
		EOperation operation = (EOperation) message.getModelElement();
		for (int i = 0; i < operation.getEParameters().size(); i++) {
			EParameter parameter = operation.getEParameters().get(i);
			ParameterValue val = createParameterValue(parameter.getEType());
			val.setEParameter(parameter);
			val.setStrucFeatureOrEOp(parameter);
			event.getParameterValues().add(val);
			
			ParameterBinding parameterBinding = message.getParameters().get(i);
			if (parameterBinding.getBindingExpression() instanceof VariableBindingParameterExpression
					|| parameterBinding.getBindingExpression() instanceof WildcardParameterExpression)
				val.setWildcardParameter(true);
			
		}
		return event;
	}

	static MessageEvent createMessageEvent(EObject sendingEObject, EObject receivingEObject, Message message,
			CollectionOperation op) {

		MessageEvent messageEvent = RuntimeFactory.eINSTANCE.createMessageEvent();
		messageEvent.setSendingObject(sendingEObject);
		messageEvent.setReceivingObject(receivingEObject);
		messageEvent.setTypedElement(message.getModelElement());

		if (message.getModelElement() instanceof EOperation) {
			return createOperationMessageEvent(messageEvent, message);
		} else if (message.getModelElement() instanceof EStructuralFeature) {
			EStructuralFeature feature = (EStructuralFeature) message.getModelElement();
			if (feature.isMany()) {
				return createCollectionOperationMessageEvent(messageEvent, feature, op);
			} else {
				return createSetFeatureMessageEvent(messageEvent, feature);
			}
		} else {
			throw new IllegalArgumentException();
		}
	}

	private static ParameterValue createParameterValue(EClassifier type) {
		ParameterValue result = null;
		if (type == EcorePackage.Literals.EBOOLEAN) {
			result = RuntimeFactory.eINSTANCE.createBooleanParameterValue();
		} else if (type == EcorePackage.Literals.ESTRING) {
			result = RuntimeFactory.eINSTANCE.createStringParameterValue();
		} else if (type == EcorePackage.Literals.EINT) {
			result = RuntimeFactory.eINSTANCE.createIntegerParameterValue();
		} else if (type instanceof EClass) {
			result = RuntimeFactory.eINSTANCE.createEObjectParameterValue();
		} else if (type instanceof EEnum) {
			result = RuntimeFactory.eINSTANCE.createEEnumParameterValue();
		} else {
			throw new IllegalArgumentException("Unknown type" + type);
		}
		result.setUnset(true);
		return result;
	}

	/**
	 * Use for creating message events within active scenarios: the method will
	 * first check whether an equal message event already exists in the active
	 * scenario's alphabet. If this is true, it will return the method will
	 * first return this alphabet message event. Otherwise it will create a new
	 * message event.
	 * 
	 * @param sendingEObject
	 * @param receivingEObject
	 * @param message
	 * @param activeScenario
	 * @return
	 */
	public static MessageEvent createMessageEventInActiveScenario(EObject sendingEObject, EObject receivingEObject,
			Message message, ActiveScenario activeScenario) {
		MessageEvent messageEvent = createMessageEvent(sendingEObject, receivingEObject, message);
		activeScenario.getAlphabet().add(messageEvent);
		return messageEvent;
	}

	// TODO use ObjectSystem.isEnvironmentMessageEvent(messageEvent)
	@Deprecated
	public static boolean isSystemMessage(MessageEvent messageEvent, SMLObjectSystem objectSystem) {
		return objectSystem.isControllable(messageEvent.getSendingObject());
	}

	public static EList<MessageEvent> getAllConcreteMessageEvents(MessageEvent messageEvent,
			ParameterRangesProvider parameterRangesProvider, SMLObjectSystem smlObjectSystem) {

		EList<ParameterValues<?>> parameterValues = new BasicEList<ParameterValues<?>>();
		EList<MessageEvent> result = new BasicEList<MessageEvent>();

		for (ParameterValue messagePV : messageEvent.getParameterValues()) {
			if (messagePV.isWildcardParameter()) {
				ETypedElement ep = messagePV.getStrucFeatureOrEOp();
				ParameterValues<?> pv = parameterRangesProvider.getParameterValues(ep, smlObjectSystem);
				parameterValues.add(pv);
			} else {
				ParameterValues<?> pv = parameterRangesProvider.getSingelParameterValue(messagePV.getValue());
				parameterValues.add(pv);
			}
		}

		combineParameters(new BasicEList<Object>(), parameterValues, messageEvent, result);
		return result;
	}

	public static EList<MessageEvent> getMergedConcreteMessageEvents(MessageEvent messageEvent,
			ParameterRangesProvider parameterRangesProvider, SMLObjectSystem smlObjectSystem) {

		EList<ParameterValues<?>> parameterValues = new BasicEList<ParameterValues<?>>();
		EList<MessageEvent> result = new BasicEList<MessageEvent>();

		for (ParameterValue messagePV : messageEvent.getParameterValues()) {
			ETypedElement ep = messagePV.getStrucFeatureOrEOp();
			ParameterValues<?> pv = parameterRangesProvider.getParameterValues(ep, smlObjectSystem);
			parameterValues.add(pv);
		}

		combineParameters(new BasicEList<Object>(), parameterValues, messageEvent, result);
		return result;
	}

	private static void combineParameters(EList<Object> leftList, EList<ParameterValues<?>> rightList,
			MessageEvent messageEvent, EList<MessageEvent> result) {
		// recursion end, add message to result List
		if (rightList.size() == 0) {
			MessageEvent m = EcoreUtil.copy(messageEvent);

			Iterator<ParameterValue> parameterIter = m.getParameterValues().iterator();
			Iterator<Object> parameterObjectIter = leftList.iterator();

			while (parameterIter.hasNext() && parameterObjectIter.hasNext()) {
				ParameterValue pv = parameterIter.next();
				Object po = parameterObjectIter.next();

				pv.setValue(po);
				pv.setUnset(false);
				pv.setWildcardParameter(false);
			}
			if (logger.isDebugEnabled()) {
				if (parameterIter.hasNext() || parameterObjectIter.hasNext()) {
					logger.debug("Message Parameter list and Parameter Object list have not the same length!");
				}
			}
			result.add(m);
		} else {

			// recursion
			// Max recursion depth is number of parameter.
			ParameterValues<?> parameterValues = rightList.get(0);
			EList<ParameterValues<?>> newRightList = new BasicEList<ParameterValues<?>>();
			newRightList.addAll(rightList);
			newRightList.remove(0);

			for (Object concreteParameterValue : parameterValues) {
				EList<Object> newLeftList = new BasicEList<Object>();
				newLeftList.addAll(leftList);
				newLeftList.add(concreteParameterValue);
				combineParameters(newLeftList, newRightList, messageEvent, result);
			}
		}
	}

	public static MessageEvent createMessageEvent(EObject sendingEObject, EObject receivingEObject, Message message) {
		return MessageEventHelper.createMessageEvent(sendingEObject, receivingEObject, message,
				message.getCollectionModification());

	}

}
