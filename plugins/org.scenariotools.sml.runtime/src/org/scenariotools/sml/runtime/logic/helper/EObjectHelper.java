/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic.helper;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

public class EObjectHelper {

//	private static ClassLoaderHelper cl;
//	public static void initialize(Configuration c) {
//		if (c.getGenmodels().isEmpty()) {
//			cl = null;
//			return;
//		}
//		cl = new ClassLoaderHelper(c);
//		c.getSpecification().getDomains().set(0, cl.getEPackages().get(0));
//	}

	public static void setName(EObject eObject, String name) {
		EStructuralFeature nameFeature = eObject.eClass().getEStructuralFeature("name");
		if (nameFeature != null && "EString".equals(nameFeature.getEType().getName())){
			eObject.eSet(nameFeature, name);
		}else
		throw new IllegalArgumentException("The feature 'name' is not a valid feature of object " + eObject);
	}

	public static String getName(EObject eObject){
		if(eObject != null){
			EClass eClass = eObject.eClass();
			EStructuralFeature nameFeature = eClass.getEStructuralFeature("name");
			if (nameFeature != null && "EString".equals(nameFeature.getEType().getName()))
				return (String) eObject.eGet(nameFeature);
			else
				return eObject.hashCode() + "";				
		}
		return "<null>";
	}

	
	public static EList<EObject> getAllReferencedObjectsOfEClass(EObject eObject, EClass eClass){
		final EList<EObject> allReferencedObjectsOfEClass = new UniqueEList<EObject>();
		
		for (EReference eReference : eObject.eClass().getEAllReferences()) {
				for (EObject referencedObject : getAllReferencedObjects(eObject, eReference)) {
					if(eClass.isInstance(referencedObject))
						allReferencedObjectsOfEClass.add(referencedObject);
				}
		}
		return allReferencedObjectsOfEClass;
	}
	
	public static void setEReferenceByName(EObject eObject, String eReferenceName, EObject value){
		for (EReference eReference : eObject.eClass().getEAllReferences()) {
			if (eReferenceName.equals(eReference.getName())) {
				eObject.eSet(eReference, value);
				return;
			}
		}
		throw new IllegalArgumentException("The feature '" + eReferenceName + "' is not a valid feature of object " + eObject);
	}

	public static EObject getEReferenceByName(EObject eObject, String eReferenceName){
		for (EReference eReference : eObject.eClass().getEAllReferences()) {
			if (eReferenceName.equals(eReference.getName())) {
				return (EObject) eObject.eGet(eReference);
			}
		}
      throw new IllegalArgumentException("The feature '" + eReferenceName + "' is not a valid feature of object " + eObject);
	}

	public static boolean getBooleanEAttributeByName(EObject eObject, String eAttributeName){
		for (EAttribute eAttribute : eObject.eClass().getEAllAttributes()) {
			if (eAttributeName.equals(eAttribute.getName())) {
				return (boolean) eObject.eGet(eAttribute);
			}
		}
      throw new IllegalArgumentException("The feature '" + eAttributeName + "' is not a valid feature of object " + eObject);
	}
	
	
	/**
	 * @return A list of eobjects that  <code>eObject</code> references by <code>eReference</code>. In case <code>eReference</code> is not a feature of <code>eObject</code>'s
	 *  eclass return an empty list.
	 */
	@SuppressWarnings("unchecked")
	public static EList<EObject> getAllReferencedObjects(EObject eObject, EReference eReference) {
		final EList<EObject> allReferencedObjectsOfEClass = new UniqueEList<EObject>();
		if(!eObject.eClass().getEAllReferences().contains(eReference))
			return allReferencedObjectsOfEClass;
		if (eReference.isMany()) {
			allReferencedObjectsOfEClass.addAll((EList<EObject>) eObject.eGet(eReference));
		} else {
			allReferencedObjectsOfEClass.add((EObject) eObject.eGet(eReference));
		}
		return allReferencedObjectsOfEClass;
	}
	
	
}
