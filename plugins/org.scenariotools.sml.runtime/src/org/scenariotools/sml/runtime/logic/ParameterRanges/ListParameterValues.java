/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic.ParameterRanges;

import java.util.Iterator;

import org.eclipse.emf.common.util.EList;

public class ListParameterValues<T> implements ParameterValues<T>{
	private EList<T> ranges;
	
	protected ListParameterValues(EList<T> ranges){
		this.ranges = ranges;
	}
	
	@Override
	public boolean contains(T value) {
		return ranges.contains(value);
	}
	
	@Override
	public String toString() {
		return ranges.toString();
	}

	@Override
	public Iterator<T> iterator() {
		return ranges.iterator();
	}
}