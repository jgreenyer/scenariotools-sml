/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic.helper;

import org.scenariotools.sml.expressions.scenarioExpressions.Expression;
import org.scenariotools.sml.runtime.Context;

/**
 * Evaluates expressions of the SML expression language.
 * 
 * @author Nils
 * @see Expression
 * @see Context
 */
public interface Interpreter {

	/**
	 * Evaluates an expression. Variable values are looked up in the supplied
	 * contexts in the given order. The expression is assumed to be syntactically valid.
	 * @param e
	 *            A syntactically valid expression
	 * @param contexts
	 *            one or more contexts that are used for lookup of variable values
	 * @return The value of <em>e</em> or {@link Context#UNDEFINED UNDEFINED}
	 */
	Object evaluate(Expression e, Context... contexts);
}
