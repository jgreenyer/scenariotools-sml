/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic;

import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.ViolationCondition;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.ActiveViolationCondition;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.impl.ActivePartImpl;

public abstract class ActiveViolationConditionLogic extends ActivePartImpl implements ActiveViolationCondition {

	@Override
	public void init(ActiveScenarioRoleBindings roleBindings,
			ActivePart parentActivePart, 
			ActiveScenario activeScenario) {
		
		super.init(roleBindings, parentActivePart, activeScenario);
	}
	
	/**
	 * Returns {@link ActiveScenarioProgress#SAFETY_VIOLATION SAFETY_VIOLATION} if the condition evaluates to <code>true</code>,
	 * {@link ActiveScenarioProgress#INTERACTION_END INTERACTION_END} otherwise. 
	 */
	@Override
	public ActiveScenarioProgress performStep(MessageEvent event, ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		
		boolean conditionFulfilled = isViolationConditionFulfilled(activeScenario, smlRuntimeState);
		
		if (conditionFulfilled)
			return ActiveScenarioProgress.SAFETY_VIOLATION;
		else
			return ActiveScenarioProgress.INTERACTION_END;
	}
	
	private boolean isViolationConditionFulfilled(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		return evaluateConditionExpression(
				((ViolationCondition) this.getInteractionFragment()).getConditionExpression(),
				smlRuntimeState,
				activeScenario, 
				this);
	}
	
	@Override
	public ActiveScenarioProgress postPerformStep(MessageEvent event, ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		return performStep(event, activeScenario, smlRuntimeState);
	}
	
	@Override
	public ActiveScenarioProgress enable(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState){
		return ActiveScenarioProgress.CONTINUE;
	}
}
