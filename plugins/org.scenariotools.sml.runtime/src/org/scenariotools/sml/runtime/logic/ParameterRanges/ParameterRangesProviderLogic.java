/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.logic.ParameterRanges;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.ParameterValue;
import org.scenariotools.sml.AbstractRanges;
import org.scenariotools.sml.EnumRanges;
import org.scenariotools.sml.EventParameterRanges;
import org.scenariotools.sml.IntegerRanges;
import org.scenariotools.sml.RangesForParameter;
import org.scenariotools.sml.StringRanges;
import org.scenariotools.sml.runtime.ParameterRangesProvider;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.configuration.Configuration;

public abstract class ParameterRangesProviderLogic extends MinimalEObjectImpl.Container implements ParameterRangesProvider{
	Map<ETypedElement, ParameterValues<?>> parameterToParameterValuesMap = new HashMap<ETypedElement, ParameterValues<?>>();

	public void init(Configuration config) {
		// Read parameter ranges from configuration and add them to eParameterToParameterValuesMap
		// Boolean Ranges are implicit. They will be created only if they needed in getParameterValues()
		
		for(EventParameterRanges eventParameterRanges :config.getSpecification().getEventParameterRanges()){
			for(RangesForParameter range : eventParameterRanges.getRangesForParameter()){
				
				ETypedElement key = range.getParameter();
				AbstractRanges abstractRanges = range.getRanges();
				
				if(abstractRanges instanceof IntegerRanges){						
					addIntegerRanges(key, abstractRanges);
					
				}else if(abstractRanges instanceof EnumRanges){							
					addEnumRanges(key, abstractRanges);
					
				}else if(abstractRanges instanceof StringRanges){							
					addStringRanges(key, abstractRanges);	
					
				}else{
					throw new UnsupportedOperationException("ParameterType not Supported!");
				}
			}
		}
	}
	
	public void init(EList<MessageEvent> messageEvents){
		Map<ETypedElement, EList<Object>> parameterToParameterListMap = new HashMap<ETypedElement, EList<Object>>();
		
		for(MessageEvent messageEvent : messageEvents){
			if(messageEvent.isParameterized()){
				for(ParameterValue parameterValue : messageEvent.getParameterValues()){
					if(parameterValue.isWildcardParameter() 
							|| parameterValue.isUnset())continue;
						
					
					ETypedElement eType = parameterValue.getStrucFeatureOrEOp();
	
					if(parameterToParameterListMap.containsKey(eType)){
						if(!parameterToParameterListMap.get(eType).contains(parameterValue.getValue())){
							parameterToParameterListMap.get(eType).add(parameterValue.getValue());
						}
					}else{
						EList<Object> list = new BasicEList<>();
						list.add(parameterValue.getValue());
						parameterToParameterListMap.put(eType, list);
					}
				}
			}
		}
		
		for(Entry<ETypedElement, EList<Object>> entry : parameterToParameterListMap.entrySet()){
			parameterToParameterValuesMap.put(entry.getKey(), ParameterValues.Factory.getListParameterValues(entry.getValue()));
		}
	}

	private void addStringRanges(ETypedElement key, AbstractRanges abstractRanges) {
		StringRanges stringRange = (StringRanges) abstractRanges;
		ParameterValues<String> pvString = ParameterValues.Factory.getListParameterValues(stringRange.getValues());
		
		parameterToParameterValuesMap.put(key, pvString);
	}

	private void addEnumRanges(ETypedElement key, AbstractRanges abstractRanges) {
		EnumRanges enumRanges = (EnumRanges) abstractRanges;
		ParameterValues<EEnumLiteral> pvEnum = ParameterValues.Factory.getListParameterValues(enumRanges.getValues());
		
		parameterToParameterValuesMap.put(key, pvEnum);
	}

	private void addIntegerRanges(ETypedElement key, AbstractRanges abstractRanges) {
		IntegerRanges intRange = (IntegerRanges) abstractRanges;	
		
		EList<IntegerParameterRange> rangesList;
		if(intRange.getMin() == 0 && intRange.getMax() == 0){
			// same as not initialized - not allowed
			rangesList = null;
		}else{
			rangesList= new BasicEList<IntegerParameterRange>();
			rangesList.add(ParameterValues.Factory.getIntegerParameterRange(intRange.getMin(), intRange.getMax()));
		}
		EList<Integer> values = intRange.getValues();
		ParameterValues<Integer> pvInteger = ParameterValues.Factory.getIntegerParameterValues(rangesList, values);
		
		parameterToParameterValuesMap.put(key, pvInteger);
	}
	
	public ParameterValues<?> getParameterValues(ETypedElement eParameter, SMLObjectSystem smlObjectSystem) {
		ParameterValues<?> result = parameterToParameterValuesMap.get(eParameter);
		if (result != null)
			return result;
		
		if (eParameter.getEType().getName() == "EBoolean") {
			ParameterValues<Boolean> pv = ParameterValues.Factory.getBooleanParameterValues();
			this.parameterToParameterValuesMap.put(eParameter, pv);
			return pv;
		} else if (eParameter.getEType() instanceof EClass){
			EList<EObject> eObjectList = new BasicEList<EObject>();
			for (EObject eObject : smlObjectSystem.getObjects()) {
				if (eObject.eClass() == eParameter.getEType()
						|| eObject.eClass().getEAllSuperTypes().contains(eParameter.getEType()))
					eObjectList.add(eObject);
			}
			ParameterValues<EObject> pv = ParameterValues.Factory.getListParameterValues(eObjectList);
			return pv; 
		} else 
			return ParameterValues.Factory.getEmptyParameterValues();
	}
	
	public ParameterValues<?> getSingelParameterValue(Object value){
		EList<Object> values = new BasicEList<Object>();
		values.add(value);
		return ParameterValues.Factory.getListParameterValues(values);
	}
	
	public ParameterValues<?> getTestIntegerParameterValues(){
		EList<IntegerParameterRange> ranges = new BasicEList<IntegerParameterRange>();
		ranges.add(ParameterValues.Factory.getIntegerParameterRange(1, 10));
		ranges.add(ParameterValues.Factory.getIntegerParameterRange(12, 12));
		ranges.add(ParameterValues.Factory.getIntegerParameterRange(1000, 1007));
		
		EList<Integer> values = new BasicEList<Integer>();
		values.add(-3);
		values.add(-33);
		values.add(77);
		values.add(777);
		values.add(455);
		values.add(3333);
		
		return ParameterValues.Factory.getIntegerParameterValues(ranges, values);
	}
	
	public boolean containsParameterValues(ETypedElement eParameter) {
		return parameterToParameterValuesMap.containsKey(eParameter);
		
	}
}
