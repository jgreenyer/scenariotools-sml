/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.impl;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.MultiActiveScenarioInitializations;
import org.scenariotools.sml.runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Multi Active Scenario Initializations</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.impl.MultiActiveScenarioInitializationsImpl#getActiveScenarioToActiveScenarioProgressMap <em>Active Scenario To Active Scenario Progress Map</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MultiActiveScenarioInitializationsImpl extends MinimalEObjectImpl.Container implements MultiActiveScenarioInitializations {
	/**
	 * The cached value of the '{@link #getActiveScenarioToActiveScenarioProgressMap() <em>Active Scenario To Active Scenario Progress Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActiveScenarioToActiveScenarioProgressMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<ActiveScenario, ActiveScenarioProgress> activeScenarioToActiveScenarioProgressMap;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MultiActiveScenarioInitializationsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.MULTI_ACTIVE_SCENARIO_INITIALIZATIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<ActiveScenario, ActiveScenarioProgress> getActiveScenarioToActiveScenarioProgressMap() {
		if (activeScenarioToActiveScenarioProgressMap == null) {
			activeScenarioToActiveScenarioProgressMap = new EcoreEMap<ActiveScenario,ActiveScenarioProgress>(RuntimePackage.Literals.ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP_ENTRY, ActiveScenarioToActiveScenarioProgressMapEntryImpl.class, this, RuntimePackage.MULTI_ACTIVE_SCENARIO_INITIALIZATIONS__ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP);
		}
		return activeScenarioToActiveScenarioProgressMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.MULTI_ACTIVE_SCENARIO_INITIALIZATIONS__ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP:
				return ((InternalEList<?>)getActiveScenarioToActiveScenarioProgressMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.MULTI_ACTIVE_SCENARIO_INITIALIZATIONS__ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP:
				if (coreType) return getActiveScenarioToActiveScenarioProgressMap();
				else return getActiveScenarioToActiveScenarioProgressMap().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.MULTI_ACTIVE_SCENARIO_INITIALIZATIONS__ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP:
				((EStructuralFeature.Setting)getActiveScenarioToActiveScenarioProgressMap()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.MULTI_ACTIVE_SCENARIO_INITIALIZATIONS__ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP:
				getActiveScenarioToActiveScenarioProgressMap().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.MULTI_ACTIVE_SCENARIO_INITIALIZATIONS__ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP:
				return activeScenarioToActiveScenarioProgressMap != null && !activeScenarioToActiveScenarioProgressMap.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MultiActiveScenarioInitializationsImpl
