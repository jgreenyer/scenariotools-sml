/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation;

import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.ParameterValue;
import org.scenariotools.sml.runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Message Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.impl.MessageEventImpl#getSendingObject <em>Sending Object</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.MessageEventImpl#getReceivingObject <em>Receiving Object</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.MessageEventImpl#getMessageName <em>Message Name</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.MessageEventImpl#isConcrete <em>Concrete</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.MessageEventImpl#isParameterized <em>Parameterized</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.MessageEventImpl#getTypedElement <em>Typed Element</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.MessageEventImpl#getParameterValues <em>Parameter Values</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.MessageEventImpl#getCollectionOperation <em>Collection Operation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MessageEventImpl extends EventImpl implements MessageEvent {
	/**
	 * The cached value of the '{@link #getSendingObject() <em>Sending Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSendingObject()
	 * @generated
	 * @ordered
	 */
	protected EObject sendingObject;

	/**
	 * The cached value of the '{@link #getReceivingObject() <em>Receiving Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReceivingObject()
	 * @generated
	 * @ordered
	 */
	protected EObject receivingObject;

	/**
	 * The default value of the '{@link #getMessageName() <em>Message Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageName()
	 * @generated
	 * @ordered
	 */
	protected static final String MESSAGE_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #isConcrete() <em>Concrete</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConcrete()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CONCRETE_EDEFAULT = false;

	/**
	 * The default value of the '{@link #isParameterized() <em>Parameterized</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isParameterized()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PARAMETERIZED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #getTypedElement() <em>Typed Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypedElement()
	 * @generated
	 * @ordered
	 */
	protected ETypedElement typedElement;

	/**
	 * The cached value of the '{@link #getParameterValues() <em>Parameter Values</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterValues()
	 * @generated
	 * @ordered
	 */
	protected EList<ParameterValue> parameterValues;

	/**
	 * The default value of the '{@link #getCollectionOperation() <em>Collection Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollectionOperation()
	 * @generated
	 * @ordered
	 */
	protected static final CollectionOperation COLLECTION_OPERATION_EDEFAULT = CollectionOperation.IS_EMPTY;

	/**
	 * The cached value of the '{@link #getCollectionOperation() <em>Collection Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollectionOperation()
	 * @generated
	 * @ordered
	 */
	protected CollectionOperation collectionOperation = COLLECTION_OPERATION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MessageEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.MESSAGE_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getSendingObject() {
		if (sendingObject != null && sendingObject.eIsProxy()) {
			InternalEObject oldSendingObject = (InternalEObject)sendingObject;
			sendingObject = eResolveProxy(oldSendingObject);
			if (sendingObject != oldSendingObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.MESSAGE_EVENT__SENDING_OBJECT, oldSendingObject, sendingObject));
			}
		}
		return sendingObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetSendingObject() {
		return sendingObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSendingObject(EObject newSendingObject) {
		EObject oldSendingObject = sendingObject;
		sendingObject = newSendingObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.MESSAGE_EVENT__SENDING_OBJECT, oldSendingObject, sendingObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getReceivingObject() {
		if (receivingObject != null && receivingObject.eIsProxy()) {
			InternalEObject oldReceivingObject = (InternalEObject)receivingObject;
			receivingObject = eResolveProxy(oldReceivingObject);
			if (receivingObject != oldReceivingObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.MESSAGE_EVENT__RECEIVING_OBJECT, oldReceivingObject, receivingObject));
			}
		}
		return receivingObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetReceivingObject() {
		return receivingObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReceivingObject(EObject newReceivingObject) {
		EObject oldReceivingObject = receivingObject;
		receivingObject = newReceivingObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.MESSAGE_EVENT__RECEIVING_OBJECT, oldReceivingObject, receivingObject));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public String getMessageName() {
		try {
			String paramString = "(";
			if (isParameterized()) {
				boolean secondRun = false;
				for (ParameterValue parameterValue : getParameterValues()) {
					if(secondRun)paramString += ", ";
					Object parameterValueValue = parameterValue.getValue();
					if (parameterValueValue == null) {
						paramString += "null";
					} else if (parameterValueValue instanceof EObject) {
						paramString += getEObjectName((EObject)parameterValueValue);
					}else {
						paramString += parameterValueValue.toString();
					}
					secondRun = true;
				}
				paramString += ")";
			} else {
				paramString += ")";
			}
			ETypedElement te = getTypedElement();
			String result = getEObjectName(getSendingObject()) + "->"
					+ getEObjectName(getReceivingObject()) + "."
					+ te.getName() + paramString;
			if (te instanceof EStructuralFeature) {
				if(((EStructuralFeature) te).isMany())
					return result.replace(".", "."+getCollectionOperation().getName());
				return result.replace(".", ".set");
			} else
				return result;
		} catch (NullPointerException e) {
			return super.toString();
		}
	}

	/**
	 * <!-- begin-user-doc --> Returns if the message is concrete, i.e. it has
	 * no wildcard parameters. <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean isConcrete() {
		
		if (isParameterized()){
			for(ParameterValue parameterValue : getParameterValues()){
				if(parameterValue.isWildcardParameter()){
					return false;
				}
			}
			return true;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean isParameterized() {
		return !getParameterValues().isEmpty();	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ETypedElement getTypedElement() {
		if (typedElement != null && typedElement.eIsProxy()) {
			InternalEObject oldTypedElement = (InternalEObject)typedElement;
			typedElement = (ETypedElement)eResolveProxy(oldTypedElement);
			if (typedElement != oldTypedElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.MESSAGE_EVENT__TYPED_ELEMENT, oldTypedElement, typedElement));
			}
		}
		return typedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ETypedElement basicGetTypedElement() {
		return typedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypedElement(ETypedElement newTypedElement) {
		ETypedElement oldTypedElement = typedElement;
		typedElement = newTypedElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.MESSAGE_EVENT__TYPED_ELEMENT, oldTypedElement, typedElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterValue> getParameterValues() {
		if (parameterValues == null) {
			parameterValues = new EObjectContainmentEList<ParameterValue>(ParameterValue.class, this, RuntimePackage.MESSAGE_EVENT__PARAMETER_VALUES);
		}
		return parameterValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CollectionOperation getCollectionOperation() {
		return collectionOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCollectionOperation(CollectionOperation newCollectionOperation) {
		CollectionOperation oldCollectionOperation = collectionOperation;
		collectionOperation = newCollectionOperation == null ? COLLECTION_OPERATION_EDEFAULT : newCollectionOperation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.MESSAGE_EVENT__COLLECTION_OPERATION, oldCollectionOperation, collectionOperation));
	}

	/**
	 * A MessageEvent is equal to an other MessageEvent 
	 * if they are message unifiable and the ParameterValues are equal.
	 * Use this method for gathering different messages.
	 * 
	 * @generated NOT
	 */
	public boolean eq(MessageEvent messageEvent) {
		if(this.isMessageUnifiableWith(messageEvent)){
			for(int i = 0; i < getParameterValues().size(); i++){
				final ParameterValue parameterValue = this.getParameterValues().get(i);
				final ParameterValue otherParameterValue = messageEvent.getParameterValues().get(i);
				if(!parameterValue.eq(otherParameterValue)){
					return false;
				}
			}
			return true;
		}else{
			return false;
		}
	}

	/**
	 * A MessageEvent is unifiable with an other MessageEvent 
	 * if the SML ModelElements, sender and receiver are equal. 
	 * Use this method for comparison in play-out.
	 * 
	 * @generated NOT
	 */
	public boolean isMessageUnifiableWith(MessageEvent messageEvent) {
		return messageEvent.getTypedElement() == getTypedElement()
				&& messageEvent.getCollectionOperation() == getCollectionOperation()
				&& messageEvent.getSendingObject() == getSendingObject()
				&& messageEvent.getReceivingObject() == getReceivingObject();

	}

	/**
	 * A MessageEvent is parameter unifable with an other MessageEvent 
	 * if they are message unifiable and the ParameterValues are parameter unifiable.
	 * Use this method for comparison in play-out.
	 * 
	 * @generated NOT
	 */
	public boolean isParameterUnifiableWith(MessageEvent messageEvent) {
		if(isMessageUnifiableWith(messageEvent)){
			final EList<ParameterValue> parameters = getParameterValues();
			boolean isUnifiable = parameters.size() == messageEvent.getParameterValues().size();
			for(int i = 0; isUnifiable && i < getParameterValues().size(); i++) {
				final ParameterValue value = parameters.get(i);
				final ParameterValue otherValue = messageEvent.getParameterValues().get(i);
					
				isUnifiable = value.isParameterUnifiableWith(otherValue);
			}
			return isUnifiable;
		} else {
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.MESSAGE_EVENT__PARAMETER_VALUES:
				return ((InternalEList<?>)getParameterValues()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.MESSAGE_EVENT__SENDING_OBJECT:
				if (resolve) return getSendingObject();
				return basicGetSendingObject();
			case RuntimePackage.MESSAGE_EVENT__RECEIVING_OBJECT:
				if (resolve) return getReceivingObject();
				return basicGetReceivingObject();
			case RuntimePackage.MESSAGE_EVENT__MESSAGE_NAME:
				return getMessageName();
			case RuntimePackage.MESSAGE_EVENT__CONCRETE:
				return isConcrete();
			case RuntimePackage.MESSAGE_EVENT__PARAMETERIZED:
				return isParameterized();
			case RuntimePackage.MESSAGE_EVENT__TYPED_ELEMENT:
				if (resolve) return getTypedElement();
				return basicGetTypedElement();
			case RuntimePackage.MESSAGE_EVENT__PARAMETER_VALUES:
				return getParameterValues();
			case RuntimePackage.MESSAGE_EVENT__COLLECTION_OPERATION:
				return getCollectionOperation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.MESSAGE_EVENT__SENDING_OBJECT:
				setSendingObject((EObject)newValue);
				return;
			case RuntimePackage.MESSAGE_EVENT__RECEIVING_OBJECT:
				setReceivingObject((EObject)newValue);
				return;
			case RuntimePackage.MESSAGE_EVENT__TYPED_ELEMENT:
				setTypedElement((ETypedElement)newValue);
				return;
			case RuntimePackage.MESSAGE_EVENT__PARAMETER_VALUES:
				getParameterValues().clear();
				getParameterValues().addAll((Collection<? extends ParameterValue>)newValue);
				return;
			case RuntimePackage.MESSAGE_EVENT__COLLECTION_OPERATION:
				setCollectionOperation((CollectionOperation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.MESSAGE_EVENT__SENDING_OBJECT:
				setSendingObject((EObject)null);
				return;
			case RuntimePackage.MESSAGE_EVENT__RECEIVING_OBJECT:
				setReceivingObject((EObject)null);
				return;
			case RuntimePackage.MESSAGE_EVENT__TYPED_ELEMENT:
				setTypedElement((ETypedElement)null);
				return;
			case RuntimePackage.MESSAGE_EVENT__PARAMETER_VALUES:
				getParameterValues().clear();
				return;
			case RuntimePackage.MESSAGE_EVENT__COLLECTION_OPERATION:
				setCollectionOperation(COLLECTION_OPERATION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.MESSAGE_EVENT__SENDING_OBJECT:
				return sendingObject != null;
			case RuntimePackage.MESSAGE_EVENT__RECEIVING_OBJECT:
				return receivingObject != null;
			case RuntimePackage.MESSAGE_EVENT__MESSAGE_NAME:
				return MESSAGE_NAME_EDEFAULT == null ? getMessageName() != null : !MESSAGE_NAME_EDEFAULT.equals(getMessageName());
			case RuntimePackage.MESSAGE_EVENT__CONCRETE:
				return isConcrete() != CONCRETE_EDEFAULT;
			case RuntimePackage.MESSAGE_EVENT__PARAMETERIZED:
				return isParameterized() != PARAMETERIZED_EDEFAULT;
			case RuntimePackage.MESSAGE_EVENT__TYPED_ELEMENT:
				return typedElement != null;
			case RuntimePackage.MESSAGE_EVENT__PARAMETER_VALUES:
				return parameterValues != null && !parameterValues.isEmpty();
			case RuntimePackage.MESSAGE_EVENT__COLLECTION_OPERATION:
				return collectionOperation != COLLECTION_OPERATION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.MESSAGE_EVENT___EQ__MESSAGEEVENT:
				return eq((MessageEvent)arguments.get(0));
			case RuntimePackage.MESSAGE_EVENT___IS_MESSAGE_UNIFIABLE_WITH__MESSAGEEVENT:
				return isMessageUnifiableWith((MessageEvent)arguments.get(0));
			case RuntimePackage.MESSAGE_EVENT___IS_PARAMETER_UNIFIABLE_WITH__MESSAGEEVENT:
				return isParameterUnifiableWith((MessageEvent)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		return getMessageName();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected String getEObjectName(EObject eObject) {
		if (eObject == null)
			return "<null>";
		for (EAttribute eAttribute : eObject.eClass().getEAllAttributes()) {
			if ("name".equals(eAttribute.getName())) {
				if(eAttribute.getEType().getInstanceClass() == String.class)
					return (String) eObject.eGet(eAttribute);
				else
					return eObject.eContainingFeature().getName();
			}
		}
		return eObject.hashCode() + "";
	}
} //MessageEventImpl
