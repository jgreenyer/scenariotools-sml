/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.impl;

import java.util.Map;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.expressions.scenarioExpressions.Variable;
import org.scenariotools.sml.runtime.*;
import org.scenariotools.sml.runtime.keywrapper.ActiveInteractionKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.ActiveScenarioRoleBindingsKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.ObjectSystemKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.StateKeyWrapper;
import org.scenariotools.sml.runtime.logic.ParameterRanges.ParameterValues;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RuntimeFactoryImpl extends EFactoryImpl implements RuntimeFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static RuntimeFactory init() {
		try {
			RuntimeFactory theRuntimeFactory = (RuntimeFactory)EPackage.Registry.INSTANCE.getEFactory(RuntimePackage.eNS_URI);
			if (theRuntimeFactory != null) {
				return theRuntimeFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new RuntimeFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case RuntimePackage.ANNOTATABLE_ELEMENT: return createAnnotatableElement();
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH: return createSMLRuntimeStateGraph();
			case RuntimePackage.ELEMENT_CONTAINER: return createElementContainer();
			case RuntimePackage.SML_RUNTIME_STATE: return createSMLRuntimeState();
			case RuntimePackage.SML_OBJECT_SYSTEM: return createSMLObjectSystem();
			case RuntimePackage.DYNAMIC_OBJECT_CONTAINER: return createDynamicObjectContainer();
			case RuntimePackage.TRANSITION: return createTransition();
			case RuntimePackage.WAIT_EVENT: return createWaitEvent();
			case RuntimePackage.MESSAGE_EVENT: return createMessageEvent();
			case RuntimePackage.BOOLEAN_PARAMETER_VALUE: return createBooleanParameterValue();
			case RuntimePackage.INTEGER_PARAMETER_VALUE: return createIntegerParameterValue();
			case RuntimePackage.STRING_PARAMETER_VALUE: return createStringParameterValue();
			case RuntimePackage.EOBJECT_PARAMETER_VALUE: return createEObjectParameterValue();
			case RuntimePackage.EENUM_PARAMETER_VALUE: return createEEnumParameterValue();
			case RuntimePackage.ACTIVE_SCENARIO: return createActiveScenario();
			case RuntimePackage.ACTIVE_SCENARIO_ROLE_BINDINGS: return createActiveScenarioRoleBindings();
			case RuntimePackage.ACTIVE_PART: return createActivePart();
			case RuntimePackage.ACTIVE_ALTERNATIVE: return createActiveAlternative();
			case RuntimePackage.ACTIVE_CASE: return createActiveCase();
			case RuntimePackage.ACTIVE_INTERACTION: return createActiveInteraction();
			case RuntimePackage.ACTIVE_INTERRUPT_CONDITION: return createActiveInterruptCondition();
			case RuntimePackage.ACTIVE_LOOP: return createActiveLoop();
			case RuntimePackage.ACTIVE_MODAL_MESSAGE: return createActiveModalMessage();
			case RuntimePackage.ACTIVE_PARALLEL: return createActiveParallel();
			case RuntimePackage.ACTIVE_VARIABLE_FRAGMENT: return createActiveVariableFragment();
			case RuntimePackage.ACTIVE_VIOLATION_CONDITION: return createActiveViolationCondition();
			case RuntimePackage.ACTIVE_WAIT_CONDITION: return createActiveWaitCondition();
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION: return createActiveMessageParameterWithValueExpression();
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR: return createActiveMessageParameterWithBindToVar();
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD: return createActiveMessageParameterWithWildcard();
			case RuntimePackage.ACTIVE_CONSTRAINT_CONSIDER: return createActiveConstraintConsider();
			case RuntimePackage.ACTIVE_CONSTRAINT_IGNORE: return createActiveConstraintIgnore();
			case RuntimePackage.ACTIVE_CONSTRAINT_INTERRUPT: return createActiveConstraintInterrupt();
			case RuntimePackage.ACTIVE_CONSTRAINT_FORBIDDEN: return createActiveConstraintForbidden();
			case RuntimePackage.PARAMETER_RANGES_PROVIDER: return createParameterRangesProvider();
			case RuntimePackage.MESSAGE_EVENT_BLOCKED_INFORMATION: return createMessageEventBlockedInformation();
			case RuntimePackage.MULTI_ACTIVE_SCENARIO_INITIALIZATIONS: return createMultiActiveScenarioInitializations();
			case RuntimePackage.ECLASS_TO_EOBJECT_MAP_ENTRY: return (EObject)createEClassToEObjectMapEntry();
			case RuntimePackage.ROLE_TO_EOBJECT_MAP_ENTRY: return (EObject)createRoleToEObjectMapEntry();
			case RuntimePackage.STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY: return (EObject)createStaticEObjectToDynamicEObjectMapEntry();
			case RuntimePackage.ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP_ENTRY: return (EObject)createActiveScenarioToActiveScenarioProgressMapEntry();
			case RuntimePackage.EVENT_TO_TRANSITION_MAP_ENTRY: return (EObject)createEventToTransitionMapEntry();
			case RuntimePackage.STRING_TO_BOOLEAN_MAP_ENTRY: return (EObject)createStringToBooleanMapEntry();
			case RuntimePackage.STRING_TO_STRING_MAP_ENTRY: return (EObject)createStringToStringMapEntry();
			case RuntimePackage.STRING_TO_EOBJECT_MAP_ENTRY: return (EObject)createStringToEObjectMapEntry();
			case RuntimePackage.ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY: return (EObject)createActiveInteractionKeyWrapperToActiveInteractionMapEntry();
			case RuntimePackage.ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY: return (EObject)createActiveScenarioKeyWrapperToActiveScenarioMapEntry();
			case RuntimePackage.OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY: return (EObject)createObjectSystemKeyWrapperToObjectSystemMapEntry();
			case RuntimePackage.OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY: return (EObject)createObjectSystemKeyWrapperToDynamicObjectContainerMapEntry();
			case RuntimePackage.STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY: return (EObject)createStateKeyWrapperToStateMapEntry();
			case RuntimePackage.VARIABLE_TO_OBJECT_MAP_ENTRY: return (EObject)createVariableToObjectMapEntry();
			case RuntimePackage.VARIABLE_TO_EOBJECT_MAP_ENTRY: return (EObject)createVariableToEObjectMapEntry();
			case RuntimePackage.ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY: return (EObject)createActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case RuntimePackage.VIOLATION_KIND:
				return createViolationKindFromString(eDataType, initialValue);
			case RuntimePackage.ACTIVE_SCENARIO_PROGRESS:
				return createActiveScenarioProgressFromString(eDataType, initialValue);
			case RuntimePackage.BLOCKED_TYPE:
				return createBlockedTypeFromString(eDataType, initialValue);
			case RuntimePackage.PARAMETER_VALUES:
				return createParameterValuesFromString(eDataType, initialValue);
			case RuntimePackage.ACTIVE_INTERACTION_KEY_WRAPPER:
				return createActiveInteractionKeyWrapperFromString(eDataType, initialValue);
			case RuntimePackage.ACTIVE_SCENARIO_KEY_WRAPPER:
				return createActiveScenarioKeyWrapperFromString(eDataType, initialValue);
			case RuntimePackage.OBJECT_SYSTEM_KEY_WRAPPER:
				return createObjectSystemKeyWrapperFromString(eDataType, initialValue);
			case RuntimePackage.STATE_KEY_WRAPPER:
				return createStateKeyWrapperFromString(eDataType, initialValue);
			case RuntimePackage.ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER:
				return createActiveScenarioRoleBindingsKeyWrapperFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case RuntimePackage.VIOLATION_KIND:
				return convertViolationKindToString(eDataType, instanceValue);
			case RuntimePackage.ACTIVE_SCENARIO_PROGRESS:
				return convertActiveScenarioProgressToString(eDataType, instanceValue);
			case RuntimePackage.BLOCKED_TYPE:
				return convertBlockedTypeToString(eDataType, instanceValue);
			case RuntimePackage.PARAMETER_VALUES:
				return convertParameterValuesToString(eDataType, instanceValue);
			case RuntimePackage.ACTIVE_INTERACTION_KEY_WRAPPER:
				return convertActiveInteractionKeyWrapperToString(eDataType, instanceValue);
			case RuntimePackage.ACTIVE_SCENARIO_KEY_WRAPPER:
				return convertActiveScenarioKeyWrapperToString(eDataType, instanceValue);
			case RuntimePackage.OBJECT_SYSTEM_KEY_WRAPPER:
				return convertObjectSystemKeyWrapperToString(eDataType, instanceValue);
			case RuntimePackage.STATE_KEY_WRAPPER:
				return convertStateKeyWrapperToString(eDataType, instanceValue);
			case RuntimePackage.ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER:
				return convertActiveScenarioRoleBindingsKeyWrapperToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SMLRuntimeStateGraph createSMLRuntimeStateGraph() {
		SMLRuntimeStateGraphImpl smlRuntimeStateGraph = new SMLRuntimeStateGraphImpl();
		return smlRuntimeStateGraph;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SMLRuntimeState createSMLRuntimeState() {
		SMLRuntimeStateImpl smlRuntimeState = new SMLRuntimeStateImpl();
		return smlRuntimeState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveScenario createActiveScenario() {
		ActiveScenarioImpl activeScenario = new ActiveScenarioImpl();
		return activeScenario;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementContainer createElementContainer() {
		ElementContainerImpl elementContainer = new ElementContainerImpl();
		return elementContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SMLObjectSystem createSMLObjectSystem() {
		SMLObjectSystemImpl smlObjectSystem = new SMLObjectSystemImpl();
		return smlObjectSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivePart createActivePart() {
		ActivePartImpl activePart = new ActivePartImpl();
		return activePart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveAlternative createActiveAlternative() {
		ActiveAlternativeImpl activeAlternative = new ActiveAlternativeImpl();
		return activeAlternative;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveCase createActiveCase() {
		ActiveCaseImpl activeCase = new ActiveCaseImpl();
		return activeCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveInteraction createActiveInteraction() {
		ActiveInteractionImpl activeInteraction = new ActiveInteractionImpl();
		return activeInteraction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveInterruptCondition createActiveInterruptCondition() {
		ActiveInterruptConditionImpl activeInterruptCondition = new ActiveInterruptConditionImpl();
		return activeInterruptCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveLoop createActiveLoop() {
		ActiveLoopImpl activeLoop = new ActiveLoopImpl();
		return activeLoop;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveModalMessage createActiveModalMessage() {
		ActiveModalMessageImpl activeModalMessage = new ActiveModalMessageImpl();
		return activeModalMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveParallel createActiveParallel() {
		ActiveParallelImpl activeParallel = new ActiveParallelImpl();
		return activeParallel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveVariableFragment createActiveVariableFragment() {
		ActiveVariableFragmentImpl activeVariableFragment = new ActiveVariableFragmentImpl();
		return activeVariableFragment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveViolationCondition createActiveViolationCondition() {
		ActiveViolationConditionImpl activeViolationCondition = new ActiveViolationConditionImpl();
		return activeViolationCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveWaitCondition createActiveWaitCondition() {
		ActiveWaitConditionImpl activeWaitCondition = new ActiveWaitConditionImpl();
		return activeWaitCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<EClass, EObject> createEClassToEObjectMapEntry() {
		EClassToEObjectMapEntryImpl eClassToEObjectMapEntry = new EClassToEObjectMapEntryImpl();
		return eClassToEObjectMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Role, EObject> createRoleToEObjectMapEntry() {
		RoleToEObjectMapEntryImpl roleToEObjectMapEntry = new RoleToEObjectMapEntryImpl();
		return roleToEObjectMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<ActiveInteractionKeyWrapper, ActivePart> createActiveInteractionKeyWrapperToActiveInteractionMapEntry() {
		ActiveInteractionKeyWrapperToActiveInteractionMapEntryImpl activeInteractionKeyWrapperToActiveInteractionMapEntry = new ActiveInteractionKeyWrapperToActiveInteractionMapEntryImpl();
		return activeInteractionKeyWrapperToActiveInteractionMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<ActiveScenarioKeyWrapper, ActiveScenario> createActiveScenarioKeyWrapperToActiveScenarioMapEntry() {
		ActiveScenarioKeyWrapperToActiveScenarioMapEntryImpl activeScenarioKeyWrapperToActiveScenarioMapEntry = new ActiveScenarioKeyWrapperToActiveScenarioMapEntryImpl();
		return activeScenarioKeyWrapperToActiveScenarioMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<ObjectSystemKeyWrapper, SMLObjectSystem> createObjectSystemKeyWrapperToObjectSystemMapEntry() {
		ObjectSystemKeyWrapperToObjectSystemMapEntryImpl objectSystemKeyWrapperToObjectSystemMapEntry = new ObjectSystemKeyWrapperToObjectSystemMapEntryImpl();
		return objectSystemKeyWrapperToObjectSystemMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<ObjectSystemKeyWrapper, DynamicObjectContainer> createObjectSystemKeyWrapperToDynamicObjectContainerMapEntry() {
		ObjectSystemKeyWrapperToDynamicObjectContainerMapEntryImpl objectSystemKeyWrapperToDynamicObjectContainerMapEntry = new ObjectSystemKeyWrapperToDynamicObjectContainerMapEntryImpl();
		return objectSystemKeyWrapperToDynamicObjectContainerMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<StateKeyWrapper, SMLRuntimeState> createStateKeyWrapperToStateMapEntry() {
		StateKeyWrapperToStateMapEntryImpl stateKeyWrapperToStateMapEntry = new StateKeyWrapperToStateMapEntryImpl();
		return stateKeyWrapperToStateMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Variable, Object> createVariableToObjectMapEntry() {
		VariableToObjectMapEntryImpl variableToObjectMapEntry = new VariableToObjectMapEntryImpl();
		return variableToObjectMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Variable, EObject> createVariableToEObjectMapEntry() {
		VariableToEObjectMapEntryImpl variableToEObjectMapEntry = new VariableToEObjectMapEntryImpl();
		return variableToEObjectMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<ActiveScenarioRoleBindingsKeyWrapper, ActiveScenarioRoleBindings> createActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry() {
		ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryImpl activeScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry = new ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryImpl();
		return activeScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DynamicObjectContainer createDynamicObjectContainer() {
		DynamicObjectContainerImpl dynamicObjectContainer = new DynamicObjectContainerImpl();
		return dynamicObjectContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<EObject, EObject> createStaticEObjectToDynamicEObjectMapEntry() {
		StaticEObjectToDynamicEObjectMapEntryImpl staticEObjectToDynamicEObjectMapEntry = new StaticEObjectToDynamicEObjectMapEntryImpl();
		return staticEObjectToDynamicEObjectMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMessageParameterWithValueExpression createActiveMessageParameterWithValueExpression() {
		ActiveMessageParameterWithValueExpressionImpl activeMessageParameterWithValueExpression = new ActiveMessageParameterWithValueExpressionImpl();
		return activeMessageParameterWithValueExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMessageParameterWithBindToVar createActiveMessageParameterWithBindToVar() {
		ActiveMessageParameterWithBindToVarImpl activeMessageParameterWithBindToVar = new ActiveMessageParameterWithBindToVarImpl();
		return activeMessageParameterWithBindToVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveMessageParameterWithWildcard createActiveMessageParameterWithWildcard() {
		ActiveMessageParameterWithWildcardImpl activeMessageParameterWithWildcard = new ActiveMessageParameterWithWildcardImpl();
		return activeMessageParameterWithWildcard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveConstraintConsider createActiveConstraintConsider() {
		ActiveConstraintConsiderImpl activeConstraintConsider = new ActiveConstraintConsiderImpl();
		return activeConstraintConsider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveConstraintIgnore createActiveConstraintIgnore() {
		ActiveConstraintIgnoreImpl activeConstraintIgnore = new ActiveConstraintIgnoreImpl();
		return activeConstraintIgnore;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveConstraintInterrupt createActiveConstraintInterrupt() {
		ActiveConstraintInterruptImpl activeConstraintInterrupt = new ActiveConstraintInterruptImpl();
		return activeConstraintInterrupt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveConstraintForbidden createActiveConstraintForbidden() {
		ActiveConstraintForbiddenImpl activeConstraintForbidden = new ActiveConstraintForbiddenImpl();
		return activeConstraintForbidden;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterRangesProvider createParameterRangesProvider() {
		ParameterRangesProviderImpl parameterRangesProvider = new ParameterRangesProviderImpl();
		return parameterRangesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageEventBlockedInformation createMessageEventBlockedInformation() {
		MessageEventBlockedInformationImpl messageEventBlockedInformation = new MessageEventBlockedInformationImpl();
		return messageEventBlockedInformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiActiveScenarioInitializations createMultiActiveScenarioInitializations() {
		MultiActiveScenarioInitializationsImpl multiActiveScenarioInitializations = new MultiActiveScenarioInitializationsImpl();
		return multiActiveScenarioInitializations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<ActiveScenario, ActiveScenarioProgress> createActiveScenarioToActiveScenarioProgressMapEntry() {
		ActiveScenarioToActiveScenarioProgressMapEntryImpl activeScenarioToActiveScenarioProgressMapEntry = new ActiveScenarioToActiveScenarioProgressMapEntryImpl();
		return activeScenarioToActiveScenarioProgressMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Event, Transition> createEventToTransitionMapEntry() {
		EventToTransitionMapEntryImpl eventToTransitionMapEntry = new EventToTransitionMapEntryImpl();
		return eventToTransitionMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition createTransition() {
		TransitionImpl transition = new TransitionImpl();
		return transition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotatableElement createAnnotatableElement() {
		AnnotatableElementImpl annotatableElement = new AnnotatableElementImpl();
		return annotatableElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<String, Boolean> createStringToBooleanMapEntry() {
		StringToBooleanMapEntryImpl stringToBooleanMapEntry = new StringToBooleanMapEntryImpl();
		return stringToBooleanMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<String, String> createStringToStringMapEntry() {
		StringToStringMapEntryImpl stringToStringMapEntry = new StringToStringMapEntryImpl();
		return stringToStringMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<String, EObject> createStringToEObjectMapEntry() {
		StringToEObjectMapEntryImpl stringToEObjectMapEntry = new StringToEObjectMapEntryImpl();
		return stringToEObjectMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageEvent createMessageEvent() {
		MessageEventImpl messageEvent = new MessageEventImpl();
		return messageEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanParameterValue createBooleanParameterValue() {
		BooleanParameterValueImpl booleanParameterValue = new BooleanParameterValueImpl();
		return booleanParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerParameterValue createIntegerParameterValue() {
		IntegerParameterValueImpl integerParameterValue = new IntegerParameterValueImpl();
		return integerParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringParameterValue createStringParameterValue() {
		StringParameterValueImpl stringParameterValue = new StringParameterValueImpl();
		return stringParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObjectParameterValue createEObjectParameterValue() {
		EObjectParameterValueImpl eObjectParameterValue = new EObjectParameterValueImpl();
		return eObjectParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnumParameterValue createEEnumParameterValue() {
		EEnumParameterValueImpl eEnumParameterValue = new EEnumParameterValueImpl();
		return eEnumParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WaitEvent createWaitEvent() {
		WaitEventImpl waitEvent = new WaitEventImpl();
		return waitEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveScenarioRoleBindings createActiveScenarioRoleBindings() {
		ActiveScenarioRoleBindingsImpl activeScenarioRoleBindings = new ActiveScenarioRoleBindingsImpl();
		return activeScenarioRoleBindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ViolationKind createViolationKindFromString(EDataType eDataType, String initialValue) {
		ViolationKind result = ViolationKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertViolationKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveScenarioProgress createActiveScenarioProgressFromString(EDataType eDataType, String initialValue) {
		ActiveScenarioProgress result = ActiveScenarioProgress.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertActiveScenarioProgressToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlockedType createBlockedTypeFromString(EDataType eDataType, String initialValue) {
		BlockedType result = BlockedType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBlockedTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveInteractionKeyWrapper createActiveInteractionKeyWrapperFromString(EDataType eDataType, String initialValue) {
		return (ActiveInteractionKeyWrapper)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertActiveInteractionKeyWrapperToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveScenarioKeyWrapper createActiveScenarioKeyWrapperFromString(EDataType eDataType, String initialValue) {
		return (ActiveScenarioKeyWrapper)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertActiveScenarioKeyWrapperToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectSystemKeyWrapper createObjectSystemKeyWrapperFromString(EDataType eDataType, String initialValue) {
		return (ObjectSystemKeyWrapper)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertObjectSystemKeyWrapperToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateKeyWrapper createStateKeyWrapperFromString(EDataType eDataType, String initialValue) {
		return (StateKeyWrapper)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStateKeyWrapperToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveScenarioRoleBindingsKeyWrapper createActiveScenarioRoleBindingsKeyWrapperFromString(EDataType eDataType, String initialValue) {
		return (ActiveScenarioRoleBindingsKeyWrapper)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertActiveScenarioRoleBindingsKeyWrapperToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterValues<?> createParameterValuesFromString(EDataType eDataType, String initialValue) {
		return (ParameterValues<?>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertParameterValuesToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimePackage getRuntimePackage() {
		return (RuntimePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static RuntimePackage getPackage() {
		return RuntimePackage.eINSTANCE;
	}

} //RuntimeFactoryImpl
