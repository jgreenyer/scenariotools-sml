/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.DynamicObjectContainer;
import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.runtime.MessageEventBlockedInformation;
import org.scenariotools.sml.runtime.RuntimePackage;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.Transition;
import org.scenariotools.sml.runtime.logic.SMLRuntimeStateLogic;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SML Runtime State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.impl.SMLRuntimeStateImpl#getActiveScenarios <em>Active Scenarios</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.SMLRuntimeStateImpl#getDynamicObjectContainer <em>Dynamic Object Container</em>}</li>
 * </ul>
 * </p>
 *
 * @generated PROTECT_DECLARATION 
 * (generated NOT would protect all the content of the class)
 */
public class SMLRuntimeStateImpl extends SMLRuntimeStateLogic {
	/**
	 * The cached value of the '{@link #getActiveScenarios() <em>Active Scenarios</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActiveScenarios()
	 * @generated
	 * @ordered
	 */
	protected EList<ActiveScenario> activeScenarios;

	/**
	 * The cached value of the '{@link #getDynamicObjectContainer() <em>Dynamic Object Container</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDynamicObjectContainer()
	 * @generated
	 * @ordered
	 */
	protected DynamicObjectContainer dynamicObjectContainer;

	/**
	 * The cached value of the '{@link #getComputedInitializingMessageEvents() <em>Computed Initializing Message Events</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComputedInitializingMessageEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageEvent> computedInitializingMessageEvents;

	/**
	 * The cached value of the '{@link #getTerminatedExistentialScenariosFromLastPerformStep() <em>Terminated Existential Scenarios From Last Perform Step</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTerminatedExistentialScenariosFromLastPerformStep()
	 * @generated
	 * @ordered
	 */
	protected EList<Scenario> terminatedExistentialScenariosFromLastPerformStep;

	/**
	 * The cached value of the '{@link #getMessageEventBlockedInformation() <em>Message Event Blocked Information</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageEventBlockedInformation()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageEventBlockedInformation> messageEventBlockedInformation;

	/**
	 * The cached value of the '{@link #getObjectSystem() <em>Object System</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectSystem()
	 * @generated
	 * @ordered
	 */
	protected SMLObjectSystem objectSystem;

	/**
	 * The default value of the '{@link #isSafetyViolationOccurredInGuarantees() <em>Safety Violation Occurred In Guarantees</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSafetyViolationOccurredInGuarantees()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SAFETY_VIOLATION_OCCURRED_IN_GUARANTEES_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSafetyViolationOccurredInGuarantees() <em>Safety Violation Occurred In Guarantees</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSafetyViolationOccurredInGuarantees()
	 * @generated
	 * @ordered
	 */
	protected boolean safetyViolationOccurredInGuarantees = SAFETY_VIOLATION_OCCURRED_IN_GUARANTEES_EDEFAULT;

	/**
	 * The default value of the '{@link #isSafetyViolationOccurredInAssumptions() <em>Safety Violation Occurred In Assumptions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSafetyViolationOccurredInAssumptions()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSafetyViolationOccurredInAssumptions() <em>Safety Violation Occurred In Assumptions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSafetyViolationOccurredInAssumptions()
	 * @generated
	 * @ordered
	 */
	protected boolean safetyViolationOccurredInAssumptions = SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEventToTransitionMap() <em>Event To Transition Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventToTransitionMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Event, Transition> eventToTransitionMap;

	/**
	 * The cached value of the '{@link #getEnabledEvents() <em>Enabled Events</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnabledEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> enabledEvents;

	/**
	 * The cached value of the '{@link #getOutgoingTransition() <em>Outgoing Transition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutgoingTransition()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> outgoingTransition;

	/**
	 * The cached value of the '{@link #getIncomingTransition() <em>Incoming Transition</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncomingTransition()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> incomingTransition;

	/**
	 * The default value of the '{@link #isSystemChoseToWait() <em>System Chose To Wait</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSystemChoseToWait()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SYSTEM_CHOSE_TO_WAIT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSystemChoseToWait() <em>System Chose To Wait</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSystemChoseToWait()
	 * @generated
	 * @ordered
	 */
	protected boolean systemChoseToWait = SYSTEM_CHOSE_TO_WAIT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SMLRuntimeStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.SML_RUNTIME_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActiveScenario> getActiveScenarios() {
		if (activeScenarios == null) {
			activeScenarios = new EObjectResolvingEList<ActiveScenario>(ActiveScenario.class, this, RuntimePackage.SML_RUNTIME_STATE__ACTIVE_SCENARIOS);
		}
		return activeScenarios;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DynamicObjectContainer getDynamicObjectContainer() {
		if (dynamicObjectContainer != null && dynamicObjectContainer.eIsProxy()) {
			InternalEObject oldDynamicObjectContainer = (InternalEObject)dynamicObjectContainer;
			dynamicObjectContainer = (DynamicObjectContainer)eResolveProxy(oldDynamicObjectContainer);
			if (dynamicObjectContainer != oldDynamicObjectContainer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.SML_RUNTIME_STATE__DYNAMIC_OBJECT_CONTAINER, oldDynamicObjectContainer, dynamicObjectContainer));
			}
		}
		return dynamicObjectContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DynamicObjectContainer basicGetDynamicObjectContainer() {
		return dynamicObjectContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDynamicObjectContainer(DynamicObjectContainer newDynamicObjectContainer) {
		DynamicObjectContainer oldDynamicObjectContainer = dynamicObjectContainer;
		dynamicObjectContainer = newDynamicObjectContainer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.SML_RUNTIME_STATE__DYNAMIC_OBJECT_CONTAINER, oldDynamicObjectContainer, dynamicObjectContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageEvent> getComputedInitializingMessageEvents() {
		if (computedInitializingMessageEvents == null) {
			computedInitializingMessageEvents = new EObjectContainmentEList<MessageEvent>(MessageEvent.class, this, RuntimePackage.SML_RUNTIME_STATE__COMPUTED_INITIALIZING_MESSAGE_EVENTS);
		}
		return computedInitializingMessageEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Scenario> getTerminatedExistentialScenariosFromLastPerformStep() {
		if (terminatedExistentialScenariosFromLastPerformStep == null) {
			terminatedExistentialScenariosFromLastPerformStep = new EObjectResolvingEList<Scenario>(Scenario.class, this, RuntimePackage.SML_RUNTIME_STATE__TERMINATED_EXISTENTIAL_SCENARIOS_FROM_LAST_PERFORM_STEP);
		}
		return terminatedExistentialScenariosFromLastPerformStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageEventBlockedInformation> getMessageEventBlockedInformation() {
		if (messageEventBlockedInformation == null) {
			messageEventBlockedInformation = new EObjectContainmentEList<MessageEventBlockedInformation>(MessageEventBlockedInformation.class, this, RuntimePackage.SML_RUNTIME_STATE__MESSAGE_EVENT_BLOCKED_INFORMATION);
		}
		return messageEventBlockedInformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SMLObjectSystem getObjectSystem() {
		if (objectSystem != null && objectSystem.eIsProxy()) {
			InternalEObject oldObjectSystem = (InternalEObject)objectSystem;
			objectSystem = (SMLObjectSystem)eResolveProxy(oldObjectSystem);
			if (objectSystem != oldObjectSystem) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.SML_RUNTIME_STATE__OBJECT_SYSTEM, oldObjectSystem, objectSystem));
			}
		}
		return objectSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SMLObjectSystem basicGetObjectSystem() {
		return objectSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectSystem(SMLObjectSystem newObjectSystem) {
		SMLObjectSystem oldObjectSystem = objectSystem;
		objectSystem = newObjectSystem;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.SML_RUNTIME_STATE__OBJECT_SYSTEM, oldObjectSystem, objectSystem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSafetyViolationOccurredInGuarantees() {
		return safetyViolationOccurredInGuarantees;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSafetyViolationOccurredInGuarantees(boolean newSafetyViolationOccurredInGuarantees) {
		boolean oldSafetyViolationOccurredInGuarantees = safetyViolationOccurredInGuarantees;
		safetyViolationOccurredInGuarantees = newSafetyViolationOccurredInGuarantees;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.SML_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_GUARANTEES, oldSafetyViolationOccurredInGuarantees, safetyViolationOccurredInGuarantees));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSafetyViolationOccurredInAssumptions() {
		return safetyViolationOccurredInAssumptions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSafetyViolationOccurredInAssumptions(boolean newSafetyViolationOccurredInAssumptions) {
		boolean oldSafetyViolationOccurredInAssumptions = safetyViolationOccurredInAssumptions;
		safetyViolationOccurredInAssumptions = newSafetyViolationOccurredInAssumptions;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.SML_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS, oldSafetyViolationOccurredInAssumptions, safetyViolationOccurredInAssumptions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Event, Transition> getEventToTransitionMap() {
		if (eventToTransitionMap == null) {
			eventToTransitionMap = new EcoreEMap<Event,Transition>(RuntimePackage.Literals.EVENT_TO_TRANSITION_MAP_ENTRY, EventToTransitionMapEntryImpl.class, this, RuntimePackage.SML_RUNTIME_STATE__EVENT_TO_TRANSITION_MAP);
		}
		return eventToTransitionMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getEnabledEvents() {
		if (enabledEvents == null) {
			enabledEvents = new EObjectResolvingEList<Event>(Event.class, this, RuntimePackage.SML_RUNTIME_STATE__ENABLED_EVENTS);
		}
		return enabledEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getOutgoingTransition() {
		if (outgoingTransition == null) {
			outgoingTransition = new EObjectContainmentWithInverseEList<Transition>(Transition.class, this, RuntimePackage.SML_RUNTIME_STATE__OUTGOING_TRANSITION, RuntimePackage.TRANSITION__SOURCE_STATE);
		}
		return outgoingTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getIncomingTransition() {
		if (incomingTransition == null) {
			incomingTransition = new EObjectWithInverseResolvingEList<Transition>(Transition.class, this, RuntimePackage.SML_RUNTIME_STATE__INCOMING_TRANSITION, RuntimePackage.TRANSITION__TARGET_STATE);
		}
		return incomingTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SMLRuntimeStateGraph getStateGraph() {
		if (eContainerFeatureID() != RuntimePackage.SML_RUNTIME_STATE__STATE_GRAPH) return null;
		return (SMLRuntimeStateGraph)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStateGraph(SMLRuntimeStateGraph newStateGraph, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newStateGraph, RuntimePackage.SML_RUNTIME_STATE__STATE_GRAPH, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStateGraph(SMLRuntimeStateGraph newStateGraph) {
		if (newStateGraph != eInternalContainer() || (eContainerFeatureID() != RuntimePackage.SML_RUNTIME_STATE__STATE_GRAPH && newStateGraph != null)) {
			if (EcoreUtil.isAncestor(this, newStateGraph))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newStateGraph != null)
				msgs = ((InternalEObject)newStateGraph).eInverseAdd(this, RuntimePackage.SML_RUNTIME_STATE_GRAPH__STATES, SMLRuntimeStateGraph.class, msgs);
			msgs = basicSetStateGraph(newStateGraph, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.SML_RUNTIME_STATE__STATE_GRAPH, newStateGraph, newStateGraph));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSystemChoseToWait() {
		return systemChoseToWait;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSystemChoseToWait(boolean newSystemChoseToWait) {
		boolean oldSystemChoseToWait = systemChoseToWait;
		systemChoseToWait = newSystemChoseToWait;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.SML_RUNTIME_STATE__SYSTEM_CHOSE_TO_WAIT, oldSystemChoseToWait, systemChoseToWait));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void init(SMLObjectSystem objectSystem) {
		super.init(objectSystem);
	}

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void updateEnabledMessageEvents() {
		super.updateEnabledMessageEvents();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void performStep(Event event) {
		super.performStep(event);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.SML_RUNTIME_STATE__COMPUTED_INITIALIZING_MESSAGE_EVENTS:
				return ((InternalEList<?>)getComputedInitializingMessageEvents()).basicRemove(otherEnd, msgs);
			case RuntimePackage.SML_RUNTIME_STATE__MESSAGE_EVENT_BLOCKED_INFORMATION:
				return ((InternalEList<?>)getMessageEventBlockedInformation()).basicRemove(otherEnd, msgs);
			case RuntimePackage.SML_RUNTIME_STATE__EVENT_TO_TRANSITION_MAP:
				return ((InternalEList<?>)getEventToTransitionMap()).basicRemove(otherEnd, msgs);
			case RuntimePackage.SML_RUNTIME_STATE__OUTGOING_TRANSITION:
				return ((InternalEList<?>)getOutgoingTransition()).basicRemove(otherEnd, msgs);
			case RuntimePackage.SML_RUNTIME_STATE__INCOMING_TRANSITION:
				return ((InternalEList<?>)getIncomingTransition()).basicRemove(otherEnd, msgs);
			case RuntimePackage.SML_RUNTIME_STATE__STATE_GRAPH:
				return basicSetStateGraph(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case RuntimePackage.SML_RUNTIME_STATE__STATE_GRAPH:
				return eInternalContainer().eInverseRemove(this, RuntimePackage.SML_RUNTIME_STATE_GRAPH__STATES, SMLRuntimeStateGraph.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.SML_RUNTIME_STATE__OUTGOING_TRANSITION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOutgoingTransition()).basicAdd(otherEnd, msgs);
			case RuntimePackage.SML_RUNTIME_STATE__INCOMING_TRANSITION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getIncomingTransition()).basicAdd(otherEnd, msgs);
			case RuntimePackage.SML_RUNTIME_STATE__STATE_GRAPH:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetStateGraph((SMLRuntimeStateGraph)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.SML_RUNTIME_STATE__ACTIVE_SCENARIOS:
				return getActiveScenarios();
			case RuntimePackage.SML_RUNTIME_STATE__DYNAMIC_OBJECT_CONTAINER:
				if (resolve) return getDynamicObjectContainer();
				return basicGetDynamicObjectContainer();
			case RuntimePackage.SML_RUNTIME_STATE__COMPUTED_INITIALIZING_MESSAGE_EVENTS:
				return getComputedInitializingMessageEvents();
			case RuntimePackage.SML_RUNTIME_STATE__TERMINATED_EXISTENTIAL_SCENARIOS_FROM_LAST_PERFORM_STEP:
				return getTerminatedExistentialScenariosFromLastPerformStep();
			case RuntimePackage.SML_RUNTIME_STATE__MESSAGE_EVENT_BLOCKED_INFORMATION:
				return getMessageEventBlockedInformation();
			case RuntimePackage.SML_RUNTIME_STATE__OBJECT_SYSTEM:
				if (resolve) return getObjectSystem();
				return basicGetObjectSystem();
			case RuntimePackage.SML_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_GUARANTEES:
				return isSafetyViolationOccurredInGuarantees();
			case RuntimePackage.SML_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS:
				return isSafetyViolationOccurredInAssumptions();
			case RuntimePackage.SML_RUNTIME_STATE__EVENT_TO_TRANSITION_MAP:
				if (coreType) return getEventToTransitionMap();
				else return getEventToTransitionMap().map();
			case RuntimePackage.SML_RUNTIME_STATE__ENABLED_EVENTS:
				return getEnabledEvents();
			case RuntimePackage.SML_RUNTIME_STATE__OUTGOING_TRANSITION:
				return getOutgoingTransition();
			case RuntimePackage.SML_RUNTIME_STATE__INCOMING_TRANSITION:
				return getIncomingTransition();
			case RuntimePackage.SML_RUNTIME_STATE__STATE_GRAPH:
				return getStateGraph();
			case RuntimePackage.SML_RUNTIME_STATE__SYSTEM_CHOSE_TO_WAIT:
				return isSystemChoseToWait();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.SML_RUNTIME_STATE__ACTIVE_SCENARIOS:
				getActiveScenarios().clear();
				getActiveScenarios().addAll((Collection<? extends ActiveScenario>)newValue);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__DYNAMIC_OBJECT_CONTAINER:
				setDynamicObjectContainer((DynamicObjectContainer)newValue);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__COMPUTED_INITIALIZING_MESSAGE_EVENTS:
				getComputedInitializingMessageEvents().clear();
				getComputedInitializingMessageEvents().addAll((Collection<? extends MessageEvent>)newValue);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__TERMINATED_EXISTENTIAL_SCENARIOS_FROM_LAST_PERFORM_STEP:
				getTerminatedExistentialScenariosFromLastPerformStep().clear();
				getTerminatedExistentialScenariosFromLastPerformStep().addAll((Collection<? extends Scenario>)newValue);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__MESSAGE_EVENT_BLOCKED_INFORMATION:
				getMessageEventBlockedInformation().clear();
				getMessageEventBlockedInformation().addAll((Collection<? extends MessageEventBlockedInformation>)newValue);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__OBJECT_SYSTEM:
				setObjectSystem((SMLObjectSystem)newValue);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_GUARANTEES:
				setSafetyViolationOccurredInGuarantees((Boolean)newValue);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS:
				setSafetyViolationOccurredInAssumptions((Boolean)newValue);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__EVENT_TO_TRANSITION_MAP:
				((EStructuralFeature.Setting)getEventToTransitionMap()).set(newValue);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__ENABLED_EVENTS:
				getEnabledEvents().clear();
				getEnabledEvents().addAll((Collection<? extends Event>)newValue);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__OUTGOING_TRANSITION:
				getOutgoingTransition().clear();
				getOutgoingTransition().addAll((Collection<? extends Transition>)newValue);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__INCOMING_TRANSITION:
				getIncomingTransition().clear();
				getIncomingTransition().addAll((Collection<? extends Transition>)newValue);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__STATE_GRAPH:
				setStateGraph((SMLRuntimeStateGraph)newValue);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__SYSTEM_CHOSE_TO_WAIT:
				setSystemChoseToWait((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.SML_RUNTIME_STATE__ACTIVE_SCENARIOS:
				getActiveScenarios().clear();
				return;
			case RuntimePackage.SML_RUNTIME_STATE__DYNAMIC_OBJECT_CONTAINER:
				setDynamicObjectContainer((DynamicObjectContainer)null);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__COMPUTED_INITIALIZING_MESSAGE_EVENTS:
				getComputedInitializingMessageEvents().clear();
				return;
			case RuntimePackage.SML_RUNTIME_STATE__TERMINATED_EXISTENTIAL_SCENARIOS_FROM_LAST_PERFORM_STEP:
				getTerminatedExistentialScenariosFromLastPerformStep().clear();
				return;
			case RuntimePackage.SML_RUNTIME_STATE__MESSAGE_EVENT_BLOCKED_INFORMATION:
				getMessageEventBlockedInformation().clear();
				return;
			case RuntimePackage.SML_RUNTIME_STATE__OBJECT_SYSTEM:
				setObjectSystem((SMLObjectSystem)null);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_GUARANTEES:
				setSafetyViolationOccurredInGuarantees(SAFETY_VIOLATION_OCCURRED_IN_GUARANTEES_EDEFAULT);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS:
				setSafetyViolationOccurredInAssumptions(SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS_EDEFAULT);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__EVENT_TO_TRANSITION_MAP:
				getEventToTransitionMap().clear();
				return;
			case RuntimePackage.SML_RUNTIME_STATE__ENABLED_EVENTS:
				getEnabledEvents().clear();
				return;
			case RuntimePackage.SML_RUNTIME_STATE__OUTGOING_TRANSITION:
				getOutgoingTransition().clear();
				return;
			case RuntimePackage.SML_RUNTIME_STATE__INCOMING_TRANSITION:
				getIncomingTransition().clear();
				return;
			case RuntimePackage.SML_RUNTIME_STATE__STATE_GRAPH:
				setStateGraph((SMLRuntimeStateGraph)null);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__SYSTEM_CHOSE_TO_WAIT:
				setSystemChoseToWait(SYSTEM_CHOSE_TO_WAIT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.SML_RUNTIME_STATE__ACTIVE_SCENARIOS:
				return activeScenarios != null && !activeScenarios.isEmpty();
			case RuntimePackage.SML_RUNTIME_STATE__DYNAMIC_OBJECT_CONTAINER:
				return dynamicObjectContainer != null;
			case RuntimePackage.SML_RUNTIME_STATE__COMPUTED_INITIALIZING_MESSAGE_EVENTS:
				return computedInitializingMessageEvents != null && !computedInitializingMessageEvents.isEmpty();
			case RuntimePackage.SML_RUNTIME_STATE__TERMINATED_EXISTENTIAL_SCENARIOS_FROM_LAST_PERFORM_STEP:
				return terminatedExistentialScenariosFromLastPerformStep != null && !terminatedExistentialScenariosFromLastPerformStep.isEmpty();
			case RuntimePackage.SML_RUNTIME_STATE__MESSAGE_EVENT_BLOCKED_INFORMATION:
				return messageEventBlockedInformation != null && !messageEventBlockedInformation.isEmpty();
			case RuntimePackage.SML_RUNTIME_STATE__OBJECT_SYSTEM:
				return objectSystem != null;
			case RuntimePackage.SML_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_GUARANTEES:
				return safetyViolationOccurredInGuarantees != SAFETY_VIOLATION_OCCURRED_IN_GUARANTEES_EDEFAULT;
			case RuntimePackage.SML_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS:
				return safetyViolationOccurredInAssumptions != SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS_EDEFAULT;
			case RuntimePackage.SML_RUNTIME_STATE__EVENT_TO_TRANSITION_MAP:
				return eventToTransitionMap != null && !eventToTransitionMap.isEmpty();
			case RuntimePackage.SML_RUNTIME_STATE__ENABLED_EVENTS:
				return enabledEvents != null && !enabledEvents.isEmpty();
			case RuntimePackage.SML_RUNTIME_STATE__OUTGOING_TRANSITION:
				return outgoingTransition != null && !outgoingTransition.isEmpty();
			case RuntimePackage.SML_RUNTIME_STATE__INCOMING_TRANSITION:
				return incomingTransition != null && !incomingTransition.isEmpty();
			case RuntimePackage.SML_RUNTIME_STATE__STATE_GRAPH:
				return getStateGraph() != null;
			case RuntimePackage.SML_RUNTIME_STATE__SYSTEM_CHOSE_TO_WAIT:
				return systemChoseToWait != SYSTEM_CHOSE_TO_WAIT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.SML_RUNTIME_STATE___INIT__SMLOBJECTSYSTEM:
				init((SMLObjectSystem)arguments.get(0));
				return null;
			case RuntimePackage.SML_RUNTIME_STATE___UPDATE_ENABLED_MESSAGE_EVENTS:
				updateEnabledMessageEvents();
				return null;
			case RuntimePackage.SML_RUNTIME_STATE___PERFORM_STEP__EVENT:
				performStep((Event)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (safetyViolationOccurredInGuarantees: ");
		result.append(safetyViolationOccurredInGuarantees);
		result.append(", safetyViolationOccurredInAssumptions: ");
		result.append(safetyViolationOccurredInAssumptions);
		result.append(", systemChoseToWait: ");
		result.append(systemChoseToWait);
		result.append(')');
		return result.toString();
	}

} //SMLRuntimeStateImpl