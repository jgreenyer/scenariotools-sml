/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.scenariotools.sml.runtime.ParameterValue;

import org.scenariotools.sml.ValueParameterExpression;

import org.scenariotools.sml.runtime.ActiveMessageParameterWithValueExpression;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.RuntimePackage;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.logic.ActiveMessageParameterWithValueExpressionLogic;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active Message Parameter With Value Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithValueExpressionImpl#getParameter <em>Parameter</em>}</li>
 * </ul>
 *
 * @generated PROTECT_DECLARATION 
 * (generated NOT would protect all the content of the class)
 */
public class ActiveMessageParameterWithValueExpressionImpl extends ActiveMessageParameterWithValueExpressionLogic implements ActiveMessageParameterWithValueExpression {
	/**
	 * The cached value of the '{@link #getParameter() <em>Parameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameter()
	 * @generated
	 * @ordered
	 */
	protected ValueParameterExpression parameter;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActiveMessageParameterWithValueExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueParameterExpression getParameter() {
		if (parameter != null && parameter.eIsProxy()) {
			InternalEObject oldParameter = (InternalEObject)parameter;
			parameter = (ValueParameterExpression)eResolveProxy(oldParameter);
			if (parameter != oldParameter) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION__PARAMETER, oldParameter, parameter));
			}
		}
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueParameterExpression basicGetParameter() {
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameter(ValueParameterExpression newParameter) {
		ValueParameterExpression oldParameter = parameter;
		parameter = newParameter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION__PARAMETER, oldParameter, parameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void init(ParameterValue parameterValue) {
		super.init(parameterValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void update(ParameterValue parameterValue, ActivePart parent, ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		super.update(parameterValue, parent, activeScenario, smlRuntimeState);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean hasSideEffectsOnUnification() {
		return super.hasSideEffectsOnUnification();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean executeSideEffectsOnUnification(ParameterValue parameterValueFromOccuredMessage, ParameterValue parameterValue, ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		return super.executeSideEffectsOnUnification(parameterValueFromOccuredMessage, parameterValue, activeScenario, smlRuntimeState);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION__PARAMETER:
				if (resolve) return getParameter();
				return basicGetParameter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION__PARAMETER:
				setParameter((ValueParameterExpression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION__PARAMETER:
				setParameter((ValueParameterExpression)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION__PARAMETER:
				return parameter != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION___INIT__PARAMETERVALUE:
				init((ParameterValue)arguments.get(0));
				return null;
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION___UPDATE__PARAMETERVALUE_ACTIVEPART_ACTIVESCENARIO_SMLRUNTIMESTATE:
				update((ParameterValue)arguments.get(0), (ActivePart)arguments.get(1), (ActiveScenario)arguments.get(2), (SMLRuntimeState)arguments.get(3));
				return null;
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION___HAS_SIDE_EFFECTS_ON_UNIFICATION:
				return hasSideEffectsOnUnification();
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION___EXECUTE_SIDE_EFFECTS_ON_UNIFICATION__PARAMETERVALUE_PARAMETERVALUE_ACTIVESCENARIO_SMLRUNTIMESTATE:
				return executeSideEffectsOnUnification((ParameterValue)arguments.get(0), (ParameterValue)arguments.get(1), (ActiveScenario)arguments.get(2), (SMLRuntimeState)arguments.get(3));
		}
		return super.eInvoke(operationID, arguments);
	}

} //ActiveMessageParameterWithValueExpressionImpl
