/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.sml.runtime.DynamicObjectContainer;
import org.scenariotools.sml.runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dynamic Object Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.impl.DynamicObjectContainerImpl#getStaticEObjectToDynamicEObjectMap <em>Static EObject To Dynamic EObject Map</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.DynamicObjectContainerImpl#getRootObjects <em>Root Objects</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DynamicObjectContainerImpl extends MinimalEObjectImpl.Container implements DynamicObjectContainer {
	/**
	 * The cached value of the '{@link #getStaticEObjectToDynamicEObjectMap() <em>Static EObject To Dynamic EObject Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStaticEObjectToDynamicEObjectMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<EObject, EObject> staticEObjectToDynamicEObjectMap;

	/**
	 * The cached value of the '{@link #getRootObjects() <em>Root Objects</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> rootObjects;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DynamicObjectContainerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.DYNAMIC_OBJECT_CONTAINER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EObject, EObject> getStaticEObjectToDynamicEObjectMap() {
		if (staticEObjectToDynamicEObjectMap == null) {
			staticEObjectToDynamicEObjectMap = new EcoreEMap<EObject,EObject>(RuntimePackage.Literals.STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY, StaticEObjectToDynamicEObjectMapEntryImpl.class, this, RuntimePackage.DYNAMIC_OBJECT_CONTAINER__STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP);
		}
		return staticEObjectToDynamicEObjectMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getRootObjects() {
		if (rootObjects == null) {
			rootObjects = new EObjectContainmentEList<EObject>(EObject.class, this, RuntimePackage.DYNAMIC_OBJECT_CONTAINER__ROOT_OBJECTS);
		}
		return rootObjects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.DYNAMIC_OBJECT_CONTAINER__STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP:
				return ((InternalEList<?>)getStaticEObjectToDynamicEObjectMap()).basicRemove(otherEnd, msgs);
			case RuntimePackage.DYNAMIC_OBJECT_CONTAINER__ROOT_OBJECTS:
				return ((InternalEList<?>)getRootObjects()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.DYNAMIC_OBJECT_CONTAINER__STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP:
				if (coreType) return getStaticEObjectToDynamicEObjectMap();
				else return getStaticEObjectToDynamicEObjectMap().map();
			case RuntimePackage.DYNAMIC_OBJECT_CONTAINER__ROOT_OBJECTS:
				return getRootObjects();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.DYNAMIC_OBJECT_CONTAINER__STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP:
				((EStructuralFeature.Setting)getStaticEObjectToDynamicEObjectMap()).set(newValue);
				return;
			case RuntimePackage.DYNAMIC_OBJECT_CONTAINER__ROOT_OBJECTS:
				getRootObjects().clear();
				getRootObjects().addAll((Collection<? extends EObject>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.DYNAMIC_OBJECT_CONTAINER__STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP:
				getStaticEObjectToDynamicEObjectMap().clear();
				return;
			case RuntimePackage.DYNAMIC_OBJECT_CONTAINER__ROOT_OBJECTS:
				getRootObjects().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.DYNAMIC_OBJECT_CONTAINER__STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP:
				return staticEObjectToDynamicEObjectMap != null && !staticEObjectToDynamicEObjectMap.isEmpty();
			case RuntimePackage.DYNAMIC_OBJECT_CONTAINER__ROOT_OBJECTS:
				return rootObjects != null && !rootObjects.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DynamicObjectContainerImpl
