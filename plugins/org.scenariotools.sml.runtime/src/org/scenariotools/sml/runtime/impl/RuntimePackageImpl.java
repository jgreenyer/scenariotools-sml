/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.impl;


import java.util.Map;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.ETypeParameter;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.scenariotools.sml.SmlPackage;
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage;
import org.scenariotools.sml.runtime.ActiveAlternative;
import org.scenariotools.sml.runtime.ActiveCase;
import org.scenariotools.sml.runtime.ActiveConstraint;
import org.scenariotools.sml.runtime.ActiveConstraintConsider;
import org.scenariotools.sml.runtime.ActiveConstraintForbidden;
import org.scenariotools.sml.runtime.ActiveConstraintIgnore;
import org.scenariotools.sml.runtime.ActiveConstraintInterrupt;
import org.scenariotools.sml.runtime.ActiveInteraction;
import org.scenariotools.sml.runtime.ActiveInterruptCondition;
import org.scenariotools.sml.runtime.ActiveLoop;
import org.scenariotools.sml.runtime.ActiveMessageParameter;
import org.scenariotools.sml.runtime.ActiveMessageParameterWithBindToVar;
import org.scenariotools.sml.runtime.ActiveMessageParameterWithValueExpression;
import org.scenariotools.sml.runtime.ActiveMessageParameterWithWildcard;
import org.scenariotools.sml.runtime.ActiveModalMessage;
import org.scenariotools.sml.runtime.ActiveParallel;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.ActiveVariableFragment;
import org.scenariotools.sml.runtime.ActiveViolationCondition;
import org.scenariotools.sml.runtime.ActiveWaitCondition;
import org.scenariotools.sml.runtime.AnnotatableElement;
import org.scenariotools.sml.runtime.BlockedType;
import org.scenariotools.sml.runtime.BooleanParameterValue;
import org.scenariotools.sml.runtime.Context;
import org.scenariotools.sml.runtime.DynamicObjectContainer;
import org.scenariotools.sml.runtime.EEnumParameterValue;
import org.scenariotools.sml.runtime.EObjectParameterValue;
import org.scenariotools.sml.runtime.ElementContainer;
import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.runtime.IntegerParameterValue;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.MessageEventBlockedInformation;
import org.scenariotools.sml.runtime.MessageEventExtensionInterface;
import org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator;
import org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor;
import org.scenariotools.sml.runtime.MultiActiveScenarioInitializations;
import org.scenariotools.sml.runtime.ParameterRangesProvider;
import org.scenariotools.sml.runtime.ParameterValue;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.RuntimePackage;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.StringParameterValue;
import org.scenariotools.sml.runtime.Transition;
import org.scenariotools.sml.runtime.ViolationKind;
import org.scenariotools.sml.runtime.WaitEvent;
import org.scenariotools.sml.runtime.configuration.ConfigurationPackage;
import org.scenariotools.sml.runtime.keywrapper.ActiveInteractionKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.ActiveScenarioRoleBindingsKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.ObjectSystemKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.StateKeyWrapper;
import org.scenariotools.sml.runtime.logic.ParameterRanges.ParameterValues;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RuntimePackageImpl extends EPackageImpl implements RuntimePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass smlRuntimeStateGraphEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass smlRuntimeStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeScenarioEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass contextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass elementContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass smlObjectSystemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activePartEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeAlternativeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeCaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeInteractionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeInterruptConditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeLoopEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeModalMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeParallelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeVariableFragmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeViolationConditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeWaitConditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeScenarioRoleBindingsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eClassToEObjectMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roleToEObjectMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeInteractionKeyWrapperToActiveInteractionMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeScenarioKeyWrapperToActiveScenarioMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass objectSystemKeyWrapperToObjectSystemMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass objectSystemKeyWrapperToDynamicObjectContainerMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stateKeyWrapperToStateMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variableToObjectMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variableToEObjectMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dynamicObjectContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass staticEObjectToDynamicEObjectMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass messageEventExtensionInterfaceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass messageEventSideEffectsExecutorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass messageEventIsIndependentEvaluatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeMessageParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeMessageParameterWithValueExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeMessageParameterWithBindToVarEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeMessageParameterWithWildcardEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeConstraintConsiderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeConstraintIgnoreEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeConstraintInterruptEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeConstraintForbiddenEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterRangesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass messageEventBlockedInformationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multiActiveScenarioInitializationsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeScenarioToActiveScenarioProgressMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventToTransitionMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annotatableElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringToBooleanMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringToStringMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringToEObjectMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass messageEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass booleanParameterValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass integerParameterValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringParameterValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eObjectParameterValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eEnumParameterValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass waitEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum violationKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum activeScenarioProgressEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum blockedTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType activeInteractionKeyWrapperEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType activeScenarioKeyWrapperEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType objectSystemKeyWrapperEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType stateKeyWrapperEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType activeScenarioRoleBindingsKeyWrapperEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType parameterValuesEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.scenariotools.sml.runtime.RuntimePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RuntimePackageImpl() {
		super(eNS_URI, RuntimeFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link RuntimePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RuntimePackage init() {
		if (isInited) return (RuntimePackage)EPackage.Registry.INSTANCE.getEPackage(RuntimePackage.eNS_URI);

		// Obtain or create and register package
		RuntimePackageImpl theRuntimePackage = (RuntimePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof RuntimePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new RuntimePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ConfigurationPackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		SmlPackage.eINSTANCE.eClass();
		ScenarioExpressionsPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theRuntimePackage.createPackageContents();

		// Initialize created meta-data
		theRuntimePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRuntimePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RuntimePackage.eNS_URI, theRuntimePackage);
		return theRuntimePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSMLRuntimeStateGraph() {
		return smlRuntimeStateGraphEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLRuntimeStateGraph_Configuration() {
		return (EReference)smlRuntimeStateGraphEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLRuntimeStateGraph_ElementContainer() {
		return (EReference)smlRuntimeStateGraphEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLRuntimeStateGraph_ParameterRangesProvider() {
		return (EReference)smlRuntimeStateGraphEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLRuntimeStateGraph_States() {
		return (EReference)smlRuntimeStateGraphEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLRuntimeStateGraph_StartState() {
		return (EReference)smlRuntimeStateGraphEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSMLRuntimeStateGraph__Init__Configuration() {
		return smlRuntimeStateGraphEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSMLRuntimeStateGraph__GenerateSuccessor__SMLRuntimeState_Event() {
		return smlRuntimeStateGraphEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSMLRuntimeStateGraph__GenerateAllSuccessors__SMLRuntimeState() {
		return smlRuntimeStateGraphEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSMLRuntimeState() {
		return smlRuntimeStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLRuntimeState_ActiveScenarios() {
		return (EReference)smlRuntimeStateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLRuntimeState_DynamicObjectContainer() {
		return (EReference)smlRuntimeStateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLRuntimeState_ComputedInitializingMessageEvents() {
		return (EReference)smlRuntimeStateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLRuntimeState_TerminatedExistentialScenariosFromLastPerformStep() {
		return (EReference)smlRuntimeStateEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLRuntimeState_MessageEventBlockedInformation() {
		return (EReference)smlRuntimeStateEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLRuntimeState_ObjectSystem() {
		return (EReference)smlRuntimeStateEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSMLRuntimeState_SafetyViolationOccurredInGuarantees() {
		return (EAttribute)smlRuntimeStateEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSMLRuntimeState_SafetyViolationOccurredInAssumptions() {
		return (EAttribute)smlRuntimeStateEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLRuntimeState_EventToTransitionMap() {
		return (EReference)smlRuntimeStateEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLRuntimeState_EnabledEvents() {
		return (EReference)smlRuntimeStateEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLRuntimeState_OutgoingTransition() {
		return (EReference)smlRuntimeStateEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLRuntimeState_IncomingTransition() {
		return (EReference)smlRuntimeStateEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLRuntimeState_StateGraph() {
		return (EReference)smlRuntimeStateEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSMLRuntimeState_SystemChoseToWait() {
		return (EAttribute)smlRuntimeStateEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSMLRuntimeState__Init__SMLObjectSystem() {
		return smlRuntimeStateEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSMLRuntimeState__UpdateEnabledMessageEvents() {
		return smlRuntimeStateEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSMLRuntimeState__PerformStep__Event() {
		return smlRuntimeStateEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveScenario() {
		return activeScenarioEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveScenario_Scenario() {
		return (EReference)activeScenarioEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveScenario_MainActiveInteraction() {
		return (EReference)activeScenarioEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveScenario_Alphabet() {
		return (EReference)activeScenarioEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveScenario_RoleBindings() {
		return (EReference)activeScenarioEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActiveScenario_SafetyViolationOccurred() {
		return (EAttribute)activeScenarioEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveScenario_ContextHelperClassInstances() {
		return (EReference)activeScenarioEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActiveScenario__PerformStep__MessageEvent_SMLRuntimeState() {
		return activeScenarioEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActiveScenario__Init__SMLObjectSystem_DynamicObjectContainer_SMLRuntimeState_MessageEvent() {
		return activeScenarioEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActiveScenario__IsBlocked__MessageEvent() {
		return activeScenarioEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActiveScenario__GetRequestedEvents() {
		return activeScenarioEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActiveScenario__IsInRequestedState() {
		return activeScenarioEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActiveScenario__IsInStrictState() {
		return activeScenarioEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getContext() {
		return contextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getContext__GetValue__Variable() {
		return contextEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getElementContainer() {
		return elementContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ActiveScenarios() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ActiveInteractions() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ObjectSystems() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ActiveScenarioRoleBindings() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_DynamicObjectContainer() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ActiveInteractionKeyWrapperToActiveInteractionMap() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ActiveScenarioKeyWrapperToActiveScenarioMap() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ObjectSystemKeyWrapperToObjectSystemMap() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ObjectSystemKeyWrapperToDynamicObjectContainerMap() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_StateKeyWrapperToStateMap() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getElementContainer_Enabled() {
		return (EAttribute)elementContainerEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementContainer_WaitEvent() {
		return (EReference)elementContainerEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getElementContainer__GetActiveScenario__ActiveScenario() {
		return elementContainerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getElementContainer__GetObjectSystem__SMLObjectSystem() {
		return elementContainerEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getElementContainer__GetSMLRuntimeState__SMLRuntimeState() {
		return elementContainerEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getElementContainer__GetActiveScenarioRoleBindings__ActiveScenarioRoleBindings() {
		return elementContainerEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getElementContainer__GetDynamicObjectContainer__DynamicObjectContainer_SMLObjectSystem() {
		return elementContainerEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSMLObjectSystem() {
		return smlObjectSystemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLObjectSystem_EClassToEObject() {
		return (EReference)smlObjectSystemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLObjectSystem_StaticRoleBindings() {
		return (EReference)smlObjectSystemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLObjectSystem_Specification() {
		return (EReference)smlObjectSystemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLObjectSystem_Runconfiguration() {
		return (EReference)smlObjectSystemEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLObjectSystem_MessageEventSideEffectsExecutor() {
		return (EReference)smlObjectSystemEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLObjectSystem_MessageEventIsIndependentEvaluators() {
		return (EReference)smlObjectSystemEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLObjectSystem_Objects() {
		return (EReference)smlObjectSystemEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLObjectSystem_ControllableObjects() {
		return (EReference)smlObjectSystemEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLObjectSystem_UncontrollableObjects() {
		return (EReference)smlObjectSystemEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSMLObjectSystem_RootObjects() {
		return (EReference)smlObjectSystemEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSMLObjectSystem__Init__Configuration_SMLRuntimeState() {
		return smlObjectSystemEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSMLObjectSystem__ExecuteSideEffects__MessageEvent_DynamicObjectContainer() {
		return smlObjectSystemEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSMLObjectSystem__CanExecuteSideEffects__MessageEvent_DynamicObjectContainer() {
		return smlObjectSystemEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSMLObjectSystem__IsIndependent__MessageEvent_DynamicObjectContainer() {
		return smlObjectSystemEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSMLObjectSystem__IsNonSpontaneousMessageEvent__MessageEvent() {
		return smlObjectSystemEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSMLObjectSystem__GetScenariosForInitMessageEvent__MessageEvent() {
		return smlObjectSystemEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSMLObjectSystem__GetInitializingEnvironmentMessageEvents__DynamicObjectContainer() {
		return smlObjectSystemEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSMLObjectSystem__IsControllable__EObject() {
		return smlObjectSystemEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSMLObjectSystem__IsEnvironmentMessageEvent__Event() {
		return smlObjectSystemEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSMLObjectSystem__ContainsEObject__EObject() {
		return smlObjectSystemEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActivePart() {
		return activePartEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivePart_NestedActiveInteractions() {
		return (EReference)activePartEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivePart_CoveredEvents() {
		return (EReference)activePartEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivePart_ForbiddenEvents() {
		return (EReference)activePartEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivePart_InterruptingEvents() {
		return (EReference)activePartEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivePart_ConsideredEvents() {
		return (EReference)activePartEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivePart_IgnoredEvents() {
		return (EReference)activePartEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivePart_EnabledEvents() {
		return (EReference)activePartEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivePart_ParentActiveInteraction() {
		return (EReference)activePartEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivePart_EnabledNestedActiveInteractions() {
		return (EReference)activePartEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivePart_VariableMap() {
		return (EReference)activePartEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivePart_EObjectVariableMap() {
		return (EReference)activePartEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivePart_InteractionFragment() {
		return (EReference)activePartEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActivePart__PerformStep__MessageEvent_ActiveScenario_SMLRuntimeState() {
		return activePartEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActivePart__PostPerformStep__MessageEvent_ActiveScenario_SMLRuntimeState() {
		return activePartEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActivePart__Init__ActiveScenarioRoleBindings_ActivePart_ActiveScenario() {
		return activePartEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActivePart__IsViolatingInInteraction__MessageEvent_boolean() {
		return activePartEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActivePart__UpdateMessageEvents__ActiveScenario_SMLRuntimeState() {
		return activePartEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActivePart__GetRequestedEvents() {
		return activePartEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActivePart__IsBlocked__MessageEvent_boolean() {
		return activePartEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActivePart__Enable__ActiveScenario_SMLRuntimeState() {
		return activePartEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActivePart__IsInRequestedState() {
		return activePartEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActivePart__IsInStrictState() {
		return activePartEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveAlternative() {
		return activeAlternativeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveCase() {
		return activeCaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveCase_Case() {
		return (EReference)activeCaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveInteraction() {
		return activeInteractionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveInteraction_ActiveConstraints() {
		return (EReference)activeInteractionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveInterruptCondition() {
		return activeInterruptConditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveLoop() {
		return activeLoopEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveModalMessage() {
		return activeModalMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveModalMessage_ActiveMessageParameters() {
		return (EReference)activeModalMessageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveParallel() {
		return activeParallelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveVariableFragment() {
		return activeVariableFragmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveViolationCondition() {
		return activeViolationConditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveWaitCondition() {
		return activeWaitConditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveScenarioRoleBindings() {
		return activeScenarioRoleBindingsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveScenarioRoleBindings_RoleBindings() {
		return (EReference)activeScenarioRoleBindingsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEClassToEObjectMapEntry() {
		return eClassToEObjectMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEClassToEObjectMapEntry_Key() {
		return (EReference)eClassToEObjectMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEClassToEObjectMapEntry_Value() {
		return (EReference)eClassToEObjectMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoleToEObjectMapEntry() {
		return roleToEObjectMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoleToEObjectMapEntry_Key() {
		return (EReference)roleToEObjectMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoleToEObjectMapEntry_Value() {
		return (EReference)roleToEObjectMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveInteractionKeyWrapperToActiveInteractionMapEntry() {
		return activeInteractionKeyWrapperToActiveInteractionMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActiveInteractionKeyWrapperToActiveInteractionMapEntry_Key() {
		return (EAttribute)activeInteractionKeyWrapperToActiveInteractionMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveInteractionKeyWrapperToActiveInteractionMapEntry_Value() {
		return (EReference)activeInteractionKeyWrapperToActiveInteractionMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveScenarioKeyWrapperToActiveScenarioMapEntry() {
		return activeScenarioKeyWrapperToActiveScenarioMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActiveScenarioKeyWrapperToActiveScenarioMapEntry_Key() {
		return (EAttribute)activeScenarioKeyWrapperToActiveScenarioMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveScenarioKeyWrapperToActiveScenarioMapEntry_Value() {
		return (EReference)activeScenarioKeyWrapperToActiveScenarioMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObjectSystemKeyWrapperToObjectSystemMapEntry() {
		return objectSystemKeyWrapperToObjectSystemMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getObjectSystemKeyWrapperToObjectSystemMapEntry_Key() {
		return (EAttribute)objectSystemKeyWrapperToObjectSystemMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObjectSystemKeyWrapperToObjectSystemMapEntry_Value() {
		return (EReference)objectSystemKeyWrapperToObjectSystemMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry() {
		return objectSystemKeyWrapperToDynamicObjectContainerMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry_Key() {
		return (EAttribute)objectSystemKeyWrapperToDynamicObjectContainerMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry_Value() {
		return (EReference)objectSystemKeyWrapperToDynamicObjectContainerMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStateKeyWrapperToStateMapEntry() {
		return stateKeyWrapperToStateMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStateKeyWrapperToStateMapEntry_Key() {
		return (EAttribute)stateKeyWrapperToStateMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStateKeyWrapperToStateMapEntry_Value() {
		return (EReference)stateKeyWrapperToStateMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVariableToObjectMapEntry() {
		return variableToObjectMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariableToObjectMapEntry_Key() {
		return (EReference)variableToObjectMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVariableToObjectMapEntry_Value() {
		return (EAttribute)variableToObjectMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVariableToEObjectMapEntry() {
		return variableToEObjectMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariableToEObjectMapEntry_Key() {
		return (EReference)variableToEObjectMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariableToEObjectMapEntry_Value() {
		return (EReference)variableToEObjectMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry() {
		return activeScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry_Key() {
		return (EAttribute)activeScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry_Value() {
		return (EReference)activeScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDynamicObjectContainer() {
		return dynamicObjectContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDynamicObjectContainer_StaticEObjectToDynamicEObjectMap() {
		return (EReference)dynamicObjectContainerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDynamicObjectContainer_RootObjects() {
		return (EReference)dynamicObjectContainerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStaticEObjectToDynamicEObjectMapEntry() {
		return staticEObjectToDynamicEObjectMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStaticEObjectToDynamicEObjectMapEntry_Key() {
		return (EReference)staticEObjectToDynamicEObjectMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStaticEObjectToDynamicEObjectMapEntry_Value() {
		return (EReference)staticEObjectToDynamicEObjectMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMessageEventExtensionInterface() {
		return messageEventExtensionInterfaceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMessageEventExtensionInterface__Init__Configuration() {
		return messageEventExtensionInterfaceEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMessageEventSideEffectsExecutor() {
		return messageEventSideEffectsExecutorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMessageEventSideEffectsExecutor__ExecuteSideEffects__MessageEvent_DynamicObjectContainer() {
		return messageEventSideEffectsExecutorEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMessageEventSideEffectsExecutor__CanExecuteSideEffects__MessageEvent_DynamicObjectContainer() {
		return messageEventSideEffectsExecutorEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMessageEventIsIndependentEvaluator() {
		return messageEventIsIndependentEvaluatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMessageEventIsIndependentEvaluator__IsIndependent__MessageEvent_DynamicObjectContainer() {
		return messageEventIsIndependentEvaluatorEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveMessageParameter() {
		return activeMessageParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActiveMessageParameter__Init__ParameterValue() {
		return activeMessageParameterEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActiveMessageParameter__Update__ParameterValue_ActivePart_ActiveScenario_SMLRuntimeState() {
		return activeMessageParameterEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActiveMessageParameter__HasSideEffectsOnUnification() {
		return activeMessageParameterEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActiveMessageParameter__ExecuteSideEffectsOnUnification__ParameterValue_ParameterValue_ActiveScenario_SMLRuntimeState() {
		return activeMessageParameterEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveMessageParameterWithValueExpression() {
		return activeMessageParameterWithValueExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveMessageParameterWithValueExpression_Parameter() {
		return (EReference)activeMessageParameterWithValueExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveMessageParameterWithBindToVar() {
		return activeMessageParameterWithBindToVarEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveMessageParameterWithBindToVar_Parameter() {
		return (EReference)activeMessageParameterWithBindToVarEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveMessageParameterWithWildcard() {
		return activeMessageParameterWithWildcardEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveMessageParameterWithWildcard_Parameter() {
		return (EReference)activeMessageParameterWithWildcardEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveConstraint() {
		return activeConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveConstraint_ConstraintMessageEvent() {
		return (EReference)activeConstraintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveConstraint_ActiveMessageParameters() {
		return (EReference)activeConstraintEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveConstraint_Message() {
		return (EReference)activeConstraintEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActiveConstraint__Init__ActiveScenarioRoleBindings_ActivePart_ActiveScenario() {
		return activeConstraintEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActiveConstraint__UpdateConstraintEvent__ActiveScenario_SMLRuntimeState() {
		return activeConstraintEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getActiveConstraint__AddToParentSpecificConstraintList__ActiveInteraction_MessageEvent() {
		return activeConstraintEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveConstraintConsider() {
		return activeConstraintConsiderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveConstraintIgnore() {
		return activeConstraintIgnoreEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveConstraintInterrupt() {
		return activeConstraintInterruptEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveConstraintForbidden() {
		return activeConstraintForbiddenEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameterRangesProvider() {
		return parameterRangesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getParameterRangesProvider__Init__Configuration() {
		return parameterRangesProviderEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getParameterRangesProvider__GetParameterValues__ETypedElement_SMLObjectSystem() {
		return parameterRangesProviderEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getParameterRangesProvider__GetSingelParameterValue__Object() {
		return parameterRangesProviderEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getParameterRangesProvider__Init__EList() {
		return parameterRangesProviderEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getParameterRangesProvider__ContainsParameterValues__ETypedElement() {
		return parameterRangesProviderEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMessageEventBlockedInformation() {
		return messageEventBlockedInformationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessageEventBlockedInformation_Description() {
		return (EAttribute)messageEventBlockedInformationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessageEventBlockedInformation_MessageEventString() {
		return (EAttribute)messageEventBlockedInformationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMultiActiveScenarioInitializations() {
		return multiActiveScenarioInitializationsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMultiActiveScenarioInitializations_ActiveScenarioToActiveScenarioProgressMap() {
		return (EReference)multiActiveScenarioInitializationsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveScenarioToActiveScenarioProgressMapEntry() {
		return activeScenarioToActiveScenarioProgressMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveScenarioToActiveScenarioProgressMapEntry_Key() {
		return (EReference)activeScenarioToActiveScenarioProgressMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActiveScenarioToActiveScenarioProgressMapEntry_Value() {
		return (EAttribute)activeScenarioToActiveScenarioProgressMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventToTransitionMapEntry() {
		return eventToTransitionMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventToTransitionMapEntry_Key() {
		return (EReference)eventToTransitionMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventToTransitionMapEntry_Value() {
		return (EReference)eventToTransitionMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransition() {
		return transitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransition_SourceState() {
		return (EReference)transitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransition_TargetState() {
		return (EReference)transitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransition_Event() {
		return (EReference)transitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransition_Label() {
		return (EAttribute)transitionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnnotatableElement() {
		return annotatableElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnotatableElement_StringToBooleanAnnotationMap() {
		return (EReference)annotatableElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnotatableElement_StringToStringAnnotationMap() {
		return (EReference)annotatableElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnotatableElement_StringToEObjectAnnotationMap() {
		return (EReference)annotatableElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringToBooleanMapEntry() {
		return stringToBooleanMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToBooleanMapEntry_Key() {
		return (EAttribute)stringToBooleanMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToBooleanMapEntry_Value() {
		return (EAttribute)stringToBooleanMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringToStringMapEntry() {
		return stringToStringMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToStringMapEntry_Key() {
		return (EAttribute)stringToStringMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToStringMapEntry_Value() {
		return (EAttribute)stringToStringMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringToEObjectMapEntry() {
		return stringToEObjectMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToEObjectMapEntry_Key() {
		return (EAttribute)stringToEObjectMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStringToEObjectMapEntry_Value() {
		return (EReference)stringToEObjectMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEvent() {
		return eventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMessageEvent() {
		return messageEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessageEvent_SendingObject() {
		return (EReference)messageEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessageEvent_ReceivingObject() {
		return (EReference)messageEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessageEvent_MessageName() {
		return (EAttribute)messageEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessageEvent_Concrete() {
		return (EAttribute)messageEventEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessageEvent_Parameterized() {
		return (EAttribute)messageEventEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessageEvent_TypedElement() {
		return (EReference)messageEventEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessageEvent_ParameterValues() {
		return (EReference)messageEventEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessageEvent_CollectionOperation() {
		return (EAttribute)messageEventEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMessageEvent__Eq__MessageEvent() {
		return messageEventEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMessageEvent__IsMessageUnifiableWith__MessageEvent() {
		return messageEventEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMessageEvent__IsParameterUnifiableWith__MessageEvent() {
		return messageEventEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameterValue() {
		return parameterValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameterValue_EParameter() {
		return (EReference)parameterValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterValue_Unset() {
		return (EAttribute)parameterValueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterValue_WildcardParameter() {
		return (EAttribute)parameterValueEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameterValue_StrucFeatureOrEOp() {
		return (EReference)parameterValueEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getParameterValue__SetValue__Object() {
		return parameterValueEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getParameterValue__GetValue() {
		return parameterValueEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getParameterValue__Eq__ParameterValue() {
		return parameterValueEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getParameterValue__IsParameterUnifiableWith__ParameterValue() {
		return parameterValueEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBooleanParameterValue() {
		return booleanParameterValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooleanParameterValue_BooleanParameterValue() {
		return (EAttribute)booleanParameterValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntegerParameterValue() {
		return integerParameterValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntegerParameterValue_IntegerParameterValue() {
		return (EAttribute)integerParameterValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringParameterValue() {
		return stringParameterValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringParameterValue_StringParameterValue() {
		return (EAttribute)stringParameterValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEObjectParameterValue() {
		return eObjectParameterValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEObjectParameterValue_EObjectParameterValue() {
		return (EReference)eObjectParameterValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEEnumParameterValue() {
		return eEnumParameterValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEEnumParameterValue_EEnumParameterType() {
		return (EReference)eEnumParameterValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEEnumParameterValue_EEnumParameterValue() {
		return (EReference)eEnumParameterValueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWaitEvent() {
		return waitEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getViolationKind() {
		return violationKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getActiveScenarioProgress() {
		return activeScenarioProgressEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getBlockedType() {
		return blockedTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getActiveInteractionKeyWrapper() {
		return activeInteractionKeyWrapperEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getActiveScenarioKeyWrapper() {
		return activeScenarioKeyWrapperEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getObjectSystemKeyWrapper() {
		return objectSystemKeyWrapperEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getStateKeyWrapper() {
		return stateKeyWrapperEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getActiveScenarioRoleBindingsKeyWrapper() {
		return activeScenarioRoleBindingsKeyWrapperEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getParameterValues() {
		return parameterValuesEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeFactory getRuntimeFactory() {
		return (RuntimeFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		annotatableElementEClass = createEClass(ANNOTATABLE_ELEMENT);
		createEReference(annotatableElementEClass, ANNOTATABLE_ELEMENT__STRING_TO_BOOLEAN_ANNOTATION_MAP);
		createEReference(annotatableElementEClass, ANNOTATABLE_ELEMENT__STRING_TO_STRING_ANNOTATION_MAP);
		createEReference(annotatableElementEClass, ANNOTATABLE_ELEMENT__STRING_TO_EOBJECT_ANNOTATION_MAP);

		smlRuntimeStateGraphEClass = createEClass(SML_RUNTIME_STATE_GRAPH);
		createEReference(smlRuntimeStateGraphEClass, SML_RUNTIME_STATE_GRAPH__CONFIGURATION);
		createEReference(smlRuntimeStateGraphEClass, SML_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER);
		createEReference(smlRuntimeStateGraphEClass, SML_RUNTIME_STATE_GRAPH__PARAMETER_RANGES_PROVIDER);
		createEReference(smlRuntimeStateGraphEClass, SML_RUNTIME_STATE_GRAPH__STATES);
		createEReference(smlRuntimeStateGraphEClass, SML_RUNTIME_STATE_GRAPH__START_STATE);
		createEOperation(smlRuntimeStateGraphEClass, SML_RUNTIME_STATE_GRAPH___INIT__CONFIGURATION);
		createEOperation(smlRuntimeStateGraphEClass, SML_RUNTIME_STATE_GRAPH___GENERATE_SUCCESSOR__SMLRUNTIMESTATE_EVENT);
		createEOperation(smlRuntimeStateGraphEClass, SML_RUNTIME_STATE_GRAPH___GENERATE_ALL_SUCCESSORS__SMLRUNTIMESTATE);

		elementContainerEClass = createEClass(ELEMENT_CONTAINER);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__ACTIVE_SCENARIOS);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__ACTIVE_INTERACTIONS);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__OBJECT_SYSTEMS);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__DYNAMIC_OBJECT_CONTAINER);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__STATE_KEY_WRAPPER_TO_STATE_MAP);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP);
		createEAttribute(elementContainerEClass, ELEMENT_CONTAINER__ENABLED);
		createEReference(elementContainerEClass, ELEMENT_CONTAINER__WAIT_EVENT);
		createEOperation(elementContainerEClass, ELEMENT_CONTAINER___GET_ACTIVE_SCENARIO__ACTIVESCENARIO);
		createEOperation(elementContainerEClass, ELEMENT_CONTAINER___GET_OBJECT_SYSTEM__SMLOBJECTSYSTEM);
		createEOperation(elementContainerEClass, ELEMENT_CONTAINER___GET_SML_RUNTIME_STATE__SMLRUNTIMESTATE);
		createEOperation(elementContainerEClass, ELEMENT_CONTAINER___GET_ACTIVE_SCENARIO_ROLE_BINDINGS__ACTIVESCENARIOROLEBINDINGS);
		createEOperation(elementContainerEClass, ELEMENT_CONTAINER___GET_DYNAMIC_OBJECT_CONTAINER__DYNAMICOBJECTCONTAINER_SMLOBJECTSYSTEM);

		smlRuntimeStateEClass = createEClass(SML_RUNTIME_STATE);
		createEReference(smlRuntimeStateEClass, SML_RUNTIME_STATE__ACTIVE_SCENARIOS);
		createEReference(smlRuntimeStateEClass, SML_RUNTIME_STATE__DYNAMIC_OBJECT_CONTAINER);
		createEReference(smlRuntimeStateEClass, SML_RUNTIME_STATE__COMPUTED_INITIALIZING_MESSAGE_EVENTS);
		createEReference(smlRuntimeStateEClass, SML_RUNTIME_STATE__TERMINATED_EXISTENTIAL_SCENARIOS_FROM_LAST_PERFORM_STEP);
		createEReference(smlRuntimeStateEClass, SML_RUNTIME_STATE__MESSAGE_EVENT_BLOCKED_INFORMATION);
		createEReference(smlRuntimeStateEClass, SML_RUNTIME_STATE__OBJECT_SYSTEM);
		createEAttribute(smlRuntimeStateEClass, SML_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_GUARANTEES);
		createEAttribute(smlRuntimeStateEClass, SML_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS);
		createEReference(smlRuntimeStateEClass, SML_RUNTIME_STATE__EVENT_TO_TRANSITION_MAP);
		createEReference(smlRuntimeStateEClass, SML_RUNTIME_STATE__ENABLED_EVENTS);
		createEReference(smlRuntimeStateEClass, SML_RUNTIME_STATE__OUTGOING_TRANSITION);
		createEReference(smlRuntimeStateEClass, SML_RUNTIME_STATE__INCOMING_TRANSITION);
		createEReference(smlRuntimeStateEClass, SML_RUNTIME_STATE__STATE_GRAPH);
		createEAttribute(smlRuntimeStateEClass, SML_RUNTIME_STATE__SYSTEM_CHOSE_TO_WAIT);
		createEOperation(smlRuntimeStateEClass, SML_RUNTIME_STATE___INIT__SMLOBJECTSYSTEM);
		createEOperation(smlRuntimeStateEClass, SML_RUNTIME_STATE___UPDATE_ENABLED_MESSAGE_EVENTS);
		createEOperation(smlRuntimeStateEClass, SML_RUNTIME_STATE___PERFORM_STEP__EVENT);

		smlObjectSystemEClass = createEClass(SML_OBJECT_SYSTEM);
		createEReference(smlObjectSystemEClass, SML_OBJECT_SYSTEM__ECLASS_TO_EOBJECT);
		createEReference(smlObjectSystemEClass, SML_OBJECT_SYSTEM__STATIC_ROLE_BINDINGS);
		createEReference(smlObjectSystemEClass, SML_OBJECT_SYSTEM__SPECIFICATION);
		createEReference(smlObjectSystemEClass, SML_OBJECT_SYSTEM__RUNCONFIGURATION);
		createEReference(smlObjectSystemEClass, SML_OBJECT_SYSTEM__MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR);
		createEReference(smlObjectSystemEClass, SML_OBJECT_SYSTEM__MESSAGE_EVENT_IS_INDEPENDENT_EVALUATORS);
		createEReference(smlObjectSystemEClass, SML_OBJECT_SYSTEM__OBJECTS);
		createEReference(smlObjectSystemEClass, SML_OBJECT_SYSTEM__CONTROLLABLE_OBJECTS);
		createEReference(smlObjectSystemEClass, SML_OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS);
		createEReference(smlObjectSystemEClass, SML_OBJECT_SYSTEM__ROOT_OBJECTS);
		createEOperation(smlObjectSystemEClass, SML_OBJECT_SYSTEM___INIT__CONFIGURATION_SMLRUNTIMESTATE);
		createEOperation(smlObjectSystemEClass, SML_OBJECT_SYSTEM___EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER);
		createEOperation(smlObjectSystemEClass, SML_OBJECT_SYSTEM___CAN_EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER);
		createEOperation(smlObjectSystemEClass, SML_OBJECT_SYSTEM___IS_INDEPENDENT__MESSAGEEVENT_DYNAMICOBJECTCONTAINER);
		createEOperation(smlObjectSystemEClass, SML_OBJECT_SYSTEM___IS_NON_SPONTANEOUS_MESSAGE_EVENT__MESSAGEEVENT);
		createEOperation(smlObjectSystemEClass, SML_OBJECT_SYSTEM___GET_SCENARIOS_FOR_INIT_MESSAGE_EVENT__MESSAGEEVENT);
		createEOperation(smlObjectSystemEClass, SML_OBJECT_SYSTEM___GET_INITIALIZING_ENVIRONMENT_MESSAGE_EVENTS__DYNAMICOBJECTCONTAINER);
		createEOperation(smlObjectSystemEClass, SML_OBJECT_SYSTEM___IS_CONTROLLABLE__EOBJECT);
		createEOperation(smlObjectSystemEClass, SML_OBJECT_SYSTEM___IS_ENVIRONMENT_MESSAGE_EVENT__EVENT);
		createEOperation(smlObjectSystemEClass, SML_OBJECT_SYSTEM___CONTAINS_EOBJECT__EOBJECT);

		dynamicObjectContainerEClass = createEClass(DYNAMIC_OBJECT_CONTAINER);
		createEReference(dynamicObjectContainerEClass, DYNAMIC_OBJECT_CONTAINER__STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP);
		createEReference(dynamicObjectContainerEClass, DYNAMIC_OBJECT_CONTAINER__ROOT_OBJECTS);

		transitionEClass = createEClass(TRANSITION);
		createEReference(transitionEClass, TRANSITION__SOURCE_STATE);
		createEReference(transitionEClass, TRANSITION__TARGET_STATE);
		createEReference(transitionEClass, TRANSITION__EVENT);
		createEAttribute(transitionEClass, TRANSITION__LABEL);

		eventEClass = createEClass(EVENT);

		waitEventEClass = createEClass(WAIT_EVENT);

		messageEventEClass = createEClass(MESSAGE_EVENT);
		createEReference(messageEventEClass, MESSAGE_EVENT__SENDING_OBJECT);
		createEReference(messageEventEClass, MESSAGE_EVENT__RECEIVING_OBJECT);
		createEAttribute(messageEventEClass, MESSAGE_EVENT__MESSAGE_NAME);
		createEAttribute(messageEventEClass, MESSAGE_EVENT__CONCRETE);
		createEAttribute(messageEventEClass, MESSAGE_EVENT__PARAMETERIZED);
		createEReference(messageEventEClass, MESSAGE_EVENT__TYPED_ELEMENT);
		createEReference(messageEventEClass, MESSAGE_EVENT__PARAMETER_VALUES);
		createEAttribute(messageEventEClass, MESSAGE_EVENT__COLLECTION_OPERATION);
		createEOperation(messageEventEClass, MESSAGE_EVENT___EQ__MESSAGEEVENT);
		createEOperation(messageEventEClass, MESSAGE_EVENT___IS_MESSAGE_UNIFIABLE_WITH__MESSAGEEVENT);
		createEOperation(messageEventEClass, MESSAGE_EVENT___IS_PARAMETER_UNIFIABLE_WITH__MESSAGEEVENT);

		parameterValueEClass = createEClass(PARAMETER_VALUE);
		createEReference(parameterValueEClass, PARAMETER_VALUE__EPARAMETER);
		createEAttribute(parameterValueEClass, PARAMETER_VALUE__UNSET);
		createEAttribute(parameterValueEClass, PARAMETER_VALUE__WILDCARD_PARAMETER);
		createEReference(parameterValueEClass, PARAMETER_VALUE__STRUC_FEATURE_OR_EOP);
		createEOperation(parameterValueEClass, PARAMETER_VALUE___SET_VALUE__OBJECT);
		createEOperation(parameterValueEClass, PARAMETER_VALUE___GET_VALUE);
		createEOperation(parameterValueEClass, PARAMETER_VALUE___EQ__PARAMETERVALUE);
		createEOperation(parameterValueEClass, PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE);

		booleanParameterValueEClass = createEClass(BOOLEAN_PARAMETER_VALUE);
		createEAttribute(booleanParameterValueEClass, BOOLEAN_PARAMETER_VALUE__BOOLEAN_PARAMETER_VALUE);

		integerParameterValueEClass = createEClass(INTEGER_PARAMETER_VALUE);
		createEAttribute(integerParameterValueEClass, INTEGER_PARAMETER_VALUE__INTEGER_PARAMETER_VALUE);

		stringParameterValueEClass = createEClass(STRING_PARAMETER_VALUE);
		createEAttribute(stringParameterValueEClass, STRING_PARAMETER_VALUE__STRING_PARAMETER_VALUE);

		eObjectParameterValueEClass = createEClass(EOBJECT_PARAMETER_VALUE);
		createEReference(eObjectParameterValueEClass, EOBJECT_PARAMETER_VALUE__EOBJECT_PARAMETER_VALUE);

		eEnumParameterValueEClass = createEClass(EENUM_PARAMETER_VALUE);
		createEReference(eEnumParameterValueEClass, EENUM_PARAMETER_VALUE__EENUM_PARAMETER_TYPE);
		createEReference(eEnumParameterValueEClass, EENUM_PARAMETER_VALUE__EENUM_PARAMETER_VALUE);

		contextEClass = createEClass(CONTEXT);
		createEOperation(contextEClass, CONTEXT___GET_VALUE__VARIABLE);

		activeScenarioEClass = createEClass(ACTIVE_SCENARIO);
		createEReference(activeScenarioEClass, ACTIVE_SCENARIO__SCENARIO);
		createEReference(activeScenarioEClass, ACTIVE_SCENARIO__MAIN_ACTIVE_INTERACTION);
		createEReference(activeScenarioEClass, ACTIVE_SCENARIO__ALPHABET);
		createEReference(activeScenarioEClass, ACTIVE_SCENARIO__ROLE_BINDINGS);
		createEAttribute(activeScenarioEClass, ACTIVE_SCENARIO__SAFETY_VIOLATION_OCCURRED);
		createEReference(activeScenarioEClass, ACTIVE_SCENARIO__CONTEXT_HELPER_CLASS_INSTANCES);
		createEOperation(activeScenarioEClass, ACTIVE_SCENARIO___PERFORM_STEP__MESSAGEEVENT_SMLRUNTIMESTATE);
		createEOperation(activeScenarioEClass, ACTIVE_SCENARIO___INIT__SMLOBJECTSYSTEM_DYNAMICOBJECTCONTAINER_SMLRUNTIMESTATE_MESSAGEEVENT);
		createEOperation(activeScenarioEClass, ACTIVE_SCENARIO___IS_BLOCKED__MESSAGEEVENT);
		createEOperation(activeScenarioEClass, ACTIVE_SCENARIO___GET_REQUESTED_EVENTS);
		createEOperation(activeScenarioEClass, ACTIVE_SCENARIO___IS_IN_REQUESTED_STATE);
		createEOperation(activeScenarioEClass, ACTIVE_SCENARIO___IS_IN_STRICT_STATE);

		activeScenarioRoleBindingsEClass = createEClass(ACTIVE_SCENARIO_ROLE_BINDINGS);
		createEReference(activeScenarioRoleBindingsEClass, ACTIVE_SCENARIO_ROLE_BINDINGS__ROLE_BINDINGS);

		activePartEClass = createEClass(ACTIVE_PART);
		createEReference(activePartEClass, ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS);
		createEReference(activePartEClass, ACTIVE_PART__COVERED_EVENTS);
		createEReference(activePartEClass, ACTIVE_PART__FORBIDDEN_EVENTS);
		createEReference(activePartEClass, ACTIVE_PART__INTERRUPTING_EVENTS);
		createEReference(activePartEClass, ACTIVE_PART__CONSIDERED_EVENTS);
		createEReference(activePartEClass, ACTIVE_PART__IGNORED_EVENTS);
		createEReference(activePartEClass, ACTIVE_PART__ENABLED_EVENTS);
		createEReference(activePartEClass, ACTIVE_PART__PARENT_ACTIVE_INTERACTION);
		createEReference(activePartEClass, ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS);
		createEReference(activePartEClass, ACTIVE_PART__VARIABLE_MAP);
		createEReference(activePartEClass, ACTIVE_PART__EOBJECT_VARIABLE_MAP);
		createEReference(activePartEClass, ACTIVE_PART__INTERACTION_FRAGMENT);
		createEOperation(activePartEClass, ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE);
		createEOperation(activePartEClass, ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE);
		createEOperation(activePartEClass, ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO);
		createEOperation(activePartEClass, ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN);
		createEOperation(activePartEClass, ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE);
		createEOperation(activePartEClass, ACTIVE_PART___GET_REQUESTED_EVENTS);
		createEOperation(activePartEClass, ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN);
		createEOperation(activePartEClass, ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE);
		createEOperation(activePartEClass, ACTIVE_PART___IS_IN_REQUESTED_STATE);
		createEOperation(activePartEClass, ACTIVE_PART___IS_IN_STRICT_STATE);

		activeAlternativeEClass = createEClass(ACTIVE_ALTERNATIVE);

		activeCaseEClass = createEClass(ACTIVE_CASE);
		createEReference(activeCaseEClass, ACTIVE_CASE__CASE);

		activeInteractionEClass = createEClass(ACTIVE_INTERACTION);
		createEReference(activeInteractionEClass, ACTIVE_INTERACTION__ACTIVE_CONSTRAINTS);

		activeInterruptConditionEClass = createEClass(ACTIVE_INTERRUPT_CONDITION);

		activeLoopEClass = createEClass(ACTIVE_LOOP);

		activeModalMessageEClass = createEClass(ACTIVE_MODAL_MESSAGE);
		createEReference(activeModalMessageEClass, ACTIVE_MODAL_MESSAGE__ACTIVE_MESSAGE_PARAMETERS);

		activeParallelEClass = createEClass(ACTIVE_PARALLEL);

		activeVariableFragmentEClass = createEClass(ACTIVE_VARIABLE_FRAGMENT);

		activeViolationConditionEClass = createEClass(ACTIVE_VIOLATION_CONDITION);

		activeWaitConditionEClass = createEClass(ACTIVE_WAIT_CONDITION);

		activeMessageParameterEClass = createEClass(ACTIVE_MESSAGE_PARAMETER);
		createEOperation(activeMessageParameterEClass, ACTIVE_MESSAGE_PARAMETER___INIT__PARAMETERVALUE);
		createEOperation(activeMessageParameterEClass, ACTIVE_MESSAGE_PARAMETER___UPDATE__PARAMETERVALUE_ACTIVEPART_ACTIVESCENARIO_SMLRUNTIMESTATE);
		createEOperation(activeMessageParameterEClass, ACTIVE_MESSAGE_PARAMETER___HAS_SIDE_EFFECTS_ON_UNIFICATION);
		createEOperation(activeMessageParameterEClass, ACTIVE_MESSAGE_PARAMETER___EXECUTE_SIDE_EFFECTS_ON_UNIFICATION__PARAMETERVALUE_PARAMETERVALUE_ACTIVESCENARIO_SMLRUNTIMESTATE);

		activeMessageParameterWithValueExpressionEClass = createEClass(ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION);
		createEReference(activeMessageParameterWithValueExpressionEClass, ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION__PARAMETER);

		activeMessageParameterWithBindToVarEClass = createEClass(ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR);
		createEReference(activeMessageParameterWithBindToVarEClass, ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR__PARAMETER);

		activeMessageParameterWithWildcardEClass = createEClass(ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD);
		createEReference(activeMessageParameterWithWildcardEClass, ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD__PARAMETER);

		activeConstraintEClass = createEClass(ACTIVE_CONSTRAINT);
		createEReference(activeConstraintEClass, ACTIVE_CONSTRAINT__CONSTRAINT_MESSAGE_EVENT);
		createEReference(activeConstraintEClass, ACTIVE_CONSTRAINT__ACTIVE_MESSAGE_PARAMETERS);
		createEReference(activeConstraintEClass, ACTIVE_CONSTRAINT__MESSAGE);
		createEOperation(activeConstraintEClass, ACTIVE_CONSTRAINT___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO);
		createEOperation(activeConstraintEClass, ACTIVE_CONSTRAINT___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE);
		createEOperation(activeConstraintEClass, ACTIVE_CONSTRAINT___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT);

		activeConstraintConsiderEClass = createEClass(ACTIVE_CONSTRAINT_CONSIDER);

		activeConstraintIgnoreEClass = createEClass(ACTIVE_CONSTRAINT_IGNORE);

		activeConstraintInterruptEClass = createEClass(ACTIVE_CONSTRAINT_INTERRUPT);

		activeConstraintForbiddenEClass = createEClass(ACTIVE_CONSTRAINT_FORBIDDEN);

		parameterRangesProviderEClass = createEClass(PARAMETER_RANGES_PROVIDER);
		createEOperation(parameterRangesProviderEClass, PARAMETER_RANGES_PROVIDER___INIT__CONFIGURATION);
		createEOperation(parameterRangesProviderEClass, PARAMETER_RANGES_PROVIDER___GET_PARAMETER_VALUES__ETYPEDELEMENT_SMLOBJECTSYSTEM);
		createEOperation(parameterRangesProviderEClass, PARAMETER_RANGES_PROVIDER___GET_SINGEL_PARAMETER_VALUE__OBJECT);
		createEOperation(parameterRangesProviderEClass, PARAMETER_RANGES_PROVIDER___INIT__ELIST);
		createEOperation(parameterRangesProviderEClass, PARAMETER_RANGES_PROVIDER___CONTAINS_PARAMETER_VALUES__ETYPEDELEMENT);

		messageEventBlockedInformationEClass = createEClass(MESSAGE_EVENT_BLOCKED_INFORMATION);
		createEAttribute(messageEventBlockedInformationEClass, MESSAGE_EVENT_BLOCKED_INFORMATION__DESCRIPTION);
		createEAttribute(messageEventBlockedInformationEClass, MESSAGE_EVENT_BLOCKED_INFORMATION__MESSAGE_EVENT_STRING);

		multiActiveScenarioInitializationsEClass = createEClass(MULTI_ACTIVE_SCENARIO_INITIALIZATIONS);
		createEReference(multiActiveScenarioInitializationsEClass, MULTI_ACTIVE_SCENARIO_INITIALIZATIONS__ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP);

		messageEventExtensionInterfaceEClass = createEClass(MESSAGE_EVENT_EXTENSION_INTERFACE);
		createEOperation(messageEventExtensionInterfaceEClass, MESSAGE_EVENT_EXTENSION_INTERFACE___INIT__CONFIGURATION);

		messageEventSideEffectsExecutorEClass = createEClass(MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR);
		createEOperation(messageEventSideEffectsExecutorEClass, MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR___EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER);
		createEOperation(messageEventSideEffectsExecutorEClass, MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR___CAN_EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER);

		messageEventIsIndependentEvaluatorEClass = createEClass(MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR);
		createEOperation(messageEventIsIndependentEvaluatorEClass, MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR___IS_INDEPENDENT__MESSAGEEVENT_DYNAMICOBJECTCONTAINER);

		eClassToEObjectMapEntryEClass = createEClass(ECLASS_TO_EOBJECT_MAP_ENTRY);
		createEReference(eClassToEObjectMapEntryEClass, ECLASS_TO_EOBJECT_MAP_ENTRY__KEY);
		createEReference(eClassToEObjectMapEntryEClass, ECLASS_TO_EOBJECT_MAP_ENTRY__VALUE);

		roleToEObjectMapEntryEClass = createEClass(ROLE_TO_EOBJECT_MAP_ENTRY);
		createEReference(roleToEObjectMapEntryEClass, ROLE_TO_EOBJECT_MAP_ENTRY__KEY);
		createEReference(roleToEObjectMapEntryEClass, ROLE_TO_EOBJECT_MAP_ENTRY__VALUE);

		staticEObjectToDynamicEObjectMapEntryEClass = createEClass(STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY);
		createEReference(staticEObjectToDynamicEObjectMapEntryEClass, STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY__KEY);
		createEReference(staticEObjectToDynamicEObjectMapEntryEClass, STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY__VALUE);

		activeScenarioToActiveScenarioProgressMapEntryEClass = createEClass(ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP_ENTRY);
		createEReference(activeScenarioToActiveScenarioProgressMapEntryEClass, ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP_ENTRY__KEY);
		createEAttribute(activeScenarioToActiveScenarioProgressMapEntryEClass, ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP_ENTRY__VALUE);

		eventToTransitionMapEntryEClass = createEClass(EVENT_TO_TRANSITION_MAP_ENTRY);
		createEReference(eventToTransitionMapEntryEClass, EVENT_TO_TRANSITION_MAP_ENTRY__KEY);
		createEReference(eventToTransitionMapEntryEClass, EVENT_TO_TRANSITION_MAP_ENTRY__VALUE);

		stringToBooleanMapEntryEClass = createEClass(STRING_TO_BOOLEAN_MAP_ENTRY);
		createEAttribute(stringToBooleanMapEntryEClass, STRING_TO_BOOLEAN_MAP_ENTRY__KEY);
		createEAttribute(stringToBooleanMapEntryEClass, STRING_TO_BOOLEAN_MAP_ENTRY__VALUE);

		stringToStringMapEntryEClass = createEClass(STRING_TO_STRING_MAP_ENTRY);
		createEAttribute(stringToStringMapEntryEClass, STRING_TO_STRING_MAP_ENTRY__KEY);
		createEAttribute(stringToStringMapEntryEClass, STRING_TO_STRING_MAP_ENTRY__VALUE);

		stringToEObjectMapEntryEClass = createEClass(STRING_TO_EOBJECT_MAP_ENTRY);
		createEAttribute(stringToEObjectMapEntryEClass, STRING_TO_EOBJECT_MAP_ENTRY__KEY);
		createEReference(stringToEObjectMapEntryEClass, STRING_TO_EOBJECT_MAP_ENTRY__VALUE);

		activeInteractionKeyWrapperToActiveInteractionMapEntryEClass = createEClass(ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY);
		createEAttribute(activeInteractionKeyWrapperToActiveInteractionMapEntryEClass, ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY__KEY);
		createEReference(activeInteractionKeyWrapperToActiveInteractionMapEntryEClass, ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY__VALUE);

		activeScenarioKeyWrapperToActiveScenarioMapEntryEClass = createEClass(ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY);
		createEAttribute(activeScenarioKeyWrapperToActiveScenarioMapEntryEClass, ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY__KEY);
		createEReference(activeScenarioKeyWrapperToActiveScenarioMapEntryEClass, ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY__VALUE);

		objectSystemKeyWrapperToObjectSystemMapEntryEClass = createEClass(OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY);
		createEAttribute(objectSystemKeyWrapperToObjectSystemMapEntryEClass, OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY__KEY);
		createEReference(objectSystemKeyWrapperToObjectSystemMapEntryEClass, OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY__VALUE);

		objectSystemKeyWrapperToDynamicObjectContainerMapEntryEClass = createEClass(OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY);
		createEAttribute(objectSystemKeyWrapperToDynamicObjectContainerMapEntryEClass, OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY__KEY);
		createEReference(objectSystemKeyWrapperToDynamicObjectContainerMapEntryEClass, OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY__VALUE);

		stateKeyWrapperToStateMapEntryEClass = createEClass(STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY);
		createEAttribute(stateKeyWrapperToStateMapEntryEClass, STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY__KEY);
		createEReference(stateKeyWrapperToStateMapEntryEClass, STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY__VALUE);

		variableToObjectMapEntryEClass = createEClass(VARIABLE_TO_OBJECT_MAP_ENTRY);
		createEReference(variableToObjectMapEntryEClass, VARIABLE_TO_OBJECT_MAP_ENTRY__KEY);
		createEAttribute(variableToObjectMapEntryEClass, VARIABLE_TO_OBJECT_MAP_ENTRY__VALUE);

		variableToEObjectMapEntryEClass = createEClass(VARIABLE_TO_EOBJECT_MAP_ENTRY);
		createEReference(variableToEObjectMapEntryEClass, VARIABLE_TO_EOBJECT_MAP_ENTRY__KEY);
		createEReference(variableToEObjectMapEntryEClass, VARIABLE_TO_EOBJECT_MAP_ENTRY__VALUE);

		activeScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryEClass = createEClass(ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY);
		createEAttribute(activeScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryEClass, ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__KEY);
		createEReference(activeScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryEClass, ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__VALUE);

		// Create enums
		violationKindEEnum = createEEnum(VIOLATION_KIND);
		activeScenarioProgressEEnum = createEEnum(ACTIVE_SCENARIO_PROGRESS);
		blockedTypeEEnum = createEEnum(BLOCKED_TYPE);

		// Create data types
		parameterValuesEDataType = createEDataType(PARAMETER_VALUES);
		activeInteractionKeyWrapperEDataType = createEDataType(ACTIVE_INTERACTION_KEY_WRAPPER);
		activeScenarioKeyWrapperEDataType = createEDataType(ACTIVE_SCENARIO_KEY_WRAPPER);
		objectSystemKeyWrapperEDataType = createEDataType(OBJECT_SYSTEM_KEY_WRAPPER);
		stateKeyWrapperEDataType = createEDataType(STATE_KEY_WRAPPER);
		activeScenarioRoleBindingsKeyWrapperEDataType = createEDataType(ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ConfigurationPackage theConfigurationPackage = (ConfigurationPackage)EPackage.Registry.INSTANCE.getEPackage(ConfigurationPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		SmlPackage theSmlPackage = (SmlPackage)EPackage.Registry.INSTANCE.getEPackage(SmlPackage.eNS_URI);
		ScenarioExpressionsPackage theScenarioExpressionsPackage = (ScenarioExpressionsPackage)EPackage.Registry.INSTANCE.getEPackage(ScenarioExpressionsPackage.eNS_URI);

		// Create type parameters
		ETypeParameter parameterValuesEDataType_T = addETypeParameter(parameterValuesEDataType, "T");

		// Set bounds for type parameters
		EGenericType g1 = createEGenericType(theEcorePackage.getEJavaObject());
		parameterValuesEDataType_T.getEBounds().add(g1);

		// Add supertypes to classes
		smlRuntimeStateEClass.getESuperTypes().add(this.getAnnotatableElement());
		transitionEClass.getESuperTypes().add(this.getAnnotatableElement());
		waitEventEClass.getESuperTypes().add(this.getEvent());
		messageEventEClass.getESuperTypes().add(this.getEvent());
		booleanParameterValueEClass.getESuperTypes().add(this.getParameterValue());
		integerParameterValueEClass.getESuperTypes().add(this.getParameterValue());
		stringParameterValueEClass.getESuperTypes().add(this.getParameterValue());
		eObjectParameterValueEClass.getESuperTypes().add(this.getParameterValue());
		eEnumParameterValueEClass.getESuperTypes().add(this.getParameterValue());
		activeScenarioEClass.getESuperTypes().add(this.getContext());
		activePartEClass.getESuperTypes().add(this.getContext());
		activeAlternativeEClass.getESuperTypes().add(this.getActivePart());
		activeCaseEClass.getESuperTypes().add(this.getActivePart());
		activeInteractionEClass.getESuperTypes().add(this.getActivePart());
		activeInterruptConditionEClass.getESuperTypes().add(this.getActivePart());
		activeLoopEClass.getESuperTypes().add(this.getActivePart());
		activeModalMessageEClass.getESuperTypes().add(this.getActivePart());
		activeParallelEClass.getESuperTypes().add(this.getActivePart());
		activeVariableFragmentEClass.getESuperTypes().add(this.getActivePart());
		activeViolationConditionEClass.getESuperTypes().add(this.getActivePart());
		activeWaitConditionEClass.getESuperTypes().add(this.getActivePart());
		activeMessageParameterWithValueExpressionEClass.getESuperTypes().add(this.getActiveMessageParameter());
		activeMessageParameterWithBindToVarEClass.getESuperTypes().add(this.getActiveMessageParameter());
		activeMessageParameterWithWildcardEClass.getESuperTypes().add(this.getActiveMessageParameter());
		activeConstraintConsiderEClass.getESuperTypes().add(this.getActiveConstraint());
		activeConstraintIgnoreEClass.getESuperTypes().add(this.getActiveConstraint());
		activeConstraintInterruptEClass.getESuperTypes().add(this.getActiveConstraint());
		activeConstraintForbiddenEClass.getESuperTypes().add(this.getActiveConstraint());
		messageEventSideEffectsExecutorEClass.getESuperTypes().add(this.getMessageEventExtensionInterface());
		messageEventIsIndependentEvaluatorEClass.getESuperTypes().add(this.getMessageEventExtensionInterface());

		// Initialize classes, features, and operations; add parameters
		initEClass(annotatableElementEClass, AnnotatableElement.class, "AnnotatableElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAnnotatableElement_StringToBooleanAnnotationMap(), this.getStringToBooleanMapEntry(), null, "stringToBooleanAnnotationMap", null, 0, -1, AnnotatableElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAnnotatableElement_StringToStringAnnotationMap(), this.getStringToStringMapEntry(), null, "stringToStringAnnotationMap", null, 0, -1, AnnotatableElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAnnotatableElement_StringToEObjectAnnotationMap(), this.getStringToEObjectMapEntry(), null, "stringToEObjectAnnotationMap", null, 0, -1, AnnotatableElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(smlRuntimeStateGraphEClass, SMLRuntimeStateGraph.class, "SMLRuntimeStateGraph", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSMLRuntimeStateGraph_Configuration(), theConfigurationPackage.getConfiguration(), null, "configuration", null, 0, 1, SMLRuntimeStateGraph.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLRuntimeStateGraph_ElementContainer(), this.getElementContainer(), null, "elementContainer", null, 0, 1, SMLRuntimeStateGraph.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLRuntimeStateGraph_ParameterRangesProvider(), this.getParameterRangesProvider(), null, "parameterRangesProvider", null, 0, 1, SMLRuntimeStateGraph.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLRuntimeStateGraph_States(), this.getSMLRuntimeState(), this.getSMLRuntimeState_StateGraph(), "states", null, 0, -1, SMLRuntimeStateGraph.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLRuntimeStateGraph_StartState(), this.getSMLRuntimeState(), null, "startState", null, 0, 1, SMLRuntimeStateGraph.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getSMLRuntimeStateGraph__Init__Configuration(), this.getSMLRuntimeState(), "init", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theConfigurationPackage.getConfiguration(), "config", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSMLRuntimeStateGraph__GenerateSuccessor__SMLRuntimeState_Event(), this.getTransition(), "generateSuccessor", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSMLRuntimeState(), "state", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEvent(), "event", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSMLRuntimeStateGraph__GenerateAllSuccessors__SMLRuntimeState(), this.getTransition(), "generateAllSuccessors", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSMLRuntimeState(), "state", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(elementContainerEClass, ElementContainer.class, "ElementContainer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getElementContainer_ActiveScenarios(), this.getActiveScenario(), null, "activeScenarios", null, 0, -1, ElementContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_ActiveInteractions(), this.getActivePart(), null, "activeInteractions", null, 0, -1, ElementContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_ObjectSystems(), this.getSMLObjectSystem(), null, "objectSystems", null, 0, -1, ElementContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_ActiveScenarioRoleBindings(), this.getActiveScenarioRoleBindings(), null, "activeScenarioRoleBindings", null, 0, -1, ElementContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_DynamicObjectContainer(), this.getDynamicObjectContainer(), null, "dynamicObjectContainer", null, 0, -1, ElementContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_ActiveInteractionKeyWrapperToActiveInteractionMap(), this.getActiveInteractionKeyWrapperToActiveInteractionMapEntry(), null, "activeInteractionKeyWrapperToActiveInteractionMap", null, 0, -1, ElementContainer.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_ActiveScenarioKeyWrapperToActiveScenarioMap(), this.getActiveScenarioKeyWrapperToActiveScenarioMapEntry(), null, "activeScenarioKeyWrapperToActiveScenarioMap", null, 0, -1, ElementContainer.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_ObjectSystemKeyWrapperToObjectSystemMap(), this.getObjectSystemKeyWrapperToObjectSystemMapEntry(), null, "objectSystemKeyWrapperToObjectSystemMap", null, 0, -1, ElementContainer.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_ObjectSystemKeyWrapperToDynamicObjectContainerMap(), this.getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry(), null, "objectSystemKeyWrapperToDynamicObjectContainerMap", null, 0, -1, ElementContainer.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_StateKeyWrapperToStateMap(), this.getStateKeyWrapperToStateMapEntry(), null, "stateKeyWrapperToStateMap", null, 0, -1, ElementContainer.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap(), this.getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry(), null, "activeScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap", null, 0, -1, ElementContainer.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getElementContainer_Enabled(), theEcorePackage.getEBoolean(), "enabled", "true", 0, 1, ElementContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getElementContainer_WaitEvent(), this.getWaitEvent(), null, "waitEvent", null, 0, 1, ElementContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getElementContainer__GetActiveScenario__ActiveScenario(), this.getActiveScenario(), "getActiveScenario", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActiveScenario(), "activeScenario", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getElementContainer__GetObjectSystem__SMLObjectSystem(), this.getSMLObjectSystem(), "getObjectSystem", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSMLObjectSystem(), "smlObjectSystem", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getElementContainer__GetSMLRuntimeState__SMLRuntimeState(), this.getSMLRuntimeState(), "getSMLRuntimeState", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSMLRuntimeState(), "smlRuntimeState", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getElementContainer__GetActiveScenarioRoleBindings__ActiveScenarioRoleBindings(), this.getActiveScenarioRoleBindings(), "getActiveScenarioRoleBindings", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActiveScenarioRoleBindings(), "activeScenarioRoleBindings", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getElementContainer__GetDynamicObjectContainer__DynamicObjectContainer_SMLObjectSystem(), this.getDynamicObjectContainer(), "getDynamicObjectContainer", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDynamicObjectContainer(), "dynamicObjectContainer", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSMLObjectSystem(), "objectSystem", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(smlRuntimeStateEClass, SMLRuntimeState.class, "SMLRuntimeState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSMLRuntimeState_ActiveScenarios(), this.getActiveScenario(), null, "activeScenarios", null, 0, -1, SMLRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLRuntimeState_DynamicObjectContainer(), this.getDynamicObjectContainer(), null, "dynamicObjectContainer", null, 0, 1, SMLRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLRuntimeState_ComputedInitializingMessageEvents(), this.getMessageEvent(), null, "computedInitializingMessageEvents", null, 0, -1, SMLRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLRuntimeState_TerminatedExistentialScenariosFromLastPerformStep(), theSmlPackage.getScenario(), null, "terminatedExistentialScenariosFromLastPerformStep", null, 0, -1, SMLRuntimeState.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLRuntimeState_MessageEventBlockedInformation(), this.getMessageEventBlockedInformation(), null, "messageEventBlockedInformation", null, 0, -1, SMLRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLRuntimeState_ObjectSystem(), this.getSMLObjectSystem(), null, "objectSystem", null, 0, 1, SMLRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSMLRuntimeState_SafetyViolationOccurredInGuarantees(), theEcorePackage.getEBoolean(), "safetyViolationOccurredInGuarantees", null, 0, 1, SMLRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSMLRuntimeState_SafetyViolationOccurredInAssumptions(), theEcorePackage.getEBoolean(), "safetyViolationOccurredInAssumptions", null, 0, 1, SMLRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLRuntimeState_EventToTransitionMap(), this.getEventToTransitionMapEntry(), null, "eventToTransitionMap", null, 0, -1, SMLRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLRuntimeState_EnabledEvents(), this.getEvent(), null, "enabledEvents", null, 0, -1, SMLRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLRuntimeState_OutgoingTransition(), this.getTransition(), this.getTransition_SourceState(), "outgoingTransition", null, 0, -1, SMLRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLRuntimeState_IncomingTransition(), this.getTransition(), this.getTransition_TargetState(), "incomingTransition", null, 0, -1, SMLRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLRuntimeState_StateGraph(), this.getSMLRuntimeStateGraph(), this.getSMLRuntimeStateGraph_States(), "stateGraph", null, 0, 1, SMLRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSMLRuntimeState_SystemChoseToWait(), ecorePackage.getEBoolean(), "systemChoseToWait", "false", 1, 1, SMLRuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getSMLRuntimeState__Init__SMLObjectSystem(), null, "init", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSMLObjectSystem(), "objectSystem", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getSMLRuntimeState__UpdateEnabledMessageEvents(), null, "updateEnabledMessageEvents", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSMLRuntimeState__PerformStep__Event(), null, "performStep", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEvent(), "event", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(smlObjectSystemEClass, SMLObjectSystem.class, "SMLObjectSystem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSMLObjectSystem_EClassToEObject(), this.getEClassToEObjectMapEntry(), null, "eClassToEObject", null, 0, -1, SMLObjectSystem.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLObjectSystem_StaticRoleBindings(), this.getRoleToEObjectMapEntry(), null, "staticRoleBindings", null, 0, -1, SMLObjectSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLObjectSystem_Specification(), theSmlPackage.getSpecification(), null, "specification", null, 0, 1, SMLObjectSystem.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getSMLObjectSystem_Runconfiguration(), theConfigurationPackage.getConfiguration(), null, "runconfiguration", null, 0, 1, SMLObjectSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLObjectSystem_MessageEventSideEffectsExecutor(), this.getMessageEventSideEffectsExecutor(), null, "messageEventSideEffectsExecutor", null, 0, -1, SMLObjectSystem.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLObjectSystem_MessageEventIsIndependentEvaluators(), this.getMessageEventIsIndependentEvaluator(), null, "messageEventIsIndependentEvaluators", null, 0, -1, SMLObjectSystem.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLObjectSystem_Objects(), theEcorePackage.getEObject(), null, "objects", null, 0, -1, SMLObjectSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLObjectSystem_ControllableObjects(), theEcorePackage.getEObject(), null, "controllableObjects", null, 0, -1, SMLObjectSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLObjectSystem_UncontrollableObjects(), theEcorePackage.getEObject(), null, "uncontrollableObjects", null, 0, -1, SMLObjectSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSMLObjectSystem_RootObjects(), theEcorePackage.getEObject(), null, "rootObjects", null, 0, -1, SMLObjectSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getSMLObjectSystem__Init__Configuration_SMLRuntimeState(), null, "init", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theConfigurationPackage.getConfiguration(), "runConfiguration", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSMLRuntimeState(), "smlRuntimeState", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSMLObjectSystem__ExecuteSideEffects__MessageEvent_DynamicObjectContainer(), null, "executeSideEffects", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDynamicObjectContainer(), "dynamicObjectContainer", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSMLObjectSystem__CanExecuteSideEffects__MessageEvent_DynamicObjectContainer(), theEcorePackage.getEBoolean(), "canExecuteSideEffects", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDynamicObjectContainer(), "dynamicObjectContainer", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSMLObjectSystem__IsIndependent__MessageEvent_DynamicObjectContainer(), theEcorePackage.getEBoolean(), "isIndependent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDynamicObjectContainer(), "dynamicObjectContainer", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSMLObjectSystem__IsNonSpontaneousMessageEvent__MessageEvent(), theEcorePackage.getEBoolean(), "isNonSpontaneousMessageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSMLObjectSystem__GetScenariosForInitMessageEvent__MessageEvent(), theSmlPackage.getScenario(), "getScenariosForInitMessageEvent", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "initMessageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSMLObjectSystem__GetInitializingEnvironmentMessageEvents__DynamicObjectContainer(), this.getMessageEvent(), "getInitializingEnvironmentMessageEvents", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDynamicObjectContainer(), "dynamicObjectProvider", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSMLObjectSystem__IsControllable__EObject(), ecorePackage.getEBoolean(), "isControllable", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSMLObjectSystem__IsEnvironmentMessageEvent__Event(), theEcorePackage.getEBoolean(), "isEnvironmentMessageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEvent(), "event", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSMLObjectSystem__ContainsEObject__EObject(), ecorePackage.getEBoolean(), "containsEObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(dynamicObjectContainerEClass, DynamicObjectContainer.class, "DynamicObjectContainer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDynamicObjectContainer_StaticEObjectToDynamicEObjectMap(), this.getStaticEObjectToDynamicEObjectMapEntry(), null, "staticEObjectToDynamicEObjectMap", null, 0, -1, DynamicObjectContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDynamicObjectContainer_RootObjects(), theEcorePackage.getEObject(), null, "rootObjects", null, 0, -1, DynamicObjectContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(transitionEClass, Transition.class, "Transition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTransition_SourceState(), this.getSMLRuntimeState(), this.getSMLRuntimeState_OutgoingTransition(), "sourceState", null, 0, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransition_TargetState(), this.getSMLRuntimeState(), this.getSMLRuntimeState_IncomingTransition(), "targetState", null, 0, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransition_Event(), this.getEvent(), null, "event", null, 0, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTransition_Label(), theEcorePackage.getEString(), "label", null, 0, 1, Transition.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(eventEClass, Event.class, "Event", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(waitEventEClass, WaitEvent.class, "WaitEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(messageEventEClass, MessageEvent.class, "MessageEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMessageEvent_SendingObject(), theEcorePackage.getEObject(), null, "sendingObject", null, 0, 1, MessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessageEvent_ReceivingObject(), theEcorePackage.getEObject(), null, "receivingObject", null, 0, 1, MessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMessageEvent_MessageName(), ecorePackage.getEString(), "messageName", null, 0, 1, MessageEvent.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMessageEvent_Concrete(), ecorePackage.getEBoolean(), "concrete", null, 0, 1, MessageEvent.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMessageEvent_Parameterized(), theEcorePackage.getEBoolean(), "parameterized", null, 0, 1, MessageEvent.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessageEvent_TypedElement(), theEcorePackage.getETypedElement(), null, "typedElement", null, 0, 1, MessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessageEvent_ParameterValues(), this.getParameterValue(), null, "parameterValues", null, 0, -1, MessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMessageEvent_CollectionOperation(), theScenarioExpressionsPackage.getCollectionOperation(), "collectionOperation", "isEmpty", 0, 1, MessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getMessageEvent__Eq__MessageEvent(), theEcorePackage.getEBoolean(), "eq", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMessageEvent__IsMessageUnifiableWith__MessageEvent(), theEcorePackage.getEBoolean(), "isMessageUnifiableWith", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMessageEvent__IsParameterUnifiableWith__MessageEvent(), theEcorePackage.getEBoolean(), "isParameterUnifiableWith", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(parameterValueEClass, ParameterValue.class, "ParameterValue", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getParameterValue_EParameter(), theEcorePackage.getEParameter(), null, "eParameter", null, 0, 1, ParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getParameterValue_Unset(), ecorePackage.getEBoolean(), "unset", "true", 0, 1, ParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getParameterValue_WildcardParameter(), theEcorePackage.getEBoolean(), "wildcardParameter", null, 0, 1, ParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getParameterValue_StrucFeatureOrEOp(), theEcorePackage.getETypedElement(), null, "strucFeatureOrEOp", null, 0, 1, ParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getParameterValue__SetValue__Object(), null, "setValue", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "value", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getParameterValue__GetValue(), theEcorePackage.getEJavaObject(), "getValue", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getParameterValue__Eq__ParameterValue(), theEcorePackage.getEBoolean(), "eq", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getParameterValue(), "otherParameterValue", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getParameterValue__IsParameterUnifiableWith__ParameterValue(), theEcorePackage.getEBoolean(), "isParameterUnifiableWith", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getParameterValue(), "otherParameterValue", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(booleanParameterValueEClass, BooleanParameterValue.class, "BooleanParameterValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBooleanParameterValue_BooleanParameterValue(), theEcorePackage.getEBoolean(), "booleanParameterValue", null, 0, 1, BooleanParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(integerParameterValueEClass, IntegerParameterValue.class, "IntegerParameterValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntegerParameterValue_IntegerParameterValue(), theEcorePackage.getEInt(), "integerParameterValue", null, 0, 1, IntegerParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stringParameterValueEClass, StringParameterValue.class, "StringParameterValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringParameterValue_StringParameterValue(), theEcorePackage.getEString(), "stringParameterValue", null, 0, 1, StringParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eObjectParameterValueEClass, EObjectParameterValue.class, "EObjectParameterValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEObjectParameterValue_EObjectParameterValue(), theEcorePackage.getEObject(), null, "eObjectParameterValue", null, 0, 1, EObjectParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eEnumParameterValueEClass, EEnumParameterValue.class, "EEnumParameterValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEEnumParameterValue_EEnumParameterType(), theEcorePackage.getEEnum(), null, "eEnumParameterType", null, 0, 1, EEnumParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEEnumParameterValue_EEnumParameterValue(), theEcorePackage.getEEnumLiteral(), null, "eEnumParameterValue", null, 0, 1, EEnumParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(contextEClass, Context.class, "Context", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getContext__GetValue__Variable(), theEcorePackage.getEJavaObject(), "getValue", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theScenarioExpressionsPackage.getVariable(), "variable", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(activeScenarioEClass, ActiveScenario.class, "ActiveScenario", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveScenario_Scenario(), theSmlPackage.getScenario(), null, "scenario", null, 0, 1, ActiveScenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveScenario_MainActiveInteraction(), this.getActivePart(), null, "mainActiveInteraction", null, 0, 1, ActiveScenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveScenario_Alphabet(), this.getMessageEvent(), null, "alphabet", null, 0, -1, ActiveScenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveScenario_RoleBindings(), this.getActiveScenarioRoleBindings(), null, "roleBindings", null, 0, 1, ActiveScenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActiveScenario_SafetyViolationOccurred(), theEcorePackage.getEBoolean(), "safetyViolationOccurred", null, 0, 1, ActiveScenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveScenario_ContextHelperClassInstances(), this.getEClassToEObjectMapEntry(), null, "contextHelperClassInstances", null, 0, -1, ActiveScenario.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getActiveScenario__PerformStep__MessageEvent_SMLRuntimeState(), this.getActiveScenarioProgress(), "performStep", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "event", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSMLRuntimeState(), "smlRuntimeState", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getActiveScenario__Init__SMLObjectSystem_DynamicObjectContainer_SMLRuntimeState_MessageEvent(), this.getMultiActiveScenarioInitializations(), "init", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSMLObjectSystem(), "objectSystem", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDynamicObjectContainer(), "dynamicObjectContainer", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSMLRuntimeState(), "smlRuntimeState", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "initializingMessageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getActiveScenario__IsBlocked__MessageEvent(), ecorePackage.getEBoolean(), "isBlocked", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "MessageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getActiveScenario__GetRequestedEvents(), this.getMessageEvent(), "getRequestedEvents", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getActiveScenario__IsInRequestedState(), theEcorePackage.getEBoolean(), "isInRequestedState", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getActiveScenario__IsInStrictState(), theEcorePackage.getEBoolean(), "isInStrictState", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(activeScenarioRoleBindingsEClass, ActiveScenarioRoleBindings.class, "ActiveScenarioRoleBindings", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveScenarioRoleBindings_RoleBindings(), this.getRoleToEObjectMapEntry(), null, "roleBindings", null, 0, -1, ActiveScenarioRoleBindings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activePartEClass, ActivePart.class, "ActivePart", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActivePart_NestedActiveInteractions(), this.getActivePart(), null, "nestedActiveInteractions", null, 0, -1, ActivePart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActivePart_CoveredEvents(), this.getMessageEvent(), null, "coveredEvents", null, 0, -1, ActivePart.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActivePart_ForbiddenEvents(), this.getMessageEvent(), null, "forbiddenEvents", null, 0, -1, ActivePart.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActivePart_InterruptingEvents(), this.getMessageEvent(), null, "interruptingEvents", null, 0, -1, ActivePart.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActivePart_ConsideredEvents(), this.getMessageEvent(), null, "consideredEvents", null, 0, -1, ActivePart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActivePart_IgnoredEvents(), this.getMessageEvent(), null, "ignoredEvents", null, 0, -1, ActivePart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActivePart_EnabledEvents(), this.getMessageEvent(), null, "enabledEvents", null, 0, -1, ActivePart.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActivePart_ParentActiveInteraction(), this.getActivePart(), null, "parentActiveInteraction", null, 0, 1, ActivePart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActivePart_EnabledNestedActiveInteractions(), this.getActivePart(), null, "enabledNestedActiveInteractions", null, 0, -1, ActivePart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActivePart_VariableMap(), this.getVariableToObjectMapEntry(), null, "variableMap", null, 0, -1, ActivePart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActivePart_EObjectVariableMap(), this.getVariableToEObjectMapEntry(), null, "eObjectVariableMap", null, 0, -1, ActivePart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActivePart_InteractionFragment(), theSmlPackage.getInteractionFragment(), null, "interactionFragment", null, 0, 1, ActivePart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getActivePart__PerformStep__MessageEvent_ActiveScenario_SMLRuntimeState(), this.getActiveScenarioProgress(), "performStep", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "event", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActiveScenario(), "activeScenario", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSMLRuntimeState(), "smlRuntimeState", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getActivePart__PostPerformStep__MessageEvent_ActiveScenario_SMLRuntimeState(), this.getActiveScenarioProgress(), "postPerformStep", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "event", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActiveScenario(), "activeScenario", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSMLRuntimeState(), "smlRuntimeState", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getActivePart__Init__ActiveScenarioRoleBindings_ActivePart_ActiveScenario(), null, "init", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActiveScenarioRoleBindings(), "roleBindings", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActivePart(), "parentActivePart", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActiveScenario(), "activeScenario", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getActivePart__IsViolatingInInteraction__MessageEvent_boolean(), this.getViolationKind(), "isViolatingInInteraction", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "event", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEBoolean(), "isInStrictCut", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getActivePart__UpdateMessageEvents__ActiveScenario_SMLRuntimeState(), null, "updateMessageEvents", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActiveScenario(), "activeScenario", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSMLRuntimeState(), "smlRuntimeState", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getActivePart__GetRequestedEvents(), this.getMessageEvent(), "getRequestedEvents", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getActivePart__IsBlocked__MessageEvent_boolean(), this.getBlockedType(), "isBlocked", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEBoolean(), "isInStrictCut", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getActivePart__Enable__ActiveScenario_SMLRuntimeState(), this.getActiveScenarioProgress(), "enable", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActiveScenario(), "activeScenario", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSMLRuntimeState(), "smlRuntimeState", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getActivePart__IsInRequestedState(), theEcorePackage.getEBoolean(), "isInRequestedState", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getActivePart__IsInStrictState(), theEcorePackage.getEBoolean(), "isInStrictState", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(activeAlternativeEClass, ActiveAlternative.class, "ActiveAlternative", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(activeCaseEClass, ActiveCase.class, "ActiveCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveCase_Case(), theSmlPackage.getCase(), null, "case", null, 0, 1, ActiveCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activeInteractionEClass, ActiveInteraction.class, "ActiveInteraction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveInteraction_ActiveConstraints(), this.getActiveConstraint(), null, "activeConstraints", null, 0, -1, ActiveInteraction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activeInterruptConditionEClass, ActiveInterruptCondition.class, "ActiveInterruptCondition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(activeLoopEClass, ActiveLoop.class, "ActiveLoop", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(activeModalMessageEClass, ActiveModalMessage.class, "ActiveModalMessage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveModalMessage_ActiveMessageParameters(), this.getActiveMessageParameter(), null, "activeMessageParameters", null, 0, -1, ActiveModalMessage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activeParallelEClass, ActiveParallel.class, "ActiveParallel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(activeVariableFragmentEClass, ActiveVariableFragment.class, "ActiveVariableFragment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(activeViolationConditionEClass, ActiveViolationCondition.class, "ActiveViolationCondition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(activeWaitConditionEClass, ActiveWaitCondition.class, "ActiveWaitCondition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(activeMessageParameterEClass, ActiveMessageParameter.class, "ActiveMessageParameter", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getActiveMessageParameter__Init__ParameterValue(), null, "init", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getParameterValue(), "parameterValue", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getActiveMessageParameter__Update__ParameterValue_ActivePart_ActiveScenario_SMLRuntimeState(), null, "update", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getParameterValue(), "parameterValue", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActivePart(), "parent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActiveScenario(), "activeScenario", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSMLRuntimeState(), "smlRuntimeState", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getActiveMessageParameter__HasSideEffectsOnUnification(), theEcorePackage.getEBoolean(), "hasSideEffectsOnUnification", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getActiveMessageParameter__ExecuteSideEffectsOnUnification__ParameterValue_ParameterValue_ActiveScenario_SMLRuntimeState(), theEcorePackage.getEBoolean(), "executeSideEffectsOnUnification", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getParameterValue(), "parameterValueFromOccuredMessage", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getParameterValue(), "parameterValue", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActiveScenario(), "activeScenario", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSMLRuntimeState(), "smlRuntimeState", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(activeMessageParameterWithValueExpressionEClass, ActiveMessageParameterWithValueExpression.class, "ActiveMessageParameterWithValueExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveMessageParameterWithValueExpression_Parameter(), theSmlPackage.getValueParameterExpression(), null, "parameter", null, 0, 1, ActiveMessageParameterWithValueExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activeMessageParameterWithBindToVarEClass, ActiveMessageParameterWithBindToVar.class, "ActiveMessageParameterWithBindToVar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveMessageParameterWithBindToVar_Parameter(), theSmlPackage.getVariableBindingParameterExpression(), null, "parameter", null, 0, 1, ActiveMessageParameterWithBindToVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activeMessageParameterWithWildcardEClass, ActiveMessageParameterWithWildcard.class, "ActiveMessageParameterWithWildcard", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveMessageParameterWithWildcard_Parameter(), theSmlPackage.getWildcardParameterExpression(), null, "parameter", null, 0, 1, ActiveMessageParameterWithWildcard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activeConstraintEClass, ActiveConstraint.class, "ActiveConstraint", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveConstraint_ConstraintMessageEvent(), this.getMessageEvent(), null, "constraintMessageEvent", null, 0, 1, ActiveConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveConstraint_ActiveMessageParameters(), this.getActiveMessageParameter(), null, "activeMessageParameters", null, 0, -1, ActiveConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveConstraint_Message(), theSmlPackage.getMessage(), null, "message", null, 0, 1, ActiveConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getActiveConstraint__Init__ActiveScenarioRoleBindings_ActivePart_ActiveScenario(), null, "init", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActiveScenarioRoleBindings(), "roleBindings", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActivePart(), "parentActivePart", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActiveScenario(), "activeScenario", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getActiveConstraint__UpdateConstraintEvent__ActiveScenario_SMLRuntimeState(), null, "updateConstraintEvent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActiveScenario(), "activeScenario", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSMLRuntimeState(), "smlRuntimeState", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getActiveConstraint__AddToParentSpecificConstraintList__ActiveInteraction_MessageEvent(), null, "addToParentSpecificConstraintList", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getActiveInteraction(), "parant", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "constraintMessage", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(activeConstraintConsiderEClass, ActiveConstraintConsider.class, "ActiveConstraintConsider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(activeConstraintIgnoreEClass, ActiveConstraintIgnore.class, "ActiveConstraintIgnore", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(activeConstraintInterruptEClass, ActiveConstraintInterrupt.class, "ActiveConstraintInterrupt", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(activeConstraintForbiddenEClass, ActiveConstraintForbidden.class, "ActiveConstraintForbidden", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(parameterRangesProviderEClass, ParameterRangesProvider.class, "ParameterRangesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getParameterRangesProvider__Init__Configuration(), null, "init", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theConfigurationPackage.getConfiguration(), "config", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getParameterRangesProvider__GetParameterValues__ETypedElement_SMLObjectSystem(), null, "getParameterValues", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eParameter", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSMLObjectSystem(), "smlObjectSystem", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getParameterValues());
		EGenericType g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getParameterRangesProvider__GetSingelParameterValue__Object(), null, "getSingelParameterValue", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "value", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getParameterValues());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getParameterRangesProvider__Init__EList(), null, "init", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "messageEvents", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getParameterRangesProvider__ContainsParameterValues__ETypedElement(), theEcorePackage.getEBoolean(), "containsParameterValues", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eParameter", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(messageEventBlockedInformationEClass, MessageEventBlockedInformation.class, "MessageEventBlockedInformation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMessageEventBlockedInformation_Description(), theEcorePackage.getEString(), "description", null, 0, 1, MessageEventBlockedInformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMessageEventBlockedInformation_MessageEventString(), ecorePackage.getEString(), "messageEventString", null, 0, 1, MessageEventBlockedInformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(multiActiveScenarioInitializationsEClass, MultiActiveScenarioInitializations.class, "MultiActiveScenarioInitializations", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMultiActiveScenarioInitializations_ActiveScenarioToActiveScenarioProgressMap(), this.getActiveScenarioToActiveScenarioProgressMapEntry(), null, "activeScenarioToActiveScenarioProgressMap", null, 0, -1, MultiActiveScenarioInitializations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(messageEventExtensionInterfaceEClass, MessageEventExtensionInterface.class, "MessageEventExtensionInterface", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getMessageEventExtensionInterface__Init__Configuration(), null, "init", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theConfigurationPackage.getConfiguration(), "runConfig", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(messageEventSideEffectsExecutorEClass, MessageEventSideEffectsExecutor.class, "MessageEventSideEffectsExecutor", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getMessageEventSideEffectsExecutor__ExecuteSideEffects__MessageEvent_DynamicObjectContainer(), null, "executeSideEffects", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDynamicObjectContainer(), "dynamicObjectContainer", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMessageEventSideEffectsExecutor__CanExecuteSideEffects__MessageEvent_DynamicObjectContainer(), theEcorePackage.getEBoolean(), "canExecuteSideEffects", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDynamicObjectContainer(), "dynamicObjectContainer", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(messageEventIsIndependentEvaluatorEClass, MessageEventIsIndependentEvaluator.class, "MessageEventIsIndependentEvaluator", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getMessageEventIsIndependentEvaluator__IsIndependent__MessageEvent_DynamicObjectContainer(), ecorePackage.getEBoolean(), "isIndependent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDynamicObjectContainer(), "dynamicObjectContainer", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(eClassToEObjectMapEntryEClass, Map.Entry.class, "EClassToEObjectMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEClassToEObjectMapEntry_Key(), theEcorePackage.getEClass(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEClassToEObjectMapEntry_Value(), theEcorePackage.getEObject(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(roleToEObjectMapEntryEClass, Map.Entry.class, "RoleToEObjectMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoleToEObjectMapEntry_Key(), theSmlPackage.getRole(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRoleToEObjectMapEntry_Value(), theEcorePackage.getEObject(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(staticEObjectToDynamicEObjectMapEntryEClass, Map.Entry.class, "StaticEObjectToDynamicEObjectMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStaticEObjectToDynamicEObjectMapEntry_Key(), theEcorePackage.getEObject(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStaticEObjectToDynamicEObjectMapEntry_Value(), theEcorePackage.getEObject(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activeScenarioToActiveScenarioProgressMapEntryEClass, Map.Entry.class, "ActiveScenarioToActiveScenarioProgressMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActiveScenarioToActiveScenarioProgressMapEntry_Key(), this.getActiveScenario(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActiveScenarioToActiveScenarioProgressMapEntry_Value(), this.getActiveScenarioProgress(), "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventToTransitionMapEntryEClass, Map.Entry.class, "EventToTransitionMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEventToTransitionMapEntry_Key(), this.getEvent(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEventToTransitionMapEntry_Value(), this.getTransition(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stringToBooleanMapEntryEClass, Map.Entry.class, "StringToBooleanMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringToBooleanMapEntry_Key(), ecorePackage.getEString(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStringToBooleanMapEntry_Value(), ecorePackage.getEBooleanObject(), "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stringToStringMapEntryEClass, Map.Entry.class, "StringToStringMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringToStringMapEntry_Key(), ecorePackage.getEString(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStringToStringMapEntry_Value(), ecorePackage.getEString(), "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stringToEObjectMapEntryEClass, Map.Entry.class, "StringToEObjectMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringToEObjectMapEntry_Key(), ecorePackage.getEString(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStringToEObjectMapEntry_Value(), theEcorePackage.getEObject(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activeInteractionKeyWrapperToActiveInteractionMapEntryEClass, Map.Entry.class, "ActiveInteractionKeyWrapperToActiveInteractionMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getActiveInteractionKeyWrapperToActiveInteractionMapEntry_Key(), this.getActiveInteractionKeyWrapper(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveInteractionKeyWrapperToActiveInteractionMapEntry_Value(), this.getActivePart(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activeScenarioKeyWrapperToActiveScenarioMapEntryEClass, Map.Entry.class, "ActiveScenarioKeyWrapperToActiveScenarioMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getActiveScenarioKeyWrapperToActiveScenarioMapEntry_Key(), this.getActiveScenarioKeyWrapper(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveScenarioKeyWrapperToActiveScenarioMapEntry_Value(), this.getActiveScenario(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(objectSystemKeyWrapperToObjectSystemMapEntryEClass, Map.Entry.class, "ObjectSystemKeyWrapperToObjectSystemMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getObjectSystemKeyWrapperToObjectSystemMapEntry_Key(), this.getObjectSystemKeyWrapper(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObjectSystemKeyWrapperToObjectSystemMapEntry_Value(), this.getSMLObjectSystem(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(objectSystemKeyWrapperToDynamicObjectContainerMapEntryEClass, Map.Entry.class, "ObjectSystemKeyWrapperToDynamicObjectContainerMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry_Key(), this.getObjectSystemKeyWrapper(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry_Value(), this.getDynamicObjectContainer(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stateKeyWrapperToStateMapEntryEClass, Map.Entry.class, "StateKeyWrapperToStateMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStateKeyWrapperToStateMapEntry_Key(), this.getStateKeyWrapper(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStateKeyWrapperToStateMapEntry_Value(), this.getSMLRuntimeState(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(variableToObjectMapEntryEClass, Map.Entry.class, "VariableToObjectMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVariableToObjectMapEntry_Key(), theScenarioExpressionsPackage.getVariable(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVariableToObjectMapEntry_Value(), theEcorePackage.getEJavaObject(), "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(variableToEObjectMapEntryEClass, Map.Entry.class, "VariableToEObjectMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVariableToEObjectMapEntry_Key(), theScenarioExpressionsPackage.getVariable(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVariableToEObjectMapEntry_Value(), theEcorePackage.getEObject(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activeScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryEClass, Map.Entry.class, "ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry_Key(), this.getActiveScenarioRoleBindingsKeyWrapper(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry_Value(), this.getActiveScenarioRoleBindings(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(violationKindEEnum, ViolationKind.class, "ViolationKind");
		addEEnumLiteral(violationKindEEnum, ViolationKind.NONE);
		addEEnumLiteral(violationKindEEnum, ViolationKind.COLD);
		addEEnumLiteral(violationKindEEnum, ViolationKind.SAFETY);

		initEEnum(activeScenarioProgressEEnum, ActiveScenarioProgress.class, "ActiveScenarioProgress");
		addEEnumLiteral(activeScenarioProgressEEnum, ActiveScenarioProgress.PROGRESS);
		addEEnumLiteral(activeScenarioProgressEEnum, ActiveScenarioProgress.NO_PROGRESS);
		addEEnumLiteral(activeScenarioProgressEEnum, ActiveScenarioProgress.INTERACTION_END);
		addEEnumLiteral(activeScenarioProgressEEnum, ActiveScenarioProgress.COLD_VIOLATION);
		addEEnumLiteral(activeScenarioProgressEEnum, ActiveScenarioProgress.SAFETY_VIOLATION);
		addEEnumLiteral(activeScenarioProgressEEnum, ActiveScenarioProgress.CONTINUE);
		addEEnumLiteral(activeScenarioProgressEEnum, ActiveScenarioProgress.STEP_OVER);
		addEEnumLiteral(activeScenarioProgressEEnum, ActiveScenarioProgress.MESSAGE_PROGRESSED);

		initEEnum(blockedTypeEEnum, BlockedType.class, "BlockedType");
		addEEnumLiteral(blockedTypeEEnum, BlockedType.ENABLED);
		addEEnumLiteral(blockedTypeEEnum, BlockedType.CUT_NOT_STRICT);
		addEEnumLiteral(blockedTypeEEnum, BlockedType.FORBIDDEN);
		addEEnumLiteral(blockedTypeEEnum, BlockedType.IGNORE);
		addEEnumLiteral(blockedTypeEEnum, BlockedType.CUT_STRICT);
		addEEnumLiteral(blockedTypeEEnum, BlockedType.STRICT_CONSIDERED);
		addEEnumLiteral(blockedTypeEEnum, BlockedType.BLOCKED);
		addEEnumLiteral(blockedTypeEEnum, BlockedType.INTERRUPTED);

		// Initialize data types
		initEDataType(parameterValuesEDataType, ParameterValues.class, "ParameterValues", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(activeInteractionKeyWrapperEDataType, ActiveInteractionKeyWrapper.class, "ActiveInteractionKeyWrapper", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(activeScenarioKeyWrapperEDataType, ActiveScenarioKeyWrapper.class, "ActiveScenarioKeyWrapper", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(objectSystemKeyWrapperEDataType, ObjectSystemKeyWrapper.class, "ObjectSystemKeyWrapper", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(stateKeyWrapperEDataType, StateKeyWrapper.class, "StateKeyWrapper", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(activeScenarioRoleBindingsKeyWrapperEDataType, ActiveScenarioRoleBindingsKeyWrapper.class, "ActiveScenarioRoleBindingsKeyWrapper", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //RuntimePackageImpl
