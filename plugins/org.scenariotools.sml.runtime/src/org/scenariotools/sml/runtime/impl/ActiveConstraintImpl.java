/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.Message;
import org.scenariotools.sml.runtime.ActiveInteraction;
import org.scenariotools.sml.runtime.ActiveMessageParameter;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.RuntimePackage;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.logic.ActiveConstraintLogic;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActiveConstraintImpl#getConstraintMessageEvent <em>Constraint Message Event</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActiveConstraintImpl#getInteractionFragment <em>Interaction Fragment</em>}</li>
 * </ul>
 * </p>
 *
 * @generated PROTECT_DECLARATION 
 * (generated NOT would protect all the content of the class)
 */
public abstract class ActiveConstraintImpl extends ActiveConstraintLogic {
	/**
	 * The cached value of the '{@link #getConstraintMessageEvent() <em>Constraint Message Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraintMessageEvent()
	 * @generated
	 * @ordered
	 */
	protected MessageEvent constraintMessageEvent;

	/**
	 * The cached value of the '{@link #getActiveMessageParameters() <em>Active Message Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActiveMessageParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<ActiveMessageParameter> activeMessageParameters;

	/**
	 * The cached value of the '{@link #getMessage() <em>Message</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage()
	 * @generated
	 * @ordered
	 */
	protected Message message;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActiveConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ACTIVE_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageEvent getConstraintMessageEvent() {
		if (constraintMessageEvent != null && constraintMessageEvent.eIsProxy()) {
			InternalEObject oldConstraintMessageEvent = (InternalEObject)constraintMessageEvent;
			constraintMessageEvent = (MessageEvent)eResolveProxy(oldConstraintMessageEvent);
			if (constraintMessageEvent != oldConstraintMessageEvent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.ACTIVE_CONSTRAINT__CONSTRAINT_MESSAGE_EVENT, oldConstraintMessageEvent, constraintMessageEvent));
			}
		}
		return constraintMessageEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageEvent basicGetConstraintMessageEvent() {
		return constraintMessageEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstraintMessageEvent(MessageEvent newConstraintMessageEvent) {
		MessageEvent oldConstraintMessageEvent = constraintMessageEvent;
		constraintMessageEvent = newConstraintMessageEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_CONSTRAINT__CONSTRAINT_MESSAGE_EVENT, oldConstraintMessageEvent, constraintMessageEvent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActiveMessageParameter> getActiveMessageParameters() {
		if (activeMessageParameters == null) {
			activeMessageParameters = new EObjectContainmentEList<ActiveMessageParameter>(ActiveMessageParameter.class, this, RuntimePackage.ACTIVE_CONSTRAINT__ACTIVE_MESSAGE_PARAMETERS);
		}
		return activeMessageParameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getMessage() {
		if (message != null && message.eIsProxy()) {
			InternalEObject oldMessage = (InternalEObject)message;
			message = (Message)eResolveProxy(oldMessage);
			if (message != oldMessage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.ACTIVE_CONSTRAINT__MESSAGE, oldMessage, message));
			}
		}
		return message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message basicGetMessage() {
		return message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMessage(Message newMessage) {
		Message oldMessage = message;
		message = newMessage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_CONSTRAINT__MESSAGE, oldMessage, message));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void init(ActiveScenarioRoleBindings roleBindings, ActivePart parentActivePart, ActiveScenario activeScenario) {
		super.init(roleBindings, parentActivePart, activeScenario);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void updateConstraintEvent(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		super.updateConstraintEvent(activeScenario, smlRuntimeState);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public abstract void addToParentSpecificConstraintList(ActiveInteraction parant, MessageEvent constraintMessage);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_CONSTRAINT__ACTIVE_MESSAGE_PARAMETERS:
				return ((InternalEList<?>)getActiveMessageParameters()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_CONSTRAINT__CONSTRAINT_MESSAGE_EVENT:
				if (resolve) return getConstraintMessageEvent();
				return basicGetConstraintMessageEvent();
			case RuntimePackage.ACTIVE_CONSTRAINT__ACTIVE_MESSAGE_PARAMETERS:
				return getActiveMessageParameters();
			case RuntimePackage.ACTIVE_CONSTRAINT__MESSAGE:
				if (resolve) return getMessage();
				return basicGetMessage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_CONSTRAINT__CONSTRAINT_MESSAGE_EVENT:
				setConstraintMessageEvent((MessageEvent)newValue);
				return;
			case RuntimePackage.ACTIVE_CONSTRAINT__ACTIVE_MESSAGE_PARAMETERS:
				getActiveMessageParameters().clear();
				getActiveMessageParameters().addAll((Collection<? extends ActiveMessageParameter>)newValue);
				return;
			case RuntimePackage.ACTIVE_CONSTRAINT__MESSAGE:
				setMessage((Message)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_CONSTRAINT__CONSTRAINT_MESSAGE_EVENT:
				setConstraintMessageEvent((MessageEvent)null);
				return;
			case RuntimePackage.ACTIVE_CONSTRAINT__ACTIVE_MESSAGE_PARAMETERS:
				getActiveMessageParameters().clear();
				return;
			case RuntimePackage.ACTIVE_CONSTRAINT__MESSAGE:
				setMessage((Message)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_CONSTRAINT__CONSTRAINT_MESSAGE_EVENT:
				return constraintMessageEvent != null;
			case RuntimePackage.ACTIVE_CONSTRAINT__ACTIVE_MESSAGE_PARAMETERS:
				return activeMessageParameters != null && !activeMessageParameters.isEmpty();
			case RuntimePackage.ACTIVE_CONSTRAINT__MESSAGE:
				return message != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.ACTIVE_CONSTRAINT___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO:
				init((ActiveScenarioRoleBindings)arguments.get(0), (ActivePart)arguments.get(1), (ActiveScenario)arguments.get(2));
				return null;
			case RuntimePackage.ACTIVE_CONSTRAINT___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE:
				updateConstraintEvent((ActiveScenario)arguments.get(0), (SMLRuntimeState)arguments.get(1));
				return null;
			case RuntimePackage.ACTIVE_CONSTRAINT___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT:
				addToParentSpecificConstraintList((ActiveInteraction)arguments.get(0), (MessageEvent)arguments.get(1));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //ActiveConstraintImpl
