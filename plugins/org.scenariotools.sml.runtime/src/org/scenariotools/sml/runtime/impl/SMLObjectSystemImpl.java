/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.Specification;
import org.scenariotools.sml.runtime.DynamicObjectContainer;
import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator;
import org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor;
import org.scenariotools.sml.runtime.RuntimePackage;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.WaitEvent;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.runtime.logic.SMLObjectSystemLogic;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SML Object System</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.impl.SMLObjectSystemImpl#getEClassToEObject <em>EClass To EObject</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.SMLObjectSystemImpl#getInitializingEnvironmentMessageEvents <em>Initializing Environment Message Events</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.SMLObjectSystemImpl#getInitializingSystemMessageEvents <em>Initializing System Message Events</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.SMLObjectSystemImpl#getStaticRoleBindings <em>Static Role Bindings</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.SMLObjectSystemImpl#getSpecification <em>Specification</em>}</li>
 * </ul>
 * </p>
 *
 * @generated PROTECT_DECLARATION 
 * (generated NOT would protect all the content of the class)
 */
public class SMLObjectSystemImpl extends SMLObjectSystemLogic {
	/**
	 * The cached value of the '{@link #getEClassToEObject() <em>EClass To EObject</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEClassToEObject()
	 * @generated
	 * @ordered
	 */
	protected EMap<EClass, EObject> eClassToEObject;

	/**
	 * The cached value of the '{@link #getStaticRoleBindings() <em>Static Role Bindings</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStaticRoleBindings()
	 * @generated
	 * @ordered
	 */
	protected EMap<Role, EObject> staticRoleBindings;

	/**
	 * The cached value of the '{@link #getRunconfiguration() <em>Runconfiguration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRunconfiguration()
	 * @generated
	 * @ordered
	 */
	protected Configuration runconfiguration;

	/**
	 * The cached value of the '{@link #getMessageEventSideEffectsExecutor() <em>Message Event Side Effects Executor</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageEventSideEffectsExecutor()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageEventSideEffectsExecutor> messageEventSideEffectsExecutor;

	/**
	 * The cached value of the '{@link #getMessageEventIsIndependentEvaluators() <em>Message Event Is Independent Evaluators</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageEventIsIndependentEvaluators()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageEventIsIndependentEvaluator> messageEventIsIndependentEvaluators;

	/**
	 * The cached value of the '{@link #getObjects() <em>Objects</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> objects;

	/**
	 * The cached value of the '{@link #getControllableObjects() <em>Controllable Objects</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getControllableObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> controllableObjects;

	/**
	 * The cached value of the '{@link #getUncontrollableObjects() <em>Uncontrollable Objects</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUncontrollableObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> uncontrollableObjects;

	/**
	 * The cached value of the '{@link #getRootObjects() <em>Root Objects</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> rootObjects;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SMLObjectSystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.SML_OBJECT_SYSTEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EClass, EObject> getEClassToEObject() {
		if (eClassToEObject == null) {
			eClassToEObject = new EcoreEMap<EClass,EObject>(RuntimePackage.Literals.ECLASS_TO_EOBJECT_MAP_ENTRY, EClassToEObjectMapEntryImpl.class, this, RuntimePackage.SML_OBJECT_SYSTEM__ECLASS_TO_EOBJECT);
		}
		return eClassToEObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<MessageEvent> getInitializingEnvironmentMessageEvents(DynamicObjectContainer dynamicObjectProvider) {
		return super.getInitializingEnvironmentMessageEvents(dynamicObjectProvider);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isControllable(EObject eObject) {
		return getControllableObjects().contains(eObject);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isEnvironmentMessageEvent(Event event) {
		if(event instanceof WaitEvent)
			return false;
		else
			return getUncontrollableObjects().contains(((MessageEvent)event).getSendingObject());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean containsEObject(EObject eObject) {
		for (EObject rootEObject : getRootObjects()) {
			if (eObject == rootEObject) return true;
			TreeIterator<EObject> eAllContentsIterator = rootEObject.eAllContents();
			while(eAllContentsIterator.hasNext()) {
				if (eObject == eAllContentsIterator.next())
					return true;
			}
		}
		return false;	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Role, EObject> getStaticRoleBindings() {
		if (staticRoleBindings == null) {
			staticRoleBindings = new EcoreEMap<Role,EObject>(RuntimePackage.Literals.ROLE_TO_EOBJECT_MAP_ENTRY, RoleToEObjectMapEntryImpl.class, this, RuntimePackage.SML_OBJECT_SYSTEM__STATIC_ROLE_BINDINGS);
		}
		return staticRoleBindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Specification getSpecification() {
		Specification specification = basicGetSpecification();
		return specification != null && specification.eIsProxy() ? (Specification)eResolveProxy((InternalEObject)specification) : specification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Specification basicGetSpecification() {
		return getRunconfiguration().getSpecification();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Configuration getRunconfiguration() {
		if (runconfiguration != null && runconfiguration.eIsProxy()) {
			InternalEObject oldRunconfiguration = (InternalEObject)runconfiguration;
			runconfiguration = (Configuration)eResolveProxy(oldRunconfiguration);
			if (runconfiguration != oldRunconfiguration) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.SML_OBJECT_SYSTEM__RUNCONFIGURATION, oldRunconfiguration, runconfiguration));
			}
		}
		return runconfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Configuration basicGetRunconfiguration() {
		return runconfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRunconfiguration(Configuration newRunconfiguration) {
		Configuration oldRunconfiguration = runconfiguration;
		runconfiguration = newRunconfiguration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.SML_OBJECT_SYSTEM__RUNCONFIGURATION, oldRunconfiguration, runconfiguration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageEventSideEffectsExecutor> getMessageEventSideEffectsExecutor() {
		if (messageEventSideEffectsExecutor == null) {
			messageEventSideEffectsExecutor = new EObjectResolvingEList<MessageEventSideEffectsExecutor>(MessageEventSideEffectsExecutor.class, this, RuntimePackage.SML_OBJECT_SYSTEM__MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR);
		}
		return messageEventSideEffectsExecutor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageEventIsIndependentEvaluator> getMessageEventIsIndependentEvaluators() {
		if (messageEventIsIndependentEvaluators == null) {
			messageEventIsIndependentEvaluators = new EObjectResolvingEList<MessageEventIsIndependentEvaluator>(MessageEventIsIndependentEvaluator.class, this, RuntimePackage.SML_OBJECT_SYSTEM__MESSAGE_EVENT_IS_INDEPENDENT_EVALUATORS);
		}
		return messageEventIsIndependentEvaluators;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getObjects() {
		if (objects == null) {
			objects = new EObjectResolvingEList<EObject>(EObject.class, this, RuntimePackage.SML_OBJECT_SYSTEM__OBJECTS);
		}
		return objects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getControllableObjects() {
		if (controllableObjects == null) {
			controllableObjects = new EObjectResolvingEList<EObject>(EObject.class, this, RuntimePackage.SML_OBJECT_SYSTEM__CONTROLLABLE_OBJECTS);
		}
		return controllableObjects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getUncontrollableObjects() {
		if (uncontrollableObjects == null) {
			uncontrollableObjects = new EObjectResolvingEList<EObject>(EObject.class, this, RuntimePackage.SML_OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS);
		}
		return uncontrollableObjects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getRootObjects() {
		if (rootObjects == null) {
			rootObjects = new EObjectResolvingEList<EObject>(EObject.class, this, RuntimePackage.SML_OBJECT_SYSTEM__ROOT_OBJECTS);
		}
		return rootObjects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void init(Configuration runConfiguration, SMLRuntimeState smlRuntimeState) {
		super.init(runConfiguration, smlRuntimeState);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void executeSideEffects(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer) {
		super.executeSideEffects(messageEvent, dynamicObjectContainer);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean canExecuteSideEffects(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer) {
		return super.canExecuteSideEffects(messageEvent, dynamicObjectContainer);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isIndependent(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer) {
		return super.isIndependent(messageEvent, dynamicObjectContainer);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isNonSpontaneousMessageEvent(MessageEvent messageEvent) {
		return super.isNonSpontaneousMessageEvent(messageEvent);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Scenario> getScenariosForInitMessageEvent(MessageEvent initMessageEvent) {
		return super.getScenariosForInitMessageEvent(initMessageEvent);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.SML_OBJECT_SYSTEM__ECLASS_TO_EOBJECT:
				return ((InternalEList<?>)getEClassToEObject()).basicRemove(otherEnd, msgs);
			case RuntimePackage.SML_OBJECT_SYSTEM__STATIC_ROLE_BINDINGS:
				return ((InternalEList<?>)getStaticRoleBindings()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.SML_OBJECT_SYSTEM__ECLASS_TO_EOBJECT:
				if (coreType) return getEClassToEObject();
				else return getEClassToEObject().map();
			case RuntimePackage.SML_OBJECT_SYSTEM__STATIC_ROLE_BINDINGS:
				if (coreType) return getStaticRoleBindings();
				else return getStaticRoleBindings().map();
			case RuntimePackage.SML_OBJECT_SYSTEM__SPECIFICATION:
				if (resolve) return getSpecification();
				return basicGetSpecification();
			case RuntimePackage.SML_OBJECT_SYSTEM__RUNCONFIGURATION:
				if (resolve) return getRunconfiguration();
				return basicGetRunconfiguration();
			case RuntimePackage.SML_OBJECT_SYSTEM__MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR:
				return getMessageEventSideEffectsExecutor();
			case RuntimePackage.SML_OBJECT_SYSTEM__MESSAGE_EVENT_IS_INDEPENDENT_EVALUATORS:
				return getMessageEventIsIndependentEvaluators();
			case RuntimePackage.SML_OBJECT_SYSTEM__OBJECTS:
				return getObjects();
			case RuntimePackage.SML_OBJECT_SYSTEM__CONTROLLABLE_OBJECTS:
				return getControllableObjects();
			case RuntimePackage.SML_OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS:
				return getUncontrollableObjects();
			case RuntimePackage.SML_OBJECT_SYSTEM__ROOT_OBJECTS:
				return getRootObjects();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.SML_OBJECT_SYSTEM__ECLASS_TO_EOBJECT:
				((EStructuralFeature.Setting)getEClassToEObject()).set(newValue);
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__STATIC_ROLE_BINDINGS:
				((EStructuralFeature.Setting)getStaticRoleBindings()).set(newValue);
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__RUNCONFIGURATION:
				setRunconfiguration((Configuration)newValue);
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR:
				getMessageEventSideEffectsExecutor().clear();
				getMessageEventSideEffectsExecutor().addAll((Collection<? extends MessageEventSideEffectsExecutor>)newValue);
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__MESSAGE_EVENT_IS_INDEPENDENT_EVALUATORS:
				getMessageEventIsIndependentEvaluators().clear();
				getMessageEventIsIndependentEvaluators().addAll((Collection<? extends MessageEventIsIndependentEvaluator>)newValue);
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__OBJECTS:
				getObjects().clear();
				getObjects().addAll((Collection<? extends EObject>)newValue);
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__CONTROLLABLE_OBJECTS:
				getControllableObjects().clear();
				getControllableObjects().addAll((Collection<? extends EObject>)newValue);
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS:
				getUncontrollableObjects().clear();
				getUncontrollableObjects().addAll((Collection<? extends EObject>)newValue);
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__ROOT_OBJECTS:
				getRootObjects().clear();
				getRootObjects().addAll((Collection<? extends EObject>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.SML_OBJECT_SYSTEM__ECLASS_TO_EOBJECT:
				getEClassToEObject().clear();
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__STATIC_ROLE_BINDINGS:
				getStaticRoleBindings().clear();
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__RUNCONFIGURATION:
				setRunconfiguration((Configuration)null);
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR:
				getMessageEventSideEffectsExecutor().clear();
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__MESSAGE_EVENT_IS_INDEPENDENT_EVALUATORS:
				getMessageEventIsIndependentEvaluators().clear();
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__OBJECTS:
				getObjects().clear();
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__CONTROLLABLE_OBJECTS:
				getControllableObjects().clear();
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS:
				getUncontrollableObjects().clear();
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__ROOT_OBJECTS:
				getRootObjects().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.SML_OBJECT_SYSTEM__ECLASS_TO_EOBJECT:
				return eClassToEObject != null && !eClassToEObject.isEmpty();
			case RuntimePackage.SML_OBJECT_SYSTEM__STATIC_ROLE_BINDINGS:
				return staticRoleBindings != null && !staticRoleBindings.isEmpty();
			case RuntimePackage.SML_OBJECT_SYSTEM__SPECIFICATION:
				return basicGetSpecification() != null;
			case RuntimePackage.SML_OBJECT_SYSTEM__RUNCONFIGURATION:
				return runconfiguration != null;
			case RuntimePackage.SML_OBJECT_SYSTEM__MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR:
				return messageEventSideEffectsExecutor != null && !messageEventSideEffectsExecutor.isEmpty();
			case RuntimePackage.SML_OBJECT_SYSTEM__MESSAGE_EVENT_IS_INDEPENDENT_EVALUATORS:
				return messageEventIsIndependentEvaluators != null && !messageEventIsIndependentEvaluators.isEmpty();
			case RuntimePackage.SML_OBJECT_SYSTEM__OBJECTS:
				return objects != null && !objects.isEmpty();
			case RuntimePackage.SML_OBJECT_SYSTEM__CONTROLLABLE_OBJECTS:
				return controllableObjects != null && !controllableObjects.isEmpty();
			case RuntimePackage.SML_OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS:
				return uncontrollableObjects != null && !uncontrollableObjects.isEmpty();
			case RuntimePackage.SML_OBJECT_SYSTEM__ROOT_OBJECTS:
				return rootObjects != null && !rootObjects.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.SML_OBJECT_SYSTEM___INIT__CONFIGURATION_SMLRUNTIMESTATE:
				init((Configuration)arguments.get(0), (SMLRuntimeState)arguments.get(1));
				return null;
			case RuntimePackage.SML_OBJECT_SYSTEM___EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER:
				executeSideEffects((MessageEvent)arguments.get(0), (DynamicObjectContainer)arguments.get(1));
				return null;
			case RuntimePackage.SML_OBJECT_SYSTEM___CAN_EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER:
				return canExecuteSideEffects((MessageEvent)arguments.get(0), (DynamicObjectContainer)arguments.get(1));
			case RuntimePackage.SML_OBJECT_SYSTEM___IS_INDEPENDENT__MESSAGEEVENT_DYNAMICOBJECTCONTAINER:
				return isIndependent((MessageEvent)arguments.get(0), (DynamicObjectContainer)arguments.get(1));
			case RuntimePackage.SML_OBJECT_SYSTEM___IS_NON_SPONTANEOUS_MESSAGE_EVENT__MESSAGEEVENT:
				return isNonSpontaneousMessageEvent((MessageEvent)arguments.get(0));
			case RuntimePackage.SML_OBJECT_SYSTEM___GET_SCENARIOS_FOR_INIT_MESSAGE_EVENT__MESSAGEEVENT:
				return getScenariosForInitMessageEvent((MessageEvent)arguments.get(0));
			case RuntimePackage.SML_OBJECT_SYSTEM___GET_INITIALIZING_ENVIRONMENT_MESSAGE_EVENTS__DYNAMICOBJECTCONTAINER:
				return getInitializingEnvironmentMessageEvents((DynamicObjectContainer)arguments.get(0));
			case RuntimePackage.SML_OBJECT_SYSTEM___IS_CONTROLLABLE__EOBJECT:
				return isControllable((EObject)arguments.get(0));
			case RuntimePackage.SML_OBJECT_SYSTEM___IS_ENVIRONMENT_MESSAGE_EVENT__EVENT:
				return isEnvironmentMessageEvent((Event)arguments.get(0));
			case RuntimePackage.SML_OBJECT_SYSTEM___CONTAINS_EOBJECT__EOBJECT:
				return containsEObject((EObject)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //SMLObjectSystemImpl
