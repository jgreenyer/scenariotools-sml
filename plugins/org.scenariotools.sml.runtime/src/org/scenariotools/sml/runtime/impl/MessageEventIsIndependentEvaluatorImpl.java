/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.scenariotools.sml.runtime.DynamicObjectContainer;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator;
import org.scenariotools.sml.runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Message Event Is Independent Evaluator</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class MessageEventIsIndependentEvaluatorImpl extends MessageEventExtensionInterfaceImpl implements MessageEventIsIndependentEvaluator {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MessageEventIsIndependentEvaluatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIndependent(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR___IS_INDEPENDENT__MESSAGEEVENT_DYNAMICOBJECTCONTAINER:
				return isIndependent((MessageEvent)arguments.get(0), (DynamicObjectContainer)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //MessageEventIsIndependentEvaluatorImpl
