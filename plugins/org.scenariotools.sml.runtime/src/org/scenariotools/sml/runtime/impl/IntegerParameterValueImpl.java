/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.scenariotools.sml.runtime.ParameterValue;
import org.scenariotools.sml.runtime.IntegerParameterValue;
import org.scenariotools.sml.runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Integer Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.impl.IntegerParameterValueImpl#getIntegerParameterValue <em>Integer Parameter Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IntegerParameterValueImpl extends ParameterValueImpl implements IntegerParameterValue {
	/**
	 * The default value of the '{@link #getIntegerParameterValue() <em>Integer Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerParameterValue()
	 * @generated
	 * @ordered
	 */
	protected static final int INTEGER_PARAMETER_VALUE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getIntegerParameterValue() <em>Integer Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerParameterValue()
	 * @generated
	 * @ordered
	 */
	protected int integerParameterValue = INTEGER_PARAMETER_VALUE_EDEFAULT;

	/**
	 * This is true if the Integer Parameter Value attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean integerParameterValueESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntegerParameterValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.INTEGER_PARAMETER_VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getIntegerParameterValue() {
		return integerParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegerParameterValue(int newIntegerParameterValue) {
		int oldIntegerParameterValue = integerParameterValue;
		integerParameterValue = newIntegerParameterValue;
		boolean oldIntegerParameterValueESet = integerParameterValueESet;
		integerParameterValueESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.INTEGER_PARAMETER_VALUE__INTEGER_PARAMETER_VALUE, oldIntegerParameterValue, integerParameterValue, !oldIntegerParameterValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIntegerParameterValue() {
		int oldIntegerParameterValue = integerParameterValue;
		boolean oldIntegerParameterValueESet = integerParameterValueESet;
		integerParameterValue = INTEGER_PARAMETER_VALUE_EDEFAULT;
		integerParameterValueESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, RuntimePackage.INTEGER_PARAMETER_VALUE__INTEGER_PARAMETER_VALUE, oldIntegerParameterValue, INTEGER_PARAMETER_VALUE_EDEFAULT, oldIntegerParameterValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIntegerParameterValue() {
		return integerParameterValueESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.INTEGER_PARAMETER_VALUE__INTEGER_PARAMETER_VALUE:
				return getIntegerParameterValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.INTEGER_PARAMETER_VALUE__INTEGER_PARAMETER_VALUE:
				setIntegerParameterValue((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.INTEGER_PARAMETER_VALUE__INTEGER_PARAMETER_VALUE:
				unsetIntegerParameterValue();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.INTEGER_PARAMETER_VALUE__INTEGER_PARAMETER_VALUE:
				return isSetIntegerParameterValue();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();
		if(isWildcardParameter() && isUnset()) return "*";
		if(isUnset()) return "<unset>";
		
		try {
			return Integer.toString(getIntegerParameterValue());
//			return getEParameter().getName() + "=" + getIntegerParameterValue();
		} catch (Exception e) {
			StringBuffer result = new StringBuffer(super.toString());
			result.append(" (integerParameterValue: ");
			if (integerParameterValueESet) result.append(integerParameterValue); else result.append("<unset>");
			result.append(')');
			return result.toString();
		}
		
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public void setValue(Object value) {
		setIntegerParameterValue((int) value);
		setUnset(false);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public Object getValue() {
		// TODO Auto-generated method stub
		return getIntegerParameterValue();
	}
	
	/**
	 * Checks for parameter equality.
	 * @generated NOT
	 */
	@Override
	public boolean eq(ParameterValue otherParameterValue) {
		if(otherParameterValue.isWildcardParameter() && this.isWildcardParameter()){
			// both parameter are wildcard parameter
			// check for type
			if(otherParameterValue instanceof IntegerParameterValue){
				return true;
			}else{
				return false;
			}
		}else if(otherParameterValue.isWildcardParameter() || this.isWildcardParameter()){
			// wildcard parameter and concrete parameter are always different
			return false;
		}else{
			// check for parameter values (int == int)
			return this.getValue().equals(otherParameterValue.getValue());
		}
	}
	
	/**
	 * Checks if parameter is unifiable.
	 * @generated NOT
	 */
	@Override
	public boolean isParameterUnifiableWith(ParameterValue otherParameterValue) {
		if(otherParameterValue.isWildcardParameter() || this.isWildcardParameter()){
			// wildcard parameter
			// check for type
			if(otherParameterValue instanceof IntegerParameterValue){
				return true;
			}else{
				return false;
			}
		}else{
			// check for bool == bool
			return this.getValue().equals(otherParameterValue.getValue());
		}
	}


} //IntegerParameterValueImpl
