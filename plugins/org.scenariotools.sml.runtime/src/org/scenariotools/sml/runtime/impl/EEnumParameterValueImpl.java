/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.scenariotools.sml.runtime.ParameterValue;
import org.scenariotools.sml.runtime.EEnumParameterValue;
import org.scenariotools.sml.runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EEnum Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.impl.EEnumParameterValueImpl#getEEnumParameterType <em>EEnum Parameter Type</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.EEnumParameterValueImpl#getEEnumParameterValue <em>EEnum Parameter Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EEnumParameterValueImpl extends ParameterValueImpl implements EEnumParameterValue {
	/**
	 * The cached value of the '{@link #getEEnumParameterType() <em>EEnum Parameter Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEEnumParameterType()
	 * @generated
	 * @ordered
	 */
	protected EEnum eEnumParameterType;

	/**
	 * The cached value of the '{@link #getEEnumParameterValue() <em>EEnum Parameter Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEEnumParameterValue()
	 * @generated
	 * @ordered
	 */
	protected EEnumLiteral eEnumParameterValue;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EEnumParameterValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.EENUM_PARAMETER_VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getEEnumParameterType() {
		if (eEnumParameterType != null && eEnumParameterType.eIsProxy()) {
			InternalEObject oldEEnumParameterType = (InternalEObject)eEnumParameterType;
			eEnumParameterType = (EEnum)eResolveProxy(oldEEnumParameterType);
			if (eEnumParameterType != oldEEnumParameterType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_TYPE, oldEEnumParameterType, eEnumParameterType));
			}
		}
		return eEnumParameterType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum basicGetEEnumParameterType() {
		return eEnumParameterType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEEnumParameterType(EEnum newEEnumParameterType) {
		EEnum oldEEnumParameterType = eEnumParameterType;
		eEnumParameterType = newEEnumParameterType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_TYPE, oldEEnumParameterType, eEnumParameterType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnumLiteral getEEnumParameterValue() {
		if (eEnumParameterValue != null && eEnumParameterValue.eIsProxy()) {
			InternalEObject oldEEnumParameterValue = (InternalEObject)eEnumParameterValue;
			eEnumParameterValue = (EEnumLiteral)eResolveProxy(oldEEnumParameterValue);
			if (eEnumParameterValue != oldEEnumParameterValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_VALUE, oldEEnumParameterValue, eEnumParameterValue));
			}
		}
		return eEnumParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnumLiteral basicGetEEnumParameterValue() {
		return eEnumParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEEnumParameterValue(EEnumLiteral newEEnumParameterValue) {
		EEnumLiteral oldEEnumParameterValue = eEnumParameterValue;
		eEnumParameterValue = newEEnumParameterValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_VALUE, oldEEnumParameterValue, eEnumParameterValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_TYPE:
				if (resolve) return getEEnumParameterType();
				return basicGetEEnumParameterType();
			case RuntimePackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_VALUE:
				if (resolve) return getEEnumParameterValue();
				return basicGetEEnumParameterValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_TYPE:
				setEEnumParameterType((EEnum)newValue);
				return;
			case RuntimePackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_VALUE:
				setEEnumParameterValue((EEnumLiteral)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_TYPE:
				setEEnumParameterType((EEnum)null);
				return;
			case RuntimePackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_VALUE:
				setEEnumParameterValue((EEnumLiteral)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_TYPE:
				return eEnumParameterType != null;
			case RuntimePackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_VALUE:
				return eEnumParameterValue != null;
		}
		return super.eIsSet(featureID);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void setValue(Object value) {
		EEnumLiteral other = (EEnumLiteral)value;
		setEEnumParameterType((EEnum)other.eContainer());
		setEEnumParameterValue(other);
		setUnset(false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Object getValue() {
		return getEEnumParameterValue();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public boolean eq(ParameterValue otherParameterValue) {
		EEnumParameterValue other = (EEnumParameterValue)otherParameterValue;
		
		if (getEEnumParameterType() != other.getEEnumParameterType())
			return false;
		
		return getEEnumParameterValue() == other.getEEnumParameterValue();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public boolean isParameterUnifiableWith(ParameterValue otherParameterValue) {
		if(otherParameterValue.isWildcardParameter() || this.isWildcardParameter()){
			// wildcard parameter
			// check for type
			if(otherParameterValue instanceof EEnumParameterValue){
				return true;
			}else{
				return false;
			}
		}else{
			// check for bool == bool
			return this.getValue().equals(otherParameterValue.getValue());
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();
		
		if(this.isWildcardParameter()) return "*";

		EEnum type = getEEnumParameterType();
		EEnumLiteral value = getEEnumParameterValue();
		return (type == null ? "<unknown enum type>": type.getName())
				+ ":" +
				(value == null ? "<unknown value type>" : value.toString());
	}
} //EEnumParameterValueImpl
