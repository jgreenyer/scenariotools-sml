/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.scenariotools.sml.runtime.MessageEventExtensionInterface;
import org.scenariotools.sml.runtime.RuntimePackage;

import org.scenariotools.sml.runtime.configuration.Configuration;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Message Event Extension Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class MessageEventExtensionInterfaceImpl extends MinimalEObjectImpl.Container implements MessageEventExtensionInterface {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MessageEventExtensionInterfaceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.MESSAGE_EVENT_EXTENSION_INTERFACE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void init(Configuration runConfig) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.MESSAGE_EVENT_EXTENSION_INTERFACE___INIT__CONFIGURATION:
				init((Configuration)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //MessageEventExtensionInterfaceImpl
