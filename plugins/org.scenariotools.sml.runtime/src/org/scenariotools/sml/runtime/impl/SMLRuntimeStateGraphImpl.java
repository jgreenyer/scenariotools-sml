/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.sml.runtime.ElementContainer;
import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.runtime.ParameterRangesProvider;
import org.scenariotools.sml.runtime.RuntimePackage;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.runtime.logic.SMLRuntimeStateGraphLogic;
import org.scenariotools.sml.runtime.Transition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SML Runtime State Graph</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.impl.SMLRuntimeStateGraphImpl#getConfiguration <em>Configuration</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.SMLRuntimeStateGraphImpl#getElementContainer <em>Element Container</em>}</li>
 * </ul>
 * </p>
 *
 * @generated PROTECT_DECLARATION 
 * (generated NOT would protect all the content of the class)
 */
public class SMLRuntimeStateGraphImpl extends SMLRuntimeStateGraphLogic {
	/**
	 * The cached value of the '{@link #getConfiguration() <em>Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfiguration()
	 * @generated
	 * @ordered
	 */
	protected Configuration configuration;
	/**
	 * The cached value of the '{@link #getElementContainer() <em>Element Container</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementContainer()
	 * @generated
	 * @ordered
	 */
	protected ElementContainer elementContainer;
	/**
	 * The cached value of the '{@link #getParameterRangesProvider() <em>Parameter Ranges Provider</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterRangesProvider()
	 * @generated
	 * @ordered
	 */
	protected ParameterRangesProvider parameterRangesProvider;
	/**
	 * The cached value of the '{@link #getStates() <em>States</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStates()
	 * @generated
	 * @ordered
	 */
	protected EList<SMLRuntimeState> states;
	/**
	 * The cached value of the '{@link #getStartState() <em>Start State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartState()
	 * @generated
	 * @ordered
	 */
	protected SMLRuntimeState startState;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SMLRuntimeStateGraphImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.SML_RUNTIME_STATE_GRAPH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Configuration getConfiguration() {
		if (configuration != null && configuration.eIsProxy()) {
			InternalEObject oldConfiguration = (InternalEObject)configuration;
			configuration = (Configuration)eResolveProxy(oldConfiguration);
			if (configuration != oldConfiguration) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.SML_RUNTIME_STATE_GRAPH__CONFIGURATION, oldConfiguration, configuration));
			}
		}
		return configuration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Configuration basicGetConfiguration() {
		return configuration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfiguration(Configuration newConfiguration) {
		Configuration oldConfiguration = configuration;
		configuration = newConfiguration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.SML_RUNTIME_STATE_GRAPH__CONFIGURATION, oldConfiguration, configuration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementContainer getElementContainer() {
		return elementContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElementContainer(ElementContainer newElementContainer, NotificationChain msgs) {
		ElementContainer oldElementContainer = elementContainer;
		elementContainer = newElementContainer;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RuntimePackage.SML_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER, oldElementContainer, newElementContainer);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElementContainer(ElementContainer newElementContainer) {
		if (newElementContainer != elementContainer) {
			NotificationChain msgs = null;
			if (elementContainer != null)
				msgs = ((InternalEObject)elementContainer).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.SML_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER, null, msgs);
			if (newElementContainer != null)
				msgs = ((InternalEObject)newElementContainer).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.SML_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER, null, msgs);
			msgs = basicSetElementContainer(newElementContainer, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.SML_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER, newElementContainer, newElementContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterRangesProvider getParameterRangesProvider() {
		return parameterRangesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameterRangesProvider(ParameterRangesProvider newParameterRangesProvider, NotificationChain msgs) {
		ParameterRangesProvider oldParameterRangesProvider = parameterRangesProvider;
		parameterRangesProvider = newParameterRangesProvider;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RuntimePackage.SML_RUNTIME_STATE_GRAPH__PARAMETER_RANGES_PROVIDER, oldParameterRangesProvider, newParameterRangesProvider);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterRangesProvider(ParameterRangesProvider newParameterRangesProvider) {
		if (newParameterRangesProvider != parameterRangesProvider) {
			NotificationChain msgs = null;
			if (parameterRangesProvider != null)
				msgs = ((InternalEObject)parameterRangesProvider).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.SML_RUNTIME_STATE_GRAPH__PARAMETER_RANGES_PROVIDER, null, msgs);
			if (newParameterRangesProvider != null)
				msgs = ((InternalEObject)newParameterRangesProvider).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.SML_RUNTIME_STATE_GRAPH__PARAMETER_RANGES_PROVIDER, null, msgs);
			msgs = basicSetParameterRangesProvider(newParameterRangesProvider, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.SML_RUNTIME_STATE_GRAPH__PARAMETER_RANGES_PROVIDER, newParameterRangesProvider, newParameterRangesProvider));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SMLRuntimeState> getStates() {
		if (states == null) {
			states = new EObjectContainmentWithInverseEList<SMLRuntimeState>(SMLRuntimeState.class, this, RuntimePackage.SML_RUNTIME_STATE_GRAPH__STATES, RuntimePackage.SML_RUNTIME_STATE__STATE_GRAPH);
		}
		return states;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SMLRuntimeState getStartState() {
		if (startState != null && startState.eIsProxy()) {
			InternalEObject oldStartState = (InternalEObject)startState;
			startState = (SMLRuntimeState)eResolveProxy(oldStartState);
			if (startState != oldStartState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.SML_RUNTIME_STATE_GRAPH__START_STATE, oldStartState, startState));
			}
		}
		return startState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SMLRuntimeState basicGetStartState() {
		return startState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartState(SMLRuntimeState newStartState) {
		SMLRuntimeState oldStartState = startState;
		startState = newStartState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.SML_RUNTIME_STATE_GRAPH__START_STATE, oldStartState, startState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public SMLRuntimeState init(Configuration config) {
		return super.init(config);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Transition generateSuccessor(SMLRuntimeState state, Event event) {
		return super.generateSuccessor(state, event);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Transition> generateAllSuccessors(SMLRuntimeState state) {
		return super.generateAllSuccessors(state);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__STATES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getStates()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER:
				return basicSetElementContainer(null, msgs);
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__PARAMETER_RANGES_PROVIDER:
				return basicSetParameterRangesProvider(null, msgs);
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__STATES:
				return ((InternalEList<?>)getStates()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__CONFIGURATION:
				if (resolve) return getConfiguration();
				return basicGetConfiguration();
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER:
				return getElementContainer();
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__PARAMETER_RANGES_PROVIDER:
				return getParameterRangesProvider();
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__STATES:
				return getStates();
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__START_STATE:
				if (resolve) return getStartState();
				return basicGetStartState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__CONFIGURATION:
				setConfiguration((Configuration)newValue);
				return;
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER:
				setElementContainer((ElementContainer)newValue);
				return;
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__PARAMETER_RANGES_PROVIDER:
				setParameterRangesProvider((ParameterRangesProvider)newValue);
				return;
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__STATES:
				getStates().clear();
				getStates().addAll((Collection<? extends SMLRuntimeState>)newValue);
				return;
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__START_STATE:
				setStartState((SMLRuntimeState)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__CONFIGURATION:
				setConfiguration((Configuration)null);
				return;
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER:
				setElementContainer((ElementContainer)null);
				return;
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__PARAMETER_RANGES_PROVIDER:
				setParameterRangesProvider((ParameterRangesProvider)null);
				return;
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__STATES:
				getStates().clear();
				return;
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__START_STATE:
				setStartState((SMLRuntimeState)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__CONFIGURATION:
				return configuration != null;
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER:
				return elementContainer != null;
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__PARAMETER_RANGES_PROVIDER:
				return parameterRangesProvider != null;
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__STATES:
				return states != null && !states.isEmpty();
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH__START_STATE:
				return startState != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH___INIT__CONFIGURATION:
				return init((Configuration)arguments.get(0));
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH___GENERATE_SUCCESSOR__SMLRUNTIMESTATE_EVENT:
				return generateSuccessor((SMLRuntimeState)arguments.get(0), (Event)arguments.get(1));
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH___GENERATE_ALL_SUCCESSORS__SMLRUNTIMESTATE:
				return generateAllSuccessors((SMLRuntimeState)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //SMLRuntimeStateGraphImpl
