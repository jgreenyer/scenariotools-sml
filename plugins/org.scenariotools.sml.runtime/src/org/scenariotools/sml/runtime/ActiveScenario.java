/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.sml.Scenario;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * An active copy of an SML scenario. Records which fragment of the scenario
 * it references is currently enabled. Stores information about which objects
 * are bound to the scenario's roles in a {@link #roleBindings ActiveScenarioRoleBindings} instance.
 * Can request / block / listen to message events.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.ActiveScenario#getScenario <em>Scenario</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActiveScenario#getMainActiveInteraction <em>Main Active Interaction</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActiveScenario#getAlphabet <em>Alphabet</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActiveScenario#getRoleBindings <em>Role Bindings</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActiveScenario#isSafetyViolationOccurred <em>Safety Violation Occurred</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActiveScenario#getContextHelperClassInstances <em>Context Helper Class Instances</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveScenario()
 * @model
 * @generated
 */
public interface ActiveScenario extends Context {
	/**
	 * Returns the value of the '<em><b>Scenario</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scenario</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The scenario that is the source of this ActiveScenario instance.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Scenario</em>' reference.
	 * @see #setScenario(Scenario)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveScenario_Scenario()
	 * @model
	 * @generated
	 */
	Scenario getScenario();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.ActiveScenario#getScenario <em>Scenario</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scenario</em>' reference.
	 * @see #getScenario()
	 * @generated
	 */
	void setScenario(Scenario value);

	/**
	 * Returns the value of the '<em><b>Main Active Interaction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Main Active Interaction</em>' containment reference.
	 * @see #setMainActiveInteraction(ActivePart)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveScenario_MainActiveInteraction()
	 * @model containment="true"
	 * @generated
	 */
	ActivePart getMainActiveInteraction();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.ActiveScenario#getMainActiveInteraction <em>Main Active Interaction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Main Active Interaction</em>' containment reference.
	 * @see #getMainActiveInteraction()
	 * @generated
	 */
	void setMainActiveInteraction(ActivePart value);

	/**
	 * Returns the value of the '<em><b>Alphabet</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.MessageEvent}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * <p>Message events that occur in any part of this ActiveScenario, that is,
	 * in any interaction or constraint block.<p>
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Alphabet</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveScenario_Alphabet()
	 * @model containment="true"
	 * @generated
	 */
	EList<MessageEvent> getAlphabet();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<MessageEvent> getRequestedEvents();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Returns true, if this ActiveScenario requests any events.
	 * <!-- end-model-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isInRequestedState();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * <p>Returns true, if a strict fragment is enabled in this ActiveScenario.</p>
	 * <!-- end-model-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isInStrictState();

	/**
	 * Returns the value of the '<em><b>Role Bindings</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * <p>Provides data about objects that are bound to roles in this ActiveScenario.
	 * </p>
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Role Bindings</em>' reference.
	 * @see #setRoleBindings(ActiveScenarioRoleBindings)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveScenario_RoleBindings()
	 * @model
	 * @generated
	 */
	ActiveScenarioRoleBindings getRoleBindings();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.ActiveScenario#getRoleBindings <em>Role Bindings</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role Bindings</em>' reference.
	 * @see #getRoleBindings()
	 * @generated
	 */
	void setRoleBindings(ActiveScenarioRoleBindings value);

	/**
	 * Returns the value of the '<em><b>Safety Violation Occurred</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Indicates that a safety violation occurred during an update of this ActiveScenario.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Safety Violation Occurred</em>' attribute.
	 * @see #setSafetyViolationOccurred(boolean)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveScenario_SafetyViolationOccurred()
	 * @model
	 * @generated
	 */
	boolean isSafetyViolationOccurred();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.ActiveScenario#isSafetyViolationOccurred <em>Safety Violation Occurred</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Safety Violation Occurred</em>' attribute.
	 * @see #isSafetyViolationOccurred()
	 * @generated
	 */
	void setSafetyViolationOccurred(boolean value);

	/**
	 * Returns the value of the '<em><b>Context Helper Class Instances</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.ecore.EClass},
	 * and the value is of type {@link org.eclipse.emf.ecore.EObject},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Context Helper Class Instances</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context Helper Class Instances</em>' map.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveScenario_ContextHelperClassInstances()
	 * @model mapType="org.scenariotools.sml.runtime.EClassToEObjectMapEntry&lt;org.eclipse.emf.ecore.EClass, org.eclipse.emf.ecore.EObject&gt;" transient="true"
	 * @generated
	 */
	EMap<EClass, EObject> getContextHelperClassInstances();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Progresses this active scenario with the given message event if possible.
	 * The {@link org.scenariotools.sml.runtime.ActiveScenarioProgress return value}
	 * indicates whether  this active scenario was progressed, violated or nothing happened.
	 * @param smlRuntimeState Required for computing values for message parameters.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	ActiveScenarioProgress performStep(MessageEvent event, SMLRuntimeState smlRuntimeState);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Prepares this ActiveScenario such that the <a href="https://bitbucket.org/jgreenyer/scenariotools-sml/wiki/Interaction%20Fragment%20Order">first</a> message fragment becomes enabled.
	 * The first three parameters are required for binding all roles and the  last parameter is the message event whose occurrence leads to creation of this ActiveScenario instance.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	MultiActiveScenarioInitializations init(SMLObjectSystem objectSystem, DynamicObjectContainer dynamicObjectContainer, SMLRuntimeState smlRuntimeState, MessageEvent initializingMessageEvent);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Returns true, if occurrence of this message event is forbidden by this ActiveScenario
	 * instance, that is, if it leads to a safety violation when invoking {@link #performStep}.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean isBlocked(MessageEvent MessageEvent);

} // ActiveScenario
