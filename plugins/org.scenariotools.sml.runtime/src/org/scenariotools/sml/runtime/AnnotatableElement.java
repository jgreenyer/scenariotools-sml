/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Annotatable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.AnnotatableElement#getStringToBooleanAnnotationMap <em>String To Boolean Annotation Map</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.AnnotatableElement#getStringToStringAnnotationMap <em>String To String Annotation Map</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.AnnotatableElement#getStringToEObjectAnnotationMap <em>String To EObject Annotation Map</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getAnnotatableElement()
 * @model
 * @generated
 */
public interface AnnotatableElement extends EObject {
	/**
	 * Returns the value of the '<em><b>String To Boolean Annotation Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.Boolean},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String To Boolean Annotation Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String To Boolean Annotation Map</em>' map.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getAnnotatableElement_StringToBooleanAnnotationMap()
	 * @model mapType="org.scenariotools.sml.runtime.StringToBooleanMapEntry&lt;org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EBooleanObject&gt;"
	 * @generated
	 */
	EMap<String, Boolean> getStringToBooleanAnnotationMap();

	/**
	 * Returns the value of the '<em><b>String To String Annotation Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String To String Annotation Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String To String Annotation Map</em>' map.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getAnnotatableElement_StringToStringAnnotationMap()
	 * @model mapType="org.scenariotools.sml.runtime.StringToStringMapEntry&lt;org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString&gt;"
	 * @generated
	 */
	EMap<String, String> getStringToStringAnnotationMap();

	/**
	 * Returns the value of the '<em><b>String To EObject Annotation Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link org.eclipse.emf.ecore.EObject},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String To EObject Annotation Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String To EObject Annotation Map</em>' map.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getAnnotatableElement_StringToEObjectAnnotationMap()
	 * @model mapType="org.scenariotools.sml.runtime.StringToEObjectMapEntry&lt;org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EObject&gt;"
	 * @generated
	 */
	EMap<String, EObject> getStringToEObjectAnnotationMap();

} // AnnotatableElement
