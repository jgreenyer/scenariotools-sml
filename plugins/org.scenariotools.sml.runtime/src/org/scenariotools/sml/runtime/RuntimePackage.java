/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.scenariotools.sml.runtime.RuntimeFactory
 * @model kind="package"
 * @generated
 */
public interface RuntimePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "runtime";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.scenariotools.org/sml/runtime/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "runtime";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RuntimePackage eINSTANCE = org.scenariotools.sml.runtime.impl.RuntimePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.SMLRuntimeStateGraphImpl <em>SML Runtime State Graph</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.SMLRuntimeStateGraphImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getSMLRuntimeStateGraph()
	 * @generated
	 */
	int SML_RUNTIME_STATE_GRAPH = 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.AnnotatableElementImpl <em>Annotatable Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.AnnotatableElementImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getAnnotatableElement()
	 * @generated
	 */
	int ANNOTATABLE_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>String To Boolean Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATABLE_ELEMENT__STRING_TO_BOOLEAN_ANNOTATION_MAP = 0;

	/**
	 * The feature id for the '<em><b>String To String Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATABLE_ELEMENT__STRING_TO_STRING_ANNOTATION_MAP = 1;

	/**
	 * The feature id for the '<em><b>String To EObject Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATABLE_ELEMENT__STRING_TO_EOBJECT_ANNOTATION_MAP = 2;

	/**
	 * The number of structural features of the '<em>Annotatable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATABLE_ELEMENT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Annotatable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATABLE_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH__CONFIGURATION = 0;

	/**
	 * The feature id for the '<em><b>Element Container</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER = 1;

	/**
	 * The feature id for the '<em><b>Parameter Ranges Provider</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH__PARAMETER_RANGES_PROVIDER = 2;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH__STATES = 3;

	/**
	 * The feature id for the '<em><b>Start State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH__START_STATE = 4;

	/**
	 * The number of structural features of the '<em>SML Runtime State Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH_FEATURE_COUNT = 5;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH___INIT__CONFIGURATION = 0;

	/**
	 * The operation id for the '<em>Generate Successor</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH___GENERATE_SUCCESSOR__SMLRUNTIMESTATE_EVENT = 1;

	/**
	 * The operation id for the '<em>Generate All Successors</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH___GENERATE_ALL_SUCCESSORS__SMLRUNTIMESTATE = 2;

	/**
	 * The number of operations of the '<em>SML Runtime State Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH_OPERATION_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.SMLRuntimeStateImpl <em>SML Runtime State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.SMLRuntimeStateImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getSMLRuntimeState()
	 * @generated
	 */
	int SML_RUNTIME_STATE = 3;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.Context <em>Context</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.Context
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getContext()
	 * @generated
	 */
	int CONTEXT = 16;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveScenarioImpl <em>Active Scenario</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveScenarioImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenario()
	 * @generated
	 */
	int ACTIVE_SCENARIO = 17;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ElementContainerImpl <em>Element Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ElementContainerImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getElementContainer()
	 * @generated
	 */
	int ELEMENT_CONTAINER = 2;

	/**
	 * The feature id for the '<em><b>Active Scenarios</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_SCENARIOS = 0;

	/**
	 * The feature id for the '<em><b>Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_INTERACTIONS = 1;

	/**
	 * The feature id for the '<em><b>Object Systems</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__OBJECT_SYSTEMS = 2;

	/**
	 * The feature id for the '<em><b>Active Scenario Role Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS = 3;

	/**
	 * The feature id for the '<em><b>Dynamic Object Container</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__DYNAMIC_OBJECT_CONTAINER = 4;

	/**
	 * The feature id for the '<em><b>Active Interaction Key Wrapper To Active Interaction Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP = 5;

	/**
	 * The feature id for the '<em><b>Active Scenario Key Wrapper To Active Scenario Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP = 6;

	/**
	 * The feature id for the '<em><b>Object System Key Wrapper To Object System Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP = 7;

	/**
	 * The feature id for the '<em><b>Object System Key Wrapper To Dynamic Object Container Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP = 8;

	/**
	 * The feature id for the '<em><b>State Key Wrapper To State Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__STATE_KEY_WRAPPER_TO_STATE_MAP = 9;

	/**
	 * The feature id for the '<em><b>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP = 10;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ENABLED = 11;

	/**
	 * The feature id for the '<em><b>Wait Event</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__WAIT_EVENT = 12;

	/**
	 * The number of structural features of the '<em>Element Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER_FEATURE_COUNT = 13;

	/**
	 * The operation id for the '<em>Get Active Scenario</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER___GET_ACTIVE_SCENARIO__ACTIVESCENARIO = 0;

	/**
	 * The operation id for the '<em>Get Object System</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER___GET_OBJECT_SYSTEM__SMLOBJECTSYSTEM = 1;

	/**
	 * The operation id for the '<em>Get SML Runtime State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER___GET_SML_RUNTIME_STATE__SMLRUNTIMESTATE = 2;

	/**
	 * The operation id for the '<em>Get Active Scenario Role Bindings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER___GET_ACTIVE_SCENARIO_ROLE_BINDINGS__ACTIVESCENARIOROLEBINDINGS = 3;

	/**
	 * The operation id for the '<em>Get Dynamic Object Container</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER___GET_DYNAMIC_OBJECT_CONTAINER__DYNAMICOBJECTCONTAINER_SMLOBJECTSYSTEM = 4;

	/**
	 * The number of operations of the '<em>Element Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER_OPERATION_COUNT = 5;

	/**
	 * The feature id for the '<em><b>String To Boolean Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__STRING_TO_BOOLEAN_ANNOTATION_MAP = ANNOTATABLE_ELEMENT__STRING_TO_BOOLEAN_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>String To String Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__STRING_TO_STRING_ANNOTATION_MAP = ANNOTATABLE_ELEMENT__STRING_TO_STRING_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>String To EObject Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__STRING_TO_EOBJECT_ANNOTATION_MAP = ANNOTATABLE_ELEMENT__STRING_TO_EOBJECT_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>Active Scenarios</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__ACTIVE_SCENARIOS = ANNOTATABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dynamic Object Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__DYNAMIC_OBJECT_CONTAINER = ANNOTATABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Computed Initializing Message Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__COMPUTED_INITIALIZING_MESSAGE_EVENTS = ANNOTATABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Terminated Existential Scenarios From Last Perform Step</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__TERMINATED_EXISTENTIAL_SCENARIOS_FROM_LAST_PERFORM_STEP = ANNOTATABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Message Event Blocked Information</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__MESSAGE_EVENT_BLOCKED_INFORMATION = ANNOTATABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Object System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__OBJECT_SYSTEM = ANNOTATABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Safety Violation Occurred In Guarantees</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_GUARANTEES = ANNOTATABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Safety Violation Occurred In Assumptions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS = ANNOTATABLE_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Event To Transition Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__EVENT_TO_TRANSITION_MAP = ANNOTATABLE_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__ENABLED_EVENTS = ANNOTATABLE_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Outgoing Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__OUTGOING_TRANSITION = ANNOTATABLE_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Incoming Transition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__INCOMING_TRANSITION = ANNOTATABLE_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>State Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__STATE_GRAPH = ANNOTATABLE_ELEMENT_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>System Chose To Wait</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__SYSTEM_CHOSE_TO_WAIT = ANNOTATABLE_ELEMENT_FEATURE_COUNT + 13;

	/**
	 * The number of structural features of the '<em>SML Runtime State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_FEATURE_COUNT = ANNOTATABLE_ELEMENT_FEATURE_COUNT + 14;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE___INIT__SMLOBJECTSYSTEM = ANNOTATABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Update Enabled Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE___UPDATE_ENABLED_MESSAGE_EVENTS = ANNOTATABLE_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE___PERFORM_STEP__EVENT = ANNOTATABLE_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>SML Runtime State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_OPERATION_COUNT = ANNOTATABLE_ELEMENT_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.SMLObjectSystemImpl <em>SML Object System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.SMLObjectSystemImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getSMLObjectSystem()
	 * @generated
	 */
	int SML_OBJECT_SYSTEM = 4;

	/**
	 * The feature id for the '<em><b>EClass To EObject</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM__ECLASS_TO_EOBJECT = 0;

	/**
	 * The feature id for the '<em><b>Static Role Bindings</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM__STATIC_ROLE_BINDINGS = 1;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM__SPECIFICATION = 2;

	/**
	 * The feature id for the '<em><b>Runconfiguration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM__RUNCONFIGURATION = 3;

	/**
	 * The feature id for the '<em><b>Message Event Side Effects Executor</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM__MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR = 4;

	/**
	 * The feature id for the '<em><b>Message Event Is Independent Evaluators</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM__MESSAGE_EVENT_IS_INDEPENDENT_EVALUATORS = 5;

	/**
	 * The feature id for the '<em><b>Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM__OBJECTS = 6;

	/**
	 * The feature id for the '<em><b>Controllable Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM__CONTROLLABLE_OBJECTS = 7;

	/**
	 * The feature id for the '<em><b>Uncontrollable Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS = 8;

	/**
	 * The feature id for the '<em><b>Root Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM__ROOT_OBJECTS = 9;

	/**
	 * The number of structural features of the '<em>SML Object System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM_FEATURE_COUNT = 10;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM___INIT__CONFIGURATION_SMLRUNTIMESTATE = 0;

	/**
	 * The operation id for the '<em>Execute Side Effects</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM___EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = 1;

	/**
	 * The operation id for the '<em>Can Execute Side Effects</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM___CAN_EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = 2;

	/**
	 * The operation id for the '<em>Is Independent</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM___IS_INDEPENDENT__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = 3;

	/**
	 * The operation id for the '<em>Is Non Spontaneous Message Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM___IS_NON_SPONTANEOUS_MESSAGE_EVENT__MESSAGEEVENT = 4;

	/**
	 * The operation id for the '<em>Get Scenarios For Init Message Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM___GET_SCENARIOS_FOR_INIT_MESSAGE_EVENT__MESSAGEEVENT = 5;

	/**
	 * The operation id for the '<em>Get Initializing Environment Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM___GET_INITIALIZING_ENVIRONMENT_MESSAGE_EVENTS__DYNAMICOBJECTCONTAINER = 6;

	/**
	 * The operation id for the '<em>Is Controllable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM___IS_CONTROLLABLE__EOBJECT = 7;

	/**
	 * The operation id for the '<em>Is Environment Message Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM___IS_ENVIRONMENT_MESSAGE_EVENT__EVENT = 8;

	/**
	 * The operation id for the '<em>Contains EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM___CONTAINS_EOBJECT__EOBJECT = 9;

	/**
	 * The number of operations of the '<em>SML Object System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM_OPERATION_COUNT = 10;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActivePartImpl <em>Active Part</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActivePartImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActivePart()
	 * @generated
	 */
	int ACTIVE_PART = 19;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveAlternativeImpl <em>Active Alternative</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveAlternativeImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveAlternative()
	 * @generated
	 */
	int ACTIVE_ALTERNATIVE = 20;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveCaseImpl <em>Active Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveCaseImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveCase()
	 * @generated
	 */
	int ACTIVE_CASE = 21;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveInteractionImpl <em>Active Interaction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveInteractionImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveInteraction()
	 * @generated
	 */
	int ACTIVE_INTERACTION = 22;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveInterruptConditionImpl <em>Active Interrupt Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveInterruptConditionImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveInterruptCondition()
	 * @generated
	 */
	int ACTIVE_INTERRUPT_CONDITION = 23;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveLoopImpl <em>Active Loop</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveLoopImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveLoop()
	 * @generated
	 */
	int ACTIVE_LOOP = 24;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveModalMessageImpl <em>Active Modal Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveModalMessageImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveModalMessage()
	 * @generated
	 */
	int ACTIVE_MODAL_MESSAGE = 25;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveParallelImpl <em>Active Parallel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveParallelImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveParallel()
	 * @generated
	 */
	int ACTIVE_PARALLEL = 26;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveVariableFragmentImpl <em>Active Variable Fragment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveVariableFragmentImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveVariableFragment()
	 * @generated
	 */
	int ACTIVE_VARIABLE_FRAGMENT = 27;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveViolationConditionImpl <em>Active Violation Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveViolationConditionImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveViolationCondition()
	 * @generated
	 */
	int ACTIVE_VIOLATION_CONDITION = 28;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveWaitConditionImpl <em>Active Wait Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveWaitConditionImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveWaitCondition()
	 * @generated
	 */
	int ACTIVE_WAIT_CONDITION = 29;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveScenarioRoleBindingsImpl <em>Active Scenario Role Bindings</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveScenarioRoleBindingsImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioRoleBindings()
	 * @generated
	 */
	int ACTIVE_SCENARIO_ROLE_BINDINGS = 18;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.EClassToEObjectMapEntryImpl <em>EClass To EObject Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.EClassToEObjectMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getEClassToEObjectMapEntry()
	 * @generated
	 */
	int ECLASS_TO_EOBJECT_MAP_ENTRY = 45;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.RoleToEObjectMapEntryImpl <em>Role To EObject Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.RoleToEObjectMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getRoleToEObjectMapEntry()
	 * @generated
	 */
	int ROLE_TO_EOBJECT_MAP_ENTRY = 46;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveInteractionKeyWrapperToActiveInteractionMapEntryImpl <em>Active Interaction Key Wrapper To Active Interaction Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveInteractionKeyWrapperToActiveInteractionMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveInteractionKeyWrapperToActiveInteractionMapEntry()
	 * @generated
	 */
	int ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY = 53;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveScenarioKeyWrapperToActiveScenarioMapEntryImpl <em>Active Scenario Key Wrapper To Active Scenario Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveScenarioKeyWrapperToActiveScenarioMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioKeyWrapperToActiveScenarioMapEntry()
	 * @generated
	 */
	int ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY = 54;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ObjectSystemKeyWrapperToObjectSystemMapEntryImpl <em>Object System Key Wrapper To Object System Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ObjectSystemKeyWrapperToObjectSystemMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getObjectSystemKeyWrapperToObjectSystemMapEntry()
	 * @generated
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY = 55;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ObjectSystemKeyWrapperToDynamicObjectContainerMapEntryImpl <em>Object System Key Wrapper To Dynamic Object Container Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ObjectSystemKeyWrapperToDynamicObjectContainerMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry()
	 * @generated
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY = 56;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.StateKeyWrapperToStateMapEntryImpl <em>State Key Wrapper To State Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.StateKeyWrapperToStateMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getStateKeyWrapperToStateMapEntry()
	 * @generated
	 */
	int STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY = 57;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.VariableToObjectMapEntryImpl <em>Variable To Object Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.VariableToObjectMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getVariableToObjectMapEntry()
	 * @generated
	 */
	int VARIABLE_TO_OBJECT_MAP_ENTRY = 58;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.VariableToEObjectMapEntryImpl <em>Variable To EObject Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.VariableToEObjectMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getVariableToEObjectMapEntry()
	 * @generated
	 */
	int VARIABLE_TO_EOBJECT_MAP_ENTRY = 59;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryImpl <em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry()
	 * @generated
	 */
	int ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY = 60;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.DynamicObjectContainerImpl <em>Dynamic Object Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.DynamicObjectContainerImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getDynamicObjectContainer()
	 * @generated
	 */
	int DYNAMIC_OBJECT_CONTAINER = 5;

	/**
	 * The feature id for the '<em><b>Static EObject To Dynamic EObject Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC_OBJECT_CONTAINER__STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP = 0;

	/**
	 * The feature id for the '<em><b>Root Objects</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC_OBJECT_CONTAINER__ROOT_OBJECTS = 1;

	/**
	 * The number of structural features of the '<em>Dynamic Object Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC_OBJECT_CONTAINER_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Dynamic Object Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC_OBJECT_CONTAINER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.StaticEObjectToDynamicEObjectMapEntryImpl <em>Static EObject To Dynamic EObject Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.StaticEObjectToDynamicEObjectMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getStaticEObjectToDynamicEObjectMapEntry()
	 * @generated
	 */
	int STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY = 47;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.MessageEventExtensionInterfaceImpl <em>Message Event Extension Interface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.MessageEventExtensionInterfaceImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getMessageEventExtensionInterface()
	 * @generated
	 */
	int MESSAGE_EVENT_EXTENSION_INTERFACE = 42;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.MessageEventSideEffectsExecutorImpl <em>Message Event Side Effects Executor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.MessageEventSideEffectsExecutorImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getMessageEventSideEffectsExecutor()
	 * @generated
	 */
	int MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR = 43;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.MessageEventIsIndependentEvaluatorImpl <em>Message Event Is Independent Evaluator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.MessageEventIsIndependentEvaluatorImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getMessageEventIsIndependentEvaluator()
	 * @generated
	 */
	int MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR = 44;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.ActiveMessageParameter <em>Active Message Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameter
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveMessageParameter()
	 * @generated
	 */
	int ACTIVE_MESSAGE_PARAMETER = 30;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithValueExpressionImpl <em>Active Message Parameter With Value Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithValueExpressionImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveMessageParameterWithValueExpression()
	 * @generated
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION = 31;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithBindToVarImpl <em>Active Message Parameter With Bind To Var</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithBindToVarImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveMessageParameterWithBindToVar()
	 * @generated
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR = 32;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithWildcardImpl <em>Active Message Parameter With Wildcard</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithWildcardImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveMessageParameterWithWildcard()
	 * @generated
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD = 33;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveConstraintImpl <em>Active Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveConstraintImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveConstraint()
	 * @generated
	 */
	int ACTIVE_CONSTRAINT = 34;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveConstraintConsiderImpl <em>Active Constraint Consider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveConstraintConsiderImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveConstraintConsider()
	 * @generated
	 */
	int ACTIVE_CONSTRAINT_CONSIDER = 35;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveConstraintIgnoreImpl <em>Active Constraint Ignore</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveConstraintIgnoreImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveConstraintIgnore()
	 * @generated
	 */
	int ACTIVE_CONSTRAINT_IGNORE = 36;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveConstraintInterruptImpl <em>Active Constraint Interrupt</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveConstraintInterruptImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveConstraintInterrupt()
	 * @generated
	 */
	int ACTIVE_CONSTRAINT_INTERRUPT = 37;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveConstraintForbiddenImpl <em>Active Constraint Forbidden</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveConstraintForbiddenImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveConstraintForbidden()
	 * @generated
	 */
	int ACTIVE_CONSTRAINT_FORBIDDEN = 38;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ParameterRangesProviderImpl <em>Parameter Ranges Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ParameterRangesProviderImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getParameterRangesProvider()
	 * @generated
	 */
	int PARAMETER_RANGES_PROVIDER = 39;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.MessageEventBlockedInformationImpl <em>Message Event Blocked Information</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.MessageEventBlockedInformationImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getMessageEventBlockedInformation()
	 * @generated
	 */
	int MESSAGE_EVENT_BLOCKED_INFORMATION = 40;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.MultiActiveScenarioInitializationsImpl <em>Multi Active Scenario Initializations</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.MultiActiveScenarioInitializationsImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getMultiActiveScenarioInitializations()
	 * @generated
	 */
	int MULTI_ACTIVE_SCENARIO_INITIALIZATIONS = 41;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveScenarioToActiveScenarioProgressMapEntryImpl <em>Active Scenario To Active Scenario Progress Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveScenarioToActiveScenarioProgressMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioToActiveScenarioProgressMapEntry()
	 * @generated
	 */
	int ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP_ENTRY = 48;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.TransitionImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 6;

	/**
	 * The feature id for the '<em><b>String To Boolean Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__STRING_TO_BOOLEAN_ANNOTATION_MAP = ANNOTATABLE_ELEMENT__STRING_TO_BOOLEAN_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>String To String Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__STRING_TO_STRING_ANNOTATION_MAP = ANNOTATABLE_ELEMENT__STRING_TO_STRING_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>String To EObject Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__STRING_TO_EOBJECT_ANNOTATION_MAP = ANNOTATABLE_ELEMENT__STRING_TO_EOBJECT_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>Source State</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__SOURCE_STATE = ANNOTATABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TARGET_STATE = ANNOTATABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__EVENT = ANNOTATABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__LABEL = ANNOTATABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = ANNOTATABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_OPERATION_COUNT = ANNOTATABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.StringToBooleanMapEntryImpl <em>String To Boolean Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.StringToBooleanMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getStringToBooleanMapEntry()
	 * @generated
	 */
	int STRING_TO_BOOLEAN_MAP_ENTRY = 50;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.StringToStringMapEntryImpl <em>String To String Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.StringToStringMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getStringToStringMapEntry()
	 * @generated
	 */
	int STRING_TO_STRING_MAP_ENTRY = 51;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.StringToEObjectMapEntryImpl <em>String To EObject Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.StringToEObjectMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getStringToEObjectMapEntry()
	 * @generated
	 */
	int STRING_TO_EOBJECT_MAP_ENTRY = 52;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.EventImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 7;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.MessageEventImpl <em>Message Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.MessageEventImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getMessageEvent()
	 * @generated
	 */
	int MESSAGE_EVENT = 9;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ParameterValueImpl <em>Parameter Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ParameterValueImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getParameterValue()
	 * @generated
	 */
	int PARAMETER_VALUE = 10;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.BooleanParameterValueImpl <em>Boolean Parameter Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.BooleanParameterValueImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getBooleanParameterValue()
	 * @generated
	 */
	int BOOLEAN_PARAMETER_VALUE = 11;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.IntegerParameterValueImpl <em>Integer Parameter Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.IntegerParameterValueImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getIntegerParameterValue()
	 * @generated
	 */
	int INTEGER_PARAMETER_VALUE = 12;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.StringParameterValueImpl <em>String Parameter Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.StringParameterValueImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getStringParameterValue()
	 * @generated
	 */
	int STRING_PARAMETER_VALUE = 13;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.EObjectParameterValueImpl <em>EObject Parameter Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.EObjectParameterValueImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getEObjectParameterValue()
	 * @generated
	 */
	int EOBJECT_PARAMETER_VALUE = 14;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.EEnumParameterValueImpl <em>EEnum Parameter Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.EEnumParameterValueImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getEEnumParameterValue()
	 * @generated
	 */
	int EENUM_PARAMETER_VALUE = 15;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.WaitEventImpl <em>Wait Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.WaitEventImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getWaitEvent()
	 * @generated
	 */
	int WAIT_EVENT = 8;

	/**
	 * The number of structural features of the '<em>Wait Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Wait Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_EVENT_OPERATION_COUNT = EVENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sending Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__SENDING_OBJECT = EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Receiving Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__RECEIVING_OBJECT = EVENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Message Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__MESSAGE_NAME = EVENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Concrete</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__CONCRETE = EVENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Parameterized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__PARAMETERIZED = EVENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Typed Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__TYPED_ELEMENT = EVENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Parameter Values</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__PARAMETER_VALUES = EVENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Collection Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__COLLECTION_OPERATION = EVENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Message Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Eq</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT___EQ__MESSAGEEVENT = EVENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Message Unifiable With</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT___IS_MESSAGE_UNIFIABLE_WITH__MESSAGEEVENT = EVENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Parameter Unifiable With</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT___IS_PARAMETER_UNIFIABLE_WITH__MESSAGEEVENT = EVENT_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Message Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_OPERATION_COUNT = EVENT_OPERATION_COUNT + 3;

	/**
	 * The feature id for the '<em><b>EParameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE__EPARAMETER = 0;

	/**
	 * The feature id for the '<em><b>Unset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE__UNSET = 1;

	/**
	 * The feature id for the '<em><b>Wildcard Parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE__WILDCARD_PARAMETER = 2;

	/**
	 * The feature id for the '<em><b>Struc Feature Or EOp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE__STRUC_FEATURE_OR_EOP = 3;

	/**
	 * The number of structural features of the '<em>Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE_FEATURE_COUNT = 4;

	/**
	 * The operation id for the '<em>Set Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE___SET_VALUE__OBJECT = 0;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE___GET_VALUE = 1;

	/**
	 * The operation id for the '<em>Eq</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE___EQ__PARAMETERVALUE = 2;

	/**
	 * The operation id for the '<em>Is Parameter Unifiable With</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE = 3;

	/**
	 * The number of operations of the '<em>Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE_OPERATION_COUNT = 4;

	/**
	 * The feature id for the '<em><b>EParameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE__EPARAMETER = PARAMETER_VALUE__EPARAMETER;

	/**
	 * The feature id for the '<em><b>Unset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE__UNSET = PARAMETER_VALUE__UNSET;

	/**
	 * The feature id for the '<em><b>Wildcard Parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE__WILDCARD_PARAMETER = PARAMETER_VALUE__WILDCARD_PARAMETER;

	/**
	 * The feature id for the '<em><b>Struc Feature Or EOp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE__STRUC_FEATURE_OR_EOP = PARAMETER_VALUE__STRUC_FEATURE_OR_EOP;

	/**
	 * The feature id for the '<em><b>Boolean Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE__BOOLEAN_PARAMETER_VALUE = PARAMETER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Boolean Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE_FEATURE_COUNT = PARAMETER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Set Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE___SET_VALUE__OBJECT = PARAMETER_VALUE___SET_VALUE__OBJECT;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE___GET_VALUE = PARAMETER_VALUE___GET_VALUE;

	/**
	 * The operation id for the '<em>Eq</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE___EQ__PARAMETERVALUE = PARAMETER_VALUE___EQ__PARAMETERVALUE;

	/**
	 * The operation id for the '<em>Is Parameter Unifiable With</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE = PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE;

	/**
	 * The number of operations of the '<em>Boolean Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE_OPERATION_COUNT = PARAMETER_VALUE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>EParameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE__EPARAMETER = PARAMETER_VALUE__EPARAMETER;

	/**
	 * The feature id for the '<em><b>Unset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE__UNSET = PARAMETER_VALUE__UNSET;

	/**
	 * The feature id for the '<em><b>Wildcard Parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE__WILDCARD_PARAMETER = PARAMETER_VALUE__WILDCARD_PARAMETER;

	/**
	 * The feature id for the '<em><b>Struc Feature Or EOp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE__STRUC_FEATURE_OR_EOP = PARAMETER_VALUE__STRUC_FEATURE_OR_EOP;

	/**
	 * The feature id for the '<em><b>Integer Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE__INTEGER_PARAMETER_VALUE = PARAMETER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Integer Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE_FEATURE_COUNT = PARAMETER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Set Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE___SET_VALUE__OBJECT = PARAMETER_VALUE___SET_VALUE__OBJECT;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE___GET_VALUE = PARAMETER_VALUE___GET_VALUE;

	/**
	 * The operation id for the '<em>Eq</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE___EQ__PARAMETERVALUE = PARAMETER_VALUE___EQ__PARAMETERVALUE;

	/**
	 * The operation id for the '<em>Is Parameter Unifiable With</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE = PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE;

	/**
	 * The number of operations of the '<em>Integer Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE_OPERATION_COUNT = PARAMETER_VALUE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>EParameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE__EPARAMETER = PARAMETER_VALUE__EPARAMETER;

	/**
	 * The feature id for the '<em><b>Unset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE__UNSET = PARAMETER_VALUE__UNSET;

	/**
	 * The feature id for the '<em><b>Wildcard Parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE__WILDCARD_PARAMETER = PARAMETER_VALUE__WILDCARD_PARAMETER;

	/**
	 * The feature id for the '<em><b>Struc Feature Or EOp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE__STRUC_FEATURE_OR_EOP = PARAMETER_VALUE__STRUC_FEATURE_OR_EOP;

	/**
	 * The feature id for the '<em><b>String Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE__STRING_PARAMETER_VALUE = PARAMETER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>String Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE_FEATURE_COUNT = PARAMETER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Set Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE___SET_VALUE__OBJECT = PARAMETER_VALUE___SET_VALUE__OBJECT;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE___GET_VALUE = PARAMETER_VALUE___GET_VALUE;

	/**
	 * The operation id for the '<em>Eq</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE___EQ__PARAMETERVALUE = PARAMETER_VALUE___EQ__PARAMETERVALUE;

	/**
	 * The operation id for the '<em>Is Parameter Unifiable With</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE = PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE;

	/**
	 * The number of operations of the '<em>String Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE_OPERATION_COUNT = PARAMETER_VALUE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>EParameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE__EPARAMETER = PARAMETER_VALUE__EPARAMETER;

	/**
	 * The feature id for the '<em><b>Unset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE__UNSET = PARAMETER_VALUE__UNSET;

	/**
	 * The feature id for the '<em><b>Wildcard Parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE__WILDCARD_PARAMETER = PARAMETER_VALUE__WILDCARD_PARAMETER;

	/**
	 * The feature id for the '<em><b>Struc Feature Or EOp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE__STRUC_FEATURE_OR_EOP = PARAMETER_VALUE__STRUC_FEATURE_OR_EOP;

	/**
	 * The feature id for the '<em><b>EObject Parameter Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE__EOBJECT_PARAMETER_VALUE = PARAMETER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>EObject Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE_FEATURE_COUNT = PARAMETER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Set Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE___SET_VALUE__OBJECT = PARAMETER_VALUE___SET_VALUE__OBJECT;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE___GET_VALUE = PARAMETER_VALUE___GET_VALUE;

	/**
	 * The operation id for the '<em>Eq</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE___EQ__PARAMETERVALUE = PARAMETER_VALUE___EQ__PARAMETERVALUE;

	/**
	 * The operation id for the '<em>Is Parameter Unifiable With</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE = PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE;

	/**
	 * The number of operations of the '<em>EObject Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE_OPERATION_COUNT = PARAMETER_VALUE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>EParameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE__EPARAMETER = PARAMETER_VALUE__EPARAMETER;

	/**
	 * The feature id for the '<em><b>Unset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE__UNSET = PARAMETER_VALUE__UNSET;

	/**
	 * The feature id for the '<em><b>Wildcard Parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE__WILDCARD_PARAMETER = PARAMETER_VALUE__WILDCARD_PARAMETER;

	/**
	 * The feature id for the '<em><b>Struc Feature Or EOp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE__STRUC_FEATURE_OR_EOP = PARAMETER_VALUE__STRUC_FEATURE_OR_EOP;

	/**
	 * The feature id for the '<em><b>EEnum Parameter Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE__EENUM_PARAMETER_TYPE = PARAMETER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>EEnum Parameter Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE__EENUM_PARAMETER_VALUE = PARAMETER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>EEnum Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE_FEATURE_COUNT = PARAMETER_VALUE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Set Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE___SET_VALUE__OBJECT = PARAMETER_VALUE___SET_VALUE__OBJECT;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE___GET_VALUE = PARAMETER_VALUE___GET_VALUE;

	/**
	 * The operation id for the '<em>Eq</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE___EQ__PARAMETERVALUE = PARAMETER_VALUE___EQ__PARAMETERVALUE;

	/**
	 * The operation id for the '<em>Is Parameter Unifiable With</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE = PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE;

	/**
	 * The number of operations of the '<em>EEnum Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE_OPERATION_COUNT = PARAMETER_VALUE_OPERATION_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTEXT_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTEXT___GET_VALUE__VARIABLE = 0;

	/**
	 * The number of operations of the '<em>Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTEXT_OPERATION_COUNT = 1;

	/**
	 * The feature id for the '<em><b>Scenario</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO__SCENARIO = CONTEXT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Main Active Interaction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO__MAIN_ACTIVE_INTERACTION = CONTEXT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Alphabet</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO__ALPHABET = CONTEXT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Role Bindings</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO__ROLE_BINDINGS = CONTEXT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Safety Violation Occurred</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO__SAFETY_VIOLATION_OCCURRED = CONTEXT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Context Helper Class Instances</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO__CONTEXT_HELPER_CLASS_INSTANCES = CONTEXT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Active Scenario</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_FEATURE_COUNT = CONTEXT_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO___GET_VALUE__VARIABLE = CONTEXT___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO___PERFORM_STEP__MESSAGEEVENT_SMLRUNTIMESTATE = CONTEXT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO___INIT__SMLOBJECTSYSTEM_DYNAMICOBJECTCONTAINER_SMLRUNTIMESTATE_MESSAGEEVENT = CONTEXT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO___IS_BLOCKED__MESSAGEEVENT = CONTEXT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO___GET_REQUESTED_EVENTS = CONTEXT_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO___IS_IN_REQUESTED_STATE = CONTEXT_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO___IS_IN_STRICT_STATE = CONTEXT_OPERATION_COUNT + 5;

	/**
	 * The number of operations of the '<em>Active Scenario</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_OPERATION_COUNT = CONTEXT_OPERATION_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Role Bindings</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_ROLE_BINDINGS__ROLE_BINDINGS = 0;

	/**
	 * The number of structural features of the '<em>Active Scenario Role Bindings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_ROLE_BINDINGS_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Active Scenario Role Bindings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_ROLE_BINDINGS_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS = CONTEXT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__COVERED_EVENTS = CONTEXT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__FORBIDDEN_EVENTS = CONTEXT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__INTERRUPTING_EVENTS = CONTEXT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__CONSIDERED_EVENTS = CONTEXT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__IGNORED_EVENTS = CONTEXT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__ENABLED_EVENTS = CONTEXT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__PARENT_ACTIVE_INTERACTION = CONTEXT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS = CONTEXT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__VARIABLE_MAP = CONTEXT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__EOBJECT_VARIABLE_MAP = CONTEXT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__INTERACTION_FRAGMENT = CONTEXT_FEATURE_COUNT + 11;

	/**
	 * The number of structural features of the '<em>Active Part</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART_FEATURE_COUNT = CONTEXT_FEATURE_COUNT + 12;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___GET_VALUE__VARIABLE = CONTEXT___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = CONTEXT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = CONTEXT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = CONTEXT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = CONTEXT_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = CONTEXT_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___GET_REQUESTED_EVENTS = CONTEXT_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = CONTEXT_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = CONTEXT_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___IS_IN_REQUESTED_STATE = CONTEXT_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___IS_IN_STRICT_STATE = CONTEXT_OPERATION_COUNT + 9;

	/**
	 * The number of operations of the '<em>Active Part</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART_OPERATION_COUNT = CONTEXT_OPERATION_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__COVERED_EVENTS = ACTIVE_PART__COVERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__FORBIDDEN_EVENTS = ACTIVE_PART__FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__INTERRUPTING_EVENTS = ACTIVE_PART__INTERRUPTING_EVENTS;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__CONSIDERED_EVENTS = ACTIVE_PART__CONSIDERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__IGNORED_EVENTS = ACTIVE_PART__IGNORED_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__ENABLED_EVENTS = ACTIVE_PART__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__PARENT_ACTIVE_INTERACTION = ACTIVE_PART__PARENT_ACTIVE_INTERACTION;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__ENABLED_NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__VARIABLE_MAP = ACTIVE_PART__VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__EOBJECT_VARIABLE_MAP = ACTIVE_PART__EOBJECT_VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__INTERACTION_FRAGMENT = ACTIVE_PART__INTERACTION_FRAGMENT;

	/**
	 * The number of structural features of the '<em>Active Alternative</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE_FEATURE_COUNT = ACTIVE_PART_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___GET_VALUE__VARIABLE = ACTIVE_PART___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___GET_REQUESTED_EVENTS = ACTIVE_PART___GET_REQUESTED_EVENTS;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___IS_IN_REQUESTED_STATE = ACTIVE_PART___IS_IN_REQUESTED_STATE;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___IS_IN_STRICT_STATE = ACTIVE_PART___IS_IN_STRICT_STATE;

	/**
	 * The number of operations of the '<em>Active Alternative</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE_OPERATION_COUNT = ACTIVE_PART_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__COVERED_EVENTS = ACTIVE_PART__COVERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__FORBIDDEN_EVENTS = ACTIVE_PART__FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__INTERRUPTING_EVENTS = ACTIVE_PART__INTERRUPTING_EVENTS;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__CONSIDERED_EVENTS = ACTIVE_PART__CONSIDERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__IGNORED_EVENTS = ACTIVE_PART__IGNORED_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__ENABLED_EVENTS = ACTIVE_PART__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__PARENT_ACTIVE_INTERACTION = ACTIVE_PART__PARENT_ACTIVE_INTERACTION;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__ENABLED_NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__VARIABLE_MAP = ACTIVE_PART__VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__EOBJECT_VARIABLE_MAP = ACTIVE_PART__EOBJECT_VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__INTERACTION_FRAGMENT = ACTIVE_PART__INTERACTION_FRAGMENT;

	/**
	 * The feature id for the '<em><b>Case</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__CASE = ACTIVE_PART_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Active Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE_FEATURE_COUNT = ACTIVE_PART_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___GET_VALUE__VARIABLE = ACTIVE_PART___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___GET_REQUESTED_EVENTS = ACTIVE_PART___GET_REQUESTED_EVENTS;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___IS_IN_REQUESTED_STATE = ACTIVE_PART___IS_IN_REQUESTED_STATE;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___IS_IN_STRICT_STATE = ACTIVE_PART___IS_IN_STRICT_STATE;

	/**
	 * The number of operations of the '<em>Active Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE_OPERATION_COUNT = ACTIVE_PART_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__COVERED_EVENTS = ACTIVE_PART__COVERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__FORBIDDEN_EVENTS = ACTIVE_PART__FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__INTERRUPTING_EVENTS = ACTIVE_PART__INTERRUPTING_EVENTS;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__CONSIDERED_EVENTS = ACTIVE_PART__CONSIDERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__IGNORED_EVENTS = ACTIVE_PART__IGNORED_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__ENABLED_EVENTS = ACTIVE_PART__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__PARENT_ACTIVE_INTERACTION = ACTIVE_PART__PARENT_ACTIVE_INTERACTION;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__ENABLED_NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__VARIABLE_MAP = ACTIVE_PART__VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__EOBJECT_VARIABLE_MAP = ACTIVE_PART__EOBJECT_VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__INTERACTION_FRAGMENT = ACTIVE_PART__INTERACTION_FRAGMENT;

	/**
	 * The feature id for the '<em><b>Active Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__ACTIVE_CONSTRAINTS = ACTIVE_PART_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Active Interaction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION_FEATURE_COUNT = ACTIVE_PART_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___GET_VALUE__VARIABLE = ACTIVE_PART___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___GET_REQUESTED_EVENTS = ACTIVE_PART___GET_REQUESTED_EVENTS;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___IS_IN_REQUESTED_STATE = ACTIVE_PART___IS_IN_REQUESTED_STATE;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___IS_IN_STRICT_STATE = ACTIVE_PART___IS_IN_STRICT_STATE;

	/**
	 * The number of operations of the '<em>Active Interaction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION_OPERATION_COUNT = ACTIVE_PART_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__COVERED_EVENTS = ACTIVE_PART__COVERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__FORBIDDEN_EVENTS = ACTIVE_PART__FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__INTERRUPTING_EVENTS = ACTIVE_PART__INTERRUPTING_EVENTS;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__CONSIDERED_EVENTS = ACTIVE_PART__CONSIDERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__IGNORED_EVENTS = ACTIVE_PART__IGNORED_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__ENABLED_EVENTS = ACTIVE_PART__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__PARENT_ACTIVE_INTERACTION = ACTIVE_PART__PARENT_ACTIVE_INTERACTION;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__ENABLED_NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__VARIABLE_MAP = ACTIVE_PART__VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__EOBJECT_VARIABLE_MAP = ACTIVE_PART__EOBJECT_VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__INTERACTION_FRAGMENT = ACTIVE_PART__INTERACTION_FRAGMENT;

	/**
	 * The number of structural features of the '<em>Active Interrupt Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION_FEATURE_COUNT = ACTIVE_PART_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___GET_VALUE__VARIABLE = ACTIVE_PART___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___GET_REQUESTED_EVENTS = ACTIVE_PART___GET_REQUESTED_EVENTS;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___IS_IN_REQUESTED_STATE = ACTIVE_PART___IS_IN_REQUESTED_STATE;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___IS_IN_STRICT_STATE = ACTIVE_PART___IS_IN_STRICT_STATE;

	/**
	 * The number of operations of the '<em>Active Interrupt Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION_OPERATION_COUNT = ACTIVE_PART_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__COVERED_EVENTS = ACTIVE_PART__COVERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__FORBIDDEN_EVENTS = ACTIVE_PART__FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__INTERRUPTING_EVENTS = ACTIVE_PART__INTERRUPTING_EVENTS;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__CONSIDERED_EVENTS = ACTIVE_PART__CONSIDERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__IGNORED_EVENTS = ACTIVE_PART__IGNORED_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__ENABLED_EVENTS = ACTIVE_PART__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__PARENT_ACTIVE_INTERACTION = ACTIVE_PART__PARENT_ACTIVE_INTERACTION;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__ENABLED_NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__VARIABLE_MAP = ACTIVE_PART__VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__EOBJECT_VARIABLE_MAP = ACTIVE_PART__EOBJECT_VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__INTERACTION_FRAGMENT = ACTIVE_PART__INTERACTION_FRAGMENT;

	/**
	 * The number of structural features of the '<em>Active Loop</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP_FEATURE_COUNT = ACTIVE_PART_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___GET_VALUE__VARIABLE = ACTIVE_PART___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___GET_REQUESTED_EVENTS = ACTIVE_PART___GET_REQUESTED_EVENTS;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___IS_IN_REQUESTED_STATE = ACTIVE_PART___IS_IN_REQUESTED_STATE;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___IS_IN_STRICT_STATE = ACTIVE_PART___IS_IN_STRICT_STATE;

	/**
	 * The number of operations of the '<em>Active Loop</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP_OPERATION_COUNT = ACTIVE_PART_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__COVERED_EVENTS = ACTIVE_PART__COVERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__FORBIDDEN_EVENTS = ACTIVE_PART__FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__INTERRUPTING_EVENTS = ACTIVE_PART__INTERRUPTING_EVENTS;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__CONSIDERED_EVENTS = ACTIVE_PART__CONSIDERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__IGNORED_EVENTS = ACTIVE_PART__IGNORED_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__ENABLED_EVENTS = ACTIVE_PART__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__PARENT_ACTIVE_INTERACTION = ACTIVE_PART__PARENT_ACTIVE_INTERACTION;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__ENABLED_NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__VARIABLE_MAP = ACTIVE_PART__VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__EOBJECT_VARIABLE_MAP = ACTIVE_PART__EOBJECT_VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__INTERACTION_FRAGMENT = ACTIVE_PART__INTERACTION_FRAGMENT;

	/**
	 * The feature id for the '<em><b>Active Message Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__ACTIVE_MESSAGE_PARAMETERS = ACTIVE_PART_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Active Modal Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE_FEATURE_COUNT = ACTIVE_PART_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___GET_VALUE__VARIABLE = ACTIVE_PART___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___GET_REQUESTED_EVENTS = ACTIVE_PART___GET_REQUESTED_EVENTS;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___IS_IN_REQUESTED_STATE = ACTIVE_PART___IS_IN_REQUESTED_STATE;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___IS_IN_STRICT_STATE = ACTIVE_PART___IS_IN_STRICT_STATE;

	/**
	 * The number of operations of the '<em>Active Modal Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE_OPERATION_COUNT = ACTIVE_PART_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__COVERED_EVENTS = ACTIVE_PART__COVERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__FORBIDDEN_EVENTS = ACTIVE_PART__FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__INTERRUPTING_EVENTS = ACTIVE_PART__INTERRUPTING_EVENTS;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__CONSIDERED_EVENTS = ACTIVE_PART__CONSIDERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__IGNORED_EVENTS = ACTIVE_PART__IGNORED_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__ENABLED_EVENTS = ACTIVE_PART__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__PARENT_ACTIVE_INTERACTION = ACTIVE_PART__PARENT_ACTIVE_INTERACTION;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__ENABLED_NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__VARIABLE_MAP = ACTIVE_PART__VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__EOBJECT_VARIABLE_MAP = ACTIVE_PART__EOBJECT_VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__INTERACTION_FRAGMENT = ACTIVE_PART__INTERACTION_FRAGMENT;

	/**
	 * The number of structural features of the '<em>Active Parallel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL_FEATURE_COUNT = ACTIVE_PART_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___GET_VALUE__VARIABLE = ACTIVE_PART___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___GET_REQUESTED_EVENTS = ACTIVE_PART___GET_REQUESTED_EVENTS;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___IS_IN_REQUESTED_STATE = ACTIVE_PART___IS_IN_REQUESTED_STATE;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___IS_IN_STRICT_STATE = ACTIVE_PART___IS_IN_STRICT_STATE;

	/**
	 * The number of operations of the '<em>Active Parallel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL_OPERATION_COUNT = ACTIVE_PART_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__COVERED_EVENTS = ACTIVE_PART__COVERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__FORBIDDEN_EVENTS = ACTIVE_PART__FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__INTERRUPTING_EVENTS = ACTIVE_PART__INTERRUPTING_EVENTS;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__CONSIDERED_EVENTS = ACTIVE_PART__CONSIDERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__IGNORED_EVENTS = ACTIVE_PART__IGNORED_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__ENABLED_EVENTS = ACTIVE_PART__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__PARENT_ACTIVE_INTERACTION = ACTIVE_PART__PARENT_ACTIVE_INTERACTION;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__ENABLED_NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__VARIABLE_MAP = ACTIVE_PART__VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__EOBJECT_VARIABLE_MAP = ACTIVE_PART__EOBJECT_VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__INTERACTION_FRAGMENT = ACTIVE_PART__INTERACTION_FRAGMENT;

	/**
	 * The number of structural features of the '<em>Active Variable Fragment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT_FEATURE_COUNT = ACTIVE_PART_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___GET_VALUE__VARIABLE = ACTIVE_PART___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___GET_REQUESTED_EVENTS = ACTIVE_PART___GET_REQUESTED_EVENTS;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___IS_IN_REQUESTED_STATE = ACTIVE_PART___IS_IN_REQUESTED_STATE;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___IS_IN_STRICT_STATE = ACTIVE_PART___IS_IN_STRICT_STATE;

	/**
	 * The number of operations of the '<em>Active Variable Fragment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT_OPERATION_COUNT = ACTIVE_PART_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__COVERED_EVENTS = ACTIVE_PART__COVERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__FORBIDDEN_EVENTS = ACTIVE_PART__FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__INTERRUPTING_EVENTS = ACTIVE_PART__INTERRUPTING_EVENTS;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__CONSIDERED_EVENTS = ACTIVE_PART__CONSIDERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__IGNORED_EVENTS = ACTIVE_PART__IGNORED_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__ENABLED_EVENTS = ACTIVE_PART__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__PARENT_ACTIVE_INTERACTION = ACTIVE_PART__PARENT_ACTIVE_INTERACTION;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__ENABLED_NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__VARIABLE_MAP = ACTIVE_PART__VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__EOBJECT_VARIABLE_MAP = ACTIVE_PART__EOBJECT_VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__INTERACTION_FRAGMENT = ACTIVE_PART__INTERACTION_FRAGMENT;

	/**
	 * The number of structural features of the '<em>Active Violation Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION_FEATURE_COUNT = ACTIVE_PART_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___GET_VALUE__VARIABLE = ACTIVE_PART___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___GET_REQUESTED_EVENTS = ACTIVE_PART___GET_REQUESTED_EVENTS;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___IS_IN_REQUESTED_STATE = ACTIVE_PART___IS_IN_REQUESTED_STATE;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___IS_IN_STRICT_STATE = ACTIVE_PART___IS_IN_STRICT_STATE;

	/**
	 * The number of operations of the '<em>Active Violation Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION_OPERATION_COUNT = ACTIVE_PART_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__COVERED_EVENTS = ACTIVE_PART__COVERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__FORBIDDEN_EVENTS = ACTIVE_PART__FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__INTERRUPTING_EVENTS = ACTIVE_PART__INTERRUPTING_EVENTS;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__CONSIDERED_EVENTS = ACTIVE_PART__CONSIDERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__IGNORED_EVENTS = ACTIVE_PART__IGNORED_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__ENABLED_EVENTS = ACTIVE_PART__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__PARENT_ACTIVE_INTERACTION = ACTIVE_PART__PARENT_ACTIVE_INTERACTION;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__ENABLED_NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__VARIABLE_MAP = ACTIVE_PART__VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__EOBJECT_VARIABLE_MAP = ACTIVE_PART__EOBJECT_VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__INTERACTION_FRAGMENT = ACTIVE_PART__INTERACTION_FRAGMENT;

	/**
	 * The number of structural features of the '<em>Active Wait Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION_FEATURE_COUNT = ACTIVE_PART_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___GET_VALUE__VARIABLE = ACTIVE_PART___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___GET_REQUESTED_EVENTS = ACTIVE_PART___GET_REQUESTED_EVENTS;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___IS_IN_REQUESTED_STATE = ACTIVE_PART___IS_IN_REQUESTED_STATE;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___IS_IN_STRICT_STATE = ACTIVE_PART___IS_IN_STRICT_STATE;

	/**
	 * The number of operations of the '<em>Active Wait Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION_OPERATION_COUNT = ACTIVE_PART_OPERATION_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Active Message Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER___INIT__PARAMETERVALUE = 0;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER___UPDATE__PARAMETERVALUE_ACTIVEPART_ACTIVESCENARIO_SMLRUNTIMESTATE = 1;

	/**
	 * The operation id for the '<em>Has Side Effects On Unification</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER___HAS_SIDE_EFFECTS_ON_UNIFICATION = 2;

	/**
	 * The operation id for the '<em>Execute Side Effects On Unification</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER___EXECUTE_SIDE_EFFECTS_ON_UNIFICATION__PARAMETERVALUE_PARAMETERVALUE_ACTIVESCENARIO_SMLRUNTIMESTATE = 3;

	/**
	 * The number of operations of the '<em>Active Message Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_OPERATION_COUNT = 4;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION__PARAMETER = ACTIVE_MESSAGE_PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Active Message Parameter With Value Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION_FEATURE_COUNT = ACTIVE_MESSAGE_PARAMETER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION___INIT__PARAMETERVALUE = ACTIVE_MESSAGE_PARAMETER___INIT__PARAMETERVALUE;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION___UPDATE__PARAMETERVALUE_ACTIVEPART_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_MESSAGE_PARAMETER___UPDATE__PARAMETERVALUE_ACTIVEPART_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Has Side Effects On Unification</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION___HAS_SIDE_EFFECTS_ON_UNIFICATION = ACTIVE_MESSAGE_PARAMETER___HAS_SIDE_EFFECTS_ON_UNIFICATION;

	/**
	 * The operation id for the '<em>Execute Side Effects On Unification</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION___EXECUTE_SIDE_EFFECTS_ON_UNIFICATION__PARAMETERVALUE_PARAMETERVALUE_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_MESSAGE_PARAMETER___EXECUTE_SIDE_EFFECTS_ON_UNIFICATION__PARAMETERVALUE_PARAMETERVALUE_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The number of operations of the '<em>Active Message Parameter With Value Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION_OPERATION_COUNT = ACTIVE_MESSAGE_PARAMETER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR__PARAMETER = ACTIVE_MESSAGE_PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Active Message Parameter With Bind To Var</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR_FEATURE_COUNT = ACTIVE_MESSAGE_PARAMETER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR___INIT__PARAMETERVALUE = ACTIVE_MESSAGE_PARAMETER___INIT__PARAMETERVALUE;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR___UPDATE__PARAMETERVALUE_ACTIVEPART_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_MESSAGE_PARAMETER___UPDATE__PARAMETERVALUE_ACTIVEPART_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Has Side Effects On Unification</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR___HAS_SIDE_EFFECTS_ON_UNIFICATION = ACTIVE_MESSAGE_PARAMETER___HAS_SIDE_EFFECTS_ON_UNIFICATION;

	/**
	 * The operation id for the '<em>Execute Side Effects On Unification</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR___EXECUTE_SIDE_EFFECTS_ON_UNIFICATION__PARAMETERVALUE_PARAMETERVALUE_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_MESSAGE_PARAMETER___EXECUTE_SIDE_EFFECTS_ON_UNIFICATION__PARAMETERVALUE_PARAMETERVALUE_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The number of operations of the '<em>Active Message Parameter With Bind To Var</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR_OPERATION_COUNT = ACTIVE_MESSAGE_PARAMETER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD__PARAMETER = ACTIVE_MESSAGE_PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Active Message Parameter With Wildcard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD_FEATURE_COUNT = ACTIVE_MESSAGE_PARAMETER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD___INIT__PARAMETERVALUE = ACTIVE_MESSAGE_PARAMETER___INIT__PARAMETERVALUE;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD___UPDATE__PARAMETERVALUE_ACTIVEPART_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_MESSAGE_PARAMETER___UPDATE__PARAMETERVALUE_ACTIVEPART_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Has Side Effects On Unification</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD___HAS_SIDE_EFFECTS_ON_UNIFICATION = ACTIVE_MESSAGE_PARAMETER___HAS_SIDE_EFFECTS_ON_UNIFICATION;

	/**
	 * The operation id for the '<em>Execute Side Effects On Unification</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD___EXECUTE_SIDE_EFFECTS_ON_UNIFICATION__PARAMETERVALUE_PARAMETERVALUE_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_MESSAGE_PARAMETER___EXECUTE_SIDE_EFFECTS_ON_UNIFICATION__PARAMETERVALUE_PARAMETERVALUE_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The number of operations of the '<em>Active Message Parameter With Wildcard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD_OPERATION_COUNT = ACTIVE_MESSAGE_PARAMETER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constraint Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT__CONSTRAINT_MESSAGE_EVENT = 0;

	/**
	 * The feature id for the '<em><b>Active Message Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT__ACTIVE_MESSAGE_PARAMETERS = 1;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT__MESSAGE = 2;

	/**
	 * The number of structural features of the '<em>Active Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = 0;

	/**
	 * The operation id for the '<em>Update Constraint Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE = 1;

	/**
	 * The operation id for the '<em>Add To Parent Specific Constraint List</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT = 2;

	/**
	 * The number of operations of the '<em>Active Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_OPERATION_COUNT = 3;

	/**
	 * The feature id for the '<em><b>Constraint Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_CONSIDER__CONSTRAINT_MESSAGE_EVENT = ACTIVE_CONSTRAINT__CONSTRAINT_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Active Message Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_CONSIDER__ACTIVE_MESSAGE_PARAMETERS = ACTIVE_CONSTRAINT__ACTIVE_MESSAGE_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_CONSIDER__MESSAGE = ACTIVE_CONSTRAINT__MESSAGE;

	/**
	 * The number of structural features of the '<em>Active Constraint Consider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_CONSIDER_FEATURE_COUNT = ACTIVE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_CONSIDER___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_CONSTRAINT___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Update Constraint Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_CONSIDER___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_CONSTRAINT___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Add To Parent Specific Constraint List</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_CONSIDER___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT = ACTIVE_CONSTRAINT___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT;

	/**
	 * The number of operations of the '<em>Active Constraint Consider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_CONSIDER_OPERATION_COUNT = ACTIVE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constraint Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_IGNORE__CONSTRAINT_MESSAGE_EVENT = ACTIVE_CONSTRAINT__CONSTRAINT_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Active Message Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_IGNORE__ACTIVE_MESSAGE_PARAMETERS = ACTIVE_CONSTRAINT__ACTIVE_MESSAGE_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_IGNORE__MESSAGE = ACTIVE_CONSTRAINT__MESSAGE;

	/**
	 * The number of structural features of the '<em>Active Constraint Ignore</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_IGNORE_FEATURE_COUNT = ACTIVE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_IGNORE___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_CONSTRAINT___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Update Constraint Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_IGNORE___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_CONSTRAINT___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Add To Parent Specific Constraint List</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_IGNORE___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT = ACTIVE_CONSTRAINT___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT;

	/**
	 * The number of operations of the '<em>Active Constraint Ignore</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_IGNORE_OPERATION_COUNT = ACTIVE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constraint Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_INTERRUPT__CONSTRAINT_MESSAGE_EVENT = ACTIVE_CONSTRAINT__CONSTRAINT_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Active Message Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_INTERRUPT__ACTIVE_MESSAGE_PARAMETERS = ACTIVE_CONSTRAINT__ACTIVE_MESSAGE_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_INTERRUPT__MESSAGE = ACTIVE_CONSTRAINT__MESSAGE;

	/**
	 * The number of structural features of the '<em>Active Constraint Interrupt</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_INTERRUPT_FEATURE_COUNT = ACTIVE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_INTERRUPT___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_CONSTRAINT___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Update Constraint Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_INTERRUPT___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_CONSTRAINT___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Add To Parent Specific Constraint List</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_INTERRUPT___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT = ACTIVE_CONSTRAINT___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT;

	/**
	 * The number of operations of the '<em>Active Constraint Interrupt</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_INTERRUPT_OPERATION_COUNT = ACTIVE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constraint Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_FORBIDDEN__CONSTRAINT_MESSAGE_EVENT = ACTIVE_CONSTRAINT__CONSTRAINT_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Active Message Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_FORBIDDEN__ACTIVE_MESSAGE_PARAMETERS = ACTIVE_CONSTRAINT__ACTIVE_MESSAGE_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_FORBIDDEN__MESSAGE = ACTIVE_CONSTRAINT__MESSAGE;

	/**
	 * The number of structural features of the '<em>Active Constraint Forbidden</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_FORBIDDEN_FEATURE_COUNT = ACTIVE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_FORBIDDEN___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_CONSTRAINT___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Update Constraint Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_FORBIDDEN___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_CONSTRAINT___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Add To Parent Specific Constraint List</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_FORBIDDEN___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT = ACTIVE_CONSTRAINT___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT;

	/**
	 * The number of operations of the '<em>Active Constraint Forbidden</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_FORBIDDEN_OPERATION_COUNT = ACTIVE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Parameter Ranges Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_RANGES_PROVIDER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_RANGES_PROVIDER___INIT__CONFIGURATION = 0;

	/**
	 * The operation id for the '<em>Get Parameter Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_RANGES_PROVIDER___GET_PARAMETER_VALUES__ETYPEDELEMENT_SMLOBJECTSYSTEM = 1;

	/**
	 * The operation id for the '<em>Get Singel Parameter Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_RANGES_PROVIDER___GET_SINGEL_PARAMETER_VALUE__OBJECT = 2;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_RANGES_PROVIDER___INIT__ELIST = 3;

	/**
	 * The operation id for the '<em>Contains Parameter Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_RANGES_PROVIDER___CONTAINS_PARAMETER_VALUES__ETYPEDELEMENT = 4;

	/**
	 * The number of operations of the '<em>Parameter Ranges Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_RANGES_PROVIDER_OPERATION_COUNT = 5;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_BLOCKED_INFORMATION__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Message Event String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_BLOCKED_INFORMATION__MESSAGE_EVENT_STRING = 1;

	/**
	 * The number of structural features of the '<em>Message Event Blocked Information</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_BLOCKED_INFORMATION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Message Event Blocked Information</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_BLOCKED_INFORMATION_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Active Scenario To Active Scenario Progress Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_ACTIVE_SCENARIO_INITIALIZATIONS__ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP = 0;

	/**
	 * The number of structural features of the '<em>Multi Active Scenario Initializations</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_ACTIVE_SCENARIO_INITIALIZATIONS_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Multi Active Scenario Initializations</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_ACTIVE_SCENARIO_INITIALIZATIONS_OPERATION_COUNT = 0;

	/**
	 * The number of structural features of the '<em>Message Event Extension Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_EXTENSION_INTERFACE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_EXTENSION_INTERFACE___INIT__CONFIGURATION = 0;

	/**
	 * The number of operations of the '<em>Message Event Extension Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_EXTENSION_INTERFACE_OPERATION_COUNT = 1;

	/**
	 * The number of structural features of the '<em>Message Event Side Effects Executor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR_FEATURE_COUNT = MESSAGE_EVENT_EXTENSION_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR___INIT__CONFIGURATION = MESSAGE_EVENT_EXTENSION_INTERFACE___INIT__CONFIGURATION;

	/**
	 * The operation id for the '<em>Execute Side Effects</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR___EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = MESSAGE_EVENT_EXTENSION_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Can Execute Side Effects</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR___CAN_EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = MESSAGE_EVENT_EXTENSION_INTERFACE_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Message Event Side Effects Executor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR_OPERATION_COUNT = MESSAGE_EVENT_EXTENSION_INTERFACE_OPERATION_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Message Event Is Independent Evaluator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR_FEATURE_COUNT = MESSAGE_EVENT_EXTENSION_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR___INIT__CONFIGURATION = MESSAGE_EVENT_EXTENSION_INTERFACE___INIT__CONFIGURATION;

	/**
	 * The operation id for the '<em>Is Independent</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR___IS_INDEPENDENT__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = MESSAGE_EVENT_EXTENSION_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Message Event Is Independent Evaluator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR_OPERATION_COUNT = MESSAGE_EVENT_EXTENSION_INTERFACE_OPERATION_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_EOBJECT_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_EOBJECT_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>EClass To EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_EOBJECT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>EClass To EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_EOBJECT_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TO_EOBJECT_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TO_EOBJECT_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Role To EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TO_EOBJECT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Role To EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TO_EOBJECT_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Static EObject To Dynamic EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Static EObject To Dynamic EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Active Scenario To Active Scenario Progress Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Active Scenario To Active Scenario Progress Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.EventToTransitionMapEntryImpl <em>Event To Transition Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.EventToTransitionMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getEventToTransitionMapEntry()
	 * @generated
	 */
	int EVENT_TO_TRANSITION_MAP_ENTRY = 49;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TO_TRANSITION_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TO_TRANSITION_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Event To Transition Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TO_TRANSITION_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Event To Transition Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TO_TRANSITION_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_BOOLEAN_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_BOOLEAN_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>String To Boolean Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_BOOLEAN_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>String To Boolean Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_BOOLEAN_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_STRING_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_STRING_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>String To String Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_STRING_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>String To String Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_STRING_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_EOBJECT_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_EOBJECT_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>String To EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_EOBJECT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>String To EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_EOBJECT_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Active Interaction Key Wrapper To Active Interaction Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Active Interaction Key Wrapper To Active Interaction Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Active Scenario Key Wrapper To Active Scenario Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Active Scenario Key Wrapper To Active Scenario Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Object System Key Wrapper To Object System Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Object System Key Wrapper To Object System Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Object System Key Wrapper To Dynamic Object Container Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Object System Key Wrapper To Dynamic Object Container Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>State Key Wrapper To State Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>State Key Wrapper To State Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_TO_OBJECT_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_TO_OBJECT_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Variable To Object Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_TO_OBJECT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Variable To Object Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_TO_OBJECT_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_TO_EOBJECT_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_TO_EOBJECT_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Variable To EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_TO_EOBJECT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Variable To EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_TO_EOBJECT_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.ViolationKind <em>Violation Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.ViolationKind
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getViolationKind()
	 * @generated
	 */
	int VIOLATION_KIND = 61;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.ActiveScenarioProgress <em>Active Scenario Progress</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.ActiveScenarioProgress
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioProgress()
	 * @generated
	 */
	int ACTIVE_SCENARIO_PROGRESS = 62;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.BlockedType <em>Blocked Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.BlockedType
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getBlockedType()
	 * @generated
	 */
	int BLOCKED_TYPE = 63;

	/**
	 * The meta object id for the '<em>Active Interaction Key Wrapper</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.keywrapper.ActiveInteractionKeyWrapper
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveInteractionKeyWrapper()
	 * @generated
	 */
	int ACTIVE_INTERACTION_KEY_WRAPPER = 65;

	/**
	 * The meta object id for the '<em>Active Scenario Key Wrapper</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioKeyWrapper()
	 * @generated
	 */
	int ACTIVE_SCENARIO_KEY_WRAPPER = 66;

	/**
	 * The meta object id for the '<em>Object System Key Wrapper</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.keywrapper.ObjectSystemKeyWrapper
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getObjectSystemKeyWrapper()
	 * @generated
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER = 67;

	/**
	 * The meta object id for the '<em>State Key Wrapper</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.keywrapper.StateKeyWrapper
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getStateKeyWrapper()
	 * @generated
	 */
	int STATE_KEY_WRAPPER = 68;

	/**
	 * The meta object id for the '<em>Active Scenario Role Bindings Key Wrapper</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.keywrapper.ActiveScenarioRoleBindingsKeyWrapper
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioRoleBindingsKeyWrapper()
	 * @generated
	 */
	int ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER = 69;

	/**
	 * The meta object id for the '<em>Parameter Values</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.logic.ParameterRanges.ParameterValues
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getParameterValues()
	 * @generated
	 */
	int PARAMETER_VALUES = 64;


	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph <em>SML Runtime State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SML Runtime State Graph</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeStateGraph
	 * @generated
	 */
	EClass getSMLRuntimeStateGraph();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getConfiguration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Configuration</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getConfiguration()
	 * @see #getSMLRuntimeStateGraph()
	 * @generated
	 */
	EReference getSMLRuntimeStateGraph_Configuration();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getElementContainer <em>Element Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Element Container</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getElementContainer()
	 * @see #getSMLRuntimeStateGraph()
	 * @generated
	 */
	EReference getSMLRuntimeStateGraph_ElementContainer();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getParameterRangesProvider <em>Parameter Ranges Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameter Ranges Provider</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getParameterRangesProvider()
	 * @see #getSMLRuntimeStateGraph()
	 * @generated
	 */
	EReference getSMLRuntimeStateGraph_ParameterRangesProvider();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getStates <em>States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>States</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getStates()
	 * @see #getSMLRuntimeStateGraph()
	 * @generated
	 */
	EReference getSMLRuntimeStateGraph_States();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getStartState <em>Start State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Start State</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getStartState()
	 * @see #getSMLRuntimeStateGraph()
	 * @generated
	 */
	EReference getSMLRuntimeStateGraph_StartState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#init(org.scenariotools.sml.runtime.configuration.Configuration) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeStateGraph#init(org.scenariotools.sml.runtime.configuration.Configuration)
	 * @generated
	 */
	EOperation getSMLRuntimeStateGraph__Init__Configuration();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#generateSuccessor(org.scenariotools.sml.runtime.SMLRuntimeState, org.scenariotools.sml.runtime.Event) <em>Generate Successor</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Generate Successor</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeStateGraph#generateSuccessor(org.scenariotools.sml.runtime.SMLRuntimeState, org.scenariotools.sml.runtime.Event)
	 * @generated
	 */
	EOperation getSMLRuntimeStateGraph__GenerateSuccessor__SMLRuntimeState_Event();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#generateAllSuccessors(org.scenariotools.sml.runtime.SMLRuntimeState) <em>Generate All Successors</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Generate All Successors</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeStateGraph#generateAllSuccessors(org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getSMLRuntimeStateGraph__GenerateAllSuccessors__SMLRuntimeState();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.SMLRuntimeState <em>SML Runtime State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SML Runtime State</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState
	 * @generated
	 */
	EClass getSMLRuntimeState();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getActiveScenarios <em>Active Scenarios</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Active Scenarios</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#getActiveScenarios()
	 * @see #getSMLRuntimeState()
	 * @generated
	 */
	EReference getSMLRuntimeState_ActiveScenarios();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getDynamicObjectContainer <em>Dynamic Object Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dynamic Object Container</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#getDynamicObjectContainer()
	 * @see #getSMLRuntimeState()
	 * @generated
	 */
	EReference getSMLRuntimeState_DynamicObjectContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getComputedInitializingMessageEvents <em>Computed Initializing Message Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Computed Initializing Message Events</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#getComputedInitializingMessageEvents()
	 * @see #getSMLRuntimeState()
	 * @generated
	 */
	EReference getSMLRuntimeState_ComputedInitializingMessageEvents();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getTerminatedExistentialScenariosFromLastPerformStep <em>Terminated Existential Scenarios From Last Perform Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Terminated Existential Scenarios From Last Perform Step</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#getTerminatedExistentialScenariosFromLastPerformStep()
	 * @see #getSMLRuntimeState()
	 * @generated
	 */
	EReference getSMLRuntimeState_TerminatedExistentialScenariosFromLastPerformStep();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getMessageEventBlockedInformation <em>Message Event Blocked Information</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Message Event Blocked Information</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#getMessageEventBlockedInformation()
	 * @see #getSMLRuntimeState()
	 * @generated
	 */
	EReference getSMLRuntimeState_MessageEventBlockedInformation();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getObjectSystem <em>Object System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object System</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#getObjectSystem()
	 * @see #getSMLRuntimeState()
	 * @generated
	 */
	EReference getSMLRuntimeState_ObjectSystem();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.runtime.SMLRuntimeState#isSafetyViolationOccurredInGuarantees <em>Safety Violation Occurred In Guarantees</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Safety Violation Occurred In Guarantees</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#isSafetyViolationOccurredInGuarantees()
	 * @see #getSMLRuntimeState()
	 * @generated
	 */
	EAttribute getSMLRuntimeState_SafetyViolationOccurredInGuarantees();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.runtime.SMLRuntimeState#isSafetyViolationOccurredInAssumptions <em>Safety Violation Occurred In Assumptions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Safety Violation Occurred In Assumptions</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#isSafetyViolationOccurredInAssumptions()
	 * @see #getSMLRuntimeState()
	 * @generated
	 */
	EAttribute getSMLRuntimeState_SafetyViolationOccurredInAssumptions();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getEventToTransitionMap <em>Event To Transition Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Event To Transition Map</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#getEventToTransitionMap()
	 * @see #getSMLRuntimeState()
	 * @generated
	 */
	EReference getSMLRuntimeState_EventToTransitionMap();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getEnabledEvents <em>Enabled Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Enabled Events</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#getEnabledEvents()
	 * @see #getSMLRuntimeState()
	 * @generated
	 */
	EReference getSMLRuntimeState_EnabledEvents();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getOutgoingTransition <em>Outgoing Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Outgoing Transition</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#getOutgoingTransition()
	 * @see #getSMLRuntimeState()
	 * @generated
	 */
	EReference getSMLRuntimeState_OutgoingTransition();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getIncomingTransition <em>Incoming Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Incoming Transition</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#getIncomingTransition()
	 * @see #getSMLRuntimeState()
	 * @generated
	 */
	EReference getSMLRuntimeState_IncomingTransition();

	/**
	 * Returns the meta object for the container reference '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getStateGraph <em>State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>State Graph</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#getStateGraph()
	 * @see #getSMLRuntimeState()
	 * @generated
	 */
	EReference getSMLRuntimeState_StateGraph();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.runtime.SMLRuntimeState#isSystemChoseToWait <em>System Chose To Wait</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>System Chose To Wait</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#isSystemChoseToWait()
	 * @see #getSMLRuntimeState()
	 * @generated
	 */
	EAttribute getSMLRuntimeState_SystemChoseToWait();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLRuntimeState#init(org.scenariotools.sml.runtime.SMLObjectSystem) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#init(org.scenariotools.sml.runtime.SMLObjectSystem)
	 * @generated
	 */
	EOperation getSMLRuntimeState__Init__SMLObjectSystem();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLRuntimeState#updateEnabledMessageEvents() <em>Update Enabled Message Events</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Enabled Message Events</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#updateEnabledMessageEvents()
	 * @generated
	 */
	EOperation getSMLRuntimeState__UpdateEnabledMessageEvents();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLRuntimeState#performStep(org.scenariotools.sml.runtime.Event) <em>Perform Step</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Perform Step</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#performStep(org.scenariotools.sml.runtime.Event)
	 * @generated
	 */
	EOperation getSMLRuntimeState__PerformStep__Event();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveScenario <em>Active Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Scenario</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveScenario
	 * @generated
	 */
	EClass getActiveScenario();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ActiveScenario#getScenario <em>Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scenario</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#getScenario()
	 * @see #getActiveScenario()
	 * @generated
	 */
	EReference getActiveScenario_Scenario();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.sml.runtime.ActiveScenario#getMainActiveInteraction <em>Main Active Interaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Main Active Interaction</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#getMainActiveInteraction()
	 * @see #getActiveScenario()
	 * @generated
	 */
	EReference getActiveScenario_MainActiveInteraction();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.ActiveScenario#getAlphabet <em>Alphabet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Alphabet</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#getAlphabet()
	 * @see #getActiveScenario()
	 * @generated
	 */
	EReference getActiveScenario_Alphabet();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ActiveScenario#getRoleBindings <em>Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role Bindings</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#getRoleBindings()
	 * @see #getActiveScenario()
	 * @generated
	 */
	EReference getActiveScenario_RoleBindings();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.runtime.ActiveScenario#isSafetyViolationOccurred <em>Safety Violation Occurred</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Safety Violation Occurred</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#isSafetyViolationOccurred()
	 * @see #getActiveScenario()
	 * @generated
	 */
	EAttribute getActiveScenario_SafetyViolationOccurred();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.ActiveScenario#getContextHelperClassInstances <em>Context Helper Class Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Context Helper Class Instances</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#getContextHelperClassInstances()
	 * @see #getActiveScenario()
	 * @generated
	 */
	EReference getActiveScenario_ContextHelperClassInstances();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveScenario#performStep(org.scenariotools.sml.runtime.MessageEvent, org.scenariotools.sml.runtime.SMLRuntimeState) <em>Perform Step</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Perform Step</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#performStep(org.scenariotools.sml.runtime.MessageEvent, org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getActiveScenario__PerformStep__MessageEvent_SMLRuntimeState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveScenario#init(org.scenariotools.sml.runtime.SMLObjectSystem, org.scenariotools.sml.runtime.DynamicObjectContainer, org.scenariotools.sml.runtime.SMLRuntimeState, org.scenariotools.sml.runtime.MessageEvent) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#init(org.scenariotools.sml.runtime.SMLObjectSystem, org.scenariotools.sml.runtime.DynamicObjectContainer, org.scenariotools.sml.runtime.SMLRuntimeState, org.scenariotools.sml.runtime.MessageEvent)
	 * @generated
	 */
	EOperation getActiveScenario__Init__SMLObjectSystem_DynamicObjectContainer_SMLRuntimeState_MessageEvent();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveScenario#isBlocked(org.scenariotools.sml.runtime.MessageEvent) <em>Is Blocked</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Blocked</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#isBlocked(org.scenariotools.sml.runtime.MessageEvent)
	 * @generated
	 */
	EOperation getActiveScenario__IsBlocked__MessageEvent();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveScenario#getRequestedEvents() <em>Get Requested Events</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Requested Events</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#getRequestedEvents()
	 * @generated
	 */
	EOperation getActiveScenario__GetRequestedEvents();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveScenario#isInRequestedState() <em>Is In Requested State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is In Requested State</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#isInRequestedState()
	 * @generated
	 */
	EOperation getActiveScenario__IsInRequestedState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveScenario#isInStrictState() <em>Is In Strict State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is In Strict State</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#isInStrictState()
	 * @generated
	 */
	EOperation getActiveScenario__IsInStrictState();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.Context <em>Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Context</em>'.
	 * @see org.scenariotools.sml.runtime.Context
	 * @generated
	 */
	EClass getContext();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.Context#getValue(org.scenariotools.sml.expressions.scenarioExpressions.Variable) <em>Get Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Value</em>' operation.
	 * @see org.scenariotools.sml.runtime.Context#getValue(org.scenariotools.sml.expressions.scenarioExpressions.Variable)
	 * @generated
	 */
	EOperation getContext__GetValue__Variable();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ElementContainer <em>Element Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element Container</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer
	 * @generated
	 */
	EClass getElementContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.ElementContainer#getActiveScenarios <em>Active Scenarios</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Active Scenarios</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getActiveScenarios()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveScenarios();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.ElementContainer#getActiveInteractions <em>Active Interactions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Active Interactions</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getActiveInteractions()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveInteractions();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.ElementContainer#getObjectSystems <em>Object Systems</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Systems</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getObjectSystems()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ObjectSystems();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.ElementContainer#getActiveScenarioRoleBindings <em>Active Scenario Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Active Scenario Role Bindings</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getActiveScenarioRoleBindings()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveScenarioRoleBindings();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.ElementContainer#getDynamicObjectContainer <em>Dynamic Object Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dynamic Object Container</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getDynamicObjectContainer()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_DynamicObjectContainer();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.ElementContainer#getActiveInteractionKeyWrapperToActiveInteractionMap <em>Active Interaction Key Wrapper To Active Interaction Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Active Interaction Key Wrapper To Active Interaction Map</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getActiveInteractionKeyWrapperToActiveInteractionMap()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveInteractionKeyWrapperToActiveInteractionMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.ElementContainer#getActiveScenarioKeyWrapperToActiveScenarioMap <em>Active Scenario Key Wrapper To Active Scenario Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Active Scenario Key Wrapper To Active Scenario Map</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getActiveScenarioKeyWrapperToActiveScenarioMap()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveScenarioKeyWrapperToActiveScenarioMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.ElementContainer#getObjectSystemKeyWrapperToObjectSystemMap <em>Object System Key Wrapper To Object System Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Object System Key Wrapper To Object System Map</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getObjectSystemKeyWrapperToObjectSystemMap()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ObjectSystemKeyWrapperToObjectSystemMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.ElementContainer#getObjectSystemKeyWrapperToDynamicObjectContainerMap <em>Object System Key Wrapper To Dynamic Object Container Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Object System Key Wrapper To Dynamic Object Container Map</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getObjectSystemKeyWrapperToDynamicObjectContainerMap()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ObjectSystemKeyWrapperToDynamicObjectContainerMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.ElementContainer#getStateKeyWrapperToStateMap <em>State Key Wrapper To State Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>State Key Wrapper To State Map</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getStateKeyWrapperToStateMap()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_StateKeyWrapperToStateMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.ElementContainer#getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap <em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.runtime.ElementContainer#isEnabled <em>Enabled</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Enabled</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#isEnabled()
	 * @see #getElementContainer()
	 * @generated
	 */
	EAttribute getElementContainer_Enabled();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.sml.runtime.ElementContainer#getWaitEvent <em>Wait Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Wait Event</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getWaitEvent()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_WaitEvent();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ElementContainer#getActiveScenario(org.scenariotools.sml.runtime.ActiveScenario) <em>Get Active Scenario</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Active Scenario</em>' operation.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getActiveScenario(org.scenariotools.sml.runtime.ActiveScenario)
	 * @generated
	 */
	EOperation getElementContainer__GetActiveScenario__ActiveScenario();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ElementContainer#getObjectSystem(org.scenariotools.sml.runtime.SMLObjectSystem) <em>Get Object System</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Object System</em>' operation.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getObjectSystem(org.scenariotools.sml.runtime.SMLObjectSystem)
	 * @generated
	 */
	EOperation getElementContainer__GetObjectSystem__SMLObjectSystem();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ElementContainer#getSMLRuntimeState(org.scenariotools.sml.runtime.SMLRuntimeState) <em>Get SML Runtime State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get SML Runtime State</em>' operation.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getSMLRuntimeState(org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getElementContainer__GetSMLRuntimeState__SMLRuntimeState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ElementContainer#getActiveScenarioRoleBindings(org.scenariotools.sml.runtime.ActiveScenarioRoleBindings) <em>Get Active Scenario Role Bindings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Active Scenario Role Bindings</em>' operation.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getActiveScenarioRoleBindings(org.scenariotools.sml.runtime.ActiveScenarioRoleBindings)
	 * @generated
	 */
	EOperation getElementContainer__GetActiveScenarioRoleBindings__ActiveScenarioRoleBindings();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ElementContainer#getDynamicObjectContainer(org.scenariotools.sml.runtime.DynamicObjectContainer, org.scenariotools.sml.runtime.SMLObjectSystem) <em>Get Dynamic Object Container</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Dynamic Object Container</em>' operation.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getDynamicObjectContainer(org.scenariotools.sml.runtime.DynamicObjectContainer, org.scenariotools.sml.runtime.SMLObjectSystem)
	 * @generated
	 */
	EOperation getElementContainer__GetDynamicObjectContainer__DynamicObjectContainer_SMLObjectSystem();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.SMLObjectSystem <em>SML Object System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SML Object System</em>'.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem
	 * @generated
	 */
	EClass getSMLObjectSystem();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getEClassToEObject <em>EClass To EObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>EClass To EObject</em>'.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#getEClassToEObject()
	 * @see #getSMLObjectSystem()
	 * @generated
	 */
	EReference getSMLObjectSystem_EClassToEObject();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getStaticRoleBindings <em>Static Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Static Role Bindings</em>'.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#getStaticRoleBindings()
	 * @see #getSMLObjectSystem()
	 * @generated
	 */
	EReference getSMLObjectSystem_StaticRoleBindings();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getSpecification <em>Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Specification</em>'.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#getSpecification()
	 * @see #getSMLObjectSystem()
	 * @generated
	 */
	EReference getSMLObjectSystem_Specification();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getRunconfiguration <em>Runconfiguration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Runconfiguration</em>'.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#getRunconfiguration()
	 * @see #getSMLObjectSystem()
	 * @generated
	 */
	EReference getSMLObjectSystem_Runconfiguration();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getMessageEventSideEffectsExecutor <em>Message Event Side Effects Executor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Message Event Side Effects Executor</em>'.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#getMessageEventSideEffectsExecutor()
	 * @see #getSMLObjectSystem()
	 * @generated
	 */
	EReference getSMLObjectSystem_MessageEventSideEffectsExecutor();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getMessageEventIsIndependentEvaluators <em>Message Event Is Independent Evaluators</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Message Event Is Independent Evaluators</em>'.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#getMessageEventIsIndependentEvaluators()
	 * @see #getSMLObjectSystem()
	 * @generated
	 */
	EReference getSMLObjectSystem_MessageEventIsIndependentEvaluators();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getObjects <em>Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Objects</em>'.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#getObjects()
	 * @see #getSMLObjectSystem()
	 * @generated
	 */
	EReference getSMLObjectSystem_Objects();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getControllableObjects <em>Controllable Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Controllable Objects</em>'.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#getControllableObjects()
	 * @see #getSMLObjectSystem()
	 * @generated
	 */
	EReference getSMLObjectSystem_ControllableObjects();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getUncontrollableObjects <em>Uncontrollable Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Uncontrollable Objects</em>'.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#getUncontrollableObjects()
	 * @see #getSMLObjectSystem()
	 * @generated
	 */
	EReference getSMLObjectSystem_UncontrollableObjects();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getRootObjects <em>Root Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Root Objects</em>'.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#getRootObjects()
	 * @see #getSMLObjectSystem()
	 * @generated
	 */
	EReference getSMLObjectSystem_RootObjects();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLObjectSystem#init(org.scenariotools.sml.runtime.configuration.Configuration, org.scenariotools.sml.runtime.SMLRuntimeState) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#init(org.scenariotools.sml.runtime.configuration.Configuration, org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getSMLObjectSystem__Init__Configuration_SMLRuntimeState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLObjectSystem#executeSideEffects(org.scenariotools.sml.runtime.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer) <em>Execute Side Effects</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Execute Side Effects</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#executeSideEffects(org.scenariotools.sml.runtime.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer)
	 * @generated
	 */
	EOperation getSMLObjectSystem__ExecuteSideEffects__MessageEvent_DynamicObjectContainer();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLObjectSystem#canExecuteSideEffects(org.scenariotools.sml.runtime.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer) <em>Can Execute Side Effects</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Can Execute Side Effects</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#canExecuteSideEffects(org.scenariotools.sml.runtime.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer)
	 * @generated
	 */
	EOperation getSMLObjectSystem__CanExecuteSideEffects__MessageEvent_DynamicObjectContainer();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLObjectSystem#isIndependent(org.scenariotools.sml.runtime.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer) <em>Is Independent</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Independent</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#isIndependent(org.scenariotools.sml.runtime.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer)
	 * @generated
	 */
	EOperation getSMLObjectSystem__IsIndependent__MessageEvent_DynamicObjectContainer();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLObjectSystem#isNonSpontaneousMessageEvent(org.scenariotools.sml.runtime.MessageEvent) <em>Is Non Spontaneous Message Event</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Non Spontaneous Message Event</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#isNonSpontaneousMessageEvent(org.scenariotools.sml.runtime.MessageEvent)
	 * @generated
	 */
	EOperation getSMLObjectSystem__IsNonSpontaneousMessageEvent__MessageEvent();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getScenariosForInitMessageEvent(org.scenariotools.sml.runtime.MessageEvent) <em>Get Scenarios For Init Message Event</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Scenarios For Init Message Event</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#getScenariosForInitMessageEvent(org.scenariotools.sml.runtime.MessageEvent)
	 * @generated
	 */
	EOperation getSMLObjectSystem__GetScenariosForInitMessageEvent__MessageEvent();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getInitializingEnvironmentMessageEvents(org.scenariotools.sml.runtime.DynamicObjectContainer) <em>Get Initializing Environment Message Events</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Initializing Environment Message Events</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#getInitializingEnvironmentMessageEvents(org.scenariotools.sml.runtime.DynamicObjectContainer)
	 * @generated
	 */
	EOperation getSMLObjectSystem__GetInitializingEnvironmentMessageEvents__DynamicObjectContainer();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLObjectSystem#isControllable(org.eclipse.emf.ecore.EObject) <em>Is Controllable</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Controllable</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#isControllable(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	EOperation getSMLObjectSystem__IsControllable__EObject();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLObjectSystem#isEnvironmentMessageEvent(org.scenariotools.sml.runtime.Event) <em>Is Environment Message Event</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Environment Message Event</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#isEnvironmentMessageEvent(org.scenariotools.sml.runtime.Event)
	 * @generated
	 */
	EOperation getSMLObjectSystem__IsEnvironmentMessageEvent__Event();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLObjectSystem#containsEObject(org.eclipse.emf.ecore.EObject) <em>Contains EObject</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Contains EObject</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#containsEObject(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	EOperation getSMLObjectSystem__ContainsEObject__EObject();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActivePart <em>Active Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Part</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart
	 * @generated
	 */
	EClass getActivePart();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.ActivePart#getNestedActiveInteractions <em>Nested Active Interactions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Nested Active Interactions</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getNestedActiveInteractions()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_NestedActiveInteractions();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.ActivePart#getCoveredEvents <em>Covered Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Covered Events</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getCoveredEvents()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_CoveredEvents();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.ActivePart#getForbiddenEvents <em>Forbidden Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Forbidden Events</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getForbiddenEvents()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_ForbiddenEvents();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.ActivePart#getInterruptingEvents <em>Interrupting Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Interrupting Events</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getInterruptingEvents()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_InterruptingEvents();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.ActivePart#getConsideredEvents <em>Considered Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Considered Events</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getConsideredEvents()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_ConsideredEvents();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.ActivePart#getIgnoredEvents <em>Ignored Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Ignored Events</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getIgnoredEvents()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_IgnoredEvents();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.ActivePart#getEnabledEvents <em>Enabled Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Enabled Events</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getEnabledEvents()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_EnabledEvents();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ActivePart#getParentActiveInteraction <em>Parent Active Interaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parent Active Interaction</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getParentActiveInteraction()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_ParentActiveInteraction();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.ActivePart#getEnabledNestedActiveInteractions <em>Enabled Nested Active Interactions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Enabled Nested Active Interactions</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getEnabledNestedActiveInteractions()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_EnabledNestedActiveInteractions();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.ActivePart#getVariableMap <em>Variable Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Variable Map</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getVariableMap()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_VariableMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.ActivePart#getEObjectVariableMap <em>EObject Variable Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>EObject Variable Map</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getEObjectVariableMap()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_EObjectVariableMap();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ActivePart#getInteractionFragment <em>Interaction Fragment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Interaction Fragment</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getInteractionFragment()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_InteractionFragment();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActivePart#performStep(org.scenariotools.sml.runtime.MessageEvent, org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState) <em>Perform Step</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Perform Step</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActivePart#performStep(org.scenariotools.sml.runtime.MessageEvent, org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getActivePart__PerformStep__MessageEvent_ActiveScenario_SMLRuntimeState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActivePart#postPerformStep(org.scenariotools.sml.runtime.MessageEvent, org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState) <em>Post Perform Step</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Post Perform Step</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActivePart#postPerformStep(org.scenariotools.sml.runtime.MessageEvent, org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getActivePart__PostPerformStep__MessageEvent_ActiveScenario_SMLRuntimeState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActivePart#init(org.scenariotools.sml.runtime.ActiveScenarioRoleBindings, org.scenariotools.sml.runtime.ActivePart, org.scenariotools.sml.runtime.ActiveScenario) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActivePart#init(org.scenariotools.sml.runtime.ActiveScenarioRoleBindings, org.scenariotools.sml.runtime.ActivePart, org.scenariotools.sml.runtime.ActiveScenario)
	 * @generated
	 */
	EOperation getActivePart__Init__ActiveScenarioRoleBindings_ActivePart_ActiveScenario();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActivePart#isViolatingInInteraction(org.scenariotools.sml.runtime.MessageEvent, boolean) <em>Is Violating In Interaction</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Violating In Interaction</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActivePart#isViolatingInInteraction(org.scenariotools.sml.runtime.MessageEvent, boolean)
	 * @generated
	 */
	EOperation getActivePart__IsViolatingInInteraction__MessageEvent_boolean();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActivePart#updateMessageEvents(org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState) <em>Update Message Events</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Message Events</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActivePart#updateMessageEvents(org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getActivePart__UpdateMessageEvents__ActiveScenario_SMLRuntimeState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActivePart#getRequestedEvents() <em>Get Requested Events</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Requested Events</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActivePart#getRequestedEvents()
	 * @generated
	 */
	EOperation getActivePart__GetRequestedEvents();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActivePart#isBlocked(org.scenariotools.sml.runtime.MessageEvent, boolean) <em>Is Blocked</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Blocked</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActivePart#isBlocked(org.scenariotools.sml.runtime.MessageEvent, boolean)
	 * @generated
	 */
	EOperation getActivePart__IsBlocked__MessageEvent_boolean();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActivePart#enable(org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState) <em>Enable</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Enable</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActivePart#enable(org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getActivePart__Enable__ActiveScenario_SMLRuntimeState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActivePart#isInRequestedState() <em>Is In Requested State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is In Requested State</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActivePart#isInRequestedState()
	 * @generated
	 */
	EOperation getActivePart__IsInRequestedState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActivePart#isInStrictState() <em>Is In Strict State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is In Strict State</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActivePart#isInStrictState()
	 * @generated
	 */
	EOperation getActivePart__IsInStrictState();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveAlternative <em>Active Alternative</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Alternative</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveAlternative
	 * @generated
	 */
	EClass getActiveAlternative();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveCase <em>Active Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Case</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveCase
	 * @generated
	 */
	EClass getActiveCase();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ActiveCase#getCase <em>Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Case</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveCase#getCase()
	 * @see #getActiveCase()
	 * @generated
	 */
	EReference getActiveCase_Case();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveInteraction <em>Active Interaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Interaction</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveInteraction
	 * @generated
	 */
	EClass getActiveInteraction();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.ActiveInteraction#getActiveConstraints <em>Active Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Active Constraints</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveInteraction#getActiveConstraints()
	 * @see #getActiveInteraction()
	 * @generated
	 */
	EReference getActiveInteraction_ActiveConstraints();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveInterruptCondition <em>Active Interrupt Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Interrupt Condition</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveInterruptCondition
	 * @generated
	 */
	EClass getActiveInterruptCondition();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveLoop <em>Active Loop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Loop</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveLoop
	 * @generated
	 */
	EClass getActiveLoop();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveModalMessage <em>Active Modal Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Modal Message</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveModalMessage
	 * @generated
	 */
	EClass getActiveModalMessage();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.ActiveModalMessage#getActiveMessageParameters <em>Active Message Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Active Message Parameters</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveModalMessage#getActiveMessageParameters()
	 * @see #getActiveModalMessage()
	 * @generated
	 */
	EReference getActiveModalMessage_ActiveMessageParameters();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveParallel <em>Active Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Parallel</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveParallel
	 * @generated
	 */
	EClass getActiveParallel();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveVariableFragment <em>Active Variable Fragment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Variable Fragment</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveVariableFragment
	 * @generated
	 */
	EClass getActiveVariableFragment();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveViolationCondition <em>Active Violation Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Violation Condition</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveViolationCondition
	 * @generated
	 */
	EClass getActiveViolationCondition();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveWaitCondition <em>Active Wait Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Wait Condition</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveWaitCondition
	 * @generated
	 */
	EClass getActiveWaitCondition();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveScenarioRoleBindings <em>Active Scenario Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Scenario Role Bindings</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveScenarioRoleBindings
	 * @generated
	 */
	EClass getActiveScenarioRoleBindings();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.ActiveScenarioRoleBindings#getRoleBindings <em>Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Role Bindings</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveScenarioRoleBindings#getRoleBindings()
	 * @see #getActiveScenarioRoleBindings()
	 * @generated
	 */
	EReference getActiveScenarioRoleBindings_RoleBindings();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>EClass To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EClass To EObject Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.emf.ecore.EClass"
	 *        valueType="org.eclipse.emf.ecore.EObject"
	 * @generated
	 */
	EClass getEClassToEObjectMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEClassToEObjectMapEntry()
	 * @generated
	 */
	EReference getEClassToEObjectMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEClassToEObjectMapEntry()
	 * @generated
	 */
	EReference getEClassToEObjectMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Role To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role To EObject Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.scenariotools.sml.Role"
	 *        valueType="org.eclipse.emf.ecore.EObject"
	 * @generated
	 */
	EClass getRoleToEObjectMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getRoleToEObjectMapEntry()
	 * @generated
	 */
	EReference getRoleToEObjectMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getRoleToEObjectMapEntry()
	 * @generated
	 */
	EReference getRoleToEObjectMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Active Interaction Key Wrapper To Active Interaction Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Interaction Key Wrapper To Active Interaction Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.scenariotools.sml.runtime.ActiveInteractionKeyWrapper"
	 *        valueType="org.scenariotools.sml.runtime.ActivePart"
	 * @generated
	 */
	EClass getActiveInteractionKeyWrapperToActiveInteractionMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveInteractionKeyWrapperToActiveInteractionMapEntry()
	 * @generated
	 */
	EAttribute getActiveInteractionKeyWrapperToActiveInteractionMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveInteractionKeyWrapperToActiveInteractionMapEntry()
	 * @generated
	 */
	EReference getActiveInteractionKeyWrapperToActiveInteractionMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Active Scenario Key Wrapper To Active Scenario Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Scenario Key Wrapper To Active Scenario Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.scenariotools.sml.runtime.ActiveScenarioKeyWrapper"
	 *        valueType="org.scenariotools.sml.runtime.ActiveScenario"
	 * @generated
	 */
	EClass getActiveScenarioKeyWrapperToActiveScenarioMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveScenarioKeyWrapperToActiveScenarioMapEntry()
	 * @generated
	 */
	EAttribute getActiveScenarioKeyWrapperToActiveScenarioMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveScenarioKeyWrapperToActiveScenarioMapEntry()
	 * @generated
	 */
	EReference getActiveScenarioKeyWrapperToActiveScenarioMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Object System Key Wrapper To Object System Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object System Key Wrapper To Object System Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.scenariotools.sml.runtime.ObjectSystemKeyWrapper"
	 *        valueType="org.scenariotools.sml.runtime.SMLObjectSystem"
	 * @generated
	 */
	EClass getObjectSystemKeyWrapperToObjectSystemMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getObjectSystemKeyWrapperToObjectSystemMapEntry()
	 * @generated
	 */
	EAttribute getObjectSystemKeyWrapperToObjectSystemMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getObjectSystemKeyWrapperToObjectSystemMapEntry()
	 * @generated
	 */
	EReference getObjectSystemKeyWrapperToObjectSystemMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Object System Key Wrapper To Dynamic Object Container Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object System Key Wrapper To Dynamic Object Container Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.scenariotools.sml.runtime.ObjectSystemKeyWrapper"
	 *        valueType="org.scenariotools.sml.runtime.DynamicObjectContainer"
	 * @generated
	 */
	EClass getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry()
	 * @generated
	 */
	EAttribute getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry()
	 * @generated
	 */
	EReference getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>State Key Wrapper To State Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State Key Wrapper To State Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.scenariotools.sml.runtime.StateKeyWrapper"
	 *        valueType="org.scenariotools.sml.runtime.SMLRuntimeState"
	 * @generated
	 */
	EClass getStateKeyWrapperToStateMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStateKeyWrapperToStateMapEntry()
	 * @generated
	 */
	EAttribute getStateKeyWrapperToStateMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStateKeyWrapperToStateMapEntry()
	 * @generated
	 */
	EReference getStateKeyWrapperToStateMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Variable To Object Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable To Object Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.scenariotools.sml.expressions.scenarioExpressions.Variable"
	 *        valueDataType="org.eclipse.emf.ecore.EJavaObject"
	 * @generated
	 */
	EClass getVariableToObjectMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getVariableToObjectMapEntry()
	 * @generated
	 */
	EReference getVariableToObjectMapEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getVariableToObjectMapEntry()
	 * @generated
	 */
	EAttribute getVariableToObjectMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Variable To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable To EObject Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.scenariotools.sml.expressions.scenarioExpressions.Variable"
	 *        valueType="org.eclipse.emf.ecore.EObject"
	 * @generated
	 */
	EClass getVariableToEObjectMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getVariableToEObjectMapEntry()
	 * @generated
	 */
	EReference getVariableToEObjectMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getVariableToEObjectMapEntry()
	 * @generated
	 */
	EReference getVariableToEObjectMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.scenariotools.sml.runtime.ActiveScenarioRoleBindingsKeyWrapper"
	 *        valueType="org.scenariotools.sml.runtime.ActiveScenarioRoleBindings"
	 * @generated
	 */
	EClass getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry()
	 * @generated
	 */
	EAttribute getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry()
	 * @generated
	 */
	EReference getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.DynamicObjectContainer <em>Dynamic Object Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dynamic Object Container</em>'.
	 * @see org.scenariotools.sml.runtime.DynamicObjectContainer
	 * @generated
	 */
	EClass getDynamicObjectContainer();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.DynamicObjectContainer#getStaticEObjectToDynamicEObjectMap <em>Static EObject To Dynamic EObject Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Static EObject To Dynamic EObject Map</em>'.
	 * @see org.scenariotools.sml.runtime.DynamicObjectContainer#getStaticEObjectToDynamicEObjectMap()
	 * @see #getDynamicObjectContainer()
	 * @generated
	 */
	EReference getDynamicObjectContainer_StaticEObjectToDynamicEObjectMap();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.DynamicObjectContainer#getRootObjects <em>Root Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Root Objects</em>'.
	 * @see org.scenariotools.sml.runtime.DynamicObjectContainer#getRootObjects()
	 * @see #getDynamicObjectContainer()
	 * @generated
	 */
	EReference getDynamicObjectContainer_RootObjects();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Static EObject To Dynamic EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Static EObject To Dynamic EObject Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.emf.ecore.EObject"
	 *        valueType="org.eclipse.emf.ecore.EObject"
	 * @generated
	 */
	EClass getStaticEObjectToDynamicEObjectMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStaticEObjectToDynamicEObjectMapEntry()
	 * @generated
	 */
	EReference getStaticEObjectToDynamicEObjectMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStaticEObjectToDynamicEObjectMapEntry()
	 * @generated
	 */
	EReference getStaticEObjectToDynamicEObjectMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.MessageEventExtensionInterface <em>Message Event Extension Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Event Extension Interface</em>'.
	 * @see org.scenariotools.sml.runtime.MessageEventExtensionInterface
	 * @generated
	 */
	EClass getMessageEventExtensionInterface();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.MessageEventExtensionInterface#init(org.scenariotools.sml.runtime.configuration.Configuration) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.sml.runtime.MessageEventExtensionInterface#init(org.scenariotools.sml.runtime.configuration.Configuration)
	 * @generated
	 */
	EOperation getMessageEventExtensionInterface__Init__Configuration();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor <em>Message Event Side Effects Executor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Event Side Effects Executor</em>'.
	 * @see org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor
	 * @generated
	 */
	EClass getMessageEventSideEffectsExecutor();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor#executeSideEffects(org.scenariotools.sml.runtime.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer) <em>Execute Side Effects</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Execute Side Effects</em>' operation.
	 * @see org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor#executeSideEffects(org.scenariotools.sml.runtime.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer)
	 * @generated
	 */
	EOperation getMessageEventSideEffectsExecutor__ExecuteSideEffects__MessageEvent_DynamicObjectContainer();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor#canExecuteSideEffects(org.scenariotools.sml.runtime.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer) <em>Can Execute Side Effects</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Can Execute Side Effects</em>' operation.
	 * @see org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor#canExecuteSideEffects(org.scenariotools.sml.runtime.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer)
	 * @generated
	 */
	EOperation getMessageEventSideEffectsExecutor__CanExecuteSideEffects__MessageEvent_DynamicObjectContainer();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator <em>Message Event Is Independent Evaluator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Event Is Independent Evaluator</em>'.
	 * @see org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator
	 * @generated
	 */
	EClass getMessageEventIsIndependentEvaluator();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator#isIndependent(org.scenariotools.sml.runtime.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer) <em>Is Independent</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Independent</em>' operation.
	 * @see org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator#isIndependent(org.scenariotools.sml.runtime.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer)
	 * @generated
	 */
	EOperation getMessageEventIsIndependentEvaluator__IsIndependent__MessageEvent_DynamicObjectContainer();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveMessageParameter <em>Active Message Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Message Parameter</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameter
	 * @generated
	 */
	EClass getActiveMessageParameter();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveMessageParameter#init(org.scenariotools.sml.runtime.ParameterValue) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameter#init(org.scenariotools.sml.runtime.ParameterValue)
	 * @generated
	 */
	EOperation getActiveMessageParameter__Init__ParameterValue();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveMessageParameter#update(org.scenariotools.sml.runtime.ParameterValue, org.scenariotools.sml.runtime.ActivePart, org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState) <em>Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameter#update(org.scenariotools.sml.runtime.ParameterValue, org.scenariotools.sml.runtime.ActivePart, org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getActiveMessageParameter__Update__ParameterValue_ActivePart_ActiveScenario_SMLRuntimeState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveMessageParameter#hasSideEffectsOnUnification() <em>Has Side Effects On Unification</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Has Side Effects On Unification</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameter#hasSideEffectsOnUnification()
	 * @generated
	 */
	EOperation getActiveMessageParameter__HasSideEffectsOnUnification();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveMessageParameter#executeSideEffectsOnUnification(org.scenariotools.sml.runtime.ParameterValue, org.scenariotools.sml.runtime.ParameterValue, org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState) <em>Execute Side Effects On Unification</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Execute Side Effects On Unification</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameter#executeSideEffectsOnUnification(org.scenariotools.sml.runtime.ParameterValue, org.scenariotools.sml.runtime.ParameterValue, org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getActiveMessageParameter__ExecuteSideEffectsOnUnification__ParameterValue_ParameterValue_ActiveScenario_SMLRuntimeState();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithValueExpression <em>Active Message Parameter With Value Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Message Parameter With Value Expression</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameterWithValueExpression
	 * @generated
	 */
	EClass getActiveMessageParameterWithValueExpression();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithValueExpression#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parameter</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameterWithValueExpression#getParameter()
	 * @see #getActiveMessageParameterWithValueExpression()
	 * @generated
	 */
	EReference getActiveMessageParameterWithValueExpression_Parameter();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithBindToVar <em>Active Message Parameter With Bind To Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Message Parameter With Bind To Var</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameterWithBindToVar
	 * @generated
	 */
	EClass getActiveMessageParameterWithBindToVar();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithBindToVar#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parameter</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameterWithBindToVar#getParameter()
	 * @see #getActiveMessageParameterWithBindToVar()
	 * @generated
	 */
	EReference getActiveMessageParameterWithBindToVar_Parameter();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithWildcard <em>Active Message Parameter With Wildcard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Message Parameter With Wildcard</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameterWithWildcard
	 * @generated
	 */
	EClass getActiveMessageParameterWithWildcard();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithWildcard#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parameter</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameterWithWildcard#getParameter()
	 * @see #getActiveMessageParameterWithWildcard()
	 * @generated
	 */
	EReference getActiveMessageParameterWithWildcard_Parameter();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveConstraint <em>Active Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Constraint</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveConstraint
	 * @generated
	 */
	EClass getActiveConstraint();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ActiveConstraint#getConstraintMessageEvent <em>Constraint Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constraint Message Event</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveConstraint#getConstraintMessageEvent()
	 * @see #getActiveConstraint()
	 * @generated
	 */
	EReference getActiveConstraint_ConstraintMessageEvent();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.ActiveConstraint#getActiveMessageParameters <em>Active Message Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Active Message Parameters</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveConstraint#getActiveMessageParameters()
	 * @see #getActiveConstraint()
	 * @generated
	 */
	EReference getActiveConstraint_ActiveMessageParameters();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ActiveConstraint#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Message</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveConstraint#getMessage()
	 * @see #getActiveConstraint()
	 * @generated
	 */
	EReference getActiveConstraint_Message();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveConstraint#init(org.scenariotools.sml.runtime.ActiveScenarioRoleBindings, org.scenariotools.sml.runtime.ActivePart, org.scenariotools.sml.runtime.ActiveScenario) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveConstraint#init(org.scenariotools.sml.runtime.ActiveScenarioRoleBindings, org.scenariotools.sml.runtime.ActivePart, org.scenariotools.sml.runtime.ActiveScenario)
	 * @generated
	 */
	EOperation getActiveConstraint__Init__ActiveScenarioRoleBindings_ActivePart_ActiveScenario();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveConstraint#updateConstraintEvent(org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState) <em>Update Constraint Event</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Constraint Event</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveConstraint#updateConstraintEvent(org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getActiveConstraint__UpdateConstraintEvent__ActiveScenario_SMLRuntimeState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveConstraint#addToParentSpecificConstraintList(org.scenariotools.sml.runtime.ActiveInteraction, org.scenariotools.sml.runtime.MessageEvent) <em>Add To Parent Specific Constraint List</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add To Parent Specific Constraint List</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveConstraint#addToParentSpecificConstraintList(org.scenariotools.sml.runtime.ActiveInteraction, org.scenariotools.sml.runtime.MessageEvent)
	 * @generated
	 */
	EOperation getActiveConstraint__AddToParentSpecificConstraintList__ActiveInteraction_MessageEvent();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveConstraintConsider <em>Active Constraint Consider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Constraint Consider</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveConstraintConsider
	 * @generated
	 */
	EClass getActiveConstraintConsider();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveConstraintIgnore <em>Active Constraint Ignore</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Constraint Ignore</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveConstraintIgnore
	 * @generated
	 */
	EClass getActiveConstraintIgnore();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveConstraintInterrupt <em>Active Constraint Interrupt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Constraint Interrupt</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveConstraintInterrupt
	 * @generated
	 */
	EClass getActiveConstraintInterrupt();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveConstraintForbidden <em>Active Constraint Forbidden</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Constraint Forbidden</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveConstraintForbidden
	 * @generated
	 */
	EClass getActiveConstraintForbidden();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ParameterRangesProvider <em>Parameter Ranges Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Ranges Provider</em>'.
	 * @see org.scenariotools.sml.runtime.ParameterRangesProvider
	 * @generated
	 */
	EClass getParameterRangesProvider();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ParameterRangesProvider#init(org.scenariotools.sml.runtime.configuration.Configuration) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.sml.runtime.ParameterRangesProvider#init(org.scenariotools.sml.runtime.configuration.Configuration)
	 * @generated
	 */
	EOperation getParameterRangesProvider__Init__Configuration();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ParameterRangesProvider#getParameterValues(org.eclipse.emf.ecore.ETypedElement, org.scenariotools.sml.runtime.SMLObjectSystem) <em>Get Parameter Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Parameter Values</em>' operation.
	 * @see org.scenariotools.sml.runtime.ParameterRangesProvider#getParameterValues(org.eclipse.emf.ecore.ETypedElement, org.scenariotools.sml.runtime.SMLObjectSystem)
	 * @generated
	 */
	EOperation getParameterRangesProvider__GetParameterValues__ETypedElement_SMLObjectSystem();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ParameterRangesProvider#getSingelParameterValue(java.lang.Object) <em>Get Singel Parameter Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Singel Parameter Value</em>' operation.
	 * @see org.scenariotools.sml.runtime.ParameterRangesProvider#getSingelParameterValue(java.lang.Object)
	 * @generated
	 */
	EOperation getParameterRangesProvider__GetSingelParameterValue__Object();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ParameterRangesProvider#init(org.eclipse.emf.common.util.EList) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.sml.runtime.ParameterRangesProvider#init(org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getParameterRangesProvider__Init__EList();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ParameterRangesProvider#containsParameterValues(org.eclipse.emf.ecore.ETypedElement) <em>Contains Parameter Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Contains Parameter Values</em>' operation.
	 * @see org.scenariotools.sml.runtime.ParameterRangesProvider#containsParameterValues(org.eclipse.emf.ecore.ETypedElement)
	 * @generated
	 */
	EOperation getParameterRangesProvider__ContainsParameterValues__ETypedElement();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.MessageEventBlockedInformation <em>Message Event Blocked Information</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Event Blocked Information</em>'.
	 * @see org.scenariotools.sml.runtime.MessageEventBlockedInformation
	 * @generated
	 */
	EClass getMessageEventBlockedInformation();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.runtime.MessageEventBlockedInformation#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.scenariotools.sml.runtime.MessageEventBlockedInformation#getDescription()
	 * @see #getMessageEventBlockedInformation()
	 * @generated
	 */
	EAttribute getMessageEventBlockedInformation_Description();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.runtime.MessageEventBlockedInformation#getMessageEventString <em>Message Event String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Event String</em>'.
	 * @see org.scenariotools.sml.runtime.MessageEventBlockedInformation#getMessageEventString()
	 * @see #getMessageEventBlockedInformation()
	 * @generated
	 */
	EAttribute getMessageEventBlockedInformation_MessageEventString();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.MultiActiveScenarioInitializations <em>Multi Active Scenario Initializations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multi Active Scenario Initializations</em>'.
	 * @see org.scenariotools.sml.runtime.MultiActiveScenarioInitializations
	 * @generated
	 */
	EClass getMultiActiveScenarioInitializations();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.MultiActiveScenarioInitializations#getActiveScenarioToActiveScenarioProgressMap <em>Active Scenario To Active Scenario Progress Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Active Scenario To Active Scenario Progress Map</em>'.
	 * @see org.scenariotools.sml.runtime.MultiActiveScenarioInitializations#getActiveScenarioToActiveScenarioProgressMap()
	 * @see #getMultiActiveScenarioInitializations()
	 * @generated
	 */
	EReference getMultiActiveScenarioInitializations_ActiveScenarioToActiveScenarioProgressMap();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Active Scenario To Active Scenario Progress Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Scenario To Active Scenario Progress Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.scenariotools.sml.runtime.ActiveScenario"
	 *        valueDataType="org.scenariotools.sml.runtime.ActiveScenarioProgress"
	 * @generated
	 */
	EClass getActiveScenarioToActiveScenarioProgressMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveScenarioToActiveScenarioProgressMapEntry()
	 * @generated
	 */
	EReference getActiveScenarioToActiveScenarioProgressMapEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveScenarioToActiveScenarioProgressMapEntry()
	 * @generated
	 */
	EAttribute getActiveScenarioToActiveScenarioProgressMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Event To Transition Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event To Transition Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.scenariotools.sml.runtime.Event"
	 *        valueType="org.scenariotools.sml.runtime.Transition"
	 * @generated
	 */
	EClass getEventToTransitionMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEventToTransitionMapEntry()
	 * @generated
	 */
	EReference getEventToTransitionMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEventToTransitionMapEntry()
	 * @generated
	 */
	EReference getEventToTransitionMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see org.scenariotools.sml.runtime.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the container reference '{@link org.scenariotools.sml.runtime.Transition#getSourceState <em>Source State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Source State</em>'.
	 * @see org.scenariotools.sml.runtime.Transition#getSourceState()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_SourceState();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.Transition#getTargetState <em>Target State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target State</em>'.
	 * @see org.scenariotools.sml.runtime.Transition#getTargetState()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_TargetState();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.Transition#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Event</em>'.
	 * @see org.scenariotools.sml.runtime.Transition#getEvent()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Event();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.runtime.Transition#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see org.scenariotools.sml.runtime.Transition#getLabel()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_Label();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.AnnotatableElement <em>Annotatable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotatable Element</em>'.
	 * @see org.scenariotools.sml.runtime.AnnotatableElement
	 * @generated
	 */
	EClass getAnnotatableElement();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.AnnotatableElement#getStringToBooleanAnnotationMap <em>String To Boolean Annotation Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>String To Boolean Annotation Map</em>'.
	 * @see org.scenariotools.sml.runtime.AnnotatableElement#getStringToBooleanAnnotationMap()
	 * @see #getAnnotatableElement()
	 * @generated
	 */
	EReference getAnnotatableElement_StringToBooleanAnnotationMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.AnnotatableElement#getStringToStringAnnotationMap <em>String To String Annotation Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>String To String Annotation Map</em>'.
	 * @see org.scenariotools.sml.runtime.AnnotatableElement#getStringToStringAnnotationMap()
	 * @see #getAnnotatableElement()
	 * @generated
	 */
	EReference getAnnotatableElement_StringToStringAnnotationMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.AnnotatableElement#getStringToEObjectAnnotationMap <em>String To EObject Annotation Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>String To EObject Annotation Map</em>'.
	 * @see org.scenariotools.sml.runtime.AnnotatableElement#getStringToEObjectAnnotationMap()
	 * @see #getAnnotatableElement()
	 * @generated
	 */
	EReference getAnnotatableElement_StringToEObjectAnnotationMap();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>String To Boolean Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String To Boolean Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.eclipse.emf.ecore.EString"
	 *        valueDataType="org.eclipse.emf.ecore.EBooleanObject"
	 * @generated
	 */
	EClass getStringToBooleanMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToBooleanMapEntry()
	 * @generated
	 */
	EAttribute getStringToBooleanMapEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToBooleanMapEntry()
	 * @generated
	 */
	EAttribute getStringToBooleanMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>String To String Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String To String Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.eclipse.emf.ecore.EString"
	 *        valueDataType="org.eclipse.emf.ecore.EString"
	 * @generated
	 */
	EClass getStringToStringMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToStringMapEntry()
	 * @generated
	 */
	EAttribute getStringToStringMapEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToStringMapEntry()
	 * @generated
	 */
	EAttribute getStringToStringMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>String To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String To EObject Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.eclipse.emf.ecore.EString"
	 *        valueType="org.eclipse.emf.ecore.EObject"
	 * @generated
	 */
	EClass getStringToEObjectMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToEObjectMapEntry()
	 * @generated
	 */
	EAttribute getStringToEObjectMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToEObjectMapEntry()
	 * @generated
	 */
	EReference getStringToEObjectMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see org.scenariotools.sml.runtime.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.MessageEvent <em>Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Event</em>'.
	 * @see org.scenariotools.sml.runtime.MessageEvent
	 * @generated
	 */
	EClass getMessageEvent();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.MessageEvent#getSendingObject <em>Sending Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sending Object</em>'.
	 * @see org.scenariotools.sml.runtime.MessageEvent#getSendingObject()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EReference getMessageEvent_SendingObject();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.MessageEvent#getReceivingObject <em>Receiving Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Receiving Object</em>'.
	 * @see org.scenariotools.sml.runtime.MessageEvent#getReceivingObject()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EReference getMessageEvent_ReceivingObject();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.runtime.MessageEvent#getMessageName <em>Message Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Name</em>'.
	 * @see org.scenariotools.sml.runtime.MessageEvent#getMessageName()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EAttribute getMessageEvent_MessageName();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.runtime.MessageEvent#isConcrete <em>Concrete</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Concrete</em>'.
	 * @see org.scenariotools.sml.runtime.MessageEvent#isConcrete()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EAttribute getMessageEvent_Concrete();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.runtime.MessageEvent#isParameterized <em>Parameterized</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Parameterized</em>'.
	 * @see org.scenariotools.sml.runtime.MessageEvent#isParameterized()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EAttribute getMessageEvent_Parameterized();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.MessageEvent#getTypedElement <em>Typed Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Typed Element</em>'.
	 * @see org.scenariotools.sml.runtime.MessageEvent#getTypedElement()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EReference getMessageEvent_TypedElement();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.MessageEvent#getParameterValues <em>Parameter Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameter Values</em>'.
	 * @see org.scenariotools.sml.runtime.MessageEvent#getParameterValues()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EReference getMessageEvent_ParameterValues();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.runtime.MessageEvent#getCollectionOperation <em>Collection Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Collection Operation</em>'.
	 * @see org.scenariotools.sml.runtime.MessageEvent#getCollectionOperation()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EAttribute getMessageEvent_CollectionOperation();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.MessageEvent#eq(org.scenariotools.sml.runtime.MessageEvent) <em>Eq</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Eq</em>' operation.
	 * @see org.scenariotools.sml.runtime.MessageEvent#eq(org.scenariotools.sml.runtime.MessageEvent)
	 * @generated
	 */
	EOperation getMessageEvent__Eq__MessageEvent();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.MessageEvent#isMessageUnifiableWith(org.scenariotools.sml.runtime.MessageEvent) <em>Is Message Unifiable With</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Message Unifiable With</em>' operation.
	 * @see org.scenariotools.sml.runtime.MessageEvent#isMessageUnifiableWith(org.scenariotools.sml.runtime.MessageEvent)
	 * @generated
	 */
	EOperation getMessageEvent__IsMessageUnifiableWith__MessageEvent();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.MessageEvent#isParameterUnifiableWith(org.scenariotools.sml.runtime.MessageEvent) <em>Is Parameter Unifiable With</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Parameter Unifiable With</em>' operation.
	 * @see org.scenariotools.sml.runtime.MessageEvent#isParameterUnifiableWith(org.scenariotools.sml.runtime.MessageEvent)
	 * @generated
	 */
	EOperation getMessageEvent__IsParameterUnifiableWith__MessageEvent();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ParameterValue <em>Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Value</em>'.
	 * @see org.scenariotools.sml.runtime.ParameterValue
	 * @generated
	 */
	EClass getParameterValue();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ParameterValue#getEParameter <em>EParameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EParameter</em>'.
	 * @see org.scenariotools.sml.runtime.ParameterValue#getEParameter()
	 * @see #getParameterValue()
	 * @generated
	 */
	EReference getParameterValue_EParameter();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.runtime.ParameterValue#isUnset <em>Unset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unset</em>'.
	 * @see org.scenariotools.sml.runtime.ParameterValue#isUnset()
	 * @see #getParameterValue()
	 * @generated
	 */
	EAttribute getParameterValue_Unset();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.runtime.ParameterValue#isWildcardParameter <em>Wildcard Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wildcard Parameter</em>'.
	 * @see org.scenariotools.sml.runtime.ParameterValue#isWildcardParameter()
	 * @see #getParameterValue()
	 * @generated
	 */
	EAttribute getParameterValue_WildcardParameter();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ParameterValue#getStrucFeatureOrEOp <em>Struc Feature Or EOp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Struc Feature Or EOp</em>'.
	 * @see org.scenariotools.sml.runtime.ParameterValue#getStrucFeatureOrEOp()
	 * @see #getParameterValue()
	 * @generated
	 */
	EReference getParameterValue_StrucFeatureOrEOp();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ParameterValue#setValue(java.lang.Object) <em>Set Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Value</em>' operation.
	 * @see org.scenariotools.sml.runtime.ParameterValue#setValue(java.lang.Object)
	 * @generated
	 */
	EOperation getParameterValue__SetValue__Object();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ParameterValue#getValue() <em>Get Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Value</em>' operation.
	 * @see org.scenariotools.sml.runtime.ParameterValue#getValue()
	 * @generated
	 */
	EOperation getParameterValue__GetValue();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ParameterValue#eq(org.scenariotools.sml.runtime.ParameterValue) <em>Eq</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Eq</em>' operation.
	 * @see org.scenariotools.sml.runtime.ParameterValue#eq(org.scenariotools.sml.runtime.ParameterValue)
	 * @generated
	 */
	EOperation getParameterValue__Eq__ParameterValue();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ParameterValue#isParameterUnifiableWith(org.scenariotools.sml.runtime.ParameterValue) <em>Is Parameter Unifiable With</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Parameter Unifiable With</em>' operation.
	 * @see org.scenariotools.sml.runtime.ParameterValue#isParameterUnifiableWith(org.scenariotools.sml.runtime.ParameterValue)
	 * @generated
	 */
	EOperation getParameterValue__IsParameterUnifiableWith__ParameterValue();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.BooleanParameterValue <em>Boolean Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Parameter Value</em>'.
	 * @see org.scenariotools.sml.runtime.BooleanParameterValue
	 * @generated
	 */
	EClass getBooleanParameterValue();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.runtime.BooleanParameterValue#isBooleanParameterValue <em>Boolean Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Boolean Parameter Value</em>'.
	 * @see org.scenariotools.sml.runtime.BooleanParameterValue#isBooleanParameterValue()
	 * @see #getBooleanParameterValue()
	 * @generated
	 */
	EAttribute getBooleanParameterValue_BooleanParameterValue();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.IntegerParameterValue <em>Integer Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Parameter Value</em>'.
	 * @see org.scenariotools.sml.runtime.IntegerParameterValue
	 * @generated
	 */
	EClass getIntegerParameterValue();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.runtime.IntegerParameterValue#getIntegerParameterValue <em>Integer Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Integer Parameter Value</em>'.
	 * @see org.scenariotools.sml.runtime.IntegerParameterValue#getIntegerParameterValue()
	 * @see #getIntegerParameterValue()
	 * @generated
	 */
	EAttribute getIntegerParameterValue_IntegerParameterValue();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.StringParameterValue <em>String Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Parameter Value</em>'.
	 * @see org.scenariotools.sml.runtime.StringParameterValue
	 * @generated
	 */
	EClass getStringParameterValue();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.runtime.StringParameterValue#getStringParameterValue <em>String Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>String Parameter Value</em>'.
	 * @see org.scenariotools.sml.runtime.StringParameterValue#getStringParameterValue()
	 * @see #getStringParameterValue()
	 * @generated
	 */
	EAttribute getStringParameterValue_StringParameterValue();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.EObjectParameterValue <em>EObject Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EObject Parameter Value</em>'.
	 * @see org.scenariotools.sml.runtime.EObjectParameterValue
	 * @generated
	 */
	EClass getEObjectParameterValue();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.EObjectParameterValue#getEObjectParameterValue <em>EObject Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EObject Parameter Value</em>'.
	 * @see org.scenariotools.sml.runtime.EObjectParameterValue#getEObjectParameterValue()
	 * @see #getEObjectParameterValue()
	 * @generated
	 */
	EReference getEObjectParameterValue_EObjectParameterValue();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.EEnumParameterValue <em>EEnum Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EEnum Parameter Value</em>'.
	 * @see org.scenariotools.sml.runtime.EEnumParameterValue
	 * @generated
	 */
	EClass getEEnumParameterValue();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.EEnumParameterValue#getEEnumParameterType <em>EEnum Parameter Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EEnum Parameter Type</em>'.
	 * @see org.scenariotools.sml.runtime.EEnumParameterValue#getEEnumParameterType()
	 * @see #getEEnumParameterValue()
	 * @generated
	 */
	EReference getEEnumParameterValue_EEnumParameterType();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.EEnumParameterValue#getEEnumParameterValue <em>EEnum Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EEnum Parameter Value</em>'.
	 * @see org.scenariotools.sml.runtime.EEnumParameterValue#getEEnumParameterValue()
	 * @see #getEEnumParameterValue()
	 * @generated
	 */
	EReference getEEnumParameterValue_EEnumParameterValue();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.WaitEvent <em>Wait Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Wait Event</em>'.
	 * @see org.scenariotools.sml.runtime.WaitEvent
	 * @generated
	 */
	EClass getWaitEvent();

	/**
	 * Returns the meta object for enum '{@link org.scenariotools.sml.runtime.ViolationKind <em>Violation Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Violation Kind</em>'.
	 * @see org.scenariotools.sml.runtime.ViolationKind
	 * @generated
	 */
	EEnum getViolationKind();

	/**
	 * Returns the meta object for enum '{@link org.scenariotools.sml.runtime.ActiveScenarioProgress <em>Active Scenario Progress</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Active Scenario Progress</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveScenarioProgress
	 * @generated
	 */
	EEnum getActiveScenarioProgress();

	/**
	 * Returns the meta object for enum '{@link org.scenariotools.sml.runtime.BlockedType <em>Blocked Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Blocked Type</em>'.
	 * @see org.scenariotools.sml.runtime.BlockedType
	 * @generated
	 */
	EEnum getBlockedType();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.sml.runtime.keywrapper.ActiveInteractionKeyWrapper <em>Active Interaction Key Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Active Interaction Key Wrapper</em>'.
	 * @see org.scenariotools.sml.runtime.keywrapper.ActiveInteractionKeyWrapper
	 * @model instanceClass="org.scenariotools.sml.runtime.keywrapper.ActiveInteractionKeyWrapper"
	 * @generated
	 */
	EDataType getActiveInteractionKeyWrapper();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper <em>Active Scenario Key Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Active Scenario Key Wrapper</em>'.
	 * @see org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper
	 * @model instanceClass="org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper"
	 * @generated
	 */
	EDataType getActiveScenarioKeyWrapper();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.sml.runtime.keywrapper.ObjectSystemKeyWrapper <em>Object System Key Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Object System Key Wrapper</em>'.
	 * @see org.scenariotools.sml.runtime.keywrapper.ObjectSystemKeyWrapper
	 * @model instanceClass="org.scenariotools.sml.runtime.keywrapper.ObjectSystemKeyWrapper"
	 * @generated
	 */
	EDataType getObjectSystemKeyWrapper();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.sml.runtime.keywrapper.StateKeyWrapper <em>State Key Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>State Key Wrapper</em>'.
	 * @see org.scenariotools.sml.runtime.keywrapper.StateKeyWrapper
	 * @model instanceClass="org.scenariotools.sml.runtime.keywrapper.StateKeyWrapper"
	 * @generated
	 */
	EDataType getStateKeyWrapper();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.sml.runtime.keywrapper.ActiveScenarioRoleBindingsKeyWrapper <em>Active Scenario Role Bindings Key Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Active Scenario Role Bindings Key Wrapper</em>'.
	 * @see org.scenariotools.sml.runtime.keywrapper.ActiveScenarioRoleBindingsKeyWrapper
	 * @model instanceClass="org.scenariotools.sml.runtime.keywrapper.ActiveScenarioRoleBindingsKeyWrapper"
	 * @generated
	 */
	EDataType getActiveScenarioRoleBindingsKeyWrapper();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.sml.runtime.logic.ParameterRanges.ParameterValues <em>Parameter Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Parameter Values</em>'.
	 * @see org.scenariotools.sml.runtime.logic.ParameterRanges.ParameterValues
	 * @model instanceClass="org.scenariotools.sml.runtime.logic.ParameterRanges.ParameterValues" typeParameters="T" TBounds="org.eclipse.emf.ecore.EJavaObject"
	 * @generated
	 */
	EDataType getParameterValues();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RuntimeFactory getRuntimeFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.SMLRuntimeStateGraphImpl <em>SML Runtime State Graph</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.SMLRuntimeStateGraphImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getSMLRuntimeStateGraph()
		 * @generated
		 */
		EClass SML_RUNTIME_STATE_GRAPH = eINSTANCE.getSMLRuntimeStateGraph();

		/**
		 * The meta object literal for the '<em><b>Configuration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE_GRAPH__CONFIGURATION = eINSTANCE.getSMLRuntimeStateGraph_Configuration();

		/**
		 * The meta object literal for the '<em><b>Element Container</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER = eINSTANCE.getSMLRuntimeStateGraph_ElementContainer();

		/**
		 * The meta object literal for the '<em><b>Parameter Ranges Provider</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE_GRAPH__PARAMETER_RANGES_PROVIDER = eINSTANCE.getSMLRuntimeStateGraph_ParameterRangesProvider();

		/**
		 * The meta object literal for the '<em><b>States</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE_GRAPH__STATES = eINSTANCE.getSMLRuntimeStateGraph_States();

		/**
		 * The meta object literal for the '<em><b>Start State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE_GRAPH__START_STATE = eINSTANCE.getSMLRuntimeStateGraph_StartState();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_RUNTIME_STATE_GRAPH___INIT__CONFIGURATION = eINSTANCE.getSMLRuntimeStateGraph__Init__Configuration();

		/**
		 * The meta object literal for the '<em><b>Generate Successor</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_RUNTIME_STATE_GRAPH___GENERATE_SUCCESSOR__SMLRUNTIMESTATE_EVENT = eINSTANCE.getSMLRuntimeStateGraph__GenerateSuccessor__SMLRuntimeState_Event();

		/**
		 * The meta object literal for the '<em><b>Generate All Successors</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_RUNTIME_STATE_GRAPH___GENERATE_ALL_SUCCESSORS__SMLRUNTIMESTATE = eINSTANCE.getSMLRuntimeStateGraph__GenerateAllSuccessors__SMLRuntimeState();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.SMLRuntimeStateImpl <em>SML Runtime State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.SMLRuntimeStateImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getSMLRuntimeState()
		 * @generated
		 */
		EClass SML_RUNTIME_STATE = eINSTANCE.getSMLRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Active Scenarios</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE__ACTIVE_SCENARIOS = eINSTANCE.getSMLRuntimeState_ActiveScenarios();

		/**
		 * The meta object literal for the '<em><b>Dynamic Object Container</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE__DYNAMIC_OBJECT_CONTAINER = eINSTANCE.getSMLRuntimeState_DynamicObjectContainer();

		/**
		 * The meta object literal for the '<em><b>Computed Initializing Message Events</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE__COMPUTED_INITIALIZING_MESSAGE_EVENTS = eINSTANCE.getSMLRuntimeState_ComputedInitializingMessageEvents();

		/**
		 * The meta object literal for the '<em><b>Terminated Existential Scenarios From Last Perform Step</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE__TERMINATED_EXISTENTIAL_SCENARIOS_FROM_LAST_PERFORM_STEP = eINSTANCE.getSMLRuntimeState_TerminatedExistentialScenariosFromLastPerformStep();

		/**
		 * The meta object literal for the '<em><b>Message Event Blocked Information</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE__MESSAGE_EVENT_BLOCKED_INFORMATION = eINSTANCE.getSMLRuntimeState_MessageEventBlockedInformation();

		/**
		 * The meta object literal for the '<em><b>Object System</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE__OBJECT_SYSTEM = eINSTANCE.getSMLRuntimeState_ObjectSystem();

		/**
		 * The meta object literal for the '<em><b>Safety Violation Occurred In Guarantees</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SML_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_GUARANTEES = eINSTANCE.getSMLRuntimeState_SafetyViolationOccurredInGuarantees();

		/**
		 * The meta object literal for the '<em><b>Safety Violation Occurred In Assumptions</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SML_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS = eINSTANCE.getSMLRuntimeState_SafetyViolationOccurredInAssumptions();

		/**
		 * The meta object literal for the '<em><b>Event To Transition Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE__EVENT_TO_TRANSITION_MAP = eINSTANCE.getSMLRuntimeState_EventToTransitionMap();

		/**
		 * The meta object literal for the '<em><b>Enabled Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE__ENABLED_EVENTS = eINSTANCE.getSMLRuntimeState_EnabledEvents();

		/**
		 * The meta object literal for the '<em><b>Outgoing Transition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE__OUTGOING_TRANSITION = eINSTANCE.getSMLRuntimeState_OutgoingTransition();

		/**
		 * The meta object literal for the '<em><b>Incoming Transition</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE__INCOMING_TRANSITION = eINSTANCE.getSMLRuntimeState_IncomingTransition();

		/**
		 * The meta object literal for the '<em><b>State Graph</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE__STATE_GRAPH = eINSTANCE.getSMLRuntimeState_StateGraph();

		/**
		 * The meta object literal for the '<em><b>System Chose To Wait</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SML_RUNTIME_STATE__SYSTEM_CHOSE_TO_WAIT = eINSTANCE.getSMLRuntimeState_SystemChoseToWait();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_RUNTIME_STATE___INIT__SMLOBJECTSYSTEM = eINSTANCE.getSMLRuntimeState__Init__SMLObjectSystem();

		/**
		 * The meta object literal for the '<em><b>Update Enabled Message Events</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_RUNTIME_STATE___UPDATE_ENABLED_MESSAGE_EVENTS = eINSTANCE.getSMLRuntimeState__UpdateEnabledMessageEvents();

		/**
		 * The meta object literal for the '<em><b>Perform Step</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_RUNTIME_STATE___PERFORM_STEP__EVENT = eINSTANCE.getSMLRuntimeState__PerformStep__Event();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveScenarioImpl <em>Active Scenario</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveScenarioImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenario()
		 * @generated
		 */
		EClass ACTIVE_SCENARIO = eINSTANCE.getActiveScenario();

		/**
		 * The meta object literal for the '<em><b>Scenario</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_SCENARIO__SCENARIO = eINSTANCE.getActiveScenario_Scenario();

		/**
		 * The meta object literal for the '<em><b>Main Active Interaction</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_SCENARIO__MAIN_ACTIVE_INTERACTION = eINSTANCE.getActiveScenario_MainActiveInteraction();

		/**
		 * The meta object literal for the '<em><b>Alphabet</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_SCENARIO__ALPHABET = eINSTANCE.getActiveScenario_Alphabet();

		/**
		 * The meta object literal for the '<em><b>Role Bindings</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_SCENARIO__ROLE_BINDINGS = eINSTANCE.getActiveScenario_RoleBindings();

		/**
		 * The meta object literal for the '<em><b>Safety Violation Occurred</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVE_SCENARIO__SAFETY_VIOLATION_OCCURRED = eINSTANCE.getActiveScenario_SafetyViolationOccurred();

		/**
		 * The meta object literal for the '<em><b>Context Helper Class Instances</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_SCENARIO__CONTEXT_HELPER_CLASS_INSTANCES = eINSTANCE.getActiveScenario_ContextHelperClassInstances();

		/**
		 * The meta object literal for the '<em><b>Perform Step</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_SCENARIO___PERFORM_STEP__MESSAGEEVENT_SMLRUNTIMESTATE = eINSTANCE.getActiveScenario__PerformStep__MessageEvent_SMLRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_SCENARIO___INIT__SMLOBJECTSYSTEM_DYNAMICOBJECTCONTAINER_SMLRUNTIMESTATE_MESSAGEEVENT = eINSTANCE.getActiveScenario__Init__SMLObjectSystem_DynamicObjectContainer_SMLRuntimeState_MessageEvent();

		/**
		 * The meta object literal for the '<em><b>Is Blocked</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_SCENARIO___IS_BLOCKED__MESSAGEEVENT = eINSTANCE.getActiveScenario__IsBlocked__MessageEvent();

		/**
		 * The meta object literal for the '<em><b>Get Requested Events</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_SCENARIO___GET_REQUESTED_EVENTS = eINSTANCE.getActiveScenario__GetRequestedEvents();

		/**
		 * The meta object literal for the '<em><b>Is In Requested State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_SCENARIO___IS_IN_REQUESTED_STATE = eINSTANCE.getActiveScenario__IsInRequestedState();

		/**
		 * The meta object literal for the '<em><b>Is In Strict State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_SCENARIO___IS_IN_STRICT_STATE = eINSTANCE.getActiveScenario__IsInStrictState();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.Context <em>Context</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.Context
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getContext()
		 * @generated
		 */
		EClass CONTEXT = eINSTANCE.getContext();

		/**
		 * The meta object literal for the '<em><b>Get Value</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONTEXT___GET_VALUE__VARIABLE = eINSTANCE.getContext__GetValue__Variable();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ElementContainerImpl <em>Element Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ElementContainerImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getElementContainer()
		 * @generated
		 */
		EClass ELEMENT_CONTAINER = eINSTANCE.getElementContainer();

		/**
		 * The meta object literal for the '<em><b>Active Scenarios</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_SCENARIOS = eINSTANCE.getElementContainer_ActiveScenarios();

		/**
		 * The meta object literal for the '<em><b>Active Interactions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_INTERACTIONS = eINSTANCE.getElementContainer_ActiveInteractions();

		/**
		 * The meta object literal for the '<em><b>Object Systems</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__OBJECT_SYSTEMS = eINSTANCE.getElementContainer_ObjectSystems();

		/**
		 * The meta object literal for the '<em><b>Active Scenario Role Bindings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS = eINSTANCE.getElementContainer_ActiveScenarioRoleBindings();

		/**
		 * The meta object literal for the '<em><b>Dynamic Object Container</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__DYNAMIC_OBJECT_CONTAINER = eINSTANCE.getElementContainer_DynamicObjectContainer();

		/**
		 * The meta object literal for the '<em><b>Active Interaction Key Wrapper To Active Interaction Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP = eINSTANCE.getElementContainer_ActiveInteractionKeyWrapperToActiveInteractionMap();

		/**
		 * The meta object literal for the '<em><b>Active Scenario Key Wrapper To Active Scenario Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP = eINSTANCE.getElementContainer_ActiveScenarioKeyWrapperToActiveScenarioMap();

		/**
		 * The meta object literal for the '<em><b>Object System Key Wrapper To Object System Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP = eINSTANCE.getElementContainer_ObjectSystemKeyWrapperToObjectSystemMap();

		/**
		 * The meta object literal for the '<em><b>Object System Key Wrapper To Dynamic Object Container Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP = eINSTANCE.getElementContainer_ObjectSystemKeyWrapperToDynamicObjectContainerMap();

		/**
		 * The meta object literal for the '<em><b>State Key Wrapper To State Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__STATE_KEY_WRAPPER_TO_STATE_MAP = eINSTANCE.getElementContainer_StateKeyWrapperToStateMap();

		/**
		 * The meta object literal for the '<em><b>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP = eINSTANCE.getElementContainer_ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap();

		/**
		 * The meta object literal for the '<em><b>Enabled</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT_CONTAINER__ENABLED = eINSTANCE.getElementContainer_Enabled();

		/**
		 * The meta object literal for the '<em><b>Wait Event</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__WAIT_EVENT = eINSTANCE.getElementContainer_WaitEvent();

		/**
		 * The meta object literal for the '<em><b>Get Active Scenario</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ELEMENT_CONTAINER___GET_ACTIVE_SCENARIO__ACTIVESCENARIO = eINSTANCE.getElementContainer__GetActiveScenario__ActiveScenario();

		/**
		 * The meta object literal for the '<em><b>Get Object System</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ELEMENT_CONTAINER___GET_OBJECT_SYSTEM__SMLOBJECTSYSTEM = eINSTANCE.getElementContainer__GetObjectSystem__SMLObjectSystem();

		/**
		 * The meta object literal for the '<em><b>Get SML Runtime State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ELEMENT_CONTAINER___GET_SML_RUNTIME_STATE__SMLRUNTIMESTATE = eINSTANCE.getElementContainer__GetSMLRuntimeState__SMLRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Get Active Scenario Role Bindings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ELEMENT_CONTAINER___GET_ACTIVE_SCENARIO_ROLE_BINDINGS__ACTIVESCENARIOROLEBINDINGS = eINSTANCE.getElementContainer__GetActiveScenarioRoleBindings__ActiveScenarioRoleBindings();

		/**
		 * The meta object literal for the '<em><b>Get Dynamic Object Container</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ELEMENT_CONTAINER___GET_DYNAMIC_OBJECT_CONTAINER__DYNAMICOBJECTCONTAINER_SMLOBJECTSYSTEM = eINSTANCE.getElementContainer__GetDynamicObjectContainer__DynamicObjectContainer_SMLObjectSystem();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.SMLObjectSystemImpl <em>SML Object System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.SMLObjectSystemImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getSMLObjectSystem()
		 * @generated
		 */
		EClass SML_OBJECT_SYSTEM = eINSTANCE.getSMLObjectSystem();

		/**
		 * The meta object literal for the '<em><b>EClass To EObject</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_OBJECT_SYSTEM__ECLASS_TO_EOBJECT = eINSTANCE.getSMLObjectSystem_EClassToEObject();

		/**
		 * The meta object literal for the '<em><b>Static Role Bindings</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_OBJECT_SYSTEM__STATIC_ROLE_BINDINGS = eINSTANCE.getSMLObjectSystem_StaticRoleBindings();

		/**
		 * The meta object literal for the '<em><b>Specification</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_OBJECT_SYSTEM__SPECIFICATION = eINSTANCE.getSMLObjectSystem_Specification();

		/**
		 * The meta object literal for the '<em><b>Runconfiguration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_OBJECT_SYSTEM__RUNCONFIGURATION = eINSTANCE.getSMLObjectSystem_Runconfiguration();

		/**
		 * The meta object literal for the '<em><b>Message Event Side Effects Executor</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_OBJECT_SYSTEM__MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR = eINSTANCE.getSMLObjectSystem_MessageEventSideEffectsExecutor();

		/**
		 * The meta object literal for the '<em><b>Message Event Is Independent Evaluators</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_OBJECT_SYSTEM__MESSAGE_EVENT_IS_INDEPENDENT_EVALUATORS = eINSTANCE.getSMLObjectSystem_MessageEventIsIndependentEvaluators();

		/**
		 * The meta object literal for the '<em><b>Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_OBJECT_SYSTEM__OBJECTS = eINSTANCE.getSMLObjectSystem_Objects();

		/**
		 * The meta object literal for the '<em><b>Controllable Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_OBJECT_SYSTEM__CONTROLLABLE_OBJECTS = eINSTANCE.getSMLObjectSystem_ControllableObjects();

		/**
		 * The meta object literal for the '<em><b>Uncontrollable Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS = eINSTANCE.getSMLObjectSystem_UncontrollableObjects();

		/**
		 * The meta object literal for the '<em><b>Root Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_OBJECT_SYSTEM__ROOT_OBJECTS = eINSTANCE.getSMLObjectSystem_RootObjects();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_OBJECT_SYSTEM___INIT__CONFIGURATION_SMLRUNTIMESTATE = eINSTANCE.getSMLObjectSystem__Init__Configuration_SMLRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Execute Side Effects</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_OBJECT_SYSTEM___EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = eINSTANCE.getSMLObjectSystem__ExecuteSideEffects__MessageEvent_DynamicObjectContainer();

		/**
		 * The meta object literal for the '<em><b>Can Execute Side Effects</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_OBJECT_SYSTEM___CAN_EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = eINSTANCE.getSMLObjectSystem__CanExecuteSideEffects__MessageEvent_DynamicObjectContainer();

		/**
		 * The meta object literal for the '<em><b>Is Independent</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_OBJECT_SYSTEM___IS_INDEPENDENT__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = eINSTANCE.getSMLObjectSystem__IsIndependent__MessageEvent_DynamicObjectContainer();

		/**
		 * The meta object literal for the '<em><b>Is Non Spontaneous Message Event</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_OBJECT_SYSTEM___IS_NON_SPONTANEOUS_MESSAGE_EVENT__MESSAGEEVENT = eINSTANCE.getSMLObjectSystem__IsNonSpontaneousMessageEvent__MessageEvent();

		/**
		 * The meta object literal for the '<em><b>Get Scenarios For Init Message Event</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_OBJECT_SYSTEM___GET_SCENARIOS_FOR_INIT_MESSAGE_EVENT__MESSAGEEVENT = eINSTANCE.getSMLObjectSystem__GetScenariosForInitMessageEvent__MessageEvent();

		/**
		 * The meta object literal for the '<em><b>Get Initializing Environment Message Events</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_OBJECT_SYSTEM___GET_INITIALIZING_ENVIRONMENT_MESSAGE_EVENTS__DYNAMICOBJECTCONTAINER = eINSTANCE.getSMLObjectSystem__GetInitializingEnvironmentMessageEvents__DynamicObjectContainer();

		/**
		 * The meta object literal for the '<em><b>Is Controllable</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_OBJECT_SYSTEM___IS_CONTROLLABLE__EOBJECT = eINSTANCE.getSMLObjectSystem__IsControllable__EObject();

		/**
		 * The meta object literal for the '<em><b>Is Environment Message Event</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_OBJECT_SYSTEM___IS_ENVIRONMENT_MESSAGE_EVENT__EVENT = eINSTANCE.getSMLObjectSystem__IsEnvironmentMessageEvent__Event();

		/**
		 * The meta object literal for the '<em><b>Contains EObject</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_OBJECT_SYSTEM___CONTAINS_EOBJECT__EOBJECT = eINSTANCE.getSMLObjectSystem__ContainsEObject__EObject();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActivePartImpl <em>Active Part</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActivePartImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActivePart()
		 * @generated
		 */
		EClass ACTIVE_PART = eINSTANCE.getActivePart();

		/**
		 * The meta object literal for the '<em><b>Nested Active Interactions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS = eINSTANCE.getActivePart_NestedActiveInteractions();

		/**
		 * The meta object literal for the '<em><b>Covered Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__COVERED_EVENTS = eINSTANCE.getActivePart_CoveredEvents();

		/**
		 * The meta object literal for the '<em><b>Forbidden Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__FORBIDDEN_EVENTS = eINSTANCE.getActivePart_ForbiddenEvents();

		/**
		 * The meta object literal for the '<em><b>Interrupting Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__INTERRUPTING_EVENTS = eINSTANCE.getActivePart_InterruptingEvents();

		/**
		 * The meta object literal for the '<em><b>Considered Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__CONSIDERED_EVENTS = eINSTANCE.getActivePart_ConsideredEvents();

		/**
		 * The meta object literal for the '<em><b>Ignored Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__IGNORED_EVENTS = eINSTANCE.getActivePart_IgnoredEvents();

		/**
		 * The meta object literal for the '<em><b>Enabled Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__ENABLED_EVENTS = eINSTANCE.getActivePart_EnabledEvents();

		/**
		 * The meta object literal for the '<em><b>Parent Active Interaction</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__PARENT_ACTIVE_INTERACTION = eINSTANCE.getActivePart_ParentActiveInteraction();

		/**
		 * The meta object literal for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS = eINSTANCE.getActivePart_EnabledNestedActiveInteractions();

		/**
		 * The meta object literal for the '<em><b>Variable Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__VARIABLE_MAP = eINSTANCE.getActivePart_VariableMap();

		/**
		 * The meta object literal for the '<em><b>EObject Variable Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__EOBJECT_VARIABLE_MAP = eINSTANCE.getActivePart_EObjectVariableMap();

		/**
		 * The meta object literal for the '<em><b>Interaction Fragment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__INTERACTION_FRAGMENT = eINSTANCE.getActivePart_InteractionFragment();

		/**
		 * The meta object literal for the '<em><b>Perform Step</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = eINSTANCE.getActivePart__PerformStep__MessageEvent_ActiveScenario_SMLRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Post Perform Step</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = eINSTANCE.getActivePart__PostPerformStep__MessageEvent_ActiveScenario_SMLRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = eINSTANCE.getActivePart__Init__ActiveScenarioRoleBindings_ActivePart_ActiveScenario();

		/**
		 * The meta object literal for the '<em><b>Is Violating In Interaction</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = eINSTANCE.getActivePart__IsViolatingInInteraction__MessageEvent_boolean();

		/**
		 * The meta object literal for the '<em><b>Update Message Events</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = eINSTANCE.getActivePart__UpdateMessageEvents__ActiveScenario_SMLRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Get Requested Events</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_PART___GET_REQUESTED_EVENTS = eINSTANCE.getActivePart__GetRequestedEvents();

		/**
		 * The meta object literal for the '<em><b>Is Blocked</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = eINSTANCE.getActivePart__IsBlocked__MessageEvent_boolean();

		/**
		 * The meta object literal for the '<em><b>Enable</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = eINSTANCE.getActivePart__Enable__ActiveScenario_SMLRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Is In Requested State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_PART___IS_IN_REQUESTED_STATE = eINSTANCE.getActivePart__IsInRequestedState();

		/**
		 * The meta object literal for the '<em><b>Is In Strict State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_PART___IS_IN_STRICT_STATE = eINSTANCE.getActivePart__IsInStrictState();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveAlternativeImpl <em>Active Alternative</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveAlternativeImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveAlternative()
		 * @generated
		 */
		EClass ACTIVE_ALTERNATIVE = eINSTANCE.getActiveAlternative();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveCaseImpl <em>Active Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveCaseImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveCase()
		 * @generated
		 */
		EClass ACTIVE_CASE = eINSTANCE.getActiveCase();

		/**
		 * The meta object literal for the '<em><b>Case</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_CASE__CASE = eINSTANCE.getActiveCase_Case();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveInteractionImpl <em>Active Interaction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveInteractionImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveInteraction()
		 * @generated
		 */
		EClass ACTIVE_INTERACTION = eINSTANCE.getActiveInteraction();

		/**
		 * The meta object literal for the '<em><b>Active Constraints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_INTERACTION__ACTIVE_CONSTRAINTS = eINSTANCE.getActiveInteraction_ActiveConstraints();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveInterruptConditionImpl <em>Active Interrupt Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveInterruptConditionImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveInterruptCondition()
		 * @generated
		 */
		EClass ACTIVE_INTERRUPT_CONDITION = eINSTANCE.getActiveInterruptCondition();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveLoopImpl <em>Active Loop</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveLoopImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveLoop()
		 * @generated
		 */
		EClass ACTIVE_LOOP = eINSTANCE.getActiveLoop();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveModalMessageImpl <em>Active Modal Message</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveModalMessageImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveModalMessage()
		 * @generated
		 */
		EClass ACTIVE_MODAL_MESSAGE = eINSTANCE.getActiveModalMessage();

		/**
		 * The meta object literal for the '<em><b>Active Message Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MODAL_MESSAGE__ACTIVE_MESSAGE_PARAMETERS = eINSTANCE.getActiveModalMessage_ActiveMessageParameters();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveParallelImpl <em>Active Parallel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveParallelImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveParallel()
		 * @generated
		 */
		EClass ACTIVE_PARALLEL = eINSTANCE.getActiveParallel();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveVariableFragmentImpl <em>Active Variable Fragment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveVariableFragmentImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveVariableFragment()
		 * @generated
		 */
		EClass ACTIVE_VARIABLE_FRAGMENT = eINSTANCE.getActiveVariableFragment();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveViolationConditionImpl <em>Active Violation Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveViolationConditionImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveViolationCondition()
		 * @generated
		 */
		EClass ACTIVE_VIOLATION_CONDITION = eINSTANCE.getActiveViolationCondition();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveWaitConditionImpl <em>Active Wait Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveWaitConditionImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveWaitCondition()
		 * @generated
		 */
		EClass ACTIVE_WAIT_CONDITION = eINSTANCE.getActiveWaitCondition();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveScenarioRoleBindingsImpl <em>Active Scenario Role Bindings</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveScenarioRoleBindingsImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioRoleBindings()
		 * @generated
		 */
		EClass ACTIVE_SCENARIO_ROLE_BINDINGS = eINSTANCE.getActiveScenarioRoleBindings();

		/**
		 * The meta object literal for the '<em><b>Role Bindings</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_SCENARIO_ROLE_BINDINGS__ROLE_BINDINGS = eINSTANCE.getActiveScenarioRoleBindings_RoleBindings();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.EClassToEObjectMapEntryImpl <em>EClass To EObject Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.EClassToEObjectMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getEClassToEObjectMapEntry()
		 * @generated
		 */
		EClass ECLASS_TO_EOBJECT_MAP_ENTRY = eINSTANCE.getEClassToEObjectMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ECLASS_TO_EOBJECT_MAP_ENTRY__KEY = eINSTANCE.getEClassToEObjectMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ECLASS_TO_EOBJECT_MAP_ENTRY__VALUE = eINSTANCE.getEClassToEObjectMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.RoleToEObjectMapEntryImpl <em>Role To EObject Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.RoleToEObjectMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getRoleToEObjectMapEntry()
		 * @generated
		 */
		EClass ROLE_TO_EOBJECT_MAP_ENTRY = eINSTANCE.getRoleToEObjectMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_TO_EOBJECT_MAP_ENTRY__KEY = eINSTANCE.getRoleToEObjectMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_TO_EOBJECT_MAP_ENTRY__VALUE = eINSTANCE.getRoleToEObjectMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveInteractionKeyWrapperToActiveInteractionMapEntryImpl <em>Active Interaction Key Wrapper To Active Interaction Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveInteractionKeyWrapperToActiveInteractionMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveInteractionKeyWrapperToActiveInteractionMapEntry()
		 * @generated
		 */
		EClass ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY = eINSTANCE.getActiveInteractionKeyWrapperToActiveInteractionMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY__KEY = eINSTANCE.getActiveInteractionKeyWrapperToActiveInteractionMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY__VALUE = eINSTANCE.getActiveInteractionKeyWrapperToActiveInteractionMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveScenarioKeyWrapperToActiveScenarioMapEntryImpl <em>Active Scenario Key Wrapper To Active Scenario Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveScenarioKeyWrapperToActiveScenarioMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioKeyWrapperToActiveScenarioMapEntry()
		 * @generated
		 */
		EClass ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY = eINSTANCE.getActiveScenarioKeyWrapperToActiveScenarioMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY__KEY = eINSTANCE.getActiveScenarioKeyWrapperToActiveScenarioMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY__VALUE = eINSTANCE.getActiveScenarioKeyWrapperToActiveScenarioMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ObjectSystemKeyWrapperToObjectSystemMapEntryImpl <em>Object System Key Wrapper To Object System Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ObjectSystemKeyWrapperToObjectSystemMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getObjectSystemKeyWrapperToObjectSystemMapEntry()
		 * @generated
		 */
		EClass OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY = eINSTANCE.getObjectSystemKeyWrapperToObjectSystemMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY__KEY = eINSTANCE.getObjectSystemKeyWrapperToObjectSystemMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY__VALUE = eINSTANCE.getObjectSystemKeyWrapperToObjectSystemMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ObjectSystemKeyWrapperToDynamicObjectContainerMapEntryImpl <em>Object System Key Wrapper To Dynamic Object Container Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ObjectSystemKeyWrapperToDynamicObjectContainerMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry()
		 * @generated
		 */
		EClass OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY = eINSTANCE.getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY__KEY = eINSTANCE.getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY__VALUE = eINSTANCE.getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.StateKeyWrapperToStateMapEntryImpl <em>State Key Wrapper To State Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.StateKeyWrapperToStateMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getStateKeyWrapperToStateMapEntry()
		 * @generated
		 */
		EClass STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY = eINSTANCE.getStateKeyWrapperToStateMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY__KEY = eINSTANCE.getStateKeyWrapperToStateMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY__VALUE = eINSTANCE.getStateKeyWrapperToStateMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.VariableToObjectMapEntryImpl <em>Variable To Object Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.VariableToObjectMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getVariableToObjectMapEntry()
		 * @generated
		 */
		EClass VARIABLE_TO_OBJECT_MAP_ENTRY = eINSTANCE.getVariableToObjectMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_TO_OBJECT_MAP_ENTRY__KEY = eINSTANCE.getVariableToObjectMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VARIABLE_TO_OBJECT_MAP_ENTRY__VALUE = eINSTANCE.getVariableToObjectMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.VariableToEObjectMapEntryImpl <em>Variable To EObject Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.VariableToEObjectMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getVariableToEObjectMapEntry()
		 * @generated
		 */
		EClass VARIABLE_TO_EOBJECT_MAP_ENTRY = eINSTANCE.getVariableToEObjectMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_TO_EOBJECT_MAP_ENTRY__KEY = eINSTANCE.getVariableToEObjectMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_TO_EOBJECT_MAP_ENTRY__VALUE = eINSTANCE.getVariableToEObjectMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryImpl <em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry()
		 * @generated
		 */
		EClass ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY = eINSTANCE.getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__KEY = eINSTANCE.getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__VALUE = eINSTANCE.getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.DynamicObjectContainerImpl <em>Dynamic Object Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.DynamicObjectContainerImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getDynamicObjectContainer()
		 * @generated
		 */
		EClass DYNAMIC_OBJECT_CONTAINER = eINSTANCE.getDynamicObjectContainer();

		/**
		 * The meta object literal for the '<em><b>Static EObject To Dynamic EObject Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DYNAMIC_OBJECT_CONTAINER__STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP = eINSTANCE.getDynamicObjectContainer_StaticEObjectToDynamicEObjectMap();

		/**
		 * The meta object literal for the '<em><b>Root Objects</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DYNAMIC_OBJECT_CONTAINER__ROOT_OBJECTS = eINSTANCE.getDynamicObjectContainer_RootObjects();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.StaticEObjectToDynamicEObjectMapEntryImpl <em>Static EObject To Dynamic EObject Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.StaticEObjectToDynamicEObjectMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getStaticEObjectToDynamicEObjectMapEntry()
		 * @generated
		 */
		EClass STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY = eINSTANCE.getStaticEObjectToDynamicEObjectMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY__KEY = eINSTANCE.getStaticEObjectToDynamicEObjectMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY__VALUE = eINSTANCE.getStaticEObjectToDynamicEObjectMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.MessageEventExtensionInterfaceImpl <em>Message Event Extension Interface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.MessageEventExtensionInterfaceImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getMessageEventExtensionInterface()
		 * @generated
		 */
		EClass MESSAGE_EVENT_EXTENSION_INTERFACE = eINSTANCE.getMessageEventExtensionInterface();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MESSAGE_EVENT_EXTENSION_INTERFACE___INIT__CONFIGURATION = eINSTANCE.getMessageEventExtensionInterface__Init__Configuration();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.MessageEventSideEffectsExecutorImpl <em>Message Event Side Effects Executor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.MessageEventSideEffectsExecutorImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getMessageEventSideEffectsExecutor()
		 * @generated
		 */
		EClass MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR = eINSTANCE.getMessageEventSideEffectsExecutor();

		/**
		 * The meta object literal for the '<em><b>Execute Side Effects</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR___EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = eINSTANCE.getMessageEventSideEffectsExecutor__ExecuteSideEffects__MessageEvent_DynamicObjectContainer();

		/**
		 * The meta object literal for the '<em><b>Can Execute Side Effects</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR___CAN_EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = eINSTANCE.getMessageEventSideEffectsExecutor__CanExecuteSideEffects__MessageEvent_DynamicObjectContainer();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.MessageEventIsIndependentEvaluatorImpl <em>Message Event Is Independent Evaluator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.MessageEventIsIndependentEvaluatorImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getMessageEventIsIndependentEvaluator()
		 * @generated
		 */
		EClass MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR = eINSTANCE.getMessageEventIsIndependentEvaluator();

		/**
		 * The meta object literal for the '<em><b>Is Independent</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR___IS_INDEPENDENT__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = eINSTANCE.getMessageEventIsIndependentEvaluator__IsIndependent__MessageEvent_DynamicObjectContainer();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.ActiveMessageParameter <em>Active Message Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.ActiveMessageParameter
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveMessageParameter()
		 * @generated
		 */
		EClass ACTIVE_MESSAGE_PARAMETER = eINSTANCE.getActiveMessageParameter();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_MESSAGE_PARAMETER___INIT__PARAMETERVALUE = eINSTANCE.getActiveMessageParameter__Init__ParameterValue();

		/**
		 * The meta object literal for the '<em><b>Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_MESSAGE_PARAMETER___UPDATE__PARAMETERVALUE_ACTIVEPART_ACTIVESCENARIO_SMLRUNTIMESTATE = eINSTANCE.getActiveMessageParameter__Update__ParameterValue_ActivePart_ActiveScenario_SMLRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Has Side Effects On Unification</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_MESSAGE_PARAMETER___HAS_SIDE_EFFECTS_ON_UNIFICATION = eINSTANCE.getActiveMessageParameter__HasSideEffectsOnUnification();

		/**
		 * The meta object literal for the '<em><b>Execute Side Effects On Unification</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_MESSAGE_PARAMETER___EXECUTE_SIDE_EFFECTS_ON_UNIFICATION__PARAMETERVALUE_PARAMETERVALUE_ACTIVESCENARIO_SMLRUNTIMESTATE = eINSTANCE.getActiveMessageParameter__ExecuteSideEffectsOnUnification__ParameterValue_ParameterValue_ActiveScenario_SMLRuntimeState();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithValueExpressionImpl <em>Active Message Parameter With Value Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithValueExpressionImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveMessageParameterWithValueExpression()
		 * @generated
		 */
		EClass ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION = eINSTANCE.getActiveMessageParameterWithValueExpression();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MESSAGE_PARAMETER_WITH_VALUE_EXPRESSION__PARAMETER = eINSTANCE.getActiveMessageParameterWithValueExpression_Parameter();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithBindToVarImpl <em>Active Message Parameter With Bind To Var</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithBindToVarImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveMessageParameterWithBindToVar()
		 * @generated
		 */
		EClass ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR = eINSTANCE.getActiveMessageParameterWithBindToVar();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR__PARAMETER = eINSTANCE.getActiveMessageParameterWithBindToVar_Parameter();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithWildcardImpl <em>Active Message Parameter With Wildcard</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithWildcardImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveMessageParameterWithWildcard()
		 * @generated
		 */
		EClass ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD = eINSTANCE.getActiveMessageParameterWithWildcard();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD__PARAMETER = eINSTANCE.getActiveMessageParameterWithWildcard_Parameter();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveConstraintImpl <em>Active Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveConstraintImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveConstraint()
		 * @generated
		 */
		EClass ACTIVE_CONSTRAINT = eINSTANCE.getActiveConstraint();

		/**
		 * The meta object literal for the '<em><b>Constraint Message Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_CONSTRAINT__CONSTRAINT_MESSAGE_EVENT = eINSTANCE.getActiveConstraint_ConstraintMessageEvent();

		/**
		 * The meta object literal for the '<em><b>Active Message Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_CONSTRAINT__ACTIVE_MESSAGE_PARAMETERS = eINSTANCE.getActiveConstraint_ActiveMessageParameters();

		/**
		 * The meta object literal for the '<em><b>Message</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_CONSTRAINT__MESSAGE = eINSTANCE.getActiveConstraint_Message();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_CONSTRAINT___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = eINSTANCE.getActiveConstraint__Init__ActiveScenarioRoleBindings_ActivePart_ActiveScenario();

		/**
		 * The meta object literal for the '<em><b>Update Constraint Event</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_CONSTRAINT___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE = eINSTANCE.getActiveConstraint__UpdateConstraintEvent__ActiveScenario_SMLRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Add To Parent Specific Constraint List</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_CONSTRAINT___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT = eINSTANCE.getActiveConstraint__AddToParentSpecificConstraintList__ActiveInteraction_MessageEvent();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveConstraintConsiderImpl <em>Active Constraint Consider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveConstraintConsiderImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveConstraintConsider()
		 * @generated
		 */
		EClass ACTIVE_CONSTRAINT_CONSIDER = eINSTANCE.getActiveConstraintConsider();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveConstraintIgnoreImpl <em>Active Constraint Ignore</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveConstraintIgnoreImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveConstraintIgnore()
		 * @generated
		 */
		EClass ACTIVE_CONSTRAINT_IGNORE = eINSTANCE.getActiveConstraintIgnore();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveConstraintInterruptImpl <em>Active Constraint Interrupt</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveConstraintInterruptImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveConstraintInterrupt()
		 * @generated
		 */
		EClass ACTIVE_CONSTRAINT_INTERRUPT = eINSTANCE.getActiveConstraintInterrupt();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveConstraintForbiddenImpl <em>Active Constraint Forbidden</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveConstraintForbiddenImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveConstraintForbidden()
		 * @generated
		 */
		EClass ACTIVE_CONSTRAINT_FORBIDDEN = eINSTANCE.getActiveConstraintForbidden();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ParameterRangesProviderImpl <em>Parameter Ranges Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ParameterRangesProviderImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getParameterRangesProvider()
		 * @generated
		 */
		EClass PARAMETER_RANGES_PROVIDER = eINSTANCE.getParameterRangesProvider();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PARAMETER_RANGES_PROVIDER___INIT__CONFIGURATION = eINSTANCE.getParameterRangesProvider__Init__Configuration();

		/**
		 * The meta object literal for the '<em><b>Get Parameter Values</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PARAMETER_RANGES_PROVIDER___GET_PARAMETER_VALUES__ETYPEDELEMENT_SMLOBJECTSYSTEM = eINSTANCE.getParameterRangesProvider__GetParameterValues__ETypedElement_SMLObjectSystem();

		/**
		 * The meta object literal for the '<em><b>Get Singel Parameter Value</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PARAMETER_RANGES_PROVIDER___GET_SINGEL_PARAMETER_VALUE__OBJECT = eINSTANCE.getParameterRangesProvider__GetSingelParameterValue__Object();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PARAMETER_RANGES_PROVIDER___INIT__ELIST = eINSTANCE.getParameterRangesProvider__Init__EList();

		/**
		 * The meta object literal for the '<em><b>Contains Parameter Values</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PARAMETER_RANGES_PROVIDER___CONTAINS_PARAMETER_VALUES__ETYPEDELEMENT = eINSTANCE.getParameterRangesProvider__ContainsParameterValues__ETypedElement();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.MessageEventBlockedInformationImpl <em>Message Event Blocked Information</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.MessageEventBlockedInformationImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getMessageEventBlockedInformation()
		 * @generated
		 */
		EClass MESSAGE_EVENT_BLOCKED_INFORMATION = eINSTANCE.getMessageEventBlockedInformation();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_EVENT_BLOCKED_INFORMATION__DESCRIPTION = eINSTANCE.getMessageEventBlockedInformation_Description();

		/**
		 * The meta object literal for the '<em><b>Message Event String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_EVENT_BLOCKED_INFORMATION__MESSAGE_EVENT_STRING = eINSTANCE.getMessageEventBlockedInformation_MessageEventString();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.MultiActiveScenarioInitializationsImpl <em>Multi Active Scenario Initializations</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.MultiActiveScenarioInitializationsImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getMultiActiveScenarioInitializations()
		 * @generated
		 */
		EClass MULTI_ACTIVE_SCENARIO_INITIALIZATIONS = eINSTANCE.getMultiActiveScenarioInitializations();

		/**
		 * The meta object literal for the '<em><b>Active Scenario To Active Scenario Progress Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MULTI_ACTIVE_SCENARIO_INITIALIZATIONS__ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP = eINSTANCE.getMultiActiveScenarioInitializations_ActiveScenarioToActiveScenarioProgressMap();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveScenarioToActiveScenarioProgressMapEntryImpl <em>Active Scenario To Active Scenario Progress Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveScenarioToActiveScenarioProgressMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioToActiveScenarioProgressMapEntry()
		 * @generated
		 */
		EClass ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP_ENTRY = eINSTANCE.getActiveScenarioToActiveScenarioProgressMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP_ENTRY__KEY = eINSTANCE.getActiveScenarioToActiveScenarioProgressMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVE_SCENARIO_TO_ACTIVE_SCENARIO_PROGRESS_MAP_ENTRY__VALUE = eINSTANCE.getActiveScenarioToActiveScenarioProgressMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.EventToTransitionMapEntryImpl <em>Event To Transition Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.EventToTransitionMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getEventToTransitionMapEntry()
		 * @generated
		 */
		EClass EVENT_TO_TRANSITION_MAP_ENTRY = eINSTANCE.getEventToTransitionMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_TO_TRANSITION_MAP_ENTRY__KEY = eINSTANCE.getEventToTransitionMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_TO_TRANSITION_MAP_ENTRY__VALUE = eINSTANCE.getEventToTransitionMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.TransitionImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>Source State</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__SOURCE_STATE = eINSTANCE.getTransition_SourceState();

		/**
		 * The meta object literal for the '<em><b>Target State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__TARGET_STATE = eINSTANCE.getTransition_TargetState();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__EVENT = eINSTANCE.getTransition_Event();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__LABEL = eINSTANCE.getTransition_Label();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.AnnotatableElementImpl <em>Annotatable Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.AnnotatableElementImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getAnnotatableElement()
		 * @generated
		 */
		EClass ANNOTATABLE_ELEMENT = eINSTANCE.getAnnotatableElement();

		/**
		 * The meta object literal for the '<em><b>String To Boolean Annotation Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANNOTATABLE_ELEMENT__STRING_TO_BOOLEAN_ANNOTATION_MAP = eINSTANCE.getAnnotatableElement_StringToBooleanAnnotationMap();

		/**
		 * The meta object literal for the '<em><b>String To String Annotation Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANNOTATABLE_ELEMENT__STRING_TO_STRING_ANNOTATION_MAP = eINSTANCE.getAnnotatableElement_StringToStringAnnotationMap();

		/**
		 * The meta object literal for the '<em><b>String To EObject Annotation Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANNOTATABLE_ELEMENT__STRING_TO_EOBJECT_ANNOTATION_MAP = eINSTANCE.getAnnotatableElement_StringToEObjectAnnotationMap();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.StringToBooleanMapEntryImpl <em>String To Boolean Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.StringToBooleanMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getStringToBooleanMapEntry()
		 * @generated
		 */
		EClass STRING_TO_BOOLEAN_MAP_ENTRY = eINSTANCE.getStringToBooleanMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_BOOLEAN_MAP_ENTRY__KEY = eINSTANCE.getStringToBooleanMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_BOOLEAN_MAP_ENTRY__VALUE = eINSTANCE.getStringToBooleanMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.StringToStringMapEntryImpl <em>String To String Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.StringToStringMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getStringToStringMapEntry()
		 * @generated
		 */
		EClass STRING_TO_STRING_MAP_ENTRY = eINSTANCE.getStringToStringMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_STRING_MAP_ENTRY__KEY = eINSTANCE.getStringToStringMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_STRING_MAP_ENTRY__VALUE = eINSTANCE.getStringToStringMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.StringToEObjectMapEntryImpl <em>String To EObject Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.StringToEObjectMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getStringToEObjectMapEntry()
		 * @generated
		 */
		EClass STRING_TO_EOBJECT_MAP_ENTRY = eINSTANCE.getStringToEObjectMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_EOBJECT_MAP_ENTRY__KEY = eINSTANCE.getStringToEObjectMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRING_TO_EOBJECT_MAP_ENTRY__VALUE = eINSTANCE.getStringToEObjectMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.EventImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.MessageEventImpl <em>Message Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.MessageEventImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getMessageEvent()
		 * @generated
		 */
		EClass MESSAGE_EVENT = eINSTANCE.getMessageEvent();

		/**
		 * The meta object literal for the '<em><b>Sending Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT__SENDING_OBJECT = eINSTANCE.getMessageEvent_SendingObject();

		/**
		 * The meta object literal for the '<em><b>Receiving Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT__RECEIVING_OBJECT = eINSTANCE.getMessageEvent_ReceivingObject();

		/**
		 * The meta object literal for the '<em><b>Message Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_EVENT__MESSAGE_NAME = eINSTANCE.getMessageEvent_MessageName();

		/**
		 * The meta object literal for the '<em><b>Concrete</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_EVENT__CONCRETE = eINSTANCE.getMessageEvent_Concrete();

		/**
		 * The meta object literal for the '<em><b>Parameterized</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_EVENT__PARAMETERIZED = eINSTANCE.getMessageEvent_Parameterized();

		/**
		 * The meta object literal for the '<em><b>Typed Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT__TYPED_ELEMENT = eINSTANCE.getMessageEvent_TypedElement();

		/**
		 * The meta object literal for the '<em><b>Parameter Values</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT__PARAMETER_VALUES = eINSTANCE.getMessageEvent_ParameterValues();

		/**
		 * The meta object literal for the '<em><b>Collection Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_EVENT__COLLECTION_OPERATION = eINSTANCE.getMessageEvent_CollectionOperation();

		/**
		 * The meta object literal for the '<em><b>Eq</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MESSAGE_EVENT___EQ__MESSAGEEVENT = eINSTANCE.getMessageEvent__Eq__MessageEvent();

		/**
		 * The meta object literal for the '<em><b>Is Message Unifiable With</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MESSAGE_EVENT___IS_MESSAGE_UNIFIABLE_WITH__MESSAGEEVENT = eINSTANCE.getMessageEvent__IsMessageUnifiableWith__MessageEvent();

		/**
		 * The meta object literal for the '<em><b>Is Parameter Unifiable With</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MESSAGE_EVENT___IS_PARAMETER_UNIFIABLE_WITH__MESSAGEEVENT = eINSTANCE.getMessageEvent__IsParameterUnifiableWith__MessageEvent();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ParameterValueImpl <em>Parameter Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ParameterValueImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getParameterValue()
		 * @generated
		 */
		EClass PARAMETER_VALUE = eINSTANCE.getParameterValue();

		/**
		 * The meta object literal for the '<em><b>EParameter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER_VALUE__EPARAMETER = eINSTANCE.getParameterValue_EParameter();

		/**
		 * The meta object literal for the '<em><b>Unset</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_VALUE__UNSET = eINSTANCE.getParameterValue_Unset();

		/**
		 * The meta object literal for the '<em><b>Wildcard Parameter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_VALUE__WILDCARD_PARAMETER = eINSTANCE.getParameterValue_WildcardParameter();

		/**
		 * The meta object literal for the '<em><b>Struc Feature Or EOp</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER_VALUE__STRUC_FEATURE_OR_EOP = eINSTANCE.getParameterValue_StrucFeatureOrEOp();

		/**
		 * The meta object literal for the '<em><b>Set Value</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PARAMETER_VALUE___SET_VALUE__OBJECT = eINSTANCE.getParameterValue__SetValue__Object();

		/**
		 * The meta object literal for the '<em><b>Get Value</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PARAMETER_VALUE___GET_VALUE = eINSTANCE.getParameterValue__GetValue();

		/**
		 * The meta object literal for the '<em><b>Eq</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PARAMETER_VALUE___EQ__PARAMETERVALUE = eINSTANCE.getParameterValue__Eq__ParameterValue();

		/**
		 * The meta object literal for the '<em><b>Is Parameter Unifiable With</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE = eINSTANCE.getParameterValue__IsParameterUnifiableWith__ParameterValue();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.BooleanParameterValueImpl <em>Boolean Parameter Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.BooleanParameterValueImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getBooleanParameterValue()
		 * @generated
		 */
		EClass BOOLEAN_PARAMETER_VALUE = eINSTANCE.getBooleanParameterValue();

		/**
		 * The meta object literal for the '<em><b>Boolean Parameter Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOLEAN_PARAMETER_VALUE__BOOLEAN_PARAMETER_VALUE = eINSTANCE.getBooleanParameterValue_BooleanParameterValue();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.IntegerParameterValueImpl <em>Integer Parameter Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.IntegerParameterValueImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getIntegerParameterValue()
		 * @generated
		 */
		EClass INTEGER_PARAMETER_VALUE = eINSTANCE.getIntegerParameterValue();

		/**
		 * The meta object literal for the '<em><b>Integer Parameter Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_PARAMETER_VALUE__INTEGER_PARAMETER_VALUE = eINSTANCE.getIntegerParameterValue_IntegerParameterValue();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.StringParameterValueImpl <em>String Parameter Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.StringParameterValueImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getStringParameterValue()
		 * @generated
		 */
		EClass STRING_PARAMETER_VALUE = eINSTANCE.getStringParameterValue();

		/**
		 * The meta object literal for the '<em><b>String Parameter Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_PARAMETER_VALUE__STRING_PARAMETER_VALUE = eINSTANCE.getStringParameterValue_StringParameterValue();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.EObjectParameterValueImpl <em>EObject Parameter Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.EObjectParameterValueImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getEObjectParameterValue()
		 * @generated
		 */
		EClass EOBJECT_PARAMETER_VALUE = eINSTANCE.getEObjectParameterValue();

		/**
		 * The meta object literal for the '<em><b>EObject Parameter Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EOBJECT_PARAMETER_VALUE__EOBJECT_PARAMETER_VALUE = eINSTANCE.getEObjectParameterValue_EObjectParameterValue();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.EEnumParameterValueImpl <em>EEnum Parameter Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.EEnumParameterValueImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getEEnumParameterValue()
		 * @generated
		 */
		EClass EENUM_PARAMETER_VALUE = eINSTANCE.getEEnumParameterValue();

		/**
		 * The meta object literal for the '<em><b>EEnum Parameter Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EENUM_PARAMETER_VALUE__EENUM_PARAMETER_TYPE = eINSTANCE.getEEnumParameterValue_EEnumParameterType();

		/**
		 * The meta object literal for the '<em><b>EEnum Parameter Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EENUM_PARAMETER_VALUE__EENUM_PARAMETER_VALUE = eINSTANCE.getEEnumParameterValue_EEnumParameterValue();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.WaitEventImpl <em>Wait Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.WaitEventImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getWaitEvent()
		 * @generated
		 */
		EClass WAIT_EVENT = eINSTANCE.getWaitEvent();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.ViolationKind <em>Violation Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.ViolationKind
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getViolationKind()
		 * @generated
		 */
		EEnum VIOLATION_KIND = eINSTANCE.getViolationKind();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.ActiveScenarioProgress <em>Active Scenario Progress</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.ActiveScenarioProgress
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioProgress()
		 * @generated
		 */
		EEnum ACTIVE_SCENARIO_PROGRESS = eINSTANCE.getActiveScenarioProgress();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.BlockedType <em>Blocked Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.BlockedType
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getBlockedType()
		 * @generated
		 */
		EEnum BLOCKED_TYPE = eINSTANCE.getBlockedType();

		/**
		 * The meta object literal for the '<em>Active Interaction Key Wrapper</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.keywrapper.ActiveInteractionKeyWrapper
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveInteractionKeyWrapper()
		 * @generated
		 */
		EDataType ACTIVE_INTERACTION_KEY_WRAPPER = eINSTANCE.getActiveInteractionKeyWrapper();

		/**
		 * The meta object literal for the '<em>Active Scenario Key Wrapper</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioKeyWrapper()
		 * @generated
		 */
		EDataType ACTIVE_SCENARIO_KEY_WRAPPER = eINSTANCE.getActiveScenarioKeyWrapper();

		/**
		 * The meta object literal for the '<em>Object System Key Wrapper</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.keywrapper.ObjectSystemKeyWrapper
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getObjectSystemKeyWrapper()
		 * @generated
		 */
		EDataType OBJECT_SYSTEM_KEY_WRAPPER = eINSTANCE.getObjectSystemKeyWrapper();

		/**
		 * The meta object literal for the '<em>State Key Wrapper</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.keywrapper.StateKeyWrapper
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getStateKeyWrapper()
		 * @generated
		 */
		EDataType STATE_KEY_WRAPPER = eINSTANCE.getStateKeyWrapper();

		/**
		 * The meta object literal for the '<em>Active Scenario Role Bindings Key Wrapper</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.keywrapper.ActiveScenarioRoleBindingsKeyWrapper
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioRoleBindingsKeyWrapper()
		 * @generated
		 */
		EDataType ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER = eINSTANCE.getActiveScenarioRoleBindingsKeyWrapper();

		/**
		 * The meta object literal for the '<em>Parameter Values</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.logic.ParameterRanges.ParameterValues
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getParameterValues()
		 * @generated
		 */
		EDataType PARAMETER_VALUES = eINSTANCE.getParameterValues();

	}

} //RuntimePackage
