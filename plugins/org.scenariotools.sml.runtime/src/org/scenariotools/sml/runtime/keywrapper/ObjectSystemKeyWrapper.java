/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.keywrapper;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.EqualityHelper;

public class ObjectSystemKeyWrapper {

	int hashCode;
	private EList<EObject> rootObjects;
	private EMap<EObject,Integer> eObjectToHashValueMap;
	private EList<EObject> allEObjectList;
	
	public ObjectSystemKeyWrapper(EList<EObject> rootObjects) {
		this.rootObjects = rootObjects;
		hashCode = 1;
		
		eObjectToHashValueMap = new BasicEMap<EObject,Integer>();
		allEObjectList = new BasicEList<EObject>();
		
		allEObjectList.addAll(rootObjects);
		
		// calculates the hashcode as follows
		// 1. it recursively calculates the hash value for all objects based on their attribute values 
		//    and the hash values of contained Objects. I.e. non-containment references are ignored.
		//    These hashValues are stored in the eObjectToHashValueMap.
		// 2. We iterate over all objects (key list of of the eObjectToHashValueMap)
		//    and update the stored hash values of the objects based on their non-containment references.
		// 3. We compute a hash value based on the hash values stored in the map.
		
		// 1.
		for (EObject rootEObject : rootObjects) {
			hashCode += hashCode*31 + calculateHashCodeForEObject(rootEObject);
		}
		
//		for (EObject eObject : allEObjectList) {
//			System.out.println("Object/nashValue: " + eObject + " --> " +  eObjectToHashValueMap.get(eObject));
//		}
		
		
		// 2.
		for (EObject eObject : allEObjectList) {
		    for (EStructuralFeature feature : eObject.eClass().getEAllStructuralFeatures()){
		    	if(feature instanceof EReference
	        			&& !((EReference)feature).isContainment()){
		    		if (!eObject.eIsSet(feature)){
						eObjectToHashValueMap.put(eObject, eObjectToHashValueMap.get(eObject)*31 + Boolean.FALSE.hashCode());
		    		}else{
		        		EReference eReference = (EReference)feature;
		        		if (eReference.isMany()){
			        		EList<EObject> referencedEObjects = (EList<EObject>) eObject.eGet(feature);
			        		if (eReference.isOrdered()){
								int hashValueForList = 1; 
								for (EObject referencedEObject : referencedEObjects) {
//									System.out.println(eObjectToHashValueMap.get(eObject));
//									System.out.println(eObjectToHashValueMap.get(referencedEObject));
									hashValueForList += hashValueForList*31 + eObjectToHashValueMap.get(referencedEObject);
								}
								eObjectToHashValueMap.put(eObject, eObjectToHashValueMap.get(eObject)*31 + hashValueForList);
			        		}else{
								int hashValueForSet = 1; 
								for (EObject referencedEObject : referencedEObjects) {
									hashValueForSet += eObjectToHashValueMap.get(referencedEObject);
								}			        			
								eObjectToHashValueMap.put(eObject, eObjectToHashValueMap.get(eObject)*31 + hashValueForSet);
			        		}
		        		}else{
		        			EObject referencedEObject = (EObject) eObject.eGet(feature);
							eObjectToHashValueMap.put(eObject, eObjectToHashValueMap.get(eObject)*31 + eObjectToHashValueMap.get(referencedEObject));
		        		}
		    		}
		    	}
		    }
		}
		
		// 3.
		for (EObject eObject : allEObjectList) {
//			hashCode += hashCode*31 + eObjectToHashValueMap.get(eObject);
			hashCode += eObjectToHashValueMap.get(eObject);
		}
		
		eObjectToHashValueMap = null;
		allEObjectList = null;
		
	}
	
	/**
	 * This will calculate the hashCode for the object, its attribute values and all recursively contained objects.
	 * I.e. cross-references will be ignored.
	 * @param eObject
	 * @return
	 */
	protected int calculateHashCodeForEObject(EObject eObject){
		int hashValue = 1;
		
		// What identifies an eObject?
		
		// 1. its class
		EClass eClass = eObject.eClass();
		hashValue += hashValue*31 + eClass.hashCode();
		
		// the value of all non-derived features:
	    for (EStructuralFeature feature : eObject.eClass().getEAllStructuralFeatures()){
	        if (!feature.isDerived()){
	        	if (!eObject.eIsSet(feature)){
	        		hashValue += hashValue*31 + Boolean.FALSE.hashCode();
	        	}else if (feature instanceof EAttribute){
	        		hashValue += hashValue*31 + calculateHashCodeForEObjectAttribute(eObject, (EAttribute) feature);
	        	}else if (feature instanceof EReference
	        			&& ((EReference)feature).isContainment()){
	        		EReference eReference = (EReference)feature;
	        		if (eReference.isMany()){
		        		EList<EObject> containedObjects = (EList<EObject>) eObject.eGet(feature);
		        		allEObjectList.addAll(containedObjects);
		        		if (eReference.isOrdered()){
							for (EObject containedEObject : containedObjects) {
				        		int hashValueOfChildEObject = calculateHashCodeForEObject(containedEObject);
				        		hashValue += hashValue*31 + hashValueOfChildEObject;
							}
		        		}else{
							int hashCodeForContainedSet = 1;
							for (EObject containedEObject : containedObjects) {
				        		int hashValueOfChildEObject = calculateHashCodeForEObject(containedEObject);
				        		hashCodeForContainedSet += hashValueOfChildEObject; 
							}
			        		hashValue += hashValue*31 + hashCodeForContainedSet;
		        		}
	        		}else{
	        			EObject containedEObject = (EObject) eObject.eGet(feature);
		        		hashValue += hashValue*31 + calculateHashCodeForEObject(containedEObject);
		        		allEObjectList.add(containedEObject);
	        		}
	        	} 
	        }
	    }
		eObjectToHashValueMap.put(eObject, hashValue);
	    return hashValue;
	}


	@SuppressWarnings("unchecked")
	protected int calculateHashCodeForEObjectAttribute(EObject eObject, EAttribute eAttribute){
		if(eAttribute.isMany()){
			int hashValue = 1;
			if(eAttribute.isOrdered()){
				for (Object listObject : ((Collection<Object>)eObject.eGet(eAttribute))) {
					hashValue += hashValue*31 + listObject.hashCode();
				}
			}else{
				Assert.isTrue(false, "ordered attributes are not (yet) supported, see how the object mapping is created in MSDRuntimeState.getMSDRuntimeState");
				for (Object listObject : ((Collection<Object>)eObject.eGet(eAttribute))) {
					hashValue += listObject.hashCode();
				}
			}
			return hashValue;
		}else
			return eObject.eGet(eAttribute).hashCode();
	}
	
	public EList<EObject> getRootObjects() {
		return rootObjects;
	}
	
	@Override
	public int hashCode() {
		return hashCode;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ObjectSystemKeyWrapper){
			ObjectSystemKeyWrapper objectSystemKeyWrapper = (ObjectSystemKeyWrapper) obj;
			
			EList<EObject> otherObjectSystemRootObjects = objectSystemKeyWrapper.getRootObjects();
			EList<EObject> objectSystemRootObjects = getRootObjects();
			
			// Use a custom implementation of EqualityHelper that handles
			// ordered/unordered references correctly.
			EqualityHelper customHelper = new EqualityHelper() {
				@Override
				@SuppressWarnings("unchecked")
				protected boolean haveEqualReference(EObject eObject1, EObject eObject2, EReference reference) {

					Object value1 = eObject1.eGet(reference);
					Object value2 = eObject2.eGet(reference);
					if (reference.isMany()) {
						if (reference.isOrdered()) {
							return super.equals((List<EObject>) value1, (List<EObject>) value2);
						} else {
							return unorderedEquals((List<EObject>) value1, (List<EObject>) value2);
						}
					} else
						return equals((EObject) value1, (EObject) value2);

				}

				/**
				 * 
				 * @return <code>true</code> if for any object o1 in list1 an
				 *         object o2 in list2 exists such that o1 and o2 are
				 *         structurally equal.
				 */
				private boolean unorderedEquals(List<EObject> list1, List<EObject> list2) {
					
					if (list1.size() == list2.size()) {
						list2 = new LinkedList<>(list2);
						ListIterator<EObject> it2 = list2.listIterator();
						outer: for (EObject o1 : list1) {
							while (it2.hasNext()) {
								if (equals(o1, it2.next())) {
									it2.remove();
									continue outer;
								}
							}
							break;
						}
						return true;
					}
					return false;
				}
			};
			return customHelper.equals(otherObjectSystemRootObjects, objectSystemRootObjects);
			
		}
		return false;
	}
	
}
