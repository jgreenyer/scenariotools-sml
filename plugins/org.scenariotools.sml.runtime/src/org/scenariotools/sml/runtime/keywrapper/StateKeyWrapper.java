/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.keywrapper;

import java.util.HashSet;

import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.SMLRuntimeState;

public class StateKeyWrapper extends KeyWrapper {
	public StateKeyWrapper(SMLRuntimeState state) {
		addSubObject(state.isSafetyViolationOccurredInAssumptions());
//		addSubObject(state.isSafetyViolationOccurredInRequirements());
		addSubObject(state.isSafetyViolationOccurredInGuarantees());
		addSubObject(state.isSystemChoseToWait());
		addSubObject(new HashSet<ActiveScenario>(state.getActiveScenarios()));
		addSubObject(state.getObjectSystem());
		addSubObject(state.getDynamicObjectContainer());
	}
}
