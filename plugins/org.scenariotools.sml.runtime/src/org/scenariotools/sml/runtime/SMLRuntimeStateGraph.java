/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.sml.runtime.configuration.Configuration;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SML Runtime State Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Represents the statespace of an execution of an SML specification with playout semantics by an object system.
 * After instantiation of this class, clients must call the init(Configuration) operation first.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getConfiguration <em>Configuration</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getElementContainer <em>Element Container</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getParameterRangesProvider <em>Parameter Ranges Provider</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getStates <em>States</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getStartState <em>Start State</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeStateGraph()
 * @model
 * @generated
 */
public interface SMLRuntimeStateGraph extends EObject {
	/**
	 * Returns the value of the '<em><b>Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Reference to the runtime configuration (runconfig), that this StateGraph instance is initialized with.
	 * Clients should NOT set this reference.
	 * 
	 * @see SMLRuntimeStateGraph.init
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Configuration</em>' reference.
	 * @see #setConfiguration(Configuration)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeStateGraph_Configuration()
	 * @model
	 * @generated
	 */
	Configuration getConfiguration();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getConfiguration <em>Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Configuration</em>' reference.
	 * @see #getConfiguration()
	 * @generated
	 */
	void setConfiguration(Configuration value);

	/**
	 * Returns the value of the '<em><b>Element Container</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * {@link org.scenariotools.sml.runtime.ElementContainer} that contains objects shared by states of this stategraph instance.
	 * This reference should not be set by client code.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Element Container</em>' containment reference.
	 * @see #setElementContainer(ElementContainer)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeStateGraph_ElementContainer()
	 * @model containment="true"
	 * @generated
	 */
	ElementContainer getElementContainer();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getElementContainer <em>Element Container</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element Container</em>' containment reference.
	 * @see #getElementContainer()
	 * @generated
	 */
	void setElementContainer(ElementContainer value);

	/**
	 * Returns the value of the '<em><b>Parameter Ranges Provider</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * {@link org.scenariotools.sml.runtime.ParameterRangesProvider} instance that provides ranges for message event parameters.
	 * This reference should not be set by client code.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Parameter Ranges Provider</em>' containment reference.
	 * @see #setParameterRangesProvider(ParameterRangesProvider)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeStateGraph_ParameterRangesProvider()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	ParameterRangesProvider getParameterRangesProvider();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getParameterRangesProvider <em>Parameter Ranges Provider</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Ranges Provider</em>' containment reference.
	 * @see #getParameterRangesProvider()
	 * @generated
	 */
	void setParameterRangesProvider(ParameterRangesProvider value);

	/**
	 * Returns the value of the '<em><b>States</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.SMLRuntimeState}.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getStateGraph <em>State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>States</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeStateGraph_States()
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#getStateGraph
	 * @model opposite="stateGraph" containment="true"
	 * @generated
	 */
	EList<SMLRuntimeState> getStates();

	/**
	 * Returns the value of the '<em><b>Start State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start State</em>' reference.
	 * @see #setStartState(SMLRuntimeState)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeStateGraph_StartState()
	 * @model
	 * @generated
	 */
	SMLRuntimeState getStartState();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getStartState <em>Start State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start State</em>' reference.
	 * @see #getStartState()
	 * @generated
	 */
	void setStartState(SMLRuntimeState value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Resets the SMLRuntimeStateGraph instance such that it represents an empty (unexplored) statespace for the
	 * SML specification and object system referenced by the given runtime configuration.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	SMLRuntimeState init(Configuration config);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Returns a transition originating in the given runtime state. The transition will be labelled with the given message event.
	 * If the target of the transition is a state that has not been reached before, it will be added to this StateGraph instance.
	 * When the occurrence of the given event successfully terminates active existential scenarios, the transition
	 * is annotated with the corresponding scenarios.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	Transition generateSuccessor(SMLRuntimeState state, Event event);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Explores transitions for all message events that are {@link org.scenariotools.sml.runtime.SMLRuntimeState#getEnabledMessageEvents() enabled} in a runtime state.
	 * The successor states are added to this stategraph instance. 
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	EList<Transition> generateAllSuccessors(SMLRuntimeState state);

} // SMLRuntimeStateGraph
