/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.Specification;

import org.scenariotools.sml.runtime.configuration.Configuration;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SML Object System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.SMLObjectSystem#getEClassToEObject <em>EClass To EObject</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLObjectSystem#getStaticRoleBindings <em>Static Role Bindings</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLObjectSystem#getSpecification <em>Specification</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLObjectSystem#getRunconfiguration <em>Runconfiguration</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLObjectSystem#getMessageEventSideEffectsExecutor <em>Message Event Side Effects Executor</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLObjectSystem#getMessageEventIsIndependentEvaluators <em>Message Event Is Independent Evaluators</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLObjectSystem#getObjects <em>Objects</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLObjectSystem#getControllableObjects <em>Controllable Objects</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLObjectSystem#getUncontrollableObjects <em>Uncontrollable Objects</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLObjectSystem#getRootObjects <em>Root Objects</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLObjectSystem()
 * @model
 * @generated
 */
public interface SMLObjectSystem extends EObject {
	/**
	 * Returns the value of the '<em><b>EClass To EObject</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.ecore.EClass},
	 * and the value is of type {@link org.eclipse.emf.ecore.EObject},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EClass To EObject</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EClass To EObject</em>' map.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLObjectSystem_EClassToEObject()
	 * @model mapType="org.scenariotools.sml.runtime.EClassToEObjectMapEntry&lt;org.eclipse.emf.ecore.EClass, org.eclipse.emf.ecore.EObject&gt;" transient="true"
	 * @generated
	 */
	EMap<EClass, EObject> getEClassToEObject();

	/**
	 * Returns the value of the '<em><b>Static Role Bindings</b></em>' map.
	 * The key is of type {@link org.scenariotools.sml.Role},
	 * and the value is of type {@link org.eclipse.emf.ecore.EObject},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Static Role Bindings</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Static Role Bindings</em>' map.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLObjectSystem_StaticRoleBindings()
	 * @model mapType="org.scenariotools.sml.runtime.RoleToEObjectMapEntry&lt;org.scenariotools.sml.Role, org.eclipse.emf.ecore.EObject&gt;"
	 * @generated
	 */
	EMap<Role, EObject> getStaticRoleBindings();

	/**
	 * Returns the value of the '<em><b>Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specification</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specification</em>' reference.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLObjectSystem_Specification()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	Specification getSpecification();

	/**
	 * Returns the value of the '<em><b>Runconfiguration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Runconfiguration</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runconfiguration</em>' reference.
	 * @see #setRunconfiguration(Configuration)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLObjectSystem_Runconfiguration()
	 * @model
	 * @generated
	 */
	Configuration getRunconfiguration();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getRunconfiguration <em>Runconfiguration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Runconfiguration</em>' reference.
	 * @see #getRunconfiguration()
	 * @generated
	 */
	void setRunconfiguration(Configuration value);

	/**
	 * Returns the value of the '<em><b>Message Event Side Effects Executor</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Event Side Effects Executor</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Event Side Effects Executor</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLObjectSystem_MessageEventSideEffectsExecutor()
	 * @model transient="true"
	 * @generated
	 */
	EList<MessageEventSideEffectsExecutor> getMessageEventSideEffectsExecutor();

	/**
	 * Returns the value of the '<em><b>Message Event Is Independent Evaluators</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Event Is Independent Evaluators</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Event Is Independent Evaluators</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLObjectSystem_MessageEventIsIndependentEvaluators()
	 * @model transient="true"
	 * @generated
	 */
	EList<MessageEventIsIndependentEvaluator> getMessageEventIsIndependentEvaluators();

	/**
	 * Returns the value of the '<em><b>Objects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Objects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Objects</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLObjectSystem_Objects()
	 * @model
	 * @generated
	 */
	EList<EObject> getObjects();

	/**
	 * Returns the value of the '<em><b>Controllable Objects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controllable Objects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controllable Objects</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLObjectSystem_ControllableObjects()
	 * @model
	 * @generated
	 */
	EList<EObject> getControllableObjects();

	/**
	 * Returns the value of the '<em><b>Uncontrollable Objects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uncontrollable Objects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uncontrollable Objects</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLObjectSystem_UncontrollableObjects()
	 * @model
	 * @generated
	 */
	EList<EObject> getUncontrollableObjects();

	/**
	 * Returns the value of the '<em><b>Root Objects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Objects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Objects</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLObjectSystem_RootObjects()
	 * @model
	 * @generated
	 */
	EList<EObject> getRootObjects();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void init(Configuration runConfiguration, SMLRuntimeState smlRuntimeState);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void executeSideEffects(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean canExecuteSideEffects(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isIndependent(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isNonSpontaneousMessageEvent(MessageEvent messageEvent);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EList<Scenario> getScenariosForInitMessageEvent(MessageEvent initMessageEvent);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EList<MessageEvent> getInitializingEnvironmentMessageEvents(DynamicObjectContainer dynamicObjectProvider);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isControllable(EObject eObject);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isEnvironmentMessageEvent(Event event);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean containsEObject(EObject eObject);

} // SMLObjectSystem
