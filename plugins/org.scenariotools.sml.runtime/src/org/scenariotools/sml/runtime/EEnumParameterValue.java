/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EEnum Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.EEnumParameterValue#getEEnumParameterType <em>EEnum Parameter Type</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.EEnumParameterValue#getEEnumParameterValue <em>EEnum Parameter Value</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getEEnumParameterValue()
 * @model
 * @generated
 */
public interface EEnumParameterValue extends ParameterValue {
	/**
	 * Returns the value of the '<em><b>EEnum Parameter Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EEnum Parameter Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EEnum Parameter Type</em>' reference.
	 * @see #setEEnumParameterType(EEnum)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getEEnumParameterValue_EEnumParameterType()
	 * @model
	 * @generated
	 */
	EEnum getEEnumParameterType();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.EEnumParameterValue#getEEnumParameterType <em>EEnum Parameter Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EEnum Parameter Type</em>' reference.
	 * @see #getEEnumParameterType()
	 * @generated
	 */
	void setEEnumParameterType(EEnum value);

	/**
	 * Returns the value of the '<em><b>EEnum Parameter Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EEnum Parameter Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EEnum Parameter Value</em>' reference.
	 * @see #setEEnumParameterValue(EEnumLiteral)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getEEnumParameterValue_EEnumParameterValue()
	 * @model
	 * @generated
	 */
	EEnumLiteral getEEnumParameterValue();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.EEnumParameterValue#getEEnumParameterValue <em>EEnum Parameter Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EEnum Parameter Value</em>' reference.
	 * @see #getEEnumParameterValue()
	 * @generated
	 */
	void setEEnumParameterValue(EEnumLiteral value);

} // EEnumParameterValue
