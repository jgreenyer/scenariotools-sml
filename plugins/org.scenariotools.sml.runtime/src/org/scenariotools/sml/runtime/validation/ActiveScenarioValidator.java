/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.validation;

import org.eclipse.emf.common.util.EList;

import org.scenariotools.sml.runtime.MessageEvent;

import org.scenariotools.sml.Scenario;

import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;

/**
 * A sample validator interface for {@link org.scenariotools.sml.runtime.ActiveScenario}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface ActiveScenarioValidator {
	boolean validate();

	boolean validateScenario(Scenario value);
	boolean validateMainActiveInteraction(ActivePart value);
	boolean validateAlphabet(EList<MessageEvent> value);
	boolean validateRoleBindings(ActiveScenarioRoleBindings value);
	boolean validateInStrictCut(boolean value);
}
