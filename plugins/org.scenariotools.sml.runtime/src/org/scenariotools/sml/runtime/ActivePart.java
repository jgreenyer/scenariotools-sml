/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;
import org.scenariotools.sml.InteractionFragment;

import org.scenariotools.sml.expressions.scenarioExpressions.Variable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Part</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.ActivePart#getNestedActiveInteractions <em>Nested Active Interactions</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActivePart#getCoveredEvents <em>Covered Events</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActivePart#getForbiddenEvents <em>Forbidden Events</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActivePart#getInterruptingEvents <em>Interrupting Events</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActivePart#getConsideredEvents <em>Considered Events</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActivePart#getIgnoredEvents <em>Ignored Events</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActivePart#getEnabledEvents <em>Enabled Events</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActivePart#getParentActiveInteraction <em>Parent Active Interaction</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActivePart#getEnabledNestedActiveInteractions <em>Enabled Nested Active Interactions</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActivePart#getVariableMap <em>Variable Map</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActivePart#getEObjectVariableMap <em>EObject Variable Map</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActivePart#getInteractionFragment <em>Interaction Fragment</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActivePart()
 * @model
 * @generated
 */
public interface ActivePart extends Context {
	/**
	 * Returns the value of the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.ActivePart}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nested Active Interactions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nested Active Interactions</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActivePart_NestedActiveInteractions()
	 * @model containment="true"
	 * @generated
	 */
	EList<ActivePart> getNestedActiveInteractions();

	/**
	 * Returns the value of the '<em><b>Covered Events</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.MessageEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Covered Events</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Covered Events</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActivePart_CoveredEvents()
	 * @model transient="true"
	 * @generated
	 */
	EList<MessageEvent> getCoveredEvents();

	/**
	 * Returns the value of the '<em><b>Forbidden Events</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.MessageEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Forbidden Events</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Forbidden Events</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActivePart_ForbiddenEvents()
	 * @model transient="true"
	 * @generated
	 */
	EList<MessageEvent> getForbiddenEvents();

	/**
	 * Returns the value of the '<em><b>Interrupting Events</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.MessageEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupting Events</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupting Events</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActivePart_InterruptingEvents()
	 * @model transient="true"
	 * @generated
	 */
	EList<MessageEvent> getInterruptingEvents();

	/**
	 * Returns the value of the '<em><b>Considered Events</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.MessageEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Considered Events</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Considered Events</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActivePart_ConsideredEvents()
	 * @model
	 * @generated
	 */
	EList<MessageEvent> getConsideredEvents();

	/**
	 * Returns the value of the '<em><b>Ignored Events</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.MessageEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ignored Events</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ignored Events</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActivePart_IgnoredEvents()
	 * @model
	 * @generated
	 */
	EList<MessageEvent> getIgnoredEvents();

	/**
	 * Returns the value of the '<em><b>Enabled Events</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.MessageEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enabled Events</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enabled Events</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActivePart_EnabledEvents()
	 * @model transient="true"
	 * @generated
	 */
	EList<MessageEvent> getEnabledEvents();

	/**
	 * Returns the value of the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Active Interaction</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Active Interaction</em>' reference.
	 * @see #setParentActiveInteraction(ActivePart)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActivePart_ParentActiveInteraction()
	 * @model
	 * @generated
	 */
	ActivePart getParentActiveInteraction();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.ActivePart#getParentActiveInteraction <em>Parent Active Interaction</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Active Interaction</em>' reference.
	 * @see #getParentActiveInteraction()
	 * @generated
	 */
	void setParentActiveInteraction(ActivePart value);

	/**
	 * Returns the value of the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.ActivePart}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enabled Nested Active Interactions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enabled Nested Active Interactions</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActivePart_EnabledNestedActiveInteractions()
	 * @model
	 * @generated
	 */
	EList<ActivePart> getEnabledNestedActiveInteractions();

	/**
	 * Returns the value of the '<em><b>Variable Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.sml.expressions.scenarioExpressions.Variable},
	 * and the value is of type {@link java.lang.Object},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable Map</em>' map.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActivePart_VariableMap()
	 * @model mapType="org.scenariotools.sml.runtime.VariableToObjectMapEntry&lt;org.scenariotools.sml.expressions.scenarioExpressions.Variable, org.eclipse.emf.ecore.EJavaObject&gt;"
	 * @generated
	 */
	EMap<Variable, Object> getVariableMap();

	/**
	 * Returns the value of the '<em><b>EObject Variable Map</b></em>' map.
	 * The key is of type {@link org.scenariotools.sml.expressions.scenarioExpressions.Variable},
	 * and the value is of type {@link org.eclipse.emf.ecore.EObject},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EObject Variable Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EObject Variable Map</em>' map.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActivePart_EObjectVariableMap()
	 * @model mapType="org.scenariotools.sml.runtime.VariableToEObjectMapEntry&lt;org.scenariotools.sml.expressions.scenarioExpressions.Variable, org.eclipse.emf.ecore.EObject&gt;"
	 * @generated
	 */
	EMap<Variable, EObject> getEObjectVariableMap();

	/**
	 * Returns the value of the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interaction Fragment</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interaction Fragment</em>' reference.
	 * @see #setInteractionFragment(InteractionFragment)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActivePart_InteractionFragment()
	 * @model
	 * @generated
	 */
	InteractionFragment getInteractionFragment();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.ActivePart#getInteractionFragment <em>Interaction Fragment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interaction Fragment</em>' reference.
	 * @see #getInteractionFragment()
	 * @generated
	 */
	void setInteractionFragment(InteractionFragment value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The method can progress an enabled event, lead to a cold or safety violation, or can have no effect.
	 * The method is called recursively for nested ActiveInteractions.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	ActiveScenarioProgress performStep(MessageEvent event, ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The method can progress an enabled event, lead to a cold or safety violation, or can have no effect.
	 * The method is called recursively for nested ActiveInteractions.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	ActiveScenarioProgress postPerformStep(MessageEvent event, ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void init(ActiveScenarioRoleBindings roleBindings, ActivePart parentActivePart, ActiveScenario activeScenario);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The method is called recursively for parent Interactions to see whether a message event is cold or safety violating or ignored by a parent Interaction.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	ViolationKind isViolatingInInteraction(MessageEvent event, boolean isInStrictCut);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void updateMessageEvents(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<MessageEvent> getRequestedEvents();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	BlockedType isBlocked(MessageEvent messageEvent, boolean isInStrictCut);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	ActiveScenarioProgress enable(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isInRequestedState();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isInStrictState();

} // ActivePart
