/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.BooleanParameterValue#isBooleanParameterValue <em>Boolean Parameter Value</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getBooleanParameterValue()
 * @model
 * @generated
 */
public interface BooleanParameterValue extends ParameterValue {
	/**
	 * Returns the value of the '<em><b>Boolean Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Boolean Parameter Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Boolean Parameter Value</em>' attribute.
	 * @see #isSetBooleanParameterValue()
	 * @see #unsetBooleanParameterValue()
	 * @see #setBooleanParameterValue(boolean)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getBooleanParameterValue_BooleanParameterValue()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isBooleanParameterValue();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.BooleanParameterValue#isBooleanParameterValue <em>Boolean Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Boolean Parameter Value</em>' attribute.
	 * @see #isSetBooleanParameterValue()
	 * @see #unsetBooleanParameterValue()
	 * @see #isBooleanParameterValue()
	 * @generated
	 */
	void setBooleanParameterValue(boolean value);

	/**
	 * Unsets the value of the '{@link org.scenariotools.sml.runtime.BooleanParameterValue#isBooleanParameterValue <em>Boolean Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBooleanParameterValue()
	 * @see #isBooleanParameterValue()
	 * @see #setBooleanParameterValue(boolean)
	 * @generated
	 */
	void unsetBooleanParameterValue();

	/**
	 * Returns whether the value of the '{@link org.scenariotools.sml.runtime.BooleanParameterValue#isBooleanParameterValue <em>Boolean Parameter Value</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Boolean Parameter Value</em>' attribute is set.
	 * @see #unsetBooleanParameterValue()
	 * @see #isBooleanParameterValue()
	 * @see #setBooleanParameterValue(boolean)
	 * @generated
	 */
	boolean isSetBooleanParameterValue();

} // BooleanParameterValue
