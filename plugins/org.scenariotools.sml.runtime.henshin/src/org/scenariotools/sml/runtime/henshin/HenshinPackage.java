/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.henshin;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EReference;
import org.scenariotools.sml.runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.scenariotools.sml.runtime.henshin.HenshinFactory
 * @model kind="package"
 * @generated
 */
public interface HenshinPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "henshin";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.scenariotools.org/sml/runtime/henshin/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "henshin";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	HenshinPackage eINSTANCE = org.scenariotools.sml.runtime.henshin.impl.HenshinPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.henshin.impl.HenshinMessageEventsSideEffectsExecutorImpl <em>Message Events Side Effects Executor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.henshin.impl.HenshinMessageEventsSideEffectsExecutorImpl
	 * @see org.scenariotools.sml.runtime.henshin.impl.HenshinPackageImpl#getHenshinMessageEventsSideEffectsExecutor()
	 * @generated
	 */
	int HENSHIN_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR = 0;

	/**
	 * The feature id for the '<em><b>Henshin Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HENSHIN_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR__HENSHIN_MODULE = RuntimePackage.MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Message Events Side Effects Executor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HENSHIN_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR_FEATURE_COUNT = RuntimePackage.MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Execute Side Effects</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HENSHIN_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR___EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = RuntimePackage.MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR___EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER;

	/**
	 * The operation id for the '<em>Can Execute Side Effects</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HENSHIN_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR___CAN_EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = RuntimePackage.MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR___CAN_EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HENSHIN_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR___INIT__CONFIGURATION = RuntimePackage.MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR___INIT__CONFIGURATION;

	/**
	 * The number of operations of the '<em>Message Events Side Effects Executor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HENSHIN_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR_OPERATION_COUNT = RuntimePackage.MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.henshin.impl.HenshinMessageEventIsIndependentEvaluatorImpl <em>Message Event Is Independent Evaluator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.henshin.impl.HenshinMessageEventIsIndependentEvaluatorImpl
	 * @see org.scenariotools.sml.runtime.henshin.impl.HenshinPackageImpl#getHenshinMessageEventIsIndependentEvaluator()
	 * @generated
	 */
	int HENSHIN_MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR = 1;

	/**
	 * The feature id for the '<em><b>Henshin Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HENSHIN_MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR__HENSHIN_MODULE = RuntimePackage.MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Message Event Is Independent Evaluator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HENSHIN_MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR_FEATURE_COUNT = RuntimePackage.MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Independent</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HENSHIN_MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR___IS_INDEPENDENT__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = RuntimePackage.MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR___IS_INDEPENDENT__MESSAGEEVENT_DYNAMICOBJECTCONTAINER;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HENSHIN_MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR___INIT__CONFIGURATION = RuntimePackage.MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR___INIT__CONFIGURATION;

	/**
	 * The number of operations of the '<em>Message Event Is Independent Evaluator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HENSHIN_MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR_OPERATION_COUNT = RuntimePackage.MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.henshin.HenshinMessageEventsSideEffectsExecutor <em>Message Events Side Effects Executor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Events Side Effects Executor</em>'.
	 * @see org.scenariotools.sml.runtime.henshin.HenshinMessageEventsSideEffectsExecutor
	 * @generated
	 */
	EClass getHenshinMessageEventsSideEffectsExecutor();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.henshin.HenshinMessageEventsSideEffectsExecutor#getHenshinModule <em>Henshin Module</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Henshin Module</em>'.
	 * @see org.scenariotools.sml.runtime.henshin.HenshinMessageEventsSideEffectsExecutor#getHenshinModule()
	 * @see #getHenshinMessageEventsSideEffectsExecutor()
	 * @generated
	 */
	EReference getHenshinMessageEventsSideEffectsExecutor_HenshinModule();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.henshin.HenshinMessageEventIsIndependentEvaluator <em>Message Event Is Independent Evaluator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Event Is Independent Evaluator</em>'.
	 * @see org.scenariotools.sml.runtime.henshin.HenshinMessageEventIsIndependentEvaluator
	 * @generated
	 */
	EClass getHenshinMessageEventIsIndependentEvaluator();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.henshin.HenshinMessageEventIsIndependentEvaluator#getHenshinModule <em>Henshin Module</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Henshin Module</em>'.
	 * @see org.scenariotools.sml.runtime.henshin.HenshinMessageEventIsIndependentEvaluator#getHenshinModule()
	 * @see #getHenshinMessageEventIsIndependentEvaluator()
	 * @generated
	 */
	EReference getHenshinMessageEventIsIndependentEvaluator_HenshinModule();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	HenshinFactory getHenshinFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.henshin.impl.HenshinMessageEventsSideEffectsExecutorImpl <em>Message Events Side Effects Executor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.henshin.impl.HenshinMessageEventsSideEffectsExecutorImpl
		 * @see org.scenariotools.sml.runtime.henshin.impl.HenshinPackageImpl#getHenshinMessageEventsSideEffectsExecutor()
		 * @generated
		 */
		EClass HENSHIN_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR = eINSTANCE.getHenshinMessageEventsSideEffectsExecutor();
		/**
		 * The meta object literal for the '<em><b>Henshin Module</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HENSHIN_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR__HENSHIN_MODULE = eINSTANCE.getHenshinMessageEventsSideEffectsExecutor_HenshinModule();
		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.henshin.impl.HenshinMessageEventIsIndependentEvaluatorImpl <em>Message Event Is Independent Evaluator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.henshin.impl.HenshinMessageEventIsIndependentEvaluatorImpl
		 * @see org.scenariotools.sml.runtime.henshin.impl.HenshinPackageImpl#getHenshinMessageEventIsIndependentEvaluator()
		 * @generated
		 */
		EClass HENSHIN_MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR = eINSTANCE.getHenshinMessageEventIsIndependentEvaluator();
		/**
		 * The meta object literal for the '<em><b>Henshin Module</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HENSHIN_MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR__HENSHIN_MODULE = eINSTANCE.getHenshinMessageEventIsIndependentEvaluator_HenshinModule();

	}

} //HenshinPackage
