/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.henshin.logic;

import java.util.Iterator;

import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.henshin.interpreter.EGraph;
import org.eclipse.emf.henshin.interpreter.Engine;
import org.eclipse.emf.henshin.interpreter.InterpreterFactory;
import org.eclipse.emf.henshin.interpreter.Match;
import org.eclipse.emf.henshin.interpreter.RuleApplication;
import org.eclipse.emf.henshin.interpreter.impl.MatchImpl;
import org.eclipse.emf.henshin.interpreter.impl.RuleApplicationImpl;
import org.eclipse.emf.henshin.model.HenshinFactory;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.Unit;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.ParameterValue;
import org.scenariotools.sml.runtime.DynamicObjectContainer;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.runtime.henshin.HenshinMessageEventsSideEffectsExecutor;
import org.scenariotools.sml.runtime.impl.MessageEventSideEffectsExecutorImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Message Events Side Effects Executor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public abstract class HenshinMessageEventsSideEffectsExecutorLogic extends MessageEventSideEffectsExecutorImpl implements HenshinMessageEventsSideEffectsExecutor {

	
	
	@Override
	public void init(Configuration runConfig) {

		try {
			// find a henshin model in the same folder as the runconfig
			ResourceSet resourceSet = runConfig.eResource().getResourceSet();
			Module module = (Module)resourceSet.getResource(runConfig.eResource().getURI().trimFileExtension().appendFileExtension("henshin"), true).getContents().get(0);
			setHenshinModule(module);
		} catch (Exception e) {
			try {
				// otherwise find a henshin model in the same folder as the specification 
				ResourceSet resourceSet = runConfig.getSpecification().eResource().getResourceSet();
				Module module = (Module)resourceSet.getResource(runConfig.getSpecification().eResource().getURI().trimFileExtension().appendFileExtension("henshin"), true).getContents().get(0);
				setHenshinModule(module);
			} catch (Exception e2) {
				//otherwise, no henshin module can be found
				//set dummy module to not have to test for NULL all the time.
				setHenshinModule(HenshinFactory.eINSTANCE.createModule());
			}
		}
		
//		System.out.println(getHenshinModule().getUnits());

		engine = InterpreterFactory.INSTANCE.createEngine();
		
//		engine.getOptions().put(Engine.OPTION_SORT_VARIABLES, false);

		
	}
	
	private Engine engine;

	
	@Override
	public boolean canExecuteSideEffects(MessageEvent messageEvent,
			DynamicObjectContainer dynamicObjectContainer) {

//		System.out.println("canExecuteSideEffects: for MessageEvent? " + messageEvent);

		
		ETypedElement typedElement = messageEvent.getTypedElement();
		
		if (typedElement instanceof EOperation
				|| typedElement instanceof EStructuralFeature){
			
			String ruleName = typedElement.getName();
			if (typedElement instanceof EStructuralFeature) {
				ruleName = "set" + ruleName;
			}
			
			for (Unit unit : getHenshinModule().getUnits()) {
				if (unit instanceof Rule 
						&& unit.getName().equals(ruleName)){
					Rule rule = (Rule) unit;
					Match match = new MatchImpl(rule);
					
					if (rule.getLhs().getNode("sender") != null)
						match.setNodeTarget(rule.getLhs().getNode("sender"), dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(messageEvent.getSendingObject()));
					if (rule.getLhs().getNode("receiver") != null)
						match.setNodeTarget(rule.getLhs().getNode("receiver"), dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(messageEvent.getReceivingObject()));
					
					
					for (ParameterValue parameterValue : messageEvent.getParameterValues()) {
//						System.out.println("param name:   " + rule.getLhs().getNode(parameterValue.getEParameter().getName()));
//						System.out.println("param value1: " + parameterValue.getValue());
//						System.out.println("param value2: " + dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(parameterValue.getValue()));
						
						if(parameterValue.getValue() == null)
							continue;
						
						if (parameterValue.getValue() instanceof EObject){
							if (parameterValue.getEParameter() != null) {
								match.setNodeTarget(
										rule.getLhs().getNode(parameterValue.getEParameter().getName()),
										dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(parameterValue.getValue())
										);
							}else if (parameterValue.getStrucFeatureOrEOp() != null) {
								match.setNodeTarget(
										rule.getLhs().getNode(parameterValue.getStrucFeatureOrEOp().getName()),
										dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(parameterValue.getValue())
										);
							} 
						}else{
							// TODO test this.
							match.setParameterValue(
									rule.getParameter(parameterValue.getEParameter().getName()),
									parameterValue.getValue()
									);
						}
					}
					
					
					EGraph graph = InterpreterFactory.INSTANCE.createEGraph();
					
					for (EObject eObject : dynamicObjectContainer.getRootObjects()) {
						graph.addGraph(eObject);
					}
								
//					RuleApplication application = new RuleApplicationImpl(engine,graph,rule,match);
					
					Iterator<Match> matchesIterator = engine.findMatches(rule, graph, match).iterator();
					
					if (!matchesIterator.hasNext()){
//						System.out.println("canExecuteSideEffects: no match found, returning false");
						return false;
					}else{
						//application.undo(null);
//						System.out.println("canExecuteSideEffects: match found, returning true");
						return true;
					}
				}
			}
			
		}
			
//		System.out.println("canExecuteSideEffects: no rule found, returning TRUE");
		return true;
	}
	
	
	@Override
	public void executeSideEffects(MessageEvent messageEvent,
			DynamicObjectContainer dynamicObjectContainer) {

//		System.out.println("executeSideEffects: for MessageEvent? " + messageEvent);
		
		ETypedElement typedElement = messageEvent.getTypedElement();

		if (typedElement instanceof EOperation
				|| typedElement instanceof EStructuralFeature){
			
			String ruleName = typedElement.getName();
			if (typedElement instanceof EStructuralFeature) {
				ruleName = "set" + ruleName;
			}

			for (Unit unit : getHenshinModule().getUnits()) {
				if (unit instanceof Rule 
						&& unit.getName().equals(ruleName)){
					Rule rule = (Rule) unit;
					Match match = new MatchImpl(rule);
					if (rule.getLhs().getNode("sender") != null)
						match.setNodeTarget(rule.getLhs().getNode("sender"), dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(messageEvent.getSendingObject()));
					if (rule.getLhs().getNode("receiver") != null)
						match.setNodeTarget(rule.getLhs().getNode("receiver"), dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(messageEvent.getReceivingObject()));
					
					
					
					for (ParameterValue parameterValue : messageEvent.getParameterValues()) {
//						System.out.println("param name:   " + rule.getLhs().getNode(parameterValue.getEParameter().getName()));
//						System.out.println("param value1: " + parameterValue.getValue());
//						System.out.println("param value2: " + dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(parameterValue.getValue()));
						
						if (parameterValue.getValue() instanceof EObject){
							if (parameterValue.getEParameter() != null) {
								match.setNodeTarget(
										rule.getLhs().getNode(parameterValue.getEParameter().getName()),
										dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(parameterValue.getValue())
										);
							}else if (parameterValue.getStrucFeatureOrEOp() != null) {
								match.setNodeTarget(
										rule.getLhs().getNode(parameterValue.getStrucFeatureOrEOp().getName()),
										dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(parameterValue.getValue())
										);
							} 
						}else{
							// TODO test this.
							match.setParameterValue(
									rule.getParameter(parameterValue.getEParameter().getName()),
									parameterValue.getValue()
									);
						}
					}

					EGraph graph = InterpreterFactory.INSTANCE.createEGraph();

					
					for (EObject eObject : dynamicObjectContainer.getRootObjects()) {
						graph.addGraph(eObject);
					}
								
					RuleApplication application = new RuleApplicationImpl(engine,graph,rule,match);
					
					if (!application.execute(null)){
						;
//						System.out.println("Could not apply rule");
					}

				}
			}
		}


	}

	
} //HenshinMessageEventsSideEffectsExecutorImpl
