/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.henshin.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.henshin.model.Module;
import org.scenariotools.sml.runtime.henshin.HenshinPackage;
import org.scenariotools.sml.runtime.henshin.logic.HenshinMessageEventIsIndependentEvaluatorLogic;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Message Event Is Independent Evaluator</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated PROTECT_DECLARATION
 */
public class HenshinMessageEventIsIndependentEvaluatorImpl extends HenshinMessageEventIsIndependentEvaluatorLogic {
	/**
	 * The cached value of the '{@link #getHenshinModule() <em>Henshin Module</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHenshinModule()
	 * @generated
	 * @ordered
	 */
	protected Module henshinModule;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HenshinMessageEventIsIndependentEvaluatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HenshinPackage.Literals.HENSHIN_MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Module getHenshinModule() {
		if (henshinModule != null && henshinModule.eIsProxy()) {
			InternalEObject oldHenshinModule = (InternalEObject)henshinModule;
			henshinModule = (Module)eResolveProxy(oldHenshinModule);
			if (henshinModule != oldHenshinModule) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, HenshinPackage.HENSHIN_MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR__HENSHIN_MODULE, oldHenshinModule, henshinModule));
			}
		}
		return henshinModule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Module basicGetHenshinModule() {
		return henshinModule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHenshinModule(Module newHenshinModule) {
		Module oldHenshinModule = henshinModule;
		henshinModule = newHenshinModule;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HenshinPackage.HENSHIN_MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR__HENSHIN_MODULE, oldHenshinModule, henshinModule));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HenshinPackage.HENSHIN_MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR__HENSHIN_MODULE:
				if (resolve) return getHenshinModule();
				return basicGetHenshinModule();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HenshinPackage.HENSHIN_MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR__HENSHIN_MODULE:
				setHenshinModule((Module)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HenshinPackage.HENSHIN_MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR__HENSHIN_MODULE:
				setHenshinModule((Module)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HenshinPackage.HENSHIN_MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR__HENSHIN_MODULE:
				return henshinModule != null;
		}
		return super.eIsSet(featureID);
	}

} //HenshinMessageEventIsIndependentEvaluatorImpl
