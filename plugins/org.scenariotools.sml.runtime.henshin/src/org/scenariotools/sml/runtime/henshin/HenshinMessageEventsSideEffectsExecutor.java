/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.henshin;

import org.eclipse.emf.henshin.model.Module;
import org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message Events Side Effects Executor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.henshin.HenshinMessageEventsSideEffectsExecutor#getHenshinModule <em>Henshin Module</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.henshin.HenshinPackage#getHenshinMessageEventsSideEffectsExecutor()
 * @model
 * @generated
 */
public interface HenshinMessageEventsSideEffectsExecutor extends MessageEventSideEffectsExecutor {

	/**
	 * Returns the value of the '<em><b>Henshin Module</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Henshin Module</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Henshin Module</em>' reference.
	 * @see #setHenshinModule(Module)
	 * @see org.scenariotools.sml.runtime.henshin.HenshinPackage#getHenshinMessageEventsSideEffectsExecutor_HenshinModule()
	 * @model
	 * @generated
	 */
	Module getHenshinModule();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.henshin.HenshinMessageEventsSideEffectsExecutor#getHenshinModule <em>Henshin Module</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Henshin Module</em>' reference.
	 * @see #getHenshinModule()
	 * @generated
	 */
	void setHenshinModule(Module value);
} // HenshinMessageEventsSideEffectsExecutor
