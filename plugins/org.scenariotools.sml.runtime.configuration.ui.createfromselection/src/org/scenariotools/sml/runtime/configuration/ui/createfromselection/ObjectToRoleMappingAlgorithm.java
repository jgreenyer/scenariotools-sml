/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.configuration.ui.createfromselection;

import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.sml.Collaboration;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.runtime.configuration.ConfigurationFactory;
import org.scenariotools.sml.runtime.configuration.RoleAssignment;
import org.scenariotools.sml.runtime.configuration.RoleBindings;

final class ObjectToRoleMappingAlgorithm {

	final static RoleBindings createRoleBindingsForCollaboration(Set<EObject> objects, Collaboration c)
			throws NoObjectForRoleException {
		RoleBindings result = ConfigurationFactory.eINSTANCE.createRoleBindings();
		result.setCollaboration(c);
		for (Role r : c.getRoles()) {
			if (r.isStatic()) {
				EObject object = findObjectForRole(objects, r);
				if (object == null) {
					throw new NoObjectForRoleException(r);
				}
				result.getBindings().add(createAssignment(r, object));
			}
		}
		if (result.getBindings().size() == 0)
			result = null;
		return result;
	}

	private static RoleAssignment createAssignment(Role r, EObject object) {
		RoleAssignment a = ConfigurationFactory.eINSTANCE.createRoleAssignment();
		a.setRole(r);
		a.setObject(object);
		return a;
	}
	/**
	 * 
	 * @param objects Set of objects that may play the given role 
	 * @param r
	 * @return <code>null</code> if no object could  be found, the best matching object for the role otherwise.
	 */
	private static EObject findObjectForRole(Set<EObject> objects, Role r) {
		EObject object = null;
		int rating = Integer.MAX_VALUE;
		for (EObject o : objects) {
			if (((EClass) r.getType()).isSuperTypeOf(o.eClass())) {
				int newRating = rate(o, r);
				if (newRating < rating) {
					rating = newRating;
					object = o;
				}
			}
		}
		return object;
	}
	/**
	 * Rates how well an object fits a role. Lower scores are better.
	 * @param o
	 * @param r
	 * @return 0, if role's and object's names are equal, edit distance otherwise.
	 */
	private static int rate(EObject o, Role r) {
		String objectName = (String) o.eGet(o.eClass().getEStructuralFeature("name"));
		String roleName = r.getName();
		if (roleName.equalsIgnoreCase(objectName))
			return 0;
		else {
			return distance(objectName, roleName);
		}
	}

	private static int distance(String s1, String s2) {
		s1 = s1.toLowerCase();
		s2 = s2.toLowerCase();
		final int l1 = s1.length(), l2 = s2.length();
		final char[] c1 = s1.toCharArray(), c2 = s2.toCharArray();
		int[][] d = new int[l1 + 1][l2 + 1];
		for (int i = 0; i <= l1; i++) {
			d[i][0] = i;
		}
		for (int j = 0; j <= l2; j++) {
			d[0][j] = j;
		}
		for (int j = 1; j <= l2; j++) {
			for (int i = 1; i <= l1; i++) {
				if (c1[i - 1] == c2[j - 1])
					d[i][j] = d[i - 1][j - 1];
				else
					d[i][j] = Math.min(Math.min(d[i - 1][j] + 1, d[i][j - 1] + 1), d[i - 1][j - 1] + 1);
			}
		}
		return d[l1][l2];
	}

}
