/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.configuration.ui.createfromselection;

import org.scenariotools.sml.Role;

public class NoObjectForRoleException extends Exception {
	
	private static final long serialVersionUID = 3466255627676907438L;
	final Role role;
	public NoObjectForRoleException(Role r) {
		role = r;
	}
	public Role getRole() {
		return role;
	}

}
