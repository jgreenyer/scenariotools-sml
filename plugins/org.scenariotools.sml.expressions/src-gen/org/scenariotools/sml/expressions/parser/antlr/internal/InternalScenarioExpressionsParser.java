package org.scenariotools.sml.expressions.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.scenariotools.sml.expressions.services.ScenarioExpressionsGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
@SuppressWarnings("all")
public class InternalScenarioExpressionsParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_SIGNEDINT", "RULE_BOOL", "RULE_DOUBLE", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'domain'", "'import'", "'{'", "';'", "'}'", "'='", "'var'", "'clock'", "'reset clock'", "'=>'", "'||'", "'&&'", "'=='", "'!='", "'<'", "'>'", "'<='", "'>='", "'+'", "'-'", "'*'", "'/'", "'!'", "'('", "')'", "':'", "'null'", "'.'", "','", "'any'", "'contains'", "'containsAll'", "'first'", "'get'", "'isEmpty'", "'last'", "'size'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__14=14;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=10;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_SIGNEDINT=7;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=11;
    public static final int T__37=37;
    public static final int RULE_DOUBLE=9;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=12;
    public static final int RULE_ANY_OTHER=13;
    public static final int RULE_BOOL=8;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalScenarioExpressionsParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalScenarioExpressionsParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalScenarioExpressionsParser.tokenNames; }
    public String getGrammarFileName() { return "InternalScenarioExpressions.g"; }



     	private ScenarioExpressionsGrammarAccess grammarAccess;
     	
        public InternalScenarioExpressionsParser(TokenStream input, ScenarioExpressionsGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Document";	
       	}
       	
       	@Override
       	protected ScenarioExpressionsGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleDocument"
    // InternalScenarioExpressions.g:77:1: entryRuleDocument returns [EObject current=null] : iv_ruleDocument= ruleDocument EOF ;
    public final EObject entryRuleDocument() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDocument = null;


        try {
            // InternalScenarioExpressions.g:78:2: (iv_ruleDocument= ruleDocument EOF )
            // InternalScenarioExpressions.g:79:2: iv_ruleDocument= ruleDocument EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDocumentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleDocument=ruleDocument();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDocument; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDocument"


    // $ANTLR start "ruleDocument"
    // InternalScenarioExpressions.g:86:1: ruleDocument returns [EObject current=null] : ( ( (lv_imports_0_0= ruleImport ) )* (otherlv_1= 'domain' ( (otherlv_2= RULE_ID ) ) )* ( (lv_expressions_3_0= ruleExpressionRegion ) )* ) ;
    public final EObject ruleDocument() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_imports_0_0 = null;

        EObject lv_expressions_3_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:89:28: ( ( ( (lv_imports_0_0= ruleImport ) )* (otherlv_1= 'domain' ( (otherlv_2= RULE_ID ) ) )* ( (lv_expressions_3_0= ruleExpressionRegion ) )* ) )
            // InternalScenarioExpressions.g:90:1: ( ( (lv_imports_0_0= ruleImport ) )* (otherlv_1= 'domain' ( (otherlv_2= RULE_ID ) ) )* ( (lv_expressions_3_0= ruleExpressionRegion ) )* )
            {
            // InternalScenarioExpressions.g:90:1: ( ( (lv_imports_0_0= ruleImport ) )* (otherlv_1= 'domain' ( (otherlv_2= RULE_ID ) ) )* ( (lv_expressions_3_0= ruleExpressionRegion ) )* )
            // InternalScenarioExpressions.g:90:2: ( (lv_imports_0_0= ruleImport ) )* (otherlv_1= 'domain' ( (otherlv_2= RULE_ID ) ) )* ( (lv_expressions_3_0= ruleExpressionRegion ) )*
            {
            // InternalScenarioExpressions.g:90:2: ( (lv_imports_0_0= ruleImport ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==15) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalScenarioExpressions.g:91:1: (lv_imports_0_0= ruleImport )
            	    {
            	    // InternalScenarioExpressions.g:91:1: (lv_imports_0_0= ruleImport )
            	    // InternalScenarioExpressions.g:92:3: lv_imports_0_0= ruleImport
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getDocumentAccess().getImportsImportParserRuleCall_0_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_3);
            	    lv_imports_0_0=ruleImport();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getDocumentRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"imports",
            	              		lv_imports_0_0, 
            	              		"org.scenariotools.sml.expressions.ScenarioExpressions.Import");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalScenarioExpressions.g:108:3: (otherlv_1= 'domain' ( (otherlv_2= RULE_ID ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==14) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalScenarioExpressions.g:108:5: otherlv_1= 'domain' ( (otherlv_2= RULE_ID ) )
            	    {
            	    otherlv_1=(Token)match(input,14,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_1, grammarAccess.getDocumentAccess().getDomainKeyword_1_0());
            	          
            	    }
            	    // InternalScenarioExpressions.g:112:1: ( (otherlv_2= RULE_ID ) )
            	    // InternalScenarioExpressions.g:113:1: (otherlv_2= RULE_ID )
            	    {
            	    // InternalScenarioExpressions.g:113:1: (otherlv_2= RULE_ID )
            	    // InternalScenarioExpressions.g:114:3: otherlv_2= RULE_ID
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getDocumentRule());
            	      	        }
            	              
            	    }
            	    otherlv_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_5); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		newLeafNode(otherlv_2, grammarAccess.getDocumentAccess().getDomainsEPackageCrossReference_1_1_0()); 
            	      	
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // InternalScenarioExpressions.g:125:4: ( (lv_expressions_3_0= ruleExpressionRegion ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==16) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalScenarioExpressions.g:126:1: (lv_expressions_3_0= ruleExpressionRegion )
            	    {
            	    // InternalScenarioExpressions.g:126:1: (lv_expressions_3_0= ruleExpressionRegion )
            	    // InternalScenarioExpressions.g:127:3: lv_expressions_3_0= ruleExpressionRegion
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getDocumentAccess().getExpressionsExpressionRegionParserRuleCall_2_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_6);
            	    lv_expressions_3_0=ruleExpressionRegion();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getDocumentRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"expressions",
            	              		lv_expressions_3_0, 
            	              		"org.scenariotools.sml.expressions.ScenarioExpressions.ExpressionRegion");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDocument"


    // $ANTLR start "entryRuleImport"
    // InternalScenarioExpressions.g:151:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalScenarioExpressions.g:152:2: (iv_ruleImport= ruleImport EOF )
            // InternalScenarioExpressions.g:153:2: iv_ruleImport= ruleImport EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getImportRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleImport; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalScenarioExpressions.g:160:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_importURI_1_0=null;

         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:163:28: ( (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) )
            // InternalScenarioExpressions.g:164:1: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            {
            // InternalScenarioExpressions.g:164:1: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            // InternalScenarioExpressions.g:164:3: otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,15,FollowSets000.FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
                  
            }
            // InternalScenarioExpressions.g:168:1: ( (lv_importURI_1_0= RULE_STRING ) )
            // InternalScenarioExpressions.g:169:1: (lv_importURI_1_0= RULE_STRING )
            {
            // InternalScenarioExpressions.g:169:1: (lv_importURI_1_0= RULE_STRING )
            // InternalScenarioExpressions.g:170:3: lv_importURI_1_0= RULE_STRING
            {
            lv_importURI_1_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_importURI_1_0, grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getImportRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"importURI",
                      		lv_importURI_1_0, 
                      		"org.eclipse.xtext.common.Terminals.STRING");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleExpressionRegion"
    // InternalScenarioExpressions.g:194:1: entryRuleExpressionRegion returns [EObject current=null] : iv_ruleExpressionRegion= ruleExpressionRegion EOF ;
    public final EObject entryRuleExpressionRegion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionRegion = null;


        try {
            // InternalScenarioExpressions.g:195:2: (iv_ruleExpressionRegion= ruleExpressionRegion EOF )
            // InternalScenarioExpressions.g:196:2: iv_ruleExpressionRegion= ruleExpressionRegion EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionRegionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpressionRegion=ruleExpressionRegion();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionRegion; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionRegion"


    // $ANTLR start "ruleExpressionRegion"
    // InternalScenarioExpressions.g:203:1: ruleExpressionRegion returns [EObject current=null] : ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' ) ;
    public final EObject ruleExpressionRegion() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject lv_expressions_2_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:206:28: ( ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' ) )
            // InternalScenarioExpressions.g:207:1: ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' )
            {
            // InternalScenarioExpressions.g:207:1: ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' )
            // InternalScenarioExpressions.g:207:2: () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}'
            {
            // InternalScenarioExpressions.g:207:2: ()
            // InternalScenarioExpressions.g:208:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getExpressionRegionAccess().getExpressionRegionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,16,FollowSets000.FOLLOW_8); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getExpressionRegionAccess().getLeftCurlyBracketKeyword_1());
                  
            }
            // InternalScenarioExpressions.g:217:1: ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>=RULE_ID && LA4_0<=RULE_BOOL)||LA4_0==16||(LA4_0>=20 && LA4_0<=22)||LA4_0==33||(LA4_0>=36 && LA4_0<=37)||LA4_0==40) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalScenarioExpressions.g:217:2: ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';'
            	    {
            	    // InternalScenarioExpressions.g:217:2: ( (lv_expressions_2_0= ruleExpressionOrRegion ) )
            	    // InternalScenarioExpressions.g:218:1: (lv_expressions_2_0= ruleExpressionOrRegion )
            	    {
            	    // InternalScenarioExpressions.g:218:1: (lv_expressions_2_0= ruleExpressionOrRegion )
            	    // InternalScenarioExpressions.g:219:3: lv_expressions_2_0= ruleExpressionOrRegion
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getExpressionRegionAccess().getExpressionsExpressionOrRegionParserRuleCall_2_0_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_9);
            	    lv_expressions_2_0=ruleExpressionOrRegion();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getExpressionRegionRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"expressions",
            	              		lv_expressions_2_0, 
            	              		"org.scenariotools.sml.expressions.ScenarioExpressions.ExpressionOrRegion");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }

            	    otherlv_3=(Token)match(input,17,FollowSets000.FOLLOW_8); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getExpressionRegionAccess().getSemicolonKeyword_2_1());
            	          
            	    }

            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_4=(Token)match(input,18,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getExpressionRegionAccess().getRightCurlyBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionRegion"


    // $ANTLR start "entryRuleExpressionOrRegion"
    // InternalScenarioExpressions.g:251:1: entryRuleExpressionOrRegion returns [EObject current=null] : iv_ruleExpressionOrRegion= ruleExpressionOrRegion EOF ;
    public final EObject entryRuleExpressionOrRegion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionOrRegion = null;


        try {
            // InternalScenarioExpressions.g:252:2: (iv_ruleExpressionOrRegion= ruleExpressionOrRegion EOF )
            // InternalScenarioExpressions.g:253:2: iv_ruleExpressionOrRegion= ruleExpressionOrRegion EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionOrRegionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpressionOrRegion=ruleExpressionOrRegion();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionOrRegion; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionOrRegion"


    // $ANTLR start "ruleExpressionOrRegion"
    // InternalScenarioExpressions.g:260:1: ruleExpressionOrRegion returns [EObject current=null] : (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables ) ;
    public final EObject ruleExpressionOrRegion() throws RecognitionException {
        EObject current = null;

        EObject this_ExpressionRegion_0 = null;

        EObject this_ExpressionAndVariables_1 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:263:28: ( (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables ) )
            // InternalScenarioExpressions.g:264:1: (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables )
            {
            // InternalScenarioExpressions.g:264:1: (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==16) ) {
                alt5=1;
            }
            else if ( ((LA5_0>=RULE_ID && LA5_0<=RULE_BOOL)||(LA5_0>=20 && LA5_0<=22)||LA5_0==33||(LA5_0>=36 && LA5_0<=37)||LA5_0==40) ) {
                alt5=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalScenarioExpressions.g:265:5: this_ExpressionRegion_0= ruleExpressionRegion
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionOrRegionAccess().getExpressionRegionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ExpressionRegion_0=ruleExpressionRegion();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ExpressionRegion_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:275:5: this_ExpressionAndVariables_1= ruleExpressionAndVariables
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionOrRegionAccess().getExpressionAndVariablesParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ExpressionAndVariables_1=ruleExpressionAndVariables();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ExpressionAndVariables_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionOrRegion"


    // $ANTLR start "entryRuleExpressionAndVariables"
    // InternalScenarioExpressions.g:291:1: entryRuleExpressionAndVariables returns [EObject current=null] : iv_ruleExpressionAndVariables= ruleExpressionAndVariables EOF ;
    public final EObject entryRuleExpressionAndVariables() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionAndVariables = null;


        try {
            // InternalScenarioExpressions.g:292:2: (iv_ruleExpressionAndVariables= ruleExpressionAndVariables EOF )
            // InternalScenarioExpressions.g:293:2: iv_ruleExpressionAndVariables= ruleExpressionAndVariables EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionAndVariablesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpressionAndVariables=ruleExpressionAndVariables();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionAndVariables; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionAndVariables"


    // $ANTLR start "ruleExpressionAndVariables"
    // InternalScenarioExpressions.g:300:1: ruleExpressionAndVariables returns [EObject current=null] : (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression ) ;
    public final EObject ruleExpressionAndVariables() throws RecognitionException {
        EObject current = null;

        EObject this_VariableExpression_0 = null;

        EObject this_Expression_1 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:303:28: ( (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression ) )
            // InternalScenarioExpressions.g:304:1: (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression )
            {
            // InternalScenarioExpressions.g:304:1: (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression )
            int alt6=2;
            switch ( input.LA(1) ) {
            case 20:
            case 21:
            case 22:
                {
                alt6=1;
                }
                break;
            case RULE_ID:
                {
                int LA6_2 = input.LA(2);

                if ( (LA6_2==EOF||LA6_2==17||(LA6_2>=23 && LA6_2<=35)||LA6_2==39||LA6_2==41) ) {
                    alt6=2;
                }
                else if ( (LA6_2==19) ) {
                    alt6=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 6, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_STRING:
            case RULE_INT:
            case RULE_SIGNEDINT:
            case RULE_BOOL:
            case 33:
            case 36:
            case 37:
            case 40:
                {
                alt6=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalScenarioExpressions.g:305:5: this_VariableExpression_0= ruleVariableExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionAndVariablesAccess().getVariableExpressionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableExpression_0=ruleVariableExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableExpression_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:315:5: this_Expression_1= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionAndVariablesAccess().getExpressionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Expression_1=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Expression_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionAndVariables"


    // $ANTLR start "entryRuleVariableExpression"
    // InternalScenarioExpressions.g:331:1: entryRuleVariableExpression returns [EObject current=null] : iv_ruleVariableExpression= ruleVariableExpression EOF ;
    public final EObject entryRuleVariableExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableExpression = null;


        try {
            // InternalScenarioExpressions.g:332:2: (iv_ruleVariableExpression= ruleVariableExpression EOF )
            // InternalScenarioExpressions.g:333:2: iv_ruleVariableExpression= ruleVariableExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableExpression=ruleVariableExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableExpression"


    // $ANTLR start "ruleVariableExpression"
    // InternalScenarioExpressions.g:340:1: ruleVariableExpression returns [EObject current=null] : (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment | this_ClockDeclaration_2= ruleClockDeclaration | this_ClockAssignment_3= ruleClockAssignment ) ;
    public final EObject ruleVariableExpression() throws RecognitionException {
        EObject current = null;

        EObject this_TypedVariableDeclaration_0 = null;

        EObject this_VariableAssignment_1 = null;

        EObject this_ClockDeclaration_2 = null;

        EObject this_ClockAssignment_3 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:343:28: ( (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment | this_ClockDeclaration_2= ruleClockDeclaration | this_ClockAssignment_3= ruleClockAssignment ) )
            // InternalScenarioExpressions.g:344:1: (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment | this_ClockDeclaration_2= ruleClockDeclaration | this_ClockAssignment_3= ruleClockAssignment )
            {
            // InternalScenarioExpressions.g:344:1: (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment | this_ClockDeclaration_2= ruleClockDeclaration | this_ClockAssignment_3= ruleClockAssignment )
            int alt7=4;
            switch ( input.LA(1) ) {
            case 20:
                {
                alt7=1;
                }
                break;
            case RULE_ID:
                {
                alt7=2;
                }
                break;
            case 21:
                {
                alt7=3;
                }
                break;
            case 22:
                {
                alt7=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // InternalScenarioExpressions.g:345:5: this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getVariableExpressionAccess().getTypedVariableDeclarationParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_TypedVariableDeclaration_0=ruleTypedVariableDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TypedVariableDeclaration_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:355:5: this_VariableAssignment_1= ruleVariableAssignment
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getVariableExpressionAccess().getVariableAssignmentParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableAssignment_1=ruleVariableAssignment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableAssignment_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalScenarioExpressions.g:365:5: this_ClockDeclaration_2= ruleClockDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getVariableExpressionAccess().getClockDeclarationParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ClockDeclaration_2=ruleClockDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ClockDeclaration_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // InternalScenarioExpressions.g:375:5: this_ClockAssignment_3= ruleClockAssignment
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getVariableExpressionAccess().getClockAssignmentParserRuleCall_3()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ClockAssignment_3=ruleClockAssignment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ClockAssignment_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableExpression"


    // $ANTLR start "entryRuleVariableAssignment"
    // InternalScenarioExpressions.g:393:1: entryRuleVariableAssignment returns [EObject current=null] : iv_ruleVariableAssignment= ruleVariableAssignment EOF ;
    public final EObject entryRuleVariableAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableAssignment = null;


        try {
            // InternalScenarioExpressions.g:394:2: (iv_ruleVariableAssignment= ruleVariableAssignment EOF )
            // InternalScenarioExpressions.g:395:2: iv_ruleVariableAssignment= ruleVariableAssignment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableAssignmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableAssignment=ruleVariableAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableAssignment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableAssignment"


    // $ANTLR start "ruleVariableAssignment"
    // InternalScenarioExpressions.g:402:1: ruleVariableAssignment returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) ) ;
    public final EObject ruleVariableAssignment() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_expression_2_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:405:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) ) )
            // InternalScenarioExpressions.g:406:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) )
            {
            // InternalScenarioExpressions.g:406:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) )
            // InternalScenarioExpressions.g:406:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) )
            {
            // InternalScenarioExpressions.g:406:2: ( (otherlv_0= RULE_ID ) )
            // InternalScenarioExpressions.g:407:1: (otherlv_0= RULE_ID )
            {
            // InternalScenarioExpressions.g:407:1: (otherlv_0= RULE_ID )
            // InternalScenarioExpressions.g:408:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getVariableAssignmentRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_10); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,19,FollowSets000.FOLLOW_11); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getVariableAssignmentAccess().getEqualsSignKeyword_1());
                  
            }
            // InternalScenarioExpressions.g:423:1: ( (lv_expression_2_0= ruleExpression ) )
            // InternalScenarioExpressions.g:424:1: (lv_expression_2_0= ruleExpression )
            {
            // InternalScenarioExpressions.g:424:1: (lv_expression_2_0= ruleExpression )
            // InternalScenarioExpressions.g:425:3: lv_expression_2_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getVariableAssignmentAccess().getExpressionExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_expression_2_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getVariableAssignmentRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_2_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableAssignment"


    // $ANTLR start "entryRuleTypedVariableDeclaration"
    // InternalScenarioExpressions.g:449:1: entryRuleTypedVariableDeclaration returns [EObject current=null] : iv_ruleTypedVariableDeclaration= ruleTypedVariableDeclaration EOF ;
    public final EObject entryRuleTypedVariableDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypedVariableDeclaration = null;


        try {
            // InternalScenarioExpressions.g:450:2: (iv_ruleTypedVariableDeclaration= ruleTypedVariableDeclaration EOF )
            // InternalScenarioExpressions.g:451:2: iv_ruleTypedVariableDeclaration= ruleTypedVariableDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypedVariableDeclarationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleTypedVariableDeclaration=ruleTypedVariableDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTypedVariableDeclaration; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypedVariableDeclaration"


    // $ANTLR start "ruleTypedVariableDeclaration"
    // InternalScenarioExpressions.g:458:1: ruleTypedVariableDeclaration returns [EObject current=null] : (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? ) ;
    public final EObject ruleTypedVariableDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        EObject lv_expression_4_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:461:28: ( (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? ) )
            // InternalScenarioExpressions.g:462:1: (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? )
            {
            // InternalScenarioExpressions.g:462:1: (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? )
            // InternalScenarioExpressions.g:462:3: otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )?
            {
            otherlv_0=(Token)match(input,20,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getTypedVariableDeclarationAccess().getVarKeyword_0());
                  
            }
            // InternalScenarioExpressions.g:466:1: ( (otherlv_1= RULE_ID ) )
            // InternalScenarioExpressions.g:467:1: (otherlv_1= RULE_ID )
            {
            // InternalScenarioExpressions.g:467:1: (otherlv_1= RULE_ID )
            // InternalScenarioExpressions.g:468:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getTypedVariableDeclarationRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierCrossReference_1_0()); 
              	
            }

            }


            }

            // InternalScenarioExpressions.g:479:2: ( (lv_name_2_0= RULE_ID ) )
            // InternalScenarioExpressions.g:480:1: (lv_name_2_0= RULE_ID )
            {
            // InternalScenarioExpressions.g:480:1: (lv_name_2_0= RULE_ID )
            // InternalScenarioExpressions.g:481:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_12); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getTypedVariableDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getTypedVariableDeclarationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalScenarioExpressions.g:497:2: (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==19) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalScenarioExpressions.g:497:4: otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) )
                    {
                    otherlv_3=(Token)match(input,19,FollowSets000.FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getTypedVariableDeclarationAccess().getEqualsSignKeyword_3_0());
                          
                    }
                    // InternalScenarioExpressions.g:501:1: ( (lv_expression_4_0= ruleExpression ) )
                    // InternalScenarioExpressions.g:502:1: (lv_expression_4_0= ruleExpression )
                    {
                    // InternalScenarioExpressions.g:502:1: (lv_expression_4_0= ruleExpression )
                    // InternalScenarioExpressions.g:503:3: lv_expression_4_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getTypedVariableDeclarationAccess().getExpressionExpressionParserRuleCall_3_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_4_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getTypedVariableDeclarationRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_4_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypedVariableDeclaration"


    // $ANTLR start "entryRuleClockDeclaration"
    // InternalScenarioExpressions.g:527:1: entryRuleClockDeclaration returns [EObject current=null] : iv_ruleClockDeclaration= ruleClockDeclaration EOF ;
    public final EObject entryRuleClockDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClockDeclaration = null;


        try {
            // InternalScenarioExpressions.g:528:2: (iv_ruleClockDeclaration= ruleClockDeclaration EOF )
            // InternalScenarioExpressions.g:529:2: iv_ruleClockDeclaration= ruleClockDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClockDeclarationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleClockDeclaration=ruleClockDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClockDeclaration; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClockDeclaration"


    // $ANTLR start "ruleClockDeclaration"
    // InternalScenarioExpressions.g:536:1: ruleClockDeclaration returns [EObject current=null] : ( () otherlv_1= 'clock' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) ) )? ) ;
    public final EObject ruleClockDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        EObject lv_expression_4_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:539:28: ( ( () otherlv_1= 'clock' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) ) )? ) )
            // InternalScenarioExpressions.g:540:1: ( () otherlv_1= 'clock' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) ) )? )
            {
            // InternalScenarioExpressions.g:540:1: ( () otherlv_1= 'clock' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) ) )? )
            // InternalScenarioExpressions.g:540:2: () otherlv_1= 'clock' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) ) )?
            {
            // InternalScenarioExpressions.g:540:2: ()
            // InternalScenarioExpressions.g:541:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getClockDeclarationAccess().getClockDeclarationAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,21,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getClockDeclarationAccess().getClockKeyword_1());
                  
            }
            // InternalScenarioExpressions.g:550:1: ( (lv_name_2_0= RULE_ID ) )
            // InternalScenarioExpressions.g:551:1: (lv_name_2_0= RULE_ID )
            {
            // InternalScenarioExpressions.g:551:1: (lv_name_2_0= RULE_ID )
            // InternalScenarioExpressions.g:552:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_12); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getClockDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getClockDeclarationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalScenarioExpressions.g:568:2: (otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==19) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalScenarioExpressions.g:568:4: otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) )
                    {
                    otherlv_3=(Token)match(input,19,FollowSets000.FOLLOW_13); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getClockDeclarationAccess().getEqualsSignKeyword_3_0());
                          
                    }
                    // InternalScenarioExpressions.g:572:1: ( (lv_expression_4_0= ruleIntegerValue ) )
                    // InternalScenarioExpressions.g:573:1: (lv_expression_4_0= ruleIntegerValue )
                    {
                    // InternalScenarioExpressions.g:573:1: (lv_expression_4_0= ruleIntegerValue )
                    // InternalScenarioExpressions.g:574:3: lv_expression_4_0= ruleIntegerValue
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getClockDeclarationAccess().getExpressionIntegerValueParserRuleCall_3_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_4_0=ruleIntegerValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getClockDeclarationRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_4_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.IntegerValue");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClockDeclaration"


    // $ANTLR start "entryRuleClockAssignment"
    // InternalScenarioExpressions.g:600:1: entryRuleClockAssignment returns [EObject current=null] : iv_ruleClockAssignment= ruleClockAssignment EOF ;
    public final EObject entryRuleClockAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClockAssignment = null;


        try {
            // InternalScenarioExpressions.g:601:2: (iv_ruleClockAssignment= ruleClockAssignment EOF )
            // InternalScenarioExpressions.g:602:2: iv_ruleClockAssignment= ruleClockAssignment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClockAssignmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleClockAssignment=ruleClockAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClockAssignment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClockAssignment"


    // $ANTLR start "ruleClockAssignment"
    // InternalScenarioExpressions.g:609:1: ruleClockAssignment returns [EObject current=null] : (otherlv_0= 'reset clock' ( (otherlv_1= RULE_ID ) ) (otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) ) )? ) ;
    public final EObject ruleClockAssignment() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_expression_3_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:612:28: ( (otherlv_0= 'reset clock' ( (otherlv_1= RULE_ID ) ) (otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) ) )? ) )
            // InternalScenarioExpressions.g:613:1: (otherlv_0= 'reset clock' ( (otherlv_1= RULE_ID ) ) (otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) ) )? )
            {
            // InternalScenarioExpressions.g:613:1: (otherlv_0= 'reset clock' ( (otherlv_1= RULE_ID ) ) (otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) ) )? )
            // InternalScenarioExpressions.g:613:3: otherlv_0= 'reset clock' ( (otherlv_1= RULE_ID ) ) (otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) ) )?
            {
            otherlv_0=(Token)match(input,22,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getClockAssignmentAccess().getResetClockKeyword_0());
                  
            }
            // InternalScenarioExpressions.g:617:1: ( (otherlv_1= RULE_ID ) )
            // InternalScenarioExpressions.g:618:1: (otherlv_1= RULE_ID )
            {
            // InternalScenarioExpressions.g:618:1: (otherlv_1= RULE_ID )
            // InternalScenarioExpressions.g:619:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getClockAssignmentRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_12); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getClockAssignmentAccess().getVariableClockDeclarationCrossReference_1_0()); 
              	
            }

            }


            }

            // InternalScenarioExpressions.g:630:2: (otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==19) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalScenarioExpressions.g:630:4: otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) )
                    {
                    otherlv_2=(Token)match(input,19,FollowSets000.FOLLOW_13); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getClockAssignmentAccess().getEqualsSignKeyword_2_0());
                          
                    }
                    // InternalScenarioExpressions.g:634:1: ( (lv_expression_3_0= ruleIntegerValue ) )
                    // InternalScenarioExpressions.g:635:1: (lv_expression_3_0= ruleIntegerValue )
                    {
                    // InternalScenarioExpressions.g:635:1: (lv_expression_3_0= ruleIntegerValue )
                    // InternalScenarioExpressions.g:636:3: lv_expression_3_0= ruleIntegerValue
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getClockAssignmentAccess().getExpressionIntegerValueParserRuleCall_2_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_3_0=ruleIntegerValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getClockAssignmentRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.IntegerValue");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClockAssignment"


    // $ANTLR start "entryRuleExpression"
    // InternalScenarioExpressions.g:660:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // InternalScenarioExpressions.g:661:2: (iv_ruleExpression= ruleExpression EOF )
            // InternalScenarioExpressions.g:662:2: iv_ruleExpression= ruleExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpression=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalScenarioExpressions.g:669:1: ruleExpression returns [EObject current=null] : this_ImplicationExpression_0= ruleImplicationExpression ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_ImplicationExpression_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:672:28: (this_ImplicationExpression_0= ruleImplicationExpression )
            // InternalScenarioExpressions.g:674:5: this_ImplicationExpression_0= ruleImplicationExpression
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getExpressionAccess().getImplicationExpressionParserRuleCall()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_2);
            this_ImplicationExpression_0=ruleImplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_ImplicationExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleImplicationExpression"
    // InternalScenarioExpressions.g:690:1: entryRuleImplicationExpression returns [EObject current=null] : iv_ruleImplicationExpression= ruleImplicationExpression EOF ;
    public final EObject entryRuleImplicationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImplicationExpression = null;


        try {
            // InternalScenarioExpressions.g:691:2: (iv_ruleImplicationExpression= ruleImplicationExpression EOF )
            // InternalScenarioExpressions.g:692:2: iv_ruleImplicationExpression= ruleImplicationExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getImplicationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleImplicationExpression=ruleImplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleImplicationExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImplicationExpression"


    // $ANTLR start "ruleImplicationExpression"
    // InternalScenarioExpressions.g:699:1: ruleImplicationExpression returns [EObject current=null] : (this_DisjunctionExpression_0= ruleDisjunctionExpression ( () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) ) )? ) ;
    public final EObject ruleImplicationExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_0=null;
        EObject this_DisjunctionExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:702:28: ( (this_DisjunctionExpression_0= ruleDisjunctionExpression ( () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) ) )? ) )
            // InternalScenarioExpressions.g:703:1: (this_DisjunctionExpression_0= ruleDisjunctionExpression ( () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) ) )? )
            {
            // InternalScenarioExpressions.g:703:1: (this_DisjunctionExpression_0= ruleDisjunctionExpression ( () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) ) )? )
            // InternalScenarioExpressions.g:704:5: this_DisjunctionExpression_0= ruleDisjunctionExpression ( () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getImplicationExpressionAccess().getDisjunctionExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_14);
            this_DisjunctionExpression_0=ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_DisjunctionExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalScenarioExpressions.g:712:1: ( () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==23) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalScenarioExpressions.g:712:2: () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) )
                    {
                    // InternalScenarioExpressions.g:712:2: ()
                    // InternalScenarioExpressions.g:713:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getImplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalScenarioExpressions.g:718:2: ( (lv_operator_2_0= '=>' ) )
                    // InternalScenarioExpressions.g:719:1: (lv_operator_2_0= '=>' )
                    {
                    // InternalScenarioExpressions.g:719:1: (lv_operator_2_0= '=>' )
                    // InternalScenarioExpressions.g:720:3: lv_operator_2_0= '=>'
                    {
                    lv_operator_2_0=(Token)match(input,23,FollowSets000.FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_2_0, grammarAccess.getImplicationExpressionAccess().getOperatorEqualsSignGreaterThanSignKeyword_1_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getImplicationExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_2_0, "=>");
                      	    
                    }

                    }


                    }

                    // InternalScenarioExpressions.g:733:2: ( (lv_right_3_0= ruleImplicationExpression ) )
                    // InternalScenarioExpressions.g:734:1: (lv_right_3_0= ruleImplicationExpression )
                    {
                    // InternalScenarioExpressions.g:734:1: (lv_right_3_0= ruleImplicationExpression )
                    // InternalScenarioExpressions.g:735:3: lv_right_3_0= ruleImplicationExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getImplicationExpressionAccess().getRightImplicationExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleImplicationExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getImplicationExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.ImplicationExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImplicationExpression"


    // $ANTLR start "entryRuleDisjunctionExpression"
    // InternalScenarioExpressions.g:759:1: entryRuleDisjunctionExpression returns [EObject current=null] : iv_ruleDisjunctionExpression= ruleDisjunctionExpression EOF ;
    public final EObject entryRuleDisjunctionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDisjunctionExpression = null;


        try {
            // InternalScenarioExpressions.g:760:2: (iv_ruleDisjunctionExpression= ruleDisjunctionExpression EOF )
            // InternalScenarioExpressions.g:761:2: iv_ruleDisjunctionExpression= ruleDisjunctionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDisjunctionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleDisjunctionExpression=ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDisjunctionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDisjunctionExpression"


    // $ANTLR start "ruleDisjunctionExpression"
    // InternalScenarioExpressions.g:768:1: ruleDisjunctionExpression returns [EObject current=null] : (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? ) ;
    public final EObject ruleDisjunctionExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_0=null;
        EObject this_ConjunctionExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:771:28: ( (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? ) )
            // InternalScenarioExpressions.g:772:1: (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? )
            {
            // InternalScenarioExpressions.g:772:1: (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? )
            // InternalScenarioExpressions.g:773:5: this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getDisjunctionExpressionAccess().getConjunctionExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_15);
            this_ConjunctionExpression_0=ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_ConjunctionExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalScenarioExpressions.g:781:1: ( () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==24) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalScenarioExpressions.g:781:2: () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) )
                    {
                    // InternalScenarioExpressions.g:781:2: ()
                    // InternalScenarioExpressions.g:782:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getDisjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalScenarioExpressions.g:787:2: ( (lv_operator_2_0= '||' ) )
                    // InternalScenarioExpressions.g:788:1: (lv_operator_2_0= '||' )
                    {
                    // InternalScenarioExpressions.g:788:1: (lv_operator_2_0= '||' )
                    // InternalScenarioExpressions.g:789:3: lv_operator_2_0= '||'
                    {
                    lv_operator_2_0=(Token)match(input,24,FollowSets000.FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_2_0, grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineVerticalLineKeyword_1_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getDisjunctionExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_2_0, "||");
                      	    
                    }

                    }


                    }

                    // InternalScenarioExpressions.g:802:2: ( (lv_right_3_0= ruleDisjunctionExpression ) )
                    // InternalScenarioExpressions.g:803:1: (lv_right_3_0= ruleDisjunctionExpression )
                    {
                    // InternalScenarioExpressions.g:803:1: (lv_right_3_0= ruleDisjunctionExpression )
                    // InternalScenarioExpressions.g:804:3: lv_right_3_0= ruleDisjunctionExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getDisjunctionExpressionAccess().getRightDisjunctionExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleDisjunctionExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getDisjunctionExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.DisjunctionExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDisjunctionExpression"


    // $ANTLR start "entryRuleConjunctionExpression"
    // InternalScenarioExpressions.g:828:1: entryRuleConjunctionExpression returns [EObject current=null] : iv_ruleConjunctionExpression= ruleConjunctionExpression EOF ;
    public final EObject entryRuleConjunctionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConjunctionExpression = null;


        try {
            // InternalScenarioExpressions.g:829:2: (iv_ruleConjunctionExpression= ruleConjunctionExpression EOF )
            // InternalScenarioExpressions.g:830:2: iv_ruleConjunctionExpression= ruleConjunctionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConjunctionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConjunctionExpression=ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConjunctionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConjunctionExpression"


    // $ANTLR start "ruleConjunctionExpression"
    // InternalScenarioExpressions.g:837:1: ruleConjunctionExpression returns [EObject current=null] : (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? ) ;
    public final EObject ruleConjunctionExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_0=null;
        EObject this_RelationExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:840:28: ( (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? ) )
            // InternalScenarioExpressions.g:841:1: (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? )
            {
            // InternalScenarioExpressions.g:841:1: (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? )
            // InternalScenarioExpressions.g:842:5: this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getConjunctionExpressionAccess().getRelationExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_16);
            this_RelationExpression_0=ruleRelationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_RelationExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalScenarioExpressions.g:850:1: ( () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==25) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalScenarioExpressions.g:850:2: () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) )
                    {
                    // InternalScenarioExpressions.g:850:2: ()
                    // InternalScenarioExpressions.g:851:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getConjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalScenarioExpressions.g:856:2: ( (lv_operator_2_0= '&&' ) )
                    // InternalScenarioExpressions.g:857:1: (lv_operator_2_0= '&&' )
                    {
                    // InternalScenarioExpressions.g:857:1: (lv_operator_2_0= '&&' )
                    // InternalScenarioExpressions.g:858:3: lv_operator_2_0= '&&'
                    {
                    lv_operator_2_0=(Token)match(input,25,FollowSets000.FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_2_0, grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandAmpersandKeyword_1_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getConjunctionExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_2_0, "&&");
                      	    
                    }

                    }


                    }

                    // InternalScenarioExpressions.g:871:2: ( (lv_right_3_0= ruleConjunctionExpression ) )
                    // InternalScenarioExpressions.g:872:1: (lv_right_3_0= ruleConjunctionExpression )
                    {
                    // InternalScenarioExpressions.g:872:1: (lv_right_3_0= ruleConjunctionExpression )
                    // InternalScenarioExpressions.g:873:3: lv_right_3_0= ruleConjunctionExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getConjunctionExpressionAccess().getRightConjunctionExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleConjunctionExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getConjunctionExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.ConjunctionExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConjunctionExpression"


    // $ANTLR start "entryRuleRelationExpression"
    // InternalScenarioExpressions.g:897:1: entryRuleRelationExpression returns [EObject current=null] : iv_ruleRelationExpression= ruleRelationExpression EOF ;
    public final EObject entryRuleRelationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRelationExpression = null;


        try {
            // InternalScenarioExpressions.g:898:2: (iv_ruleRelationExpression= ruleRelationExpression EOF )
            // InternalScenarioExpressions.g:899:2: iv_ruleRelationExpression= ruleRelationExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRelationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRelationExpression=ruleRelationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRelationExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelationExpression"


    // $ANTLR start "ruleRelationExpression"
    // InternalScenarioExpressions.g:906:1: ruleRelationExpression returns [EObject current=null] : (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? ) ;
    public final EObject ruleRelationExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        Token lv_operator_2_3=null;
        Token lv_operator_2_4=null;
        Token lv_operator_2_5=null;
        Token lv_operator_2_6=null;
        EObject this_AdditionExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:909:28: ( (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? ) )
            // InternalScenarioExpressions.g:910:1: (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? )
            {
            // InternalScenarioExpressions.g:910:1: (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? )
            // InternalScenarioExpressions.g:911:5: this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getRelationExpressionAccess().getAdditionExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_17);
            this_AdditionExpression_0=ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_AdditionExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalScenarioExpressions.g:919:1: ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( ((LA15_0>=26 && LA15_0<=31)) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalScenarioExpressions.g:919:2: () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) )
                    {
                    // InternalScenarioExpressions.g:919:2: ()
                    // InternalScenarioExpressions.g:920:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getRelationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalScenarioExpressions.g:925:2: ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) )
                    // InternalScenarioExpressions.g:926:1: ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) )
                    {
                    // InternalScenarioExpressions.g:926:1: ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) )
                    // InternalScenarioExpressions.g:927:1: (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' )
                    {
                    // InternalScenarioExpressions.g:927:1: (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' )
                    int alt14=6;
                    switch ( input.LA(1) ) {
                    case 26:
                        {
                        alt14=1;
                        }
                        break;
                    case 27:
                        {
                        alt14=2;
                        }
                        break;
                    case 28:
                        {
                        alt14=3;
                        }
                        break;
                    case 29:
                        {
                        alt14=4;
                        }
                        break;
                    case 30:
                        {
                        alt14=5;
                        }
                        break;
                    case 31:
                        {
                        alt14=6;
                        }
                        break;
                    default:
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 14, 0, input);

                        throw nvae;
                    }

                    switch (alt14) {
                        case 1 :
                            // InternalScenarioExpressions.g:928:3: lv_operator_2_1= '=='
                            {
                            lv_operator_2_1=(Token)match(input,26,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_1, grammarAccess.getRelationExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalScenarioExpressions.g:940:8: lv_operator_2_2= '!='
                            {
                            lv_operator_2_2=(Token)match(input,27,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_2, grammarAccess.getRelationExpressionAccess().getOperatorExclamationMarkEqualsSignKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
                              	    
                            }

                            }
                            break;
                        case 3 :
                            // InternalScenarioExpressions.g:952:8: lv_operator_2_3= '<'
                            {
                            lv_operator_2_3=(Token)match(input,28,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_3, grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignKeyword_1_1_0_2());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_3, null);
                              	    
                            }

                            }
                            break;
                        case 4 :
                            // InternalScenarioExpressions.g:964:8: lv_operator_2_4= '>'
                            {
                            lv_operator_2_4=(Token)match(input,29,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_4, grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignKeyword_1_1_0_3());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_4, null);
                              	    
                            }

                            }
                            break;
                        case 5 :
                            // InternalScenarioExpressions.g:976:8: lv_operator_2_5= '<='
                            {
                            lv_operator_2_5=(Token)match(input,30,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_5, grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_1_0_4());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_5, null);
                              	    
                            }

                            }
                            break;
                        case 6 :
                            // InternalScenarioExpressions.g:988:8: lv_operator_2_6= '>='
                            {
                            lv_operator_2_6=(Token)match(input,31,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_6, grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_1_0_5());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_6, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalScenarioExpressions.g:1003:2: ( (lv_right_3_0= ruleRelationExpression ) )
                    // InternalScenarioExpressions.g:1004:1: (lv_right_3_0= ruleRelationExpression )
                    {
                    // InternalScenarioExpressions.g:1004:1: (lv_right_3_0= ruleRelationExpression )
                    // InternalScenarioExpressions.g:1005:3: lv_right_3_0= ruleRelationExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getRelationExpressionAccess().getRightRelationExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleRelationExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getRelationExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.RelationExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelationExpression"


    // $ANTLR start "entryRuleAdditionExpression"
    // InternalScenarioExpressions.g:1031:1: entryRuleAdditionExpression returns [EObject current=null] : iv_ruleAdditionExpression= ruleAdditionExpression EOF ;
    public final EObject entryRuleAdditionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAdditionExpression = null;


        try {
            // InternalScenarioExpressions.g:1032:2: (iv_ruleAdditionExpression= ruleAdditionExpression EOF )
            // InternalScenarioExpressions.g:1033:2: iv_ruleAdditionExpression= ruleAdditionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAdditionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAdditionExpression=ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAdditionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAdditionExpression"


    // $ANTLR start "ruleAdditionExpression"
    // InternalScenarioExpressions.g:1040:1: ruleAdditionExpression returns [EObject current=null] : (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? ) ;
    public final EObject ruleAdditionExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        EObject this_MultiplicationExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1043:28: ( (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? ) )
            // InternalScenarioExpressions.g:1044:1: (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? )
            {
            // InternalScenarioExpressions.g:1044:1: (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? )
            // InternalScenarioExpressions.g:1045:5: this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getAdditionExpressionAccess().getMultiplicationExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_18);
            this_MultiplicationExpression_0=ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_MultiplicationExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalScenarioExpressions.g:1053:1: ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( ((LA17_0>=32 && LA17_0<=33)) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalScenarioExpressions.g:1053:2: () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) )
                    {
                    // InternalScenarioExpressions.g:1053:2: ()
                    // InternalScenarioExpressions.g:1054:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getAdditionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalScenarioExpressions.g:1059:2: ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) )
                    // InternalScenarioExpressions.g:1060:1: ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) )
                    {
                    // InternalScenarioExpressions.g:1060:1: ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) )
                    // InternalScenarioExpressions.g:1061:1: (lv_operator_2_1= '+' | lv_operator_2_2= '-' )
                    {
                    // InternalScenarioExpressions.g:1061:1: (lv_operator_2_1= '+' | lv_operator_2_2= '-' )
                    int alt16=2;
                    int LA16_0 = input.LA(1);

                    if ( (LA16_0==32) ) {
                        alt16=1;
                    }
                    else if ( (LA16_0==33) ) {
                        alt16=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 16, 0, input);

                        throw nvae;
                    }
                    switch (alt16) {
                        case 1 :
                            // InternalScenarioExpressions.g:1062:3: lv_operator_2_1= '+'
                            {
                            lv_operator_2_1=(Token)match(input,32,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_1, grammarAccess.getAdditionExpressionAccess().getOperatorPlusSignKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getAdditionExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalScenarioExpressions.g:1074:8: lv_operator_2_2= '-'
                            {
                            lv_operator_2_2=(Token)match(input,33,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_2, grammarAccess.getAdditionExpressionAccess().getOperatorHyphenMinusKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getAdditionExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalScenarioExpressions.g:1089:2: ( (lv_right_3_0= ruleAdditionExpression ) )
                    // InternalScenarioExpressions.g:1090:1: (lv_right_3_0= ruleAdditionExpression )
                    {
                    // InternalScenarioExpressions.g:1090:1: (lv_right_3_0= ruleAdditionExpression )
                    // InternalScenarioExpressions.g:1091:3: lv_right_3_0= ruleAdditionExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getAdditionExpressionAccess().getRightAdditionExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleAdditionExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getAdditionExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.AdditionExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdditionExpression"


    // $ANTLR start "entryRuleMultiplicationExpression"
    // InternalScenarioExpressions.g:1115:1: entryRuleMultiplicationExpression returns [EObject current=null] : iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF ;
    public final EObject entryRuleMultiplicationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMultiplicationExpression = null;


        try {
            // InternalScenarioExpressions.g:1116:2: (iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF )
            // InternalScenarioExpressions.g:1117:2: iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMultiplicationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleMultiplicationExpression=ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMultiplicationExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultiplicationExpression"


    // $ANTLR start "ruleMultiplicationExpression"
    // InternalScenarioExpressions.g:1124:1: ruleMultiplicationExpression returns [EObject current=null] : (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? ) ;
    public final EObject ruleMultiplicationExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        EObject this_NegatedExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1127:28: ( (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? ) )
            // InternalScenarioExpressions.g:1128:1: (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? )
            {
            // InternalScenarioExpressions.g:1128:1: (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? )
            // InternalScenarioExpressions.g:1129:5: this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getMultiplicationExpressionAccess().getNegatedExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_19);
            this_NegatedExpression_0=ruleNegatedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_NegatedExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalScenarioExpressions.g:1137:1: ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( ((LA19_0>=34 && LA19_0<=35)) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalScenarioExpressions.g:1137:2: () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) )
                    {
                    // InternalScenarioExpressions.g:1137:2: ()
                    // InternalScenarioExpressions.g:1138:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getMultiplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalScenarioExpressions.g:1143:2: ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) )
                    // InternalScenarioExpressions.g:1144:1: ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) )
                    {
                    // InternalScenarioExpressions.g:1144:1: ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) )
                    // InternalScenarioExpressions.g:1145:1: (lv_operator_2_1= '*' | lv_operator_2_2= '/' )
                    {
                    // InternalScenarioExpressions.g:1145:1: (lv_operator_2_1= '*' | lv_operator_2_2= '/' )
                    int alt18=2;
                    int LA18_0 = input.LA(1);

                    if ( (LA18_0==34) ) {
                        alt18=1;
                    }
                    else if ( (LA18_0==35) ) {
                        alt18=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 18, 0, input);

                        throw nvae;
                    }
                    switch (alt18) {
                        case 1 :
                            // InternalScenarioExpressions.g:1146:3: lv_operator_2_1= '*'
                            {
                            lv_operator_2_1=(Token)match(input,34,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_1, grammarAccess.getMultiplicationExpressionAccess().getOperatorAsteriskKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getMultiplicationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalScenarioExpressions.g:1158:8: lv_operator_2_2= '/'
                            {
                            lv_operator_2_2=(Token)match(input,35,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_2, grammarAccess.getMultiplicationExpressionAccess().getOperatorSolidusKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getMultiplicationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalScenarioExpressions.g:1173:2: ( (lv_right_3_0= ruleMultiplicationExpression ) )
                    // InternalScenarioExpressions.g:1174:1: (lv_right_3_0= ruleMultiplicationExpression )
                    {
                    // InternalScenarioExpressions.g:1174:1: (lv_right_3_0= ruleMultiplicationExpression )
                    // InternalScenarioExpressions.g:1175:3: lv_right_3_0= ruleMultiplicationExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getMultiplicationExpressionAccess().getRightMultiplicationExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleMultiplicationExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getMultiplicationExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.MultiplicationExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicationExpression"


    // $ANTLR start "entryRuleNegatedExpression"
    // InternalScenarioExpressions.g:1199:1: entryRuleNegatedExpression returns [EObject current=null] : iv_ruleNegatedExpression= ruleNegatedExpression EOF ;
    public final EObject entryRuleNegatedExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNegatedExpression = null;


        try {
            // InternalScenarioExpressions.g:1200:2: (iv_ruleNegatedExpression= ruleNegatedExpression EOF )
            // InternalScenarioExpressions.g:1201:2: iv_ruleNegatedExpression= ruleNegatedExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNegatedExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNegatedExpression=ruleNegatedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNegatedExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNegatedExpression"


    // $ANTLR start "ruleNegatedExpression"
    // InternalScenarioExpressions.g:1208:1: ruleNegatedExpression returns [EObject current=null] : ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression ) ;
    public final EObject ruleNegatedExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_1_1=null;
        Token lv_operator_1_2=null;
        EObject lv_operand_2_0 = null;

        EObject this_BasicExpression_3 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1211:28: ( ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression ) )
            // InternalScenarioExpressions.g:1212:1: ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression )
            {
            // InternalScenarioExpressions.g:1212:1: ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression )
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==33||LA21_0==36) ) {
                alt21=1;
            }
            else if ( ((LA21_0>=RULE_ID && LA21_0<=RULE_BOOL)||LA21_0==37||LA21_0==40) ) {
                alt21=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;
            }
            switch (alt21) {
                case 1 :
                    // InternalScenarioExpressions.g:1212:2: ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) )
                    {
                    // InternalScenarioExpressions.g:1212:2: ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) )
                    // InternalScenarioExpressions.g:1212:3: () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) )
                    {
                    // InternalScenarioExpressions.g:1212:3: ()
                    // InternalScenarioExpressions.g:1213:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getNegatedExpressionAccess().getUnaryOperationExpressionAction_0_0(),
                                  current);
                          
                    }

                    }

                    // InternalScenarioExpressions.g:1218:2: ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) )
                    // InternalScenarioExpressions.g:1218:3: ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) )
                    {
                    // InternalScenarioExpressions.g:1231:1: ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) )
                    // InternalScenarioExpressions.g:1232:1: (lv_operator_1_1= '!' | lv_operator_1_2= '-' )
                    {
                    // InternalScenarioExpressions.g:1232:1: (lv_operator_1_1= '!' | lv_operator_1_2= '-' )
                    int alt20=2;
                    int LA20_0 = input.LA(1);

                    if ( (LA20_0==36) ) {
                        alt20=1;
                    }
                    else if ( (LA20_0==33) ) {
                        alt20=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 20, 0, input);

                        throw nvae;
                    }
                    switch (alt20) {
                        case 1 :
                            // InternalScenarioExpressions.g:1233:3: lv_operator_1_1= '!'
                            {
                            lv_operator_1_1=(Token)match(input,36,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_1_1, grammarAccess.getNegatedExpressionAccess().getOperatorExclamationMarkKeyword_0_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getNegatedExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_1_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalScenarioExpressions.g:1245:8: lv_operator_1_2= '-'
                            {
                            lv_operator_1_2=(Token)match(input,33,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_1_2, grammarAccess.getNegatedExpressionAccess().getOperatorHyphenMinusKeyword_0_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getNegatedExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_1_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalScenarioExpressions.g:1260:2: ( (lv_operand_2_0= ruleBasicExpression ) )
                    // InternalScenarioExpressions.g:1261:1: (lv_operand_2_0= ruleBasicExpression )
                    {
                    // InternalScenarioExpressions.g:1261:1: (lv_operand_2_0= ruleBasicExpression )
                    // InternalScenarioExpressions.g:1262:3: lv_operand_2_0= ruleBasicExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getNegatedExpressionAccess().getOperandBasicExpressionParserRuleCall_0_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_operand_2_0=ruleBasicExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getNegatedExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"operand",
                              		lv_operand_2_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.BasicExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1280:5: this_BasicExpression_3= ruleBasicExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getNegatedExpressionAccess().getBasicExpressionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_BasicExpression_3=ruleBasicExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BasicExpression_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNegatedExpression"


    // $ANTLR start "entryRuleBasicExpression"
    // InternalScenarioExpressions.g:1296:1: entryRuleBasicExpression returns [EObject current=null] : iv_ruleBasicExpression= ruleBasicExpression EOF ;
    public final EObject entryRuleBasicExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBasicExpression = null;


        try {
            // InternalScenarioExpressions.g:1297:2: (iv_ruleBasicExpression= ruleBasicExpression EOF )
            // InternalScenarioExpressions.g:1298:2: iv_ruleBasicExpression= ruleBasicExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBasicExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBasicExpression=ruleBasicExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBasicExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBasicExpression"


    // $ANTLR start "ruleBasicExpression"
    // InternalScenarioExpressions.g:1305:1: ruleBasicExpression returns [EObject current=null] : (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) ) ;
    public final EObject ruleBasicExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject this_Value_0 = null;

        EObject this_Expression_2 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1308:28: ( (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) ) )
            // InternalScenarioExpressions.g:1309:1: (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) )
            {
            // InternalScenarioExpressions.g:1309:1: (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) )
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( ((LA22_0>=RULE_ID && LA22_0<=RULE_BOOL)||LA22_0==40) ) {
                alt22=1;
            }
            else if ( (LA22_0==37) ) {
                alt22=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }
            switch (alt22) {
                case 1 :
                    // InternalScenarioExpressions.g:1310:5: this_Value_0= ruleValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBasicExpressionAccess().getValueParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Value_0=ruleValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Value_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1319:6: (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' )
                    {
                    // InternalScenarioExpressions.g:1319:6: (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' )
                    // InternalScenarioExpressions.g:1319:8: otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')'
                    {
                    otherlv_1=(Token)match(input,37,FollowSets000.FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBasicExpressionAccess().getExpressionParserRuleCall_1_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_20);
                    this_Expression_2=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Expression_2; 
                              afterParserOrEnumRuleCall();
                          
                    }
                    otherlv_3=(Token)match(input,38,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getBasicExpressionAccess().getRightParenthesisKeyword_1_2());
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBasicExpression"


    // $ANTLR start "entryRuleValue"
    // InternalScenarioExpressions.g:1344:1: entryRuleValue returns [EObject current=null] : iv_ruleValue= ruleValue EOF ;
    public final EObject entryRuleValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleValue = null;


        try {
            // InternalScenarioExpressions.g:1345:2: (iv_ruleValue= ruleValue EOF )
            // InternalScenarioExpressions.g:1346:2: iv_ruleValue= ruleValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleValue=ruleValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValue"


    // $ANTLR start "ruleValue"
    // InternalScenarioExpressions.g:1353:1: ruleValue returns [EObject current=null] : (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess ) ;
    public final EObject ruleValue() throws RecognitionException {
        EObject current = null;

        EObject this_IntegerValue_0 = null;

        EObject this_BooleanValue_1 = null;

        EObject this_StringValue_2 = null;

        EObject this_EnumValue_3 = null;

        EObject this_NullValue_4 = null;

        EObject this_VariableValue_5 = null;

        EObject this_FeatureAccess_6 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1356:28: ( (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess ) )
            // InternalScenarioExpressions.g:1357:1: (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess )
            {
            // InternalScenarioExpressions.g:1357:1: (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess )
            int alt23=7;
            switch ( input.LA(1) ) {
            case RULE_INT:
            case RULE_SIGNEDINT:
                {
                alt23=1;
                }
                break;
            case RULE_BOOL:
                {
                alt23=2;
                }
                break;
            case RULE_STRING:
                {
                alt23=3;
                }
                break;
            case RULE_ID:
                {
                switch ( input.LA(2) ) {
                case 39:
                    {
                    alt23=4;
                    }
                    break;
                case EOF:
                case 17:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 38:
                case 42:
                    {
                    alt23=6;
                    }
                    break;
                case 41:
                    {
                    alt23=7;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 23, 4, input);

                    throw nvae;
                }

                }
                break;
            case 40:
                {
                alt23=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }

            switch (alt23) {
                case 1 :
                    // InternalScenarioExpressions.g:1358:5: this_IntegerValue_0= ruleIntegerValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getIntegerValueParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntegerValue_0=ruleIntegerValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IntegerValue_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1368:5: this_BooleanValue_1= ruleBooleanValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getBooleanValueParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_BooleanValue_1=ruleBooleanValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BooleanValue_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalScenarioExpressions.g:1378:5: this_StringValue_2= ruleStringValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getStringValueParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_StringValue_2=ruleStringValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_StringValue_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // InternalScenarioExpressions.g:1388:5: this_EnumValue_3= ruleEnumValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getEnumValueParserRuleCall_3()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_EnumValue_3=ruleEnumValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_EnumValue_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // InternalScenarioExpressions.g:1398:5: this_NullValue_4= ruleNullValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getNullValueParserRuleCall_4()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_NullValue_4=ruleNullValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_NullValue_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 6 :
                    // InternalScenarioExpressions.g:1408:5: this_VariableValue_5= ruleVariableValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getVariableValueParserRuleCall_5()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableValue_5=ruleVariableValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableValue_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 7 :
                    // InternalScenarioExpressions.g:1418:5: this_FeatureAccess_6= ruleFeatureAccess
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getFeatureAccessParserRuleCall_6()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_FeatureAccess_6=ruleFeatureAccess();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_FeatureAccess_6; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValue"


    // $ANTLR start "entryRuleIntegerValue"
    // InternalScenarioExpressions.g:1434:1: entryRuleIntegerValue returns [EObject current=null] : iv_ruleIntegerValue= ruleIntegerValue EOF ;
    public final EObject entryRuleIntegerValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerValue = null;


        try {
            // InternalScenarioExpressions.g:1435:2: (iv_ruleIntegerValue= ruleIntegerValue EOF )
            // InternalScenarioExpressions.g:1436:2: iv_ruleIntegerValue= ruleIntegerValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntegerValue=ruleIntegerValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerValue"


    // $ANTLR start "ruleIntegerValue"
    // InternalScenarioExpressions.g:1443:1: ruleIntegerValue returns [EObject current=null] : ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) ) ;
    public final EObject ruleIntegerValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_1=null;
        Token lv_value_0_2=null;

         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1446:28: ( ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) ) )
            // InternalScenarioExpressions.g:1447:1: ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) )
            {
            // InternalScenarioExpressions.g:1447:1: ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) )
            // InternalScenarioExpressions.g:1448:1: ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) )
            {
            // InternalScenarioExpressions.g:1448:1: ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) )
            // InternalScenarioExpressions.g:1449:1: (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT )
            {
            // InternalScenarioExpressions.g:1449:1: (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT )
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==RULE_INT) ) {
                alt24=1;
            }
            else if ( (LA24_0==RULE_SIGNEDINT) ) {
                alt24=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;
            }
            switch (alt24) {
                case 1 :
                    // InternalScenarioExpressions.g:1450:3: lv_value_0_1= RULE_INT
                    {
                    lv_value_0_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_value_0_1, grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getIntegerValueRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"value",
                              		lv_value_0_1, 
                              		"org.eclipse.xtext.common.Terminals.INT");
                      	    
                    }

                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1465:8: lv_value_0_2= RULE_SIGNEDINT
                    {
                    lv_value_0_2=(Token)match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_value_0_2, grammarAccess.getIntegerValueAccess().getValueSIGNEDINTTerminalRuleCall_0_1()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getIntegerValueRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"value",
                              		lv_value_0_2, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
                      	    
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerValue"


    // $ANTLR start "entryRuleBooleanValue"
    // InternalScenarioExpressions.g:1491:1: entryRuleBooleanValue returns [EObject current=null] : iv_ruleBooleanValue= ruleBooleanValue EOF ;
    public final EObject entryRuleBooleanValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanValue = null;


        try {
            // InternalScenarioExpressions.g:1492:2: (iv_ruleBooleanValue= ruleBooleanValue EOF )
            // InternalScenarioExpressions.g:1493:2: iv_ruleBooleanValue= ruleBooleanValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBooleanValue=ruleBooleanValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanValue"


    // $ANTLR start "ruleBooleanValue"
    // InternalScenarioExpressions.g:1500:1: ruleBooleanValue returns [EObject current=null] : ( (lv_value_0_0= RULE_BOOL ) ) ;
    public final EObject ruleBooleanValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1503:28: ( ( (lv_value_0_0= RULE_BOOL ) ) )
            // InternalScenarioExpressions.g:1504:1: ( (lv_value_0_0= RULE_BOOL ) )
            {
            // InternalScenarioExpressions.g:1504:1: ( (lv_value_0_0= RULE_BOOL ) )
            // InternalScenarioExpressions.g:1505:1: (lv_value_0_0= RULE_BOOL )
            {
            // InternalScenarioExpressions.g:1505:1: (lv_value_0_0= RULE_BOOL )
            // InternalScenarioExpressions.g:1506:3: lv_value_0_0= RULE_BOOL
            {
            lv_value_0_0=(Token)match(input,RULE_BOOL,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_0_0, grammarAccess.getBooleanValueAccess().getValueBOOLTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getBooleanValueRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.BOOL");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanValue"


    // $ANTLR start "entryRuleStringValue"
    // InternalScenarioExpressions.g:1530:1: entryRuleStringValue returns [EObject current=null] : iv_ruleStringValue= ruleStringValue EOF ;
    public final EObject entryRuleStringValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringValue = null;


        try {
            // InternalScenarioExpressions.g:1531:2: (iv_ruleStringValue= ruleStringValue EOF )
            // InternalScenarioExpressions.g:1532:2: iv_ruleStringValue= ruleStringValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStringValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStringValue=ruleStringValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStringValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringValue"


    // $ANTLR start "ruleStringValue"
    // InternalScenarioExpressions.g:1539:1: ruleStringValue returns [EObject current=null] : ( (lv_value_0_0= RULE_STRING ) ) ;
    public final EObject ruleStringValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1542:28: ( ( (lv_value_0_0= RULE_STRING ) ) )
            // InternalScenarioExpressions.g:1543:1: ( (lv_value_0_0= RULE_STRING ) )
            {
            // InternalScenarioExpressions.g:1543:1: ( (lv_value_0_0= RULE_STRING ) )
            // InternalScenarioExpressions.g:1544:1: (lv_value_0_0= RULE_STRING )
            {
            // InternalScenarioExpressions.g:1544:1: (lv_value_0_0= RULE_STRING )
            // InternalScenarioExpressions.g:1545:3: lv_value_0_0= RULE_STRING
            {
            lv_value_0_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_0_0, grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getStringValueRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"org.eclipse.xtext.common.Terminals.STRING");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringValue"


    // $ANTLR start "entryRuleEnumValue"
    // InternalScenarioExpressions.g:1569:1: entryRuleEnumValue returns [EObject current=null] : iv_ruleEnumValue= ruleEnumValue EOF ;
    public final EObject entryRuleEnumValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumValue = null;


        try {
            // InternalScenarioExpressions.g:1570:2: (iv_ruleEnumValue= ruleEnumValue EOF )
            // InternalScenarioExpressions.g:1571:2: iv_ruleEnumValue= ruleEnumValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEnumValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleEnumValue=ruleEnumValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEnumValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumValue"


    // $ANTLR start "ruleEnumValue"
    // InternalScenarioExpressions.g:1578:1: ruleEnumValue returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) ) ;
    public final EObject ruleEnumValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1581:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) ) )
            // InternalScenarioExpressions.g:1582:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) )
            {
            // InternalScenarioExpressions.g:1582:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) )
            // InternalScenarioExpressions.g:1582:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) )
            {
            // InternalScenarioExpressions.g:1582:2: ( (otherlv_0= RULE_ID ) )
            // InternalScenarioExpressions.g:1583:1: (otherlv_0= RULE_ID )
            {
            // InternalScenarioExpressions.g:1583:1: (otherlv_0= RULE_ID )
            // InternalScenarioExpressions.g:1584:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getEnumValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_21); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getEnumValueAccess().getTypeEEnumCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,39,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getEnumValueAccess().getColonKeyword_1());
                  
            }
            // InternalScenarioExpressions.g:1599:1: ( (otherlv_2= RULE_ID ) )
            // InternalScenarioExpressions.g:1600:1: (otherlv_2= RULE_ID )
            {
            // InternalScenarioExpressions.g:1600:1: (otherlv_2= RULE_ID )
            // InternalScenarioExpressions.g:1601:3: otherlv_2= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getEnumValueRule());
              	        }
                      
            }
            otherlv_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_2, grammarAccess.getEnumValueAccess().getValueEEnumLiteralCrossReference_2_0()); 
              	
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumValue"


    // $ANTLR start "entryRuleNullValue"
    // InternalScenarioExpressions.g:1620:1: entryRuleNullValue returns [EObject current=null] : iv_ruleNullValue= ruleNullValue EOF ;
    public final EObject entryRuleNullValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNullValue = null;


        try {
            // InternalScenarioExpressions.g:1621:2: (iv_ruleNullValue= ruleNullValue EOF )
            // InternalScenarioExpressions.g:1622:2: iv_ruleNullValue= ruleNullValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNullValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNullValue=ruleNullValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNullValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNullValue"


    // $ANTLR start "ruleNullValue"
    // InternalScenarioExpressions.g:1629:1: ruleNullValue returns [EObject current=null] : ( () otherlv_1= 'null' ) ;
    public final EObject ruleNullValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1632:28: ( ( () otherlv_1= 'null' ) )
            // InternalScenarioExpressions.g:1633:1: ( () otherlv_1= 'null' )
            {
            // InternalScenarioExpressions.g:1633:1: ( () otherlv_1= 'null' )
            // InternalScenarioExpressions.g:1633:2: () otherlv_1= 'null'
            {
            // InternalScenarioExpressions.g:1633:2: ()
            // InternalScenarioExpressions.g:1634:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getNullValueAccess().getNullValueAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,40,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getNullValueAccess().getNullKeyword_1());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNullValue"


    // $ANTLR start "entryRuleVariableValue"
    // InternalScenarioExpressions.g:1651:1: entryRuleVariableValue returns [EObject current=null] : iv_ruleVariableValue= ruleVariableValue EOF ;
    public final EObject entryRuleVariableValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableValue = null;


        try {
            // InternalScenarioExpressions.g:1652:2: (iv_ruleVariableValue= ruleVariableValue EOF )
            // InternalScenarioExpressions.g:1653:2: iv_ruleVariableValue= ruleVariableValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableValue=ruleVariableValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableValue"


    // $ANTLR start "ruleVariableValue"
    // InternalScenarioExpressions.g:1660:1: ruleVariableValue returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleVariableValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1663:28: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalScenarioExpressions.g:1664:1: ( (otherlv_0= RULE_ID ) )
            {
            // InternalScenarioExpressions.g:1664:1: ( (otherlv_0= RULE_ID ) )
            // InternalScenarioExpressions.g:1665:1: (otherlv_0= RULE_ID )
            {
            // InternalScenarioExpressions.g:1665:1: (otherlv_0= RULE_ID )
            // InternalScenarioExpressions.g:1666:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getVariableValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getVariableValueAccess().getValueVariableCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableValue"


    // $ANTLR start "entryRuleCollectionAccess"
    // InternalScenarioExpressions.g:1685:1: entryRuleCollectionAccess returns [EObject current=null] : iv_ruleCollectionAccess= ruleCollectionAccess EOF ;
    public final EObject entryRuleCollectionAccess() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCollectionAccess = null;


        try {
            // InternalScenarioExpressions.g:1686:2: (iv_ruleCollectionAccess= ruleCollectionAccess EOF )
            // InternalScenarioExpressions.g:1687:2: iv_ruleCollectionAccess= ruleCollectionAccess EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCollectionAccessRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCollectionAccess=ruleCollectionAccess();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCollectionAccess; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCollectionAccess"


    // $ANTLR start "ruleCollectionAccess"
    // InternalScenarioExpressions.g:1694:1: ruleCollectionAccess returns [EObject current=null] : ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' ) ;
    public final EObject ruleCollectionAccess() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Enumerator lv_collectionOperation_0_0 = null;

        EObject lv_parameter_2_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1697:28: ( ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' ) )
            // InternalScenarioExpressions.g:1698:1: ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' )
            {
            // InternalScenarioExpressions.g:1698:1: ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' )
            // InternalScenarioExpressions.g:1698:2: ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')'
            {
            // InternalScenarioExpressions.g:1698:2: ( (lv_collectionOperation_0_0= ruleCollectionOperation ) )
            // InternalScenarioExpressions.g:1699:1: (lv_collectionOperation_0_0= ruleCollectionOperation )
            {
            // InternalScenarioExpressions.g:1699:1: (lv_collectionOperation_0_0= ruleCollectionOperation )
            // InternalScenarioExpressions.g:1700:3: lv_collectionOperation_0_0= ruleCollectionOperation
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getCollectionAccessAccess().getCollectionOperationCollectionOperationEnumRuleCall_0_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_22);
            lv_collectionOperation_0_0=ruleCollectionOperation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getCollectionAccessRule());
              	        }
                     		set(
                     			current, 
                     			"collectionOperation",
                      		lv_collectionOperation_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionOperation");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,37,FollowSets000.FOLLOW_23); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getCollectionAccessAccess().getLeftParenthesisKeyword_1());
                  
            }
            // InternalScenarioExpressions.g:1720:1: ( (lv_parameter_2_0= ruleExpression ) )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( ((LA25_0>=RULE_ID && LA25_0<=RULE_BOOL)||LA25_0==33||(LA25_0>=36 && LA25_0<=37)||LA25_0==40) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalScenarioExpressions.g:1721:1: (lv_parameter_2_0= ruleExpression )
                    {
                    // InternalScenarioExpressions.g:1721:1: (lv_parameter_2_0= ruleExpression )
                    // InternalScenarioExpressions.g:1722:3: lv_parameter_2_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getCollectionAccessAccess().getParameterExpressionParserRuleCall_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_20);
                    lv_parameter_2_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getCollectionAccessRule());
                      	        }
                             		set(
                             			current, 
                             			"parameter",
                              		lv_parameter_2_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,38,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getCollectionAccessAccess().getRightParenthesisKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollectionAccess"


    // $ANTLR start "entryRuleFeatureAccess"
    // InternalScenarioExpressions.g:1750:1: entryRuleFeatureAccess returns [EObject current=null] : iv_ruleFeatureAccess= ruleFeatureAccess EOF ;
    public final EObject entryRuleFeatureAccess() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureAccess = null;


        try {
            // InternalScenarioExpressions.g:1751:2: (iv_ruleFeatureAccess= ruleFeatureAccess EOF )
            // InternalScenarioExpressions.g:1752:2: iv_ruleFeatureAccess= ruleFeatureAccess EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFeatureAccessRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleFeatureAccess=ruleFeatureAccess();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFeatureAccess; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureAccess"


    // $ANTLR start "ruleFeatureAccess"
    // InternalScenarioExpressions.g:1759:1: ruleFeatureAccess returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) | ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' ) ) ) ;
    public final EObject ruleFeatureAccess() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        EObject lv_value_2_0 = null;

        EObject lv_collectionAccess_4_0 = null;

        EObject lv_value_5_0 = null;

        EObject lv_parameters_7_0 = null;

        EObject lv_parameters_9_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1762:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) | ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' ) ) ) )
            // InternalScenarioExpressions.g:1763:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) | ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' ) ) )
            {
            // InternalScenarioExpressions.g:1763:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) | ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' ) ) )
            // InternalScenarioExpressions.g:1763:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) | ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' ) )
            {
            // InternalScenarioExpressions.g:1763:2: ( (otherlv_0= RULE_ID ) )
            // InternalScenarioExpressions.g:1764:1: (otherlv_0= RULE_ID )
            {
            // InternalScenarioExpressions.g:1764:1: (otherlv_0= RULE_ID )
            // InternalScenarioExpressions.g:1765:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getFeatureAccessRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_24); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getFeatureAccessAccess().getTargetEObjectCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,41,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getFeatureAccessAccess().getFullStopKeyword_1());
                  
            }
            // InternalScenarioExpressions.g:1780:1: ( ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) | ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' ) )
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==RULE_ID) ) {
                int LA29_1 = input.LA(2);

                if ( (LA29_1==37) ) {
                    alt29=2;
                }
                else if ( (LA29_1==EOF||LA29_1==17||(LA29_1>=23 && LA29_1<=35)||LA29_1==38||(LA29_1>=41 && LA29_1<=42)) ) {
                    alt29=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 29, 1, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }
            switch (alt29) {
                case 1 :
                    // InternalScenarioExpressions.g:1780:2: ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? )
                    {
                    // InternalScenarioExpressions.g:1780:2: ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? )
                    // InternalScenarioExpressions.g:1780:3: ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )?
                    {
                    // InternalScenarioExpressions.g:1780:3: ( (lv_value_2_0= ruleStructuralFeatureValue ) )
                    // InternalScenarioExpressions.g:1781:1: (lv_value_2_0= ruleStructuralFeatureValue )
                    {
                    // InternalScenarioExpressions.g:1781:1: (lv_value_2_0= ruleStructuralFeatureValue )
                    // InternalScenarioExpressions.g:1782:3: lv_value_2_0= ruleStructuralFeatureValue
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getValueStructuralFeatureValueParserRuleCall_2_0_0_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_25);
                    lv_value_2_0=ruleStructuralFeatureValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
                      	        }
                             		set(
                             			current, 
                             			"value",
                              		lv_value_2_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.StructuralFeatureValue");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // InternalScenarioExpressions.g:1798:2: (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )?
                    int alt26=2;
                    int LA26_0 = input.LA(1);

                    if ( (LA26_0==41) ) {
                        alt26=1;
                    }
                    switch (alt26) {
                        case 1 :
                            // InternalScenarioExpressions.g:1798:4: otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) )
                            {
                            otherlv_3=(Token)match(input,41,FollowSets000.FOLLOW_26); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                  	newLeafNode(otherlv_3, grammarAccess.getFeatureAccessAccess().getFullStopKeyword_2_0_1_0());
                                  
                            }
                            // InternalScenarioExpressions.g:1802:1: ( (lv_collectionAccess_4_0= ruleCollectionAccess ) )
                            // InternalScenarioExpressions.g:1803:1: (lv_collectionAccess_4_0= ruleCollectionAccess )
                            {
                            // InternalScenarioExpressions.g:1803:1: (lv_collectionAccess_4_0= ruleCollectionAccess )
                            // InternalScenarioExpressions.g:1804:3: lv_collectionAccess_4_0= ruleCollectionAccess
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getCollectionAccessCollectionAccessParserRuleCall_2_0_1_1_0()); 
                              	    
                            }
                            pushFollow(FollowSets000.FOLLOW_2);
                            lv_collectionAccess_4_0=ruleCollectionAccess();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"collectionAccess",
                                      		lv_collectionAccess_4_0, 
                                      		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionAccess");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1821:6: ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' )
                    {
                    // InternalScenarioExpressions.g:1821:6: ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' )
                    // InternalScenarioExpressions.g:1821:7: ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')'
                    {
                    // InternalScenarioExpressions.g:1821:7: ( (lv_value_5_0= ruleOperationValue ) )
                    // InternalScenarioExpressions.g:1822:1: (lv_value_5_0= ruleOperationValue )
                    {
                    // InternalScenarioExpressions.g:1822:1: (lv_value_5_0= ruleOperationValue )
                    // InternalScenarioExpressions.g:1823:3: lv_value_5_0= ruleOperationValue
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getValueOperationValueParserRuleCall_2_1_0_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_22);
                    lv_value_5_0=ruleOperationValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
                      	        }
                             		set(
                             			current, 
                             			"value",
                              		lv_value_5_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.OperationValue");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    otherlv_6=(Token)match(input,37,FollowSets000.FOLLOW_23); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_6, grammarAccess.getFeatureAccessAccess().getLeftParenthesisKeyword_2_1_1());
                          
                    }
                    // InternalScenarioExpressions.g:1843:1: ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )?
                    int alt28=2;
                    int LA28_0 = input.LA(1);

                    if ( ((LA28_0>=RULE_ID && LA28_0<=RULE_BOOL)||LA28_0==33||(LA28_0>=36 && LA28_0<=37)||LA28_0==40) ) {
                        alt28=1;
                    }
                    switch (alt28) {
                        case 1 :
                            // InternalScenarioExpressions.g:1843:2: ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )*
                            {
                            // InternalScenarioExpressions.g:1843:2: ( (lv_parameters_7_0= ruleExpression ) )
                            // InternalScenarioExpressions.g:1844:1: (lv_parameters_7_0= ruleExpression )
                            {
                            // InternalScenarioExpressions.g:1844:1: (lv_parameters_7_0= ruleExpression )
                            // InternalScenarioExpressions.g:1845:3: lv_parameters_7_0= ruleExpression
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getParametersExpressionParserRuleCall_2_1_2_0_0()); 
                              	    
                            }
                            pushFollow(FollowSets000.FOLLOW_27);
                            lv_parameters_7_0=ruleExpression();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
                              	        }
                                     		add(
                                     			current, 
                                     			"parameters",
                                      		lv_parameters_7_0, 
                                      		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }

                            // InternalScenarioExpressions.g:1861:2: (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )*
                            loop27:
                            do {
                                int alt27=2;
                                int LA27_0 = input.LA(1);

                                if ( (LA27_0==42) ) {
                                    alt27=1;
                                }


                                switch (alt27) {
                            	case 1 :
                            	    // InternalScenarioExpressions.g:1861:4: otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) )
                            	    {
                            	    otherlv_8=(Token)match(input,42,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	          	newLeafNode(otherlv_8, grammarAccess.getFeatureAccessAccess().getCommaKeyword_2_1_2_1_0());
                            	          
                            	    }
                            	    // InternalScenarioExpressions.g:1865:1: ( (lv_parameters_9_0= ruleExpression ) )
                            	    // InternalScenarioExpressions.g:1866:1: (lv_parameters_9_0= ruleExpression )
                            	    {
                            	    // InternalScenarioExpressions.g:1866:1: (lv_parameters_9_0= ruleExpression )
                            	    // InternalScenarioExpressions.g:1867:3: lv_parameters_9_0= ruleExpression
                            	    {
                            	    if ( state.backtracking==0 ) {
                            	       
                            	      	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getParametersExpressionParserRuleCall_2_1_2_1_1_0()); 
                            	      	    
                            	    }
                            	    pushFollow(FollowSets000.FOLLOW_27);
                            	    lv_parameters_9_0=ruleExpression();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      	        if (current==null) {
                            	      	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
                            	      	        }
                            	             		add(
                            	             			current, 
                            	             			"parameters",
                            	              		lv_parameters_9_0, 
                            	              		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
                            	      	        afterParserOrEnumRuleCall();
                            	      	    
                            	    }

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop27;
                                }
                            } while (true);


                            }
                            break;

                    }

                    otherlv_10=(Token)match(input,38,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_10, grammarAccess.getFeatureAccessAccess().getRightParenthesisKeyword_2_1_3());
                          
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureAccess"


    // $ANTLR start "entryRuleStructuralFeatureValue"
    // InternalScenarioExpressions.g:1895:1: entryRuleStructuralFeatureValue returns [EObject current=null] : iv_ruleStructuralFeatureValue= ruleStructuralFeatureValue EOF ;
    public final EObject entryRuleStructuralFeatureValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStructuralFeatureValue = null;


        try {
            // InternalScenarioExpressions.g:1896:2: (iv_ruleStructuralFeatureValue= ruleStructuralFeatureValue EOF )
            // InternalScenarioExpressions.g:1897:2: iv_ruleStructuralFeatureValue= ruleStructuralFeatureValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStructuralFeatureValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStructuralFeatureValue=ruleStructuralFeatureValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStructuralFeatureValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStructuralFeatureValue"


    // $ANTLR start "ruleStructuralFeatureValue"
    // InternalScenarioExpressions.g:1904:1: ruleStructuralFeatureValue returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleStructuralFeatureValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1907:28: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalScenarioExpressions.g:1908:1: ( (otherlv_0= RULE_ID ) )
            {
            // InternalScenarioExpressions.g:1908:1: ( (otherlv_0= RULE_ID ) )
            // InternalScenarioExpressions.g:1909:1: (otherlv_0= RULE_ID )
            {
            // InternalScenarioExpressions.g:1909:1: (otherlv_0= RULE_ID )
            // InternalScenarioExpressions.g:1910:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getStructuralFeatureValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStructuralFeatureValue"


    // $ANTLR start "entryRuleOperationValue"
    // InternalScenarioExpressions.g:1929:1: entryRuleOperationValue returns [EObject current=null] : iv_ruleOperationValue= ruleOperationValue EOF ;
    public final EObject entryRuleOperationValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOperationValue = null;


        try {
            // InternalScenarioExpressions.g:1930:2: (iv_ruleOperationValue= ruleOperationValue EOF )
            // InternalScenarioExpressions.g:1931:2: iv_ruleOperationValue= ruleOperationValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOperationValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleOperationValue=ruleOperationValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOperationValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOperationValue"


    // $ANTLR start "ruleOperationValue"
    // InternalScenarioExpressions.g:1938:1: ruleOperationValue returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleOperationValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1941:28: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalScenarioExpressions.g:1942:1: ( (otherlv_0= RULE_ID ) )
            {
            // InternalScenarioExpressions.g:1942:1: ( (otherlv_0= RULE_ID ) )
            // InternalScenarioExpressions.g:1943:1: (otherlv_0= RULE_ID )
            {
            // InternalScenarioExpressions.g:1943:1: (otherlv_0= RULE_ID )
            // InternalScenarioExpressions.g:1944:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getOperationValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getOperationValueAccess().getValueEOperationCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOperationValue"


    // $ANTLR start "ruleCollectionOperation"
    // InternalScenarioExpressions.g:1963:1: ruleCollectionOperation returns [Enumerator current=null] : ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) ) ;
    public final Enumerator ruleCollectionOperation() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;
        Token enumLiteral_7=null;

         enterRule(); 
        try {
            // InternalScenarioExpressions.g:1965:28: ( ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) ) )
            // InternalScenarioExpressions.g:1966:1: ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) )
            {
            // InternalScenarioExpressions.g:1966:1: ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) )
            int alt30=8;
            switch ( input.LA(1) ) {
            case 43:
                {
                alt30=1;
                }
                break;
            case 44:
                {
                alt30=2;
                }
                break;
            case 45:
                {
                alt30=3;
                }
                break;
            case 46:
                {
                alt30=4;
                }
                break;
            case 47:
                {
                alt30=5;
                }
                break;
            case 48:
                {
                alt30=6;
                }
                break;
            case 49:
                {
                alt30=7;
                }
                break;
            case 50:
                {
                alt30=8;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }

            switch (alt30) {
                case 1 :
                    // InternalScenarioExpressions.g:1966:2: (enumLiteral_0= 'any' )
                    {
                    // InternalScenarioExpressions.g:1966:2: (enumLiteral_0= 'any' )
                    // InternalScenarioExpressions.g:1966:4: enumLiteral_0= 'any'
                    {
                    enumLiteral_0=(Token)match(input,43,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1972:6: (enumLiteral_1= 'contains' )
                    {
                    // InternalScenarioExpressions.g:1972:6: (enumLiteral_1= 'contains' )
                    // InternalScenarioExpressions.g:1972:8: enumLiteral_1= 'contains'
                    {
                    enumLiteral_1=(Token)match(input,44,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalScenarioExpressions.g:1978:6: (enumLiteral_2= 'containsAll' )
                    {
                    // InternalScenarioExpressions.g:1978:6: (enumLiteral_2= 'containsAll' )
                    // InternalScenarioExpressions.g:1978:8: enumLiteral_2= 'containsAll'
                    {
                    enumLiteral_2=(Token)match(input,45,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalScenarioExpressions.g:1984:6: (enumLiteral_3= 'first' )
                    {
                    // InternalScenarioExpressions.g:1984:6: (enumLiteral_3= 'first' )
                    // InternalScenarioExpressions.g:1984:8: enumLiteral_3= 'first'
                    {
                    enumLiteral_3=(Token)match(input,46,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_3, grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3()); 
                          
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalScenarioExpressions.g:1990:6: (enumLiteral_4= 'get' )
                    {
                    // InternalScenarioExpressions.g:1990:6: (enumLiteral_4= 'get' )
                    // InternalScenarioExpressions.g:1990:8: enumLiteral_4= 'get'
                    {
                    enumLiteral_4=(Token)match(input,47,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_4, grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4()); 
                          
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalScenarioExpressions.g:1996:6: (enumLiteral_5= 'isEmpty' )
                    {
                    // InternalScenarioExpressions.g:1996:6: (enumLiteral_5= 'isEmpty' )
                    // InternalScenarioExpressions.g:1996:8: enumLiteral_5= 'isEmpty'
                    {
                    enumLiteral_5=(Token)match(input,48,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_5, grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5()); 
                          
                    }

                    }


                    }
                    break;
                case 7 :
                    // InternalScenarioExpressions.g:2002:6: (enumLiteral_6= 'last' )
                    {
                    // InternalScenarioExpressions.g:2002:6: (enumLiteral_6= 'last' )
                    // InternalScenarioExpressions.g:2002:8: enumLiteral_6= 'last'
                    {
                    enumLiteral_6=(Token)match(input,49,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_6, grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6()); 
                          
                    }

                    }


                    }
                    break;
                case 8 :
                    // InternalScenarioExpressions.g:2008:6: (enumLiteral_7= 'size' )
                    {
                    // InternalScenarioExpressions.g:2008:6: (enumLiteral_7= 'size' )
                    // InternalScenarioExpressions.g:2008:8: enumLiteral_7= 'size'
                    {
                    enumLiteral_7=(Token)match(input,50,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_7, grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollectionOperation"

    // Delegated rules


 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x000000000001C002L});
        public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000014002L});
        public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000010002L});
        public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x00000132007501F0L});
        public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000080000L});
        public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x00000132007101F0L});
        public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000080002L});
        public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x00000000000000C0L});
        public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000800002L});
        public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000001000002L});
        public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000002000002L});
        public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x00000000FC000002L});
        public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000300000002L});
        public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000C00000002L});
        public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000004000000000L});
        public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000008000000000L});
        public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000002000000000L});
        public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x00000172007101F0L});
        public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000020000000000L});
        public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000020000000002L});
        public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0007F80000000000L});
        public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000044000000000L});
    }


}