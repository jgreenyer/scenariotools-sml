/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.expressions.serializer;

import com.google.inject.Inject;
import java.util.Set;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Parameter;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.serializer.ISerializationContext;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;
import org.scenariotools.sml.expressions.scenarioExpressions.BinaryOperationExpression;
import org.scenariotools.sml.expressions.scenarioExpressions.BooleanValue;
import org.scenariotools.sml.expressions.scenarioExpressions.Clock;
import org.scenariotools.sml.expressions.scenarioExpressions.ClockAssignment;
import org.scenariotools.sml.expressions.scenarioExpressions.ClockDeclaration;
import org.scenariotools.sml.expressions.scenarioExpressions.CollectionAccess;
import org.scenariotools.sml.expressions.scenarioExpressions.Document;
import org.scenariotools.sml.expressions.scenarioExpressions.EnumValue;
import org.scenariotools.sml.expressions.scenarioExpressions.ExpressionRegion;
import org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess;
import org.scenariotools.sml.expressions.scenarioExpressions.Import;
import org.scenariotools.sml.expressions.scenarioExpressions.IntegerValue;
import org.scenariotools.sml.expressions.scenarioExpressions.NullValue;
import org.scenariotools.sml.expressions.scenarioExpressions.OperationValue;
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage;
import org.scenariotools.sml.expressions.scenarioExpressions.StringValue;
import org.scenariotools.sml.expressions.scenarioExpressions.StructuralFeatureValue;
import org.scenariotools.sml.expressions.scenarioExpressions.TimedExpression;
import org.scenariotools.sml.expressions.scenarioExpressions.TypedVariableDeclaration;
import org.scenariotools.sml.expressions.scenarioExpressions.UnaryOperationExpression;
import org.scenariotools.sml.expressions.scenarioExpressions.VariableAssignment;
import org.scenariotools.sml.expressions.scenarioExpressions.VariableDeclaration;
import org.scenariotools.sml.expressions.scenarioExpressions.VariableValue;
import org.scenariotools.sml.expressions.services.ScenarioExpressionsGrammarAccess;

@SuppressWarnings("all")
public abstract class AbstractScenarioExpressionsSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private ScenarioExpressionsGrammarAccess grammarAccess;
	
	@Override
	public void sequence(ISerializationContext context, EObject semanticObject) {
		EPackage epackage = semanticObject.eClass().getEPackage();
		ParserRule rule = context.getParserRule();
		Action action = context.getAssignedAction();
		Set<Parameter> parameters = context.getEnabledBooleanParameters();
		if (epackage == ScenarioExpressionsPackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case ScenarioExpressionsPackage.BINARY_OPERATION_EXPRESSION:
				sequence_AdditionExpression_ConjunctionExpression_DisjunctionExpression_ImplicationExpression_MultiplicationExpression_RelationExpression(context, (BinaryOperationExpression) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.BOOLEAN_VALUE:
				sequence_BooleanValue(context, (BooleanValue) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.CLOCK:
				sequence_Clock(context, (Clock) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.CLOCK_ASSIGNMENT:
				sequence_ClockAssignment(context, (ClockAssignment) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.CLOCK_DECLARATION:
				sequence_ClockDeclaration(context, (ClockDeclaration) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.COLLECTION_ACCESS:
				sequence_CollectionAccess(context, (CollectionAccess) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.DOCUMENT:
				sequence_Document(context, (Document) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.ENUM_VALUE:
				sequence_EnumValue(context, (EnumValue) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.EXPRESSION_REGION:
				sequence_ExpressionRegion(context, (ExpressionRegion) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.FEATURE_ACCESS:
				sequence_FeatureAccess(context, (FeatureAccess) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.IMPORT:
				sequence_Import(context, (Import) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.INTEGER_VALUE:
				sequence_IntegerValue(context, (IntegerValue) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.NULL_VALUE:
				sequence_NullValue(context, (NullValue) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.OPERATION_VALUE:
				sequence_OperationValue(context, (OperationValue) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.STRING_VALUE:
				sequence_StringValue(context, (StringValue) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.STRUCTURAL_FEATURE_VALUE:
				sequence_StructuralFeatureValue(context, (StructuralFeatureValue) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.TIMED_EXPRESSION:
				sequence_TimedExpression(context, (TimedExpression) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.TYPED_VARIABLE_DECLARATION:
				sequence_TypedVariableDeclaration(context, (TypedVariableDeclaration) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.UNARY_OPERATION_EXPRESSION:
				sequence_NegatedExpression(context, (UnaryOperationExpression) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.VARIABLE_ASSIGNMENT:
				sequence_VariableAssignment(context, (VariableAssignment) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.VARIABLE_DECLARATION:
				sequence_VariableDeclaration(context, (VariableDeclaration) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.VARIABLE_VALUE:
				sequence_VariableValue(context, (VariableValue) semanticObject); 
				return; 
			}
		if (errorAcceptor != null)
			errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Contexts:
	 *     ExpressionOrRegion returns BinaryOperationExpression
	 *     ExpressionAndVariables returns BinaryOperationExpression
	 *     Expression returns BinaryOperationExpression
	 *     ImplicationExpression returns BinaryOperationExpression
	 *     ImplicationExpression.BinaryOperationExpression_1_0 returns BinaryOperationExpression
	 *     DisjunctionExpression returns BinaryOperationExpression
	 *     DisjunctionExpression.BinaryOperationExpression_1_0 returns BinaryOperationExpression
	 *     ConjunctionExpression returns BinaryOperationExpression
	 *     ConjunctionExpression.BinaryOperationExpression_1_0 returns BinaryOperationExpression
	 *     RelationExpression returns BinaryOperationExpression
	 *     RelationExpression.BinaryOperationExpression_1_0 returns BinaryOperationExpression
	 *     AdditionExpression returns BinaryOperationExpression
	 *     AdditionExpression.BinaryOperationExpression_1_0 returns BinaryOperationExpression
	 *     MultiplicationExpression returns BinaryOperationExpression
	 *     MultiplicationExpression.BinaryOperationExpression_1_0 returns BinaryOperationExpression
	 *     NegatedExpression returns BinaryOperationExpression
	 *     BasicExpression returns BinaryOperationExpression
	 *
	 * Constraint:
	 *     (
	 *         (left=ImplicationExpression_BinaryOperationExpression_1_0 operator='=>' right=ImplicationExpression) | 
	 *         (left=DisjunctionExpression_BinaryOperationExpression_1_0 operator='||' right=DisjunctionExpression) | 
	 *         (left=ConjunctionExpression_BinaryOperationExpression_1_0 operator='&&' right=ConjunctionExpression) | 
	 *         (
	 *             left=RelationExpression_BinaryOperationExpression_1_0 
	 *             (
	 *                 operator='==' | 
	 *                 operator='!=' | 
	 *                 operator='<' | 
	 *                 operator='>' | 
	 *                 operator='<=' | 
	 *                 operator='>='
	 *             ) 
	 *             right=RelationExpression
	 *         ) | 
	 *         (left=AdditionExpression_BinaryOperationExpression_1_0 (operator='+' | operator='-') right=AdditionExpression) | 
	 *         (left=MultiplicationExpression_BinaryOperationExpression_1_0 (operator='*' | operator='/') right=MultiplicationExpression)
	 *     )
	 */
	protected void sequence_AdditionExpression_ConjunctionExpression_DisjunctionExpression_ImplicationExpression_MultiplicationExpression_RelationExpression(ISerializationContext context, BinaryOperationExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     ExpressionOrRegion returns BooleanValue
	 *     ExpressionAndVariables returns BooleanValue
	 *     Expression returns BooleanValue
	 *     ImplicationExpression returns BooleanValue
	 *     ImplicationExpression.BinaryOperationExpression_1_0 returns BooleanValue
	 *     DisjunctionExpression returns BooleanValue
	 *     DisjunctionExpression.BinaryOperationExpression_1_0 returns BooleanValue
	 *     ConjunctionExpression returns BooleanValue
	 *     ConjunctionExpression.BinaryOperationExpression_1_0 returns BooleanValue
	 *     RelationExpression returns BooleanValue
	 *     RelationExpression.BinaryOperationExpression_1_0 returns BooleanValue
	 *     AdditionExpression returns BooleanValue
	 *     AdditionExpression.BinaryOperationExpression_1_0 returns BooleanValue
	 *     MultiplicationExpression returns BooleanValue
	 *     MultiplicationExpression.BinaryOperationExpression_1_0 returns BooleanValue
	 *     NegatedExpression returns BooleanValue
	 *     BasicExpression returns BooleanValue
	 *     Value returns BooleanValue
	 *     BooleanValue returns BooleanValue
	 *
	 * Constraint:
	 *     value=BOOL
	 */
	protected void sequence_BooleanValue(ISerializationContext context, BooleanValue semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, ScenarioExpressionsPackage.Literals.BOOLEAN_VALUE__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ScenarioExpressionsPackage.Literals.BOOLEAN_VALUE__VALUE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getBooleanValueAccess().getValueBOOLTerminalRuleCall_0(), semanticObject.isValue());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     ExpressionOrRegion returns ClockAssignment
	 *     ExpressionAndVariables returns ClockAssignment
	 *     VariableExpression returns ClockAssignment
	 *     ClockAssignment returns ClockAssignment
	 *
	 * Constraint:
	 *     (variable=[ClockDeclaration|ID] expression=IntegerValue?)
	 */
	protected void sequence_ClockAssignment(ISerializationContext context, ClockAssignment semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     ExpressionOrRegion returns ClockDeclaration
	 *     ExpressionAndVariables returns ClockDeclaration
	 *     VariableExpression returns ClockDeclaration
	 *     ClockDeclaration returns ClockDeclaration
	 *
	 * Constraint:
	 *     (name=ID expression=IntegerValue?)
	 */
	protected void sequence_ClockDeclaration(ISerializationContext context, ClockDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Clock returns Clock
	 *
	 * Constraint:
	 *     (name=ID leftIncluded=BOOL rightIncluded=BOOL leftValue=INT rightValue=INT)
	 */
	protected void sequence_Clock(ISerializationContext context, Clock semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, ScenarioExpressionsPackage.Literals.NAMED_ELEMENT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ScenarioExpressionsPackage.Literals.NAMED_ELEMENT__NAME));
			if (transientValues.isValueTransient(semanticObject, ScenarioExpressionsPackage.Literals.CLOCK__LEFT_INCLUDED) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ScenarioExpressionsPackage.Literals.CLOCK__LEFT_INCLUDED));
			if (transientValues.isValueTransient(semanticObject, ScenarioExpressionsPackage.Literals.CLOCK__RIGHT_INCLUDED) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ScenarioExpressionsPackage.Literals.CLOCK__RIGHT_INCLUDED));
			if (transientValues.isValueTransient(semanticObject, ScenarioExpressionsPackage.Literals.CLOCK__LEFT_VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ScenarioExpressionsPackage.Literals.CLOCK__LEFT_VALUE));
			if (transientValues.isValueTransient(semanticObject, ScenarioExpressionsPackage.Literals.CLOCK__RIGHT_VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ScenarioExpressionsPackage.Literals.CLOCK__RIGHT_VALUE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getClockAccess().getNameIDTerminalRuleCall_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getClockAccess().getLeftIncludedBOOLTerminalRuleCall_1_0(), semanticObject.isLeftIncluded());
		feeder.accept(grammarAccess.getClockAccess().getRightIncludedBOOLTerminalRuleCall_2_0(), semanticObject.isRightIncluded());
		feeder.accept(grammarAccess.getClockAccess().getLeftValueINTTerminalRuleCall_3_0(), semanticObject.getLeftValue());
		feeder.accept(grammarAccess.getClockAccess().getRightValueINTTerminalRuleCall_4_0(), semanticObject.getRightValue());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     CollectionAccess returns CollectionAccess
	 *
	 * Constraint:
	 *     (collectionOperation=CollectionOperation parameter=Expression?)
	 */
	protected void sequence_CollectionAccess(ISerializationContext context, CollectionAccess semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Document returns Document
	 *
	 * Constraint:
	 *     (
	 *         (imports+=Import* domains+=[EPackage|ID]+ expressions+=ExpressionRegion+) | 
	 *         (imports+=Import* expressions+=ExpressionRegion+) | 
	 *         expressions+=ExpressionRegion+
	 *     )?
	 */
	protected void sequence_Document(ISerializationContext context, Document semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     ExpressionOrRegion returns EnumValue
	 *     ExpressionAndVariables returns EnumValue
	 *     Expression returns EnumValue
	 *     ImplicationExpression returns EnumValue
	 *     ImplicationExpression.BinaryOperationExpression_1_0 returns EnumValue
	 *     DisjunctionExpression returns EnumValue
	 *     DisjunctionExpression.BinaryOperationExpression_1_0 returns EnumValue
	 *     ConjunctionExpression returns EnumValue
	 *     ConjunctionExpression.BinaryOperationExpression_1_0 returns EnumValue
	 *     RelationExpression returns EnumValue
	 *     RelationExpression.BinaryOperationExpression_1_0 returns EnumValue
	 *     AdditionExpression returns EnumValue
	 *     AdditionExpression.BinaryOperationExpression_1_0 returns EnumValue
	 *     MultiplicationExpression returns EnumValue
	 *     MultiplicationExpression.BinaryOperationExpression_1_0 returns EnumValue
	 *     NegatedExpression returns EnumValue
	 *     BasicExpression returns EnumValue
	 *     Value returns EnumValue
	 *     EnumValue returns EnumValue
	 *
	 * Constraint:
	 *     (type=[EEnum|ID] value=[EEnumLiteral|ID])
	 */
	protected void sequence_EnumValue(ISerializationContext context, EnumValue semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, ScenarioExpressionsPackage.Literals.ENUM_VALUE__TYPE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ScenarioExpressionsPackage.Literals.ENUM_VALUE__TYPE));
			if (transientValues.isValueTransient(semanticObject, ScenarioExpressionsPackage.Literals.ENUM_VALUE__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ScenarioExpressionsPackage.Literals.ENUM_VALUE__VALUE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getEnumValueAccess().getTypeEEnumIDTerminalRuleCall_0_0_1(), semanticObject.eGet(ScenarioExpressionsPackage.Literals.ENUM_VALUE__TYPE, false));
		feeder.accept(grammarAccess.getEnumValueAccess().getValueEEnumLiteralIDTerminalRuleCall_2_0_1(), semanticObject.eGet(ScenarioExpressionsPackage.Literals.ENUM_VALUE__VALUE, false));
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     ExpressionRegion returns ExpressionRegion
	 *     ExpressionOrRegion returns ExpressionRegion
	 *
	 * Constraint:
	 *     expressions+=ExpressionOrRegion*
	 */
	protected void sequence_ExpressionRegion(ISerializationContext context, ExpressionRegion semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     ExpressionOrRegion returns FeatureAccess
	 *     ExpressionAndVariables returns FeatureAccess
	 *     Expression returns FeatureAccess
	 *     ImplicationExpression returns FeatureAccess
	 *     ImplicationExpression.BinaryOperationExpression_1_0 returns FeatureAccess
	 *     DisjunctionExpression returns FeatureAccess
	 *     DisjunctionExpression.BinaryOperationExpression_1_0 returns FeatureAccess
	 *     ConjunctionExpression returns FeatureAccess
	 *     ConjunctionExpression.BinaryOperationExpression_1_0 returns FeatureAccess
	 *     RelationExpression returns FeatureAccess
	 *     RelationExpression.BinaryOperationExpression_1_0 returns FeatureAccess
	 *     AdditionExpression returns FeatureAccess
	 *     AdditionExpression.BinaryOperationExpression_1_0 returns FeatureAccess
	 *     MultiplicationExpression returns FeatureAccess
	 *     MultiplicationExpression.BinaryOperationExpression_1_0 returns FeatureAccess
	 *     NegatedExpression returns FeatureAccess
	 *     BasicExpression returns FeatureAccess
	 *     Value returns FeatureAccess
	 *     FeatureAccess returns FeatureAccess
	 *
	 * Constraint:
	 *     (
	 *         target=[EObject|ID] 
	 *         ((value=StructuralFeatureValue collectionAccess=CollectionAccess?) | (value=OperationValue (parameters+=Expression parameters+=Expression*)?))
	 *     )
	 */
	protected void sequence_FeatureAccess(ISerializationContext context, FeatureAccess semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Import returns Import
	 *
	 * Constraint:
	 *     importURI=STRING
	 */
	protected void sequence_Import(ISerializationContext context, Import semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, ScenarioExpressionsPackage.Literals.IMPORT__IMPORT_URI) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ScenarioExpressionsPackage.Literals.IMPORT__IMPORT_URI));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0(), semanticObject.getImportURI());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     ExpressionOrRegion returns IntegerValue
	 *     ExpressionAndVariables returns IntegerValue
	 *     Expression returns IntegerValue
	 *     ImplicationExpression returns IntegerValue
	 *     ImplicationExpression.BinaryOperationExpression_1_0 returns IntegerValue
	 *     DisjunctionExpression returns IntegerValue
	 *     DisjunctionExpression.BinaryOperationExpression_1_0 returns IntegerValue
	 *     ConjunctionExpression returns IntegerValue
	 *     ConjunctionExpression.BinaryOperationExpression_1_0 returns IntegerValue
	 *     RelationExpression returns IntegerValue
	 *     RelationExpression.BinaryOperationExpression_1_0 returns IntegerValue
	 *     AdditionExpression returns IntegerValue
	 *     AdditionExpression.BinaryOperationExpression_1_0 returns IntegerValue
	 *     MultiplicationExpression returns IntegerValue
	 *     MultiplicationExpression.BinaryOperationExpression_1_0 returns IntegerValue
	 *     NegatedExpression returns IntegerValue
	 *     BasicExpression returns IntegerValue
	 *     Value returns IntegerValue
	 *     IntegerValue returns IntegerValue
	 *
	 * Constraint:
	 *     (value=INT | value=SIGNEDINT)
	 */
	protected void sequence_IntegerValue(ISerializationContext context, IntegerValue semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     ExpressionOrRegion returns UnaryOperationExpression
	 *     ExpressionAndVariables returns UnaryOperationExpression
	 *     Expression returns UnaryOperationExpression
	 *     ImplicationExpression returns UnaryOperationExpression
	 *     ImplicationExpression.BinaryOperationExpression_1_0 returns UnaryOperationExpression
	 *     DisjunctionExpression returns UnaryOperationExpression
	 *     DisjunctionExpression.BinaryOperationExpression_1_0 returns UnaryOperationExpression
	 *     ConjunctionExpression returns UnaryOperationExpression
	 *     ConjunctionExpression.BinaryOperationExpression_1_0 returns UnaryOperationExpression
	 *     RelationExpression returns UnaryOperationExpression
	 *     RelationExpression.BinaryOperationExpression_1_0 returns UnaryOperationExpression
	 *     AdditionExpression returns UnaryOperationExpression
	 *     AdditionExpression.BinaryOperationExpression_1_0 returns UnaryOperationExpression
	 *     MultiplicationExpression returns UnaryOperationExpression
	 *     MultiplicationExpression.BinaryOperationExpression_1_0 returns UnaryOperationExpression
	 *     NegatedExpression returns UnaryOperationExpression
	 *     BasicExpression returns UnaryOperationExpression
	 *
	 * Constraint:
	 *     ((operator='!' | operator='-') operand=BasicExpression)
	 */
	protected void sequence_NegatedExpression(ISerializationContext context, UnaryOperationExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     ExpressionOrRegion returns NullValue
	 *     ExpressionAndVariables returns NullValue
	 *     Expression returns NullValue
	 *     ImplicationExpression returns NullValue
	 *     ImplicationExpression.BinaryOperationExpression_1_0 returns NullValue
	 *     DisjunctionExpression returns NullValue
	 *     DisjunctionExpression.BinaryOperationExpression_1_0 returns NullValue
	 *     ConjunctionExpression returns NullValue
	 *     ConjunctionExpression.BinaryOperationExpression_1_0 returns NullValue
	 *     RelationExpression returns NullValue
	 *     RelationExpression.BinaryOperationExpression_1_0 returns NullValue
	 *     AdditionExpression returns NullValue
	 *     AdditionExpression.BinaryOperationExpression_1_0 returns NullValue
	 *     MultiplicationExpression returns NullValue
	 *     MultiplicationExpression.BinaryOperationExpression_1_0 returns NullValue
	 *     NegatedExpression returns NullValue
	 *     BasicExpression returns NullValue
	 *     Value returns NullValue
	 *     NullValue returns NullValue
	 *
	 * Constraint:
	 *     {NullValue}
	 */
	protected void sequence_NullValue(ISerializationContext context, NullValue semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     OperationValue returns OperationValue
	 *
	 * Constraint:
	 *     value=[EOperation|ID]
	 */
	protected void sequence_OperationValue(ISerializationContext context, OperationValue semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, ScenarioExpressionsPackage.Literals.OPERATION_VALUE__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ScenarioExpressionsPackage.Literals.OPERATION_VALUE__VALUE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getOperationValueAccess().getValueEOperationIDTerminalRuleCall_0_1(), semanticObject.eGet(ScenarioExpressionsPackage.Literals.OPERATION_VALUE__VALUE, false));
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     ExpressionOrRegion returns StringValue
	 *     ExpressionAndVariables returns StringValue
	 *     Expression returns StringValue
	 *     ImplicationExpression returns StringValue
	 *     ImplicationExpression.BinaryOperationExpression_1_0 returns StringValue
	 *     DisjunctionExpression returns StringValue
	 *     DisjunctionExpression.BinaryOperationExpression_1_0 returns StringValue
	 *     ConjunctionExpression returns StringValue
	 *     ConjunctionExpression.BinaryOperationExpression_1_0 returns StringValue
	 *     RelationExpression returns StringValue
	 *     RelationExpression.BinaryOperationExpression_1_0 returns StringValue
	 *     AdditionExpression returns StringValue
	 *     AdditionExpression.BinaryOperationExpression_1_0 returns StringValue
	 *     MultiplicationExpression returns StringValue
	 *     MultiplicationExpression.BinaryOperationExpression_1_0 returns StringValue
	 *     NegatedExpression returns StringValue
	 *     BasicExpression returns StringValue
	 *     Value returns StringValue
	 *     StringValue returns StringValue
	 *
	 * Constraint:
	 *     value=STRING
	 */
	protected void sequence_StringValue(ISerializationContext context, StringValue semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, ScenarioExpressionsPackage.Literals.STRING_VALUE__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ScenarioExpressionsPackage.Literals.STRING_VALUE__VALUE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     StructuralFeatureValue returns StructuralFeatureValue
	 *
	 * Constraint:
	 *     value=[EStructuralFeature|ID]
	 */
	protected void sequence_StructuralFeatureValue(ISerializationContext context, StructuralFeatureValue semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, ScenarioExpressionsPackage.Literals.STRUCTURAL_FEATURE_VALUE__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ScenarioExpressionsPackage.Literals.STRUCTURAL_FEATURE_VALUE__VALUE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureIDTerminalRuleCall_0_1(), semanticObject.eGet(ScenarioExpressionsPackage.Literals.STRUCTURAL_FEATURE_VALUE__VALUE, false));
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     TimedExpression returns TimedExpression
	 *
	 * Constraint:
	 *     (clock=[ClockDeclaration|ID] (operator='==' | operator='<' | operator='>' | operator='<=' | operator='>=') value=INT)
	 */
	protected void sequence_TimedExpression(ISerializationContext context, TimedExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     ExpressionOrRegion returns TypedVariableDeclaration
	 *     ExpressionAndVariables returns TypedVariableDeclaration
	 *     VariableExpression returns TypedVariableDeclaration
	 *     TypedVariableDeclaration returns TypedVariableDeclaration
	 *
	 * Constraint:
	 *     (type=[EClassifier|ID] name=ID expression=Expression?)
	 */
	protected void sequence_TypedVariableDeclaration(ISerializationContext context, TypedVariableDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     ExpressionOrRegion returns VariableAssignment
	 *     ExpressionAndVariables returns VariableAssignment
	 *     VariableExpression returns VariableAssignment
	 *     VariableAssignment returns VariableAssignment
	 *
	 * Constraint:
	 *     (variable=[VariableDeclaration|ID] expression=Expression)
	 */
	protected void sequence_VariableAssignment(ISerializationContext context, VariableAssignment semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, ScenarioExpressionsPackage.Literals.VARIABLE_ASSIGNMENT__VARIABLE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ScenarioExpressionsPackage.Literals.VARIABLE_ASSIGNMENT__VARIABLE));
			if (transientValues.isValueTransient(semanticObject, ScenarioExpressionsPackage.Literals.VARIABLE_EXPRESSION__EXPRESSION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ScenarioExpressionsPackage.Literals.VARIABLE_EXPRESSION__EXPRESSION));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationIDTerminalRuleCall_0_0_1(), semanticObject.eGet(ScenarioExpressionsPackage.Literals.VARIABLE_ASSIGNMENT__VARIABLE, false));
		feeder.accept(grammarAccess.getVariableAssignmentAccess().getExpressionExpressionParserRuleCall_2_0(), semanticObject.getExpression());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     VariableDeclaration returns VariableDeclaration
	 *
	 * Constraint:
	 *     (name=ID expression=Expression)
	 */
	protected void sequence_VariableDeclaration(ISerializationContext context, VariableDeclaration semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, ScenarioExpressionsPackage.Literals.NAMED_ELEMENT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ScenarioExpressionsPackage.Literals.NAMED_ELEMENT__NAME));
			if (transientValues.isValueTransient(semanticObject, ScenarioExpressionsPackage.Literals.VARIABLE_EXPRESSION__EXPRESSION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ScenarioExpressionsPackage.Literals.VARIABLE_EXPRESSION__EXPRESSION));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getVariableDeclarationAccess().getNameIDTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getVariableDeclarationAccess().getExpressionExpressionParserRuleCall_3_0(), semanticObject.getExpression());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     ExpressionOrRegion returns VariableValue
	 *     ExpressionAndVariables returns VariableValue
	 *     Expression returns VariableValue
	 *     ImplicationExpression returns VariableValue
	 *     ImplicationExpression.BinaryOperationExpression_1_0 returns VariableValue
	 *     DisjunctionExpression returns VariableValue
	 *     DisjunctionExpression.BinaryOperationExpression_1_0 returns VariableValue
	 *     ConjunctionExpression returns VariableValue
	 *     ConjunctionExpression.BinaryOperationExpression_1_0 returns VariableValue
	 *     RelationExpression returns VariableValue
	 *     RelationExpression.BinaryOperationExpression_1_0 returns VariableValue
	 *     AdditionExpression returns VariableValue
	 *     AdditionExpression.BinaryOperationExpression_1_0 returns VariableValue
	 *     MultiplicationExpression returns VariableValue
	 *     MultiplicationExpression.BinaryOperationExpression_1_0 returns VariableValue
	 *     NegatedExpression returns VariableValue
	 *     BasicExpression returns VariableValue
	 *     Value returns VariableValue
	 *     VariableValue returns VariableValue
	 *
	 * Constraint:
	 *     value=[Variable|ID]
	 */
	protected void sequence_VariableValue(ISerializationContext context, VariableValue semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, ScenarioExpressionsPackage.Literals.VARIABLE_VALUE__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ScenarioExpressionsPackage.Literals.VARIABLE_VALUE__VALUE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getVariableValueAccess().getValueVariableIDTerminalRuleCall_0_1(), semanticObject.eGet(ScenarioExpressionsPackage.Literals.VARIABLE_VALUE__VALUE, false));
		feeder.finish();
	}
	
	
}
