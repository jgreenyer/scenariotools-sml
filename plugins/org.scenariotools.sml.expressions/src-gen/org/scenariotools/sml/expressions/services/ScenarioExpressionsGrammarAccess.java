/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.expressions.services;

import com.google.inject.Singleton;
import com.google.inject.Inject;

import java.util.List;

import org.eclipse.xtext.*;
import org.eclipse.xtext.service.GrammarProvider;
import org.eclipse.xtext.service.AbstractElementFinder.*;

import org.eclipse.xtext.common.services.TerminalsGrammarAccess;

@Singleton
public class ScenarioExpressionsGrammarAccess extends AbstractGrammarElementFinder {
	
	
	public class DocumentElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.Document");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cImportsAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final RuleCall cImportsImportParserRuleCall_0_0 = (RuleCall)cImportsAssignment_0.eContents().get(0);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Keyword cDomainKeyword_1_0 = (Keyword)cGroup_1.eContents().get(0);
		private final Assignment cDomainsAssignment_1_1 = (Assignment)cGroup_1.eContents().get(1);
		private final CrossReference cDomainsEPackageCrossReference_1_1_0 = (CrossReference)cDomainsAssignment_1_1.eContents().get(0);
		private final RuleCall cDomainsEPackageIDTerminalRuleCall_1_1_0_1 = (RuleCall)cDomainsEPackageCrossReference_1_1_0.eContents().get(1);
		private final Assignment cExpressionsAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cExpressionsExpressionRegionParserRuleCall_2_0 = (RuleCall)cExpressionsAssignment_2.eContents().get(0);
		
		//Document:
		//	imports+=Import* ('domain' domains+=[EPackage])* expressions+=ExpressionRegion*;
		@Override public ParserRule getRule() { return rule; }

		//imports+=Import* ('domain' domains+=[EPackage])* expressions+=ExpressionRegion*
		public Group getGroup() { return cGroup; }

		//imports+=Import*
		public Assignment getImportsAssignment_0() { return cImportsAssignment_0; }

		//Import
		public RuleCall getImportsImportParserRuleCall_0_0() { return cImportsImportParserRuleCall_0_0; }

		//('domain' domains+=[EPackage])*
		public Group getGroup_1() { return cGroup_1; }

		//'domain'
		public Keyword getDomainKeyword_1_0() { return cDomainKeyword_1_0; }

		//domains+=[EPackage]
		public Assignment getDomainsAssignment_1_1() { return cDomainsAssignment_1_1; }

		//[EPackage]
		public CrossReference getDomainsEPackageCrossReference_1_1_0() { return cDomainsEPackageCrossReference_1_1_0; }

		//ID
		public RuleCall getDomainsEPackageIDTerminalRuleCall_1_1_0_1() { return cDomainsEPackageIDTerminalRuleCall_1_1_0_1; }

		//expressions+=ExpressionRegion*
		public Assignment getExpressionsAssignment_2() { return cExpressionsAssignment_2; }

		//ExpressionRegion
		public RuleCall getExpressionsExpressionRegionParserRuleCall_2_0() { return cExpressionsExpressionRegionParserRuleCall_2_0; }
	}

	public class ImportElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.Import");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cImportKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cImportURIAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cImportURISTRINGTerminalRuleCall_1_0 = (RuleCall)cImportURIAssignment_1.eContents().get(0);
		
		//Import:
		//	'import' importURI=STRING;
		@Override public ParserRule getRule() { return rule; }

		//'import' importURI=STRING
		public Group getGroup() { return cGroup; }

		//'import'
		public Keyword getImportKeyword_0() { return cImportKeyword_0; }

		//importURI=STRING
		public Assignment getImportURIAssignment_1() { return cImportURIAssignment_1; }

		//STRING
		public RuleCall getImportURISTRINGTerminalRuleCall_1_0() { return cImportURISTRINGTerminalRuleCall_1_0; }
	}

	public class ExpressionRegionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.ExpressionRegion");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cExpressionRegionAction_0 = (Action)cGroup.eContents().get(0);
		private final Keyword cLeftCurlyBracketKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Group cGroup_2 = (Group)cGroup.eContents().get(2);
		private final Assignment cExpressionsAssignment_2_0 = (Assignment)cGroup_2.eContents().get(0);
		private final RuleCall cExpressionsExpressionOrRegionParserRuleCall_2_0_0 = (RuleCall)cExpressionsAssignment_2_0.eContents().get(0);
		private final Keyword cSemicolonKeyword_2_1 = (Keyword)cGroup_2.eContents().get(1);
		private final Keyword cRightCurlyBracketKeyword_3 = (Keyword)cGroup.eContents().get(3);
		
		//ExpressionRegion:
		//	{ExpressionRegion} '{' (expressions+=ExpressionOrRegion ';')* '}';
		@Override public ParserRule getRule() { return rule; }

		//{ExpressionRegion} '{' (expressions+=ExpressionOrRegion ';')* '}'
		public Group getGroup() { return cGroup; }

		//{ExpressionRegion}
		public Action getExpressionRegionAction_0() { return cExpressionRegionAction_0; }

		//'{'
		public Keyword getLeftCurlyBracketKeyword_1() { return cLeftCurlyBracketKeyword_1; }

		//(expressions+=ExpressionOrRegion ';')*
		public Group getGroup_2() { return cGroup_2; }

		//expressions+=ExpressionOrRegion
		public Assignment getExpressionsAssignment_2_0() { return cExpressionsAssignment_2_0; }

		//ExpressionOrRegion
		public RuleCall getExpressionsExpressionOrRegionParserRuleCall_2_0_0() { return cExpressionsExpressionOrRegionParserRuleCall_2_0_0; }

		//';'
		public Keyword getSemicolonKeyword_2_1() { return cSemicolonKeyword_2_1; }

		//'}'
		public Keyword getRightCurlyBracketKeyword_3() { return cRightCurlyBracketKeyword_3; }
	}

	public class ExpressionOrRegionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.ExpressionOrRegion");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cExpressionRegionParserRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final RuleCall cExpressionAndVariablesParserRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		
		//ExpressionOrRegion:
		//	ExpressionRegion | ExpressionAndVariables;
		@Override public ParserRule getRule() { return rule; }

		//ExpressionRegion | ExpressionAndVariables
		public Alternatives getAlternatives() { return cAlternatives; }

		//ExpressionRegion
		public RuleCall getExpressionRegionParserRuleCall_0() { return cExpressionRegionParserRuleCall_0; }

		//ExpressionAndVariables
		public RuleCall getExpressionAndVariablesParserRuleCall_1() { return cExpressionAndVariablesParserRuleCall_1; }
	}

	public class ExpressionAndVariablesElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.ExpressionAndVariables");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cVariableExpressionParserRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final RuleCall cExpressionParserRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		
		//ExpressionAndVariables:
		//	VariableExpression | Expression;
		@Override public ParserRule getRule() { return rule; }

		//VariableExpression | Expression
		public Alternatives getAlternatives() { return cAlternatives; }

		//VariableExpression
		public RuleCall getVariableExpressionParserRuleCall_0() { return cVariableExpressionParserRuleCall_0; }

		//Expression
		public RuleCall getExpressionParserRuleCall_1() { return cExpressionParserRuleCall_1; }
	}

	public class VariableExpressionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.VariableExpression");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cTypedVariableDeclarationParserRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final RuleCall cVariableAssignmentParserRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		private final RuleCall cClockDeclarationParserRuleCall_2 = (RuleCall)cAlternatives.eContents().get(2);
		private final RuleCall cClockAssignmentParserRuleCall_3 = (RuleCall)cAlternatives.eContents().get(3);
		
		//VariableExpression:
		//	TypedVariableDeclaration | VariableAssignment | ClockDeclaration | ClockAssignment;
		@Override public ParserRule getRule() { return rule; }

		//TypedVariableDeclaration | VariableAssignment | ClockDeclaration | ClockAssignment
		public Alternatives getAlternatives() { return cAlternatives; }

		//TypedVariableDeclaration
		public RuleCall getTypedVariableDeclarationParserRuleCall_0() { return cTypedVariableDeclarationParserRuleCall_0; }

		//VariableAssignment
		public RuleCall getVariableAssignmentParserRuleCall_1() { return cVariableAssignmentParserRuleCall_1; }

		//ClockDeclaration
		public RuleCall getClockDeclarationParserRuleCall_2() { return cClockDeclarationParserRuleCall_2; }

		//ClockAssignment
		public RuleCall getClockAssignmentParserRuleCall_3() { return cClockAssignmentParserRuleCall_3; }
	}

	public class VariableDeclarationElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.VariableDeclaration");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cVarKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cNameAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cNameIDTerminalRuleCall_1_0 = (RuleCall)cNameAssignment_1.eContents().get(0);
		private final Keyword cEqualsSignKeyword_2 = (Keyword)cGroup.eContents().get(2);
		private final Assignment cExpressionAssignment_3 = (Assignment)cGroup.eContents().get(3);
		private final RuleCall cExpressionExpressionParserRuleCall_3_0 = (RuleCall)cExpressionAssignment_3.eContents().get(0);
		
		//VariableDeclaration:
		//	'var' name=ID '=' expression=Expression;
		@Override public ParserRule getRule() { return rule; }

		//'var' name=ID '=' expression=Expression
		public Group getGroup() { return cGroup; }

		//'var'
		public Keyword getVarKeyword_0() { return cVarKeyword_0; }

		//name=ID
		public Assignment getNameAssignment_1() { return cNameAssignment_1; }

		//ID
		public RuleCall getNameIDTerminalRuleCall_1_0() { return cNameIDTerminalRuleCall_1_0; }

		//'='
		public Keyword getEqualsSignKeyword_2() { return cEqualsSignKeyword_2; }

		//expression=Expression
		public Assignment getExpressionAssignment_3() { return cExpressionAssignment_3; }

		//Expression
		public RuleCall getExpressionExpressionParserRuleCall_3_0() { return cExpressionExpressionParserRuleCall_3_0; }
	}

	public class VariableAssignmentElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.VariableAssignment");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cVariableAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final CrossReference cVariableVariableDeclarationCrossReference_0_0 = (CrossReference)cVariableAssignment_0.eContents().get(0);
		private final RuleCall cVariableVariableDeclarationIDTerminalRuleCall_0_0_1 = (RuleCall)cVariableVariableDeclarationCrossReference_0_0.eContents().get(1);
		private final Keyword cEqualsSignKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cExpressionAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cExpressionExpressionParserRuleCall_2_0 = (RuleCall)cExpressionAssignment_2.eContents().get(0);
		
		//VariableAssignment:
		//	variable=[VariableDeclaration] '=' expression=Expression;
		@Override public ParserRule getRule() { return rule; }

		//variable=[VariableDeclaration] '=' expression=Expression
		public Group getGroup() { return cGroup; }

		//variable=[VariableDeclaration]
		public Assignment getVariableAssignment_0() { return cVariableAssignment_0; }

		//[VariableDeclaration]
		public CrossReference getVariableVariableDeclarationCrossReference_0_0() { return cVariableVariableDeclarationCrossReference_0_0; }

		//ID
		public RuleCall getVariableVariableDeclarationIDTerminalRuleCall_0_0_1() { return cVariableVariableDeclarationIDTerminalRuleCall_0_0_1; }

		//'='
		public Keyword getEqualsSignKeyword_1() { return cEqualsSignKeyword_1; }

		//expression=Expression
		public Assignment getExpressionAssignment_2() { return cExpressionAssignment_2; }

		//Expression
		public RuleCall getExpressionExpressionParserRuleCall_2_0() { return cExpressionExpressionParserRuleCall_2_0; }
	}

	public class TypedVariableDeclarationElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.TypedVariableDeclaration");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cVarKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cTypeAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final CrossReference cTypeEClassifierCrossReference_1_0 = (CrossReference)cTypeAssignment_1.eContents().get(0);
		private final RuleCall cTypeEClassifierIDTerminalRuleCall_1_0_1 = (RuleCall)cTypeEClassifierCrossReference_1_0.eContents().get(1);
		private final Assignment cNameAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cNameIDTerminalRuleCall_2_0 = (RuleCall)cNameAssignment_2.eContents().get(0);
		private final Group cGroup_3 = (Group)cGroup.eContents().get(3);
		private final Keyword cEqualsSignKeyword_3_0 = (Keyword)cGroup_3.eContents().get(0);
		private final Assignment cExpressionAssignment_3_1 = (Assignment)cGroup_3.eContents().get(1);
		private final RuleCall cExpressionExpressionParserRuleCall_3_1_0 = (RuleCall)cExpressionAssignment_3_1.eContents().get(0);
		
		//TypedVariableDeclaration:
		//	'var' type=[EClassifier] name=ID ('=' expression=Expression)?;
		@Override public ParserRule getRule() { return rule; }

		//'var' type=[EClassifier] name=ID ('=' expression=Expression)?
		public Group getGroup() { return cGroup; }

		//'var'
		public Keyword getVarKeyword_0() { return cVarKeyword_0; }

		//type=[EClassifier]
		public Assignment getTypeAssignment_1() { return cTypeAssignment_1; }

		//[EClassifier]
		public CrossReference getTypeEClassifierCrossReference_1_0() { return cTypeEClassifierCrossReference_1_0; }

		//ID
		public RuleCall getTypeEClassifierIDTerminalRuleCall_1_0_1() { return cTypeEClassifierIDTerminalRuleCall_1_0_1; }

		//name=ID
		public Assignment getNameAssignment_2() { return cNameAssignment_2; }

		//ID
		public RuleCall getNameIDTerminalRuleCall_2_0() { return cNameIDTerminalRuleCall_2_0; }

		//('=' expression=Expression)?
		public Group getGroup_3() { return cGroup_3; }

		//'='
		public Keyword getEqualsSignKeyword_3_0() { return cEqualsSignKeyword_3_0; }

		//expression=Expression
		public Assignment getExpressionAssignment_3_1() { return cExpressionAssignment_3_1; }

		//Expression
		public RuleCall getExpressionExpressionParserRuleCall_3_1_0() { return cExpressionExpressionParserRuleCall_3_1_0; }
	}

	public class ClockDeclarationElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.ClockDeclaration");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cClockDeclarationAction_0 = (Action)cGroup.eContents().get(0);
		private final Keyword cClockKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cNameAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cNameIDTerminalRuleCall_2_0 = (RuleCall)cNameAssignment_2.eContents().get(0);
		private final Group cGroup_3 = (Group)cGroup.eContents().get(3);
		private final Keyword cEqualsSignKeyword_3_0 = (Keyword)cGroup_3.eContents().get(0);
		private final Assignment cExpressionAssignment_3_1 = (Assignment)cGroup_3.eContents().get(1);
		private final RuleCall cExpressionIntegerValueParserRuleCall_3_1_0 = (RuleCall)cExpressionAssignment_3_1.eContents().get(0);
		
		//ClockDeclaration:
		//	{ClockDeclaration} 'clock' name=ID ('=' expression=IntegerValue)?;
		@Override public ParserRule getRule() { return rule; }

		//{ClockDeclaration} 'clock' name=ID ('=' expression=IntegerValue)?
		public Group getGroup() { return cGroup; }

		//{ClockDeclaration}
		public Action getClockDeclarationAction_0() { return cClockDeclarationAction_0; }

		//'clock'
		public Keyword getClockKeyword_1() { return cClockKeyword_1; }

		//name=ID
		public Assignment getNameAssignment_2() { return cNameAssignment_2; }

		//ID
		public RuleCall getNameIDTerminalRuleCall_2_0() { return cNameIDTerminalRuleCall_2_0; }

		//('=' expression=IntegerValue)?
		public Group getGroup_3() { return cGroup_3; }

		//'='
		public Keyword getEqualsSignKeyword_3_0() { return cEqualsSignKeyword_3_0; }

		//expression=IntegerValue
		public Assignment getExpressionAssignment_3_1() { return cExpressionAssignment_3_1; }

		//IntegerValue
		public RuleCall getExpressionIntegerValueParserRuleCall_3_1_0() { return cExpressionIntegerValueParserRuleCall_3_1_0; }
	}

	public class ClockElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.Clock");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cNameAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final RuleCall cNameIDTerminalRuleCall_0_0 = (RuleCall)cNameAssignment_0.eContents().get(0);
		private final Assignment cLeftIncludedAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cLeftIncludedBOOLTerminalRuleCall_1_0 = (RuleCall)cLeftIncludedAssignment_1.eContents().get(0);
		private final Assignment cRightIncludedAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cRightIncludedBOOLTerminalRuleCall_2_0 = (RuleCall)cRightIncludedAssignment_2.eContents().get(0);
		private final Assignment cLeftValueAssignment_3 = (Assignment)cGroup.eContents().get(3);
		private final RuleCall cLeftValueINTTerminalRuleCall_3_0 = (RuleCall)cLeftValueAssignment_3.eContents().get(0);
		private final Assignment cRightValueAssignment_4 = (Assignment)cGroup.eContents().get(4);
		private final RuleCall cRightValueINTTerminalRuleCall_4_0 = (RuleCall)cRightValueAssignment_4.eContents().get(0);
		
		//Clock:
		//	name=ID leftIncluded=BOOL rightIncluded=BOOL leftValue=INT rightValue=INT;
		@Override public ParserRule getRule() { return rule; }

		//name=ID leftIncluded=BOOL rightIncluded=BOOL leftValue=INT rightValue=INT
		public Group getGroup() { return cGroup; }

		//name=ID
		public Assignment getNameAssignment_0() { return cNameAssignment_0; }

		//ID
		public RuleCall getNameIDTerminalRuleCall_0_0() { return cNameIDTerminalRuleCall_0_0; }

		//leftIncluded=BOOL
		public Assignment getLeftIncludedAssignment_1() { return cLeftIncludedAssignment_1; }

		//BOOL
		public RuleCall getLeftIncludedBOOLTerminalRuleCall_1_0() { return cLeftIncludedBOOLTerminalRuleCall_1_0; }

		//rightIncluded=BOOL
		public Assignment getRightIncludedAssignment_2() { return cRightIncludedAssignment_2; }

		//BOOL
		public RuleCall getRightIncludedBOOLTerminalRuleCall_2_0() { return cRightIncludedBOOLTerminalRuleCall_2_0; }

		//leftValue=INT
		public Assignment getLeftValueAssignment_3() { return cLeftValueAssignment_3; }

		//INT
		public RuleCall getLeftValueINTTerminalRuleCall_3_0() { return cLeftValueINTTerminalRuleCall_3_0; }

		//rightValue=INT
		public Assignment getRightValueAssignment_4() { return cRightValueAssignment_4; }

		//INT
		public RuleCall getRightValueINTTerminalRuleCall_4_0() { return cRightValueINTTerminalRuleCall_4_0; }
	}

	public class ClockAssignmentElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.ClockAssignment");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cResetClockKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cVariableAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final CrossReference cVariableClockDeclarationCrossReference_1_0 = (CrossReference)cVariableAssignment_1.eContents().get(0);
		private final RuleCall cVariableClockDeclarationIDTerminalRuleCall_1_0_1 = (RuleCall)cVariableClockDeclarationCrossReference_1_0.eContents().get(1);
		private final Group cGroup_2 = (Group)cGroup.eContents().get(2);
		private final Keyword cEqualsSignKeyword_2_0 = (Keyword)cGroup_2.eContents().get(0);
		private final Assignment cExpressionAssignment_2_1 = (Assignment)cGroup_2.eContents().get(1);
		private final RuleCall cExpressionIntegerValueParserRuleCall_2_1_0 = (RuleCall)cExpressionAssignment_2_1.eContents().get(0);
		
		//ClockAssignment:
		//	'reset clock' variable=[ClockDeclaration] ('=' expression=IntegerValue)?;
		@Override public ParserRule getRule() { return rule; }

		//'reset clock' variable=[ClockDeclaration] ('=' expression=IntegerValue)?
		public Group getGroup() { return cGroup; }

		//'reset clock'
		public Keyword getResetClockKeyword_0() { return cResetClockKeyword_0; }

		//variable=[ClockDeclaration]
		public Assignment getVariableAssignment_1() { return cVariableAssignment_1; }

		//[ClockDeclaration]
		public CrossReference getVariableClockDeclarationCrossReference_1_0() { return cVariableClockDeclarationCrossReference_1_0; }

		//ID
		public RuleCall getVariableClockDeclarationIDTerminalRuleCall_1_0_1() { return cVariableClockDeclarationIDTerminalRuleCall_1_0_1; }

		//('=' expression=IntegerValue)?
		public Group getGroup_2() { return cGroup_2; }

		//'='
		public Keyword getEqualsSignKeyword_2_0() { return cEqualsSignKeyword_2_0; }

		//expression=IntegerValue
		public Assignment getExpressionAssignment_2_1() { return cExpressionAssignment_2_1; }

		//IntegerValue
		public RuleCall getExpressionIntegerValueParserRuleCall_2_1_0() { return cExpressionIntegerValueParserRuleCall_2_1_0; }
	}

	public class ExpressionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
		private final RuleCall cImplicationExpressionParserRuleCall = (RuleCall)rule.eContents().get(1);
		
		//Expression:
		//	ImplicationExpression;
		@Override public ParserRule getRule() { return rule; }

		//ImplicationExpression
		public RuleCall getImplicationExpressionParserRuleCall() { return cImplicationExpressionParserRuleCall; }
	}

	public class ImplicationExpressionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.ImplicationExpression");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final RuleCall cDisjunctionExpressionParserRuleCall_0 = (RuleCall)cGroup.eContents().get(0);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Action cBinaryOperationExpressionLeftAction_1_0 = (Action)cGroup_1.eContents().get(0);
		private final Assignment cOperatorAssignment_1_1 = (Assignment)cGroup_1.eContents().get(1);
		private final Keyword cOperatorEqualsSignGreaterThanSignKeyword_1_1_0 = (Keyword)cOperatorAssignment_1_1.eContents().get(0);
		private final Assignment cRightAssignment_1_2 = (Assignment)cGroup_1.eContents().get(2);
		private final RuleCall cRightImplicationExpressionParserRuleCall_1_2_0 = (RuleCall)cRightAssignment_1_2.eContents().get(0);
		
		//ImplicationExpression Expression:
		//	DisjunctionExpression ({BinaryOperationExpression.left=current} operator="=>" right=ImplicationExpression)?;
		@Override public ParserRule getRule() { return rule; }

		//DisjunctionExpression ({BinaryOperationExpression.left=current} operator="=>" right=ImplicationExpression)?
		public Group getGroup() { return cGroup; }

		//DisjunctionExpression
		public RuleCall getDisjunctionExpressionParserRuleCall_0() { return cDisjunctionExpressionParserRuleCall_0; }

		//({BinaryOperationExpression.left=current} operator="=>" right=ImplicationExpression)?
		public Group getGroup_1() { return cGroup_1; }

		//{BinaryOperationExpression.left=current}
		public Action getBinaryOperationExpressionLeftAction_1_0() { return cBinaryOperationExpressionLeftAction_1_0; }

		//operator="=>"
		public Assignment getOperatorAssignment_1_1() { return cOperatorAssignment_1_1; }

		//"=>"
		public Keyword getOperatorEqualsSignGreaterThanSignKeyword_1_1_0() { return cOperatorEqualsSignGreaterThanSignKeyword_1_1_0; }

		//right=ImplicationExpression
		public Assignment getRightAssignment_1_2() { return cRightAssignment_1_2; }

		//ImplicationExpression
		public RuleCall getRightImplicationExpressionParserRuleCall_1_2_0() { return cRightImplicationExpressionParserRuleCall_1_2_0; }
	}

	public class DisjunctionExpressionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.DisjunctionExpression");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final RuleCall cConjunctionExpressionParserRuleCall_0 = (RuleCall)cGroup.eContents().get(0);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Action cBinaryOperationExpressionLeftAction_1_0 = (Action)cGroup_1.eContents().get(0);
		private final Assignment cOperatorAssignment_1_1 = (Assignment)cGroup_1.eContents().get(1);
		private final Keyword cOperatorVerticalLineVerticalLineKeyword_1_1_0 = (Keyword)cOperatorAssignment_1_1.eContents().get(0);
		private final Assignment cRightAssignment_1_2 = (Assignment)cGroup_1.eContents().get(2);
		private final RuleCall cRightDisjunctionExpressionParserRuleCall_1_2_0 = (RuleCall)cRightAssignment_1_2.eContents().get(0);
		
		//DisjunctionExpression Expression:
		//	ConjunctionExpression ({BinaryOperationExpression.left=current} operator="||" right=DisjunctionExpression)?;
		@Override public ParserRule getRule() { return rule; }

		//ConjunctionExpression ({BinaryOperationExpression.left=current} operator="||" right=DisjunctionExpression)?
		public Group getGroup() { return cGroup; }

		//ConjunctionExpression
		public RuleCall getConjunctionExpressionParserRuleCall_0() { return cConjunctionExpressionParserRuleCall_0; }

		//({BinaryOperationExpression.left=current} operator="||" right=DisjunctionExpression)?
		public Group getGroup_1() { return cGroup_1; }

		//{BinaryOperationExpression.left=current}
		public Action getBinaryOperationExpressionLeftAction_1_0() { return cBinaryOperationExpressionLeftAction_1_0; }

		//operator="||"
		public Assignment getOperatorAssignment_1_1() { return cOperatorAssignment_1_1; }

		//"||"
		public Keyword getOperatorVerticalLineVerticalLineKeyword_1_1_0() { return cOperatorVerticalLineVerticalLineKeyword_1_1_0; }

		//right=DisjunctionExpression
		public Assignment getRightAssignment_1_2() { return cRightAssignment_1_2; }

		//DisjunctionExpression
		public RuleCall getRightDisjunctionExpressionParserRuleCall_1_2_0() { return cRightDisjunctionExpressionParserRuleCall_1_2_0; }
	}

	public class ConjunctionExpressionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.ConjunctionExpression");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final RuleCall cRelationExpressionParserRuleCall_0 = (RuleCall)cGroup.eContents().get(0);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Action cBinaryOperationExpressionLeftAction_1_0 = (Action)cGroup_1.eContents().get(0);
		private final Assignment cOperatorAssignment_1_1 = (Assignment)cGroup_1.eContents().get(1);
		private final Keyword cOperatorAmpersandAmpersandKeyword_1_1_0 = (Keyword)cOperatorAssignment_1_1.eContents().get(0);
		private final Assignment cRightAssignment_1_2 = (Assignment)cGroup_1.eContents().get(2);
		private final RuleCall cRightConjunctionExpressionParserRuleCall_1_2_0 = (RuleCall)cRightAssignment_1_2.eContents().get(0);
		
		//ConjunctionExpression Expression:
		//	RelationExpression ({BinaryOperationExpression.left=current} operator="&&" right=ConjunctionExpression)?;
		@Override public ParserRule getRule() { return rule; }

		//RelationExpression ({BinaryOperationExpression.left=current} operator="&&" right=ConjunctionExpression)?
		public Group getGroup() { return cGroup; }

		//RelationExpression
		public RuleCall getRelationExpressionParserRuleCall_0() { return cRelationExpressionParserRuleCall_0; }

		//({BinaryOperationExpression.left=current} operator="&&" right=ConjunctionExpression)?
		public Group getGroup_1() { return cGroup_1; }

		//{BinaryOperationExpression.left=current}
		public Action getBinaryOperationExpressionLeftAction_1_0() { return cBinaryOperationExpressionLeftAction_1_0; }

		//operator="&&"
		public Assignment getOperatorAssignment_1_1() { return cOperatorAssignment_1_1; }

		//"&&"
		public Keyword getOperatorAmpersandAmpersandKeyword_1_1_0() { return cOperatorAmpersandAmpersandKeyword_1_1_0; }

		//right=ConjunctionExpression
		public Assignment getRightAssignment_1_2() { return cRightAssignment_1_2; }

		//ConjunctionExpression
		public RuleCall getRightConjunctionExpressionParserRuleCall_1_2_0() { return cRightConjunctionExpressionParserRuleCall_1_2_0; }
	}

	public class RelationExpressionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.RelationExpression");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final RuleCall cAdditionExpressionParserRuleCall_0 = (RuleCall)cGroup.eContents().get(0);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Action cBinaryOperationExpressionLeftAction_1_0 = (Action)cGroup_1.eContents().get(0);
		private final Assignment cOperatorAssignment_1_1 = (Assignment)cGroup_1.eContents().get(1);
		private final Alternatives cOperatorAlternatives_1_1_0 = (Alternatives)cOperatorAssignment_1_1.eContents().get(0);
		private final Keyword cOperatorEqualsSignEqualsSignKeyword_1_1_0_0 = (Keyword)cOperatorAlternatives_1_1_0.eContents().get(0);
		private final Keyword cOperatorExclamationMarkEqualsSignKeyword_1_1_0_1 = (Keyword)cOperatorAlternatives_1_1_0.eContents().get(1);
		private final Keyword cOperatorLessThanSignKeyword_1_1_0_2 = (Keyword)cOperatorAlternatives_1_1_0.eContents().get(2);
		private final Keyword cOperatorGreaterThanSignKeyword_1_1_0_3 = (Keyword)cOperatorAlternatives_1_1_0.eContents().get(3);
		private final Keyword cOperatorLessThanSignEqualsSignKeyword_1_1_0_4 = (Keyword)cOperatorAlternatives_1_1_0.eContents().get(4);
		private final Keyword cOperatorGreaterThanSignEqualsSignKeyword_1_1_0_5 = (Keyword)cOperatorAlternatives_1_1_0.eContents().get(5);
		private final Assignment cRightAssignment_1_2 = (Assignment)cGroup_1.eContents().get(2);
		private final RuleCall cRightRelationExpressionParserRuleCall_1_2_0 = (RuleCall)cRightAssignment_1_2.eContents().get(0);
		
		//RelationExpression Expression:
		//	AdditionExpression ({BinaryOperationExpression.left=current} operator=('==' | '!=' | '<' | '>' | '<=' | '>=')
		//	right=RelationExpression)?;
		@Override public ParserRule getRule() { return rule; }

		//AdditionExpression ({BinaryOperationExpression.left=current} operator=('==' | '!=' | '<' | '>' | '<=' | '>=')
		//right=RelationExpression)?
		public Group getGroup() { return cGroup; }

		//AdditionExpression
		public RuleCall getAdditionExpressionParserRuleCall_0() { return cAdditionExpressionParserRuleCall_0; }

		//({BinaryOperationExpression.left=current} operator=('==' | '!=' | '<' | '>' | '<=' | '>=') right=RelationExpression)?
		public Group getGroup_1() { return cGroup_1; }

		//{BinaryOperationExpression.left=current}
		public Action getBinaryOperationExpressionLeftAction_1_0() { return cBinaryOperationExpressionLeftAction_1_0; }

		//operator=('==' | '!=' | '<' | '>' | '<=' | '>=')
		public Assignment getOperatorAssignment_1_1() { return cOperatorAssignment_1_1; }

		//('==' | '!=' | '<' | '>' | '<=' | '>=')
		public Alternatives getOperatorAlternatives_1_1_0() { return cOperatorAlternatives_1_1_0; }

		//'=='
		public Keyword getOperatorEqualsSignEqualsSignKeyword_1_1_0_0() { return cOperatorEqualsSignEqualsSignKeyword_1_1_0_0; }

		//'!='
		public Keyword getOperatorExclamationMarkEqualsSignKeyword_1_1_0_1() { return cOperatorExclamationMarkEqualsSignKeyword_1_1_0_1; }

		//'<'
		public Keyword getOperatorLessThanSignKeyword_1_1_0_2() { return cOperatorLessThanSignKeyword_1_1_0_2; }

		//'>'
		public Keyword getOperatorGreaterThanSignKeyword_1_1_0_3() { return cOperatorGreaterThanSignKeyword_1_1_0_3; }

		//'<='
		public Keyword getOperatorLessThanSignEqualsSignKeyword_1_1_0_4() { return cOperatorLessThanSignEqualsSignKeyword_1_1_0_4; }

		//'>='
		public Keyword getOperatorGreaterThanSignEqualsSignKeyword_1_1_0_5() { return cOperatorGreaterThanSignEqualsSignKeyword_1_1_0_5; }

		//right=RelationExpression
		public Assignment getRightAssignment_1_2() { return cRightAssignment_1_2; }

		//RelationExpression
		public RuleCall getRightRelationExpressionParserRuleCall_1_2_0() { return cRightRelationExpressionParserRuleCall_1_2_0; }
	}

	public class TimedExpressionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.TimedExpression");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cClockAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final CrossReference cClockClockDeclarationCrossReference_0_0 = (CrossReference)cClockAssignment_0.eContents().get(0);
		private final RuleCall cClockClockDeclarationIDTerminalRuleCall_0_0_1 = (RuleCall)cClockClockDeclarationCrossReference_0_0.eContents().get(1);
		private final Assignment cOperatorAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final Alternatives cOperatorAlternatives_1_0 = (Alternatives)cOperatorAssignment_1.eContents().get(0);
		private final Keyword cOperatorEqualsSignEqualsSignKeyword_1_0_0 = (Keyword)cOperatorAlternatives_1_0.eContents().get(0);
		private final Keyword cOperatorLessThanSignKeyword_1_0_1 = (Keyword)cOperatorAlternatives_1_0.eContents().get(1);
		private final Keyword cOperatorGreaterThanSignKeyword_1_0_2 = (Keyword)cOperatorAlternatives_1_0.eContents().get(2);
		private final Keyword cOperatorLessThanSignEqualsSignKeyword_1_0_3 = (Keyword)cOperatorAlternatives_1_0.eContents().get(3);
		private final Keyword cOperatorGreaterThanSignEqualsSignKeyword_1_0_4 = (Keyword)cOperatorAlternatives_1_0.eContents().get(4);
		private final Assignment cValueAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cValueINTTerminalRuleCall_2_0 = (RuleCall)cValueAssignment_2.eContents().get(0);
		
		//TimedExpression:
		//	clock=[ClockDeclaration] operator=('==' | '<' | '>' | '<=' | '>=') value=INT;
		@Override public ParserRule getRule() { return rule; }

		//clock=[ClockDeclaration] operator=('==' | '<' | '>' | '<=' | '>=') value=INT
		public Group getGroup() { return cGroup; }

		//clock=[ClockDeclaration]
		public Assignment getClockAssignment_0() { return cClockAssignment_0; }

		//[ClockDeclaration]
		public CrossReference getClockClockDeclarationCrossReference_0_0() { return cClockClockDeclarationCrossReference_0_0; }

		//ID
		public RuleCall getClockClockDeclarationIDTerminalRuleCall_0_0_1() { return cClockClockDeclarationIDTerminalRuleCall_0_0_1; }

		//operator=('==' | '<' | '>' | '<=' | '>=')
		public Assignment getOperatorAssignment_1() { return cOperatorAssignment_1; }

		//('==' | '<' | '>' | '<=' | '>=')
		public Alternatives getOperatorAlternatives_1_0() { return cOperatorAlternatives_1_0; }

		//'=='
		public Keyword getOperatorEqualsSignEqualsSignKeyword_1_0_0() { return cOperatorEqualsSignEqualsSignKeyword_1_0_0; }

		//'<'
		public Keyword getOperatorLessThanSignKeyword_1_0_1() { return cOperatorLessThanSignKeyword_1_0_1; }

		//'>'
		public Keyword getOperatorGreaterThanSignKeyword_1_0_2() { return cOperatorGreaterThanSignKeyword_1_0_2; }

		//'<='
		public Keyword getOperatorLessThanSignEqualsSignKeyword_1_0_3() { return cOperatorLessThanSignEqualsSignKeyword_1_0_3; }

		//'>='
		public Keyword getOperatorGreaterThanSignEqualsSignKeyword_1_0_4() { return cOperatorGreaterThanSignEqualsSignKeyword_1_0_4; }

		//value=INT
		public Assignment getValueAssignment_2() { return cValueAssignment_2; }

		//INT
		public RuleCall getValueINTTerminalRuleCall_2_0() { return cValueINTTerminalRuleCall_2_0; }
	}

	public class AdditionExpressionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.AdditionExpression");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final RuleCall cMultiplicationExpressionParserRuleCall_0 = (RuleCall)cGroup.eContents().get(0);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Action cBinaryOperationExpressionLeftAction_1_0 = (Action)cGroup_1.eContents().get(0);
		private final Assignment cOperatorAssignment_1_1 = (Assignment)cGroup_1.eContents().get(1);
		private final Alternatives cOperatorAlternatives_1_1_0 = (Alternatives)cOperatorAssignment_1_1.eContents().get(0);
		private final Keyword cOperatorPlusSignKeyword_1_1_0_0 = (Keyword)cOperatorAlternatives_1_1_0.eContents().get(0);
		private final Keyword cOperatorHyphenMinusKeyword_1_1_0_1 = (Keyword)cOperatorAlternatives_1_1_0.eContents().get(1);
		private final Assignment cRightAssignment_1_2 = (Assignment)cGroup_1.eContents().get(2);
		private final RuleCall cRightAdditionExpressionParserRuleCall_1_2_0 = (RuleCall)cRightAssignment_1_2.eContents().get(0);
		
		//AdditionExpression Expression:
		//	MultiplicationExpression ({BinaryOperationExpression.left=current} operator=('+' | '-') right=AdditionExpression)?;
		@Override public ParserRule getRule() { return rule; }

		//MultiplicationExpression ({BinaryOperationExpression.left=current} operator=('+' | '-') right=AdditionExpression)?
		public Group getGroup() { return cGroup; }

		//MultiplicationExpression
		public RuleCall getMultiplicationExpressionParserRuleCall_0() { return cMultiplicationExpressionParserRuleCall_0; }

		//({BinaryOperationExpression.left=current} operator=('+' | '-') right=AdditionExpression)?
		public Group getGroup_1() { return cGroup_1; }

		//{BinaryOperationExpression.left=current}
		public Action getBinaryOperationExpressionLeftAction_1_0() { return cBinaryOperationExpressionLeftAction_1_0; }

		//operator=('+' | '-')
		public Assignment getOperatorAssignment_1_1() { return cOperatorAssignment_1_1; }

		//('+' | '-')
		public Alternatives getOperatorAlternatives_1_1_0() { return cOperatorAlternatives_1_1_0; }

		//'+'
		public Keyword getOperatorPlusSignKeyword_1_1_0_0() { return cOperatorPlusSignKeyword_1_1_0_0; }

		//'-'
		public Keyword getOperatorHyphenMinusKeyword_1_1_0_1() { return cOperatorHyphenMinusKeyword_1_1_0_1; }

		//right=AdditionExpression
		public Assignment getRightAssignment_1_2() { return cRightAssignment_1_2; }

		//AdditionExpression
		public RuleCall getRightAdditionExpressionParserRuleCall_1_2_0() { return cRightAdditionExpressionParserRuleCall_1_2_0; }
	}

	public class MultiplicationExpressionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.MultiplicationExpression");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final RuleCall cNegatedExpressionParserRuleCall_0 = (RuleCall)cGroup.eContents().get(0);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Action cBinaryOperationExpressionLeftAction_1_0 = (Action)cGroup_1.eContents().get(0);
		private final Assignment cOperatorAssignment_1_1 = (Assignment)cGroup_1.eContents().get(1);
		private final Alternatives cOperatorAlternatives_1_1_0 = (Alternatives)cOperatorAssignment_1_1.eContents().get(0);
		private final Keyword cOperatorAsteriskKeyword_1_1_0_0 = (Keyword)cOperatorAlternatives_1_1_0.eContents().get(0);
		private final Keyword cOperatorSolidusKeyword_1_1_0_1 = (Keyword)cOperatorAlternatives_1_1_0.eContents().get(1);
		private final Assignment cRightAssignment_1_2 = (Assignment)cGroup_1.eContents().get(2);
		private final RuleCall cRightMultiplicationExpressionParserRuleCall_1_2_0 = (RuleCall)cRightAssignment_1_2.eContents().get(0);
		
		//MultiplicationExpression Expression:
		//	NegatedExpression ({BinaryOperationExpression.left=current} operator=('*' | '/') right=MultiplicationExpression)?;
		@Override public ParserRule getRule() { return rule; }

		//NegatedExpression ({BinaryOperationExpression.left=current} operator=('*' | '/') right=MultiplicationExpression)?
		public Group getGroup() { return cGroup; }

		//NegatedExpression
		public RuleCall getNegatedExpressionParserRuleCall_0() { return cNegatedExpressionParserRuleCall_0; }

		//({BinaryOperationExpression.left=current} operator=('*' | '/') right=MultiplicationExpression)?
		public Group getGroup_1() { return cGroup_1; }

		//{BinaryOperationExpression.left=current}
		public Action getBinaryOperationExpressionLeftAction_1_0() { return cBinaryOperationExpressionLeftAction_1_0; }

		//operator=('*' | '/')
		public Assignment getOperatorAssignment_1_1() { return cOperatorAssignment_1_1; }

		//('*' | '/')
		public Alternatives getOperatorAlternatives_1_1_0() { return cOperatorAlternatives_1_1_0; }

		//'*'
		public Keyword getOperatorAsteriskKeyword_1_1_0_0() { return cOperatorAsteriskKeyword_1_1_0_0; }

		//'/'
		public Keyword getOperatorSolidusKeyword_1_1_0_1() { return cOperatorSolidusKeyword_1_1_0_1; }

		//right=MultiplicationExpression
		public Assignment getRightAssignment_1_2() { return cRightAssignment_1_2; }

		//MultiplicationExpression
		public RuleCall getRightMultiplicationExpressionParserRuleCall_1_2_0() { return cRightMultiplicationExpressionParserRuleCall_1_2_0; }
	}

	public class NegatedExpressionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.NegatedExpression");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final Group cGroup_0 = (Group)cAlternatives.eContents().get(0);
		private final Action cUnaryOperationExpressionAction_0_0 = (Action)cGroup_0.eContents().get(0);
		private final Assignment cOperatorAssignment_0_1 = (Assignment)cGroup_0.eContents().get(1);
		private final Alternatives cOperatorAlternatives_0_1_0 = (Alternatives)cOperatorAssignment_0_1.eContents().get(0);
		private final Keyword cOperatorExclamationMarkKeyword_0_1_0_0 = (Keyword)cOperatorAlternatives_0_1_0.eContents().get(0);
		private final Keyword cOperatorHyphenMinusKeyword_0_1_0_1 = (Keyword)cOperatorAlternatives_0_1_0.eContents().get(1);
		private final Assignment cOperandAssignment_0_2 = (Assignment)cGroup_0.eContents().get(2);
		private final RuleCall cOperandBasicExpressionParserRuleCall_0_2_0 = (RuleCall)cOperandAssignment_0_2.eContents().get(0);
		private final RuleCall cBasicExpressionParserRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		
		//NegatedExpression Expression:
		//	{UnaryOperationExpression} => operator=('!' | '-') operand=BasicExpression | BasicExpression;
		@Override public ParserRule getRule() { return rule; }

		//{UnaryOperationExpression} => operator=('!' | '-') operand=BasicExpression | BasicExpression
		public Alternatives getAlternatives() { return cAlternatives; }

		//{UnaryOperationExpression} => operator=('!' | '-') operand=BasicExpression
		public Group getGroup_0() { return cGroup_0; }

		//{UnaryOperationExpression}
		public Action getUnaryOperationExpressionAction_0_0() { return cUnaryOperationExpressionAction_0_0; }

		//=> operator=('!' | '-')
		public Assignment getOperatorAssignment_0_1() { return cOperatorAssignment_0_1; }

		//('!' | '-')
		public Alternatives getOperatorAlternatives_0_1_0() { return cOperatorAlternatives_0_1_0; }

		//'!'
		public Keyword getOperatorExclamationMarkKeyword_0_1_0_0() { return cOperatorExclamationMarkKeyword_0_1_0_0; }

		//'-'
		public Keyword getOperatorHyphenMinusKeyword_0_1_0_1() { return cOperatorHyphenMinusKeyword_0_1_0_1; }

		//operand=BasicExpression
		public Assignment getOperandAssignment_0_2() { return cOperandAssignment_0_2; }

		//BasicExpression
		public RuleCall getOperandBasicExpressionParserRuleCall_0_2_0() { return cOperandBasicExpressionParserRuleCall_0_2_0; }

		//BasicExpression
		public RuleCall getBasicExpressionParserRuleCall_1() { return cBasicExpressionParserRuleCall_1; }
	}

	public class BasicExpressionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.BasicExpression");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cValueParserRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final Group cGroup_1 = (Group)cAlternatives.eContents().get(1);
		private final Keyword cLeftParenthesisKeyword_1_0 = (Keyword)cGroup_1.eContents().get(0);
		private final RuleCall cExpressionParserRuleCall_1_1 = (RuleCall)cGroup_1.eContents().get(1);
		private final Keyword cRightParenthesisKeyword_1_2 = (Keyword)cGroup_1.eContents().get(2);
		
		//BasicExpression Expression:
		//	Value | '(' Expression ')';
		@Override public ParserRule getRule() { return rule; }

		//Value | '(' Expression ')'
		public Alternatives getAlternatives() { return cAlternatives; }

		//Value
		public RuleCall getValueParserRuleCall_0() { return cValueParserRuleCall_0; }

		//'(' Expression ')'
		public Group getGroup_1() { return cGroup_1; }

		//'('
		public Keyword getLeftParenthesisKeyword_1_0() { return cLeftParenthesisKeyword_1_0; }

		//Expression
		public RuleCall getExpressionParserRuleCall_1_1() { return cExpressionParserRuleCall_1_1; }

		//')'
		public Keyword getRightParenthesisKeyword_1_2() { return cRightParenthesisKeyword_1_2; }
	}

	public class ValueElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.Value");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cIntegerValueParserRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final RuleCall cBooleanValueParserRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		private final RuleCall cStringValueParserRuleCall_2 = (RuleCall)cAlternatives.eContents().get(2);
		private final RuleCall cEnumValueParserRuleCall_3 = (RuleCall)cAlternatives.eContents().get(3);
		private final RuleCall cNullValueParserRuleCall_4 = (RuleCall)cAlternatives.eContents().get(4);
		private final RuleCall cVariableValueParserRuleCall_5 = (RuleCall)cAlternatives.eContents().get(5);
		private final RuleCall cFeatureAccessParserRuleCall_6 = (RuleCall)cAlternatives.eContents().get(6);
		
		//Value:
		//	IntegerValue | BooleanValue | StringValue | EnumValue | NullValue | VariableValue | FeatureAccess;
		@Override public ParserRule getRule() { return rule; }

		//IntegerValue | BooleanValue | StringValue | EnumValue | NullValue | VariableValue | FeatureAccess
		public Alternatives getAlternatives() { return cAlternatives; }

		//IntegerValue
		public RuleCall getIntegerValueParserRuleCall_0() { return cIntegerValueParserRuleCall_0; }

		//BooleanValue
		public RuleCall getBooleanValueParserRuleCall_1() { return cBooleanValueParserRuleCall_1; }

		//StringValue
		public RuleCall getStringValueParserRuleCall_2() { return cStringValueParserRuleCall_2; }

		//EnumValue
		public RuleCall getEnumValueParserRuleCall_3() { return cEnumValueParserRuleCall_3; }

		//NullValue
		public RuleCall getNullValueParserRuleCall_4() { return cNullValueParserRuleCall_4; }

		//VariableValue
		public RuleCall getVariableValueParserRuleCall_5() { return cVariableValueParserRuleCall_5; }

		//FeatureAccess
		public RuleCall getFeatureAccessParserRuleCall_6() { return cFeatureAccessParserRuleCall_6; }
	}

	public class IntegerValueElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.IntegerValue");
		private final Assignment cValueAssignment = (Assignment)rule.eContents().get(1);
		private final Alternatives cValueAlternatives_0 = (Alternatives)cValueAssignment.eContents().get(0);
		private final RuleCall cValueINTTerminalRuleCall_0_0 = (RuleCall)cValueAlternatives_0.eContents().get(0);
		private final RuleCall cValueSIGNEDINTTerminalRuleCall_0_1 = (RuleCall)cValueAlternatives_0.eContents().get(1);
		
		//IntegerValue:
		//	value=(INT | SIGNEDINT);
		@Override public ParserRule getRule() { return rule; }

		//value=(INT | SIGNEDINT)
		public Assignment getValueAssignment() { return cValueAssignment; }

		//(INT | SIGNEDINT)
		public Alternatives getValueAlternatives_0() { return cValueAlternatives_0; }

		//INT
		public RuleCall getValueINTTerminalRuleCall_0_0() { return cValueINTTerminalRuleCall_0_0; }

		//SIGNEDINT
		public RuleCall getValueSIGNEDINTTerminalRuleCall_0_1() { return cValueSIGNEDINTTerminalRuleCall_0_1; }
	}

	public class BooleanValueElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.BooleanValue");
		private final Assignment cValueAssignment = (Assignment)rule.eContents().get(1);
		private final RuleCall cValueBOOLTerminalRuleCall_0 = (RuleCall)cValueAssignment.eContents().get(0);
		
		//BooleanValue:
		//	value=BOOL;
		@Override public ParserRule getRule() { return rule; }

		//value=BOOL
		public Assignment getValueAssignment() { return cValueAssignment; }

		//BOOL
		public RuleCall getValueBOOLTerminalRuleCall_0() { return cValueBOOLTerminalRuleCall_0; }
	}

	public class StringValueElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.StringValue");
		private final Assignment cValueAssignment = (Assignment)rule.eContents().get(1);
		private final RuleCall cValueSTRINGTerminalRuleCall_0 = (RuleCall)cValueAssignment.eContents().get(0);
		
		//StringValue:
		//	value=STRING;
		@Override public ParserRule getRule() { return rule; }

		//value=STRING
		public Assignment getValueAssignment() { return cValueAssignment; }

		//STRING
		public RuleCall getValueSTRINGTerminalRuleCall_0() { return cValueSTRINGTerminalRuleCall_0; }
	}

	public class EnumValueElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.EnumValue");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cTypeAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final CrossReference cTypeEEnumCrossReference_0_0 = (CrossReference)cTypeAssignment_0.eContents().get(0);
		private final RuleCall cTypeEEnumIDTerminalRuleCall_0_0_1 = (RuleCall)cTypeEEnumCrossReference_0_0.eContents().get(1);
		private final Keyword cColonKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cValueAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final CrossReference cValueEEnumLiteralCrossReference_2_0 = (CrossReference)cValueAssignment_2.eContents().get(0);
		private final RuleCall cValueEEnumLiteralIDTerminalRuleCall_2_0_1 = (RuleCall)cValueEEnumLiteralCrossReference_2_0.eContents().get(1);
		
		//EnumValue:
		//	type=[EEnum] ':' value=[EEnumLiteral];
		@Override public ParserRule getRule() { return rule; }

		//type=[EEnum] ':' value=[EEnumLiteral]
		public Group getGroup() { return cGroup; }

		//type=[EEnum]
		public Assignment getTypeAssignment_0() { return cTypeAssignment_0; }

		//[EEnum]
		public CrossReference getTypeEEnumCrossReference_0_0() { return cTypeEEnumCrossReference_0_0; }

		//ID
		public RuleCall getTypeEEnumIDTerminalRuleCall_0_0_1() { return cTypeEEnumIDTerminalRuleCall_0_0_1; }

		//':'
		public Keyword getColonKeyword_1() { return cColonKeyword_1; }

		//value=[EEnumLiteral]
		public Assignment getValueAssignment_2() { return cValueAssignment_2; }

		//[EEnumLiteral]
		public CrossReference getValueEEnumLiteralCrossReference_2_0() { return cValueEEnumLiteralCrossReference_2_0; }

		//ID
		public RuleCall getValueEEnumLiteralIDTerminalRuleCall_2_0_1() { return cValueEEnumLiteralIDTerminalRuleCall_2_0_1; }
	}

	public class NullValueElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.NullValue");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cNullValueAction_0 = (Action)cGroup.eContents().get(0);
		private final Keyword cNullKeyword_1 = (Keyword)cGroup.eContents().get(1);
		
		//NullValue:
		//	{NullValue} 'null';
		@Override public ParserRule getRule() { return rule; }

		//{NullValue} 'null'
		public Group getGroup() { return cGroup; }

		//{NullValue}
		public Action getNullValueAction_0() { return cNullValueAction_0; }

		//'null'
		public Keyword getNullKeyword_1() { return cNullKeyword_1; }
	}

	public class VariableValueElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.VariableValue");
		private final Assignment cValueAssignment = (Assignment)rule.eContents().get(1);
		private final CrossReference cValueVariableCrossReference_0 = (CrossReference)cValueAssignment.eContents().get(0);
		private final RuleCall cValueVariableIDTerminalRuleCall_0_1 = (RuleCall)cValueVariableCrossReference_0.eContents().get(1);
		
		//VariableValue:
		//	value=[Variable];
		@Override public ParserRule getRule() { return rule; }

		//value=[Variable]
		public Assignment getValueAssignment() { return cValueAssignment; }

		//[Variable]
		public CrossReference getValueVariableCrossReference_0() { return cValueVariableCrossReference_0; }

		//ID
		public RuleCall getValueVariableIDTerminalRuleCall_0_1() { return cValueVariableIDTerminalRuleCall_0_1; }
	}

	public class CollectionAccessElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.CollectionAccess");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cCollectionOperationAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final RuleCall cCollectionOperationCollectionOperationEnumRuleCall_0_0 = (RuleCall)cCollectionOperationAssignment_0.eContents().get(0);
		private final Keyword cLeftParenthesisKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cParameterAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cParameterExpressionParserRuleCall_2_0 = (RuleCall)cParameterAssignment_2.eContents().get(0);
		private final Keyword cRightParenthesisKeyword_3 = (Keyword)cGroup.eContents().get(3);
		
		//CollectionAccess:
		//	collectionOperation=CollectionOperation '(' parameter=Expression? ')';
		@Override public ParserRule getRule() { return rule; }

		//collectionOperation=CollectionOperation '(' parameter=Expression? ')'
		public Group getGroup() { return cGroup; }

		//collectionOperation=CollectionOperation
		public Assignment getCollectionOperationAssignment_0() { return cCollectionOperationAssignment_0; }

		//CollectionOperation
		public RuleCall getCollectionOperationCollectionOperationEnumRuleCall_0_0() { return cCollectionOperationCollectionOperationEnumRuleCall_0_0; }

		//'('
		public Keyword getLeftParenthesisKeyword_1() { return cLeftParenthesisKeyword_1; }

		//parameter=Expression?
		public Assignment getParameterAssignment_2() { return cParameterAssignment_2; }

		//Expression
		public RuleCall getParameterExpressionParserRuleCall_2_0() { return cParameterExpressionParserRuleCall_2_0; }

		//')'
		public Keyword getRightParenthesisKeyword_3() { return cRightParenthesisKeyword_3; }
	}

	public class FeatureAccessElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.FeatureAccess");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cTargetAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final CrossReference cTargetEObjectCrossReference_0_0 = (CrossReference)cTargetAssignment_0.eContents().get(0);
		private final RuleCall cTargetEObjectIDTerminalRuleCall_0_0_1 = (RuleCall)cTargetEObjectCrossReference_0_0.eContents().get(1);
		private final Keyword cFullStopKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Alternatives cAlternatives_2 = (Alternatives)cGroup.eContents().get(2);
		private final Group cGroup_2_0 = (Group)cAlternatives_2.eContents().get(0);
		private final Assignment cValueAssignment_2_0_0 = (Assignment)cGroup_2_0.eContents().get(0);
		private final RuleCall cValueStructuralFeatureValueParserRuleCall_2_0_0_0 = (RuleCall)cValueAssignment_2_0_0.eContents().get(0);
		private final Group cGroup_2_0_1 = (Group)cGroup_2_0.eContents().get(1);
		private final Keyword cFullStopKeyword_2_0_1_0 = (Keyword)cGroup_2_0_1.eContents().get(0);
		private final Assignment cCollectionAccessAssignment_2_0_1_1 = (Assignment)cGroup_2_0_1.eContents().get(1);
		private final RuleCall cCollectionAccessCollectionAccessParserRuleCall_2_0_1_1_0 = (RuleCall)cCollectionAccessAssignment_2_0_1_1.eContents().get(0);
		private final Group cGroup_2_1 = (Group)cAlternatives_2.eContents().get(1);
		private final Assignment cValueAssignment_2_1_0 = (Assignment)cGroup_2_1.eContents().get(0);
		private final RuleCall cValueOperationValueParserRuleCall_2_1_0_0 = (RuleCall)cValueAssignment_2_1_0.eContents().get(0);
		private final Keyword cLeftParenthesisKeyword_2_1_1 = (Keyword)cGroup_2_1.eContents().get(1);
		private final Group cGroup_2_1_2 = (Group)cGroup_2_1.eContents().get(2);
		private final Assignment cParametersAssignment_2_1_2_0 = (Assignment)cGroup_2_1_2.eContents().get(0);
		private final RuleCall cParametersExpressionParserRuleCall_2_1_2_0_0 = (RuleCall)cParametersAssignment_2_1_2_0.eContents().get(0);
		private final Group cGroup_2_1_2_1 = (Group)cGroup_2_1_2.eContents().get(1);
		private final Keyword cCommaKeyword_2_1_2_1_0 = (Keyword)cGroup_2_1_2_1.eContents().get(0);
		private final Assignment cParametersAssignment_2_1_2_1_1 = (Assignment)cGroup_2_1_2_1.eContents().get(1);
		private final RuleCall cParametersExpressionParserRuleCall_2_1_2_1_1_0 = (RuleCall)cParametersAssignment_2_1_2_1_1.eContents().get(0);
		private final Keyword cRightParenthesisKeyword_2_1_3 = (Keyword)cGroup_2_1.eContents().get(3);
		
		//FeatureAccess:
		//	target=[EObject] '.' (value=StructuralFeatureValue ('.' collectionAccess=CollectionAccess)? | value=OperationValue
		//	'(' (parameters+=Expression (',' parameters+=Expression)*)? ')');
		@Override public ParserRule getRule() { return rule; }

		//target=[EObject] '.' (value=StructuralFeatureValue ('.' collectionAccess=CollectionAccess)? | value=OperationValue '('
		//(parameters+=Expression (',' parameters+=Expression)*)? ')')
		public Group getGroup() { return cGroup; }

		//target=[EObject]
		public Assignment getTargetAssignment_0() { return cTargetAssignment_0; }

		//[EObject]
		public CrossReference getTargetEObjectCrossReference_0_0() { return cTargetEObjectCrossReference_0_0; }

		//ID
		public RuleCall getTargetEObjectIDTerminalRuleCall_0_0_1() { return cTargetEObjectIDTerminalRuleCall_0_0_1; }

		//'.'
		public Keyword getFullStopKeyword_1() { return cFullStopKeyword_1; }

		//value=StructuralFeatureValue ('.' collectionAccess=CollectionAccess)? | value=OperationValue '(' (parameters+=Expression
		//(',' parameters+=Expression)*)? ')'
		public Alternatives getAlternatives_2() { return cAlternatives_2; }

		//value=StructuralFeatureValue ('.' collectionAccess=CollectionAccess)?
		public Group getGroup_2_0() { return cGroup_2_0; }

		//value=StructuralFeatureValue
		public Assignment getValueAssignment_2_0_0() { return cValueAssignment_2_0_0; }

		//StructuralFeatureValue
		public RuleCall getValueStructuralFeatureValueParserRuleCall_2_0_0_0() { return cValueStructuralFeatureValueParserRuleCall_2_0_0_0; }

		//('.' collectionAccess=CollectionAccess)?
		public Group getGroup_2_0_1() { return cGroup_2_0_1; }

		//'.'
		public Keyword getFullStopKeyword_2_0_1_0() { return cFullStopKeyword_2_0_1_0; }

		//collectionAccess=CollectionAccess
		public Assignment getCollectionAccessAssignment_2_0_1_1() { return cCollectionAccessAssignment_2_0_1_1; }

		//CollectionAccess
		public RuleCall getCollectionAccessCollectionAccessParserRuleCall_2_0_1_1_0() { return cCollectionAccessCollectionAccessParserRuleCall_2_0_1_1_0; }

		//value=OperationValue '(' (parameters+=Expression (',' parameters+=Expression)*)? ')'
		public Group getGroup_2_1() { return cGroup_2_1; }

		//value=OperationValue
		public Assignment getValueAssignment_2_1_0() { return cValueAssignment_2_1_0; }

		//OperationValue
		public RuleCall getValueOperationValueParserRuleCall_2_1_0_0() { return cValueOperationValueParserRuleCall_2_1_0_0; }

		//'('
		public Keyword getLeftParenthesisKeyword_2_1_1() { return cLeftParenthesisKeyword_2_1_1; }

		//(parameters+=Expression (',' parameters+=Expression)*)?
		public Group getGroup_2_1_2() { return cGroup_2_1_2; }

		//parameters+=Expression
		public Assignment getParametersAssignment_2_1_2_0() { return cParametersAssignment_2_1_2_0; }

		//Expression
		public RuleCall getParametersExpressionParserRuleCall_2_1_2_0_0() { return cParametersExpressionParserRuleCall_2_1_2_0_0; }

		//(',' parameters+=Expression)*
		public Group getGroup_2_1_2_1() { return cGroup_2_1_2_1; }

		//','
		public Keyword getCommaKeyword_2_1_2_1_0() { return cCommaKeyword_2_1_2_1_0; }

		//parameters+=Expression
		public Assignment getParametersAssignment_2_1_2_1_1() { return cParametersAssignment_2_1_2_1_1; }

		//Expression
		public RuleCall getParametersExpressionParserRuleCall_2_1_2_1_1_0() { return cParametersExpressionParserRuleCall_2_1_2_1_1_0; }

		//')'
		public Keyword getRightParenthesisKeyword_2_1_3() { return cRightParenthesisKeyword_2_1_3; }
	}

	public class StructuralFeatureValueElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.StructuralFeatureValue");
		private final Assignment cValueAssignment = (Assignment)rule.eContents().get(1);
		private final CrossReference cValueEStructuralFeatureCrossReference_0 = (CrossReference)cValueAssignment.eContents().get(0);
		private final RuleCall cValueEStructuralFeatureIDTerminalRuleCall_0_1 = (RuleCall)cValueEStructuralFeatureCrossReference_0.eContents().get(1);
		
		//StructuralFeatureValue:
		//	value=[EStructuralFeature];
		@Override public ParserRule getRule() { return rule; }

		//value=[EStructuralFeature]
		public Assignment getValueAssignment() { return cValueAssignment; }

		//[EStructuralFeature]
		public CrossReference getValueEStructuralFeatureCrossReference_0() { return cValueEStructuralFeatureCrossReference_0; }

		//ID
		public RuleCall getValueEStructuralFeatureIDTerminalRuleCall_0_1() { return cValueEStructuralFeatureIDTerminalRuleCall_0_1; }
	}

	public class OperationValueElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.OperationValue");
		private final Assignment cValueAssignment = (Assignment)rule.eContents().get(1);
		private final CrossReference cValueEOperationCrossReference_0 = (CrossReference)cValueAssignment.eContents().get(0);
		private final RuleCall cValueEOperationIDTerminalRuleCall_0_1 = (RuleCall)cValueEOperationCrossReference_0.eContents().get(1);
		
		//OperationValue:
		//	value=[EOperation];
		@Override public ParserRule getRule() { return rule; }

		//value=[EOperation]
		public Assignment getValueAssignment() { return cValueAssignment; }

		//[EOperation]
		public CrossReference getValueEOperationCrossReference_0() { return cValueEOperationCrossReference_0; }

		//ID
		public RuleCall getValueEOperationIDTerminalRuleCall_0_1() { return cValueEOperationIDTerminalRuleCall_0_1; }
	}
	
	
	public class CollectionOperationElements extends AbstractEnumRuleElementFinder {
		private final EnumRule rule = (EnumRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.CollectionOperation");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final EnumLiteralDeclaration cAnyEnumLiteralDeclaration_0 = (EnumLiteralDeclaration)cAlternatives.eContents().get(0);
		private final Keyword cAnyAnyKeyword_0_0 = (Keyword)cAnyEnumLiteralDeclaration_0.eContents().get(0);
		private final EnumLiteralDeclaration cContainsEnumLiteralDeclaration_1 = (EnumLiteralDeclaration)cAlternatives.eContents().get(1);
		private final Keyword cContainsContainsKeyword_1_0 = (Keyword)cContainsEnumLiteralDeclaration_1.eContents().get(0);
		private final EnumLiteralDeclaration cContainsAllEnumLiteralDeclaration_2 = (EnumLiteralDeclaration)cAlternatives.eContents().get(2);
		private final Keyword cContainsAllContainsAllKeyword_2_0 = (Keyword)cContainsAllEnumLiteralDeclaration_2.eContents().get(0);
		private final EnumLiteralDeclaration cFirstEnumLiteralDeclaration_3 = (EnumLiteralDeclaration)cAlternatives.eContents().get(3);
		private final Keyword cFirstFirstKeyword_3_0 = (Keyword)cFirstEnumLiteralDeclaration_3.eContents().get(0);
		private final EnumLiteralDeclaration cGetEnumLiteralDeclaration_4 = (EnumLiteralDeclaration)cAlternatives.eContents().get(4);
		private final Keyword cGetGetKeyword_4_0 = (Keyword)cGetEnumLiteralDeclaration_4.eContents().get(0);
		private final EnumLiteralDeclaration cIsEmptyEnumLiteralDeclaration_5 = (EnumLiteralDeclaration)cAlternatives.eContents().get(5);
		private final Keyword cIsEmptyIsEmptyKeyword_5_0 = (Keyword)cIsEmptyEnumLiteralDeclaration_5.eContents().get(0);
		private final EnumLiteralDeclaration cLastEnumLiteralDeclaration_6 = (EnumLiteralDeclaration)cAlternatives.eContents().get(6);
		private final Keyword cLastLastKeyword_6_0 = (Keyword)cLastEnumLiteralDeclaration_6.eContents().get(0);
		private final EnumLiteralDeclaration cSizeEnumLiteralDeclaration_7 = (EnumLiteralDeclaration)cAlternatives.eContents().get(7);
		private final Keyword cSizeSizeKeyword_7_0 = (Keyword)cSizeEnumLiteralDeclaration_7.eContents().get(0);
		
		//enum CollectionOperation:
		//	any | contains | containsAll | first | get | isEmpty | last | size;
		public EnumRule getRule() { return rule; }

		//any | contains | containsAll | first | get | isEmpty | last | size
		public Alternatives getAlternatives() { return cAlternatives; }

		//any
		public EnumLiteralDeclaration getAnyEnumLiteralDeclaration_0() { return cAnyEnumLiteralDeclaration_0; }

		//"any"
		public Keyword getAnyAnyKeyword_0_0() { return cAnyAnyKeyword_0_0; }

		//contains
		public EnumLiteralDeclaration getContainsEnumLiteralDeclaration_1() { return cContainsEnumLiteralDeclaration_1; }

		//"contains"
		public Keyword getContainsContainsKeyword_1_0() { return cContainsContainsKeyword_1_0; }

		//containsAll
		public EnumLiteralDeclaration getContainsAllEnumLiteralDeclaration_2() { return cContainsAllEnumLiteralDeclaration_2; }

		//"containsAll"
		public Keyword getContainsAllContainsAllKeyword_2_0() { return cContainsAllContainsAllKeyword_2_0; }

		//first
		public EnumLiteralDeclaration getFirstEnumLiteralDeclaration_3() { return cFirstEnumLiteralDeclaration_3; }

		//"first"
		public Keyword getFirstFirstKeyword_3_0() { return cFirstFirstKeyword_3_0; }

		//get
		public EnumLiteralDeclaration getGetEnumLiteralDeclaration_4() { return cGetEnumLiteralDeclaration_4; }

		//"get"
		public Keyword getGetGetKeyword_4_0() { return cGetGetKeyword_4_0; }

		//isEmpty
		public EnumLiteralDeclaration getIsEmptyEnumLiteralDeclaration_5() { return cIsEmptyEnumLiteralDeclaration_5; }

		//"isEmpty"
		public Keyword getIsEmptyIsEmptyKeyword_5_0() { return cIsEmptyIsEmptyKeyword_5_0; }

		//last
		public EnumLiteralDeclaration getLastEnumLiteralDeclaration_6() { return cLastEnumLiteralDeclaration_6; }

		//"last"
		public Keyword getLastLastKeyword_6_0() { return cLastLastKeyword_6_0; }

		//size
		public EnumLiteralDeclaration getSizeEnumLiteralDeclaration_7() { return cSizeEnumLiteralDeclaration_7; }

		//"size"
		public Keyword getSizeSizeKeyword_7_0() { return cSizeSizeKeyword_7_0; }
	}

	public class CollectionModificationElements extends AbstractEnumRuleElementFinder {
		private final EnumRule rule = (EnumRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.CollectionModification");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final EnumLiteralDeclaration cAddEnumLiteralDeclaration_0 = (EnumLiteralDeclaration)cAlternatives.eContents().get(0);
		private final Keyword cAddAddKeyword_0_0 = (Keyword)cAddEnumLiteralDeclaration_0.eContents().get(0);
		private final EnumLiteralDeclaration cAddToFrontEnumLiteralDeclaration_1 = (EnumLiteralDeclaration)cAlternatives.eContents().get(1);
		private final Keyword cAddToFrontAddToFrontKeyword_1_0 = (Keyword)cAddToFrontEnumLiteralDeclaration_1.eContents().get(0);
		private final EnumLiteralDeclaration cClearEnumLiteralDeclaration_2 = (EnumLiteralDeclaration)cAlternatives.eContents().get(2);
		private final Keyword cClearClearKeyword_2_0 = (Keyword)cClearEnumLiteralDeclaration_2.eContents().get(0);
		private final EnumLiteralDeclaration cRemoveEnumLiteralDeclaration_3 = (EnumLiteralDeclaration)cAlternatives.eContents().get(3);
		private final Keyword cRemoveRemoveKeyword_3_0 = (Keyword)cRemoveEnumLiteralDeclaration_3.eContents().get(0);
		
		//enum CollectionModification returns CollectionOperation:
		//	add | addToFront | clear | remove;
		public EnumRule getRule() { return rule; }

		//add | addToFront | clear | remove
		public Alternatives getAlternatives() { return cAlternatives; }

		//add
		public EnumLiteralDeclaration getAddEnumLiteralDeclaration_0() { return cAddEnumLiteralDeclaration_0; }

		//"add"
		public Keyword getAddAddKeyword_0_0() { return cAddAddKeyword_0_0; }

		//addToFront
		public EnumLiteralDeclaration getAddToFrontEnumLiteralDeclaration_1() { return cAddToFrontEnumLiteralDeclaration_1; }

		//"addToFront"
		public Keyword getAddToFrontAddToFrontKeyword_1_0() { return cAddToFrontAddToFrontKeyword_1_0; }

		//clear
		public EnumLiteralDeclaration getClearEnumLiteralDeclaration_2() { return cClearEnumLiteralDeclaration_2; }

		//"clear"
		public Keyword getClearClearKeyword_2_0() { return cClearClearKeyword_2_0; }

		//remove
		public EnumLiteralDeclaration getRemoveEnumLiteralDeclaration_3() { return cRemoveEnumLiteralDeclaration_3; }

		//"remove"
		public Keyword getRemoveRemoveKeyword_3_0() { return cRemoveRemoveKeyword_3_0; }
	}
	
	private final DocumentElements pDocument;
	private final ImportElements pImport;
	private final ExpressionRegionElements pExpressionRegion;
	private final ExpressionOrRegionElements pExpressionOrRegion;
	private final ExpressionAndVariablesElements pExpressionAndVariables;
	private final VariableExpressionElements pVariableExpression;
	private final VariableDeclarationElements pVariableDeclaration;
	private final VariableAssignmentElements pVariableAssignment;
	private final TypedVariableDeclarationElements pTypedVariableDeclaration;
	private final ClockDeclarationElements pClockDeclaration;
	private final ClockElements pClock;
	private final ClockAssignmentElements pClockAssignment;
	private final ExpressionElements pExpression;
	private final ImplicationExpressionElements pImplicationExpression;
	private final DisjunctionExpressionElements pDisjunctionExpression;
	private final ConjunctionExpressionElements pConjunctionExpression;
	private final RelationExpressionElements pRelationExpression;
	private final TimedExpressionElements pTimedExpression;
	private final AdditionExpressionElements pAdditionExpression;
	private final MultiplicationExpressionElements pMultiplicationExpression;
	private final NegatedExpressionElements pNegatedExpression;
	private final BasicExpressionElements pBasicExpression;
	private final ValueElements pValue;
	private final IntegerValueElements pIntegerValue;
	private final BooleanValueElements pBooleanValue;
	private final StringValueElements pStringValue;
	private final EnumValueElements pEnumValue;
	private final NullValueElements pNullValue;
	private final VariableValueElements pVariableValue;
	private final CollectionAccessElements pCollectionAccess;
	private final CollectionOperationElements eCollectionOperation;
	private final CollectionModificationElements eCollectionModification;
	private final FeatureAccessElements pFeatureAccess;
	private final StructuralFeatureValueElements pStructuralFeatureValue;
	private final OperationValueElements pOperationValue;
	private final TerminalRule tBOOL;
	private final TerminalRule tSIGNEDINT;
	private final TerminalRule tDOUBLE;
	
	private final Grammar grammar;

	private final TerminalsGrammarAccess gaTerminals;

	@Inject
	public ScenarioExpressionsGrammarAccess(GrammarProvider grammarProvider,
		TerminalsGrammarAccess gaTerminals) {
		this.grammar = internalFindGrammar(grammarProvider);
		this.gaTerminals = gaTerminals;
		this.pDocument = new DocumentElements();
		this.pImport = new ImportElements();
		this.pExpressionRegion = new ExpressionRegionElements();
		this.pExpressionOrRegion = new ExpressionOrRegionElements();
		this.pExpressionAndVariables = new ExpressionAndVariablesElements();
		this.pVariableExpression = new VariableExpressionElements();
		this.pVariableDeclaration = new VariableDeclarationElements();
		this.pVariableAssignment = new VariableAssignmentElements();
		this.pTypedVariableDeclaration = new TypedVariableDeclarationElements();
		this.pClockDeclaration = new ClockDeclarationElements();
		this.pClock = new ClockElements();
		this.pClockAssignment = new ClockAssignmentElements();
		this.pExpression = new ExpressionElements();
		this.pImplicationExpression = new ImplicationExpressionElements();
		this.pDisjunctionExpression = new DisjunctionExpressionElements();
		this.pConjunctionExpression = new ConjunctionExpressionElements();
		this.pRelationExpression = new RelationExpressionElements();
		this.pTimedExpression = new TimedExpressionElements();
		this.pAdditionExpression = new AdditionExpressionElements();
		this.pMultiplicationExpression = new MultiplicationExpressionElements();
		this.pNegatedExpression = new NegatedExpressionElements();
		this.pBasicExpression = new BasicExpressionElements();
		this.pValue = new ValueElements();
		this.pIntegerValue = new IntegerValueElements();
		this.pBooleanValue = new BooleanValueElements();
		this.pStringValue = new StringValueElements();
		this.pEnumValue = new EnumValueElements();
		this.pNullValue = new NullValueElements();
		this.pVariableValue = new VariableValueElements();
		this.pCollectionAccess = new CollectionAccessElements();
		this.eCollectionOperation = new CollectionOperationElements();
		this.eCollectionModification = new CollectionModificationElements();
		this.pFeatureAccess = new FeatureAccessElements();
		this.pStructuralFeatureValue = new StructuralFeatureValueElements();
		this.pOperationValue = new OperationValueElements();
		this.tBOOL = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.BOOL");
		this.tSIGNEDINT = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
		this.tDOUBLE = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.expressions.ScenarioExpressions.DOUBLE");
	}
	
	protected Grammar internalFindGrammar(GrammarProvider grammarProvider) {
		Grammar grammar = grammarProvider.getGrammar(this);
		while (grammar != null) {
			if ("org.scenariotools.sml.expressions.ScenarioExpressions".equals(grammar.getName())) {
				return grammar;
			}
			List<Grammar> grammars = grammar.getUsedGrammars();
			if (!grammars.isEmpty()) {
				grammar = grammars.iterator().next();
			} else {
				return null;
			}
		}
		return grammar;
	}
	
	@Override
	public Grammar getGrammar() {
		return grammar;
	}
	

	public TerminalsGrammarAccess getTerminalsGrammarAccess() {
		return gaTerminals;
	}

	
	//Document:
	//	imports+=Import* ('domain' domains+=[EPackage])* expressions+=ExpressionRegion*;
	public DocumentElements getDocumentAccess() {
		return pDocument;
	}
	
	public ParserRule getDocumentRule() {
		return getDocumentAccess().getRule();
	}

	//Import:
	//	'import' importURI=STRING;
	public ImportElements getImportAccess() {
		return pImport;
	}
	
	public ParserRule getImportRule() {
		return getImportAccess().getRule();
	}

	//ExpressionRegion:
	//	{ExpressionRegion} '{' (expressions+=ExpressionOrRegion ';')* '}';
	public ExpressionRegionElements getExpressionRegionAccess() {
		return pExpressionRegion;
	}
	
	public ParserRule getExpressionRegionRule() {
		return getExpressionRegionAccess().getRule();
	}

	//ExpressionOrRegion:
	//	ExpressionRegion | ExpressionAndVariables;
	public ExpressionOrRegionElements getExpressionOrRegionAccess() {
		return pExpressionOrRegion;
	}
	
	public ParserRule getExpressionOrRegionRule() {
		return getExpressionOrRegionAccess().getRule();
	}

	//ExpressionAndVariables:
	//	VariableExpression | Expression;
	public ExpressionAndVariablesElements getExpressionAndVariablesAccess() {
		return pExpressionAndVariables;
	}
	
	public ParserRule getExpressionAndVariablesRule() {
		return getExpressionAndVariablesAccess().getRule();
	}

	//VariableExpression:
	//	TypedVariableDeclaration | VariableAssignment | ClockDeclaration | ClockAssignment;
	public VariableExpressionElements getVariableExpressionAccess() {
		return pVariableExpression;
	}
	
	public ParserRule getVariableExpressionRule() {
		return getVariableExpressionAccess().getRule();
	}

	//VariableDeclaration:
	//	'var' name=ID '=' expression=Expression;
	public VariableDeclarationElements getVariableDeclarationAccess() {
		return pVariableDeclaration;
	}
	
	public ParserRule getVariableDeclarationRule() {
		return getVariableDeclarationAccess().getRule();
	}

	//VariableAssignment:
	//	variable=[VariableDeclaration] '=' expression=Expression;
	public VariableAssignmentElements getVariableAssignmentAccess() {
		return pVariableAssignment;
	}
	
	public ParserRule getVariableAssignmentRule() {
		return getVariableAssignmentAccess().getRule();
	}

	//TypedVariableDeclaration:
	//	'var' type=[EClassifier] name=ID ('=' expression=Expression)?;
	public TypedVariableDeclarationElements getTypedVariableDeclarationAccess() {
		return pTypedVariableDeclaration;
	}
	
	public ParserRule getTypedVariableDeclarationRule() {
		return getTypedVariableDeclarationAccess().getRule();
	}

	//ClockDeclaration:
	//	{ClockDeclaration} 'clock' name=ID ('=' expression=IntegerValue)?;
	public ClockDeclarationElements getClockDeclarationAccess() {
		return pClockDeclaration;
	}
	
	public ParserRule getClockDeclarationRule() {
		return getClockDeclarationAccess().getRule();
	}

	//Clock:
	//	name=ID leftIncluded=BOOL rightIncluded=BOOL leftValue=INT rightValue=INT;
	public ClockElements getClockAccess() {
		return pClock;
	}
	
	public ParserRule getClockRule() {
		return getClockAccess().getRule();
	}

	//ClockAssignment:
	//	'reset clock' variable=[ClockDeclaration] ('=' expression=IntegerValue)?;
	public ClockAssignmentElements getClockAssignmentAccess() {
		return pClockAssignment;
	}
	
	public ParserRule getClockAssignmentRule() {
		return getClockAssignmentAccess().getRule();
	}

	//Expression:
	//	ImplicationExpression;
	public ExpressionElements getExpressionAccess() {
		return pExpression;
	}
	
	public ParserRule getExpressionRule() {
		return getExpressionAccess().getRule();
	}

	//ImplicationExpression Expression:
	//	DisjunctionExpression ({BinaryOperationExpression.left=current} operator="=>" right=ImplicationExpression)?;
	public ImplicationExpressionElements getImplicationExpressionAccess() {
		return pImplicationExpression;
	}
	
	public ParserRule getImplicationExpressionRule() {
		return getImplicationExpressionAccess().getRule();
	}

	//DisjunctionExpression Expression:
	//	ConjunctionExpression ({BinaryOperationExpression.left=current} operator="||" right=DisjunctionExpression)?;
	public DisjunctionExpressionElements getDisjunctionExpressionAccess() {
		return pDisjunctionExpression;
	}
	
	public ParserRule getDisjunctionExpressionRule() {
		return getDisjunctionExpressionAccess().getRule();
	}

	//ConjunctionExpression Expression:
	//	RelationExpression ({BinaryOperationExpression.left=current} operator="&&" right=ConjunctionExpression)?;
	public ConjunctionExpressionElements getConjunctionExpressionAccess() {
		return pConjunctionExpression;
	}
	
	public ParserRule getConjunctionExpressionRule() {
		return getConjunctionExpressionAccess().getRule();
	}

	//RelationExpression Expression:
	//	AdditionExpression ({BinaryOperationExpression.left=current} operator=('==' | '!=' | '<' | '>' | '<=' | '>=')
	//	right=RelationExpression)?;
	public RelationExpressionElements getRelationExpressionAccess() {
		return pRelationExpression;
	}
	
	public ParserRule getRelationExpressionRule() {
		return getRelationExpressionAccess().getRule();
	}

	//TimedExpression:
	//	clock=[ClockDeclaration] operator=('==' | '<' | '>' | '<=' | '>=') value=INT;
	public TimedExpressionElements getTimedExpressionAccess() {
		return pTimedExpression;
	}
	
	public ParserRule getTimedExpressionRule() {
		return getTimedExpressionAccess().getRule();
	}

	//AdditionExpression Expression:
	//	MultiplicationExpression ({BinaryOperationExpression.left=current} operator=('+' | '-') right=AdditionExpression)?;
	public AdditionExpressionElements getAdditionExpressionAccess() {
		return pAdditionExpression;
	}
	
	public ParserRule getAdditionExpressionRule() {
		return getAdditionExpressionAccess().getRule();
	}

	//MultiplicationExpression Expression:
	//	NegatedExpression ({BinaryOperationExpression.left=current} operator=('*' | '/') right=MultiplicationExpression)?;
	public MultiplicationExpressionElements getMultiplicationExpressionAccess() {
		return pMultiplicationExpression;
	}
	
	public ParserRule getMultiplicationExpressionRule() {
		return getMultiplicationExpressionAccess().getRule();
	}

	//NegatedExpression Expression:
	//	{UnaryOperationExpression} => operator=('!' | '-') operand=BasicExpression | BasicExpression;
	public NegatedExpressionElements getNegatedExpressionAccess() {
		return pNegatedExpression;
	}
	
	public ParserRule getNegatedExpressionRule() {
		return getNegatedExpressionAccess().getRule();
	}

	//BasicExpression Expression:
	//	Value | '(' Expression ')';
	public BasicExpressionElements getBasicExpressionAccess() {
		return pBasicExpression;
	}
	
	public ParserRule getBasicExpressionRule() {
		return getBasicExpressionAccess().getRule();
	}

	//Value:
	//	IntegerValue | BooleanValue | StringValue | EnumValue | NullValue | VariableValue | FeatureAccess;
	public ValueElements getValueAccess() {
		return pValue;
	}
	
	public ParserRule getValueRule() {
		return getValueAccess().getRule();
	}

	//IntegerValue:
	//	value=(INT | SIGNEDINT);
	public IntegerValueElements getIntegerValueAccess() {
		return pIntegerValue;
	}
	
	public ParserRule getIntegerValueRule() {
		return getIntegerValueAccess().getRule();
	}

	//BooleanValue:
	//	value=BOOL;
	public BooleanValueElements getBooleanValueAccess() {
		return pBooleanValue;
	}
	
	public ParserRule getBooleanValueRule() {
		return getBooleanValueAccess().getRule();
	}

	//StringValue:
	//	value=STRING;
	public StringValueElements getStringValueAccess() {
		return pStringValue;
	}
	
	public ParserRule getStringValueRule() {
		return getStringValueAccess().getRule();
	}

	//EnumValue:
	//	type=[EEnum] ':' value=[EEnumLiteral];
	public EnumValueElements getEnumValueAccess() {
		return pEnumValue;
	}
	
	public ParserRule getEnumValueRule() {
		return getEnumValueAccess().getRule();
	}

	//NullValue:
	//	{NullValue} 'null';
	public NullValueElements getNullValueAccess() {
		return pNullValue;
	}
	
	public ParserRule getNullValueRule() {
		return getNullValueAccess().getRule();
	}

	//VariableValue:
	//	value=[Variable];
	public VariableValueElements getVariableValueAccess() {
		return pVariableValue;
	}
	
	public ParserRule getVariableValueRule() {
		return getVariableValueAccess().getRule();
	}

	//CollectionAccess:
	//	collectionOperation=CollectionOperation '(' parameter=Expression? ')';
	public CollectionAccessElements getCollectionAccessAccess() {
		return pCollectionAccess;
	}
	
	public ParserRule getCollectionAccessRule() {
		return getCollectionAccessAccess().getRule();
	}

	//enum CollectionOperation:
	//	any | contains | containsAll | first | get | isEmpty | last | size;
	public CollectionOperationElements getCollectionOperationAccess() {
		return eCollectionOperation;
	}
	
	public EnumRule getCollectionOperationRule() {
		return getCollectionOperationAccess().getRule();
	}

	//enum CollectionModification returns CollectionOperation:
	//	add | addToFront | clear | remove;
	public CollectionModificationElements getCollectionModificationAccess() {
		return eCollectionModification;
	}
	
	public EnumRule getCollectionModificationRule() {
		return getCollectionModificationAccess().getRule();
	}

	//FeatureAccess:
	//	target=[EObject] '.' (value=StructuralFeatureValue ('.' collectionAccess=CollectionAccess)? | value=OperationValue
	//	'(' (parameters+=Expression (',' parameters+=Expression)*)? ')');
	public FeatureAccessElements getFeatureAccessAccess() {
		return pFeatureAccess;
	}
	
	public ParserRule getFeatureAccessRule() {
		return getFeatureAccessAccess().getRule();
	}

	//StructuralFeatureValue:
	//	value=[EStructuralFeature];
	public StructuralFeatureValueElements getStructuralFeatureValueAccess() {
		return pStructuralFeatureValue;
	}
	
	public ParserRule getStructuralFeatureValueRule() {
		return getStructuralFeatureValueAccess().getRule();
	}

	//OperationValue:
	//	value=[EOperation];
	public OperationValueElements getOperationValueAccess() {
		return pOperationValue;
	}
	
	public ParserRule getOperationValueRule() {
		return getOperationValueAccess().getRule();
	}

	//terminal BOOL returns EBoolean:
	//	'false' | 'true';
	public TerminalRule getBOOLRule() {
		return tBOOL;
	} 

	//terminal SIGNEDINT returns EInt:
	//	'-' INT;
	public TerminalRule getSIGNEDINTRule() {
		return tSIGNEDINT;
	} 

	//terminal DOUBLE returns EDouble:
	//	'-'? INT? '.' INT (('e' | 'E') '-'? INT)?;
	public TerminalRule getDOUBLERule() {
		return tDOUBLE;
	} 

	//terminal ID:
	//	'^'? ('a'..'z' | 'A'..'Z' | '_') ('a'..'z' | 'A'..'Z' | '_' | '0'..'9')*;
	public TerminalRule getIDRule() {
		return gaTerminals.getIDRule();
	} 

	//terminal INT returns ecore::EInt:
	//	'0'..'9'+;
	public TerminalRule getINTRule() {
		return gaTerminals.getINTRule();
	} 

	//terminal STRING:
	//	'"' ('\\' . | !('\\' | '"'))* '"' | "'" ('\\' . | !('\\' | "'"))* "'";
	public TerminalRule getSTRINGRule() {
		return gaTerminals.getSTRINGRule();
	} 

	//terminal ML_COMMENT:
	//	'/*'->'*/';
	public TerminalRule getML_COMMENTRule() {
		return gaTerminals.getML_COMMENTRule();
	} 

	//terminal SL_COMMENT:
	//	'//' !('\n' | '\r')* ('\r'? '\n')?;
	public TerminalRule getSL_COMMENTRule() {
		return gaTerminals.getSL_COMMENTRule();
	} 

	//terminal WS:
	//	' ' | '\t' | '\r' | '\n'+;
	public TerminalRule getWSRule() {
		return gaTerminals.getWSRule();
	} 

	//terminal ANY_OTHER:
	//	.;
	public TerminalRule getANY_OTHERRule() {
		return gaTerminals.getANY_OTHERRule();
	} 
}
