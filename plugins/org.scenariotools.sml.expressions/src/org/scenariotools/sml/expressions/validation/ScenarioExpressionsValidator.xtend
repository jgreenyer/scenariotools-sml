/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.expressions.validation

import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.xtext.validation.Check
import org.scenariotools.sml.expressions.scenarioExpressions.BinaryOperationExpression
import org.scenariotools.sml.expressions.scenarioExpressions.CollectionAccess
import org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess
import org.scenariotools.sml.expressions.scenarioExpressions.NullValue
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage
import org.scenariotools.sml.expressions.scenarioExpressions.StructuralFeatureValue
import org.scenariotools.sml.expressions.scenarioExpressions.TypedVariableDeclaration
import org.scenariotools.sml.expressions.scenarioExpressions.UnaryOperationExpression
import org.scenariotools.sml.expressions.scenarioExpressions.VariableAssignment
import org.scenariotools.sml.expressions.scenarioExpressions.VariableDeclaration
import org.scenariotools.sml.expressions.utility.CollectionMultiplicityTypes
import org.scenariotools.sml.expressions.utility.CollectionUtil

import org.scenariotools.sml.expressions.utility.ExpressionUtil
import org.eclipse.emf.ecore.ETypedElement
import org.scenariotools.sml.expressions.scenarioExpressions.OperationValue

/**
 * This class contains custom validation rules. 
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
class ScenarioExpressionsValidator extends AbstractScenarioExpressionsValidator {

	public static val INVALID_NAME = 'invalidName'
	public static val DUPLICATE_NAME = 'duplicateName'
	public static val INCORRECT_TYPE = 'incorrectType'
	public static val TOO_MANY_ARGUMENTS = 'TOO_MANY_ARGUMENTS'
	public static val TOO_MANY_ARGUMENTS_NO_FIX = 'TOO_MANY_ARGUMENTS_NO_FIX'
	public static val TOO_FEW_ARGUMENTS = 'TOO_FEW_ARGUMENTS'

	@Check
	def check() {
		
	}
	
	@Check
	def checkNoDuplicateVariables(VariableDeclaration variable) {
		val variables = ExpressionUtil.getRelevantVariablesFor(variable)
		if (variables.filter [ v |
			v.name.equals(variable.name)
		].size > 1)
			error('Duplicate variable ' + variable.name + '.', ScenarioExpressionsPackage.Literals.NAMED_ELEMENT__NAME,
				DUPLICATE_NAME)
	}

	@Check
	def checkUnaryTypeCorrectness(UnaryOperationExpression expression) {
		var err = false as boolean
		if (ExpressionUtil.isBooleanOperator(expression.operator)) {
			if (!ExpressionUtil.isBooleanExpression(expression.operand)) {
				err = true
			}
		} else if (ExpressionUtil.isIntegerOperator(expression.operator)) {
			if (!ExpressionUtil.isIntegerExpression(expression.operand)) {
				err = true
			}
		}
		if (err)
			error(
				'Incorrect type! Operator ' + expression.operator + ' can not be used with a ' +
					ExpressionUtil.getExpressionType(expression.operand).name + ' value.',
				ScenarioExpressionsPackage.Literals.UNARY_OPERATION_EXPRESSION__OPERAND, INCORRECT_TYPE)
	}

	@Check
	def checkBinaryTypeCorrectness(BinaryOperationExpression expression) {
		val left = expression.left
		val right = expression.right
		if (ExpressionUtil.isBooleanOperator(expression.operator)) {
			if (!ExpressionUtil.isBooleanExpression(left) || !ExpressionUtil.isBooleanExpression(right))
				error('Operator ' + expression.operator + ' must be used with two Boolean values.',
					ScenarioExpressionsPackage.Literals.OPERATION_EXPRESSION__OPERATOR, INCORRECT_TYPE)
		} else if (expression.operator.equals("-")) {
			if (!ExpressionUtil.isIntegerExpression(left) || !ExpressionUtil.isIntegerExpression(right))
				error('Operator ' + expression.operator + ' can only be used with Integer values.',
					ScenarioExpressionsPackage.Literals.OPERATION_EXPRESSION__OPERATOR, INCORRECT_TYPE)
		} else if (expression.operator.equals("+")) {
			if (!(ExpressionUtil.isIntegerExpression(left) && ExpressionUtil.isIntegerExpression(right) ||
				ExpressionUtil.isStringExpression(left) && ExpressionUtil.isStringExpression(right)))
				error('Operator ' + expression.operator + ' can only be used with Integer or String values.',
					ScenarioExpressionsPackage.Literals.OPERATION_EXPRESSION__OPERATOR, INCORRECT_TYPE)
		} else if (expression.operator.equals("<") || expression.operator.equals(">") ||
			expression.operator.equals("<=") || expression.operator.equals(">=")) {
			if (ExpressionUtil.getExpressionType(left) != ExpressionUtil.getExpressionType(right)) {
				error('Operator ' + expression.operator + ' can only be used with values of identical type.',
					ScenarioExpressionsPackage.Literals.OPERATION_EXPRESSION__OPERATOR, INCORRECT_TYPE)
			}
		} else if (expression.operator.equals("!=") || expression.operator.equals("==")) {
			if (ExpressionUtil.getExpressionType(left) != ExpressionUtil.getExpressionType(right) &&
				!(left instanceof NullValue) && !(right instanceof NullValue)) {
				val typeLeft = ExpressionUtil.getExpressionType(left)
				val typeRight = ExpressionUtil.getExpressionType(right)
				if (!(typeLeft instanceof EClass && typeRight instanceof EClass)) {
					error('Operator ' + expression.operator + ' can only be used with values of identical type.',
						ScenarioExpressionsPackage.Literals.OPERATION_EXPRESSION__OPERATOR, INCORRECT_TYPE)
				}
			}
		}
	}
		
	@Check
	def checkTypedVariableCorrectType(TypedVariableDeclaration variable) {
		val type = variable.type
		val exp = variable.expression
		if (exp != null) {
			val expType = ExpressionUtil.getExpressionType(exp)
			if (type != expType) {
				if (type instanceof EClass && expType instanceof EClass) {
					val class1 = type as EClass
					val class2 = expType as EClass
					if (!class1.isSuperTypeOf(class2)) {
						error('Incorrect type! Variable is of type ' + type.name + '. Given ' + expType.name + '.',
							ScenarioExpressionsPackage.Literals.VARIABLE_EXPRESSION__EXPRESSION, INCORRECT_TYPE)
					}
				} else {
					error('Incorrect type! Variable is of type ' + type.name + '. Given ' + expType.name + '.',
						ScenarioExpressionsPackage.Literals.VARIABLE_EXPRESSION__EXPRESSION, INCORRECT_TYPE)
				}
			}
		}
	}

	@Check
	def checkVariableAssignmentCorrectType(VariableAssignment variableAssignment) {
		val variable = variableAssignment.variable as TypedVariableDeclaration
		val type = variable.type
		val exp = variableAssignment.expression
		if (exp != null) {
			val expType = ExpressionUtil.getExpressionType(exp)
			if (type != expType)
				error('Incorrect type! Variable is of type ' + type.name + '. Given ' + expType.name + '.',
					ScenarioExpressionsPackage.Literals.VARIABLE_EXPRESSION__EXPRESSION, INCORRECT_TYPE)
		}
	}

	@Check
	def checkTypedVariableCorrectMultiplicity(TypedVariableDeclaration variable) {
		val type = variable.type
		val exp = variable.expression
		if (exp != null) {
			val expType = ExpressionUtil.getExpressionType(exp)
			if (expType instanceof EStructuralFeature)
				if (type != expType)
					error('Incorrect type! Variable is of type (Structural)' + type.name + '. Given ' + expType.name + '.',
						ScenarioExpressionsPackage.Literals.VARIABLE_EXPRESSION__EXPRESSION, INCORRECT_TYPE)
		}
	}

	@Check
	def checkNoCollectionOperationOnNonCollectionType(CollectionAccess collectionAccess) {
		val featureAccess = collectionAccess.eContainer as FeatureAccess
		val value = featureAccess.value
		if (value instanceof StructuralFeatureValue) {
			val feature = value.value as EStructuralFeature
			if (!CollectionUtil.isCollection(feature)) {
				error('The EStructuralFeature ' + feature.name + ' is no collection.',
					ScenarioExpressionsPackage.Literals.COLLECTION_ACCESS__COLLECTION_OPERATION, INCORRECT_TYPE)
			}
		}
	}

	@Check
	def checkOnlySetOperationsOnSetCollectionType(CollectionAccess collectionAccess) {
		val featureAccess = collectionAccess.eContainer as FeatureAccess
		val value = featureAccess.value
		if (value instanceof StructuralFeatureValue) {
			val feature = value.value as EStructuralFeature
			if (CollectionUtil.isSet(feature)) {
				if (!CollectionUtil.isSetCollectionOperation(collectionAccess.collectionOperation))
					error('Cannot use non set operations on unordered collections.',
						ScenarioExpressionsPackage.Literals.COLLECTION_ACCESS__COLLECTION_OPERATION, INCORRECT_TYPE)
			}
		}
	}
		
	@Check
	def checkOnlyListOperationsOnListCollectionType(CollectionAccess collectionAccess) {
		val featureAccess = collectionAccess.eContainer as FeatureAccess
		val value = featureAccess.value
		if (value instanceof StructuralFeatureValue) {
			val feature = value.value as EStructuralFeature
			if (CollectionUtil.isList(feature)) {
				if (!CollectionUtil.isListCollectionOperation(collectionAccess.collectionOperation))
					error('Cannot use non list operations on ordered collections.',
						ScenarioExpressionsPackage.Literals.COLLECTION_ACCESS__COLLECTION_OPERATION, INCORRECT_TYPE)
			}
		}
	}

	@Check
	def checkFeatureAccessMultiplicity(TypedVariableDeclaration variable) {
		val expression = variable.expression
		if (expression instanceof FeatureAccess) {
			val fa = expression as FeatureAccess

			var ETypedElement feature = null
			val v = fa.value
			if(v instanceof StructuralFeatureValue)
				feature = v.value as ETypedElement				
			else if(v instanceof OperationValue)
				feature = v.value as ETypedElement
			else
				error('Unsupported FeatureAccess value', ScenarioExpressionsPackage.Literals.VARIABLE_EXPRESSION__EXPRESSION, INCORRECT_TYPE)

			// Check if Feature is collection
			if (feature.upperBound != 1) {
				val collectionAccess = fa.collectionAccess

				// Check if there is no collection access
				if (collectionAccess == null) {
					error(
						'Incorrect type! Variable is of type ' + variable.type.name + '. Given collection of ' +
							ExpressionUtil.getExpressionType(expression).name + '.',
						ScenarioExpressionsPackage.Literals.VARIABLE_EXPRESSION__EXPRESSION,
						INCORRECT_TYPE
					)
				} // there is a collection access
				else {
					val type = variable.type as EClassifier // The type of the EStructuralFeature
					val collectionAccessType = CollectionUtil.getCollectionReturnType(collectionAccess)

					// Check if types do not match
					if (type != collectionAccessType) {

						// Check if collection access type is an EClass
						if (collectionAccessType instanceof EClass) {

							// Check if supertypes do not match
							if (!collectionAccessType.EAllSuperTypes.contains(type))
								error(
									'Incorrect type! Variable is of type ' + type.name + '. Given ' +
										collectionAccessType.name + '.',
									ScenarioExpressionsPackage.Literals.VARIABLE_EXPRESSION__EXPRESSION,
									INCORRECT_TYPE
								)
						} // type is ecore type (EInt, ...)
						else {
							error(
								'Incorrect type! Variable is of type ' + type.name + '. Given ' +
									collectionAccessType.name + '.',
								ScenarioExpressionsPackage.Literals.VARIABLE_EXPRESSION__EXPRESSION,
								INCORRECT_TYPE
							)
						}
					}
				}
			}
		}
	}

	@Check
	def checkCollectionAccessParameterMultiplicity(CollectionAccess collectionAccess) {
		val parameter = collectionAccess.parameter
		switch CollectionUtil.getNeededCollectionParameterMultiplicity(collectionAccess) {
			case CollectionMultiplicityTypes.NONE:
				if (parameter != null)
					error(
						'Too many arguments.',
						ScenarioExpressionsPackage.Literals.COLLECTION_ACCESS__PARAMETER,
						TOO_MANY_ARGUMENTS
					)
			case CollectionMultiplicityTypes.SINGLE:
				if (parameter == null)
					error(
						'Too few arguments.',
						ScenarioExpressionsPackage.Literals.COLLECTION_ACCESS__PARAMETER,
						TOO_FEW_ARGUMENTS
					)
				else if (CollectionUtil.getMultiplicityOfExpression(parameter) != CollectionMultiplicityTypes.SINGLE)
					error(
						'Argument is a collection type.',
						ScenarioExpressionsPackage.Literals.COLLECTION_ACCESS__PARAMETER,
						TOO_MANY_ARGUMENTS
					)
			case CollectionMultiplicityTypes.MULTIPLE:
				if (parameter == null)
					error(
						'Too few arguments.',
						ScenarioExpressionsPackage.Literals.COLLECTION_ACCESS__PARAMETER,
						TOO_FEW_ARGUMENTS
					)
				else if (CollectionUtil.getMultiplicityOfExpression(parameter) != CollectionMultiplicityTypes.MULTIPLE)
					error(
						'Argument is not a collection type.',
						ScenarioExpressionsPackage.Literals.COLLECTION_ACCESS__PARAMETER,
						INCORRECT_TYPE
					)
		}
	}

	@Check
	def checkCollectionAccessParameterHasCorrectType(CollectionAccess collectionAccess) {
		val parameter = collectionAccess.parameter
		if (parameter == null)
			return;
		val parameterType = ExpressionUtil.getExpressionType(parameter)
		val neededParameterType = CollectionUtil.getNeededCollectionParameterType(collectionAccess)
		if (neededParameterType == null)
			return;
		if (parameterType != neededParameterType)
			error(
				'Incorrect type! Parameter type is ' + neededParameterType.name + '. Given ' + parameterType.name + '.',
				ScenarioExpressionsPackage.Literals.COLLECTION_ACCESS__PARAMETER,
				INCORRECT_TYPE
			)
	}

	@Check
	def checkParametersOfOperationInvocation(FeatureAccess fa) {
		val value = fa.value
		if(value instanceof OperationValue) {
			val sourceParameters = fa.parameters
			val targetParameters = value.value.EParameters
			
			if(sourceParameters.size < targetParameters.size) {
				error('Not enough parameters', ScenarioExpressionsPackage.Literals.FEATURE_ACCESS__PARAMETERS, sourceParameters.size, TOO_FEW_ARGUMENTS)
			} else if (sourceParameters.size > targetParameters.size) {
				error('Too many parameters', ScenarioExpressionsPackage.Literals.FEATURE_ACCESS__PARAMETERS, targetParameters.size, TOO_MANY_ARGUMENTS_NO_FIX)				
			} else {
				for(var i = 0; i < sourceParameters.size; i++) {
					val sourceType = ExpressionUtil.getExpressionType(sourceParameters.get(i))
					val targetType = targetParameters.get(i).EType
					if(sourceType != targetType)
						error('Type mismatch. Expected ' + targetType.name + ', found ' + sourceType.name, ScenarioExpressionsPackage.Literals.FEATURE_ACCESS__PARAMETERS, i, INCORRECT_TYPE)  
				}
			}
		}
	}
}
