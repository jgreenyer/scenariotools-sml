/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.expressions.formatting2;

import com.google.inject.Inject;
import org.eclipse.xtext.formatting2.AbstractFormatter2;
import org.eclipse.xtext.formatting2.IFormattableDocument;
import org.scenariotools.sml.expressions.scenarioExpressions.BinaryOperationExpression;
import org.scenariotools.sml.expressions.scenarioExpressions.ClockAssignment;
import org.scenariotools.sml.expressions.scenarioExpressions.ClockDeclaration;
import org.scenariotools.sml.expressions.scenarioExpressions.CollectionAccess;
import org.scenariotools.sml.expressions.scenarioExpressions.Document;
import org.scenariotools.sml.expressions.scenarioExpressions.ExpressionOrRegion;
import org.scenariotools.sml.expressions.scenarioExpressions.ExpressionRegion;
import org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess;
import org.scenariotools.sml.expressions.scenarioExpressions.Import;
import org.scenariotools.sml.expressions.scenarioExpressions.TypedVariableDeclaration;
import org.scenariotools.sml.expressions.scenarioExpressions.UnaryOperationExpression;
import org.scenariotools.sml.expressions.scenarioExpressions.VariableAssignment;
import org.scenariotools.sml.expressions.scenarioExpressions.VariableDeclaration;
import org.scenariotools.sml.expressions.services.ScenarioExpressionsGrammarAccess;
import org.eclipse.xtext.formatting2.regionaccess.ISemanticRegion
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage

class ScenarioExpressionsFormatter extends AbstractFormatter2 {
	
	@Inject extension ScenarioExpressionsGrammarAccess

	def dispatch void format(Document theDocument, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		for (Import imports : theDocument.getImports()) {
			format(imports, document);
		}
		for (ExpressionRegion expressions : theDocument.getExpressions()) {
			format(expressions, document);
		}
	}

	def dispatch void format(ExpressionRegion expressionregion, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		for (ExpressionOrRegion expressions : expressionregion.getExpressions()) {
			format(expressions, document);
		}
	}

	def dispatch void format(VariableDeclaration variabledeclaration, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		format(variabledeclaration.getExpression(), document);
	}

	def dispatch void format(VariableAssignment variableassignment, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		format(variableassignment.getExpression(), document);
	}

	def dispatch void format(TypedVariableDeclaration typedvariabledeclaration, extension IFormattableDocument document) {
		var region = typedvariabledeclaration.regionFor.keyword("var")
		do {
			region = region.nextSemanticRegion
			region.surround[oneSpace]
		} while(region.text != "=")
		
		format(typedvariabledeclaration.getExpression(), document);
	}

	def dispatch void format(ClockDeclaration clockdeclaration, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		format(clockdeclaration.getExpression(), document);
	}

	def dispatch void format(ClockAssignment clockassignment, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		format(clockassignment.getExpression(), document);
	}

	def dispatch void format(BinaryOperationExpression binaryoperationexpression, extension IFormattableDocument document) {
		binaryoperationexpression.getLeft().append[oneSpace]
		binaryoperationexpression.getRight().prepend[oneSpace]
	}

	def dispatch void format(UnaryOperationExpression unaryoperationexpression, extension IFormattableDocument document) {
		unaryoperationexpression.semanticRegions.get(0).surround[noSpace]
	}

	def dispatch void format(CollectionAccess collectionaccess, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		format(collectionaccess.getParameter(), document);
	}

	def dispatch void format(FeatureAccess featureaccess, extension IFormattableDocument document) {
		featureaccess.regionFor.keyword(".").surround[noSpace]
		format(featureaccess.getValue(), document);
		format(featureaccess.getCollectionAccess(), document);
	}
}
