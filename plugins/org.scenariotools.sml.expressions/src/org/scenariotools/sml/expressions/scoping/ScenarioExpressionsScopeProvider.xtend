/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.expressions.scoping

import com.google.common.base.Function
import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EReference
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.ecore.EcorePackage
import org.eclipse.xtext.naming.QualifiedName
import org.eclipse.xtext.scoping.IScope
import org.eclipse.xtext.scoping.Scopes
import org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider
import org.scenariotools.sml.expressions.scenarioExpressions.EnumValue
import org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess
import org.scenariotools.sml.expressions.scenarioExpressions.StructuralFeatureValue
import org.scenariotools.sml.expressions.scenarioExpressions.TypedVariableDeclaration
import org.scenariotools.sml.expressions.scenarioExpressions.VariableAssignment
import org.scenariotools.sml.expressions.scenarioExpressions.VariableDeclaration
import org.scenariotools.sml.expressions.scenarioExpressions.VariableValue
import org.scenariotools.sml.expressions.utility.ExpressionUtil
import org.scenariotools.sml.expressions.utility.ImportUtil
import org.scenariotools.sml.expressions.utility.ValueUtil
import org.scenariotools.sml.expressions.scenarioExpressions.OperationValue
import org.eclipse.emf.ecore.EOperation
import org.scenariotools.sml.expressions.scenarioExpressions.TypedVariable
import org.eclipse.emf.ecore.EClass

/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 * 
 */
class ScenarioExpressionsScopeProvider extends AbstractDeclarativeScopeProvider {

	def IScope scope_EnumValue_type(EnumValue ev, EReference ref) {
		return Scopes::scopeFor(ImportUtil.getImportedEEnums(ev))
	}

	def IScope scope_EnumValue_value(EnumValue ev, EReference ref) {
		return Scopes::scopeFor(ev.type.eContents)
	}

	def IScope scope_VariableValue_value(VariableValue value, EReference ref) {
		val scope = ExpressionUtil.getRelevantVariablesFor(value)
		return Scopes::scopeFor(scope)
	}

	def IScope scope_VariableDeclaration_type(VariableDeclaration variable, EReference ref) {
		return Scopes::scopeFor(ImportUtil.getImportedEClasses(variable))
	}

	def IScope scope_VariableAssignment_variable(VariableAssignment assignment, EReference ref) {
		val scope = ExpressionUtil.getRelevantVariablesFor(assignment)
		return Scopes::scopeFor(scope)
	}

	def IScope scope_StructuralFeatureValue_value(StructuralFeatureValue value, EReference ref) {
		val scope = ValueUtil.getValidFeaturesForValue(value)
		return Scopes::scopeFor(scope)
	}
	
	def IScope scope_OperationValue_value(OperationValue value, EReference ref) {
		val result = new BasicEList<EOperation>()

		val container = value.eContainer		
		if (container instanceof FeatureAccess) {
			val target = container.target
			if (target instanceof TypedVariable) {
				val type = target.type
				if (type instanceof EClass) {
					val eclass = type as EClass
					result.addAll(eclass.getEAllOperations)
					eclass.getEAllSuperTypes.forEach [s | result.addAll(s.getEAllOperations)]
				}
			}
		}
		
		return Scopes::scopeFor(result)
	}
	
	def IScope scope_TypedVariable_type(TypedVariableDeclaration variable, EReference ref) {
		val scope = new BasicEList<EClassifier>()
		scope.add(EcorePackage.Literals.EINT)
		scope.add(EcorePackage.Literals.ESTRING)
		scope.add(EcorePackage.Literals.EBOOLEAN)
		scope.addAll(ImportUtil.getImportedEClasses(variable))
		return Scopes::scopeFor(scope)
	}
	
	static public def String getGetterName(EStructuralFeature feature) {
		if (feature.EType.name.equals("EBoolean"))
			return 'is' + feature.name.toFirstUpper
		else
			return 'get' + feature.name.toFirstUpper
	}

	static public var Function<EStructuralFeature, QualifiedName> getNameComputationForFeatureGetter = [
		QualifiedName::create(it.getGetterName)
	]

	def IScope scope_FeatureAccess_target(FeatureAccess fa, EReference ref) {
		val scope = ExpressionUtil.getRelevantVariablesFor(fa)
		return Scopes::scopeFor(scope)
	}
}
