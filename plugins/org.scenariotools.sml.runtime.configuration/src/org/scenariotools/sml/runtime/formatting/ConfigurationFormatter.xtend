/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.formatting

import org.eclipse.xtext.formatting.impl.AbstractDeclarativeFormatter
import org.eclipse.xtext.formatting.impl.FormattingConfig
import com.google.inject.Inject
import org.scenariotools.sml.runtime.services.ConfigurationGrammarAccess

// import com.google.inject.Inject;
// import org.scenariotools.sml.runtime.services.ConfigurationGrammarAccess
/**
 * This class contains custom formatting description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#formatting
 * on how and when to use it 
 * 
 * Also see {@link org.eclipse.xtext.xtext.XtextFormattingTokenSerializer} as an example
 */
class ConfigurationFormatter extends AbstractDeclarativeFormatter {

	@Inject extension ConfigurationGrammarAccess

	override protected void configureFormatting(FormattingConfig c) {
// It's usually a good idea to activate the following three statements.
// They will add and preserve newlines around comments
		val f = grammarAccess as ConfigurationGrammarAccess
		
		for(pair: findKeywordPairs('{', '}')) {
			c.setIndentation(pair.first, pair.second)
			c.setLinewrap(1).after(pair.first)
			c.setLinewrap(1).before(pair.second)
			c.setLinewrap(1).after(pair.second)
		}
		
//		c.setLinewrap().after(f.configurationAccess.instanceModelImportsAssignment_3);
//		c.setLinewrap(2).after(f.configurationAccess.specificationAssignment_2);
//		c.setLinewrap(2).between(f.configurationAccess.instanceModelImportsAssignment_3,f.configurationAccess.staticRoleBindingsRoleBindingsParserRuleCall_4_0)
//		c.setLinewrap().after(f.roleAssignmentAccess.roleRoleCrossReference_4_0)
//		c.setLinewrap(2).after(f.configurationAccess.staticRoleBindingsAssignment_4)
//		c.setLinewrap(0, 1, 2).before(SL_COMMENTRule)
//		c.setLinewrap(0, 1, 2).before(ML_COMMENTRule)
//		c.setLinewrap(0, 1, 1).after(ML_COMMENTRule)
//		c.setLinewrap().after(f.configurationAccess.ignoredCollaborationsCollaborationCrossReference_5_2_0)

	}
}
