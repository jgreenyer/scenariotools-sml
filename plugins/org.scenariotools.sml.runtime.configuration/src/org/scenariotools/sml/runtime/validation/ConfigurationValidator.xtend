/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.validation

import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.validation.Check
import org.scenariotools.sml.Role
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage
import org.scenariotools.sml.runtime.configuration.ConfigurationPackage
import org.scenariotools.sml.runtime.configuration.RoleAssignment
import org.scenariotools.sml.runtime.configuration.RoleBindings
import org.scenariotools.sml.runtime.configuration.Configuration

//import org.eclipse.xtext.validation.Check
/**
 * Custom validation rules. 
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
class ConfigurationValidator extends AbstractConfigurationValidator {

//	
	@Check
	def checkNoCollaborationConfiguredTwice(RoleBindings b) {
		(b.eContainer as Configuration).staticRoleBindings.forEach[rb |
			if(rb != b && rb.collaboration == b.collaboration) {
				error('Multiple configuration of collaboration \''+b.collaboration.name+'\'.', b, ConfigurationPackage.Literals.ROLE_BINDINGS__COLLABORATION)
			}
		]
	}
//	@Check
//	def checkRoleAssignmentTypes(RoleAssignment ra) {
//		if(!((ra.role.type as EClass).isSuperTypeOf(ra.object.eClass)||ra.role.type != ra.object.eClass)){
//			error("Role type must be equal to or a supertype of object's type.",ConfigurationPackage.Literals.ROLE_ASSIGNMENT__OBJECT)
//		}
//	}
	@Check
	def checkRoleBindingsCollaborationHasStaticRoles(RoleBindings r) {
		for (b : r.bindings) {
			if (!r.collaboration.roles.contains(b.role))
				error("Role not contained in Collaboration: " + b.role,
					ConfigurationPackage.Literals.ROLE_BINDINGS__COLLABORATION)
		}
		if (!r.collaboration.roles.exists[role|role.isStatic]) {
			error("Collaboration has no static roles.", ConfigurationPackage.Literals.ROLE_BINDINGS__COLLABORATION)
		}
	}
	@Check
	def checkRoleIsStatic(RoleAssignment r) {
		if (r.role!= null && !r.role.isStatic)
			error("Role is not static. Only bindings to static roles are allowed.",
				ConfigurationPackage.Literals.ROLE_ASSIGNMENT__ROLE)
	}

	@Check
	def checkStatic(Role r) {
		if (r.isStatic)
			warning("Static Role!!!", ScenarioExpressionsPackage.Literals.NAMED_ELEMENT__NAME)
	}
	@Check
	def checkAssignment(RoleAssignment ass) {
//		System.out.println(ass.role.type)
//		System.out.println(ass.object.eClass)
		if(ass.object.eClass != (ass.role.type as EClass))
			warning("Classes do not match",ConfigurationPackage.Literals.ROLE_ASSIGNMENT__ROLE)	
		
	}

}
