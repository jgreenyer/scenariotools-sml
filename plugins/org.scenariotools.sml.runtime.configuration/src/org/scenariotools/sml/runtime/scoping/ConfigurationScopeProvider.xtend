/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.scoping

import org.eclipse.emf.ecore.EReference
import org.eclipse.xtext.scoping.IScope
import org.eclipse.xtext.scoping.Scopes
import org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider
import org.eclipse.xtext.scoping.impl.FilteringScope
import org.eclipse.xtext.scoping.impl.SimpleScope
import org.scenariotools.sml.runtime.configuration.RoleAssignment
import org.scenariotools.sml.runtime.configuration.RoleBindings
import org.eclipse.xtext.EcoreUtil2
import org.scenariotools.sml.runtime.configuration.Configuration

/**
 * This class contains custom scoping description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#scoping
 * on how and when to use it 
 * 
 */
class ConfigurationScopeProvider extends AbstractDeclarativeScopeProvider {

	def IScope scope_RoleAssignment_role(RoleAssignment ra, EReference ref) {
		val bindings = ra.eContainer as RoleBindings 
		new SimpleScope(Scopes::scopedElementsFor(bindings.collaboration.roles))	
	}
	def IScope scope_Configuration_auxiliaryCollaborations(Configuration c, EReference ref) {
		c.scopeForCollaborations(ref)
	}
	def IScope scope_Configuration_ignoredCollaborations(Configuration c, EReference ref) {
		c.scopeForCollaborations(ref)
	}
	def IScope scope_RoleAssignment_object(RoleAssignment ra, EReference ref){
		val scope = delegateGetScope(ra,ref) 
		new FilteringScope(scope,[d|d.getUserData("from_xmi")!=null])
	
	}
	def IScope scope_RoleBindings_collaboration(RoleBindings b, EReference ref) {
		(EcoreUtil2.getRootContainer(b) as Configuration).scopeForCollaborations(ref)
	}
	
	public def IScope scopeForCollaborations(Configuration c,EReference ref) {
		new SimpleScope(delegateGetScope(c,ref),delegate.getScope(c.specification,ref).allElements,false)
//		Scopes::scopeFor(c.specification.collaborations,delegateGetScope(c,ref))
	}
}
