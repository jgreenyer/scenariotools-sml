/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime;

import org.eclipse.xtext.resource.IResourceServiceProvider;
import org.scenariotools.xtext.resources.xmi.XMIRuntimeModule;

import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class ConfigurationStandaloneSetup extends ConfigurationStandaloneSetupGenerated{

	public static void doSetup() {
		new ConfigurationStandaloneSetup().createInjectorAndDoEMFRegistration();
		
	}
@Override
	public Injector createInjectorAndDoEMFRegistration() {
		registerResourceServiceProviders();
		return super.createInjectorAndDoEMFRegistration();
	}
private void registerResourceServiceProviders() {
	if(!IResourceServiceProvider.Registry.INSTANCE.getExtensionToFactoryMap().containsKey("xmi"))
		IResourceServiceProvider.Registry.INSTANCE.getExtensionToFactoryMap().put("xmi", Guice.createInjector(new XMIRuntimeModule()).getInstance(IResourceServiceProvider.class));
}	
	
}

