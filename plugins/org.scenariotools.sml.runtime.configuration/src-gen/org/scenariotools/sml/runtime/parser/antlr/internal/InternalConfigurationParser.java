package org.scenariotools.sml.runtime.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.scenariotools.sml.runtime.services.ConfigurationGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalConfigurationParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'configure'", "'specification'", "'ignored'", "'collaboration'", "','", "'auxiliary'", "'collaborations'", "'.'", "'rolebindings'", "'role'", "'bindings'", "'for'", "'{'", "'}'", "'object'", "'plays'", "'use'", "'instancemodel'", "'import'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalConfigurationParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalConfigurationParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalConfigurationParser.tokenNames; }
    public String getGrammarFileName() { return "InternalConfiguration.g"; }



     	private ConfigurationGrammarAccess grammarAccess;
     	
        public InternalConfigurationParser(TokenStream input, ConfigurationGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Configuration";	
       	}
       	
       	@Override
       	protected ConfigurationGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleConfiguration"
    // InternalConfiguration.g:67:1: entryRuleConfiguration returns [EObject current=null] : iv_ruleConfiguration= ruleConfiguration EOF ;
    public final EObject entryRuleConfiguration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConfiguration = null;


        try {
            // InternalConfiguration.g:68:2: (iv_ruleConfiguration= ruleConfiguration EOF )
            // InternalConfiguration.g:69:2: iv_ruleConfiguration= ruleConfiguration EOF
            {
             newCompositeNode(grammarAccess.getConfigurationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConfiguration=ruleConfiguration();

            state._fsp--;

             current =iv_ruleConfiguration; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConfiguration"


    // $ANTLR start "ruleConfiguration"
    // InternalConfiguration.g:76:1: ruleConfiguration returns [EObject current=null] : ( ( (lv_importedResources_0_0= ruleSMLImport ) )+ otherlv_1= 'configure' otherlv_2= 'specification' ( (otherlv_3= RULE_ID ) ) (otherlv_4= 'ignored' otherlv_5= 'collaboration' ( ( ruleFQN ) ) (otherlv_7= ',' ( ( ruleFQN ) ) )* )? (otherlv_9= 'auxiliary' otherlv_10= 'collaborations' ( ( ruleFQN ) ) (otherlv_12= ',' ( ( ruleFQN ) ) )* )? ( (lv_instanceModelImports_14_0= ruleXMIImport ) )+ ( (lv_staticRoleBindings_15_0= ruleRoleBindings ) )* ) ;
    public final EObject ruleConfiguration() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        EObject lv_importedResources_0_0 = null;

        EObject lv_instanceModelImports_14_0 = null;

        EObject lv_staticRoleBindings_15_0 = null;


         enterRule(); 
            
        try {
            // InternalConfiguration.g:79:28: ( ( ( (lv_importedResources_0_0= ruleSMLImport ) )+ otherlv_1= 'configure' otherlv_2= 'specification' ( (otherlv_3= RULE_ID ) ) (otherlv_4= 'ignored' otherlv_5= 'collaboration' ( ( ruleFQN ) ) (otherlv_7= ',' ( ( ruleFQN ) ) )* )? (otherlv_9= 'auxiliary' otherlv_10= 'collaborations' ( ( ruleFQN ) ) (otherlv_12= ',' ( ( ruleFQN ) ) )* )? ( (lv_instanceModelImports_14_0= ruleXMIImport ) )+ ( (lv_staticRoleBindings_15_0= ruleRoleBindings ) )* ) )
            // InternalConfiguration.g:80:1: ( ( (lv_importedResources_0_0= ruleSMLImport ) )+ otherlv_1= 'configure' otherlv_2= 'specification' ( (otherlv_3= RULE_ID ) ) (otherlv_4= 'ignored' otherlv_5= 'collaboration' ( ( ruleFQN ) ) (otherlv_7= ',' ( ( ruleFQN ) ) )* )? (otherlv_9= 'auxiliary' otherlv_10= 'collaborations' ( ( ruleFQN ) ) (otherlv_12= ',' ( ( ruleFQN ) ) )* )? ( (lv_instanceModelImports_14_0= ruleXMIImport ) )+ ( (lv_staticRoleBindings_15_0= ruleRoleBindings ) )* )
            {
            // InternalConfiguration.g:80:1: ( ( (lv_importedResources_0_0= ruleSMLImport ) )+ otherlv_1= 'configure' otherlv_2= 'specification' ( (otherlv_3= RULE_ID ) ) (otherlv_4= 'ignored' otherlv_5= 'collaboration' ( ( ruleFQN ) ) (otherlv_7= ',' ( ( ruleFQN ) ) )* )? (otherlv_9= 'auxiliary' otherlv_10= 'collaborations' ( ( ruleFQN ) ) (otherlv_12= ',' ( ( ruleFQN ) ) )* )? ( (lv_instanceModelImports_14_0= ruleXMIImport ) )+ ( (lv_staticRoleBindings_15_0= ruleRoleBindings ) )* )
            // InternalConfiguration.g:80:2: ( (lv_importedResources_0_0= ruleSMLImport ) )+ otherlv_1= 'configure' otherlv_2= 'specification' ( (otherlv_3= RULE_ID ) ) (otherlv_4= 'ignored' otherlv_5= 'collaboration' ( ( ruleFQN ) ) (otherlv_7= ',' ( ( ruleFQN ) ) )* )? (otherlv_9= 'auxiliary' otherlv_10= 'collaborations' ( ( ruleFQN ) ) (otherlv_12= ',' ( ( ruleFQN ) ) )* )? ( (lv_instanceModelImports_14_0= ruleXMIImport ) )+ ( (lv_staticRoleBindings_15_0= ruleRoleBindings ) )*
            {
            // InternalConfiguration.g:80:2: ( (lv_importedResources_0_0= ruleSMLImport ) )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==29) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalConfiguration.g:81:1: (lv_importedResources_0_0= ruleSMLImport )
            	    {
            	    // InternalConfiguration.g:81:1: (lv_importedResources_0_0= ruleSMLImport )
            	    // InternalConfiguration.g:82:3: lv_importedResources_0_0= ruleSMLImport
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getConfigurationAccess().getImportedResourcesSMLImportParserRuleCall_0_0()); 
            	    	    
            	    pushFollow(FOLLOW_3);
            	    lv_importedResources_0_0=ruleSMLImport();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getConfigurationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"importedResources",
            	            		lv_importedResources_0_0, 
            	            		"org.scenariotools.sml.runtime.Configuration.SMLImport");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);

            otherlv_1=(Token)match(input,11,FOLLOW_4); 

                	newLeafNode(otherlv_1, grammarAccess.getConfigurationAccess().getConfigureKeyword_1());
                
            otherlv_2=(Token)match(input,12,FOLLOW_5); 

                	newLeafNode(otherlv_2, grammarAccess.getConfigurationAccess().getSpecificationKeyword_2());
                
            // InternalConfiguration.g:106:1: ( (otherlv_3= RULE_ID ) )
            // InternalConfiguration.g:107:1: (otherlv_3= RULE_ID )
            {
            // InternalConfiguration.g:107:1: (otherlv_3= RULE_ID )
            // InternalConfiguration.g:108:3: otherlv_3= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getConfigurationRule());
            	        }
                    
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_6); 

            		newLeafNode(otherlv_3, grammarAccess.getConfigurationAccess().getSpecificationSpecificationCrossReference_3_0()); 
            	

            }


            }

            // InternalConfiguration.g:119:2: (otherlv_4= 'ignored' otherlv_5= 'collaboration' ( ( ruleFQN ) ) (otherlv_7= ',' ( ( ruleFQN ) ) )* )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==13) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalConfiguration.g:119:4: otherlv_4= 'ignored' otherlv_5= 'collaboration' ( ( ruleFQN ) ) (otherlv_7= ',' ( ( ruleFQN ) ) )*
                    {
                    otherlv_4=(Token)match(input,13,FOLLOW_7); 

                        	newLeafNode(otherlv_4, grammarAccess.getConfigurationAccess().getIgnoredKeyword_4_0());
                        
                    otherlv_5=(Token)match(input,14,FOLLOW_5); 

                        	newLeafNode(otherlv_5, grammarAccess.getConfigurationAccess().getCollaborationKeyword_4_1());
                        
                    // InternalConfiguration.g:127:1: ( ( ruleFQN ) )
                    // InternalConfiguration.g:128:1: ( ruleFQN )
                    {
                    // InternalConfiguration.g:128:1: ( ruleFQN )
                    // InternalConfiguration.g:129:3: ruleFQN
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getConfigurationRule());
                    	        }
                            
                     
                    	        newCompositeNode(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsCollaborationCrossReference_4_2_0()); 
                    	    
                    pushFollow(FOLLOW_8);
                    ruleFQN();

                    state._fsp--;

                     
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalConfiguration.g:142:2: (otherlv_7= ',' ( ( ruleFQN ) ) )*
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( (LA2_0==15) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // InternalConfiguration.g:142:4: otherlv_7= ',' ( ( ruleFQN ) )
                    	    {
                    	    otherlv_7=(Token)match(input,15,FOLLOW_5); 

                    	        	newLeafNode(otherlv_7, grammarAccess.getConfigurationAccess().getCommaKeyword_4_3_0());
                    	        
                    	    // InternalConfiguration.g:146:1: ( ( ruleFQN ) )
                    	    // InternalConfiguration.g:147:1: ( ruleFQN )
                    	    {
                    	    // InternalConfiguration.g:147:1: ( ruleFQN )
                    	    // InternalConfiguration.g:148:3: ruleFQN
                    	    {

                    	    			if (current==null) {
                    	    	            current = createModelElement(grammarAccess.getConfigurationRule());
                    	    	        }
                    	            
                    	     
                    	    	        newCompositeNode(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsCollaborationCrossReference_4_3_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_8);
                    	    ruleFQN();

                    	    state._fsp--;

                    	     
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop2;
                        }
                    } while (true);


                    }
                    break;

            }

            // InternalConfiguration.g:161:6: (otherlv_9= 'auxiliary' otherlv_10= 'collaborations' ( ( ruleFQN ) ) (otherlv_12= ',' ( ( ruleFQN ) ) )* )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==16) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalConfiguration.g:161:8: otherlv_9= 'auxiliary' otherlv_10= 'collaborations' ( ( ruleFQN ) ) (otherlv_12= ',' ( ( ruleFQN ) ) )*
                    {
                    otherlv_9=(Token)match(input,16,FOLLOW_9); 

                        	newLeafNode(otherlv_9, grammarAccess.getConfigurationAccess().getAuxiliaryKeyword_5_0());
                        
                    otherlv_10=(Token)match(input,17,FOLLOW_5); 

                        	newLeafNode(otherlv_10, grammarAccess.getConfigurationAccess().getCollaborationsKeyword_5_1());
                        
                    // InternalConfiguration.g:169:1: ( ( ruleFQN ) )
                    // InternalConfiguration.g:170:1: ( ruleFQN )
                    {
                    // InternalConfiguration.g:170:1: ( ruleFQN )
                    // InternalConfiguration.g:171:3: ruleFQN
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getConfigurationRule());
                    	        }
                            
                     
                    	        newCompositeNode(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsCollaborationCrossReference_5_2_0()); 
                    	    
                    pushFollow(FOLLOW_8);
                    ruleFQN();

                    state._fsp--;

                     
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalConfiguration.g:184:2: (otherlv_12= ',' ( ( ruleFQN ) ) )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==15) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // InternalConfiguration.g:184:4: otherlv_12= ',' ( ( ruleFQN ) )
                    	    {
                    	    otherlv_12=(Token)match(input,15,FOLLOW_5); 

                    	        	newLeafNode(otherlv_12, grammarAccess.getConfigurationAccess().getCommaKeyword_5_3_0());
                    	        
                    	    // InternalConfiguration.g:188:1: ( ( ruleFQN ) )
                    	    // InternalConfiguration.g:189:1: ( ruleFQN )
                    	    {
                    	    // InternalConfiguration.g:189:1: ( ruleFQN )
                    	    // InternalConfiguration.g:190:3: ruleFQN
                    	    {

                    	    			if (current==null) {
                    	    	            current = createModelElement(grammarAccess.getConfigurationRule());
                    	    	        }
                    	            
                    	     
                    	    	        newCompositeNode(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsCollaborationCrossReference_5_3_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_8);
                    	    ruleFQN();

                    	    state._fsp--;

                    	     
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);


                    }
                    break;

            }

            // InternalConfiguration.g:203:6: ( (lv_instanceModelImports_14_0= ruleXMIImport ) )+
            int cnt6=0;
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==27) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalConfiguration.g:204:1: (lv_instanceModelImports_14_0= ruleXMIImport )
            	    {
            	    // InternalConfiguration.g:204:1: (lv_instanceModelImports_14_0= ruleXMIImport )
            	    // InternalConfiguration.g:205:3: lv_instanceModelImports_14_0= ruleXMIImport
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getConfigurationAccess().getInstanceModelImportsXMIImportParserRuleCall_6_0()); 
            	    	    
            	    pushFollow(FOLLOW_10);
            	    lv_instanceModelImports_14_0=ruleXMIImport();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getConfigurationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"instanceModelImports",
            	            		lv_instanceModelImports_14_0, 
            	            		"org.scenariotools.sml.runtime.Configuration.XMIImport");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt6 >= 1 ) break loop6;
                        EarlyExitException eee =
                            new EarlyExitException(6, input);
                        throw eee;
                }
                cnt6++;
            } while (true);

            // InternalConfiguration.g:221:3: ( (lv_staticRoleBindings_15_0= ruleRoleBindings ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>=19 && LA7_0<=20)) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalConfiguration.g:222:1: (lv_staticRoleBindings_15_0= ruleRoleBindings )
            	    {
            	    // InternalConfiguration.g:222:1: (lv_staticRoleBindings_15_0= ruleRoleBindings )
            	    // InternalConfiguration.g:223:3: lv_staticRoleBindings_15_0= ruleRoleBindings
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getConfigurationAccess().getStaticRoleBindingsRoleBindingsParserRuleCall_7_0()); 
            	    	    
            	    pushFollow(FOLLOW_11);
            	    lv_staticRoleBindings_15_0=ruleRoleBindings();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getConfigurationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"staticRoleBindings",
            	            		lv_staticRoleBindings_15_0, 
            	            		"org.scenariotools.sml.runtime.Configuration.RoleBindings");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConfiguration"


    // $ANTLR start "entryRuleFQN"
    // InternalConfiguration.g:247:1: entryRuleFQN returns [String current=null] : iv_ruleFQN= ruleFQN EOF ;
    public final String entryRuleFQN() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFQN = null;


        try {
            // InternalConfiguration.g:248:2: (iv_ruleFQN= ruleFQN EOF )
            // InternalConfiguration.g:249:2: iv_ruleFQN= ruleFQN EOF
            {
             newCompositeNode(grammarAccess.getFQNRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFQN=ruleFQN();

            state._fsp--;

             current =iv_ruleFQN.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFQN"


    // $ANTLR start "ruleFQN"
    // InternalConfiguration.g:256:1: ruleFQN returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleFQN() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;

         enterRule(); 
            
        try {
            // InternalConfiguration.g:259:28: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalConfiguration.g:260:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalConfiguration.g:260:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalConfiguration.g:260:6: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_12); 

            		current.merge(this_ID_0);
                
             
                newLeafNode(this_ID_0, grammarAccess.getFQNAccess().getIDTerminalRuleCall_0()); 
                
            // InternalConfiguration.g:267:1: (kw= '.' this_ID_2= RULE_ID )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==18) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalConfiguration.g:268:2: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,18,FOLLOW_5); 

            	            current.merge(kw);
            	            newLeafNode(kw, grammarAccess.getFQNAccess().getFullStopKeyword_1_0()); 
            	        
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_12); 

            	    		current.merge(this_ID_2);
            	        
            	     
            	        newLeafNode(this_ID_2, grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1()); 
            	        

            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFQN"


    // $ANTLR start "entryRuleRoleBindings"
    // InternalConfiguration.g:288:1: entryRuleRoleBindings returns [EObject current=null] : iv_ruleRoleBindings= ruleRoleBindings EOF ;
    public final EObject entryRuleRoleBindings() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRoleBindings = null;


        try {
            // InternalConfiguration.g:289:2: (iv_ruleRoleBindings= ruleRoleBindings EOF )
            // InternalConfiguration.g:290:2: iv_ruleRoleBindings= ruleRoleBindings EOF
            {
             newCompositeNode(grammarAccess.getRoleBindingsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRoleBindings=ruleRoleBindings();

            state._fsp--;

             current =iv_ruleRoleBindings; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoleBindings"


    // $ANTLR start "ruleRoleBindings"
    // InternalConfiguration.g:297:1: ruleRoleBindings returns [EObject current=null] : ( (otherlv_0= 'rolebindings' | (otherlv_1= 'role' otherlv_2= 'bindings' ) ) otherlv_3= 'for' otherlv_4= 'collaboration' ( ( ruleFQN ) ) otherlv_6= '{' ( (lv_bindings_7_0= ruleRoleAssignment ) )* otherlv_8= '}' ) ;
    public final EObject ruleRoleBindings() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_bindings_7_0 = null;


         enterRule(); 
            
        try {
            // InternalConfiguration.g:300:28: ( ( (otherlv_0= 'rolebindings' | (otherlv_1= 'role' otherlv_2= 'bindings' ) ) otherlv_3= 'for' otherlv_4= 'collaboration' ( ( ruleFQN ) ) otherlv_6= '{' ( (lv_bindings_7_0= ruleRoleAssignment ) )* otherlv_8= '}' ) )
            // InternalConfiguration.g:301:1: ( (otherlv_0= 'rolebindings' | (otherlv_1= 'role' otherlv_2= 'bindings' ) ) otherlv_3= 'for' otherlv_4= 'collaboration' ( ( ruleFQN ) ) otherlv_6= '{' ( (lv_bindings_7_0= ruleRoleAssignment ) )* otherlv_8= '}' )
            {
            // InternalConfiguration.g:301:1: ( (otherlv_0= 'rolebindings' | (otherlv_1= 'role' otherlv_2= 'bindings' ) ) otherlv_3= 'for' otherlv_4= 'collaboration' ( ( ruleFQN ) ) otherlv_6= '{' ( (lv_bindings_7_0= ruleRoleAssignment ) )* otherlv_8= '}' )
            // InternalConfiguration.g:301:2: (otherlv_0= 'rolebindings' | (otherlv_1= 'role' otherlv_2= 'bindings' ) ) otherlv_3= 'for' otherlv_4= 'collaboration' ( ( ruleFQN ) ) otherlv_6= '{' ( (lv_bindings_7_0= ruleRoleAssignment ) )* otherlv_8= '}'
            {
            // InternalConfiguration.g:301:2: (otherlv_0= 'rolebindings' | (otherlv_1= 'role' otherlv_2= 'bindings' ) )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==19) ) {
                alt9=1;
            }
            else if ( (LA9_0==20) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalConfiguration.g:301:4: otherlv_0= 'rolebindings'
                    {
                    otherlv_0=(Token)match(input,19,FOLLOW_13); 

                        	newLeafNode(otherlv_0, grammarAccess.getRoleBindingsAccess().getRolebindingsKeyword_0_0());
                        

                    }
                    break;
                case 2 :
                    // InternalConfiguration.g:306:6: (otherlv_1= 'role' otherlv_2= 'bindings' )
                    {
                    // InternalConfiguration.g:306:6: (otherlv_1= 'role' otherlv_2= 'bindings' )
                    // InternalConfiguration.g:306:8: otherlv_1= 'role' otherlv_2= 'bindings'
                    {
                    otherlv_1=(Token)match(input,20,FOLLOW_14); 

                        	newLeafNode(otherlv_1, grammarAccess.getRoleBindingsAccess().getRoleKeyword_0_1_0());
                        
                    otherlv_2=(Token)match(input,21,FOLLOW_13); 

                        	newLeafNode(otherlv_2, grammarAccess.getRoleBindingsAccess().getBindingsKeyword_0_1_1());
                        

                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,22,FOLLOW_7); 

                	newLeafNode(otherlv_3, grammarAccess.getRoleBindingsAccess().getForKeyword_1());
                
            otherlv_4=(Token)match(input,14,FOLLOW_5); 

                	newLeafNode(otherlv_4, grammarAccess.getRoleBindingsAccess().getCollaborationKeyword_2());
                
            // InternalConfiguration.g:322:1: ( ( ruleFQN ) )
            // InternalConfiguration.g:323:1: ( ruleFQN )
            {
            // InternalConfiguration.g:323:1: ( ruleFQN )
            // InternalConfiguration.g:324:3: ruleFQN
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getRoleBindingsRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getRoleBindingsAccess().getCollaborationCollaborationCrossReference_3_0()); 
            	    
            pushFollow(FOLLOW_15);
            ruleFQN();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_6=(Token)match(input,23,FOLLOW_16); 

                	newLeafNode(otherlv_6, grammarAccess.getRoleBindingsAccess().getLeftCurlyBracketKeyword_4());
                
            // InternalConfiguration.g:341:1: ( (lv_bindings_7_0= ruleRoleAssignment ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==25) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalConfiguration.g:342:1: (lv_bindings_7_0= ruleRoleAssignment )
            	    {
            	    // InternalConfiguration.g:342:1: (lv_bindings_7_0= ruleRoleAssignment )
            	    // InternalConfiguration.g:343:3: lv_bindings_7_0= ruleRoleAssignment
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getRoleBindingsAccess().getBindingsRoleAssignmentParserRuleCall_5_0()); 
            	    	    
            	    pushFollow(FOLLOW_16);
            	    lv_bindings_7_0=ruleRoleAssignment();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getRoleBindingsRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"bindings",
            	            		lv_bindings_7_0, 
            	            		"org.scenariotools.sml.runtime.Configuration.RoleAssignment");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            otherlv_8=(Token)match(input,24,FOLLOW_2); 

                	newLeafNode(otherlv_8, grammarAccess.getRoleBindingsAccess().getRightCurlyBracketKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoleBindings"


    // $ANTLR start "entryRuleRoleAssignment"
    // InternalConfiguration.g:371:1: entryRuleRoleAssignment returns [EObject current=null] : iv_ruleRoleAssignment= ruleRoleAssignment EOF ;
    public final EObject entryRuleRoleAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRoleAssignment = null;


        try {
            // InternalConfiguration.g:372:2: (iv_ruleRoleAssignment= ruleRoleAssignment EOF )
            // InternalConfiguration.g:373:2: iv_ruleRoleAssignment= ruleRoleAssignment EOF
            {
             newCompositeNode(grammarAccess.getRoleAssignmentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRoleAssignment=ruleRoleAssignment();

            state._fsp--;

             current =iv_ruleRoleAssignment; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoleAssignment"


    // $ANTLR start "ruleRoleAssignment"
    // InternalConfiguration.g:380:1: ruleRoleAssignment returns [EObject current=null] : (otherlv_0= 'object' ( ( ruleFQN ) ) otherlv_2= 'plays' otherlv_3= 'role' ( (otherlv_4= RULE_ID ) ) ) ;
    public final EObject ruleRoleAssignment() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;

         enterRule(); 
            
        try {
            // InternalConfiguration.g:383:28: ( (otherlv_0= 'object' ( ( ruleFQN ) ) otherlv_2= 'plays' otherlv_3= 'role' ( (otherlv_4= RULE_ID ) ) ) )
            // InternalConfiguration.g:384:1: (otherlv_0= 'object' ( ( ruleFQN ) ) otherlv_2= 'plays' otherlv_3= 'role' ( (otherlv_4= RULE_ID ) ) )
            {
            // InternalConfiguration.g:384:1: (otherlv_0= 'object' ( ( ruleFQN ) ) otherlv_2= 'plays' otherlv_3= 'role' ( (otherlv_4= RULE_ID ) ) )
            // InternalConfiguration.g:384:3: otherlv_0= 'object' ( ( ruleFQN ) ) otherlv_2= 'plays' otherlv_3= 'role' ( (otherlv_4= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,25,FOLLOW_5); 

                	newLeafNode(otherlv_0, grammarAccess.getRoleAssignmentAccess().getObjectKeyword_0());
                
            // InternalConfiguration.g:388:1: ( ( ruleFQN ) )
            // InternalConfiguration.g:389:1: ( ruleFQN )
            {
            // InternalConfiguration.g:389:1: ( ruleFQN )
            // InternalConfiguration.g:390:3: ruleFQN
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getRoleAssignmentRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getRoleAssignmentAccess().getObjectEObjectCrossReference_1_0()); 
            	    
            pushFollow(FOLLOW_17);
            ruleFQN();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,26,FOLLOW_18); 

                	newLeafNode(otherlv_2, grammarAccess.getRoleAssignmentAccess().getPlaysKeyword_2());
                
            otherlv_3=(Token)match(input,20,FOLLOW_5); 

                	newLeafNode(otherlv_3, grammarAccess.getRoleAssignmentAccess().getRoleKeyword_3());
                
            // InternalConfiguration.g:411:1: ( (otherlv_4= RULE_ID ) )
            // InternalConfiguration.g:412:1: (otherlv_4= RULE_ID )
            {
            // InternalConfiguration.g:412:1: (otherlv_4= RULE_ID )
            // InternalConfiguration.g:413:3: otherlv_4= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getRoleAssignmentRule());
            	        }
                    
            otherlv_4=(Token)match(input,RULE_ID,FOLLOW_2); 

            		newLeafNode(otherlv_4, grammarAccess.getRoleAssignmentAccess().getRoleRoleCrossReference_4_0()); 
            	

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoleAssignment"


    // $ANTLR start "entryRuleXMIImport"
    // InternalConfiguration.g:432:1: entryRuleXMIImport returns [EObject current=null] : iv_ruleXMIImport= ruleXMIImport EOF ;
    public final EObject entryRuleXMIImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXMIImport = null;


        try {
            // InternalConfiguration.g:433:2: (iv_ruleXMIImport= ruleXMIImport EOF )
            // InternalConfiguration.g:434:2: iv_ruleXMIImport= ruleXMIImport EOF
            {
             newCompositeNode(grammarAccess.getXMIImportRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXMIImport=ruleXMIImport();

            state._fsp--;

             current =iv_ruleXMIImport; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXMIImport"


    // $ANTLR start "ruleXMIImport"
    // InternalConfiguration.g:441:1: ruleXMIImport returns [EObject current=null] : (otherlv_0= 'use' otherlv_1= 'instancemodel' ( (lv_importURI_2_0= RULE_STRING ) ) ) ;
    public final EObject ruleXMIImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_importURI_2_0=null;

         enterRule(); 
            
        try {
            // InternalConfiguration.g:444:28: ( (otherlv_0= 'use' otherlv_1= 'instancemodel' ( (lv_importURI_2_0= RULE_STRING ) ) ) )
            // InternalConfiguration.g:445:1: (otherlv_0= 'use' otherlv_1= 'instancemodel' ( (lv_importURI_2_0= RULE_STRING ) ) )
            {
            // InternalConfiguration.g:445:1: (otherlv_0= 'use' otherlv_1= 'instancemodel' ( (lv_importURI_2_0= RULE_STRING ) ) )
            // InternalConfiguration.g:445:3: otherlv_0= 'use' otherlv_1= 'instancemodel' ( (lv_importURI_2_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,27,FOLLOW_19); 

                	newLeafNode(otherlv_0, grammarAccess.getXMIImportAccess().getUseKeyword_0());
                
            otherlv_1=(Token)match(input,28,FOLLOW_20); 

                	newLeafNode(otherlv_1, grammarAccess.getXMIImportAccess().getInstancemodelKeyword_1());
                
            // InternalConfiguration.g:453:1: ( (lv_importURI_2_0= RULE_STRING ) )
            // InternalConfiguration.g:454:1: (lv_importURI_2_0= RULE_STRING )
            {
            // InternalConfiguration.g:454:1: (lv_importURI_2_0= RULE_STRING )
            // InternalConfiguration.g:455:3: lv_importURI_2_0= RULE_STRING
            {
            lv_importURI_2_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            			newLeafNode(lv_importURI_2_0, grammarAccess.getXMIImportAccess().getImportURISTRINGTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getXMIImportRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"importURI",
                    		lv_importURI_2_0, 
                    		"org.eclipse.xtext.common.Terminals.STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXMIImport"


    // $ANTLR start "entryRuleSMLImport"
    // InternalConfiguration.g:479:1: entryRuleSMLImport returns [EObject current=null] : iv_ruleSMLImport= ruleSMLImport EOF ;
    public final EObject entryRuleSMLImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSMLImport = null;


        try {
            // InternalConfiguration.g:480:2: (iv_ruleSMLImport= ruleSMLImport EOF )
            // InternalConfiguration.g:481:2: iv_ruleSMLImport= ruleSMLImport EOF
            {
             newCompositeNode(grammarAccess.getSMLImportRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSMLImport=ruleSMLImport();

            state._fsp--;

             current =iv_ruleSMLImport; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSMLImport"


    // $ANTLR start "ruleSMLImport"
    // InternalConfiguration.g:488:1: ruleSMLImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleSMLImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_importURI_1_0=null;

         enterRule(); 
            
        try {
            // InternalConfiguration.g:491:28: ( (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) )
            // InternalConfiguration.g:492:1: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            {
            // InternalConfiguration.g:492:1: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            // InternalConfiguration.g:492:3: otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,29,FOLLOW_20); 

                	newLeafNode(otherlv_0, grammarAccess.getSMLImportAccess().getImportKeyword_0());
                
            // InternalConfiguration.g:496:1: ( (lv_importURI_1_0= RULE_STRING ) )
            // InternalConfiguration.g:497:1: (lv_importURI_1_0= RULE_STRING )
            {
            // InternalConfiguration.g:497:1: (lv_importURI_1_0= RULE_STRING )
            // InternalConfiguration.g:498:3: lv_importURI_1_0= RULE_STRING
            {
            lv_importURI_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            			newLeafNode(lv_importURI_1_0, grammarAccess.getSMLImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getSMLImportRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"importURI",
                    		lv_importURI_1_0, 
                    		"org.eclipse.xtext.common.Terminals.STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSMLImport"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000020000800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000008012000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x000000000801A000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000008192002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000180002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000003000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000000020L});

}