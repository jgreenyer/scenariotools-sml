/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.configuration.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.scenariotools.sml.runtime.configuration.Configuration;

public class ConfigurationUtil {

	public static Configuration loadConfiguration(String location) {
		return loadConfiguration(URI.createURI(location));
	}

	public static Configuration loadConfiguration(URI uri) {
		final ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getLoadOptions().put(XMIResource.OPTION_MISSING_PACKAGE_HANDLER,
				new CustomMissingPackageHandler(resourceSet));
		Resource runconfigResource = resourceSet.getResource(uri, true);
		Configuration c = (Configuration) runconfigResource.getContents().get(0);
		return c;
	}
}
