/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.configuration;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.scenariotools.sml.runtime.configuration.ConfigurationFactory
 * @model kind="package"
 * @generated
 */
public interface ConfigurationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "configuration";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.scenariotools.org/sml/runtime/Configuration";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "configuration";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ConfigurationPackage eINSTANCE = org.scenariotools.sml.runtime.configuration.impl.ConfigurationPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.configuration.impl.ConfigurationImpl <em>Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.configuration.impl.ConfigurationImpl
	 * @see org.scenariotools.sml.runtime.configuration.impl.ConfigurationPackageImpl#getConfiguration()
	 * @generated
	 */
	int CONFIGURATION = 0;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__SPECIFICATION = 0;

	/**
	 * The feature id for the '<em><b>Instance Model Imports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__INSTANCE_MODEL_IMPORTS = 1;

	/**
	 * The feature id for the '<em><b>Instance Model Root Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__INSTANCE_MODEL_ROOT_OBJECTS = 2;

	/**
	 * The feature id for the '<em><b>Static Role Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__STATIC_ROLE_BINDINGS = 3;

	/**
	 * The feature id for the '<em><b>Ignored Collaborations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__IGNORED_COLLABORATIONS = 4;

	/**
	 * The feature id for the '<em><b>Auxiliary Collaborations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__AUXILIARY_COLLABORATIONS = 5;

	/**
	 * The feature id for the '<em><b>Considered Collaborations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__CONSIDERED_COLLABORATIONS = 6;

	/**
	 * The feature id for the '<em><b>Imported Resources</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__IMPORTED_RESOURCES = 7;

	/**
	 * The number of structural features of the '<em>Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_FEATURE_COUNT = 8;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.configuration.impl.RoleBindingsImpl <em>Role Bindings</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.configuration.impl.RoleBindingsImpl
	 * @see org.scenariotools.sml.runtime.configuration.impl.ConfigurationPackageImpl#getRoleBindings()
	 * @generated
	 */
	int ROLE_BINDINGS = 1;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_BINDINGS__COLLABORATION = 0;

	/**
	 * The feature id for the '<em><b>Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_BINDINGS__BINDINGS = 1;

	/**
	 * The number of structural features of the '<em>Role Bindings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_BINDINGS_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.configuration.impl.RoleAssignmentImpl <em>Role Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.configuration.impl.RoleAssignmentImpl
	 * @see org.scenariotools.sml.runtime.configuration.impl.ConfigurationPackageImpl#getRoleAssignment()
	 * @generated
	 */
	int ROLE_ASSIGNMENT = 2;

	/**
	 * The feature id for the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_ASSIGNMENT__OBJECT = 0;

	/**
	 * The feature id for the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_ASSIGNMENT__ROLE = 1;

	/**
	 * The number of structural features of the '<em>Role Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_ASSIGNMENT_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.configuration.impl.ImportImpl <em>Import</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.configuration.impl.ImportImpl
	 * @see org.scenariotools.sml.runtime.configuration.impl.ConfigurationPackageImpl#getImport()
	 * @generated
	 */
	int IMPORT = 3;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT__IMPORT_URI = 0;

	/**
	 * The number of structural features of the '<em>Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_FEATURE_COUNT = 1;


	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.configuration.Configuration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Configuration</em>'.
	 * @see org.scenariotools.sml.runtime.configuration.Configuration
	 * @generated
	 */
	EClass getConfiguration();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.configuration.Configuration#getSpecification <em>Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Specification</em>'.
	 * @see org.scenariotools.sml.runtime.configuration.Configuration#getSpecification()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_Specification();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.configuration.Configuration#getInstanceModelImports <em>Instance Model Imports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Instance Model Imports</em>'.
	 * @see org.scenariotools.sml.runtime.configuration.Configuration#getInstanceModelImports()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_InstanceModelImports();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.configuration.Configuration#getInstanceModelRootObjects <em>Instance Model Root Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Instance Model Root Objects</em>'.
	 * @see org.scenariotools.sml.runtime.configuration.Configuration#getInstanceModelRootObjects()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_InstanceModelRootObjects();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.configuration.Configuration#getStaticRoleBindings <em>Static Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Static Role Bindings</em>'.
	 * @see org.scenariotools.sml.runtime.configuration.Configuration#getStaticRoleBindings()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_StaticRoleBindings();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.configuration.Configuration#getIgnoredCollaborations <em>Ignored Collaborations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Ignored Collaborations</em>'.
	 * @see org.scenariotools.sml.runtime.configuration.Configuration#getIgnoredCollaborations()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_IgnoredCollaborations();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.configuration.Configuration#getAuxiliaryCollaborations <em>Auxiliary Collaborations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Auxiliary Collaborations</em>'.
	 * @see org.scenariotools.sml.runtime.configuration.Configuration#getAuxiliaryCollaborations()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_AuxiliaryCollaborations();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.configuration.Configuration#getImportedResources <em>Imported Resources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imported Resources</em>'.
	 * @see org.scenariotools.sml.runtime.configuration.Configuration#getImportedResources()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_ImportedResources();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.configuration.Configuration#getConsideredCollaborations <em>Considered Collaborations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Considered Collaborations</em>'.
	 * @see org.scenariotools.sml.runtime.configuration.Configuration#getConsideredCollaborations()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_ConsideredCollaborations();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.configuration.RoleBindings <em>Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Bindings</em>'.
	 * @see org.scenariotools.sml.runtime.configuration.RoleBindings
	 * @generated
	 */
	EClass getRoleBindings();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.configuration.RoleBindings#getCollaboration <em>Collaboration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Collaboration</em>'.
	 * @see org.scenariotools.sml.runtime.configuration.RoleBindings#getCollaboration()
	 * @see #getRoleBindings()
	 * @generated
	 */
	EReference getRoleBindings_Collaboration();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.configuration.RoleBindings#getBindings <em>Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Bindings</em>'.
	 * @see org.scenariotools.sml.runtime.configuration.RoleBindings#getBindings()
	 * @see #getRoleBindings()
	 * @generated
	 */
	EReference getRoleBindings_Bindings();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.configuration.RoleAssignment <em>Role Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Assignment</em>'.
	 * @see org.scenariotools.sml.runtime.configuration.RoleAssignment
	 * @generated
	 */
	EClass getRoleAssignment();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.configuration.RoleAssignment#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object</em>'.
	 * @see org.scenariotools.sml.runtime.configuration.RoleAssignment#getObject()
	 * @see #getRoleAssignment()
	 * @generated
	 */
	EReference getRoleAssignment_Object();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.configuration.RoleAssignment#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role</em>'.
	 * @see org.scenariotools.sml.runtime.configuration.RoleAssignment#getRole()
	 * @see #getRoleAssignment()
	 * @generated
	 */
	EReference getRoleAssignment_Role();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.configuration.Import <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Import</em>'.
	 * @see org.scenariotools.sml.runtime.configuration.Import
	 * @generated
	 */
	EClass getImport();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.runtime.configuration.Import#getImportURI <em>Import URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Import URI</em>'.
	 * @see org.scenariotools.sml.runtime.configuration.Import#getImportURI()
	 * @see #getImport()
	 * @generated
	 */
	EAttribute getImport_ImportURI();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ConfigurationFactory getConfigurationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.configuration.impl.ConfigurationImpl <em>Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.configuration.impl.ConfigurationImpl
		 * @see org.scenariotools.sml.runtime.configuration.impl.ConfigurationPackageImpl#getConfiguration()
		 * @generated
		 */
		EClass CONFIGURATION = eINSTANCE.getConfiguration();

		/**
		 * The meta object literal for the '<em><b>Specification</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__SPECIFICATION = eINSTANCE.getConfiguration_Specification();

		/**
		 * The meta object literal for the '<em><b>Instance Model Imports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__INSTANCE_MODEL_IMPORTS = eINSTANCE.getConfiguration_InstanceModelImports();

		/**
		 * The meta object literal for the '<em><b>Instance Model Root Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__INSTANCE_MODEL_ROOT_OBJECTS = eINSTANCE.getConfiguration_InstanceModelRootObjects();

		/**
		 * The meta object literal for the '<em><b>Static Role Bindings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__STATIC_ROLE_BINDINGS = eINSTANCE.getConfiguration_StaticRoleBindings();

		/**
		 * The meta object literal for the '<em><b>Ignored Collaborations</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__IGNORED_COLLABORATIONS = eINSTANCE.getConfiguration_IgnoredCollaborations();

		/**
		 * The meta object literal for the '<em><b>Auxiliary Collaborations</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__AUXILIARY_COLLABORATIONS = eINSTANCE.getConfiguration_AuxiliaryCollaborations();

		/**
		 * The meta object literal for the '<em><b>Imported Resources</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__IMPORTED_RESOURCES = eINSTANCE.getConfiguration_ImportedResources();

		/**
		 * The meta object literal for the '<em><b>Considered Collaborations</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__CONSIDERED_COLLABORATIONS = eINSTANCE.getConfiguration_ConsideredCollaborations();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.configuration.impl.RoleBindingsImpl <em>Role Bindings</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.configuration.impl.RoleBindingsImpl
		 * @see org.scenariotools.sml.runtime.configuration.impl.ConfigurationPackageImpl#getRoleBindings()
		 * @generated
		 */
		EClass ROLE_BINDINGS = eINSTANCE.getRoleBindings();

		/**
		 * The meta object literal for the '<em><b>Collaboration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_BINDINGS__COLLABORATION = eINSTANCE.getRoleBindings_Collaboration();

		/**
		 * The meta object literal for the '<em><b>Bindings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_BINDINGS__BINDINGS = eINSTANCE.getRoleBindings_Bindings();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.configuration.impl.RoleAssignmentImpl <em>Role Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.configuration.impl.RoleAssignmentImpl
		 * @see org.scenariotools.sml.runtime.configuration.impl.ConfigurationPackageImpl#getRoleAssignment()
		 * @generated
		 */
		EClass ROLE_ASSIGNMENT = eINSTANCE.getRoleAssignment();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_ASSIGNMENT__OBJECT = eINSTANCE.getRoleAssignment_Object();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_ASSIGNMENT__ROLE = eINSTANCE.getRoleAssignment_Role();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.configuration.impl.ImportImpl <em>Import</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.configuration.impl.ImportImpl
		 * @see org.scenariotools.sml.runtime.configuration.impl.ConfigurationPackageImpl#getImport()
		 * @generated
		 */
		EClass IMPORT = eINSTANCE.getImport();

		/**
		 * The meta object literal for the '<em><b>Import URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORT__IMPORT_URI = eINSTANCE.getImport_ImportURI();

	}

} //ConfigurationPackage
