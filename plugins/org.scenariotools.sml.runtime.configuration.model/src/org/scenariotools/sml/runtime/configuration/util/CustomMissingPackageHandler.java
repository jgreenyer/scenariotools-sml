/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.configuration.util;

import java.io.IOException;
import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.scenariotools.sml.runtime.configuration.Configuration;

public final class CustomMissingPackageHandler implements XMLResource.MissingPackageHandler {
	private final ResourceSet resourceSet;

	public CustomMissingPackageHandler(ResourceSet resourceSet) {
		this.resourceSet = resourceSet;
	}

	@Override
	public EPackage getPackage(String nsURI) {
		for (Resource r : resourceSet.getResources()) {
			for (Iterator<EObject> it = r.getAllContents(); it.hasNext();) {
				EObject o = it.next();
				if (o instanceof Configuration) {
					Resource packageResource = ((Configuration) o).getSpecification().getDomains().get(0).eResource();
					if (!packageResource.isLoaded()) {
						try {
							packageResource.load(null);
						} catch (IOException e) {
							e.printStackTrace();
							return null;
						}
					}
					for (Iterator<EObject> packageResourceContentIterator = packageResource
							.getAllContents(); packageResourceContentIterator.hasNext();) {
						EObject packageCandidate = packageResourceContentIterator.next();
						if (packageCandidate instanceof EPackage
								&& nsURI.equals(((EPackage) packageCandidate).getNsURI())) {
							return (EPackage) packageCandidate;
						}
					}
				}
			}
		}
		return null;
	}
}