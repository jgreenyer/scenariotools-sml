/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.configuration;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.scenariotools.sml.Collaboration;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Role Bindings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.configuration.RoleBindings#getCollaboration <em>Collaboration</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.configuration.RoleBindings#getBindings <em>Bindings</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.configuration.ConfigurationPackage#getRoleBindings()
 * @model
 * @generated
 */
public interface RoleBindings extends EObject {
	/**
	 * Returns the value of the '<em><b>Collaboration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collaboration</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collaboration</em>' reference.
	 * @see #setCollaboration(Collaboration)
	 * @see org.scenariotools.sml.runtime.configuration.ConfigurationPackage#getRoleBindings_Collaboration()
	 * @model
	 * @generated
	 */
	Collaboration getCollaboration();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.configuration.RoleBindings#getCollaboration <em>Collaboration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Collaboration</em>' reference.
	 * @see #getCollaboration()
	 * @generated
	 */
	void setCollaboration(Collaboration value);

	/**
	 * Returns the value of the '<em><b>Bindings</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.configuration.RoleAssignment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bindings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bindings</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.configuration.ConfigurationPackage#getRoleBindings_Bindings()
	 * @model containment="true"
	 * @generated
	 */
	EList<RoleAssignment> getBindings();

} // RoleBindings
