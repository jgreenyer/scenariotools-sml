/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
grammar InternalCollaboration;

options {
	superClass=AbstractInternalContentAssistParser;
	backtrack=true;
	
}

@lexer::header {
package org.scenariotools.sml.collaboration.ui.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.Lexer;
}

@parser::header {
package org.scenariotools.sml.collaboration.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.scenariotools.sml.collaboration.services.CollaborationGrammarAccess;

}

@parser::members {
 
 	private CollaborationGrammarAccess grammarAccess;
 	
    public void setGrammarAccess(CollaborationGrammarAccess grammarAccess) {
    	this.grammarAccess = grammarAccess;
    }
    
    @Override
    protected Grammar getGrammar() {
    	return grammarAccess.getGrammar();
    }
    
    @Override
    protected String getValueForTokenName(String tokenName) {
    	return tokenName;
    }

}




// Entry rule entryRuleCollaboration
entryRuleCollaboration 
:
{ before(grammarAccess.getCollaborationRule()); }
	 ruleCollaboration
{ after(grammarAccess.getCollaborationRule()); } 
	 EOF 
;

// Rule Collaboration
ruleCollaboration
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getCollaborationAccess().getGroup()); }
(rule__Collaboration__Group__0)
{ after(grammarAccess.getCollaborationAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleFQN
entryRuleFQN 
:
{ before(grammarAccess.getFQNRule()); }
	 ruleFQN
{ after(grammarAccess.getFQNRule()); } 
	 EOF 
;

// Rule FQN
ruleFQN
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getFQNAccess().getGroup()); }
(rule__FQN__Group__0)
{ after(grammarAccess.getFQNAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleRole
entryRuleRole 
:
{ before(grammarAccess.getRoleRule()); }
	 ruleRole
{ after(grammarAccess.getRoleRule()); } 
	 EOF 
;

// Rule Role
ruleRole
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getRoleAccess().getGroup()); }
(rule__Role__Group__0)
{ after(grammarAccess.getRoleAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleScenario
entryRuleScenario 
:
{ before(grammarAccess.getScenarioRule()); }
	 ruleScenario
{ after(grammarAccess.getScenarioRule()); } 
	 EOF 
;

// Rule Scenario
ruleScenario
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getScenarioAccess().getGroup()); }
(rule__Scenario__Group__0)
{ after(grammarAccess.getScenarioAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleRoleBindingConstraint
entryRuleRoleBindingConstraint 
:
{ before(grammarAccess.getRoleBindingConstraintRule()); }
	 ruleRoleBindingConstraint
{ after(grammarAccess.getRoleBindingConstraintRule()); } 
	 EOF 
;

// Rule RoleBindingConstraint
ruleRoleBindingConstraint
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getRoleBindingConstraintAccess().getGroup()); }
(rule__RoleBindingConstraint__Group__0)
{ after(grammarAccess.getRoleBindingConstraintAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleBindingExpression
entryRuleBindingExpression 
:
{ before(grammarAccess.getBindingExpressionRule()); }
	 ruleBindingExpression
{ after(grammarAccess.getBindingExpressionRule()); } 
	 EOF 
;

// Rule BindingExpression
ruleBindingExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getBindingExpressionAccess().getFeatureAccessBindingExpressionParserRuleCall()); }
	ruleFeatureAccessBindingExpression
{ after(grammarAccess.getBindingExpressionAccess().getFeatureAccessBindingExpressionParserRuleCall()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleFeatureAccessBindingExpression
entryRuleFeatureAccessBindingExpression 
:
{ before(grammarAccess.getFeatureAccessBindingExpressionRule()); }
	 ruleFeatureAccessBindingExpression
{ after(grammarAccess.getFeatureAccessBindingExpressionRule()); } 
	 EOF 
;

// Rule FeatureAccessBindingExpression
ruleFeatureAccessBindingExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getFeatureAccessBindingExpressionAccess().getFeatureaccessAssignment()); }
(rule__FeatureAccessBindingExpression__FeatureaccessAssignment)
{ after(grammarAccess.getFeatureAccessBindingExpressionAccess().getFeatureaccessAssignment()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleInteractionFragment
entryRuleInteractionFragment 
:
{ before(grammarAccess.getInteractionFragmentRule()); }
	 ruleInteractionFragment
{ after(grammarAccess.getInteractionFragmentRule()); } 
	 EOF 
;

// Rule InteractionFragment
ruleInteractionFragment
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getInteractionFragmentAccess().getAlternatives()); }
(rule__InteractionFragment__Alternatives)
{ after(grammarAccess.getInteractionFragmentAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleVariableFragment
entryRuleVariableFragment 
:
{ before(grammarAccess.getVariableFragmentRule()); }
	 ruleVariableFragment
{ after(grammarAccess.getVariableFragmentRule()); } 
	 EOF 
;

// Rule VariableFragment
ruleVariableFragment
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getVariableFragmentAccess().getExpressionAssignment()); }
(rule__VariableFragment__ExpressionAssignment)
{ after(grammarAccess.getVariableFragmentAccess().getExpressionAssignment()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleInteraction
entryRuleInteraction 
:
{ before(grammarAccess.getInteractionRule()); }
	 ruleInteraction
{ after(grammarAccess.getInteractionRule()); } 
	 EOF 
;

// Rule Interaction
ruleInteraction
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getInteractionAccess().getGroup()); }
(rule__Interaction__Group__0)
{ after(grammarAccess.getInteractionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleModalMessage
entryRuleModalMessage 
:
{ before(grammarAccess.getModalMessageRule()); }
	 ruleModalMessage
{ after(grammarAccess.getModalMessageRule()); } 
	 EOF 
;

// Rule ModalMessage
ruleModalMessage
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getModalMessageAccess().getGroup()); }
(rule__ModalMessage__Group__0)
{ after(grammarAccess.getModalMessageAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleParameterBinding
entryRuleParameterBinding 
:
{ before(grammarAccess.getParameterBindingRule()); }
	 ruleParameterBinding
{ after(grammarAccess.getParameterBindingRule()); } 
	 EOF 
;

// Rule ParameterBinding
ruleParameterBinding
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getParameterBindingAccess().getBindingExpressionAssignment()); }
(rule__ParameterBinding__BindingExpressionAssignment)
{ after(grammarAccess.getParameterBindingAccess().getBindingExpressionAssignment()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleParameterExpression
entryRuleParameterExpression 
:
{ before(grammarAccess.getParameterExpressionRule()); }
	 ruleParameterExpression
{ after(grammarAccess.getParameterExpressionRule()); } 
	 EOF 
;

// Rule ParameterExpression
ruleParameterExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getParameterExpressionAccess().getAlternatives()); }
(rule__ParameterExpression__Alternatives)
{ after(grammarAccess.getParameterExpressionAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleWildcardParameterExpression
entryRuleWildcardParameterExpression 
:
{ before(grammarAccess.getWildcardParameterExpressionRule()); }
	 ruleWildcardParameterExpression
{ after(grammarAccess.getWildcardParameterExpressionRule()); } 
	 EOF 
;

// Rule WildcardParameterExpression
ruleWildcardParameterExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getWildcardParameterExpressionAccess().getGroup()); }
(rule__WildcardParameterExpression__Group__0)
{ after(grammarAccess.getWildcardParameterExpressionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleValueParameterExpression
entryRuleValueParameterExpression 
:
{ before(grammarAccess.getValueParameterExpressionRule()); }
	 ruleValueParameterExpression
{ after(grammarAccess.getValueParameterExpressionRule()); } 
	 EOF 
;

// Rule ValueParameterExpression
ruleValueParameterExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getValueParameterExpressionAccess().getValueAssignment()); }
(rule__ValueParameterExpression__ValueAssignment)
{ after(grammarAccess.getValueParameterExpressionAccess().getValueAssignment()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleVariableBindingParameterExpression
entryRuleVariableBindingParameterExpression 
:
{ before(grammarAccess.getVariableBindingParameterExpressionRule()); }
	 ruleVariableBindingParameterExpression
{ after(grammarAccess.getVariableBindingParameterExpressionRule()); } 
	 EOF 
;

// Rule VariableBindingParameterExpression
ruleVariableBindingParameterExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getVariableBindingParameterExpressionAccess().getGroup()); }
(rule__VariableBindingParameterExpression__Group__0)
{ after(grammarAccess.getVariableBindingParameterExpressionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleAlternative
entryRuleAlternative 
:
{ before(grammarAccess.getAlternativeRule()); }
	 ruleAlternative
{ after(grammarAccess.getAlternativeRule()); } 
	 EOF 
;

// Rule Alternative
ruleAlternative
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getAlternativeAccess().getGroup()); }
(rule__Alternative__Group__0)
{ after(grammarAccess.getAlternativeAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleCase
entryRuleCase 
:
{ before(grammarAccess.getCaseRule()); }
	 ruleCase
{ after(grammarAccess.getCaseRule()); } 
	 EOF 
;

// Rule Case
ruleCase
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getCaseAccess().getGroup()); }
(rule__Case__Group__0)
{ after(grammarAccess.getCaseAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleLoop
entryRuleLoop 
:
{ before(grammarAccess.getLoopRule()); }
	 ruleLoop
{ after(grammarAccess.getLoopRule()); } 
	 EOF 
;

// Rule Loop
ruleLoop
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getLoopAccess().getGroup()); }
(rule__Loop__Group__0)
{ after(grammarAccess.getLoopAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleParallel
entryRuleParallel 
:
{ before(grammarAccess.getParallelRule()); }
	 ruleParallel
{ after(grammarAccess.getParallelRule()); } 
	 EOF 
;

// Rule Parallel
ruleParallel
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getParallelAccess().getGroup()); }
(rule__Parallel__Group__0)
{ after(grammarAccess.getParallelAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleTimedConditionFragment
entryRuleTimedConditionFragment 
:
{ before(grammarAccess.getTimedConditionFragmentRule()); }
	 ruleTimedConditionFragment
{ after(grammarAccess.getTimedConditionFragmentRule()); } 
	 EOF 
;

// Rule TimedConditionFragment
ruleTimedConditionFragment
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getTimedConditionFragmentAccess().getAlternatives()); }
(rule__TimedConditionFragment__Alternatives)
{ after(grammarAccess.getTimedConditionFragmentAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleConditionFragment
entryRuleConditionFragment 
:
{ before(grammarAccess.getConditionFragmentRule()); }
	 ruleConditionFragment
{ after(grammarAccess.getConditionFragmentRule()); } 
	 EOF 
;

// Rule ConditionFragment
ruleConditionFragment
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getConditionFragmentAccess().getAlternatives()); }
(rule__ConditionFragment__Alternatives)
{ after(grammarAccess.getConditionFragmentAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleWaitCondition
entryRuleWaitCondition 
:
{ before(grammarAccess.getWaitConditionRule()); }
	 ruleWaitCondition
{ after(grammarAccess.getWaitConditionRule()); } 
	 EOF 
;

// Rule WaitCondition
ruleWaitCondition
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getWaitConditionAccess().getGroup()); }
(rule__WaitCondition__Group__0)
{ after(grammarAccess.getWaitConditionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleInterruptCondition
entryRuleInterruptCondition 
:
{ before(grammarAccess.getInterruptConditionRule()); }
	 ruleInterruptCondition
{ after(grammarAccess.getInterruptConditionRule()); } 
	 EOF 
;

// Rule InterruptCondition
ruleInterruptCondition
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getInterruptConditionAccess().getGroup()); }
(rule__InterruptCondition__Group__0)
{ after(grammarAccess.getInterruptConditionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleViolationCondition
entryRuleViolationCondition 
:
{ before(grammarAccess.getViolationConditionRule()); }
	 ruleViolationCondition
{ after(grammarAccess.getViolationConditionRule()); } 
	 EOF 
;

// Rule ViolationCondition
ruleViolationCondition
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getViolationConditionAccess().getGroup()); }
(rule__ViolationCondition__Group__0)
{ after(grammarAccess.getViolationConditionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleTimedWaitCondition
entryRuleTimedWaitCondition 
:
{ before(grammarAccess.getTimedWaitConditionRule()); }
	 ruleTimedWaitCondition
{ after(grammarAccess.getTimedWaitConditionRule()); } 
	 EOF 
;

// Rule TimedWaitCondition
ruleTimedWaitCondition
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getTimedWaitConditionAccess().getGroup()); }
(rule__TimedWaitCondition__Group__0)
{ after(grammarAccess.getTimedWaitConditionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleTimedViolationCondition
entryRuleTimedViolationCondition 
:
{ before(grammarAccess.getTimedViolationConditionRule()); }
	 ruleTimedViolationCondition
{ after(grammarAccess.getTimedViolationConditionRule()); } 
	 EOF 
;

// Rule TimedViolationCondition
ruleTimedViolationCondition
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getTimedViolationConditionAccess().getGroup()); }
(rule__TimedViolationCondition__Group__0)
{ after(grammarAccess.getTimedViolationConditionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleTimedInterruptCondition
entryRuleTimedInterruptCondition 
:
{ before(grammarAccess.getTimedInterruptConditionRule()); }
	 ruleTimedInterruptCondition
{ after(grammarAccess.getTimedInterruptConditionRule()); } 
	 EOF 
;

// Rule TimedInterruptCondition
ruleTimedInterruptCondition
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getTimedInterruptConditionAccess().getGroup()); }
(rule__TimedInterruptCondition__Group__0)
{ after(grammarAccess.getTimedInterruptConditionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleCondition
entryRuleCondition 
:
{ before(grammarAccess.getConditionRule()); }
	 ruleCondition
{ after(grammarAccess.getConditionRule()); } 
	 EOF 
;

// Rule Condition
ruleCondition
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getConditionAccess().getGroup()); }
(rule__Condition__Group__0)
{ after(grammarAccess.getConditionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleConditionExpression
entryRuleConditionExpression 
:
{ before(grammarAccess.getConditionExpressionRule()); }
	 ruleConditionExpression
{ after(grammarAccess.getConditionExpressionRule()); } 
	 EOF 
;

// Rule ConditionExpression
ruleConditionExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getConditionExpressionAccess().getExpressionAssignment()); }
(rule__ConditionExpression__ExpressionAssignment)
{ after(grammarAccess.getConditionExpressionAccess().getExpressionAssignment()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleConstraintBlock
entryRuleConstraintBlock 
:
{ before(grammarAccess.getConstraintBlockRule()); }
	 ruleConstraintBlock
{ after(grammarAccess.getConstraintBlockRule()); } 
	 EOF 
;

// Rule ConstraintBlock
ruleConstraintBlock
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getConstraintBlockAccess().getGroup()); }
(rule__ConstraintBlock__Group__0)
{ after(grammarAccess.getConstraintBlockAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleConstraintMessage
entryRuleConstraintMessage 
:
{ before(grammarAccess.getConstraintMessageRule()); }
	 ruleConstraintMessage
{ after(grammarAccess.getConstraintMessageRule()); } 
	 EOF 
;

// Rule ConstraintMessage
ruleConstraintMessage
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getConstraintMessageAccess().getGroup()); }
(rule__ConstraintMessage__Group__0)
{ after(grammarAccess.getConstraintMessageAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}





// Entry rule entryRuleImport
entryRuleImport 
:
{ before(grammarAccess.getImportRule()); }
	 ruleImport
{ after(grammarAccess.getImportRule()); } 
	 EOF 
;

// Rule Import
ruleImport
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getImportAccess().getGroup()); }
(rule__Import__Group__0)
{ after(grammarAccess.getImportAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleExpressionRegion
entryRuleExpressionRegion 
:
{ before(grammarAccess.getExpressionRegionRule()); }
	 ruleExpressionRegion
{ after(grammarAccess.getExpressionRegionRule()); } 
	 EOF 
;

// Rule ExpressionRegion
ruleExpressionRegion
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getExpressionRegionAccess().getGroup()); }
(rule__ExpressionRegion__Group__0)
{ after(grammarAccess.getExpressionRegionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleExpressionOrRegion
entryRuleExpressionOrRegion 
:
{ before(grammarAccess.getExpressionOrRegionRule()); }
	 ruleExpressionOrRegion
{ after(grammarAccess.getExpressionOrRegionRule()); } 
	 EOF 
;

// Rule ExpressionOrRegion
ruleExpressionOrRegion
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getExpressionOrRegionAccess().getAlternatives()); }
(rule__ExpressionOrRegion__Alternatives)
{ after(grammarAccess.getExpressionOrRegionAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleExpressionAndVariables
entryRuleExpressionAndVariables 
:
{ before(grammarAccess.getExpressionAndVariablesRule()); }
	 ruleExpressionAndVariables
{ after(grammarAccess.getExpressionAndVariablesRule()); } 
	 EOF 
;

// Rule ExpressionAndVariables
ruleExpressionAndVariables
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getExpressionAndVariablesAccess().getAlternatives()); }
(rule__ExpressionAndVariables__Alternatives)
{ after(grammarAccess.getExpressionAndVariablesAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleVariableExpression
entryRuleVariableExpression 
:
{ before(grammarAccess.getVariableExpressionRule()); }
	 ruleVariableExpression
{ after(grammarAccess.getVariableExpressionRule()); } 
	 EOF 
;

// Rule VariableExpression
ruleVariableExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getVariableExpressionAccess().getAlternatives()); }
(rule__VariableExpression__Alternatives)
{ after(grammarAccess.getVariableExpressionAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}





// Entry rule entryRuleVariableAssignment
entryRuleVariableAssignment 
:
{ before(grammarAccess.getVariableAssignmentRule()); }
	 ruleVariableAssignment
{ after(grammarAccess.getVariableAssignmentRule()); } 
	 EOF 
;

// Rule VariableAssignment
ruleVariableAssignment
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getVariableAssignmentAccess().getGroup()); }
(rule__VariableAssignment__Group__0)
{ after(grammarAccess.getVariableAssignmentAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleTypedVariableDeclaration
entryRuleTypedVariableDeclaration 
:
{ before(grammarAccess.getTypedVariableDeclarationRule()); }
	 ruleTypedVariableDeclaration
{ after(grammarAccess.getTypedVariableDeclarationRule()); } 
	 EOF 
;

// Rule TypedVariableDeclaration
ruleTypedVariableDeclaration
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getGroup()); }
(rule__TypedVariableDeclaration__Group__0)
{ after(grammarAccess.getTypedVariableDeclarationAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleClockDeclaration
entryRuleClockDeclaration 
:
{ before(grammarAccess.getClockDeclarationRule()); }
	 ruleClockDeclaration
{ after(grammarAccess.getClockDeclarationRule()); } 
	 EOF 
;

// Rule ClockDeclaration
ruleClockDeclaration
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getClockDeclarationAccess().getGroup()); }
(rule__ClockDeclaration__Group__0)
{ after(grammarAccess.getClockDeclarationAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}





// Entry rule entryRuleClockAssignment
entryRuleClockAssignment 
:
{ before(grammarAccess.getClockAssignmentRule()); }
	 ruleClockAssignment
{ after(grammarAccess.getClockAssignmentRule()); } 
	 EOF 
;

// Rule ClockAssignment
ruleClockAssignment
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getClockAssignmentAccess().getGroup()); }
(rule__ClockAssignment__Group__0)
{ after(grammarAccess.getClockAssignmentAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleExpression
entryRuleExpression 
:
{ before(grammarAccess.getExpressionRule()); }
	 ruleExpression
{ after(grammarAccess.getExpressionRule()); } 
	 EOF 
;

// Rule Expression
ruleExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getExpressionAccess().getImplicationExpressionParserRuleCall()); }
	ruleImplicationExpression
{ after(grammarAccess.getExpressionAccess().getImplicationExpressionParserRuleCall()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleImplicationExpression
entryRuleImplicationExpression 
:
{ before(grammarAccess.getImplicationExpressionRule()); }
	 ruleImplicationExpression
{ after(grammarAccess.getImplicationExpressionRule()); } 
	 EOF 
;

// Rule ImplicationExpression
ruleImplicationExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getImplicationExpressionAccess().getGroup()); }
(rule__ImplicationExpression__Group__0)
{ after(grammarAccess.getImplicationExpressionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleDisjunctionExpression
entryRuleDisjunctionExpression 
:
{ before(grammarAccess.getDisjunctionExpressionRule()); }
	 ruleDisjunctionExpression
{ after(grammarAccess.getDisjunctionExpressionRule()); } 
	 EOF 
;

// Rule DisjunctionExpression
ruleDisjunctionExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getDisjunctionExpressionAccess().getGroup()); }
(rule__DisjunctionExpression__Group__0)
{ after(grammarAccess.getDisjunctionExpressionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleConjunctionExpression
entryRuleConjunctionExpression 
:
{ before(grammarAccess.getConjunctionExpressionRule()); }
	 ruleConjunctionExpression
{ after(grammarAccess.getConjunctionExpressionRule()); } 
	 EOF 
;

// Rule ConjunctionExpression
ruleConjunctionExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getConjunctionExpressionAccess().getGroup()); }
(rule__ConjunctionExpression__Group__0)
{ after(grammarAccess.getConjunctionExpressionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleRelationExpression
entryRuleRelationExpression 
:
{ before(grammarAccess.getRelationExpressionRule()); }
	 ruleRelationExpression
{ after(grammarAccess.getRelationExpressionRule()); } 
	 EOF 
;

// Rule RelationExpression
ruleRelationExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getRelationExpressionAccess().getGroup()); }
(rule__RelationExpression__Group__0)
{ after(grammarAccess.getRelationExpressionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleTimedExpression
entryRuleTimedExpression 
:
{ before(grammarAccess.getTimedExpressionRule()); }
	 ruleTimedExpression
{ after(grammarAccess.getTimedExpressionRule()); } 
	 EOF 
;

// Rule TimedExpression
ruleTimedExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getTimedExpressionAccess().getGroup()); }
(rule__TimedExpression__Group__0)
{ after(grammarAccess.getTimedExpressionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleAdditionExpression
entryRuleAdditionExpression 
:
{ before(grammarAccess.getAdditionExpressionRule()); }
	 ruleAdditionExpression
{ after(grammarAccess.getAdditionExpressionRule()); } 
	 EOF 
;

// Rule AdditionExpression
ruleAdditionExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getAdditionExpressionAccess().getGroup()); }
(rule__AdditionExpression__Group__0)
{ after(grammarAccess.getAdditionExpressionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleMultiplicationExpression
entryRuleMultiplicationExpression 
:
{ before(grammarAccess.getMultiplicationExpressionRule()); }
	 ruleMultiplicationExpression
{ after(grammarAccess.getMultiplicationExpressionRule()); } 
	 EOF 
;

// Rule MultiplicationExpression
ruleMultiplicationExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getMultiplicationExpressionAccess().getGroup()); }
(rule__MultiplicationExpression__Group__0)
{ after(grammarAccess.getMultiplicationExpressionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleNegatedExpression
entryRuleNegatedExpression 
:
{ before(grammarAccess.getNegatedExpressionRule()); }
	 ruleNegatedExpression
{ after(grammarAccess.getNegatedExpressionRule()); } 
	 EOF 
;

// Rule NegatedExpression
ruleNegatedExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getNegatedExpressionAccess().getAlternatives()); }
(rule__NegatedExpression__Alternatives)
{ after(grammarAccess.getNegatedExpressionAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleBasicExpression
entryRuleBasicExpression 
:
{ before(grammarAccess.getBasicExpressionRule()); }
	 ruleBasicExpression
{ after(grammarAccess.getBasicExpressionRule()); } 
	 EOF 
;

// Rule BasicExpression
ruleBasicExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getBasicExpressionAccess().getAlternatives()); }
(rule__BasicExpression__Alternatives)
{ after(grammarAccess.getBasicExpressionAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleValue
entryRuleValue 
:
{ before(grammarAccess.getValueRule()); }
	 ruleValue
{ after(grammarAccess.getValueRule()); } 
	 EOF 
;

// Rule Value
ruleValue
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getValueAccess().getAlternatives()); }
(rule__Value__Alternatives)
{ after(grammarAccess.getValueAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleIntegerValue
entryRuleIntegerValue 
:
{ before(grammarAccess.getIntegerValueRule()); }
	 ruleIntegerValue
{ after(grammarAccess.getIntegerValueRule()); } 
	 EOF 
;

// Rule IntegerValue
ruleIntegerValue
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getIntegerValueAccess().getValueAssignment()); }
(rule__IntegerValue__ValueAssignment)
{ after(grammarAccess.getIntegerValueAccess().getValueAssignment()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleBooleanValue
entryRuleBooleanValue 
:
{ before(grammarAccess.getBooleanValueRule()); }
	 ruleBooleanValue
{ after(grammarAccess.getBooleanValueRule()); } 
	 EOF 
;

// Rule BooleanValue
ruleBooleanValue
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getBooleanValueAccess().getValueAssignment()); }
(rule__BooleanValue__ValueAssignment)
{ after(grammarAccess.getBooleanValueAccess().getValueAssignment()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleStringValue
entryRuleStringValue 
:
{ before(grammarAccess.getStringValueRule()); }
	 ruleStringValue
{ after(grammarAccess.getStringValueRule()); } 
	 EOF 
;

// Rule StringValue
ruleStringValue
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getStringValueAccess().getValueAssignment()); }
(rule__StringValue__ValueAssignment)
{ after(grammarAccess.getStringValueAccess().getValueAssignment()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleEnumValue
entryRuleEnumValue 
:
{ before(grammarAccess.getEnumValueRule()); }
	 ruleEnumValue
{ after(grammarAccess.getEnumValueRule()); } 
	 EOF 
;

// Rule EnumValue
ruleEnumValue
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getEnumValueAccess().getGroup()); }
(rule__EnumValue__Group__0)
{ after(grammarAccess.getEnumValueAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleNullValue
entryRuleNullValue 
:
{ before(grammarAccess.getNullValueRule()); }
	 ruleNullValue
{ after(grammarAccess.getNullValueRule()); } 
	 EOF 
;

// Rule NullValue
ruleNullValue
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getNullValueAccess().getGroup()); }
(rule__NullValue__Group__0)
{ after(grammarAccess.getNullValueAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleVariableValue
entryRuleVariableValue 
:
{ before(grammarAccess.getVariableValueRule()); }
	 ruleVariableValue
{ after(grammarAccess.getVariableValueRule()); } 
	 EOF 
;

// Rule VariableValue
ruleVariableValue
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getVariableValueAccess().getValueAssignment()); }
(rule__VariableValue__ValueAssignment)
{ after(grammarAccess.getVariableValueAccess().getValueAssignment()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleCollectionAccess
entryRuleCollectionAccess 
:
{ before(grammarAccess.getCollectionAccessRule()); }
	 ruleCollectionAccess
{ after(grammarAccess.getCollectionAccessRule()); } 
	 EOF 
;

// Rule CollectionAccess
ruleCollectionAccess
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getCollectionAccessAccess().getGroup()); }
(rule__CollectionAccess__Group__0)
{ after(grammarAccess.getCollectionAccessAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleFeatureAccess
entryRuleFeatureAccess 
:
{ before(grammarAccess.getFeatureAccessRule()); }
	 ruleFeatureAccess
{ after(grammarAccess.getFeatureAccessRule()); } 
	 EOF 
;

// Rule FeatureAccess
ruleFeatureAccess
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getFeatureAccessAccess().getGroup()); }
(rule__FeatureAccess__Group__0)
{ after(grammarAccess.getFeatureAccessAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleStructuralFeatureValue
entryRuleStructuralFeatureValue 
:
{ before(grammarAccess.getStructuralFeatureValueRule()); }
	 ruleStructuralFeatureValue
{ after(grammarAccess.getStructuralFeatureValueRule()); } 
	 EOF 
;

// Rule StructuralFeatureValue
ruleStructuralFeatureValue
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getStructuralFeatureValueAccess().getValueAssignment()); }
(rule__StructuralFeatureValue__ValueAssignment)
{ after(grammarAccess.getStructuralFeatureValueAccess().getValueAssignment()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleOperationValue
entryRuleOperationValue 
:
{ before(grammarAccess.getOperationValueRule()); }
	 ruleOperationValue
{ after(grammarAccess.getOperationValueRule()); } 
	 EOF 
;

// Rule OperationValue
ruleOperationValue
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getOperationValueAccess().getValueAssignment()); }
(rule__OperationValue__ValueAssignment)
{ after(grammarAccess.getOperationValueAccess().getValueAssignment()); }
)

;
finally {
	restoreStackSize(stackSize);
}




// Rule ScenarioKind
ruleScenarioKind
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioKindAccess().getAlternatives()); }
(rule__ScenarioKind__Alternatives)
{ after(grammarAccess.getScenarioKindAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Rule ExpectationKind
ruleExpectationKind
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExpectationKindAccess().getAlternatives()); }
(rule__ExpectationKind__Alternatives)
{ after(grammarAccess.getExpectationKindAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Rule CollectionOperation
ruleCollectionOperation
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollectionOperationAccess().getAlternatives()); }
(rule__CollectionOperation__Alternatives)
{ after(grammarAccess.getCollectionOperationAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Rule CollectionModification
ruleCollectionModification
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollectionModificationAccess().getAlternatives()); }
(rule__CollectionModification__Alternatives)
{ after(grammarAccess.getCollectionModificationAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



rule__Role__Alternatives_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRoleAccess().getStaticAssignment_0_0()); }
(rule__Role__StaticAssignment_0_0)
{ after(grammarAccess.getRoleAccess().getStaticAssignment_0_0()); }
)

    |(
{ before(grammarAccess.getRoleAccess().getGroup_0_1()); }
(rule__Role__Group_0_1__0)
{ after(grammarAccess.getRoleAccess().getGroup_0_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Alternatives_4
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getGroup_4_0()); }
(rule__Scenario__Group_4_0__0)
{ after(grammarAccess.getScenarioAccess().getGroup_4_0()); }
)

    |(
{ before(grammarAccess.getScenarioAccess().getGroup_4_1()); }
(rule__Scenario__Group_4_1__0)
{ after(grammarAccess.getScenarioAccess().getGroup_4_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__InteractionFragment__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getInteractionFragmentAccess().getInteractionParserRuleCall_0()); }
	ruleInteraction
{ after(grammarAccess.getInteractionFragmentAccess().getInteractionParserRuleCall_0()); }
)

    |(
{ before(grammarAccess.getInteractionFragmentAccess().getModalMessageParserRuleCall_1()); }
	ruleModalMessage
{ after(grammarAccess.getInteractionFragmentAccess().getModalMessageParserRuleCall_1()); }
)

    |(
{ before(grammarAccess.getInteractionFragmentAccess().getAlternativeParserRuleCall_2()); }
	ruleAlternative
{ after(grammarAccess.getInteractionFragmentAccess().getAlternativeParserRuleCall_2()); }
)

    |(
{ before(grammarAccess.getInteractionFragmentAccess().getLoopParserRuleCall_3()); }
	ruleLoop
{ after(grammarAccess.getInteractionFragmentAccess().getLoopParserRuleCall_3()); }
)

    |(
{ before(grammarAccess.getInteractionFragmentAccess().getParallelParserRuleCall_4()); }
	ruleParallel
{ after(grammarAccess.getInteractionFragmentAccess().getParallelParserRuleCall_4()); }
)

    |(
{ before(grammarAccess.getInteractionFragmentAccess().getConditionFragmentParserRuleCall_5()); }
	ruleConditionFragment
{ after(grammarAccess.getInteractionFragmentAccess().getConditionFragmentParserRuleCall_5()); }
)

    |(
{ before(grammarAccess.getInteractionFragmentAccess().getTimedConditionFragmentParserRuleCall_6()); }
	ruleTimedConditionFragment
{ after(grammarAccess.getInteractionFragmentAccess().getTimedConditionFragmentParserRuleCall_6()); }
)

    |(
{ before(grammarAccess.getInteractionFragmentAccess().getVariableFragmentParserRuleCall_7()); }
	ruleVariableFragment
{ after(grammarAccess.getInteractionFragmentAccess().getVariableFragmentParserRuleCall_7()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__VariableFragment__ExpressionAlternatives_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVariableFragmentAccess().getExpressionTypedVariableDeclarationParserRuleCall_0_0()); }
	ruleTypedVariableDeclaration
{ after(grammarAccess.getVariableFragmentAccess().getExpressionTypedVariableDeclarationParserRuleCall_0_0()); }
)

    |(
{ before(grammarAccess.getVariableFragmentAccess().getExpressionVariableAssignmentParserRuleCall_0_1()); }
	ruleVariableAssignment
{ after(grammarAccess.getVariableFragmentAccess().getExpressionVariableAssignmentParserRuleCall_0_1()); }
)

    |(
{ before(grammarAccess.getVariableFragmentAccess().getExpressionClockDeclarationParserRuleCall_0_2()); }
	ruleClockDeclaration
{ after(grammarAccess.getVariableFragmentAccess().getExpressionClockDeclarationParserRuleCall_0_2()); }
)

    |(
{ before(grammarAccess.getVariableFragmentAccess().getExpressionClockAssignmentParserRuleCall_0_3()); }
	ruleClockAssignment
{ after(grammarAccess.getVariableFragmentAccess().getExpressionClockAssignmentParserRuleCall_0_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ParameterExpression__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getParameterExpressionAccess().getWildcardParameterExpressionParserRuleCall_0()); }
	ruleWildcardParameterExpression
{ after(grammarAccess.getParameterExpressionAccess().getWildcardParameterExpressionParserRuleCall_0()); }
)

    |(
{ before(grammarAccess.getParameterExpressionAccess().getValueParameterExpressionParserRuleCall_1()); }
	ruleValueParameterExpression
{ after(grammarAccess.getParameterExpressionAccess().getValueParameterExpressionParserRuleCall_1()); }
)

    |(
{ before(grammarAccess.getParameterExpressionAccess().getVariableBindingParameterExpressionParserRuleCall_2()); }
	ruleVariableBindingParameterExpression
{ after(grammarAccess.getParameterExpressionAccess().getVariableBindingParameterExpressionParserRuleCall_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__TimedConditionFragment__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedConditionFragmentAccess().getTimedWaitConditionParserRuleCall_0()); }
	ruleTimedWaitCondition
{ after(grammarAccess.getTimedConditionFragmentAccess().getTimedWaitConditionParserRuleCall_0()); }
)

    |(
{ before(grammarAccess.getTimedConditionFragmentAccess().getTimedInterruptConditionParserRuleCall_1()); }
	ruleTimedInterruptCondition
{ after(grammarAccess.getTimedConditionFragmentAccess().getTimedInterruptConditionParserRuleCall_1()); }
)

    |(
{ before(grammarAccess.getTimedConditionFragmentAccess().getTimedViolationConditionParserRuleCall_2()); }
	ruleTimedViolationCondition
{ after(grammarAccess.getTimedConditionFragmentAccess().getTimedViolationConditionParserRuleCall_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ConditionFragment__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConditionFragmentAccess().getWaitConditionParserRuleCall_0()); }
	ruleWaitCondition
{ after(grammarAccess.getConditionFragmentAccess().getWaitConditionParserRuleCall_0()); }
)

    |(
{ before(grammarAccess.getConditionFragmentAccess().getInterruptConditionParserRuleCall_1()); }
	ruleInterruptCondition
{ after(grammarAccess.getConditionFragmentAccess().getInterruptConditionParserRuleCall_1()); }
)

    |(
{ before(grammarAccess.getConditionFragmentAccess().getViolationConditionParserRuleCall_2()); }
	ruleViolationCondition
{ after(grammarAccess.getConditionFragmentAccess().getViolationConditionParserRuleCall_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintBlock__Alternatives_3
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintBlockAccess().getGroup_3_0()); }
(rule__ConstraintBlock__Group_3_0__0)
{ after(grammarAccess.getConstraintBlockAccess().getGroup_3_0()); }
)

    |(
{ before(grammarAccess.getConstraintBlockAccess().getGroup_3_1()); }
(rule__ConstraintBlock__Group_3_1__0)
{ after(grammarAccess.getConstraintBlockAccess().getGroup_3_1()); }
)

    |(
{ before(grammarAccess.getConstraintBlockAccess().getGroup_3_2()); }
(rule__ConstraintBlock__Group_3_2__0)
{ after(grammarAccess.getConstraintBlockAccess().getGroup_3_2()); }
)

    |(
{ before(grammarAccess.getConstraintBlockAccess().getGroup_3_3()); }
(rule__ConstraintBlock__Group_3_3__0)
{ after(grammarAccess.getConstraintBlockAccess().getGroup_3_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ExpressionOrRegion__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExpressionOrRegionAccess().getExpressionRegionParserRuleCall_0()); }
	ruleExpressionRegion
{ after(grammarAccess.getExpressionOrRegionAccess().getExpressionRegionParserRuleCall_0()); }
)

    |(
{ before(grammarAccess.getExpressionOrRegionAccess().getExpressionAndVariablesParserRuleCall_1()); }
	ruleExpressionAndVariables
{ after(grammarAccess.getExpressionOrRegionAccess().getExpressionAndVariablesParserRuleCall_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ExpressionAndVariables__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExpressionAndVariablesAccess().getVariableExpressionParserRuleCall_0()); }
	ruleVariableExpression
{ after(grammarAccess.getExpressionAndVariablesAccess().getVariableExpressionParserRuleCall_0()); }
)

    |(
{ before(grammarAccess.getExpressionAndVariablesAccess().getExpressionParserRuleCall_1()); }
	ruleExpression
{ after(grammarAccess.getExpressionAndVariablesAccess().getExpressionParserRuleCall_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__VariableExpression__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVariableExpressionAccess().getTypedVariableDeclarationParserRuleCall_0()); }
	ruleTypedVariableDeclaration
{ after(grammarAccess.getVariableExpressionAccess().getTypedVariableDeclarationParserRuleCall_0()); }
)

    |(
{ before(grammarAccess.getVariableExpressionAccess().getVariableAssignmentParserRuleCall_1()); }
	ruleVariableAssignment
{ after(grammarAccess.getVariableExpressionAccess().getVariableAssignmentParserRuleCall_1()); }
)

    |(
{ before(grammarAccess.getVariableExpressionAccess().getClockDeclarationParserRuleCall_2()); }
	ruleClockDeclaration
{ after(grammarAccess.getVariableExpressionAccess().getClockDeclarationParserRuleCall_2()); }
)

    |(
{ before(grammarAccess.getVariableExpressionAccess().getClockAssignmentParserRuleCall_3()); }
	ruleClockAssignment
{ after(grammarAccess.getVariableExpressionAccess().getClockAssignmentParserRuleCall_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__RelationExpression__OperatorAlternatives_1_1_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRelationExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_1_0_0()); }

	'==' 

{ after(grammarAccess.getRelationExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_1_0_0()); }
)

    |(
{ before(grammarAccess.getRelationExpressionAccess().getOperatorExclamationMarkEqualsSignKeyword_1_1_0_1()); }

	'!=' 

{ after(grammarAccess.getRelationExpressionAccess().getOperatorExclamationMarkEqualsSignKeyword_1_1_0_1()); }
)

    |(
{ before(grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignKeyword_1_1_0_2()); }

	'<' 

{ after(grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignKeyword_1_1_0_2()); }
)

    |(
{ before(grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignKeyword_1_1_0_3()); }

	'>' 

{ after(grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignKeyword_1_1_0_3()); }
)

    |(
{ before(grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_1_0_4()); }

	'<=' 

{ after(grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_1_0_4()); }
)

    |(
{ before(grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_1_0_5()); }

	'>=' 

{ after(grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_1_0_5()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__TimedExpression__OperatorAlternatives_1_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_0_0()); }

	'==' 

{ after(grammarAccess.getTimedExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_0_0()); }
)

    |(
{ before(grammarAccess.getTimedExpressionAccess().getOperatorLessThanSignKeyword_1_0_1()); }

	'<' 

{ after(grammarAccess.getTimedExpressionAccess().getOperatorLessThanSignKeyword_1_0_1()); }
)

    |(
{ before(grammarAccess.getTimedExpressionAccess().getOperatorGreaterThanSignKeyword_1_0_2()); }

	'>' 

{ after(grammarAccess.getTimedExpressionAccess().getOperatorGreaterThanSignKeyword_1_0_2()); }
)

    |(
{ before(grammarAccess.getTimedExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_0_3()); }

	'<=' 

{ after(grammarAccess.getTimedExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_0_3()); }
)

    |(
{ before(grammarAccess.getTimedExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_0_4()); }

	'>=' 

{ after(grammarAccess.getTimedExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_0_4()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__AdditionExpression__OperatorAlternatives_1_1_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAdditionExpressionAccess().getOperatorPlusSignKeyword_1_1_0_0()); }

	'+' 

{ after(grammarAccess.getAdditionExpressionAccess().getOperatorPlusSignKeyword_1_1_0_0()); }
)

    |(
{ before(grammarAccess.getAdditionExpressionAccess().getOperatorHyphenMinusKeyword_1_1_0_1()); }

	'-' 

{ after(grammarAccess.getAdditionExpressionAccess().getOperatorHyphenMinusKeyword_1_1_0_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__MultiplicationExpression__OperatorAlternatives_1_1_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getMultiplicationExpressionAccess().getOperatorAsteriskKeyword_1_1_0_0()); }

	'*' 

{ after(grammarAccess.getMultiplicationExpressionAccess().getOperatorAsteriskKeyword_1_1_0_0()); }
)

    |(
{ before(grammarAccess.getMultiplicationExpressionAccess().getOperatorSolidusKeyword_1_1_0_1()); }

	'/' 

{ after(grammarAccess.getMultiplicationExpressionAccess().getOperatorSolidusKeyword_1_1_0_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__NegatedExpression__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getNegatedExpressionAccess().getGroup_0()); }
(rule__NegatedExpression__Group_0__0)
{ after(grammarAccess.getNegatedExpressionAccess().getGroup_0()); }
)

    |(
{ before(grammarAccess.getNegatedExpressionAccess().getBasicExpressionParserRuleCall_1()); }
	ruleBasicExpression
{ after(grammarAccess.getNegatedExpressionAccess().getBasicExpressionParserRuleCall_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__NegatedExpression__OperatorAlternatives_0_1_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getNegatedExpressionAccess().getOperatorExclamationMarkKeyword_0_1_0_0()); }

	'!' 

{ after(grammarAccess.getNegatedExpressionAccess().getOperatorExclamationMarkKeyword_0_1_0_0()); }
)

    |(
{ before(grammarAccess.getNegatedExpressionAccess().getOperatorHyphenMinusKeyword_0_1_0_1()); }

	'-' 

{ after(grammarAccess.getNegatedExpressionAccess().getOperatorHyphenMinusKeyword_0_1_0_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__BasicExpression__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getBasicExpressionAccess().getValueParserRuleCall_0()); }
	ruleValue
{ after(grammarAccess.getBasicExpressionAccess().getValueParserRuleCall_0()); }
)

    |(
{ before(grammarAccess.getBasicExpressionAccess().getGroup_1()); }
(rule__BasicExpression__Group_1__0)
{ after(grammarAccess.getBasicExpressionAccess().getGroup_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Value__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getValueAccess().getIntegerValueParserRuleCall_0()); }
	ruleIntegerValue
{ after(grammarAccess.getValueAccess().getIntegerValueParserRuleCall_0()); }
)

    |(
{ before(grammarAccess.getValueAccess().getBooleanValueParserRuleCall_1()); }
	ruleBooleanValue
{ after(grammarAccess.getValueAccess().getBooleanValueParserRuleCall_1()); }
)

    |(
{ before(grammarAccess.getValueAccess().getStringValueParserRuleCall_2()); }
	ruleStringValue
{ after(grammarAccess.getValueAccess().getStringValueParserRuleCall_2()); }
)

    |(
{ before(grammarAccess.getValueAccess().getEnumValueParserRuleCall_3()); }
	ruleEnumValue
{ after(grammarAccess.getValueAccess().getEnumValueParserRuleCall_3()); }
)

    |(
{ before(grammarAccess.getValueAccess().getNullValueParserRuleCall_4()); }
	ruleNullValue
{ after(grammarAccess.getValueAccess().getNullValueParserRuleCall_4()); }
)

    |(
{ before(grammarAccess.getValueAccess().getVariableValueParserRuleCall_5()); }
	ruleVariableValue
{ after(grammarAccess.getValueAccess().getVariableValueParserRuleCall_5()); }
)

    |(
{ before(grammarAccess.getValueAccess().getFeatureAccessParserRuleCall_6()); }
	ruleFeatureAccess
{ after(grammarAccess.getValueAccess().getFeatureAccessParserRuleCall_6()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__IntegerValue__ValueAlternatives_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0_0()); }
	RULE_INT
{ after(grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0_0()); }
)

    |(
{ before(grammarAccess.getIntegerValueAccess().getValueSIGNEDINTTerminalRuleCall_0_1()); }
	RULE_SIGNEDINT
{ after(grammarAccess.getIntegerValueAccess().getValueSIGNEDINTTerminalRuleCall_0_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Alternatives_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getGroup_2_0()); }
(rule__FeatureAccess__Group_2_0__0)
{ after(grammarAccess.getFeatureAccessAccess().getGroup_2_0()); }
)

    |(
{ before(grammarAccess.getFeatureAccessAccess().getGroup_2_1()); }
(rule__FeatureAccess__Group_2_1__0)
{ after(grammarAccess.getFeatureAccessAccess().getGroup_2_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ScenarioKind__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioKindAccess().getAssumptionEnumLiteralDeclaration_0()); }
(	'assumption' 
)
{ after(grammarAccess.getScenarioKindAccess().getAssumptionEnumLiteralDeclaration_0()); }
)

    |(
{ before(grammarAccess.getScenarioKindAccess().getGuaranteeEnumLiteralDeclaration_1()); }
(	'guarantee' 
)
{ after(grammarAccess.getScenarioKindAccess().getGuaranteeEnumLiteralDeclaration_1()); }
)

    |(
{ before(grammarAccess.getScenarioKindAccess().getExistentialEnumLiteralDeclaration_2()); }
(	'existential' 
)
{ after(grammarAccess.getScenarioKindAccess().getExistentialEnumLiteralDeclaration_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ExpectationKind__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExpectationKindAccess().getEventuallyEnumLiteralDeclaration_0()); }
(	'eventually' 
)
{ after(grammarAccess.getExpectationKindAccess().getEventuallyEnumLiteralDeclaration_0()); }
)

    |(
{ before(grammarAccess.getExpectationKindAccess().getRequestedEnumLiteralDeclaration_1()); }
(	'requested' 
)
{ after(grammarAccess.getExpectationKindAccess().getRequestedEnumLiteralDeclaration_1()); }
)

    |(
{ before(grammarAccess.getExpectationKindAccess().getUrgentEnumLiteralDeclaration_2()); }
(	'urgent' 
)
{ after(grammarAccess.getExpectationKindAccess().getUrgentEnumLiteralDeclaration_2()); }
)

    |(
{ before(grammarAccess.getExpectationKindAccess().getCommittedEnumLiteralDeclaration_3()); }
(	'committed' 
)
{ after(grammarAccess.getExpectationKindAccess().getCommittedEnumLiteralDeclaration_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__CollectionOperation__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0()); }
(	'any' 
)
{ after(grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0()); }
)

    |(
{ before(grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1()); }
(	'contains' 
)
{ after(grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1()); }
)

    |(
{ before(grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2()); }
(	'containsAll' 
)
{ after(grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2()); }
)

    |(
{ before(grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3()); }
(	'first' 
)
{ after(grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3()); }
)

    |(
{ before(grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4()); }
(	'get' 
)
{ after(grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4()); }
)

    |(
{ before(grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5()); }
(	'isEmpty' 
)
{ after(grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5()); }
)

    |(
{ before(grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6()); }
(	'last' 
)
{ after(grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6()); }
)

    |(
{ before(grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7()); }
(	'size' 
)
{ after(grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__CollectionModification__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollectionModificationAccess().getAddEnumLiteralDeclaration_0()); }
(	'add' 
)
{ after(grammarAccess.getCollectionModificationAccess().getAddEnumLiteralDeclaration_0()); }
)

    |(
{ before(grammarAccess.getCollectionModificationAccess().getAddToFrontEnumLiteralDeclaration_1()); }
(	'addToFront' 
)
{ after(grammarAccess.getCollectionModificationAccess().getAddToFrontEnumLiteralDeclaration_1()); }
)

    |(
{ before(grammarAccess.getCollectionModificationAccess().getClearEnumLiteralDeclaration_2()); }
(	'clear' 
)
{ after(grammarAccess.getCollectionModificationAccess().getClearEnumLiteralDeclaration_2()); }
)

    |(
{ before(grammarAccess.getCollectionModificationAccess().getRemoveEnumLiteralDeclaration_3()); }
(	'remove' 
)
{ after(grammarAccess.getCollectionModificationAccess().getRemoveEnumLiteralDeclaration_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}



rule__Collaboration__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Collaboration__Group__0__Impl
	rule__Collaboration__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Collaboration__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollaborationAccess().getImportsAssignment_0()); }
(rule__Collaboration__ImportsAssignment_0)*
{ after(grammarAccess.getCollaborationAccess().getImportsAssignment_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Collaboration__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Collaboration__Group__1__Impl
	rule__Collaboration__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Collaboration__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollaborationAccess().getGroup_1()); }
(rule__Collaboration__Group_1__0)*
{ after(grammarAccess.getCollaborationAccess().getGroup_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Collaboration__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Collaboration__Group__2__Impl
	rule__Collaboration__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Collaboration__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollaborationAccess().getGroup_2()); }
(rule__Collaboration__Group_2__0)*
{ after(grammarAccess.getCollaborationAccess().getGroup_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Collaboration__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Collaboration__Group__3__Impl
	rule__Collaboration__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__Collaboration__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollaborationAccess().getCollaborationKeyword_3()); }

	'collaboration' 

{ after(grammarAccess.getCollaborationAccess().getCollaborationKeyword_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Collaboration__Group__4
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Collaboration__Group__4__Impl
	rule__Collaboration__Group__5
;
finally {
	restoreStackSize(stackSize);
}

rule__Collaboration__Group__4__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollaborationAccess().getNameAssignment_4()); }
(rule__Collaboration__NameAssignment_4)
{ after(grammarAccess.getCollaborationAccess().getNameAssignment_4()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Collaboration__Group__5
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Collaboration__Group__5__Impl
	rule__Collaboration__Group__6
;
finally {
	restoreStackSize(stackSize);
}

rule__Collaboration__Group__5__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollaborationAccess().getLeftCurlyBracketKeyword_5()); }

	'{' 

{ after(grammarAccess.getCollaborationAccess().getLeftCurlyBracketKeyword_5()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Collaboration__Group__6
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Collaboration__Group__6__Impl
	rule__Collaboration__Group__7
;
finally {
	restoreStackSize(stackSize);
}

rule__Collaboration__Group__6__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollaborationAccess().getRolesAssignment_6()); }
(rule__Collaboration__RolesAssignment_6)*
{ after(grammarAccess.getCollaborationAccess().getRolesAssignment_6()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Collaboration__Group__7
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Collaboration__Group__7__Impl
	rule__Collaboration__Group__8
;
finally {
	restoreStackSize(stackSize);
}

rule__Collaboration__Group__7__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollaborationAccess().getScenariosAssignment_7()); }
(rule__Collaboration__ScenariosAssignment_7)*
{ after(grammarAccess.getCollaborationAccess().getScenariosAssignment_7()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Collaboration__Group__8
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Collaboration__Group__8__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Collaboration__Group__8__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollaborationAccess().getRightCurlyBracketKeyword_8()); }

	'}' 

{ after(grammarAccess.getCollaborationAccess().getRightCurlyBracketKeyword_8()); }
)

;
finally {
	restoreStackSize(stackSize);
}




















rule__Collaboration__Group_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Collaboration__Group_1__0__Impl
	rule__Collaboration__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Collaboration__Group_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollaborationAccess().getDomainKeyword_1_0()); }

	'domain' 

{ after(grammarAccess.getCollaborationAccess().getDomainKeyword_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Collaboration__Group_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Collaboration__Group_1__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Collaboration__Group_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollaborationAccess().getDomainsAssignment_1_1()); }
(rule__Collaboration__DomainsAssignment_1_1)
{ after(grammarAccess.getCollaborationAccess().getDomainsAssignment_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__Collaboration__Group_2__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Collaboration__Group_2__0__Impl
	rule__Collaboration__Group_2__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Collaboration__Group_2__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollaborationAccess().getContextsKeyword_2_0()); }

	'contexts' 

{ after(grammarAccess.getCollaborationAccess().getContextsKeyword_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Collaboration__Group_2__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Collaboration__Group_2__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Collaboration__Group_2__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollaborationAccess().getContextsAssignment_2_1()); }
(rule__Collaboration__ContextsAssignment_2_1)
{ after(grammarAccess.getCollaborationAccess().getContextsAssignment_2_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__FQN__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FQN__Group__0__Impl
	rule__FQN__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__FQN__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFQNAccess().getIDTerminalRuleCall_0()); }
	RULE_ID
{ after(grammarAccess.getFQNAccess().getIDTerminalRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__FQN__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FQN__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__FQN__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFQNAccess().getGroup_1()); }
(rule__FQN__Group_1__0)*
{ after(grammarAccess.getFQNAccess().getGroup_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__FQN__Group_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FQN__Group_1__0__Impl
	rule__FQN__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__FQN__Group_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFQNAccess().getFullStopKeyword_1_0()); }

	'.' 

{ after(grammarAccess.getFQNAccess().getFullStopKeyword_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__FQN__Group_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FQN__Group_1__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__FQN__Group_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1()); }
	RULE_ID
{ after(grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__Role__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Role__Group__0__Impl
	rule__Role__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Role__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRoleAccess().getAlternatives_0()); }
(rule__Role__Alternatives_0)
{ after(grammarAccess.getRoleAccess().getAlternatives_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Role__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Role__Group__1__Impl
	rule__Role__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Role__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRoleAccess().getRoleKeyword_1()); }

	'role' 

{ after(grammarAccess.getRoleAccess().getRoleKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Role__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Role__Group__2__Impl
	rule__Role__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Role__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRoleAccess().getTypeAssignment_2()); }
(rule__Role__TypeAssignment_2)
{ after(grammarAccess.getRoleAccess().getTypeAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Role__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Role__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Role__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRoleAccess().getNameAssignment_3()); }
(rule__Role__NameAssignment_3)
{ after(grammarAccess.getRoleAccess().getNameAssignment_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}










rule__Role__Group_0_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Role__Group_0_1__0__Impl
	rule__Role__Group_0_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Role__Group_0_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRoleAccess().getDynamicKeyword_0_1_0()); }

	'dynamic' 

{ after(grammarAccess.getRoleAccess().getDynamicKeyword_0_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Role__Group_0_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Role__Group_0_1__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Role__Group_0_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRoleAccess().getMultiRoleAssignment_0_1_1()); }
(rule__Role__MultiRoleAssignment_0_1_1)?
{ after(grammarAccess.getRoleAccess().getMultiRoleAssignment_0_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__Scenario__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group__0__Impl
	rule__Scenario__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getSingularAssignment_0()); }
(rule__Scenario__SingularAssignment_0)?
{ after(grammarAccess.getScenarioAccess().getSingularAssignment_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Scenario__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group__1__Impl
	rule__Scenario__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getKindAssignment_1()); }
(rule__Scenario__KindAssignment_1)
{ after(grammarAccess.getScenarioAccess().getKindAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Scenario__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group__2__Impl
	rule__Scenario__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getScenarioKeyword_2()); }

	'scenario' 

{ after(grammarAccess.getScenarioAccess().getScenarioKeyword_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Scenario__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group__3__Impl
	rule__Scenario__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getNameAssignment_3()); }
(rule__Scenario__NameAssignment_3)
{ after(grammarAccess.getScenarioAccess().getNameAssignment_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Scenario__Group__4
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group__4__Impl
	rule__Scenario__Group__5
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group__4__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getAlternatives_4()); }
(rule__Scenario__Alternatives_4)?
{ after(grammarAccess.getScenarioAccess().getAlternatives_4()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Scenario__Group__5
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group__5__Impl
	rule__Scenario__Group__6
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group__5__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getGroup_5()); }
(rule__Scenario__Group_5__0)?
{ after(grammarAccess.getScenarioAccess().getGroup_5()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Scenario__Group__6
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group__6__Impl
	rule__Scenario__Group__7
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group__6__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getGroup_6()); }
(rule__Scenario__Group_6__0)?
{ after(grammarAccess.getScenarioAccess().getGroup_6()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Scenario__Group__7
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group__7__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group__7__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getOwnedInteractionAssignment_7()); }
(rule__Scenario__OwnedInteractionAssignment_7)
{ after(grammarAccess.getScenarioAccess().getOwnedInteractionAssignment_7()); }
)

;
finally {
	restoreStackSize(stackSize);
}


















rule__Scenario__Group_4_0__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group_4_0__0__Impl
	rule__Scenario__Group_4_0__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group_4_0__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getOptimizeCostAssignment_4_0_0()); }
(rule__Scenario__OptimizeCostAssignment_4_0_0)
{ after(grammarAccess.getScenarioAccess().getOptimizeCostAssignment_4_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Scenario__Group_4_0__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group_4_0__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group_4_0__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getCostKeyword_4_0_1()); }

	'cost' 

{ after(grammarAccess.getScenarioAccess().getCostKeyword_4_0_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__Scenario__Group_4_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group_4_1__0__Impl
	rule__Scenario__Group_4_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group_4_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getCostKeyword_4_1_0()); }

	'cost' 

{ after(grammarAccess.getScenarioAccess().getCostKeyword_4_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Scenario__Group_4_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group_4_1__1__Impl
	rule__Scenario__Group_4_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group_4_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getLeftSquareBracketKeyword_4_1_1()); }

	'[' 

{ after(grammarAccess.getScenarioAccess().getLeftSquareBracketKeyword_4_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Scenario__Group_4_1__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group_4_1__2__Impl
	rule__Scenario__Group_4_1__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group_4_1__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getCostAssignment_4_1_2()); }
(rule__Scenario__CostAssignment_4_1_2)
{ after(grammarAccess.getScenarioAccess().getCostAssignment_4_1_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Scenario__Group_4_1__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group_4_1__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group_4_1__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getRightSquareBracketKeyword_4_1_3()); }

	']' 

{ after(grammarAccess.getScenarioAccess().getRightSquareBracketKeyword_4_1_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}










rule__Scenario__Group_5__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group_5__0__Impl
	rule__Scenario__Group_5__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group_5__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getContextKeyword_5_0()); }

	'context' 

{ after(grammarAccess.getScenarioAccess().getContextKeyword_5_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Scenario__Group_5__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group_5__1__Impl
	rule__Scenario__Group_5__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group_5__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getContextsAssignment_5_1()); }
(rule__Scenario__ContextsAssignment_5_1)
{ after(grammarAccess.getScenarioAccess().getContextsAssignment_5_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Scenario__Group_5__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group_5__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group_5__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getGroup_5_2()); }
(rule__Scenario__Group_5_2__0)*
{ after(grammarAccess.getScenarioAccess().getGroup_5_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__Scenario__Group_5_2__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group_5_2__0__Impl
	rule__Scenario__Group_5_2__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group_5_2__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getCommaKeyword_5_2_0()); }

	',' 

{ after(grammarAccess.getScenarioAccess().getCommaKeyword_5_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Scenario__Group_5_2__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group_5_2__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group_5_2__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getContextsAssignment_5_2_1()); }
(rule__Scenario__ContextsAssignment_5_2_1)
{ after(grammarAccess.getScenarioAccess().getContextsAssignment_5_2_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__Scenario__Group_6__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group_6__0__Impl
	rule__Scenario__Group_6__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group_6__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getBindingsKeyword_6_0()); }

	'bindings' 

{ after(grammarAccess.getScenarioAccess().getBindingsKeyword_6_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Scenario__Group_6__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group_6__1__Impl
	rule__Scenario__Group_6__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group_6__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getLeftSquareBracketKeyword_6_1()); }

	'[' 

{ after(grammarAccess.getScenarioAccess().getLeftSquareBracketKeyword_6_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Scenario__Group_6__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group_6__2__Impl
	rule__Scenario__Group_6__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group_6__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getRoleBindingsAssignment_6_2()); }
(rule__Scenario__RoleBindingsAssignment_6_2)*
{ after(grammarAccess.getScenarioAccess().getRoleBindingsAssignment_6_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Scenario__Group_6__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Scenario__Group_6__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__Group_6__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getRightSquareBracketKeyword_6_3()); }

	']' 

{ after(grammarAccess.getScenarioAccess().getRightSquareBracketKeyword_6_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}










rule__RoleBindingConstraint__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__RoleBindingConstraint__Group__0__Impl
	rule__RoleBindingConstraint__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__RoleBindingConstraint__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRoleBindingConstraintAccess().getRoleAssignment_0()); }
(rule__RoleBindingConstraint__RoleAssignment_0)
{ after(grammarAccess.getRoleBindingConstraintAccess().getRoleAssignment_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__RoleBindingConstraint__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__RoleBindingConstraint__Group__1__Impl
	rule__RoleBindingConstraint__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__RoleBindingConstraint__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRoleBindingConstraintAccess().getEqualsSignKeyword_1()); }

	'=' 

{ after(grammarAccess.getRoleBindingConstraintAccess().getEqualsSignKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__RoleBindingConstraint__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__RoleBindingConstraint__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__RoleBindingConstraint__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRoleBindingConstraintAccess().getBindingExpressionAssignment_2()); }
(rule__RoleBindingConstraint__BindingExpressionAssignment_2)
{ after(grammarAccess.getRoleBindingConstraintAccess().getBindingExpressionAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__Interaction__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Interaction__Group__0__Impl
	rule__Interaction__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Interaction__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getInteractionAccess().getInteractionAction_0()); }
(

)
{ after(grammarAccess.getInteractionAccess().getInteractionAction_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Interaction__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Interaction__Group__1__Impl
	rule__Interaction__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Interaction__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getInteractionAccess().getLeftCurlyBracketKeyword_1()); }

	'{' 

{ after(grammarAccess.getInteractionAccess().getLeftCurlyBracketKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Interaction__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Interaction__Group__2__Impl
	rule__Interaction__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Interaction__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getInteractionAccess().getFragmentsAssignment_2()); }
(rule__Interaction__FragmentsAssignment_2)*
{ after(grammarAccess.getInteractionAccess().getFragmentsAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Interaction__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Interaction__Group__3__Impl
	rule__Interaction__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__Interaction__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getInteractionAccess().getRightCurlyBracketKeyword_3()); }

	'}' 

{ after(grammarAccess.getInteractionAccess().getRightCurlyBracketKeyword_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Interaction__Group__4
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Interaction__Group__4__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Interaction__Group__4__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getInteractionAccess().getConstraintsAssignment_4()); }
(rule__Interaction__ConstraintsAssignment_4)?
{ after(grammarAccess.getInteractionAccess().getConstraintsAssignment_4()); }
)

;
finally {
	restoreStackSize(stackSize);
}












rule__ModalMessage__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ModalMessage__Group__0__Impl
	rule__ModalMessage__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getStrictAssignment_0()); }
(rule__ModalMessage__StrictAssignment_0)?
{ after(grammarAccess.getModalMessageAccess().getStrictAssignment_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ModalMessage__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ModalMessage__Group__1__Impl
	rule__ModalMessage__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getGroup_1()); }
(rule__ModalMessage__Group_1__0)?
{ after(grammarAccess.getModalMessageAccess().getGroup_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ModalMessage__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ModalMessage__Group__2__Impl
	rule__ModalMessage__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getSenderAssignment_2()); }
(rule__ModalMessage__SenderAssignment_2)
{ after(grammarAccess.getModalMessageAccess().getSenderAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ModalMessage__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ModalMessage__Group__3__Impl
	rule__ModalMessage__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getHyphenMinusGreaterThanSignKeyword_3()); }

	'->' 

{ after(grammarAccess.getModalMessageAccess().getHyphenMinusGreaterThanSignKeyword_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ModalMessage__Group__4
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ModalMessage__Group__4__Impl
	rule__ModalMessage__Group__5
;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__Group__4__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getReceiverAssignment_4()); }
(rule__ModalMessage__ReceiverAssignment_4)
{ after(grammarAccess.getModalMessageAccess().getReceiverAssignment_4()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ModalMessage__Group__5
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ModalMessage__Group__5__Impl
	rule__ModalMessage__Group__6
;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__Group__5__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getFullStopKeyword_5()); }

	'.' 

{ after(grammarAccess.getModalMessageAccess().getFullStopKeyword_5()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ModalMessage__Group__6
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ModalMessage__Group__6__Impl
	rule__ModalMessage__Group__7
;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__Group__6__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getModelElementAssignment_6()); }
(rule__ModalMessage__ModelElementAssignment_6)
{ after(grammarAccess.getModalMessageAccess().getModelElementAssignment_6()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ModalMessage__Group__7
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ModalMessage__Group__7__Impl
	rule__ModalMessage__Group__8
;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__Group__7__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getGroup_7()); }
(rule__ModalMessage__Group_7__0)?
{ after(grammarAccess.getModalMessageAccess().getGroup_7()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ModalMessage__Group__8
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ModalMessage__Group__8__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__Group__8__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getGroup_8()); }
(rule__ModalMessage__Group_8__0)?
{ after(grammarAccess.getModalMessageAccess().getGroup_8()); }
)

;
finally {
	restoreStackSize(stackSize);
}




















rule__ModalMessage__Group_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ModalMessage__Group_1__0__Impl
	rule__ModalMessage__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__Group_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getMonitoredAssignment_1_0()); }
(rule__ModalMessage__MonitoredAssignment_1_0)?
{ after(grammarAccess.getModalMessageAccess().getMonitoredAssignment_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ModalMessage__Group_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ModalMessage__Group_1__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__Group_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getExpectationKindAssignment_1_1()); }
(rule__ModalMessage__ExpectationKindAssignment_1_1)
{ after(grammarAccess.getModalMessageAccess().getExpectationKindAssignment_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ModalMessage__Group_7__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ModalMessage__Group_7__0__Impl
	rule__ModalMessage__Group_7__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__Group_7__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getFullStopKeyword_7_0()); }

	'.' 

{ after(grammarAccess.getModalMessageAccess().getFullStopKeyword_7_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ModalMessage__Group_7__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ModalMessage__Group_7__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__Group_7__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getCollectionModificationAssignment_7_1()); }
(rule__ModalMessage__CollectionModificationAssignment_7_1)
{ after(grammarAccess.getModalMessageAccess().getCollectionModificationAssignment_7_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ModalMessage__Group_8__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ModalMessage__Group_8__0__Impl
	rule__ModalMessage__Group_8__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__Group_8__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getLeftParenthesisKeyword_8_0()); }

	'(' 

{ after(grammarAccess.getModalMessageAccess().getLeftParenthesisKeyword_8_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ModalMessage__Group_8__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ModalMessage__Group_8__1__Impl
	rule__ModalMessage__Group_8__2
;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__Group_8__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getGroup_8_1()); }
(rule__ModalMessage__Group_8_1__0)?
{ after(grammarAccess.getModalMessageAccess().getGroup_8_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ModalMessage__Group_8__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ModalMessage__Group_8__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__Group_8__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getRightParenthesisKeyword_8_2()); }

	')' 

{ after(grammarAccess.getModalMessageAccess().getRightParenthesisKeyword_8_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__ModalMessage__Group_8_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ModalMessage__Group_8_1__0__Impl
	rule__ModalMessage__Group_8_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__Group_8_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getParametersAssignment_8_1_0()); }
(rule__ModalMessage__ParametersAssignment_8_1_0)
{ after(grammarAccess.getModalMessageAccess().getParametersAssignment_8_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ModalMessage__Group_8_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ModalMessage__Group_8_1__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__Group_8_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getGroup_8_1_1()); }
(rule__ModalMessage__Group_8_1_1__0)*
{ after(grammarAccess.getModalMessageAccess().getGroup_8_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ModalMessage__Group_8_1_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ModalMessage__Group_8_1_1__0__Impl
	rule__ModalMessage__Group_8_1_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__Group_8_1_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getCommaKeyword_8_1_1_0()); }

	',' 

{ after(grammarAccess.getModalMessageAccess().getCommaKeyword_8_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ModalMessage__Group_8_1_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ModalMessage__Group_8_1_1__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__Group_8_1_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getParametersAssignment_8_1_1_1()); }
(rule__ModalMessage__ParametersAssignment_8_1_1_1)
{ after(grammarAccess.getModalMessageAccess().getParametersAssignment_8_1_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__WildcardParameterExpression__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__WildcardParameterExpression__Group__0__Impl
	rule__WildcardParameterExpression__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__WildcardParameterExpression__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getWildcardParameterExpressionAccess().getWildcardParameterExpressionAction_0()); }
(

)
{ after(grammarAccess.getWildcardParameterExpressionAccess().getWildcardParameterExpressionAction_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__WildcardParameterExpression__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__WildcardParameterExpression__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__WildcardParameterExpression__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getWildcardParameterExpressionAccess().getAsteriskKeyword_1()); }

	'*' 

{ after(grammarAccess.getWildcardParameterExpressionAccess().getAsteriskKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__VariableBindingParameterExpression__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__VariableBindingParameterExpression__Group__0__Impl
	rule__VariableBindingParameterExpression__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__VariableBindingParameterExpression__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVariableBindingParameterExpressionAccess().getBindKeyword_0()); }

	'bind' 

{ after(grammarAccess.getVariableBindingParameterExpressionAccess().getBindKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__VariableBindingParameterExpression__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__VariableBindingParameterExpression__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__VariableBindingParameterExpression__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVariableBindingParameterExpressionAccess().getVariableAssignment_1()); }
(rule__VariableBindingParameterExpression__VariableAssignment_1)
{ after(grammarAccess.getVariableBindingParameterExpressionAccess().getVariableAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__Alternative__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Alternative__Group__0__Impl
	rule__Alternative__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Alternative__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAlternativeAccess().getAlternativeAction_0()); }
(

)
{ after(grammarAccess.getAlternativeAccess().getAlternativeAction_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Alternative__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Alternative__Group__1__Impl
	rule__Alternative__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Alternative__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAlternativeAccess().getAlternativeKeyword_1()); }

	'alternative' 

{ after(grammarAccess.getAlternativeAccess().getAlternativeKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Alternative__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Alternative__Group__2__Impl
	rule__Alternative__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Alternative__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAlternativeAccess().getCasesAssignment_2()); }
(rule__Alternative__CasesAssignment_2)
{ after(grammarAccess.getAlternativeAccess().getCasesAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Alternative__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Alternative__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Alternative__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAlternativeAccess().getGroup_3()); }
(rule__Alternative__Group_3__0)*
{ after(grammarAccess.getAlternativeAccess().getGroup_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}










rule__Alternative__Group_3__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Alternative__Group_3__0__Impl
	rule__Alternative__Group_3__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Alternative__Group_3__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAlternativeAccess().getOrKeyword_3_0()); }

	'or' 

{ after(grammarAccess.getAlternativeAccess().getOrKeyword_3_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Alternative__Group_3__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Alternative__Group_3__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Alternative__Group_3__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAlternativeAccess().getCasesAssignment_3_1()); }
(rule__Alternative__CasesAssignment_3_1)
{ after(grammarAccess.getAlternativeAccess().getCasesAssignment_3_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__Case__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Case__Group__0__Impl
	rule__Case__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Case__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCaseAccess().getCaseAction_0()); }
(

)
{ after(grammarAccess.getCaseAccess().getCaseAction_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Case__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Case__Group__1__Impl
	rule__Case__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Case__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCaseAccess().getCaseConditionAssignment_1()); }
(rule__Case__CaseConditionAssignment_1)?
{ after(grammarAccess.getCaseAccess().getCaseConditionAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Case__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Case__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Case__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCaseAccess().getCaseInteractionAssignment_2()); }
(rule__Case__CaseInteractionAssignment_2)
{ after(grammarAccess.getCaseAccess().getCaseInteractionAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__Loop__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Loop__Group__0__Impl
	rule__Loop__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Loop__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getLoopAccess().getWhileKeyword_0()); }

	'while' 

{ after(grammarAccess.getLoopAccess().getWhileKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Loop__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Loop__Group__1__Impl
	rule__Loop__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Loop__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getLoopAccess().getLoopConditionAssignment_1()); }
(rule__Loop__LoopConditionAssignment_1)?
{ after(grammarAccess.getLoopAccess().getLoopConditionAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Loop__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Loop__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Loop__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getLoopAccess().getBodyInteractionAssignment_2()); }
(rule__Loop__BodyInteractionAssignment_2)
{ after(grammarAccess.getLoopAccess().getBodyInteractionAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__Parallel__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Parallel__Group__0__Impl
	rule__Parallel__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Parallel__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getParallelAccess().getParallelAction_0()); }
(

)
{ after(grammarAccess.getParallelAccess().getParallelAction_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Parallel__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Parallel__Group__1__Impl
	rule__Parallel__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Parallel__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getParallelAccess().getParallelKeyword_1()); }

	'parallel' 

{ after(grammarAccess.getParallelAccess().getParallelKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Parallel__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Parallel__Group__2__Impl
	rule__Parallel__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Parallel__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getParallelAccess().getParallelInteractionAssignment_2()); }
(rule__Parallel__ParallelInteractionAssignment_2)
{ after(grammarAccess.getParallelAccess().getParallelInteractionAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Parallel__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Parallel__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Parallel__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getParallelAccess().getGroup_3()); }
(rule__Parallel__Group_3__0)*
{ after(grammarAccess.getParallelAccess().getGroup_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}










rule__Parallel__Group_3__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Parallel__Group_3__0__Impl
	rule__Parallel__Group_3__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Parallel__Group_3__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getParallelAccess().getAndKeyword_3_0()); }

	'and' 

{ after(grammarAccess.getParallelAccess().getAndKeyword_3_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Parallel__Group_3__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Parallel__Group_3__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Parallel__Group_3__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getParallelAccess().getParallelInteractionAssignment_3_1()); }
(rule__Parallel__ParallelInteractionAssignment_3_1)
{ after(grammarAccess.getParallelAccess().getParallelInteractionAssignment_3_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__WaitCondition__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__WaitCondition__Group__0__Impl
	rule__WaitCondition__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__WaitCondition__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getWaitConditionAccess().getWaitKeyword_0()); }

	'wait' 

{ after(grammarAccess.getWaitConditionAccess().getWaitKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__WaitCondition__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__WaitCondition__Group__1__Impl
	rule__WaitCondition__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__WaitCondition__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getWaitConditionAccess().getStrictAssignment_1()); }
(rule__WaitCondition__StrictAssignment_1)?
{ after(grammarAccess.getWaitConditionAccess().getStrictAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__WaitCondition__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__WaitCondition__Group__2__Impl
	rule__WaitCondition__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__WaitCondition__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getWaitConditionAccess().getRequestedAssignment_2()); }
(rule__WaitCondition__RequestedAssignment_2)?
{ after(grammarAccess.getWaitConditionAccess().getRequestedAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__WaitCondition__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__WaitCondition__Group__3__Impl
	rule__WaitCondition__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__WaitCondition__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getWaitConditionAccess().getLeftSquareBracketKeyword_3()); }

	'[' 

{ after(grammarAccess.getWaitConditionAccess().getLeftSquareBracketKeyword_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__WaitCondition__Group__4
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__WaitCondition__Group__4__Impl
	rule__WaitCondition__Group__5
;
finally {
	restoreStackSize(stackSize);
}

rule__WaitCondition__Group__4__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getWaitConditionAccess().getConditionExpressionAssignment_4()); }
(rule__WaitCondition__ConditionExpressionAssignment_4)
{ after(grammarAccess.getWaitConditionAccess().getConditionExpressionAssignment_4()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__WaitCondition__Group__5
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__WaitCondition__Group__5__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__WaitCondition__Group__5__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getWaitConditionAccess().getRightSquareBracketKeyword_5()); }

	']' 

{ after(grammarAccess.getWaitConditionAccess().getRightSquareBracketKeyword_5()); }
)

;
finally {
	restoreStackSize(stackSize);
}














rule__InterruptCondition__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__InterruptCondition__Group__0__Impl
	rule__InterruptCondition__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__InterruptCondition__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getInterruptConditionAccess().getInterruptKeyword_0()); }

	'interrupt' 

{ after(grammarAccess.getInterruptConditionAccess().getInterruptKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__InterruptCondition__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__InterruptCondition__Group__1__Impl
	rule__InterruptCondition__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__InterruptCondition__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getInterruptConditionAccess().getLeftSquareBracketKeyword_1()); }

	'[' 

{ after(grammarAccess.getInterruptConditionAccess().getLeftSquareBracketKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__InterruptCondition__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__InterruptCondition__Group__2__Impl
	rule__InterruptCondition__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__InterruptCondition__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getInterruptConditionAccess().getConditionExpressionAssignment_2()); }
(rule__InterruptCondition__ConditionExpressionAssignment_2)
{ after(grammarAccess.getInterruptConditionAccess().getConditionExpressionAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__InterruptCondition__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__InterruptCondition__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__InterruptCondition__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getInterruptConditionAccess().getRightSquareBracketKeyword_3()); }

	']' 

{ after(grammarAccess.getInterruptConditionAccess().getRightSquareBracketKeyword_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}










rule__ViolationCondition__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ViolationCondition__Group__0__Impl
	rule__ViolationCondition__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ViolationCondition__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getViolationConditionAccess().getViolationKeyword_0()); }

	'violation' 

{ after(grammarAccess.getViolationConditionAccess().getViolationKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ViolationCondition__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ViolationCondition__Group__1__Impl
	rule__ViolationCondition__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__ViolationCondition__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getViolationConditionAccess().getLeftSquareBracketKeyword_1()); }

	'[' 

{ after(grammarAccess.getViolationConditionAccess().getLeftSquareBracketKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ViolationCondition__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ViolationCondition__Group__2__Impl
	rule__ViolationCondition__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__ViolationCondition__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getViolationConditionAccess().getConditionExpressionAssignment_2()); }
(rule__ViolationCondition__ConditionExpressionAssignment_2)
{ after(grammarAccess.getViolationConditionAccess().getConditionExpressionAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ViolationCondition__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ViolationCondition__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ViolationCondition__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getViolationConditionAccess().getRightSquareBracketKeyword_3()); }

	']' 

{ after(grammarAccess.getViolationConditionAccess().getRightSquareBracketKeyword_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}










rule__TimedWaitCondition__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TimedWaitCondition__Group__0__Impl
	rule__TimedWaitCondition__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__TimedWaitCondition__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedWaitConditionAccess().getTimedKeyword_0()); }

	'timed' 

{ after(grammarAccess.getTimedWaitConditionAccess().getTimedKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TimedWaitCondition__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TimedWaitCondition__Group__1__Impl
	rule__TimedWaitCondition__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__TimedWaitCondition__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedWaitConditionAccess().getWaitKeyword_1()); }

	'wait' 

{ after(grammarAccess.getTimedWaitConditionAccess().getWaitKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TimedWaitCondition__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TimedWaitCondition__Group__2__Impl
	rule__TimedWaitCondition__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__TimedWaitCondition__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedWaitConditionAccess().getStrictAssignment_2()); }
(rule__TimedWaitCondition__StrictAssignment_2)?
{ after(grammarAccess.getTimedWaitConditionAccess().getStrictAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TimedWaitCondition__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TimedWaitCondition__Group__3__Impl
	rule__TimedWaitCondition__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__TimedWaitCondition__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedWaitConditionAccess().getRequestedAssignment_3()); }
(rule__TimedWaitCondition__RequestedAssignment_3)?
{ after(grammarAccess.getTimedWaitConditionAccess().getRequestedAssignment_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TimedWaitCondition__Group__4
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TimedWaitCondition__Group__4__Impl
	rule__TimedWaitCondition__Group__5
;
finally {
	restoreStackSize(stackSize);
}

rule__TimedWaitCondition__Group__4__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedWaitConditionAccess().getLeftSquareBracketKeyword_4()); }

	'[' 

{ after(grammarAccess.getTimedWaitConditionAccess().getLeftSquareBracketKeyword_4()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TimedWaitCondition__Group__5
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TimedWaitCondition__Group__5__Impl
	rule__TimedWaitCondition__Group__6
;
finally {
	restoreStackSize(stackSize);
}

rule__TimedWaitCondition__Group__5__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedWaitConditionAccess().getTimedConditionExpressionAssignment_5()); }
(rule__TimedWaitCondition__TimedConditionExpressionAssignment_5)
{ after(grammarAccess.getTimedWaitConditionAccess().getTimedConditionExpressionAssignment_5()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TimedWaitCondition__Group__6
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TimedWaitCondition__Group__6__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__TimedWaitCondition__Group__6__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedWaitConditionAccess().getRightSquareBracketKeyword_6()); }

	']' 

{ after(grammarAccess.getTimedWaitConditionAccess().getRightSquareBracketKeyword_6()); }
)

;
finally {
	restoreStackSize(stackSize);
}
















rule__TimedViolationCondition__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TimedViolationCondition__Group__0__Impl
	rule__TimedViolationCondition__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__TimedViolationCondition__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedViolationConditionAccess().getTimedKeyword_0()); }

	'timed' 

{ after(grammarAccess.getTimedViolationConditionAccess().getTimedKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TimedViolationCondition__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TimedViolationCondition__Group__1__Impl
	rule__TimedViolationCondition__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__TimedViolationCondition__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedViolationConditionAccess().getViolationKeyword_1()); }

	'violation' 

{ after(grammarAccess.getTimedViolationConditionAccess().getViolationKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TimedViolationCondition__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TimedViolationCondition__Group__2__Impl
	rule__TimedViolationCondition__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__TimedViolationCondition__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedViolationConditionAccess().getLeftSquareBracketKeyword_2()); }

	'[' 

{ after(grammarAccess.getTimedViolationConditionAccess().getLeftSquareBracketKeyword_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TimedViolationCondition__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TimedViolationCondition__Group__3__Impl
	rule__TimedViolationCondition__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__TimedViolationCondition__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedViolationConditionAccess().getTimedConditionExpressionAssignment_3()); }
(rule__TimedViolationCondition__TimedConditionExpressionAssignment_3)
{ after(grammarAccess.getTimedViolationConditionAccess().getTimedConditionExpressionAssignment_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TimedViolationCondition__Group__4
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TimedViolationCondition__Group__4__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__TimedViolationCondition__Group__4__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedViolationConditionAccess().getRightSquareBracketKeyword_4()); }

	']' 

{ after(grammarAccess.getTimedViolationConditionAccess().getRightSquareBracketKeyword_4()); }
)

;
finally {
	restoreStackSize(stackSize);
}












rule__TimedInterruptCondition__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TimedInterruptCondition__Group__0__Impl
	rule__TimedInterruptCondition__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__TimedInterruptCondition__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedInterruptConditionAccess().getTimedKeyword_0()); }

	'timed' 

{ after(grammarAccess.getTimedInterruptConditionAccess().getTimedKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TimedInterruptCondition__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TimedInterruptCondition__Group__1__Impl
	rule__TimedInterruptCondition__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__TimedInterruptCondition__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedInterruptConditionAccess().getInterruptKeyword_1()); }

	'interrupt' 

{ after(grammarAccess.getTimedInterruptConditionAccess().getInterruptKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TimedInterruptCondition__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TimedInterruptCondition__Group__2__Impl
	rule__TimedInterruptCondition__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__TimedInterruptCondition__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedInterruptConditionAccess().getLeftSquareBracketKeyword_2()); }

	'[' 

{ after(grammarAccess.getTimedInterruptConditionAccess().getLeftSquareBracketKeyword_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TimedInterruptCondition__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TimedInterruptCondition__Group__3__Impl
	rule__TimedInterruptCondition__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__TimedInterruptCondition__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedInterruptConditionAccess().getTimedConditionExpressionAssignment_3()); }
(rule__TimedInterruptCondition__TimedConditionExpressionAssignment_3)
{ after(grammarAccess.getTimedInterruptConditionAccess().getTimedConditionExpressionAssignment_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TimedInterruptCondition__Group__4
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TimedInterruptCondition__Group__4__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__TimedInterruptCondition__Group__4__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedInterruptConditionAccess().getRightSquareBracketKeyword_4()); }

	']' 

{ after(grammarAccess.getTimedInterruptConditionAccess().getRightSquareBracketKeyword_4()); }
)

;
finally {
	restoreStackSize(stackSize);
}












rule__Condition__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Condition__Group__0__Impl
	rule__Condition__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Condition__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConditionAccess().getLeftSquareBracketKeyword_0()); }

	'[' 

{ after(grammarAccess.getConditionAccess().getLeftSquareBracketKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Condition__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Condition__Group__1__Impl
	rule__Condition__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Condition__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConditionAccess().getConditionExpressionAssignment_1()); }
(rule__Condition__ConditionExpressionAssignment_1)
{ after(grammarAccess.getConditionAccess().getConditionExpressionAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Condition__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Condition__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Condition__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConditionAccess().getRightSquareBracketKeyword_2()); }

	']' 

{ after(grammarAccess.getConditionAccess().getRightSquareBracketKeyword_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__ConstraintBlock__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintBlock__Group__0__Impl
	rule__ConstraintBlock__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintBlock__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintBlockAccess().getConstraintBlockAction_0()); }
(

)
{ after(grammarAccess.getConstraintBlockAccess().getConstraintBlockAction_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConstraintBlock__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintBlock__Group__1__Impl
	rule__ConstraintBlock__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintBlock__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintBlockAccess().getConstraintsKeyword_1()); }

	'constraints' 

{ after(grammarAccess.getConstraintBlockAccess().getConstraintsKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConstraintBlock__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintBlock__Group__2__Impl
	rule__ConstraintBlock__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintBlock__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintBlockAccess().getLeftSquareBracketKeyword_2()); }

	'[' 

{ after(grammarAccess.getConstraintBlockAccess().getLeftSquareBracketKeyword_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConstraintBlock__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintBlock__Group__3__Impl
	rule__ConstraintBlock__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintBlock__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintBlockAccess().getAlternatives_3()); }
(rule__ConstraintBlock__Alternatives_3)*
{ after(grammarAccess.getConstraintBlockAccess().getAlternatives_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConstraintBlock__Group__4
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintBlock__Group__4__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintBlock__Group__4__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintBlockAccess().getRightSquareBracketKeyword_4()); }

	']' 

{ after(grammarAccess.getConstraintBlockAccess().getRightSquareBracketKeyword_4()); }
)

;
finally {
	restoreStackSize(stackSize);
}












rule__ConstraintBlock__Group_3_0__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintBlock__Group_3_0__0__Impl
	rule__ConstraintBlock__Group_3_0__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintBlock__Group_3_0__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintBlockAccess().getConsiderKeyword_3_0_0()); }

	'consider' 

{ after(grammarAccess.getConstraintBlockAccess().getConsiderKeyword_3_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConstraintBlock__Group_3_0__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintBlock__Group_3_0__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintBlock__Group_3_0__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintBlockAccess().getConsiderAssignment_3_0_1()); }
(rule__ConstraintBlock__ConsiderAssignment_3_0_1)
{ after(grammarAccess.getConstraintBlockAccess().getConsiderAssignment_3_0_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ConstraintBlock__Group_3_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintBlock__Group_3_1__0__Impl
	rule__ConstraintBlock__Group_3_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintBlock__Group_3_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintBlockAccess().getIgnoreKeyword_3_1_0()); }

	'ignore' 

{ after(grammarAccess.getConstraintBlockAccess().getIgnoreKeyword_3_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConstraintBlock__Group_3_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintBlock__Group_3_1__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintBlock__Group_3_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintBlockAccess().getIgnoreAssignment_3_1_1()); }
(rule__ConstraintBlock__IgnoreAssignment_3_1_1)
{ after(grammarAccess.getConstraintBlockAccess().getIgnoreAssignment_3_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ConstraintBlock__Group_3_2__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintBlock__Group_3_2__0__Impl
	rule__ConstraintBlock__Group_3_2__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintBlock__Group_3_2__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintBlockAccess().getForbiddenKeyword_3_2_0()); }

	'forbidden' 

{ after(grammarAccess.getConstraintBlockAccess().getForbiddenKeyword_3_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConstraintBlock__Group_3_2__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintBlock__Group_3_2__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintBlock__Group_3_2__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintBlockAccess().getForbiddenAssignment_3_2_1()); }
(rule__ConstraintBlock__ForbiddenAssignment_3_2_1)
{ after(grammarAccess.getConstraintBlockAccess().getForbiddenAssignment_3_2_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ConstraintBlock__Group_3_3__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintBlock__Group_3_3__0__Impl
	rule__ConstraintBlock__Group_3_3__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintBlock__Group_3_3__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintBlockAccess().getInterruptKeyword_3_3_0()); }

	'interrupt' 

{ after(grammarAccess.getConstraintBlockAccess().getInterruptKeyword_3_3_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConstraintBlock__Group_3_3__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintBlock__Group_3_3__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintBlock__Group_3_3__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintBlockAccess().getInterruptAssignment_3_3_1()); }
(rule__ConstraintBlock__InterruptAssignment_3_3_1)
{ after(grammarAccess.getConstraintBlockAccess().getInterruptAssignment_3_3_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ConstraintMessage__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintMessage__Group__0__Impl
	rule__ConstraintMessage__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getSenderAssignment_0()); }
(rule__ConstraintMessage__SenderAssignment_0)
{ after(grammarAccess.getConstraintMessageAccess().getSenderAssignment_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConstraintMessage__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintMessage__Group__1__Impl
	rule__ConstraintMessage__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getHyphenMinusGreaterThanSignKeyword_1()); }

	'->' 

{ after(grammarAccess.getConstraintMessageAccess().getHyphenMinusGreaterThanSignKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConstraintMessage__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintMessage__Group__2__Impl
	rule__ConstraintMessage__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getReceiverAssignment_2()); }
(rule__ConstraintMessage__ReceiverAssignment_2)
{ after(grammarAccess.getConstraintMessageAccess().getReceiverAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConstraintMessage__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintMessage__Group__3__Impl
	rule__ConstraintMessage__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getFullStopKeyword_3()); }

	'.' 

{ after(grammarAccess.getConstraintMessageAccess().getFullStopKeyword_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConstraintMessage__Group__4
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintMessage__Group__4__Impl
	rule__ConstraintMessage__Group__5
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__Group__4__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getModelElementAssignment_4()); }
(rule__ConstraintMessage__ModelElementAssignment_4)
{ after(grammarAccess.getConstraintMessageAccess().getModelElementAssignment_4()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConstraintMessage__Group__5
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintMessage__Group__5__Impl
	rule__ConstraintMessage__Group__6
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__Group__5__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getGroup_5()); }
(rule__ConstraintMessage__Group_5__0)?
{ after(grammarAccess.getConstraintMessageAccess().getGroup_5()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConstraintMessage__Group__6
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintMessage__Group__6__Impl
	rule__ConstraintMessage__Group__7
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__Group__6__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getLeftParenthesisKeyword_6()); }

	'(' 

{ after(grammarAccess.getConstraintMessageAccess().getLeftParenthesisKeyword_6()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConstraintMessage__Group__7
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintMessage__Group__7__Impl
	rule__ConstraintMessage__Group__8
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__Group__7__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getGroup_7()); }
(rule__ConstraintMessage__Group_7__0)?
{ after(grammarAccess.getConstraintMessageAccess().getGroup_7()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConstraintMessage__Group__8
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintMessage__Group__8__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__Group__8__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getRightParenthesisKeyword_8()); }

	')' 

{ after(grammarAccess.getConstraintMessageAccess().getRightParenthesisKeyword_8()); }
)

;
finally {
	restoreStackSize(stackSize);
}




















rule__ConstraintMessage__Group_5__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintMessage__Group_5__0__Impl
	rule__ConstraintMessage__Group_5__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__Group_5__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getFullStopKeyword_5_0()); }

	'.' 

{ after(grammarAccess.getConstraintMessageAccess().getFullStopKeyword_5_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConstraintMessage__Group_5__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintMessage__Group_5__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__Group_5__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getCollectionModificationAssignment_5_1()); }
(rule__ConstraintMessage__CollectionModificationAssignment_5_1)
{ after(grammarAccess.getConstraintMessageAccess().getCollectionModificationAssignment_5_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ConstraintMessage__Group_7__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintMessage__Group_7__0__Impl
	rule__ConstraintMessage__Group_7__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__Group_7__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getParametersAssignment_7_0()); }
(rule__ConstraintMessage__ParametersAssignment_7_0)
{ after(grammarAccess.getConstraintMessageAccess().getParametersAssignment_7_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConstraintMessage__Group_7__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintMessage__Group_7__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__Group_7__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getGroup_7_1()); }
(rule__ConstraintMessage__Group_7_1__0)*
{ after(grammarAccess.getConstraintMessageAccess().getGroup_7_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ConstraintMessage__Group_7_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintMessage__Group_7_1__0__Impl
	rule__ConstraintMessage__Group_7_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__Group_7_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getCommaKeyword_7_1_0()); }

	',' 

{ after(grammarAccess.getConstraintMessageAccess().getCommaKeyword_7_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConstraintMessage__Group_7_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConstraintMessage__Group_7_1__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__Group_7_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getParametersAssignment_7_1_1()); }
(rule__ConstraintMessage__ParametersAssignment_7_1_1)
{ after(grammarAccess.getConstraintMessageAccess().getParametersAssignment_7_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__Import__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Import__Group__0__Impl
	rule__Import__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Import__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getImportAccess().getImportKeyword_0()); }

	'import' 

{ after(grammarAccess.getImportAccess().getImportKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Import__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Import__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Import__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getImportAccess().getImportURIAssignment_1()); }
(rule__Import__ImportURIAssignment_1)
{ after(grammarAccess.getImportAccess().getImportURIAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ExpressionRegion__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExpressionRegion__Group__0__Impl
	rule__ExpressionRegion__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ExpressionRegion__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExpressionRegionAccess().getExpressionRegionAction_0()); }
(

)
{ after(grammarAccess.getExpressionRegionAccess().getExpressionRegionAction_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ExpressionRegion__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExpressionRegion__Group__1__Impl
	rule__ExpressionRegion__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__ExpressionRegion__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExpressionRegionAccess().getLeftCurlyBracketKeyword_1()); }

	'{' 

{ after(grammarAccess.getExpressionRegionAccess().getLeftCurlyBracketKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ExpressionRegion__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExpressionRegion__Group__2__Impl
	rule__ExpressionRegion__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__ExpressionRegion__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExpressionRegionAccess().getGroup_2()); }
(rule__ExpressionRegion__Group_2__0)*
{ after(grammarAccess.getExpressionRegionAccess().getGroup_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ExpressionRegion__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExpressionRegion__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ExpressionRegion__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExpressionRegionAccess().getRightCurlyBracketKeyword_3()); }

	'}' 

{ after(grammarAccess.getExpressionRegionAccess().getRightCurlyBracketKeyword_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}










rule__ExpressionRegion__Group_2__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExpressionRegion__Group_2__0__Impl
	rule__ExpressionRegion__Group_2__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ExpressionRegion__Group_2__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExpressionRegionAccess().getExpressionsAssignment_2_0()); }
(rule__ExpressionRegion__ExpressionsAssignment_2_0)
{ after(grammarAccess.getExpressionRegionAccess().getExpressionsAssignment_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ExpressionRegion__Group_2__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExpressionRegion__Group_2__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ExpressionRegion__Group_2__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExpressionRegionAccess().getSemicolonKeyword_2_1()); }

	';' 

{ after(grammarAccess.getExpressionRegionAccess().getSemicolonKeyword_2_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}







rule__VariableAssignment__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__VariableAssignment__Group__0__Impl
	rule__VariableAssignment__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__VariableAssignment__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVariableAssignmentAccess().getVariableAssignment_0()); }
(rule__VariableAssignment__VariableAssignment_0)
{ after(grammarAccess.getVariableAssignmentAccess().getVariableAssignment_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__VariableAssignment__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__VariableAssignment__Group__1__Impl
	rule__VariableAssignment__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__VariableAssignment__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVariableAssignmentAccess().getEqualsSignKeyword_1()); }

	'=' 

{ after(grammarAccess.getVariableAssignmentAccess().getEqualsSignKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__VariableAssignment__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__VariableAssignment__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__VariableAssignment__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVariableAssignmentAccess().getExpressionAssignment_2()); }
(rule__VariableAssignment__ExpressionAssignment_2)
{ after(grammarAccess.getVariableAssignmentAccess().getExpressionAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__TypedVariableDeclaration__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TypedVariableDeclaration__Group__0__Impl
	rule__TypedVariableDeclaration__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__TypedVariableDeclaration__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getVarKeyword_0()); }

	'var' 

{ after(grammarAccess.getTypedVariableDeclarationAccess().getVarKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TypedVariableDeclaration__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TypedVariableDeclaration__Group__1__Impl
	rule__TypedVariableDeclaration__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__TypedVariableDeclaration__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getTypeAssignment_1()); }
(rule__TypedVariableDeclaration__TypeAssignment_1)
{ after(grammarAccess.getTypedVariableDeclarationAccess().getTypeAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TypedVariableDeclaration__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TypedVariableDeclaration__Group__2__Impl
	rule__TypedVariableDeclaration__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__TypedVariableDeclaration__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getNameAssignment_2()); }
(rule__TypedVariableDeclaration__NameAssignment_2)
{ after(grammarAccess.getTypedVariableDeclarationAccess().getNameAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TypedVariableDeclaration__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TypedVariableDeclaration__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__TypedVariableDeclaration__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getGroup_3()); }
(rule__TypedVariableDeclaration__Group_3__0)?
{ after(grammarAccess.getTypedVariableDeclarationAccess().getGroup_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}










rule__TypedVariableDeclaration__Group_3__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TypedVariableDeclaration__Group_3__0__Impl
	rule__TypedVariableDeclaration__Group_3__1
;
finally {
	restoreStackSize(stackSize);
}

rule__TypedVariableDeclaration__Group_3__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getEqualsSignKeyword_3_0()); }

	'=' 

{ after(grammarAccess.getTypedVariableDeclarationAccess().getEqualsSignKeyword_3_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TypedVariableDeclaration__Group_3__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TypedVariableDeclaration__Group_3__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__TypedVariableDeclaration__Group_3__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getExpressionAssignment_3_1()); }
(rule__TypedVariableDeclaration__ExpressionAssignment_3_1)
{ after(grammarAccess.getTypedVariableDeclarationAccess().getExpressionAssignment_3_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ClockDeclaration__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockDeclaration__Group__0__Impl
	rule__ClockDeclaration__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockDeclaration__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockDeclarationAccess().getClockDeclarationAction_0()); }
(

)
{ after(grammarAccess.getClockDeclarationAccess().getClockDeclarationAction_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ClockDeclaration__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockDeclaration__Group__1__Impl
	rule__ClockDeclaration__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockDeclaration__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockDeclarationAccess().getClockKeyword_1()); }

	'clock' 

{ after(grammarAccess.getClockDeclarationAccess().getClockKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ClockDeclaration__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockDeclaration__Group__2__Impl
	rule__ClockDeclaration__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockDeclaration__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockDeclarationAccess().getNameAssignment_2()); }
(rule__ClockDeclaration__NameAssignment_2)
{ after(grammarAccess.getClockDeclarationAccess().getNameAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ClockDeclaration__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockDeclaration__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockDeclaration__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockDeclarationAccess().getGroup_3()); }
(rule__ClockDeclaration__Group_3__0)?
{ after(grammarAccess.getClockDeclarationAccess().getGroup_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}










rule__ClockDeclaration__Group_3__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockDeclaration__Group_3__0__Impl
	rule__ClockDeclaration__Group_3__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockDeclaration__Group_3__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockDeclarationAccess().getEqualsSignKeyword_3_0()); }

	'=' 

{ after(grammarAccess.getClockDeclarationAccess().getEqualsSignKeyword_3_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ClockDeclaration__Group_3__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockDeclaration__Group_3__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockDeclaration__Group_3__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockDeclarationAccess().getExpressionAssignment_3_1()); }
(rule__ClockDeclaration__ExpressionAssignment_3_1)
{ after(grammarAccess.getClockDeclarationAccess().getExpressionAssignment_3_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}







rule__ClockAssignment__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockAssignment__Group__0__Impl
	rule__ClockAssignment__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockAssignment__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockAssignmentAccess().getResetClockKeyword_0()); }

	'reset clock' 

{ after(grammarAccess.getClockAssignmentAccess().getResetClockKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ClockAssignment__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockAssignment__Group__1__Impl
	rule__ClockAssignment__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockAssignment__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockAssignmentAccess().getVariableAssignment_1()); }
(rule__ClockAssignment__VariableAssignment_1)
{ after(grammarAccess.getClockAssignmentAccess().getVariableAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ClockAssignment__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockAssignment__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockAssignment__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockAssignmentAccess().getGroup_2()); }
(rule__ClockAssignment__Group_2__0)?
{ after(grammarAccess.getClockAssignmentAccess().getGroup_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__ClockAssignment__Group_2__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockAssignment__Group_2__0__Impl
	rule__ClockAssignment__Group_2__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockAssignment__Group_2__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockAssignmentAccess().getEqualsSignKeyword_2_0()); }

	'=' 

{ after(grammarAccess.getClockAssignmentAccess().getEqualsSignKeyword_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ClockAssignment__Group_2__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockAssignment__Group_2__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockAssignment__Group_2__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockAssignmentAccess().getExpressionAssignment_2_1()); }
(rule__ClockAssignment__ExpressionAssignment_2_1)
{ after(grammarAccess.getClockAssignmentAccess().getExpressionAssignment_2_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ImplicationExpression__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ImplicationExpression__Group__0__Impl
	rule__ImplicationExpression__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ImplicationExpression__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getImplicationExpressionAccess().getDisjunctionExpressionParserRuleCall_0()); }
	ruleDisjunctionExpression
{ after(grammarAccess.getImplicationExpressionAccess().getDisjunctionExpressionParserRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ImplicationExpression__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ImplicationExpression__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ImplicationExpression__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getImplicationExpressionAccess().getGroup_1()); }
(rule__ImplicationExpression__Group_1__0)?
{ after(grammarAccess.getImplicationExpressionAccess().getGroup_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ImplicationExpression__Group_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ImplicationExpression__Group_1__0__Impl
	rule__ImplicationExpression__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ImplicationExpression__Group_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getImplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
(

)
{ after(grammarAccess.getImplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ImplicationExpression__Group_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ImplicationExpression__Group_1__1__Impl
	rule__ImplicationExpression__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__ImplicationExpression__Group_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getImplicationExpressionAccess().getOperatorAssignment_1_1()); }
(rule__ImplicationExpression__OperatorAssignment_1_1)
{ after(grammarAccess.getImplicationExpressionAccess().getOperatorAssignment_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ImplicationExpression__Group_1__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ImplicationExpression__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ImplicationExpression__Group_1__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getImplicationExpressionAccess().getRightAssignment_1_2()); }
(rule__ImplicationExpression__RightAssignment_1_2)
{ after(grammarAccess.getImplicationExpressionAccess().getRightAssignment_1_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__DisjunctionExpression__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__DisjunctionExpression__Group__0__Impl
	rule__DisjunctionExpression__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__DisjunctionExpression__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDisjunctionExpressionAccess().getConjunctionExpressionParserRuleCall_0()); }
	ruleConjunctionExpression
{ after(grammarAccess.getDisjunctionExpressionAccess().getConjunctionExpressionParserRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__DisjunctionExpression__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__DisjunctionExpression__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__DisjunctionExpression__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDisjunctionExpressionAccess().getGroup_1()); }
(rule__DisjunctionExpression__Group_1__0)?
{ after(grammarAccess.getDisjunctionExpressionAccess().getGroup_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__DisjunctionExpression__Group_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__DisjunctionExpression__Group_1__0__Impl
	rule__DisjunctionExpression__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__DisjunctionExpression__Group_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDisjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
(

)
{ after(grammarAccess.getDisjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__DisjunctionExpression__Group_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__DisjunctionExpression__Group_1__1__Impl
	rule__DisjunctionExpression__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__DisjunctionExpression__Group_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDisjunctionExpressionAccess().getOperatorAssignment_1_1()); }
(rule__DisjunctionExpression__OperatorAssignment_1_1)
{ after(grammarAccess.getDisjunctionExpressionAccess().getOperatorAssignment_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__DisjunctionExpression__Group_1__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__DisjunctionExpression__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__DisjunctionExpression__Group_1__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDisjunctionExpressionAccess().getRightAssignment_1_2()); }
(rule__DisjunctionExpression__RightAssignment_1_2)
{ after(grammarAccess.getDisjunctionExpressionAccess().getRightAssignment_1_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__ConjunctionExpression__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConjunctionExpression__Group__0__Impl
	rule__ConjunctionExpression__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ConjunctionExpression__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConjunctionExpressionAccess().getRelationExpressionParserRuleCall_0()); }
	ruleRelationExpression
{ after(grammarAccess.getConjunctionExpressionAccess().getRelationExpressionParserRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConjunctionExpression__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConjunctionExpression__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ConjunctionExpression__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConjunctionExpressionAccess().getGroup_1()); }
(rule__ConjunctionExpression__Group_1__0)?
{ after(grammarAccess.getConjunctionExpressionAccess().getGroup_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ConjunctionExpression__Group_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConjunctionExpression__Group_1__0__Impl
	rule__ConjunctionExpression__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ConjunctionExpression__Group_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
(

)
{ after(grammarAccess.getConjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConjunctionExpression__Group_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConjunctionExpression__Group_1__1__Impl
	rule__ConjunctionExpression__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__ConjunctionExpression__Group_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConjunctionExpressionAccess().getOperatorAssignment_1_1()); }
(rule__ConjunctionExpression__OperatorAssignment_1_1)
{ after(grammarAccess.getConjunctionExpressionAccess().getOperatorAssignment_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConjunctionExpression__Group_1__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConjunctionExpression__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ConjunctionExpression__Group_1__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConjunctionExpressionAccess().getRightAssignment_1_2()); }
(rule__ConjunctionExpression__RightAssignment_1_2)
{ after(grammarAccess.getConjunctionExpressionAccess().getRightAssignment_1_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__RelationExpression__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__RelationExpression__Group__0__Impl
	rule__RelationExpression__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__RelationExpression__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRelationExpressionAccess().getAdditionExpressionParserRuleCall_0()); }
	ruleAdditionExpression
{ after(grammarAccess.getRelationExpressionAccess().getAdditionExpressionParserRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__RelationExpression__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__RelationExpression__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__RelationExpression__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRelationExpressionAccess().getGroup_1()); }
(rule__RelationExpression__Group_1__0)?
{ after(grammarAccess.getRelationExpressionAccess().getGroup_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__RelationExpression__Group_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__RelationExpression__Group_1__0__Impl
	rule__RelationExpression__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__RelationExpression__Group_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRelationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
(

)
{ after(grammarAccess.getRelationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__RelationExpression__Group_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__RelationExpression__Group_1__1__Impl
	rule__RelationExpression__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__RelationExpression__Group_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRelationExpressionAccess().getOperatorAssignment_1_1()); }
(rule__RelationExpression__OperatorAssignment_1_1)
{ after(grammarAccess.getRelationExpressionAccess().getOperatorAssignment_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__RelationExpression__Group_1__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__RelationExpression__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__RelationExpression__Group_1__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRelationExpressionAccess().getRightAssignment_1_2()); }
(rule__RelationExpression__RightAssignment_1_2)
{ after(grammarAccess.getRelationExpressionAccess().getRightAssignment_1_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__TimedExpression__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TimedExpression__Group__0__Impl
	rule__TimedExpression__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__TimedExpression__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedExpressionAccess().getClockAssignment_0()); }
(rule__TimedExpression__ClockAssignment_0)
{ after(grammarAccess.getTimedExpressionAccess().getClockAssignment_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TimedExpression__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TimedExpression__Group__1__Impl
	rule__TimedExpression__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__TimedExpression__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedExpressionAccess().getOperatorAssignment_1()); }
(rule__TimedExpression__OperatorAssignment_1)
{ after(grammarAccess.getTimedExpressionAccess().getOperatorAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TimedExpression__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TimedExpression__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__TimedExpression__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedExpressionAccess().getValueAssignment_2()); }
(rule__TimedExpression__ValueAssignment_2)
{ after(grammarAccess.getTimedExpressionAccess().getValueAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__AdditionExpression__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__AdditionExpression__Group__0__Impl
	rule__AdditionExpression__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__AdditionExpression__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAdditionExpressionAccess().getMultiplicationExpressionParserRuleCall_0()); }
	ruleMultiplicationExpression
{ after(grammarAccess.getAdditionExpressionAccess().getMultiplicationExpressionParserRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__AdditionExpression__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__AdditionExpression__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__AdditionExpression__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAdditionExpressionAccess().getGroup_1()); }
(rule__AdditionExpression__Group_1__0)?
{ after(grammarAccess.getAdditionExpressionAccess().getGroup_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__AdditionExpression__Group_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__AdditionExpression__Group_1__0__Impl
	rule__AdditionExpression__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__AdditionExpression__Group_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAdditionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
(

)
{ after(grammarAccess.getAdditionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__AdditionExpression__Group_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__AdditionExpression__Group_1__1__Impl
	rule__AdditionExpression__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__AdditionExpression__Group_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAdditionExpressionAccess().getOperatorAssignment_1_1()); }
(rule__AdditionExpression__OperatorAssignment_1_1)
{ after(grammarAccess.getAdditionExpressionAccess().getOperatorAssignment_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__AdditionExpression__Group_1__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__AdditionExpression__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__AdditionExpression__Group_1__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAdditionExpressionAccess().getRightAssignment_1_2()); }
(rule__AdditionExpression__RightAssignment_1_2)
{ after(grammarAccess.getAdditionExpressionAccess().getRightAssignment_1_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__MultiplicationExpression__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__MultiplicationExpression__Group__0__Impl
	rule__MultiplicationExpression__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__MultiplicationExpression__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getMultiplicationExpressionAccess().getNegatedExpressionParserRuleCall_0()); }
	ruleNegatedExpression
{ after(grammarAccess.getMultiplicationExpressionAccess().getNegatedExpressionParserRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__MultiplicationExpression__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__MultiplicationExpression__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__MultiplicationExpression__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getMultiplicationExpressionAccess().getGroup_1()); }
(rule__MultiplicationExpression__Group_1__0)?
{ after(grammarAccess.getMultiplicationExpressionAccess().getGroup_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__MultiplicationExpression__Group_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__MultiplicationExpression__Group_1__0__Impl
	rule__MultiplicationExpression__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__MultiplicationExpression__Group_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getMultiplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
(

)
{ after(grammarAccess.getMultiplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__MultiplicationExpression__Group_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__MultiplicationExpression__Group_1__1__Impl
	rule__MultiplicationExpression__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__MultiplicationExpression__Group_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getMultiplicationExpressionAccess().getOperatorAssignment_1_1()); }
(rule__MultiplicationExpression__OperatorAssignment_1_1)
{ after(grammarAccess.getMultiplicationExpressionAccess().getOperatorAssignment_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__MultiplicationExpression__Group_1__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__MultiplicationExpression__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__MultiplicationExpression__Group_1__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getMultiplicationExpressionAccess().getRightAssignment_1_2()); }
(rule__MultiplicationExpression__RightAssignment_1_2)
{ after(grammarAccess.getMultiplicationExpressionAccess().getRightAssignment_1_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__NegatedExpression__Group_0__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__NegatedExpression__Group_0__0__Impl
	rule__NegatedExpression__Group_0__1
;
finally {
	restoreStackSize(stackSize);
}

rule__NegatedExpression__Group_0__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getNegatedExpressionAccess().getUnaryOperationExpressionAction_0_0()); }
(

)
{ after(grammarAccess.getNegatedExpressionAccess().getUnaryOperationExpressionAction_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__NegatedExpression__Group_0__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__NegatedExpression__Group_0__1__Impl
	rule__NegatedExpression__Group_0__2
;
finally {
	restoreStackSize(stackSize);
}

rule__NegatedExpression__Group_0__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getNegatedExpressionAccess().getOperatorAssignment_0_1()); }
(rule__NegatedExpression__OperatorAssignment_0_1)
{ after(grammarAccess.getNegatedExpressionAccess().getOperatorAssignment_0_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__NegatedExpression__Group_0__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__NegatedExpression__Group_0__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__NegatedExpression__Group_0__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getNegatedExpressionAccess().getOperandAssignment_0_2()); }
(rule__NegatedExpression__OperandAssignment_0_2)
{ after(grammarAccess.getNegatedExpressionAccess().getOperandAssignment_0_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__BasicExpression__Group_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__BasicExpression__Group_1__0__Impl
	rule__BasicExpression__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__BasicExpression__Group_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0()); }

	'(' 

{ after(grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__BasicExpression__Group_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__BasicExpression__Group_1__1__Impl
	rule__BasicExpression__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__BasicExpression__Group_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getBasicExpressionAccess().getExpressionParserRuleCall_1_1()); }
	ruleExpression
{ after(grammarAccess.getBasicExpressionAccess().getExpressionParserRuleCall_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__BasicExpression__Group_1__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__BasicExpression__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__BasicExpression__Group_1__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getBasicExpressionAccess().getRightParenthesisKeyword_1_2()); }

	')' 

{ after(grammarAccess.getBasicExpressionAccess().getRightParenthesisKeyword_1_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__EnumValue__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__EnumValue__Group__0__Impl
	rule__EnumValue__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__EnumValue__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getEnumValueAccess().getTypeAssignment_0()); }
(rule__EnumValue__TypeAssignment_0)
{ after(grammarAccess.getEnumValueAccess().getTypeAssignment_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__EnumValue__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__EnumValue__Group__1__Impl
	rule__EnumValue__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__EnumValue__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getEnumValueAccess().getColonKeyword_1()); }

	':' 

{ after(grammarAccess.getEnumValueAccess().getColonKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__EnumValue__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__EnumValue__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__EnumValue__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getEnumValueAccess().getValueAssignment_2()); }
(rule__EnumValue__ValueAssignment_2)
{ after(grammarAccess.getEnumValueAccess().getValueAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__NullValue__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__NullValue__Group__0__Impl
	rule__NullValue__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__NullValue__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getNullValueAccess().getNullValueAction_0()); }
(

)
{ after(grammarAccess.getNullValueAccess().getNullValueAction_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__NullValue__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__NullValue__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__NullValue__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getNullValueAccess().getNullKeyword_1()); }

	'null' 

{ after(grammarAccess.getNullValueAccess().getNullKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__CollectionAccess__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__CollectionAccess__Group__0__Impl
	rule__CollectionAccess__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__CollectionAccess__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollectionAccessAccess().getCollectionOperationAssignment_0()); }
(rule__CollectionAccess__CollectionOperationAssignment_0)
{ after(grammarAccess.getCollectionAccessAccess().getCollectionOperationAssignment_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__CollectionAccess__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__CollectionAccess__Group__1__Impl
	rule__CollectionAccess__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__CollectionAccess__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollectionAccessAccess().getLeftParenthesisKeyword_1()); }

	'(' 

{ after(grammarAccess.getCollectionAccessAccess().getLeftParenthesisKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__CollectionAccess__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__CollectionAccess__Group__2__Impl
	rule__CollectionAccess__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__CollectionAccess__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollectionAccessAccess().getParameterAssignment_2()); }
(rule__CollectionAccess__ParameterAssignment_2)?
{ after(grammarAccess.getCollectionAccessAccess().getParameterAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__CollectionAccess__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__CollectionAccess__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__CollectionAccess__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollectionAccessAccess().getRightParenthesisKeyword_3()); }

	')' 

{ after(grammarAccess.getCollectionAccessAccess().getRightParenthesisKeyword_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}










rule__FeatureAccess__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group__0__Impl
	rule__FeatureAccess__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getTargetAssignment_0()); }
(rule__FeatureAccess__TargetAssignment_0)
{ after(grammarAccess.getFeatureAccessAccess().getTargetAssignment_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__FeatureAccess__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group__1__Impl
	rule__FeatureAccess__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getFullStopKeyword_1()); }

	'.' 

{ after(grammarAccess.getFeatureAccessAccess().getFullStopKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__FeatureAccess__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getAlternatives_2()); }
(rule__FeatureAccess__Alternatives_2)
{ after(grammarAccess.getFeatureAccessAccess().getAlternatives_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__FeatureAccess__Group_2_0__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_0__0__Impl
	rule__FeatureAccess__Group_2_0__1
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_0__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getValueAssignment_2_0_0()); }
(rule__FeatureAccess__ValueAssignment_2_0_0)
{ after(grammarAccess.getFeatureAccessAccess().getValueAssignment_2_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__FeatureAccess__Group_2_0__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_0__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_0__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getGroup_2_0_1()); }
(rule__FeatureAccess__Group_2_0_1__0)?
{ after(grammarAccess.getFeatureAccessAccess().getGroup_2_0_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__FeatureAccess__Group_2_0_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_0_1__0__Impl
	rule__FeatureAccess__Group_2_0_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_0_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getFullStopKeyword_2_0_1_0()); }

	'.' 

{ after(grammarAccess.getFeatureAccessAccess().getFullStopKeyword_2_0_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__FeatureAccess__Group_2_0_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_0_1__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_0_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getCollectionAccessAssignment_2_0_1_1()); }
(rule__FeatureAccess__CollectionAccessAssignment_2_0_1_1)
{ after(grammarAccess.getFeatureAccessAccess().getCollectionAccessAssignment_2_0_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__FeatureAccess__Group_2_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_1__0__Impl
	rule__FeatureAccess__Group_2_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getValueAssignment_2_1_0()); }
(rule__FeatureAccess__ValueAssignment_2_1_0)
{ after(grammarAccess.getFeatureAccessAccess().getValueAssignment_2_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__FeatureAccess__Group_2_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_1__1__Impl
	rule__FeatureAccess__Group_2_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getLeftParenthesisKeyword_2_1_1()); }

	'(' 

{ after(grammarAccess.getFeatureAccessAccess().getLeftParenthesisKeyword_2_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__FeatureAccess__Group_2_1__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_1__2__Impl
	rule__FeatureAccess__Group_2_1__3
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_1__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getGroup_2_1_2()); }
(rule__FeatureAccess__Group_2_1_2__0)?
{ after(grammarAccess.getFeatureAccessAccess().getGroup_2_1_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__FeatureAccess__Group_2_1__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_1__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_1__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getRightParenthesisKeyword_2_1_3()); }

	')' 

{ after(grammarAccess.getFeatureAccessAccess().getRightParenthesisKeyword_2_1_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}










rule__FeatureAccess__Group_2_1_2__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_1_2__0__Impl
	rule__FeatureAccess__Group_2_1_2__1
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_1_2__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getParametersAssignment_2_1_2_0()); }
(rule__FeatureAccess__ParametersAssignment_2_1_2_0)
{ after(grammarAccess.getFeatureAccessAccess().getParametersAssignment_2_1_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__FeatureAccess__Group_2_1_2__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_1_2__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_1_2__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getGroup_2_1_2_1()); }
(rule__FeatureAccess__Group_2_1_2_1__0)*
{ after(grammarAccess.getFeatureAccessAccess().getGroup_2_1_2_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__FeatureAccess__Group_2_1_2_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_1_2_1__0__Impl
	rule__FeatureAccess__Group_2_1_2_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_1_2_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getCommaKeyword_2_1_2_1_0()); }

	',' 

{ after(grammarAccess.getFeatureAccessAccess().getCommaKeyword_2_1_2_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__FeatureAccess__Group_2_1_2_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_1_2_1__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_1_2_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getParametersAssignment_2_1_2_1_1()); }
(rule__FeatureAccess__ParametersAssignment_2_1_2_1_1)
{ after(grammarAccess.getFeatureAccessAccess().getParametersAssignment_2_1_2_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}







rule__Collaboration__ImportsAssignment_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollaborationAccess().getImportsImportParserRuleCall_0_0()); }
	ruleImport{ after(grammarAccess.getCollaborationAccess().getImportsImportParserRuleCall_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Collaboration__DomainsAssignment_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollaborationAccess().getDomainsEPackageCrossReference_1_1_0()); }
(
{ before(grammarAccess.getCollaborationAccess().getDomainsEPackageFQNParserRuleCall_1_1_0_1()); }
	ruleFQN{ after(grammarAccess.getCollaborationAccess().getDomainsEPackageFQNParserRuleCall_1_1_0_1()); }
)
{ after(grammarAccess.getCollaborationAccess().getDomainsEPackageCrossReference_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Collaboration__ContextsAssignment_2_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollaborationAccess().getContextsEPackageCrossReference_2_1_0()); }
(
{ before(grammarAccess.getCollaborationAccess().getContextsEPackageFQNParserRuleCall_2_1_0_1()); }
	ruleFQN{ after(grammarAccess.getCollaborationAccess().getContextsEPackageFQNParserRuleCall_2_1_0_1()); }
)
{ after(grammarAccess.getCollaborationAccess().getContextsEPackageCrossReference_2_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Collaboration__NameAssignment_4
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollaborationAccess().getNameIDTerminalRuleCall_4_0()); }
	RULE_ID{ after(grammarAccess.getCollaborationAccess().getNameIDTerminalRuleCall_4_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Collaboration__RolesAssignment_6
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollaborationAccess().getRolesRoleParserRuleCall_6_0()); }
	ruleRole{ after(grammarAccess.getCollaborationAccess().getRolesRoleParserRuleCall_6_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Collaboration__ScenariosAssignment_7
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollaborationAccess().getScenariosScenarioParserRuleCall_7_0()); }
	ruleScenario{ after(grammarAccess.getCollaborationAccess().getScenariosScenarioParserRuleCall_7_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Role__StaticAssignment_0_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRoleAccess().getStaticStaticKeyword_0_0_0()); }
(
{ before(grammarAccess.getRoleAccess().getStaticStaticKeyword_0_0_0()); }

	'static' 

{ after(grammarAccess.getRoleAccess().getStaticStaticKeyword_0_0_0()); }
)

{ after(grammarAccess.getRoleAccess().getStaticStaticKeyword_0_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Role__MultiRoleAssignment_0_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRoleAccess().getMultiRoleMultiKeyword_0_1_1_0()); }
(
{ before(grammarAccess.getRoleAccess().getMultiRoleMultiKeyword_0_1_1_0()); }

	'multi' 

{ after(grammarAccess.getRoleAccess().getMultiRoleMultiKeyword_0_1_1_0()); }
)

{ after(grammarAccess.getRoleAccess().getMultiRoleMultiKeyword_0_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Role__TypeAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRoleAccess().getTypeEClassCrossReference_2_0()); }
(
{ before(grammarAccess.getRoleAccess().getTypeEClassIDTerminalRuleCall_2_0_1()); }
	RULE_ID{ after(grammarAccess.getRoleAccess().getTypeEClassIDTerminalRuleCall_2_0_1()); }
)
{ after(grammarAccess.getRoleAccess().getTypeEClassCrossReference_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Role__NameAssignment_3
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRoleAccess().getNameIDTerminalRuleCall_3_0()); }
	RULE_ID{ after(grammarAccess.getRoleAccess().getNameIDTerminalRuleCall_3_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__SingularAssignment_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getSingularSingularKeyword_0_0()); }
(
{ before(grammarAccess.getScenarioAccess().getSingularSingularKeyword_0_0()); }

	'singular' 

{ after(grammarAccess.getScenarioAccess().getSingularSingularKeyword_0_0()); }
)

{ after(grammarAccess.getScenarioAccess().getSingularSingularKeyword_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__KindAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getKindScenarioKindEnumRuleCall_1_0()); }
	ruleScenarioKind{ after(grammarAccess.getScenarioAccess().getKindScenarioKindEnumRuleCall_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__NameAssignment_3
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getNameIDTerminalRuleCall_3_0()); }
	RULE_ID{ after(grammarAccess.getScenarioAccess().getNameIDTerminalRuleCall_3_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__OptimizeCostAssignment_4_0_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getOptimizeCostOptimizeKeyword_4_0_0_0()); }
(
{ before(grammarAccess.getScenarioAccess().getOptimizeCostOptimizeKeyword_4_0_0_0()); }

	'optimize' 

{ after(grammarAccess.getScenarioAccess().getOptimizeCostOptimizeKeyword_4_0_0_0()); }
)

{ after(grammarAccess.getScenarioAccess().getOptimizeCostOptimizeKeyword_4_0_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__CostAssignment_4_1_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getCostDOUBLETerminalRuleCall_4_1_2_0()); }
	RULE_DOUBLE{ after(grammarAccess.getScenarioAccess().getCostDOUBLETerminalRuleCall_4_1_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__ContextsAssignment_5_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getContextsEClassCrossReference_5_1_0()); }
(
{ before(grammarAccess.getScenarioAccess().getContextsEClassIDTerminalRuleCall_5_1_0_1()); }
	RULE_ID{ after(grammarAccess.getScenarioAccess().getContextsEClassIDTerminalRuleCall_5_1_0_1()); }
)
{ after(grammarAccess.getScenarioAccess().getContextsEClassCrossReference_5_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__ContextsAssignment_5_2_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getContextsEClassCrossReference_5_2_1_0()); }
(
{ before(grammarAccess.getScenarioAccess().getContextsEClassIDTerminalRuleCall_5_2_1_0_1()); }
	RULE_ID{ after(grammarAccess.getScenarioAccess().getContextsEClassIDTerminalRuleCall_5_2_1_0_1()); }
)
{ after(grammarAccess.getScenarioAccess().getContextsEClassCrossReference_5_2_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__RoleBindingsAssignment_6_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getRoleBindingsRoleBindingConstraintParserRuleCall_6_2_0()); }
	ruleRoleBindingConstraint{ after(grammarAccess.getScenarioAccess().getRoleBindingsRoleBindingConstraintParserRuleCall_6_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Scenario__OwnedInteractionAssignment_7
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getScenarioAccess().getOwnedInteractionInteractionParserRuleCall_7_0()); }
	ruleInteraction{ after(grammarAccess.getScenarioAccess().getOwnedInteractionInteractionParserRuleCall_7_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__RoleBindingConstraint__RoleAssignment_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRoleBindingConstraintAccess().getRoleRoleCrossReference_0_0()); }
(
{ before(grammarAccess.getRoleBindingConstraintAccess().getRoleRoleIDTerminalRuleCall_0_0_1()); }
	RULE_ID{ after(grammarAccess.getRoleBindingConstraintAccess().getRoleRoleIDTerminalRuleCall_0_0_1()); }
)
{ after(grammarAccess.getRoleBindingConstraintAccess().getRoleRoleCrossReference_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__RoleBindingConstraint__BindingExpressionAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRoleBindingConstraintAccess().getBindingExpressionBindingExpressionParserRuleCall_2_0()); }
	ruleBindingExpression{ after(grammarAccess.getRoleBindingConstraintAccess().getBindingExpressionBindingExpressionParserRuleCall_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccessBindingExpression__FeatureaccessAssignment
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessBindingExpressionAccess().getFeatureaccessFeatureAccessParserRuleCall_0()); }
	ruleFeatureAccess{ after(grammarAccess.getFeatureAccessBindingExpressionAccess().getFeatureaccessFeatureAccessParserRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__VariableFragment__ExpressionAssignment
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVariableFragmentAccess().getExpressionAlternatives_0()); }
(rule__VariableFragment__ExpressionAlternatives_0)
{ after(grammarAccess.getVariableFragmentAccess().getExpressionAlternatives_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Interaction__FragmentsAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getInteractionAccess().getFragmentsInteractionFragmentParserRuleCall_2_0()); }
	ruleInteractionFragment{ after(grammarAccess.getInteractionAccess().getFragmentsInteractionFragmentParserRuleCall_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Interaction__ConstraintsAssignment_4
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getInteractionAccess().getConstraintsConstraintBlockParserRuleCall_4_0()); }
	ruleConstraintBlock{ after(grammarAccess.getInteractionAccess().getConstraintsConstraintBlockParserRuleCall_4_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__StrictAssignment_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getStrictStrictKeyword_0_0()); }
(
{ before(grammarAccess.getModalMessageAccess().getStrictStrictKeyword_0_0()); }

	'strict' 

{ after(grammarAccess.getModalMessageAccess().getStrictStrictKeyword_0_0()); }
)

{ after(grammarAccess.getModalMessageAccess().getStrictStrictKeyword_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__MonitoredAssignment_1_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getMonitoredMonitoredKeyword_1_0_0()); }
(
{ before(grammarAccess.getModalMessageAccess().getMonitoredMonitoredKeyword_1_0_0()); }

	'monitored' 

{ after(grammarAccess.getModalMessageAccess().getMonitoredMonitoredKeyword_1_0_0()); }
)

{ after(grammarAccess.getModalMessageAccess().getMonitoredMonitoredKeyword_1_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__ExpectationKindAssignment_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getExpectationKindExpectationKindEnumRuleCall_1_1_0()); }
	ruleExpectationKind{ after(grammarAccess.getModalMessageAccess().getExpectationKindExpectationKindEnumRuleCall_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__SenderAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getSenderRoleCrossReference_2_0()); }
(
{ before(grammarAccess.getModalMessageAccess().getSenderRoleIDTerminalRuleCall_2_0_1()); }
	RULE_ID{ after(grammarAccess.getModalMessageAccess().getSenderRoleIDTerminalRuleCall_2_0_1()); }
)
{ after(grammarAccess.getModalMessageAccess().getSenderRoleCrossReference_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__ReceiverAssignment_4
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getReceiverRoleCrossReference_4_0()); }
(
{ before(grammarAccess.getModalMessageAccess().getReceiverRoleIDTerminalRuleCall_4_0_1()); }
	RULE_ID{ after(grammarAccess.getModalMessageAccess().getReceiverRoleIDTerminalRuleCall_4_0_1()); }
)
{ after(grammarAccess.getModalMessageAccess().getReceiverRoleCrossReference_4_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__ModelElementAssignment_6
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getModelElementETypedElementCrossReference_6_0()); }
(
{ before(grammarAccess.getModalMessageAccess().getModelElementETypedElementIDTerminalRuleCall_6_0_1()); }
	RULE_ID{ after(grammarAccess.getModalMessageAccess().getModelElementETypedElementIDTerminalRuleCall_6_0_1()); }
)
{ after(grammarAccess.getModalMessageAccess().getModelElementETypedElementCrossReference_6_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__CollectionModificationAssignment_7_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getCollectionModificationCollectionModificationEnumRuleCall_7_1_0()); }
	ruleCollectionModification{ after(grammarAccess.getModalMessageAccess().getCollectionModificationCollectionModificationEnumRuleCall_7_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__ParametersAssignment_8_1_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getParametersParameterBindingParserRuleCall_8_1_0_0()); }
	ruleParameterBinding{ after(grammarAccess.getModalMessageAccess().getParametersParameterBindingParserRuleCall_8_1_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ModalMessage__ParametersAssignment_8_1_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModalMessageAccess().getParametersParameterBindingParserRuleCall_8_1_1_1_0()); }
	ruleParameterBinding{ after(grammarAccess.getModalMessageAccess().getParametersParameterBindingParserRuleCall_8_1_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ParameterBinding__BindingExpressionAssignment
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getParameterBindingAccess().getBindingExpressionParameterExpressionParserRuleCall_0()); }
	ruleParameterExpression{ after(grammarAccess.getParameterBindingAccess().getBindingExpressionParameterExpressionParserRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ValueParameterExpression__ValueAssignment
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getValueParameterExpressionAccess().getValueExpressionParserRuleCall_0()); }
	ruleExpression{ after(grammarAccess.getValueParameterExpressionAccess().getValueExpressionParserRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__VariableBindingParameterExpression__VariableAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVariableBindingParameterExpressionAccess().getVariableVariableValueParserRuleCall_1_0()); }
	ruleVariableValue{ after(grammarAccess.getVariableBindingParameterExpressionAccess().getVariableVariableValueParserRuleCall_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Alternative__CasesAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAlternativeAccess().getCasesCaseParserRuleCall_2_0()); }
	ruleCase{ after(grammarAccess.getAlternativeAccess().getCasesCaseParserRuleCall_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Alternative__CasesAssignment_3_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAlternativeAccess().getCasesCaseParserRuleCall_3_1_0()); }
	ruleCase{ after(grammarAccess.getAlternativeAccess().getCasesCaseParserRuleCall_3_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Case__CaseConditionAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCaseAccess().getCaseConditionConditionParserRuleCall_1_0()); }
	ruleCondition{ after(grammarAccess.getCaseAccess().getCaseConditionConditionParserRuleCall_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Case__CaseInteractionAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCaseAccess().getCaseInteractionInteractionParserRuleCall_2_0()); }
	ruleInteraction{ after(grammarAccess.getCaseAccess().getCaseInteractionInteractionParserRuleCall_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Loop__LoopConditionAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getLoopAccess().getLoopConditionConditionParserRuleCall_1_0()); }
	ruleCondition{ after(grammarAccess.getLoopAccess().getLoopConditionConditionParserRuleCall_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Loop__BodyInteractionAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getLoopAccess().getBodyInteractionInteractionParserRuleCall_2_0()); }
	ruleInteraction{ after(grammarAccess.getLoopAccess().getBodyInteractionInteractionParserRuleCall_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Parallel__ParallelInteractionAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getParallelAccess().getParallelInteractionInteractionParserRuleCall_2_0()); }
	ruleInteraction{ after(grammarAccess.getParallelAccess().getParallelInteractionInteractionParserRuleCall_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Parallel__ParallelInteractionAssignment_3_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getParallelAccess().getParallelInteractionInteractionParserRuleCall_3_1_0()); }
	ruleInteraction{ after(grammarAccess.getParallelAccess().getParallelInteractionInteractionParserRuleCall_3_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__WaitCondition__StrictAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getWaitConditionAccess().getStrictStrictKeyword_1_0()); }
(
{ before(grammarAccess.getWaitConditionAccess().getStrictStrictKeyword_1_0()); }

	'strict' 

{ after(grammarAccess.getWaitConditionAccess().getStrictStrictKeyword_1_0()); }
)

{ after(grammarAccess.getWaitConditionAccess().getStrictStrictKeyword_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__WaitCondition__RequestedAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getWaitConditionAccess().getRequestedEventuallyKeyword_2_0()); }
(
{ before(grammarAccess.getWaitConditionAccess().getRequestedEventuallyKeyword_2_0()); }

	'eventually' 

{ after(grammarAccess.getWaitConditionAccess().getRequestedEventuallyKeyword_2_0()); }
)

{ after(grammarAccess.getWaitConditionAccess().getRequestedEventuallyKeyword_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__WaitCondition__ConditionExpressionAssignment_4
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getWaitConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_4_0()); }
	ruleConditionExpression{ after(grammarAccess.getWaitConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_4_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__InterruptCondition__ConditionExpressionAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getInterruptConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_2_0()); }
	ruleConditionExpression{ after(grammarAccess.getInterruptConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ViolationCondition__ConditionExpressionAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getViolationConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_2_0()); }
	ruleConditionExpression{ after(grammarAccess.getViolationConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__TimedWaitCondition__StrictAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedWaitConditionAccess().getStrictStrictKeyword_2_0()); }
(
{ before(grammarAccess.getTimedWaitConditionAccess().getStrictStrictKeyword_2_0()); }

	'strict' 

{ after(grammarAccess.getTimedWaitConditionAccess().getStrictStrictKeyword_2_0()); }
)

{ after(grammarAccess.getTimedWaitConditionAccess().getStrictStrictKeyword_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__TimedWaitCondition__RequestedAssignment_3
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedWaitConditionAccess().getRequestedEventuallyKeyword_3_0()); }
(
{ before(grammarAccess.getTimedWaitConditionAccess().getRequestedEventuallyKeyword_3_0()); }

	'eventually' 

{ after(grammarAccess.getTimedWaitConditionAccess().getRequestedEventuallyKeyword_3_0()); }
)

{ after(grammarAccess.getTimedWaitConditionAccess().getRequestedEventuallyKeyword_3_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__TimedWaitCondition__TimedConditionExpressionAssignment_5
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedWaitConditionAccess().getTimedConditionExpressionTimedExpressionParserRuleCall_5_0()); }
	ruleTimedExpression{ after(grammarAccess.getTimedWaitConditionAccess().getTimedConditionExpressionTimedExpressionParserRuleCall_5_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__TimedViolationCondition__TimedConditionExpressionAssignment_3
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedViolationConditionAccess().getTimedConditionExpressionTimedExpressionParserRuleCall_3_0()); }
	ruleTimedExpression{ after(grammarAccess.getTimedViolationConditionAccess().getTimedConditionExpressionTimedExpressionParserRuleCall_3_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__TimedInterruptCondition__TimedConditionExpressionAssignment_3
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedInterruptConditionAccess().getTimedConditionExpressionTimedExpressionParserRuleCall_3_0()); }
	ruleTimedExpression{ after(grammarAccess.getTimedInterruptConditionAccess().getTimedConditionExpressionTimedExpressionParserRuleCall_3_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Condition__ConditionExpressionAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_1_0()); }
	ruleConditionExpression{ after(grammarAccess.getConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ConditionExpression__ExpressionAssignment
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConditionExpressionAccess().getExpressionExpressionParserRuleCall_0()); }
	ruleExpression{ after(grammarAccess.getConditionExpressionAccess().getExpressionExpressionParserRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintBlock__ConsiderAssignment_3_0_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintBlockAccess().getConsiderConstraintMessageParserRuleCall_3_0_1_0()); }
	ruleConstraintMessage{ after(grammarAccess.getConstraintBlockAccess().getConsiderConstraintMessageParserRuleCall_3_0_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintBlock__IgnoreAssignment_3_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintBlockAccess().getIgnoreConstraintMessageParserRuleCall_3_1_1_0()); }
	ruleConstraintMessage{ after(grammarAccess.getConstraintBlockAccess().getIgnoreConstraintMessageParserRuleCall_3_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintBlock__ForbiddenAssignment_3_2_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintBlockAccess().getForbiddenConstraintMessageParserRuleCall_3_2_1_0()); }
	ruleConstraintMessage{ after(grammarAccess.getConstraintBlockAccess().getForbiddenConstraintMessageParserRuleCall_3_2_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintBlock__InterruptAssignment_3_3_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintBlockAccess().getInterruptConstraintMessageParserRuleCall_3_3_1_0()); }
	ruleConstraintMessage{ after(grammarAccess.getConstraintBlockAccess().getInterruptConstraintMessageParserRuleCall_3_3_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__SenderAssignment_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getSenderRoleCrossReference_0_0()); }
(
{ before(grammarAccess.getConstraintMessageAccess().getSenderRoleIDTerminalRuleCall_0_0_1()); }
	RULE_ID{ after(grammarAccess.getConstraintMessageAccess().getSenderRoleIDTerminalRuleCall_0_0_1()); }
)
{ after(grammarAccess.getConstraintMessageAccess().getSenderRoleCrossReference_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__ReceiverAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getReceiverRoleCrossReference_2_0()); }
(
{ before(grammarAccess.getConstraintMessageAccess().getReceiverRoleIDTerminalRuleCall_2_0_1()); }
	RULE_ID{ after(grammarAccess.getConstraintMessageAccess().getReceiverRoleIDTerminalRuleCall_2_0_1()); }
)
{ after(grammarAccess.getConstraintMessageAccess().getReceiverRoleCrossReference_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__ModelElementAssignment_4
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getModelElementETypedElementCrossReference_4_0()); }
(
{ before(grammarAccess.getConstraintMessageAccess().getModelElementETypedElementIDTerminalRuleCall_4_0_1()); }
	RULE_ID{ after(grammarAccess.getConstraintMessageAccess().getModelElementETypedElementIDTerminalRuleCall_4_0_1()); }
)
{ after(grammarAccess.getConstraintMessageAccess().getModelElementETypedElementCrossReference_4_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__CollectionModificationAssignment_5_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getCollectionModificationCollectionModificationEnumRuleCall_5_1_0()); }
	ruleCollectionModification{ after(grammarAccess.getConstraintMessageAccess().getCollectionModificationCollectionModificationEnumRuleCall_5_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__ParametersAssignment_7_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getParametersParameterBindingParserRuleCall_7_0_0()); }
	ruleParameterBinding{ after(grammarAccess.getConstraintMessageAccess().getParametersParameterBindingParserRuleCall_7_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ConstraintMessage__ParametersAssignment_7_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConstraintMessageAccess().getParametersParameterBindingParserRuleCall_7_1_1_0()); }
	ruleParameterBinding{ after(grammarAccess.getConstraintMessageAccess().getParametersParameterBindingParserRuleCall_7_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}




rule__Import__ImportURIAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); }
	RULE_STRING{ after(grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ExpressionRegion__ExpressionsAssignment_2_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExpressionRegionAccess().getExpressionsExpressionOrRegionParserRuleCall_2_0_0()); }
	ruleExpressionOrRegion{ after(grammarAccess.getExpressionRegionAccess().getExpressionsExpressionOrRegionParserRuleCall_2_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}



rule__VariableAssignment__VariableAssignment_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationCrossReference_0_0()); }
(
{ before(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationIDTerminalRuleCall_0_0_1()); }
	RULE_ID{ after(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationIDTerminalRuleCall_0_0_1()); }
)
{ after(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationCrossReference_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__VariableAssignment__ExpressionAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVariableAssignmentAccess().getExpressionExpressionParserRuleCall_2_0()); }
	ruleExpression{ after(grammarAccess.getVariableAssignmentAccess().getExpressionExpressionParserRuleCall_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__TypedVariableDeclaration__TypeAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierCrossReference_1_0()); }
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierIDTerminalRuleCall_1_0_1()); }
	RULE_ID{ after(grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierIDTerminalRuleCall_1_0_1()); }
)
{ after(grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierCrossReference_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__TypedVariableDeclaration__NameAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getNameIDTerminalRuleCall_2_0()); }
	RULE_ID{ after(grammarAccess.getTypedVariableDeclarationAccess().getNameIDTerminalRuleCall_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__TypedVariableDeclaration__ExpressionAssignment_3_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getExpressionExpressionParserRuleCall_3_1_0()); }
	ruleExpression{ after(grammarAccess.getTypedVariableDeclarationAccess().getExpressionExpressionParserRuleCall_3_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ClockDeclaration__NameAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockDeclarationAccess().getNameIDTerminalRuleCall_2_0()); }
	RULE_ID{ after(grammarAccess.getClockDeclarationAccess().getNameIDTerminalRuleCall_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ClockDeclaration__ExpressionAssignment_3_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockDeclarationAccess().getExpressionIntegerValueParserRuleCall_3_1_0()); }
	ruleIntegerValue{ after(grammarAccess.getClockDeclarationAccess().getExpressionIntegerValueParserRuleCall_3_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ClockAssignment__VariableAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockAssignmentAccess().getVariableClockDeclarationCrossReference_1_0()); }
(
{ before(grammarAccess.getClockAssignmentAccess().getVariableClockDeclarationIDTerminalRuleCall_1_0_1()); }
	RULE_ID{ after(grammarAccess.getClockAssignmentAccess().getVariableClockDeclarationIDTerminalRuleCall_1_0_1()); }
)
{ after(grammarAccess.getClockAssignmentAccess().getVariableClockDeclarationCrossReference_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ClockAssignment__ExpressionAssignment_2_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockAssignmentAccess().getExpressionIntegerValueParserRuleCall_2_1_0()); }
	ruleIntegerValue{ after(grammarAccess.getClockAssignmentAccess().getExpressionIntegerValueParserRuleCall_2_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ImplicationExpression__OperatorAssignment_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getImplicationExpressionAccess().getOperatorEqualsSignGreaterThanSignKeyword_1_1_0()); }
(
{ before(grammarAccess.getImplicationExpressionAccess().getOperatorEqualsSignGreaterThanSignKeyword_1_1_0()); }

	'=>' 

{ after(grammarAccess.getImplicationExpressionAccess().getOperatorEqualsSignGreaterThanSignKeyword_1_1_0()); }
)

{ after(grammarAccess.getImplicationExpressionAccess().getOperatorEqualsSignGreaterThanSignKeyword_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ImplicationExpression__RightAssignment_1_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getImplicationExpressionAccess().getRightImplicationExpressionParserRuleCall_1_2_0()); }
	ruleImplicationExpression{ after(grammarAccess.getImplicationExpressionAccess().getRightImplicationExpressionParserRuleCall_1_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__DisjunctionExpression__OperatorAssignment_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineVerticalLineKeyword_1_1_0()); }
(
{ before(grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineVerticalLineKeyword_1_1_0()); }

	'||' 

{ after(grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineVerticalLineKeyword_1_1_0()); }
)

{ after(grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineVerticalLineKeyword_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__DisjunctionExpression__RightAssignment_1_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDisjunctionExpressionAccess().getRightDisjunctionExpressionParserRuleCall_1_2_0()); }
	ruleDisjunctionExpression{ after(grammarAccess.getDisjunctionExpressionAccess().getRightDisjunctionExpressionParserRuleCall_1_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ConjunctionExpression__OperatorAssignment_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandAmpersandKeyword_1_1_0()); }
(
{ before(grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandAmpersandKeyword_1_1_0()); }

	'&&' 

{ after(grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandAmpersandKeyword_1_1_0()); }
)

{ after(grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandAmpersandKeyword_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ConjunctionExpression__RightAssignment_1_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConjunctionExpressionAccess().getRightConjunctionExpressionParserRuleCall_1_2_0()); }
	ruleConjunctionExpression{ after(grammarAccess.getConjunctionExpressionAccess().getRightConjunctionExpressionParserRuleCall_1_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__RelationExpression__OperatorAssignment_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRelationExpressionAccess().getOperatorAlternatives_1_1_0()); }
(rule__RelationExpression__OperatorAlternatives_1_1_0)
{ after(grammarAccess.getRelationExpressionAccess().getOperatorAlternatives_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__RelationExpression__RightAssignment_1_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRelationExpressionAccess().getRightRelationExpressionParserRuleCall_1_2_0()); }
	ruleRelationExpression{ after(grammarAccess.getRelationExpressionAccess().getRightRelationExpressionParserRuleCall_1_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__TimedExpression__ClockAssignment_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedExpressionAccess().getClockClockDeclarationCrossReference_0_0()); }
(
{ before(grammarAccess.getTimedExpressionAccess().getClockClockDeclarationIDTerminalRuleCall_0_0_1()); }
	RULE_ID{ after(grammarAccess.getTimedExpressionAccess().getClockClockDeclarationIDTerminalRuleCall_0_0_1()); }
)
{ after(grammarAccess.getTimedExpressionAccess().getClockClockDeclarationCrossReference_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__TimedExpression__OperatorAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedExpressionAccess().getOperatorAlternatives_1_0()); }
(rule__TimedExpression__OperatorAlternatives_1_0)
{ after(grammarAccess.getTimedExpressionAccess().getOperatorAlternatives_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__TimedExpression__ValueAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTimedExpressionAccess().getValueINTTerminalRuleCall_2_0()); }
	RULE_INT{ after(grammarAccess.getTimedExpressionAccess().getValueINTTerminalRuleCall_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__AdditionExpression__OperatorAssignment_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAdditionExpressionAccess().getOperatorAlternatives_1_1_0()); }
(rule__AdditionExpression__OperatorAlternatives_1_1_0)
{ after(grammarAccess.getAdditionExpressionAccess().getOperatorAlternatives_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__AdditionExpression__RightAssignment_1_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAdditionExpressionAccess().getRightAdditionExpressionParserRuleCall_1_2_0()); }
	ruleAdditionExpression{ after(grammarAccess.getAdditionExpressionAccess().getRightAdditionExpressionParserRuleCall_1_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__MultiplicationExpression__OperatorAssignment_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getMultiplicationExpressionAccess().getOperatorAlternatives_1_1_0()); }
(rule__MultiplicationExpression__OperatorAlternatives_1_1_0)
{ after(grammarAccess.getMultiplicationExpressionAccess().getOperatorAlternatives_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__MultiplicationExpression__RightAssignment_1_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getMultiplicationExpressionAccess().getRightMultiplicationExpressionParserRuleCall_1_2_0()); }
	ruleMultiplicationExpression{ after(grammarAccess.getMultiplicationExpressionAccess().getRightMultiplicationExpressionParserRuleCall_1_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__NegatedExpression__OperatorAssignment_0_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getNegatedExpressionAccess().getOperatorAlternatives_0_1_0()); }
(rule__NegatedExpression__OperatorAlternatives_0_1_0)
{ after(grammarAccess.getNegatedExpressionAccess().getOperatorAlternatives_0_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__NegatedExpression__OperandAssignment_0_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getNegatedExpressionAccess().getOperandBasicExpressionParserRuleCall_0_2_0()); }
	ruleBasicExpression{ after(grammarAccess.getNegatedExpressionAccess().getOperandBasicExpressionParserRuleCall_0_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__IntegerValue__ValueAssignment
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getIntegerValueAccess().getValueAlternatives_0()); }
(rule__IntegerValue__ValueAlternatives_0)
{ after(grammarAccess.getIntegerValueAccess().getValueAlternatives_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__BooleanValue__ValueAssignment
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getBooleanValueAccess().getValueBOOLTerminalRuleCall_0()); }
	RULE_BOOL{ after(grammarAccess.getBooleanValueAccess().getValueBOOLTerminalRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__StringValue__ValueAssignment
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); }
	RULE_STRING{ after(grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__EnumValue__TypeAssignment_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getEnumValueAccess().getTypeEEnumCrossReference_0_0()); }
(
{ before(grammarAccess.getEnumValueAccess().getTypeEEnumIDTerminalRuleCall_0_0_1()); }
	RULE_ID{ after(grammarAccess.getEnumValueAccess().getTypeEEnumIDTerminalRuleCall_0_0_1()); }
)
{ after(grammarAccess.getEnumValueAccess().getTypeEEnumCrossReference_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__EnumValue__ValueAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getEnumValueAccess().getValueEEnumLiteralCrossReference_2_0()); }
(
{ before(grammarAccess.getEnumValueAccess().getValueEEnumLiteralIDTerminalRuleCall_2_0_1()); }
	RULE_ID{ after(grammarAccess.getEnumValueAccess().getValueEEnumLiteralIDTerminalRuleCall_2_0_1()); }
)
{ after(grammarAccess.getEnumValueAccess().getValueEEnumLiteralCrossReference_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__VariableValue__ValueAssignment
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVariableValueAccess().getValueVariableCrossReference_0()); }
(
{ before(grammarAccess.getVariableValueAccess().getValueVariableIDTerminalRuleCall_0_1()); }
	RULE_ID{ after(grammarAccess.getVariableValueAccess().getValueVariableIDTerminalRuleCall_0_1()); }
)
{ after(grammarAccess.getVariableValueAccess().getValueVariableCrossReference_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__CollectionAccess__CollectionOperationAssignment_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollectionAccessAccess().getCollectionOperationCollectionOperationEnumRuleCall_0_0()); }
	ruleCollectionOperation{ after(grammarAccess.getCollectionAccessAccess().getCollectionOperationCollectionOperationEnumRuleCall_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__CollectionAccess__ParameterAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollectionAccessAccess().getParameterExpressionParserRuleCall_2_0()); }
	ruleExpression{ after(grammarAccess.getCollectionAccessAccess().getParameterExpressionParserRuleCall_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__TargetAssignment_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getTargetEObjectCrossReference_0_0()); }
(
{ before(grammarAccess.getFeatureAccessAccess().getTargetEObjectIDTerminalRuleCall_0_0_1()); }
	RULE_ID{ after(grammarAccess.getFeatureAccessAccess().getTargetEObjectIDTerminalRuleCall_0_0_1()); }
)
{ after(grammarAccess.getFeatureAccessAccess().getTargetEObjectCrossReference_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__ValueAssignment_2_0_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getValueStructuralFeatureValueParserRuleCall_2_0_0_0()); }
	ruleStructuralFeatureValue{ after(grammarAccess.getFeatureAccessAccess().getValueStructuralFeatureValueParserRuleCall_2_0_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__CollectionAccessAssignment_2_0_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getCollectionAccessCollectionAccessParserRuleCall_2_0_1_1_0()); }
	ruleCollectionAccess{ after(grammarAccess.getFeatureAccessAccess().getCollectionAccessCollectionAccessParserRuleCall_2_0_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__ValueAssignment_2_1_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getValueOperationValueParserRuleCall_2_1_0_0()); }
	ruleOperationValue{ after(grammarAccess.getFeatureAccessAccess().getValueOperationValueParserRuleCall_2_1_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__ParametersAssignment_2_1_2_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getParametersExpressionParserRuleCall_2_1_2_0_0()); }
	ruleExpression{ after(grammarAccess.getFeatureAccessAccess().getParametersExpressionParserRuleCall_2_1_2_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__ParametersAssignment_2_1_2_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getParametersExpressionParserRuleCall_2_1_2_1_1_0()); }
	ruleExpression{ after(grammarAccess.getFeatureAccessAccess().getParametersExpressionParserRuleCall_2_1_2_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__StructuralFeatureValue__ValueAssignment
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureCrossReference_0()); }
(
{ before(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureIDTerminalRuleCall_0_1()); }
	RULE_ID{ after(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureIDTerminalRuleCall_0_1()); }
)
{ after(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureCrossReference_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__OperationValue__ValueAssignment
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getOperationValueAccess().getValueEOperationCrossReference_0()); }
(
{ before(grammarAccess.getOperationValueAccess().getValueEOperationIDTerminalRuleCall_0_1()); }
	RULE_ID{ after(grammarAccess.getOperationValueAccess().getValueEOperationIDTerminalRuleCall_0_1()); }
)
{ after(grammarAccess.getOperationValueAccess().getValueEOperationCrossReference_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


RULE_BOOL : ('false'|'true');

RULE_SIGNEDINT : '-' RULE_INT;

RULE_DOUBLE : '-'? RULE_INT? '.' RULE_INT (('e'|'E') '-'? RULE_INT)?;

RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_INT : ('0'..'9')+;

RULE_STRING : ('"' ('\\' .|~(('\\'|'"')))* '"'|'\'' ('\\' .|~(('\\'|'\'')))* '\'');

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;


