/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.collaboration.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultHighlightingConfiguration;
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfigurationAcceptor;
import org.eclipse.xtext.ui.editor.utils.TextStyle;

public class CollaborationHighlightingConfiguration extends DefaultHighlightingConfiguration  {

	public static final String GUARANTEE = "sml.guarantee";
	public static final String ASSUMPTION = "sml.assumption";
	
	public static final String STRICT = "sml.strict";
	
	public static final String COMMITTED = "sml.committed";
	public static final String URGENT = "sml.urgent";
	public static final String REQUESTED = "sml.requested";
	public static final String EVENTUALLY = "sml.eventually";

	public static final String VIOLATION = "sml.violation";
	public static final String FORBIDDEN = "sml.forbidden";

	public static final String INTERRUPT = "sml.interrupt";
	
	public static final String UNCONTROLLABLE = "sml.uncontrollable";

	public static final RGB GRAY = new RGB(112, 112, 112);
	
	public static final RGB RED = new RGB(220, 0, 0);
	public static final RGB RED2 = new RGB(184, 0, 0);
	public static final RGB RED3 = new RGB(128, 0, 0);
	
	public static final RGB BLUE = new RGB(0, 0, 220);
	public static final RGB BLUE2 = new RGB(0, 0, 184);
	public static final RGB BLUE3 = new RGB(0, 0, 128);
	
	public static final RGB GREEN = new RGB(0, 200, 0);
	public static final RGB GREEN2 = new RGB(0, 184, 0);
	public static final RGB GREEN3 = new RGB(0, 128, 0);

	public static final RGB TURQUOISE = new RGB(0, 128, 128);
	public static final RGB TURQUOISE2 = new RGB(0, 128, 143);
	public static final RGB TURQUOISE3 = new RGB(0, 128, 153);
	public static final RGB TURQUOISE4 = new RGB(71, 184, 184);
	public static final RGB TURQUOISE5 = new RGB(51, 163, 163);

	public static final RGB DARKORCHID1 = new RGB(191, 62, 255);
	public static final RGB DARKORCHID2 = new RGB(178, 58, 238);
	public static final RGB DARKORCHID3 = new RGB(154, 50, 205);
	public static final RGB DARKORCHID4 = new RGB(104, 34, 139);
	public static final RGB DARKORCHID5 = new RGB(74, 28, 109);
	public static final RGB DARKORCHID6 = new RGB(54, 22, 89);

	
	@Override
	public void configure(IHighlightingConfigurationAcceptor acceptor) {

		acceptor.acceptDefaultHighlighting(GUARANTEE, "Guarantee", guaranteeTextStyle());
		acceptor.acceptDefaultHighlighting(ASSUMPTION, "Assumption", assumptionTextStyle());
		acceptor.acceptDefaultHighlighting(STRICT, "Strict", strictTextStyle());
		acceptor.acceptDefaultHighlighting(INTERRUPT, "Interrupt", interruptTextStyle());
		
		acceptor.acceptDefaultHighlighting(COMMITTED, "Comitted", committedTextStyle());
		acceptor.acceptDefaultHighlighting(URGENT, "Urgent", urgentTextStyle());
		acceptor.acceptDefaultHighlighting(REQUESTED, "Requested", requestedTextStyle());
		acceptor.acceptDefaultHighlighting(EVENTUALLY, "Eventually", eventuallyTextStyle());
		
		acceptor.acceptDefaultHighlighting(UNCONTROLLABLE, "Uncontrollable", uncontrollableTextStyle());

		super.configure(acceptor);
	}

	public TextStyle guaranteeTextStyle() {
		TextStyle textStyle = defaultTextStyle().copy();
		textStyle.setColor(TURQUOISE3);
		textStyle.setStyle(SWT.BOLD);
		return textStyle;
	}
	
	public TextStyle assumptionTextStyle() {
		TextStyle textStyle = defaultTextStyle().copy();
		textStyle.setColor(GREEN3);
		textStyle.setStyle(SWT.BOLD);
		return textStyle;
	}

	public TextStyle strictTextStyle() {
		TextStyle textStyle = defaultTextStyle().copy();
		textStyle.setColor(RED2);
		textStyle.setStyle(SWT.ITALIC);
		return textStyle;
	}

	public TextStyle interruptTextStyle() {
		TextStyle textStyle = defaultTextStyle().copy();
		textStyle.setColor(BLUE3);
		textStyle.setStyle(SWT.ITALIC);
		return textStyle;
	}

	public TextStyle committedTextStyle() {
		TextStyle textStyle = defaultTextStyle().copy();
		textStyle.setColor(DARKORCHID6);
		textStyle.setStyle(SWT.ITALIC);
		return textStyle;
	}

	public TextStyle urgentTextStyle() {
		TextStyle textStyle = defaultTextStyle().copy();
		textStyle.setColor(DARKORCHID4);
		textStyle.setStyle(SWT.ITALIC);
		return textStyle;
	}

	public TextStyle requestedTextStyle() {
		TextStyle textStyle = defaultTextStyle().copy();
		textStyle.setColor(DARKORCHID3);
		textStyle.setStyle(SWT.ITALIC);
		return textStyle;
	}

	public TextStyle eventuallyTextStyle() {
		TextStyle textStyle = defaultTextStyle().copy();
		textStyle.setColor(DARKORCHID2);
		textStyle.setStyle(SWT.ITALIC);
		return textStyle;
	}

	public TextStyle uncontrollableTextStyle() {
		TextStyle textStyle = defaultTextStyle().copy();
		textStyle.setColor(GRAY);
		return textStyle;
	}
}
