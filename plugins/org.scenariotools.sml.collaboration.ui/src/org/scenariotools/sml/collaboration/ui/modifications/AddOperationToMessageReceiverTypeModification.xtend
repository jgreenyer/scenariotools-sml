/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.sml.collaboration.ui.modifications

import java.util.Collections
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EcoreFactory
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext
import org.eclipse.xtext.ui.editor.model.edit.ISemanticModification
import org.eclipse.xtext.util.concurrent.IUnitOfWork
import org.scenariotools.sml.Message

class AddOperationToMessageReceiverTypeModification implements ISemanticModification {

	int messageNameOffset
	int messageNameLength

	new(int messageNameOffset, int messageNameLength) {
		this.messageNameOffset = messageNameOffset
		this.messageNameLength = messageNameLength
	}

	override apply(EObject element, IModificationContext context) throws Exception {

		// Retrieve model elements
		val message = element as Message
		val role = message.receiver
		val type = role.type as EClass

		// Retrieve name of operation
		val messageName = context.xtextDocument.get(messageNameOffset, messageNameLength)

		// Create new operation
		val newOperation = EcoreFactory.eINSTANCE.createEOperation()
		newOperation.name = messageName

		// Add to model
		type.getEOperations.add(newOperation)

		// Save model
		type.eResource.save(Collections.EMPTY_MAP)

		// Refresh Editor
		context.xtextDocument.modify(
			new IUnitOfWork.Void<XtextResource>() {
				override process(XtextResource state) throws Exception {
					state.modified = true
				}

			})

	}

}
