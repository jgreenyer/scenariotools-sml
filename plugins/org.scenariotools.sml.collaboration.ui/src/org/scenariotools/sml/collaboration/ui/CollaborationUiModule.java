/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.collaboration.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtext.ide.editor.syntaxcoloring.AbstractAntlrTokenToAttributeIdMapper;
import org.eclipse.xtext.ide.editor.syntaxcoloring.DefaultSemanticHighlightingCalculator;
import org.eclipse.xtext.linking.ILinkingDiagnosticMessageProvider;
import org.eclipse.xtext.ui.editor.model.edit.ITextEditComposer;
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfiguration;
import org.scenariotools.sml.collaboration.linking.CollaborationLinkingDiagnosticMessageProvider;
import org.scenariotools.sml.collaboration.ui.quickfix.CollaborationTextEditComposer;

import com.google.inject.Binder;

/**
 * Use this class to register components to be used within the IDE.
 */
public class CollaborationUiModule extends org.scenariotools.sml.collaboration.ui.AbstractCollaborationUiModule {
	public CollaborationUiModule(AbstractUIPlugin plugin) {
		super(plugin);
	}
	
	/*
	 * This adds a custom LinkingDiagnosticMessageProvider that extends the
	 * error messages of missing references.
	 */
	public Class<? extends ILinkingDiagnosticMessageProvider> bindILinkingDiagnosticMessageProvider() {
		return CollaborationLinkingDiagnosticMessageProvider.class;
	}

	/*
	 * This adds a custom TextEditComposer that enables auto formatting after
	 * using quickfixes.
	 */
	public Class<? extends ITextEditComposer> bindITextEditComposer() {
		return CollaborationTextEditComposer.class;
	}
	
	public Class<? extends AbstractAntlrTokenToAttributeIdMapper> bindAbstractAntlrTokenToAttributeIdMapper() {
		return CollaborationAntlrTokenToAttributeIdMapper.class;
	}

	public Class<? extends IHighlightingConfiguration> bindILexicalHighlightingConfiguration() {
		return CollaborationHighlightingConfiguration.class;
	}
	
	public void configure(Binder binder) {
		super.configure(binder);
		binder.bind(DefaultSemanticHighlightingCalculator.class).to(CollaborationSemanticHighlightingCalculator.class);
	}
}
