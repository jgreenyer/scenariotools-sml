/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.sml.collaboration.ui.modifications

import java.util.Collections
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EcoreFactory
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext
import org.eclipse.xtext.ui.editor.model.edit.ISemanticModification
import org.eclipse.xtext.util.concurrent.IUnitOfWork
import org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess
import org.scenariotools.sml.expressions.scenarioExpressions.StructuralFeatureValue
import org.scenariotools.sml.expressions.scenarioExpressions.TypedVariable

class AddReferenceToExpressionVariableTypeModification implements ISemanticModification {

	int featureNameOffset
	int featureNameLength
	EClassifier eType

	new(int featureNameOffset, int featureNameLength, EClassifier eType) {
		this.featureNameOffset = featureNameOffset
		this.featureNameLength = featureNameLength
		this.eType = eType
	}

	override apply(EObject element, IModificationContext context) throws Exception {

		// Retrieve model elements
		val structuralFeatureValue = element as StructuralFeatureValue
		val fa = structuralFeatureValue.eContainer as FeatureAccess
		val variable = fa.target as TypedVariable
		val type = variable.type as EClass

		// Retrieve name of feature
		val messageName = context.xtextDocument.get(featureNameOffset, featureNameLength).toFirstLower

		// Create new reference
		val newReference = EcoreFactory.eINSTANCE.createEReference()
		newReference.name = messageName
		newReference.EType = eType

		// Add to model
		type.getEStructuralFeatures.add(newReference)

		// Save model
		type.eResource.save(Collections.EMPTY_MAP)

		// Refresh Editor
		context.xtextDocument.modify(
			new IUnitOfWork.Void<XtextResource>() {
				override process(XtextResource state) throws Exception {
					state.modified = true
				}

			})

	}

}
