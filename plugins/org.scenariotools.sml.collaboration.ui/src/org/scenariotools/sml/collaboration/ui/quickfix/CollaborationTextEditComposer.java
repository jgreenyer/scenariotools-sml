/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.collaboration.ui.quickfix;

import org.eclipse.xtext.resource.SaveOptions;
import org.eclipse.xtext.ui.editor.model.edit.DefaultTextEditComposer;

/*
 * This class enables auto formatting when using quickfixes.
 */
public class CollaborationTextEditComposer extends DefaultTextEditComposer {

	@Override
    protected SaveOptions getSaveOptions() {
		return super.getSaveOptions();
		// TODO: This does not work yet... Don't know why
		// return SaveOptions.newBuilder().format().getOptions();
    }
	
}
