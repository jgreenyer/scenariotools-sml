/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.sml.collaboration.ui.modifications

import org.eclipse.xtext.ui.editor.model.edit.IModification
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext
import org.eclipse.xtext.ui.editor.model.IXtextDocument

class RemoveKeywordModification implements IModification {

	int offset
	int length

	new(int offset, int length) {
		this.offset = offset
		this.length = length
	}

	override apply(IModificationContext context) throws Exception {
		val IXtextDocument xtextDocument = context.getXtextDocument()
		xtextDocument.replace(offset, length, "")
	}

}
