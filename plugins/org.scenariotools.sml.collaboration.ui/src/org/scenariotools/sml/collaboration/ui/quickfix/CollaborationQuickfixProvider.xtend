/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 * 
 */
package org.scenariotools.sml.collaboration.ui.quickfix

import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EcorePackage
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext
import org.eclipse.xtext.ui.editor.model.edit.ISemanticModification
import org.eclipse.xtext.ui.editor.quickfix.Fix
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor
import org.eclipse.xtext.validation.Issue
import org.scenariotools.sml.Interaction
import org.scenariotools.sml.Message
import org.scenariotools.sml.ModalMessage
import org.scenariotools.sml.Role
import org.scenariotools.sml.RoleBindingConstraint
import org.scenariotools.sml.Scenario
import org.scenariotools.sml.SmlFactory
import org.scenariotools.sml.VariableBindingParameterExpression
import org.scenariotools.sml.collaboration.ui.modifications.AddAttributeToExpressionVariableTypeModification
import org.scenariotools.sml.collaboration.ui.modifications.AddAttributeToMessageReceiverTypeModification
import org.scenariotools.sml.collaboration.ui.modifications.AddOperationToMessageReceiverTypeModification
import org.scenariotools.sml.collaboration.ui.modifications.AddParameterToMessageEOperationModification
import org.scenariotools.sml.collaboration.ui.modifications.AddReferenceToExpressionVariableTypeModification
import org.scenariotools.sml.collaboration.ui.modifications.AddReferenceToMessageReceiverTypeModification
import org.scenariotools.sml.collaboration.ui.modifications.RemoveKeywordModification
import org.scenariotools.sml.collaboration.utility.CollaborationQuickFixUtil
import org.scenariotools.sml.collaboration.validation.CollaborationIssueCodes
import org.scenariotools.sml.expressions.scenarioExpressions.VariableValue
import org.scenariotools.sml.expressions.ui.quickfix.ScenarioExpressionsQuickfixProvider
import org.scenariotools.sml.utility.CollaborationUtil
import org.scenariotools.sml.utility.ScenarioUtil
import org.scenariotools.sml.utility.SpecificationUtil

/**
 * Custom quickfixes.
 *
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#quick-fixes
 */
class CollaborationQuickfixProvider extends ScenarioExpressionsQuickfixProvider {

	@Fix(CollaborationIssueCodes::FALSE_PARALLEL_MESSAGE)
	def handleFalseParallelMessages(Issue issue, IssueResolutionAcceptor acceptor) {
		acceptor.accept(
			issue,
			'Insert sync condition (NOT YET IMPLEMENTED)',
			'Insert sync condition.',
			null,
			new ISemanticModification() {
				override apply(EObject element, IModificationContext context) throws Exception {
					// val message = element as ModalMessage
					// val interaction = message.eContainer as Interaction
					// val index = interaction.fragments.indexOf(message)
					// val prevMessage = interaction.fragments.get(index - 1)
					// val sync = SmlFactory.eINSTANCE.createCondition
					// val expression = SmlFactory.eINSTANCE.createConditionExpression
					// sync.conditionExpression = expression
					// interaction.fragments.add(index, sync)
				}
			}
		)
		acceptor.accept(
			issue,
			'Surround with parallel',
			'Surround with parallel.',
			null,
			new ISemanticModification() {
				override apply(EObject element, IModificationContext context) throws Exception {
					val message = element as ModalMessage
					val interaction = message.eContainer as Interaction
					val index = interaction.fragments.indexOf(message)
					val prevMessage = interaction.fragments.get(index - 1)
					val parallel = SmlFactory.eINSTANCE.createParallel
					val par1 = SmlFactory.eINSTANCE.createInteraction
					val par2 = SmlFactory.eINSTANCE.createInteraction
					parallel.parallelInteraction.add(par1)
					parallel.parallelInteraction.add(par2)
					interaction.fragments.add(index - 1, parallel)
					par1.fragments.add(prevMessage)
					par2.fragments.add(message)
				}
			}
		)
	}

	@Fix(CollaborationIssueCodes::MESSAGE_EVENT_OPERATION_NOT_FOUND)
	def fixMessageEventOperationNotFound(Issue issue, IssueResolutionAcceptor acceptor) {
		createLinkingIssueResolutions(issue, acceptor);
		acceptor.accept(
			issue,
			'Add operation to model',
			'Add operation to model.',
			null,
			new AddOperationToMessageReceiverTypeModification(issue.offset, issue.length)
		)
	}

	@Fix(CollaborationIssueCodes::MESSAGE_EVENT_FEATURE_NOT_FOUND)
	def fixMessageEventFeatureNotFound(Issue issue, IssueResolutionAcceptor acceptor) {
		createLinkingIssueResolutions(issue, acceptor)
		acceptor.accept(issue, 'Add EInt attribute to model', 'Add EInt attribute to model.', null,
			new AddAttributeToMessageReceiverTypeModification(issue.offset, issue.length, EcorePackage.Literals.EINT))
		acceptor.accept(issue, 'Add EString attribute to model', 'Add EString attribute to model.', null,
			new AddAttributeToMessageReceiverTypeModification(issue.offset, issue.length, EcorePackage.Literals.ESTRING))
		acceptor.accept(issue, 'Add EBoolean attribute to model', 'Add EBoolean attribute to model.', null,
			new AddAttributeToMessageReceiverTypeModification(issue.offset, issue.length, EcorePackage.Literals.EBOOLEAN))
		acceptor.accept(issue, 'Add EObject reference to model', 'Add EObject reference to model.', null,
			new AddReferenceToMessageReceiverTypeModification(issue.offset, issue.length, EcorePackage.Literals.EOBJECT))
	}
	
	@Fix(CollaborationIssueCodes::FEATURE_IN_EXPRESSION_NOT_FOUND)
	def fixFeatureInExpressionNotFound(Issue issue, IssueResolutionAcceptor acceptor) {
		createLinkingIssueResolutions(issue, acceptor)
		acceptor.accept(issue, 'Add EInt attribute to model', 'Add EInt attribute to model.', null,
			new AddAttributeToExpressionVariableTypeModification(issue.offset, issue.length, EcorePackage.Literals.EINT))
		acceptor.accept(issue, 'Add EString attribute to model', 'Add EString attribute to model.', null,
			new AddAttributeToExpressionVariableTypeModification(issue.offset, issue.length, EcorePackage.Literals.ESTRING))
		acceptor.accept(issue, 'Add EBoolean attribute to model', 'Add EBoolean attribute to model.', null,
			new AddAttributeToExpressionVariableTypeModification(issue.offset, issue.length, EcorePackage.Literals.EBOOLEAN))
		acceptor.accept(issue, 'Add EObject reference to model', 'Add EObject reference to model.', null,
			new AddReferenceToExpressionVariableTypeModification(issue.offset, issue.length, EcorePackage.Literals.EOBJECT))
	}

	@Fix(CollaborationIssueCodes::MESSAGE_ROLE_NOT_FOUND)
	def fixMessageSenderNotFound(Issue issue, IssueResolutionAcceptor acceptor) {
		createLinkingIssueResolutions(issue, acceptor);
		for (var i = 1; i < issue.data.size; i++) {
			val solution = issue.data.get(i)
			acceptor.accept(
				issue,
				'Define role from imported class ' + solution,
				'Define role from imported class ' + solution + '.',
				null,
				new ISemanticModification() {
					override apply(EObject element, IModificationContext context) throws Exception {

						val message = element as Message
						val roleName = context.xtextDocument.get(issue.offset, issue.length)

						val scenario = ScenarioUtil.getContainingScenario(message)
						val collaboration = CollaborationUtil.getContainingCollaboration(scenario)
						val specification = SpecificationUtil.getContainingSpecification(collaboration)

						// Retrieve type of solution
						val eclasses = SpecificationUtil.getAllDefinedEClasses(specification)
						var solutionType = null as EClass
						for (EClass eclass : eclasses) {
							if (eclass.name.equals(solution)) {
								solutionType = eclass
							}
						}

						// Create new role
						val newRole = SmlFactory.eINSTANCE.createRole()
						newRole.name = roleName
						newRole.static = false
						newRole.type = solutionType

						// Add role to collaboration
						collaboration.roles.add(newRole)
					}
				}
			)
		}

	}

	@Fix(CollaborationIssueCodes::SENDER_ROLE_NOT_BOUND)
	def fixMakeSenderRoleStatic(Issue issue, IssueResolutionAcceptor acceptor) {
		acceptor.accept(
			issue,
			'Make role static',
			'Make role static.',
			null,
			new ISemanticModification() {
				override apply(EObject element, IModificationContext context) throws Exception {
					val message = element as Message

					// Make role static
					message.sender.static = true
				}
			}
		)
	}

	@Fix(CollaborationIssueCodes::RECEIVER_ROLE_NOT_BOUND)
	def fixMakeReceiverRoleStatic(Issue issue, IssueResolutionAcceptor acceptor) {
		acceptor.accept(
			issue,
			'Make role static',
			'Make role static.',
			null,
			new ISemanticModification() {
				override apply(EObject element, IModificationContext context) throws Exception {
					val message = element as Message

					// Make role static
					message.receiver.static = true
				}
			}
		)
	}
	
	@Fix(CollaborationIssueCodes::EXPRESSION_ROLE_NOT_BOUND)
	def fixMakeExpressionRoleStatic(Issue issue, IssueResolutionAcceptor acceptor) {
		acceptor.accept(
			issue,
			'Make role static',
			'Make role static.',
			null,
			new ISemanticModification() {
				override apply(EObject element, IModificationContext context) throws Exception {
					val variableValue = element as VariableValue
					if (variableValue.value instanceof Role) {
						val Role role = variableValue.value as Role
	
						// Make role static
						role.static = true
					}
				}
			}
		)
	}
	
	@Fix(CollaborationIssueCodes::DYNAMIC_BINDING_FOR_STATIC_ROLE)
	def fixMakeExpressionRoleDynamic(Issue issue, IssueResolutionAcceptor acceptor) {
		acceptor.accept(
			issue,
			'Make role dynamic',
			'Make role dynamic.',
			null,
			new ISemanticModification() {
				override apply(EObject element, IModificationContext context) throws Exception {
					val variableBindingParameterExpression = element as VariableBindingParameterExpression
					val variableValue = variableBindingParameterExpression.variable as VariableValue
					if (variableValue.value instanceof Role) {
						val Role role = variableValue.value as Role
	
						// Make role static
						role.static = false
					}
				}
			}
		)
	}

	@Fix(CollaborationIssueCodes::SENDER_ROLE_NOT_BOUND)
	def fixSenderRoleNotBound(Issue issue, IssueResolutionAcceptor acceptor) {

		// For every binding solution do
		for (var i = 0; i < issue.data.size; i += 2) {
			val targetRoleName = issue.data.get(i)
			val featureName = issue.data.get(i + 1)
			acceptor.accept(
				issue,
				'Bind this role to ' + targetRoleName + '.' + featureName,
				'Bind this role to ' + targetRoleName + '.' + featureName + '.',
				null,
				new ISemanticModification() {
					override apply(EObject element, IModificationContext context) throws Exception {
						val message = element as Message
						val scenario = ScenarioUtil.getContainingScenario(message) as Scenario
						val collaboration = CollaborationUtil.getContainingCollaboration(scenario)
						val role = message.sender as Role
						val targetRole = CollaborationQuickFixUtil.retrieveRoleByName(collaboration, targetRoleName)
						val feature = CollaborationQuickFixUtil.retrieveEReferenceByName(targetRole, featureName)

						// Add role binding
						CollaborationQuickFixUtil.addRoleBinding(role, scenario, targetRole, feature)
					}
				}
			)
		}
	}

	@Fix(CollaborationIssueCodes::RECEIVER_ROLE_NOT_BOUND)
	def fixReceiverRoleNotBound(Issue issue, IssueResolutionAcceptor acceptor) {

		// For every binding solution do
		for (var i = 0; i < issue.data.size; i += 2) {
			val targetRoleName = issue.data.get(i)
			val featureName = issue.data.get(i + 1)
			acceptor.accept(
				issue,
				'Bind this role to ' + targetRoleName + '.' + featureName,
				'Bind this role to ' + targetRoleName + '.' + featureName + '.',
				null,
				new ISemanticModification() {
					override apply(EObject element, IModificationContext context) throws Exception {
						val message = element as Message
						val scenario = ScenarioUtil.getContainingScenario(message) as Scenario
						val collaboration = CollaborationUtil.getContainingCollaboration(scenario)
						val role = message.receiver as Role
						val targetRole = CollaborationQuickFixUtil.retrieveRoleByName(collaboration, targetRoleName)
						val feature = CollaborationQuickFixUtil.retrieveEReferenceByName(targetRole, featureName)

						// Add role binding
						CollaborationQuickFixUtil.addRoleBinding(role, scenario, targetRole, feature)
					}
				}
			)
		}
	}

	@Fix(CollaborationIssueCodes::ROLE_BINDING_ROLE_IS_STATIC)
	def fixMakeRoleOfRoleBindingConstraintDynamic(Issue issue, IssueResolutionAcceptor acceptor) {
		acceptor.accept(
			issue,
			'Make role dynamic',
			'Make role dynamic.',
			null,
			new ISemanticModification() {
				override apply(EObject element, IModificationContext context) throws Exception {
					val roleBindingConstraint = element as RoleBindingConstraint

					// Make role dynamic
					roleBindingConstraint.role.static = false
				}
			}
		)
	}
	
	@Fix(CollaborationIssueCodes::STRICT_MESSAGE_IN_EXISTENTIAL_SCENARIO)
	def fixRemoveStrictKeywordInExistentialScenario(Issue issue, IssueResolutionAcceptor acceptor) {
		acceptor.accept(
			issue,
			'Remove strict keyword',
			'Remove strict keyword.',
			null,
			new RemoveKeywordModification(issue.offset,issue.length)
		)
	}
	
	@Fix(CollaborationIssueCodes::REQUESTED_MESSAGE_IN_EXISTENTIAL_SCENARIO)
	def fixRemoveRequestedKeywordInExistentialScenario(Issue issue, IssueResolutionAcceptor acceptor) {
		acceptor.accept(
			issue,
			'Remove requested keyword',
			'Remove requested keyword.',
			null,
			new RemoveKeywordModification(issue.offset,issue.length)
		)
	}
	
	@Fix(CollaborationIssueCodes::TOO_MANY_ARGUMENTS)
	def fixMessageEventHasTooManyArguments(Issue issue, IssueResolutionAcceptor acceptor) {
		createLinkingIssueResolutions(issue, acceptor)
		
		val parameterType = issue.data.get(0)
		if (parameterType == "") {
			acceptor.accept(issue, 'Add EInt parameter to model', 'Add EInt parameter to model.', null,
				new AddParameterToMessageEOperationModification(issue.offset, issue.length, EcorePackage.Literals.EINT))
			acceptor.accept(issue, 'Add EString parameter to model', 'Add EString parameter to model.', null,
				new AddParameterToMessageEOperationModification(issue.offset, issue.length, EcorePackage.Literals.ESTRING))
			acceptor.accept(issue, 'Add EBoolean parameter to model', 'Add EBoolean parameter to model.', null,
				new AddParameterToMessageEOperationModification(issue.offset, issue.length, EcorePackage.Literals.EBOOLEAN))
			acceptor.accept(issue, 'Add EObject parameter to model', 'Add EObject parameter to model.', null,
				new AddParameterToMessageEOperationModification(issue.offset, issue.length, EcorePackage.Literals.EOBJECT))
		} else {
			acceptor.accept(issue, 'Add ' + parameterType + ' parameter to model', 'Add ' + parameterType + ' parameter to model.', null,
				new AddParameterToMessageEOperationModification(issue.offset, issue.length, null))
		}
	}

}
