/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.collaboration.ui;

import org.eclipse.xtext.ide.editor.syntaxcoloring.DefaultAntlrTokenToAttributeIdMapper;

public class CollaborationAntlrTokenToAttributeIdMapper extends DefaultAntlrTokenToAttributeIdMapper {
	
	@Override
	protected String calculateId(String tokenName, int tokenType) {
        if(tokenName.equals("'guarantee'")) {
            return CollaborationHighlightingConfiguration.GUARANTEE;
        }
        if(tokenName.equals("'assumption'")) {
            return CollaborationHighlightingConfiguration.ASSUMPTION;
        }
        if(tokenName.equals("'interrupt'")) {
            return CollaborationHighlightingConfiguration.INTERRUPT;
        }
        if(tokenName.equals("'strict'")
        		|| tokenName.equals("'forbidden'")
        		|| tokenName.equals("'violation'")
        		) {
            return CollaborationHighlightingConfiguration.STRICT;
        }

        if(tokenName.equals("'committed'")) {
            return CollaborationHighlightingConfiguration.COMMITTED;
        }
        if(tokenName.equals("'urgent'")) {
            return CollaborationHighlightingConfiguration.URGENT;
        }
        if(tokenName.equals("'requested'")) {
            return CollaborationHighlightingConfiguration.REQUESTED;
        }
        if(tokenName.equals("'eventually'")) {
            return CollaborationHighlightingConfiguration.EVENTUALLY;
        }

		// TODO Auto-generated method stub
		return super.calculateId(tokenName, tokenType);
	}

}
