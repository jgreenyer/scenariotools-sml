/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.collaboration.ui;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.ide.editor.syntaxcoloring.DefaultSemanticHighlightingCalculator;
import org.eclipse.xtext.ide.editor.syntaxcoloring.IHighlightedPositionAcceptor;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.util.CancelIndicator;
import org.scenariotools.sml.Message;
import org.scenariotools.sml.ModalMessage;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.SmlPackage;
import org.scenariotools.sml.Specification;

public class CollaborationSemanticHighlightingCalculator extends DefaultSemanticHighlightingCalculator {
	@Override
	protected boolean highlightElement(EObject object, IHighlightedPositionAcceptor acceptor, CancelIndicator cancelIndicator) {
		String style = null;		

		if(object instanceof Role) {
			Role role = (Role)object;
			
			Specification specification = getSpecification(object);
			if(specification != null) {
				if(!specification.getControllableEClasses().contains(role.getType())) {
					boolean afterRole = false;
					for(INode node : NodeModelUtils.findActualNodeFor(object).getChildren()) {
						if(afterRole) {
							acceptor.addPosition(node.getOffset(), node.getLength(), CollaborationHighlightingConfiguration.UNCONTROLLABLE);
						}						
						if(node.getText().equals("role")) {
							afterRole = true;
						}
					}
				}
			}
		} else if(object instanceof Scenario) {
			Scenario scenario = (Scenario)object;
			switch(scenario.getKind()) {
			case GUARANTEE:
				style = CollaborationHighlightingConfiguration.GUARANTEE;
				break;
			case ASSUMPTION:
				style = CollaborationHighlightingConfiguration.ASSUMPTION;
				break;
			default:
				break;
			}
			
			if(style != null) {				
				for(INode node : NodeModelUtils.findActualNodeFor(object).getChildren()) {
					if(node.getText().equals("scenario")) {
						acceptor.addPosition(node.getOffset(), node.getLength(), style);
						break;
					}
				}
				
				if(scenario.isSingular()) {
					for(INode node : NodeModelUtils.findNodesForFeature(object, SmlPackage.Literals.SCENARIO__SINGULAR))
						acceptor.addPosition(node.getOffset(), node.getLength(), style);
				}
			}
		} else if(object instanceof Message) {
			Message message = (Message)object;
			
			Specification specification = getSpecification(object);
			if(specification != null) {
				if(message.getSender() != null && !specification.getControllableEClasses().contains(message.getSender().getType())) {
					for(INode node : NodeModelUtils.findNodesForFeature(object, SmlPackage.Literals.MESSAGE__SENDER))
						acceptor.addPosition(node.getOffset(), node.getLength(), CollaborationHighlightingConfiguration.UNCONTROLLABLE);			
				}
				if(message.getReceiver() != null && !specification.getControllableEClasses().contains(message.getReceiver().getType())) {
					for(INode node : NodeModelUtils.findNodesForFeature(object, SmlPackage.Literals.MESSAGE__RECEIVER))
						acceptor.addPosition(node.getOffset(), node.getLength(), CollaborationHighlightingConfiguration.UNCONTROLLABLE);			
				}					
			}			
			
			if(message instanceof ModalMessage) {
				ModalMessage modMessage = (ModalMessage)message;
				if(modMessage.isMonitored()) {
					switch(modMessage.getExpectationKind()) {
					case COMMITTED:
						style = CollaborationHighlightingConfiguration.COMMITTED;
						break;						
					case URGENT:
						style = CollaborationHighlightingConfiguration.URGENT;
						break;						
					case REQUESTED:
						style = CollaborationHighlightingConfiguration.REQUESTED;
						break;
					case EVENTUALLY:
						style = CollaborationHighlightingConfiguration.EVENTUALLY;
						break;						
					default:
						break;						
					}

					if(style != null) {
						for(INode node : NodeModelUtils.findNodesForFeature(object, SmlPackage.Literals.MODAL_MESSAGE__MONITORED))
							acceptor.addPosition(node.getOffset(), node.getLength(), style);			
					}
				}
			}
		}
		
		return super.highlightElement(object, acceptor, cancelIndicator);
	}
	
	private static Specification getSpecification(EObject object) {
		while(object != null && !(object instanceof Specification))
			object = object.eContainer();
		
		if(object != null)
			return (Specification)object;
		
		return null;
	}
}
