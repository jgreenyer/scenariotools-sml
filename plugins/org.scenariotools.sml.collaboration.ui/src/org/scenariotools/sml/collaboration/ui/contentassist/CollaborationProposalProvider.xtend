/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.collaboration.ui.contentassist

import java.util.List
import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EEnum
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EOperation
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.jface.text.contentassist.ICompletionProposal
import org.eclipse.xtext.Assignment
import org.eclipse.xtext.Keyword
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor
import org.scenariotools.sml.Collaboration
import org.scenariotools.sml.ConstraintBlock
import org.scenariotools.sml.Message
import org.scenariotools.sml.Role
import org.scenariotools.sml.ScenarioKind
import org.scenariotools.sml.SmlPackage
import org.scenariotools.sml.Specification
import org.scenariotools.sml.collaboration.utility.CollaborationProposalUtil
import org.scenariotools.sml.expressions.scenarioExpressions.EnumValue
import org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess
import org.scenariotools.sml.expressions.scenarioExpressions.Import
import org.scenariotools.sml.expressions.scenarioExpressions.StructuralFeatureValue
import org.scenariotools.sml.expressions.scenarioExpressions.VariableDeclaration
import org.scenariotools.sml.expressions.utility.EClassUtil
import org.scenariotools.sml.expressions.utility.ValueUtil
import org.scenariotools.sml.utility.CollaborationUtil
import org.scenariotools.sml.utility.InteractionUtil
import org.scenariotools.sml.utility.ScenarioUtil
import org.scenariotools.sml.utility.SpecificationUtil

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
class CollaborationProposalProvider extends AbstractCollaborationProposalProvider {

	override def ICompletionProposal createCompletionProposal(EObject element,
		ContentAssistContext contentAssistContext) {
		if (element instanceof Role) {
			return createCompletionProposal(element.name, element.name + " - Role of type " + element.type.name,
				element.image, contentAssistContext)
		} else {
			return super.createCompletionProposal(element, contentAssistContext)
		}
	}

	override completeModalMessage_Sender(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		// Add roles to proposals.
		CollaborationUtil.getRelevantRolesFor(model).forEach [ r |
			acceptor.accept(createCompletionProposal(r, context))
		]
		// Propose existing messages.
		val messages = retrieveAllMessagesForProposal(model)
		messages.remove(model)
		val proposedMessages = new BasicEList<Message>()
		messages.forEach [ m |
			// Filter out already proposed messages
			if (proposedMessages.filter [ m2 |
				messagesAreParameterUnifiable(m, m2)
			].empty) {
				proposedMessages.add(m)
				val scenario = ScenarioUtil.getContainingScenario(m)
				val messageText = getMessageSimpleText(m)
				var displayText = "message "
				displayText += messageText + " - From Scenario " + scenario.name
				acceptor.accept(createCompletionProposal(messageText, displayText, m.image, context))
			}
		]
	}

	def boolean messagesAreParameterUnifiable(Message message, Message message2) {
		val unifiable = message2.sender.name.equals(message.sender.name) &&
			message2.receiver.name.equals(message.receiver.name) &&
			message2.modelElement.name.equals(message.modelElement.name)
		if (!unifiable) {
			return false
		}
		if (message.parameters.size != message2.parameters.size) {
			return false
		}
		for (var i = 0; i < message.parameters.size; i++) {
			val par1 = message.parameters.get(i)
			val par2 = message2.parameters.get(i)
			if (!EcoreUtil.equals(par1, par2))
				return false
		}
		return true
	}

	override completeModalMessage_Receiver(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		CollaborationUtil.getRelevantRolesFor(model).forEach [ r |
			acceptor.accept(createCompletionProposal(r, context))
		]
	}

	override completeConstraintMessage_Sender(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		completeModalMessage_Sender(model, assignment, context, acceptor)
	}

	override completeConstraintMessage_Receiver(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		completeModalMessage_Receiver(model, assignment, context, acceptor)
	}

	override completeModalMessage_ModelElement(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		val receiver = (model as Message).receiver
		val eclass = receiver.type as EClass
		val elements = EClassUtil.retrieveAllModelElementsOf(eclass)
		elements.forEach [ e |
			if (e instanceof EOperation) {
				val insertedText = e.name
				val shownText = CollaborationProposalUtil.eOperationToString(e)
				acceptor.accept(createCompletionProposal(insertedText, shownText, e.image, context))
			} else if (e instanceof EStructuralFeature) {
				var insertedText = ""
				if (e.upperBound == 1)
					insertedText = 'set' + e.name.toFirstUpper + "()"
				else
					insertedText = e.name
				val shownText = CollaborationProposalUtil.eStructuralFeatureToString(e)
				acceptor.accept(createCompletionProposal(insertedText, shownText, e.image, context))
			}
		]
	}

	override completeConstraintMessage_ModelElement(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		completeModalMessage_ModelElement(model, assignment, context, acceptor)
	}

	override completeRoleBindingConstraint_Role(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		CollaborationUtil.getRelevantRolesFor(model).filter[r|!r.isStatic].forEach [ r |
			acceptor.accept(createCompletionProposal(r, context))
		]
	}

	override completeVariableValue_Value(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		CollaborationUtil.getRelevantRolesFor(model).forEach [ r |
			acceptor.accept(createCompletionProposal(r.name, r.name + " : " + r.type.name, r.type.image, context))
		]
		InteractionUtil.getRelevantVariablesFor(model).forEach [ v |
			acceptor.accept(createCompletionProposal(v, context))
		]
	}

	override completeStructuralFeatureValue_Value(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		val fa = model as FeatureAccess
		val target = fa.target
		if (target instanceof Role) {
			EClassUtil.retrieveEStructuralFeaturesOf(target.type as EClass).forEach [ f |
				acceptor.accept(createCompletionProposal(f, context))
			]
		} else if (target instanceof VariableDeclaration) {
			val value = (fa.value) as StructuralFeatureValue
			ValueUtil.getValidFeaturesForValue(value).forEach [ f |
				acceptor.accept(createCompletionProposal(f, context))
			]
		} else if (target instanceof EClass) {
			EClassUtil.retrieveEStructuralFeaturesOf(target).forEach [ f |
				acceptor.accept(createCompletionProposal(f, context))
			]
		}
	}

	override completeFeatureAccess_Target(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		CollaborationUtil.getRelevantRolesFor(model).forEach [ r |
			acceptor.accept(createCompletionProposal(r.name, r.name + " : " + r.type.name, r.type.image, context))
		]
		InteractionUtil.getRelevantVariablesFor(model).forEach [ v |
			acceptor.accept(createCompletionProposal(v, context))
		]
		val scenario = ScenarioUtil.getContainingScenario(model)
		scenario.contexts.forEach [ p |
			acceptor.accept(createCompletionProposal(p, context))
		]
	}

	override completeVariableAssignment_Variable(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		InteractionUtil.getRelevantVariablesFor(model).forEach [ v |
			acceptor.accept(createCompletionProposal(v, context))
		]
	}

	override completeCollaboration_Domains(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		scopeProvider.getScope(model, SmlPackage.Literals.COLLABORATION__DOMAINS).allElements.forEach [ d |
			val domain = d.EObjectOrProxy as EPackage
			acceptor.accept(createCompletionProposal(domain, context))
		]
	}

	override completeEnumValue_Type(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		if (model instanceof Message) {
			val message = model as Message
			val element = message.modelElement
			val parameterBindingCount = message.parameters.size

			var EClassifier type = null

			if (element instanceof EOperation) {
				if (parameterBindingCount >= element.EParameters.size) {
					return;
				} else {
					val nextParameter = element.EParameters.get(parameterBindingCount + 1)
					type = nextParameter.EType
				}
			} else if (element instanceof EAttribute) {
				if (parameterBindingCount >= 1) {
					return;
				} else {
					type = element.EType
				}
			}
			if (type instanceof EEnum) {
				type.ELiterals.forEach [ l |
					acceptor.accept(createCompletionProposal(l, context))
				]
			}
		}
	}

	override completeConstraintBlock_Consider(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		super.completeConstraintBlock_Consider(model, assignment, context, acceptor)
		val messages = retrieveAllMessagesForProposal(model)
		messages.remove(model)
		messages.forEach [ m |
			val scenario = ScenarioUtil.getContainingScenario(m)
			val messageText = "message " + getMessageSimpleText(m)
			var displayText = "consider "
			displayText += messageText + " - From Scenario " + scenario.name
			acceptor.accept(createCompletionProposal(messageText, displayText, m.image, context))
		]
	}

	override completeConstraintBlock_Forbidden(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		super.completeConstraintBlock_Consider(model, assignment, context, acceptor)
		val messages = retrieveAllMessagesForProposal(model)
		messages.remove(model)
		messages.forEach [ m |
			val scenario = ScenarioUtil.getContainingScenario(m)
			val messageText = "message " + getMessageSimpleText(m)
			var displayText = "forbidden "
			displayText += messageText + " - From Scenario " + scenario.name
			acceptor.accept(createCompletionProposal(messageText, displayText, m.image, context))
		]
	}

	override completeConstraintBlock_Ignore(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		super.completeConstraintBlock_Consider(model, assignment, context, acceptor)
		val messages = retrieveAllMessagesForProposal(model)
		messages.remove(model)
		messages.forEach [ m |
			val scenario = ScenarioUtil.getContainingScenario(m)
			val messageText = "message " + getMessageSimpleText(m)
			var displayText = "ignore "
			displayText += messageText + " - From Scenario " + scenario.name
			acceptor.accept(createCompletionProposal(messageText, displayText, m.image, context))
		]
	}

	override completeConstraintBlock_Interrupt(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		super.completeConstraintBlock_Consider(model, assignment, context, acceptor)
		val messages = retrieveAllMessagesForProposal(model)
		messages.remove(model)
		messages.forEach [ m |
			val scenario = ScenarioUtil.getContainingScenario(m)
			val messageText = "message " + getMessageSimpleText(m)
			var displayText = "interrupt "
			displayText += messageText + " - From Scenario " + scenario.name
			acceptor.accept(createCompletionProposal(messageText, displayText, m.image, context))
		]
	}

	public static def String getMessageText(Message message) {
		val node = NodeModelUtils.getNode(message)
		return node.text.trim
	}

	public static def String getMessageSimpleText(Message message) {
		var messageText = getMessageText(message)
		messageText = messageText.replace("message ", "")
		messageText = messageText.replace("strict ", "")
		messageText = messageText.replace("monitored ", "")
		messageText = messageText.replace("committed ", "")
		messageText = messageText.replace("urgent ", "")
		messageText = messageText.replace("requested ", "")
		messageText = messageText.replace("eventually ", "")
		messageText = messageText.replace("consider ", "")
		messageText = messageText.replace("forbidden ", "")
		messageText = messageText.replace("ignore ", "")
		messageText = messageText.replace("interrupt ", "")
		return messageText
	}

	private def List<Message> retrieveAllMessagesForProposal(EObject model) {
		val collaboration = CollaborationUtil.getContainingCollaboration(model)
		if (CollaborationUtil.isContainedInSpecification(collaboration)) {
			val specification = SpecificationUtil.getContainingSpecification(collaboration)
			return retrieveAllMessagesInSpecification(specification)
		} else {
			return retrieveAllMessagesInCollaboration(collaboration)
		}
	}

	private def List<Message> retrieveAllMessagesInSpecification(Specification specification) {
		return specification.eAllContents.filter(typeof(Message)).toList
	}

	private def List<Message> retrieveAllMessagesInCollaboration(Collaboration collaboration) {
		return collaboration.eAllContents.filter(typeof(Message)).toList
	}

	override completeEnumValue_Value(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		val enumValue = model as EnumValue
		enumValue.type.ELiterals.forEach [ l |
			acceptor.accept(createCompletionProposal(l, context))
		]
	}

	override completeModalMessage_Monitored(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		acceptor.accept(createCompletionProposal("monitored ", "monitored", assignment.image, context))
		acceptor.accept(
			createCompletionProposal("monitored committed ", "monitored committed", assignment.image, context))
		acceptor.accept(createCompletionProposal("monitored urgent ", "monitored urgent", assignment.image, context))
		acceptor.accept(
			createCompletionProposal("monitored requested ", "monitored requested", assignment.image, context))
		acceptor.accept(
			createCompletionProposal("monitored eventually ", "monitored eventually", assignment.image, context))
	}

	override completeModalMessage_Strict(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		acceptor.accept(createCompletionProposal("strict ", "strict", assignment.image, context))
		acceptor.accept(createCompletionProposal("strict monitored ", "strict monitored", assignment.image, context))
		acceptor.accept(createCompletionProposal("strict committed ", "strict committed", assignment.image, context))
		acceptor.accept(createCompletionProposal("strict urgent ", "strict urgent", assignment.image, context))
		acceptor.accept(createCompletionProposal("strict requested ", "strict requested", assignment.image, context))
		acceptor.accept(createCompletionProposal("strict eventually ", "strict eventually", assignment.image, context))
		acceptor.accept(
			createCompletionProposal("strict monitored committed ", "strict monitored committed", assignment.image,
				context))
		acceptor.accept(
			createCompletionProposal("strict monitored urgent ", "strict monitored urgent", assignment.image, context))
		acceptor.accept(
			createCompletionProposal("strict monitored requested ", "strict monitored requested", assignment.image,
				context))
		acceptor.accept(
			createCompletionProposal("strict monitored eventually ", "strict monitored eventually", assignment.image,
				context))
	}

	override completeModalMessage_ExpectationKind(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		acceptor.accept(createCompletionProposal("committed ", "committed", assignment.image, context))
		acceptor.accept(createCompletionProposal("urgent ", "urgent", assignment.image, context))
		acceptor.accept(createCompletionProposal("requested ", "requested", assignment.image, context))
		acceptor.accept(createCompletionProposal("eventually ", "eventually", assignment.image, context))
	}

	override completeKeyword(Keyword keyword, ContentAssistContext contentAssistContext,
		ICompletionProposalAcceptor acceptor) {
		if (keyword.value == "consider")
			acceptor.accept(
				createCompletionProposal("consider message", "consider message", keyword.image, contentAssistContext))
		else if (keyword.value == "ignore")
			acceptor.accept(
				createCompletionProposal("ignore message", "ignore message", keyword.image, contentAssistContext))
		else if (keyword.value == "forbidden")
			acceptor.accept(
				createCompletionProposal("forbidden message", "forbidden message", keyword.image, contentAssistContext))
		else if (keyword.value == "interrupt") {
			if (contentAssistContext.currentModel instanceof ConstraintBlock)
				acceptor.accept(
					createCompletionProposal("interrupt message", "interrupt message", keyword.image,
						contentAssistContext))
			else
				acceptor.accept(
					createCompletionProposal("interrupt if [", "interrupt if [ Expression ]", keyword.image,
						contentAssistContext))
		} else if (keyword.value == "violation")
			acceptor.accept(
				createCompletionProposal("violation if [", "violation if  [ Expression ]", keyword.image,
					contentAssistContext))
		else if (keyword.value == "wait")
			acceptor.accept(
				createCompletionProposal("wait until [", "wait until  [ Expression ]", keyword.image,
					contentAssistContext))
		else if (keyword.value == "timed") {
			acceptor.accept(
				createCompletionProposal("timed wait until [", "timed wait until  [ Expression ]", keyword.image,
					contentAssistContext))
			acceptor.accept(
				createCompletionProposal("timed violation if [", "timed violation if  [ Expression ]", keyword.image,
					contentAssistContext))
			acceptor.accept(
				createCompletionProposal("timed interrupt if [", "timed interrupt if  [ Expression ]", keyword.image,
					contentAssistContext))
		} else if (keyword.value == "while")
			acceptor.accept(
				createCompletionProposal("while [", "while [ Expression ]", keyword.image, contentAssistContext))
		else if (keyword.value == "with dynamic bindings")
			acceptor.accept(
				createCompletionProposal("with dynamic bindings [", "with dynamic bindings [ Binding-Expressions ]",
					keyword.image, contentAssistContext))
		else if (keyword.value == "strict") {
			val model = contentAssistContext.currentModel
			val scenario = ScenarioUtil.getContainingScenario(model)
			if (scenario != null && scenario.kind == ScenarioKind.EXISTENTIAL) {
				return
			}
			super.completeKeyword(keyword, contentAssistContext, acceptor)
		} else if (keyword.value == "committed" || keyword.value == "urgent" || keyword.value == "requested" ||
			keyword.value == "eventually") {
			val model = contentAssistContext.currentModel
			val scenario = ScenarioUtil.getContainingScenario(model)
			if (scenario != null && scenario.kind == ScenarioKind.EXISTENTIAL) {
				return
			}
			super.completeKeyword(keyword, contentAssistContext, acceptor)
		} else if (keyword.value.startsWith("http://")) {
			if (contentAssistContext.currentModel instanceof Import)
				return
		} else if (keyword.value.startsWith("message")) {
			acceptor.accept(createCompletionProposal("message ", "message", keyword.image, contentAssistContext))
		} else
			super.completeKeyword(keyword, contentAssistContext, acceptor)
	}

}
