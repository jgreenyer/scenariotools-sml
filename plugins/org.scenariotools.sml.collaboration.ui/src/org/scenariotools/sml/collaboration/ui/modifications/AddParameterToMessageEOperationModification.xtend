/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.sml.collaboration.ui.modifications

import java.util.Collections
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EOperation
import org.eclipse.emf.ecore.EcoreFactory
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext
import org.eclipse.xtext.ui.editor.model.edit.ISemanticModification
import org.eclipse.xtext.util.concurrent.IUnitOfWork
import org.scenariotools.sml.Message
import org.scenariotools.sml.ParameterBinding
import org.scenariotools.sml.ValueParameterExpression
import org.scenariotools.sml.expressions.utility.ExpressionUtil
import org.scenariotools.sml.VariableBindingParameterExpression

class AddParameterToMessageEOperationModification implements ISemanticModification {

	int featureNameOffset
	int featureNameLength
	EClassifier eType

	new(int featureNameOffset, int featureNameLength, EClassifier eType) {
		this.featureNameOffset = featureNameOffset
		this.featureNameLength = featureNameLength
		this.eType = eType
	}

	override apply(EObject element, IModificationContext context) throws Exception {

		// Retrieve model elements
		val message = element as Message
		val event = message.modelElement as EOperation
		
		if (eType == null) {
			val parameterBinding = message.parameters.get(event.EParameters.size) as ParameterBinding
			val bindingExpression = parameterBinding.bindingExpression
			if (bindingExpression instanceof ValueParameterExpression) {
				val expression = bindingExpression.value
				eType = ExpressionUtil.getExpressionType(expression)
			} else if (bindingExpression instanceof VariableBindingParameterExpression) {
				val expression = bindingExpression.variable
				eType = ExpressionUtil.getExpressionType(expression)
			}
		}
		
		// Retrieve name of feature
		var number = 1
		var parameterName = eType.name.toFirstLower + "Parameter"
		while (parameterNameExists(event, parameterName + number)) {
			number += 1
		}
		parameterName = parameterName + number

		// Create new attribute
		val newParameter = EcoreFactory.eINSTANCE.createEParameter()
		newParameter.name = parameterName
		newParameter.EType = eType

		// Add to model
		event.EParameters.add(newParameter)

		// Save model
		event.eResource.save(Collections.EMPTY_MAP)

		// Refresh Editor
		context.xtextDocument.modify(new IUnitOfWork.Void<XtextResource>() {
			override process(XtextResource state) throws Exception {
				state.modified = true
			}

		})

	}

	def boolean parameterNameExists(EOperation event, String string) {
		return event.EParameters.exists[p|p.name == string]
	}

}
