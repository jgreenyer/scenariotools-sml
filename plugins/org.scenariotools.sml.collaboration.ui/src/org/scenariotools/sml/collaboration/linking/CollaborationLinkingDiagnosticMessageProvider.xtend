/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.sml.collaboration.linking

import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.ecore.EClass
import org.eclipse.xtext.diagnostics.DiagnosticMessage
import org.eclipse.xtext.diagnostics.Severity
import org.eclipse.xtext.linking.impl.LinkingDiagnosticMessageProvider
import org.scenariotools.sml.Message
import org.scenariotools.sml.SmlPackage
import org.scenariotools.sml.collaboration.validation.CollaborationIssueCodes
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage
import org.scenariotools.sml.utility.CollaborationUtil
import org.scenariotools.sml.utility.SpecificationUtil

class CollaborationLinkingDiagnosticMessageProvider extends LinkingDiagnosticMessageProvider {

	override DiagnosticMessage getUnresolvedProxyMessage(ILinkingDiagnosticContext context) {

		val superMessage = super.getUnresolvedProxyMessage(context);
		var newMessage = superMessage

		// The operation of an event cannot be found.
		if (SmlPackage.Literals.MESSAGE__MODEL_ELEMENT.equals(context.getReference())) {
			if (context.linkText.startsWith('set')) {
				newMessage = new DiagnosticMessage(superMessage.message, Severity.ERROR,
					CollaborationIssueCodes.MESSAGE_EVENT_FEATURE_NOT_FOUND, context.linkText)
			} else {
				newMessage = new DiagnosticMessage(superMessage.message, Severity.ERROR,
					CollaborationIssueCodes.MESSAGE_EVENT_OPERATION_NOT_FOUND, context.linkText)
			}
		} // The sender of an event cannot be found.
		else if (SmlPackage.Literals.MESSAGE__SENDER.equals(context.getReference())) {
			val solutions = findLinkingSolutions(context)
			newMessage = new DiagnosticMessage(superMessage.message, Severity.ERROR,
				CollaborationIssueCodes.MESSAGE_ROLE_NOT_FOUND, solutions)
		} // The Receiver of an event cannot be found.
		else if (SmlPackage.Literals.MESSAGE__RECEIVER.equals(context.getReference())) {
			val solutions = findLinkingSolutions(context)
			newMessage = new DiagnosticMessage(superMessage.message, Severity.ERROR,
				CollaborationIssueCodes.MESSAGE_ROLE_NOT_FOUND, solutions)
		} // The feature in an expression cannot be found.
		else if (ScenarioExpressionsPackage.Literals.STRUCTURAL_FEATURE_VALUE__VALUE.equals(context.getReference())) {
			newMessage = new DiagnosticMessage(superMessage.message, Severity.ERROR,
				CollaborationIssueCodes.FEATURE_IN_EXPRESSION_NOT_FOUND, context.linkText)
		}
		return newMessage

	}

	def findLinkingSolutions(ILinkingDiagnosticContext context) {
		val message = context.context as Message
		val collaboration = CollaborationUtil.getContainingCollaboration(message)
		val eclasses = new BasicEList<EClass>()
		if (CollaborationUtil.isContainedInSpecification(collaboration)) {
			val specification = SpecificationUtil.getContainingSpecification(message)
			eclasses.addAll(SpecificationUtil.getAllDefinedEClasses(specification))
		} else {
			eclasses.addAll(CollaborationUtil.getEClassesFromAllDomains(collaboration))
		}
		val solutions = new BasicEList<String>()
		solutions.add(context.linkText)
		for (EClass eclass : eclasses) {
			if (eclass != null)
				if (eclass.name != null)
					solutions.add(eclass.name)
		}
		return solutions
	}

}
