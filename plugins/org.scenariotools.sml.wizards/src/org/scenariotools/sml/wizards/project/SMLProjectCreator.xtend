/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.sml.wizards.project

import java.io.ByteArrayInputStream
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IProject
import org.eclipse.core.resources.IProjectDescription
import org.eclipse.core.resources.IWorkspace
import org.eclipse.core.resources.IWorkspaceRoot
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.CoreException
import org.eclipse.emf.common.util.BasicEList
import org.eclipse.ui.IEditorDescriptor
import org.eclipse.ui.IWorkbenchPage
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.part.FileEditorInput

class SMLProjectCreator {

	private static final String FOLDER_MODEL = "model"
	private static final String FOLDER_SPEC = "spec"

	def static void createProject(SMLProjectInfo projectInfo) {

		val ecoreModelFileName = projectInfo.ecoreModelFileName
		val specificationFileName = projectInfo.specificationFileName
		val xmiFileName = projectInfo.XMIFileName
		val runconfigFileName = projectInfo.runconfigFileName

		val IProject project = initializeNewProject(projectInfo)

		createDirectories(project)

		val IFile ecore = project.getFolder(FOLDER_MODEL).getFile(ecoreModelFileName)
		val IFile spec = project.getFolder(FOLDER_SPEC).getFile(specificationFileName)
		val IFile xmi = project.getFolder(FOLDER_SPEC).getFile(xmiFileName)
		val IFile runconfig = project.getFolder(FOLDER_SPEC).getFile(runconfigFileName)

		ecore.create(new ByteArrayInputStream(generateEcoreFile(projectInfo).toString.getBytes()), false, null)
		spec.create(new ByteArrayInputStream(generateSmlFile(projectInfo).toString.getBytes()), false, null)
		xmi.create(new ByteArrayInputStream(generateXmiFile(projectInfo).toString.getBytes()), false, null)
		runconfig.create(new ByteArrayInputStream(generateRunconfigFile(projectInfo).toString.getBytes()), false, null)

		openFileInEditor(ecore)
		openFileInEditor(spec)
		openFileInEditor(xmi)
		openFileInEditor(runconfig)
	}

	def static openFileInEditor(IFile file) {
		val IWorkbenchPage page = PlatformUI.getWorkbench().activeWorkbenchWindow.activePage
		val IEditorDescriptor desc = PlatformUI.getWorkbench().getEditorRegistry().getDefaultEditor(file.getName());
		page.openEditor(new FileEditorInput(file), desc.getId());
	}

	def static createDirectories(IProject project) {
		project.getFolder(FOLDER_MODEL).create(true, true, null)
		project.getFolder(FOLDER_SPEC).create(true, true, null)
	}

	def static generateEcoreFile(
		SMLProjectInfo info) '''
		<?xml version="1.0" encoding="UTF-8"?>
		<ecore:EPackage xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		    xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="«info.ecoreModelPackageName»" nsURI="«info.ecoreModelNsUri»" nsPrefix="«info.ecoreModelNsPrefix»">
		  <eClassifiers xsi:type="ecore:EClass" name="Controller" eSuperTypes="#//NamedElement">
		    <eOperations name="request"/>
		  </eClassifiers>
		  <eClassifiers xsi:type="ecore:EClass" name="Environment" eSuperTypes="#//NamedElement">
		    <eOperations name="response"/>
		  </eClassifiers>
		  <eClassifiers xsi:type="ecore:EClass" name="«info.ecoreModelPackageName.toFirstUpper»Container" eSuperTypes="#//NamedElement">
		    <eStructuralFeatures xsi:type="ecore:EReference" name="controller" eType="#//Controller"
		        containment="true"/>
		    <eStructuralFeatures xsi:type="ecore:EReference" name="environment" eType="#//Environment"
		        containment="true"/>
		  </eClassifiers>
		  <eClassifiers xsi:type="ecore:EClass" name="NamedElement">
		    <eStructuralFeatures xsi:type="ecore:EAttribute" name="name" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString"/>
		  </eClassifiers>
		</ecore:EPackage>
	'''

	def static generateXmiFile(SMLProjectInfo info) '''
		<?xml version="1.0" encoding="ASCII"?>
		<«info.ecoreModelNsPrefix»:«info.ecoreModelPackageName.toFirstUpper»Container
		    xmi:version="2.0"
		    xmlns:xmi="http://www.omg.org/XMI"
		    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		    xmlns:«info.ecoreModelNsPrefix»="«info.ecoreModelNsUri»"
		    xsi:schemaLocation="«info.ecoreModelNsUri» ../model/«info.ecoreModelFileName»"
		    name="«info.ecoreModelPackageName»">
		  <controller
		      name="controller"/>
		  <environment
		      name="environment"/>
		</«info.ecoreModelNsPrefix»:«info.ecoreModelPackageName.toFirstUpper»Container>
	'''

	def static generateSmlFile(SMLProjectInfo info) '''
		import "../model/«info.ecoreModelFileName»"
		
		specification «info.specificationName» {
		
			/*
			 * Refer to a package in the imported ecore model.
			 * Hint: Use ctrl+alt+R to rename packages and classes.
			 */
			domain «info.ecoreModelPackageName»
		
			/* 
			 * Define classes of objects that are controllable.
			 * (all others are uncontrollable.)
			 */
			 controllable{
			 	Controller
			 }
		
			/*
			 * Collaborations describe how objects interact in a certain
			 * context to collectively accomplish some desired functionality.
			 */
			collaboration HelloWorldCollaboration {
		
				dynamic role Controller controller
				dynamic role Environment env
		
				guarantee scenario MyScenario {
					env -> controller.request()
					strict urgent controller -> env.response()
				}
		
			}
		
		}
	'''

	def static generateRunconfigFile(
		SMLProjectInfo info) '''
		import "«info.specificationFileName»" configure specification «info.specificationName» use instancemodel "«info.XMIFileName»"
	'''

	private static def IProject initializeNewProject(SMLProjectInfo projectInfo) throws CoreException {
		val String name = projectInfo.getProjectName();
		val IWorkspace workspace = ResourcesPlugin.getWorkspace();
		val IWorkspaceRoot root = workspace.getRoot();
		val IProject project = root.getProject(name);
		project.create(null);
		project.open(null);
		val XTEXT_NATURE = "org.eclipse.xtext.ui.shared.xtextNature"
		val hasXtextNature = project.hasNature(XTEXT_NATURE)
		val SCENARIOTOOLS_NATURE = "org.scenariotools.builder.scenariotoolsNature"
		val hasScenarioToolsNature = project.hasNature(SCENARIOTOOLS_NATURE)
		if (!hasXtextNature || !hasScenarioToolsNature) {
			val IProjectDescription description = project.getDescription()
			val natures = new BasicEList<String>
			if(!hasXtextNature)
				natures.add(XTEXT_NATURE)
			if(!hasScenarioToolsNature)
				natures.add(SCENARIOTOOLS_NATURE)
			description.natureIds = natures
			project.setDescription(description, null)
		}
		return project;
	}
}
