/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.wizards;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.scenariotools.sml.wizards.pages.SMLNewEcoreModelCreationPage;
import org.scenariotools.sml.wizards.pages.SMLNewProjectCreationPage;
import org.scenariotools.sml.wizards.pages.SMLNewSpecificationCreationPage;
import org.scenariotools.sml.wizards.project.SMLProjectCreator;
import org.scenariotools.sml.wizards.project.SMLProjectInfo;

public class NewSMLProjectWizard extends Wizard implements INewWizard {

	private SMLNewProjectCreationPage pageNewProject;
	private SMLNewEcoreModelCreationPage pageModelFiles;
	private SMLNewSpecificationCreationPage pageSpecificationFiles;

	private SMLProjectInfo projectInfo;

	public NewSMLProjectWizard() {
		setWindowTitle("New ScenarioTools SML Project Wizard");
		projectInfo = new SMLProjectInfo();
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {

	}

	@Override
	public boolean performFinish() {
		projectInfo.setSpecificationFileName(pageSpecificationFiles.getSpecificationFileName());
		projectInfo.setSpecificationName(pageSpecificationFiles.getSpecificationName());
		projectInfo.setCollaborationName(pageSpecificationFiles.getCollaborationName());
		SMLProjectCreator.createProject(projectInfo);
		return true;
	}

	@Override
	public void addPages() {
		super.addPages();

		pageNewProject = new SMLNewProjectCreationPage(projectInfo);
		addPage(pageNewProject);
		pageModelFiles = new SMLNewEcoreModelCreationPage(projectInfo);
		addPage(pageModelFiles);
		pageSpecificationFiles = new SMLNewSpecificationCreationPage(projectInfo);
		addPage(pageSpecificationFiles);

	}

}
