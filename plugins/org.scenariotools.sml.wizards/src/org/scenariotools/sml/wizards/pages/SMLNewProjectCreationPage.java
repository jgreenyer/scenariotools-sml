/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.wizards.pages;

import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;
import org.scenariotools.sml.wizards.project.SMLProjectInfo;

public class SMLNewProjectCreationPage extends WizardNewProjectCreationPage {

	private SMLProjectInfo projectInfo;

	public SMLNewProjectCreationPage(SMLProjectInfo projectInfo) {
		super("New ScenarioTools SML Project");
		this.projectInfo = projectInfo;
		setTitle("New ScenarioTools SML Project");
		setDescription("Create a new ScenarioTools SML Project.");
	}

	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		if (!visible) {
			projectInfo.setProjectName(getProjectName());
			SMLNewEcoreModelCreationPage page = (SMLNewEcoreModelCreationPage) getNextPage();
			SMLNewSpecificationCreationPage page2 = (SMLNewSpecificationCreationPage) page.getNextPage();
			page.refreshValues();
			page2.refreshValues();
		}
	}

}
