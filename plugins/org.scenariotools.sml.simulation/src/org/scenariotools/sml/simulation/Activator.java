/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.simulation;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.osgi.framework.BundleContext;
import org.eclipse.core.runtime.Plugin;

import com.tools.logging.PluginLogManager;

public class Activator extends Plugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.scenariotools.sml.simulation"; //$NON-NLS-1$
		
    // logger configuration
    private static final String LOG_PROPERTIES_FILE = "logger.properties";

    //log manager
    private PluginLogManager logManager;

	private static BundleContext context;

	// The shared instance
	private static Activator plugin;

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		super.start(bundleContext);
		plugin = this;
		configureLogger();
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		plugin = null;
		stopLogger();
		super.stop(bundleContext);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
     * @return Returns the logManager.
     */
    public static PluginLogManager getLogManager() {
        return getDefault().logManager;
    }
	
	public void stopLogger(){
		if(this.logManager != null) {
			logManager.shutdown();
			logManager = null;
		}
	}
	
	private void configureLogger() {
		URL url = getBundle().getEntry("/" + LOG_PROPERTIES_FILE);
		try {
			InputStream inputStream = url.openStream();
			if (inputStream != null) {
				Properties properties = new Properties();
				properties.load(inputStream);
				inputStream.close();
				logManager = new PluginLogManager(this, properties);
				logManager.disableDebug();
			}
		} catch (IOException e) {
			IStatus status = new Status(
					IStatus.ERROR,
					getDefault().getBundle().getSymbolicName(),
					IStatus.ERROR,
					"Error while initializing log properties." + e.getMessage(),
					e);
			getDefault().getLog().log(status);
			throw new RuntimeException(
					"Error while initializing log properties.", e);
		}
	}
}
