/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.simulation;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.scenariotools.sml.runtime.Event;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.simulation.observer.ISimulationRunListener;
import org.scenariotools.sml.runtime.Transition;

public class SimulationRun {
	
	private boolean expandStategraph = true;
	
	private SimulationRunConfiguration parent;
	private SMLRuntimeStateGraph stateGraph;
	private SMLRuntimeState actualState;
	private EList<ISimulationRunListener> simulationRunListeners;
	private String name;
	private boolean listenerHasRegistrated = false;
	private int passedIndex;
	
	
	public SimulationRun(Configuration runconfiguration, SimulationRunConfiguration parent, int id){
		this.simulationRunListeners = new BasicEList<ISimulationRunListener>();
		this.parent = parent;
		this.stateGraph = RuntimeFactory.eINSTANCE.createSMLRuntimeStateGraph();
		this.actualState = stateGraph.init(runconfiguration);
		this.actualState.getStringToStringAnnotationMap().put("passedIndex", "Start");
		//TODO should be a function of SMLRuntimeStategraph
		this.stateGraph.getElementContainer().setEnabled(expandStategraph);
		this.name = runconfiguration.getSpecification().getName() + " " + id;
		this.passedIndex = 0;
	}

	public SMLRuntimeState getActualState(){
		return this.actualState;
	}
	
	public final EList<Event> getEnabeldEvents(){
		return this.actualState.getEnabledEvents();
	}
	
	public void performStep(MessageEvent event){
		if(expandStategraph){
			Transition t = this.stateGraph.generateSuccessor(this.actualState, event);
			this.actualState = (SMLRuntimeState) t.getTargetState();
			if(this.actualState.getStringToStringAnnotationMap().get("passedIndex").equals(t.getSourceState().getStringToStringAnnotationMap().get("passedIndex")))
				this.actualState.getStringToStringAnnotationMap().put("passedIndex", String.valueOf(++this.passedIndex));
		}else{
			this.actualState.performStep(event);
		}
		
		this.simulationRunListeners.forEach(listener -> listener.stepPerformed());
	}

	public void addSimulationRunListener(ISimulationRunListener listener){
		this.simulationRunListeners.add(listener);
		this.listenerHasRegistrated = true;
	}

	public void removeSimulationRunListener(ISimulationRunListener listener){
		this.simulationRunListeners.remove(listener);
		if(this.simulationRunListeners.isEmpty() && this.listenerHasRegistrated){
			if(this.parent.removeSimulationRun(this)){
				this.actualState = null;
				this.parent = null;
				this.stateGraph = null;
			}
		}
	}

	public final EList<SMLRuntimeState> getStates() {
		return this.stateGraph.getStates();
	}
	
	public boolean isEnvironmentMessageEvent(MessageEvent messageEvent){
		return getActualState().getObjectSystem().isEnvironmentMessageEvent(messageEvent);
	}

	public String getName() {
		return this.name;
	}
}
