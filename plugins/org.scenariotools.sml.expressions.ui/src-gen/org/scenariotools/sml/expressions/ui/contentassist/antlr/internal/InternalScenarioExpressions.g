/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
grammar InternalScenarioExpressions;

options {
	superClass=AbstractInternalContentAssistParser;
	backtrack=true;
	
}

@lexer::header {
package org.scenariotools.sml.expressions.ui.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.Lexer;
}

@parser::header {
package org.scenariotools.sml.expressions.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.scenariotools.sml.expressions.services.ScenarioExpressionsGrammarAccess;

}

@parser::members {
 
 	private ScenarioExpressionsGrammarAccess grammarAccess;
 	
    public void setGrammarAccess(ScenarioExpressionsGrammarAccess grammarAccess) {
    	this.grammarAccess = grammarAccess;
    }
    
    @Override
    protected Grammar getGrammar() {
    	return grammarAccess.getGrammar();
    }
    
    @Override
    protected String getValueForTokenName(String tokenName) {
    	return tokenName;
    }

}




// Entry rule entryRuleDocument
entryRuleDocument 
:
{ before(grammarAccess.getDocumentRule()); }
	 ruleDocument
{ after(grammarAccess.getDocumentRule()); } 
	 EOF 
;

// Rule Document
ruleDocument
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getDocumentAccess().getGroup()); }
(rule__Document__Group__0)
{ after(grammarAccess.getDocumentAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleImport
entryRuleImport 
:
{ before(grammarAccess.getImportRule()); }
	 ruleImport
{ after(grammarAccess.getImportRule()); } 
	 EOF 
;

// Rule Import
ruleImport
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getImportAccess().getGroup()); }
(rule__Import__Group__0)
{ after(grammarAccess.getImportAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleExpressionRegion
entryRuleExpressionRegion 
:
{ before(grammarAccess.getExpressionRegionRule()); }
	 ruleExpressionRegion
{ after(grammarAccess.getExpressionRegionRule()); } 
	 EOF 
;

// Rule ExpressionRegion
ruleExpressionRegion
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getExpressionRegionAccess().getGroup()); }
(rule__ExpressionRegion__Group__0)
{ after(grammarAccess.getExpressionRegionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleExpressionOrRegion
entryRuleExpressionOrRegion 
:
{ before(grammarAccess.getExpressionOrRegionRule()); }
	 ruleExpressionOrRegion
{ after(grammarAccess.getExpressionOrRegionRule()); } 
	 EOF 
;

// Rule ExpressionOrRegion
ruleExpressionOrRegion
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getExpressionOrRegionAccess().getAlternatives()); }
(rule__ExpressionOrRegion__Alternatives)
{ after(grammarAccess.getExpressionOrRegionAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleExpressionAndVariables
entryRuleExpressionAndVariables 
:
{ before(grammarAccess.getExpressionAndVariablesRule()); }
	 ruleExpressionAndVariables
{ after(grammarAccess.getExpressionAndVariablesRule()); } 
	 EOF 
;

// Rule ExpressionAndVariables
ruleExpressionAndVariables
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getExpressionAndVariablesAccess().getAlternatives()); }
(rule__ExpressionAndVariables__Alternatives)
{ after(grammarAccess.getExpressionAndVariablesAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleVariableExpression
entryRuleVariableExpression 
:
{ before(grammarAccess.getVariableExpressionRule()); }
	 ruleVariableExpression
{ after(grammarAccess.getVariableExpressionRule()); } 
	 EOF 
;

// Rule VariableExpression
ruleVariableExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getVariableExpressionAccess().getAlternatives()); }
(rule__VariableExpression__Alternatives)
{ after(grammarAccess.getVariableExpressionAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}





// Entry rule entryRuleVariableAssignment
entryRuleVariableAssignment 
:
{ before(grammarAccess.getVariableAssignmentRule()); }
	 ruleVariableAssignment
{ after(grammarAccess.getVariableAssignmentRule()); } 
	 EOF 
;

// Rule VariableAssignment
ruleVariableAssignment
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getVariableAssignmentAccess().getGroup()); }
(rule__VariableAssignment__Group__0)
{ after(grammarAccess.getVariableAssignmentAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleTypedVariableDeclaration
entryRuleTypedVariableDeclaration 
:
{ before(grammarAccess.getTypedVariableDeclarationRule()); }
	 ruleTypedVariableDeclaration
{ after(grammarAccess.getTypedVariableDeclarationRule()); } 
	 EOF 
;

// Rule TypedVariableDeclaration
ruleTypedVariableDeclaration
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getGroup()); }
(rule__TypedVariableDeclaration__Group__0)
{ after(grammarAccess.getTypedVariableDeclarationAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleClockDeclaration
entryRuleClockDeclaration 
:
{ before(grammarAccess.getClockDeclarationRule()); }
	 ruleClockDeclaration
{ after(grammarAccess.getClockDeclarationRule()); } 
	 EOF 
;

// Rule ClockDeclaration
ruleClockDeclaration
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getClockDeclarationAccess().getGroup()); }
(rule__ClockDeclaration__Group__0)
{ after(grammarAccess.getClockDeclarationAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}





// Entry rule entryRuleClockAssignment
entryRuleClockAssignment 
:
{ before(grammarAccess.getClockAssignmentRule()); }
	 ruleClockAssignment
{ after(grammarAccess.getClockAssignmentRule()); } 
	 EOF 
;

// Rule ClockAssignment
ruleClockAssignment
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getClockAssignmentAccess().getGroup()); }
(rule__ClockAssignment__Group__0)
{ after(grammarAccess.getClockAssignmentAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleExpression
entryRuleExpression 
:
{ before(grammarAccess.getExpressionRule()); }
	 ruleExpression
{ after(grammarAccess.getExpressionRule()); } 
	 EOF 
;

// Rule Expression
ruleExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getExpressionAccess().getImplicationExpressionParserRuleCall()); }
	ruleImplicationExpression
{ after(grammarAccess.getExpressionAccess().getImplicationExpressionParserRuleCall()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleImplicationExpression
entryRuleImplicationExpression 
:
{ before(grammarAccess.getImplicationExpressionRule()); }
	 ruleImplicationExpression
{ after(grammarAccess.getImplicationExpressionRule()); } 
	 EOF 
;

// Rule ImplicationExpression
ruleImplicationExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getImplicationExpressionAccess().getGroup()); }
(rule__ImplicationExpression__Group__0)
{ after(grammarAccess.getImplicationExpressionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleDisjunctionExpression
entryRuleDisjunctionExpression 
:
{ before(grammarAccess.getDisjunctionExpressionRule()); }
	 ruleDisjunctionExpression
{ after(grammarAccess.getDisjunctionExpressionRule()); } 
	 EOF 
;

// Rule DisjunctionExpression
ruleDisjunctionExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getDisjunctionExpressionAccess().getGroup()); }
(rule__DisjunctionExpression__Group__0)
{ after(grammarAccess.getDisjunctionExpressionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleConjunctionExpression
entryRuleConjunctionExpression 
:
{ before(grammarAccess.getConjunctionExpressionRule()); }
	 ruleConjunctionExpression
{ after(grammarAccess.getConjunctionExpressionRule()); } 
	 EOF 
;

// Rule ConjunctionExpression
ruleConjunctionExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getConjunctionExpressionAccess().getGroup()); }
(rule__ConjunctionExpression__Group__0)
{ after(grammarAccess.getConjunctionExpressionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleRelationExpression
entryRuleRelationExpression 
:
{ before(grammarAccess.getRelationExpressionRule()); }
	 ruleRelationExpression
{ after(grammarAccess.getRelationExpressionRule()); } 
	 EOF 
;

// Rule RelationExpression
ruleRelationExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getRelationExpressionAccess().getGroup()); }
(rule__RelationExpression__Group__0)
{ after(grammarAccess.getRelationExpressionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}





// Entry rule entryRuleAdditionExpression
entryRuleAdditionExpression 
:
{ before(grammarAccess.getAdditionExpressionRule()); }
	 ruleAdditionExpression
{ after(grammarAccess.getAdditionExpressionRule()); } 
	 EOF 
;

// Rule AdditionExpression
ruleAdditionExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getAdditionExpressionAccess().getGroup()); }
(rule__AdditionExpression__Group__0)
{ after(grammarAccess.getAdditionExpressionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleMultiplicationExpression
entryRuleMultiplicationExpression 
:
{ before(grammarAccess.getMultiplicationExpressionRule()); }
	 ruleMultiplicationExpression
{ after(grammarAccess.getMultiplicationExpressionRule()); } 
	 EOF 
;

// Rule MultiplicationExpression
ruleMultiplicationExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getMultiplicationExpressionAccess().getGroup()); }
(rule__MultiplicationExpression__Group__0)
{ after(grammarAccess.getMultiplicationExpressionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleNegatedExpression
entryRuleNegatedExpression 
:
{ before(grammarAccess.getNegatedExpressionRule()); }
	 ruleNegatedExpression
{ after(grammarAccess.getNegatedExpressionRule()); } 
	 EOF 
;

// Rule NegatedExpression
ruleNegatedExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getNegatedExpressionAccess().getAlternatives()); }
(rule__NegatedExpression__Alternatives)
{ after(grammarAccess.getNegatedExpressionAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleBasicExpression
entryRuleBasicExpression 
:
{ before(grammarAccess.getBasicExpressionRule()); }
	 ruleBasicExpression
{ after(grammarAccess.getBasicExpressionRule()); } 
	 EOF 
;

// Rule BasicExpression
ruleBasicExpression
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getBasicExpressionAccess().getAlternatives()); }
(rule__BasicExpression__Alternatives)
{ after(grammarAccess.getBasicExpressionAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleValue
entryRuleValue 
:
{ before(grammarAccess.getValueRule()); }
	 ruleValue
{ after(grammarAccess.getValueRule()); } 
	 EOF 
;

// Rule Value
ruleValue
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getValueAccess().getAlternatives()); }
(rule__Value__Alternatives)
{ after(grammarAccess.getValueAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleIntegerValue
entryRuleIntegerValue 
:
{ before(grammarAccess.getIntegerValueRule()); }
	 ruleIntegerValue
{ after(grammarAccess.getIntegerValueRule()); } 
	 EOF 
;

// Rule IntegerValue
ruleIntegerValue
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getIntegerValueAccess().getValueAssignment()); }
(rule__IntegerValue__ValueAssignment)
{ after(grammarAccess.getIntegerValueAccess().getValueAssignment()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleBooleanValue
entryRuleBooleanValue 
:
{ before(grammarAccess.getBooleanValueRule()); }
	 ruleBooleanValue
{ after(grammarAccess.getBooleanValueRule()); } 
	 EOF 
;

// Rule BooleanValue
ruleBooleanValue
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getBooleanValueAccess().getValueAssignment()); }
(rule__BooleanValue__ValueAssignment)
{ after(grammarAccess.getBooleanValueAccess().getValueAssignment()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleStringValue
entryRuleStringValue 
:
{ before(grammarAccess.getStringValueRule()); }
	 ruleStringValue
{ after(grammarAccess.getStringValueRule()); } 
	 EOF 
;

// Rule StringValue
ruleStringValue
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getStringValueAccess().getValueAssignment()); }
(rule__StringValue__ValueAssignment)
{ after(grammarAccess.getStringValueAccess().getValueAssignment()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleEnumValue
entryRuleEnumValue 
:
{ before(grammarAccess.getEnumValueRule()); }
	 ruleEnumValue
{ after(grammarAccess.getEnumValueRule()); } 
	 EOF 
;

// Rule EnumValue
ruleEnumValue
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getEnumValueAccess().getGroup()); }
(rule__EnumValue__Group__0)
{ after(grammarAccess.getEnumValueAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleNullValue
entryRuleNullValue 
:
{ before(grammarAccess.getNullValueRule()); }
	 ruleNullValue
{ after(grammarAccess.getNullValueRule()); } 
	 EOF 
;

// Rule NullValue
ruleNullValue
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getNullValueAccess().getGroup()); }
(rule__NullValue__Group__0)
{ after(grammarAccess.getNullValueAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleVariableValue
entryRuleVariableValue 
:
{ before(grammarAccess.getVariableValueRule()); }
	 ruleVariableValue
{ after(grammarAccess.getVariableValueRule()); } 
	 EOF 
;

// Rule VariableValue
ruleVariableValue
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getVariableValueAccess().getValueAssignment()); }
(rule__VariableValue__ValueAssignment)
{ after(grammarAccess.getVariableValueAccess().getValueAssignment()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleCollectionAccess
entryRuleCollectionAccess 
:
{ before(grammarAccess.getCollectionAccessRule()); }
	 ruleCollectionAccess
{ after(grammarAccess.getCollectionAccessRule()); } 
	 EOF 
;

// Rule CollectionAccess
ruleCollectionAccess
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getCollectionAccessAccess().getGroup()); }
(rule__CollectionAccess__Group__0)
{ after(grammarAccess.getCollectionAccessAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleFeatureAccess
entryRuleFeatureAccess 
:
{ before(grammarAccess.getFeatureAccessRule()); }
	 ruleFeatureAccess
{ after(grammarAccess.getFeatureAccessRule()); } 
	 EOF 
;

// Rule FeatureAccess
ruleFeatureAccess
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getFeatureAccessAccess().getGroup()); }
(rule__FeatureAccess__Group__0)
{ after(grammarAccess.getFeatureAccessAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleStructuralFeatureValue
entryRuleStructuralFeatureValue 
:
{ before(grammarAccess.getStructuralFeatureValueRule()); }
	 ruleStructuralFeatureValue
{ after(grammarAccess.getStructuralFeatureValueRule()); } 
	 EOF 
;

// Rule StructuralFeatureValue
ruleStructuralFeatureValue
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getStructuralFeatureValueAccess().getValueAssignment()); }
(rule__StructuralFeatureValue__ValueAssignment)
{ after(grammarAccess.getStructuralFeatureValueAccess().getValueAssignment()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleOperationValue
entryRuleOperationValue 
:
{ before(grammarAccess.getOperationValueRule()); }
	 ruleOperationValue
{ after(grammarAccess.getOperationValueRule()); } 
	 EOF 
;

// Rule OperationValue
ruleOperationValue
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getOperationValueAccess().getValueAssignment()); }
(rule__OperationValue__ValueAssignment)
{ after(grammarAccess.getOperationValueAccess().getValueAssignment()); }
)

;
finally {
	restoreStackSize(stackSize);
}




// Rule CollectionOperation
ruleCollectionOperation
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollectionOperationAccess().getAlternatives()); }
(rule__CollectionOperation__Alternatives)
{ after(grammarAccess.getCollectionOperationAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}





rule__ExpressionOrRegion__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExpressionOrRegionAccess().getExpressionRegionParserRuleCall_0()); }
	ruleExpressionRegion
{ after(grammarAccess.getExpressionOrRegionAccess().getExpressionRegionParserRuleCall_0()); }
)

    |(
{ before(grammarAccess.getExpressionOrRegionAccess().getExpressionAndVariablesParserRuleCall_1()); }
	ruleExpressionAndVariables
{ after(grammarAccess.getExpressionOrRegionAccess().getExpressionAndVariablesParserRuleCall_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ExpressionAndVariables__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExpressionAndVariablesAccess().getVariableExpressionParserRuleCall_0()); }
	ruleVariableExpression
{ after(grammarAccess.getExpressionAndVariablesAccess().getVariableExpressionParserRuleCall_0()); }
)

    |(
{ before(grammarAccess.getExpressionAndVariablesAccess().getExpressionParserRuleCall_1()); }
	ruleExpression
{ after(grammarAccess.getExpressionAndVariablesAccess().getExpressionParserRuleCall_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__VariableExpression__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVariableExpressionAccess().getTypedVariableDeclarationParserRuleCall_0()); }
	ruleTypedVariableDeclaration
{ after(grammarAccess.getVariableExpressionAccess().getTypedVariableDeclarationParserRuleCall_0()); }
)

    |(
{ before(grammarAccess.getVariableExpressionAccess().getVariableAssignmentParserRuleCall_1()); }
	ruleVariableAssignment
{ after(grammarAccess.getVariableExpressionAccess().getVariableAssignmentParserRuleCall_1()); }
)

    |(
{ before(grammarAccess.getVariableExpressionAccess().getClockDeclarationParserRuleCall_2()); }
	ruleClockDeclaration
{ after(grammarAccess.getVariableExpressionAccess().getClockDeclarationParserRuleCall_2()); }
)

    |(
{ before(grammarAccess.getVariableExpressionAccess().getClockAssignmentParserRuleCall_3()); }
	ruleClockAssignment
{ after(grammarAccess.getVariableExpressionAccess().getClockAssignmentParserRuleCall_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__RelationExpression__OperatorAlternatives_1_1_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRelationExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_1_0_0()); }

	'==' 

{ after(grammarAccess.getRelationExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_1_0_0()); }
)

    |(
{ before(grammarAccess.getRelationExpressionAccess().getOperatorExclamationMarkEqualsSignKeyword_1_1_0_1()); }

	'!=' 

{ after(grammarAccess.getRelationExpressionAccess().getOperatorExclamationMarkEqualsSignKeyword_1_1_0_1()); }
)

    |(
{ before(grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignKeyword_1_1_0_2()); }

	'<' 

{ after(grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignKeyword_1_1_0_2()); }
)

    |(
{ before(grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignKeyword_1_1_0_3()); }

	'>' 

{ after(grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignKeyword_1_1_0_3()); }
)

    |(
{ before(grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_1_0_4()); }

	'<=' 

{ after(grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_1_0_4()); }
)

    |(
{ before(grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_1_0_5()); }

	'>=' 

{ after(grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_1_0_5()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__AdditionExpression__OperatorAlternatives_1_1_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAdditionExpressionAccess().getOperatorPlusSignKeyword_1_1_0_0()); }

	'+' 

{ after(grammarAccess.getAdditionExpressionAccess().getOperatorPlusSignKeyword_1_1_0_0()); }
)

    |(
{ before(grammarAccess.getAdditionExpressionAccess().getOperatorHyphenMinusKeyword_1_1_0_1()); }

	'-' 

{ after(grammarAccess.getAdditionExpressionAccess().getOperatorHyphenMinusKeyword_1_1_0_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__MultiplicationExpression__OperatorAlternatives_1_1_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getMultiplicationExpressionAccess().getOperatorAsteriskKeyword_1_1_0_0()); }

	'*' 

{ after(grammarAccess.getMultiplicationExpressionAccess().getOperatorAsteriskKeyword_1_1_0_0()); }
)

    |(
{ before(grammarAccess.getMultiplicationExpressionAccess().getOperatorSolidusKeyword_1_1_0_1()); }

	'/' 

{ after(grammarAccess.getMultiplicationExpressionAccess().getOperatorSolidusKeyword_1_1_0_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__NegatedExpression__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getNegatedExpressionAccess().getGroup_0()); }
(rule__NegatedExpression__Group_0__0)
{ after(grammarAccess.getNegatedExpressionAccess().getGroup_0()); }
)

    |(
{ before(grammarAccess.getNegatedExpressionAccess().getBasicExpressionParserRuleCall_1()); }
	ruleBasicExpression
{ after(grammarAccess.getNegatedExpressionAccess().getBasicExpressionParserRuleCall_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__NegatedExpression__OperatorAlternatives_0_1_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getNegatedExpressionAccess().getOperatorExclamationMarkKeyword_0_1_0_0()); }

	'!' 

{ after(grammarAccess.getNegatedExpressionAccess().getOperatorExclamationMarkKeyword_0_1_0_0()); }
)

    |(
{ before(grammarAccess.getNegatedExpressionAccess().getOperatorHyphenMinusKeyword_0_1_0_1()); }

	'-' 

{ after(grammarAccess.getNegatedExpressionAccess().getOperatorHyphenMinusKeyword_0_1_0_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__BasicExpression__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getBasicExpressionAccess().getValueParserRuleCall_0()); }
	ruleValue
{ after(grammarAccess.getBasicExpressionAccess().getValueParserRuleCall_0()); }
)

    |(
{ before(grammarAccess.getBasicExpressionAccess().getGroup_1()); }
(rule__BasicExpression__Group_1__0)
{ after(grammarAccess.getBasicExpressionAccess().getGroup_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Value__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getValueAccess().getIntegerValueParserRuleCall_0()); }
	ruleIntegerValue
{ after(grammarAccess.getValueAccess().getIntegerValueParserRuleCall_0()); }
)

    |(
{ before(grammarAccess.getValueAccess().getBooleanValueParserRuleCall_1()); }
	ruleBooleanValue
{ after(grammarAccess.getValueAccess().getBooleanValueParserRuleCall_1()); }
)

    |(
{ before(grammarAccess.getValueAccess().getStringValueParserRuleCall_2()); }
	ruleStringValue
{ after(grammarAccess.getValueAccess().getStringValueParserRuleCall_2()); }
)

    |(
{ before(grammarAccess.getValueAccess().getEnumValueParserRuleCall_3()); }
	ruleEnumValue
{ after(grammarAccess.getValueAccess().getEnumValueParserRuleCall_3()); }
)

    |(
{ before(grammarAccess.getValueAccess().getNullValueParserRuleCall_4()); }
	ruleNullValue
{ after(grammarAccess.getValueAccess().getNullValueParserRuleCall_4()); }
)

    |(
{ before(grammarAccess.getValueAccess().getVariableValueParserRuleCall_5()); }
	ruleVariableValue
{ after(grammarAccess.getValueAccess().getVariableValueParserRuleCall_5()); }
)

    |(
{ before(grammarAccess.getValueAccess().getFeatureAccessParserRuleCall_6()); }
	ruleFeatureAccess
{ after(grammarAccess.getValueAccess().getFeatureAccessParserRuleCall_6()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__IntegerValue__ValueAlternatives_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0_0()); }
	RULE_INT
{ after(grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0_0()); }
)

    |(
{ before(grammarAccess.getIntegerValueAccess().getValueSIGNEDINTTerminalRuleCall_0_1()); }
	RULE_SIGNEDINT
{ after(grammarAccess.getIntegerValueAccess().getValueSIGNEDINTTerminalRuleCall_0_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Alternatives_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getGroup_2_0()); }
(rule__FeatureAccess__Group_2_0__0)
{ after(grammarAccess.getFeatureAccessAccess().getGroup_2_0()); }
)

    |(
{ before(grammarAccess.getFeatureAccessAccess().getGroup_2_1()); }
(rule__FeatureAccess__Group_2_1__0)
{ after(grammarAccess.getFeatureAccessAccess().getGroup_2_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__CollectionOperation__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0()); }
(	'any' 
)
{ after(grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0()); }
)

    |(
{ before(grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1()); }
(	'contains' 
)
{ after(grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1()); }
)

    |(
{ before(grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2()); }
(	'containsAll' 
)
{ after(grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2()); }
)

    |(
{ before(grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3()); }
(	'first' 
)
{ after(grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3()); }
)

    |(
{ before(grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4()); }
(	'get' 
)
{ after(grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4()); }
)

    |(
{ before(grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5()); }
(	'isEmpty' 
)
{ after(grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5()); }
)

    |(
{ before(grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6()); }
(	'last' 
)
{ after(grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6()); }
)

    |(
{ before(grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7()); }
(	'size' 
)
{ after(grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7()); }
)

;
finally {
	restoreStackSize(stackSize);
}




rule__Document__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Document__Group__0__Impl
	rule__Document__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Document__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDocumentAccess().getImportsAssignment_0()); }
(rule__Document__ImportsAssignment_0)*
{ after(grammarAccess.getDocumentAccess().getImportsAssignment_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Document__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Document__Group__1__Impl
	rule__Document__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Document__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDocumentAccess().getGroup_1()); }
(rule__Document__Group_1__0)*
{ after(grammarAccess.getDocumentAccess().getGroup_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Document__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Document__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Document__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDocumentAccess().getExpressionsAssignment_2()); }
(rule__Document__ExpressionsAssignment_2)*
{ after(grammarAccess.getDocumentAccess().getExpressionsAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__Document__Group_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Document__Group_1__0__Impl
	rule__Document__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Document__Group_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDocumentAccess().getDomainKeyword_1_0()); }

	'domain' 

{ after(grammarAccess.getDocumentAccess().getDomainKeyword_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Document__Group_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Document__Group_1__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Document__Group_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDocumentAccess().getDomainsAssignment_1_1()); }
(rule__Document__DomainsAssignment_1_1)
{ after(grammarAccess.getDocumentAccess().getDomainsAssignment_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__Import__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Import__Group__0__Impl
	rule__Import__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Import__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getImportAccess().getImportKeyword_0()); }

	'import' 

{ after(grammarAccess.getImportAccess().getImportKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Import__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Import__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Import__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getImportAccess().getImportURIAssignment_1()); }
(rule__Import__ImportURIAssignment_1)
{ after(grammarAccess.getImportAccess().getImportURIAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ExpressionRegion__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExpressionRegion__Group__0__Impl
	rule__ExpressionRegion__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ExpressionRegion__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExpressionRegionAccess().getExpressionRegionAction_0()); }
(

)
{ after(grammarAccess.getExpressionRegionAccess().getExpressionRegionAction_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ExpressionRegion__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExpressionRegion__Group__1__Impl
	rule__ExpressionRegion__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__ExpressionRegion__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExpressionRegionAccess().getLeftCurlyBracketKeyword_1()); }

	'{' 

{ after(grammarAccess.getExpressionRegionAccess().getLeftCurlyBracketKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ExpressionRegion__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExpressionRegion__Group__2__Impl
	rule__ExpressionRegion__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__ExpressionRegion__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExpressionRegionAccess().getGroup_2()); }
(rule__ExpressionRegion__Group_2__0)*
{ after(grammarAccess.getExpressionRegionAccess().getGroup_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ExpressionRegion__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExpressionRegion__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ExpressionRegion__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExpressionRegionAccess().getRightCurlyBracketKeyword_3()); }

	'}' 

{ after(grammarAccess.getExpressionRegionAccess().getRightCurlyBracketKeyword_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}










rule__ExpressionRegion__Group_2__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExpressionRegion__Group_2__0__Impl
	rule__ExpressionRegion__Group_2__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ExpressionRegion__Group_2__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExpressionRegionAccess().getExpressionsAssignment_2_0()); }
(rule__ExpressionRegion__ExpressionsAssignment_2_0)
{ after(grammarAccess.getExpressionRegionAccess().getExpressionsAssignment_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ExpressionRegion__Group_2__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ExpressionRegion__Group_2__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ExpressionRegion__Group_2__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExpressionRegionAccess().getSemicolonKeyword_2_1()); }

	';' 

{ after(grammarAccess.getExpressionRegionAccess().getSemicolonKeyword_2_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}







rule__VariableAssignment__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__VariableAssignment__Group__0__Impl
	rule__VariableAssignment__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__VariableAssignment__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVariableAssignmentAccess().getVariableAssignment_0()); }
(rule__VariableAssignment__VariableAssignment_0)
{ after(grammarAccess.getVariableAssignmentAccess().getVariableAssignment_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__VariableAssignment__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__VariableAssignment__Group__1__Impl
	rule__VariableAssignment__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__VariableAssignment__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVariableAssignmentAccess().getEqualsSignKeyword_1()); }

	'=' 

{ after(grammarAccess.getVariableAssignmentAccess().getEqualsSignKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__VariableAssignment__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__VariableAssignment__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__VariableAssignment__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVariableAssignmentAccess().getExpressionAssignment_2()); }
(rule__VariableAssignment__ExpressionAssignment_2)
{ after(grammarAccess.getVariableAssignmentAccess().getExpressionAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__TypedVariableDeclaration__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TypedVariableDeclaration__Group__0__Impl
	rule__TypedVariableDeclaration__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__TypedVariableDeclaration__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getVarKeyword_0()); }

	'var' 

{ after(grammarAccess.getTypedVariableDeclarationAccess().getVarKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TypedVariableDeclaration__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TypedVariableDeclaration__Group__1__Impl
	rule__TypedVariableDeclaration__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__TypedVariableDeclaration__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getTypeAssignment_1()); }
(rule__TypedVariableDeclaration__TypeAssignment_1)
{ after(grammarAccess.getTypedVariableDeclarationAccess().getTypeAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TypedVariableDeclaration__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TypedVariableDeclaration__Group__2__Impl
	rule__TypedVariableDeclaration__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__TypedVariableDeclaration__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getNameAssignment_2()); }
(rule__TypedVariableDeclaration__NameAssignment_2)
{ after(grammarAccess.getTypedVariableDeclarationAccess().getNameAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TypedVariableDeclaration__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TypedVariableDeclaration__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__TypedVariableDeclaration__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getGroup_3()); }
(rule__TypedVariableDeclaration__Group_3__0)?
{ after(grammarAccess.getTypedVariableDeclarationAccess().getGroup_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}










rule__TypedVariableDeclaration__Group_3__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TypedVariableDeclaration__Group_3__0__Impl
	rule__TypedVariableDeclaration__Group_3__1
;
finally {
	restoreStackSize(stackSize);
}

rule__TypedVariableDeclaration__Group_3__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getEqualsSignKeyword_3_0()); }

	'=' 

{ after(grammarAccess.getTypedVariableDeclarationAccess().getEqualsSignKeyword_3_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__TypedVariableDeclaration__Group_3__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__TypedVariableDeclaration__Group_3__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__TypedVariableDeclaration__Group_3__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getExpressionAssignment_3_1()); }
(rule__TypedVariableDeclaration__ExpressionAssignment_3_1)
{ after(grammarAccess.getTypedVariableDeclarationAccess().getExpressionAssignment_3_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ClockDeclaration__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockDeclaration__Group__0__Impl
	rule__ClockDeclaration__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockDeclaration__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockDeclarationAccess().getClockDeclarationAction_0()); }
(

)
{ after(grammarAccess.getClockDeclarationAccess().getClockDeclarationAction_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ClockDeclaration__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockDeclaration__Group__1__Impl
	rule__ClockDeclaration__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockDeclaration__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockDeclarationAccess().getClockKeyword_1()); }

	'clock' 

{ after(grammarAccess.getClockDeclarationAccess().getClockKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ClockDeclaration__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockDeclaration__Group__2__Impl
	rule__ClockDeclaration__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockDeclaration__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockDeclarationAccess().getNameAssignment_2()); }
(rule__ClockDeclaration__NameAssignment_2)
{ after(grammarAccess.getClockDeclarationAccess().getNameAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ClockDeclaration__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockDeclaration__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockDeclaration__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockDeclarationAccess().getGroup_3()); }
(rule__ClockDeclaration__Group_3__0)?
{ after(grammarAccess.getClockDeclarationAccess().getGroup_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}










rule__ClockDeclaration__Group_3__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockDeclaration__Group_3__0__Impl
	rule__ClockDeclaration__Group_3__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockDeclaration__Group_3__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockDeclarationAccess().getEqualsSignKeyword_3_0()); }

	'=' 

{ after(grammarAccess.getClockDeclarationAccess().getEqualsSignKeyword_3_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ClockDeclaration__Group_3__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockDeclaration__Group_3__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockDeclaration__Group_3__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockDeclarationAccess().getExpressionAssignment_3_1()); }
(rule__ClockDeclaration__ExpressionAssignment_3_1)
{ after(grammarAccess.getClockDeclarationAccess().getExpressionAssignment_3_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}







rule__ClockAssignment__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockAssignment__Group__0__Impl
	rule__ClockAssignment__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockAssignment__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockAssignmentAccess().getResetClockKeyword_0()); }

	'reset clock' 

{ after(grammarAccess.getClockAssignmentAccess().getResetClockKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ClockAssignment__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockAssignment__Group__1__Impl
	rule__ClockAssignment__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockAssignment__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockAssignmentAccess().getVariableAssignment_1()); }
(rule__ClockAssignment__VariableAssignment_1)
{ after(grammarAccess.getClockAssignmentAccess().getVariableAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ClockAssignment__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockAssignment__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockAssignment__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockAssignmentAccess().getGroup_2()); }
(rule__ClockAssignment__Group_2__0)?
{ after(grammarAccess.getClockAssignmentAccess().getGroup_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__ClockAssignment__Group_2__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockAssignment__Group_2__0__Impl
	rule__ClockAssignment__Group_2__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockAssignment__Group_2__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockAssignmentAccess().getEqualsSignKeyword_2_0()); }

	'=' 

{ after(grammarAccess.getClockAssignmentAccess().getEqualsSignKeyword_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ClockAssignment__Group_2__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ClockAssignment__Group_2__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ClockAssignment__Group_2__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockAssignmentAccess().getExpressionAssignment_2_1()); }
(rule__ClockAssignment__ExpressionAssignment_2_1)
{ after(grammarAccess.getClockAssignmentAccess().getExpressionAssignment_2_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ImplicationExpression__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ImplicationExpression__Group__0__Impl
	rule__ImplicationExpression__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ImplicationExpression__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getImplicationExpressionAccess().getDisjunctionExpressionParserRuleCall_0()); }
	ruleDisjunctionExpression
{ after(grammarAccess.getImplicationExpressionAccess().getDisjunctionExpressionParserRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ImplicationExpression__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ImplicationExpression__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ImplicationExpression__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getImplicationExpressionAccess().getGroup_1()); }
(rule__ImplicationExpression__Group_1__0)?
{ after(grammarAccess.getImplicationExpressionAccess().getGroup_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ImplicationExpression__Group_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ImplicationExpression__Group_1__0__Impl
	rule__ImplicationExpression__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ImplicationExpression__Group_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getImplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
(

)
{ after(grammarAccess.getImplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ImplicationExpression__Group_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ImplicationExpression__Group_1__1__Impl
	rule__ImplicationExpression__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__ImplicationExpression__Group_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getImplicationExpressionAccess().getOperatorAssignment_1_1()); }
(rule__ImplicationExpression__OperatorAssignment_1_1)
{ after(grammarAccess.getImplicationExpressionAccess().getOperatorAssignment_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ImplicationExpression__Group_1__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ImplicationExpression__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ImplicationExpression__Group_1__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getImplicationExpressionAccess().getRightAssignment_1_2()); }
(rule__ImplicationExpression__RightAssignment_1_2)
{ after(grammarAccess.getImplicationExpressionAccess().getRightAssignment_1_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__DisjunctionExpression__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__DisjunctionExpression__Group__0__Impl
	rule__DisjunctionExpression__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__DisjunctionExpression__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDisjunctionExpressionAccess().getConjunctionExpressionParserRuleCall_0()); }
	ruleConjunctionExpression
{ after(grammarAccess.getDisjunctionExpressionAccess().getConjunctionExpressionParserRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__DisjunctionExpression__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__DisjunctionExpression__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__DisjunctionExpression__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDisjunctionExpressionAccess().getGroup_1()); }
(rule__DisjunctionExpression__Group_1__0)?
{ after(grammarAccess.getDisjunctionExpressionAccess().getGroup_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__DisjunctionExpression__Group_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__DisjunctionExpression__Group_1__0__Impl
	rule__DisjunctionExpression__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__DisjunctionExpression__Group_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDisjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
(

)
{ after(grammarAccess.getDisjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__DisjunctionExpression__Group_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__DisjunctionExpression__Group_1__1__Impl
	rule__DisjunctionExpression__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__DisjunctionExpression__Group_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDisjunctionExpressionAccess().getOperatorAssignment_1_1()); }
(rule__DisjunctionExpression__OperatorAssignment_1_1)
{ after(grammarAccess.getDisjunctionExpressionAccess().getOperatorAssignment_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__DisjunctionExpression__Group_1__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__DisjunctionExpression__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__DisjunctionExpression__Group_1__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDisjunctionExpressionAccess().getRightAssignment_1_2()); }
(rule__DisjunctionExpression__RightAssignment_1_2)
{ after(grammarAccess.getDisjunctionExpressionAccess().getRightAssignment_1_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__ConjunctionExpression__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConjunctionExpression__Group__0__Impl
	rule__ConjunctionExpression__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ConjunctionExpression__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConjunctionExpressionAccess().getRelationExpressionParserRuleCall_0()); }
	ruleRelationExpression
{ after(grammarAccess.getConjunctionExpressionAccess().getRelationExpressionParserRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConjunctionExpression__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConjunctionExpression__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ConjunctionExpression__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConjunctionExpressionAccess().getGroup_1()); }
(rule__ConjunctionExpression__Group_1__0)?
{ after(grammarAccess.getConjunctionExpressionAccess().getGroup_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ConjunctionExpression__Group_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConjunctionExpression__Group_1__0__Impl
	rule__ConjunctionExpression__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ConjunctionExpression__Group_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
(

)
{ after(grammarAccess.getConjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConjunctionExpression__Group_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConjunctionExpression__Group_1__1__Impl
	rule__ConjunctionExpression__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__ConjunctionExpression__Group_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConjunctionExpressionAccess().getOperatorAssignment_1_1()); }
(rule__ConjunctionExpression__OperatorAssignment_1_1)
{ after(grammarAccess.getConjunctionExpressionAccess().getOperatorAssignment_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ConjunctionExpression__Group_1__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ConjunctionExpression__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ConjunctionExpression__Group_1__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConjunctionExpressionAccess().getRightAssignment_1_2()); }
(rule__ConjunctionExpression__RightAssignment_1_2)
{ after(grammarAccess.getConjunctionExpressionAccess().getRightAssignment_1_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__RelationExpression__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__RelationExpression__Group__0__Impl
	rule__RelationExpression__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__RelationExpression__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRelationExpressionAccess().getAdditionExpressionParserRuleCall_0()); }
	ruleAdditionExpression
{ after(grammarAccess.getRelationExpressionAccess().getAdditionExpressionParserRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__RelationExpression__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__RelationExpression__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__RelationExpression__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRelationExpressionAccess().getGroup_1()); }
(rule__RelationExpression__Group_1__0)?
{ after(grammarAccess.getRelationExpressionAccess().getGroup_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__RelationExpression__Group_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__RelationExpression__Group_1__0__Impl
	rule__RelationExpression__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__RelationExpression__Group_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRelationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
(

)
{ after(grammarAccess.getRelationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__RelationExpression__Group_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__RelationExpression__Group_1__1__Impl
	rule__RelationExpression__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__RelationExpression__Group_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRelationExpressionAccess().getOperatorAssignment_1_1()); }
(rule__RelationExpression__OperatorAssignment_1_1)
{ after(grammarAccess.getRelationExpressionAccess().getOperatorAssignment_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__RelationExpression__Group_1__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__RelationExpression__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__RelationExpression__Group_1__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRelationExpressionAccess().getRightAssignment_1_2()); }
(rule__RelationExpression__RightAssignment_1_2)
{ after(grammarAccess.getRelationExpressionAccess().getRightAssignment_1_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}









rule__AdditionExpression__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__AdditionExpression__Group__0__Impl
	rule__AdditionExpression__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__AdditionExpression__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAdditionExpressionAccess().getMultiplicationExpressionParserRuleCall_0()); }
	ruleMultiplicationExpression
{ after(grammarAccess.getAdditionExpressionAccess().getMultiplicationExpressionParserRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__AdditionExpression__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__AdditionExpression__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__AdditionExpression__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAdditionExpressionAccess().getGroup_1()); }
(rule__AdditionExpression__Group_1__0)?
{ after(grammarAccess.getAdditionExpressionAccess().getGroup_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__AdditionExpression__Group_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__AdditionExpression__Group_1__0__Impl
	rule__AdditionExpression__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__AdditionExpression__Group_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAdditionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
(

)
{ after(grammarAccess.getAdditionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__AdditionExpression__Group_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__AdditionExpression__Group_1__1__Impl
	rule__AdditionExpression__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__AdditionExpression__Group_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAdditionExpressionAccess().getOperatorAssignment_1_1()); }
(rule__AdditionExpression__OperatorAssignment_1_1)
{ after(grammarAccess.getAdditionExpressionAccess().getOperatorAssignment_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__AdditionExpression__Group_1__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__AdditionExpression__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__AdditionExpression__Group_1__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAdditionExpressionAccess().getRightAssignment_1_2()); }
(rule__AdditionExpression__RightAssignment_1_2)
{ after(grammarAccess.getAdditionExpressionAccess().getRightAssignment_1_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__MultiplicationExpression__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__MultiplicationExpression__Group__0__Impl
	rule__MultiplicationExpression__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__MultiplicationExpression__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getMultiplicationExpressionAccess().getNegatedExpressionParserRuleCall_0()); }
	ruleNegatedExpression
{ after(grammarAccess.getMultiplicationExpressionAccess().getNegatedExpressionParserRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__MultiplicationExpression__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__MultiplicationExpression__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__MultiplicationExpression__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getMultiplicationExpressionAccess().getGroup_1()); }
(rule__MultiplicationExpression__Group_1__0)?
{ after(grammarAccess.getMultiplicationExpressionAccess().getGroup_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__MultiplicationExpression__Group_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__MultiplicationExpression__Group_1__0__Impl
	rule__MultiplicationExpression__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__MultiplicationExpression__Group_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getMultiplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
(

)
{ after(grammarAccess.getMultiplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__MultiplicationExpression__Group_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__MultiplicationExpression__Group_1__1__Impl
	rule__MultiplicationExpression__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__MultiplicationExpression__Group_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getMultiplicationExpressionAccess().getOperatorAssignment_1_1()); }
(rule__MultiplicationExpression__OperatorAssignment_1_1)
{ after(grammarAccess.getMultiplicationExpressionAccess().getOperatorAssignment_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__MultiplicationExpression__Group_1__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__MultiplicationExpression__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__MultiplicationExpression__Group_1__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getMultiplicationExpressionAccess().getRightAssignment_1_2()); }
(rule__MultiplicationExpression__RightAssignment_1_2)
{ after(grammarAccess.getMultiplicationExpressionAccess().getRightAssignment_1_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__NegatedExpression__Group_0__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__NegatedExpression__Group_0__0__Impl
	rule__NegatedExpression__Group_0__1
;
finally {
	restoreStackSize(stackSize);
}

rule__NegatedExpression__Group_0__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getNegatedExpressionAccess().getUnaryOperationExpressionAction_0_0()); }
(

)
{ after(grammarAccess.getNegatedExpressionAccess().getUnaryOperationExpressionAction_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__NegatedExpression__Group_0__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__NegatedExpression__Group_0__1__Impl
	rule__NegatedExpression__Group_0__2
;
finally {
	restoreStackSize(stackSize);
}

rule__NegatedExpression__Group_0__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getNegatedExpressionAccess().getOperatorAssignment_0_1()); }
(rule__NegatedExpression__OperatorAssignment_0_1)
{ after(grammarAccess.getNegatedExpressionAccess().getOperatorAssignment_0_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__NegatedExpression__Group_0__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__NegatedExpression__Group_0__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__NegatedExpression__Group_0__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getNegatedExpressionAccess().getOperandAssignment_0_2()); }
(rule__NegatedExpression__OperandAssignment_0_2)
{ after(grammarAccess.getNegatedExpressionAccess().getOperandAssignment_0_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__BasicExpression__Group_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__BasicExpression__Group_1__0__Impl
	rule__BasicExpression__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__BasicExpression__Group_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0()); }

	'(' 

{ after(grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__BasicExpression__Group_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__BasicExpression__Group_1__1__Impl
	rule__BasicExpression__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__BasicExpression__Group_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getBasicExpressionAccess().getExpressionParserRuleCall_1_1()); }
	ruleExpression
{ after(grammarAccess.getBasicExpressionAccess().getExpressionParserRuleCall_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__BasicExpression__Group_1__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__BasicExpression__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__BasicExpression__Group_1__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getBasicExpressionAccess().getRightParenthesisKeyword_1_2()); }

	')' 

{ after(grammarAccess.getBasicExpressionAccess().getRightParenthesisKeyword_1_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__EnumValue__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__EnumValue__Group__0__Impl
	rule__EnumValue__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__EnumValue__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getEnumValueAccess().getTypeAssignment_0()); }
(rule__EnumValue__TypeAssignment_0)
{ after(grammarAccess.getEnumValueAccess().getTypeAssignment_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__EnumValue__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__EnumValue__Group__1__Impl
	rule__EnumValue__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__EnumValue__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getEnumValueAccess().getColonKeyword_1()); }

	':' 

{ after(grammarAccess.getEnumValueAccess().getColonKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__EnumValue__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__EnumValue__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__EnumValue__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getEnumValueAccess().getValueAssignment_2()); }
(rule__EnumValue__ValueAssignment_2)
{ after(grammarAccess.getEnumValueAccess().getValueAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__NullValue__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__NullValue__Group__0__Impl
	rule__NullValue__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__NullValue__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getNullValueAccess().getNullValueAction_0()); }
(

)
{ after(grammarAccess.getNullValueAccess().getNullValueAction_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__NullValue__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__NullValue__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__NullValue__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getNullValueAccess().getNullKeyword_1()); }

	'null' 

{ after(grammarAccess.getNullValueAccess().getNullKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__CollectionAccess__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__CollectionAccess__Group__0__Impl
	rule__CollectionAccess__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__CollectionAccess__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollectionAccessAccess().getCollectionOperationAssignment_0()); }
(rule__CollectionAccess__CollectionOperationAssignment_0)
{ after(grammarAccess.getCollectionAccessAccess().getCollectionOperationAssignment_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__CollectionAccess__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__CollectionAccess__Group__1__Impl
	rule__CollectionAccess__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__CollectionAccess__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollectionAccessAccess().getLeftParenthesisKeyword_1()); }

	'(' 

{ after(grammarAccess.getCollectionAccessAccess().getLeftParenthesisKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__CollectionAccess__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__CollectionAccess__Group__2__Impl
	rule__CollectionAccess__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__CollectionAccess__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollectionAccessAccess().getParameterAssignment_2()); }
(rule__CollectionAccess__ParameterAssignment_2)?
{ after(grammarAccess.getCollectionAccessAccess().getParameterAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__CollectionAccess__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__CollectionAccess__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__CollectionAccess__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollectionAccessAccess().getRightParenthesisKeyword_3()); }

	')' 

{ after(grammarAccess.getCollectionAccessAccess().getRightParenthesisKeyword_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}










rule__FeatureAccess__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group__0__Impl
	rule__FeatureAccess__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getTargetAssignment_0()); }
(rule__FeatureAccess__TargetAssignment_0)
{ after(grammarAccess.getFeatureAccessAccess().getTargetAssignment_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__FeatureAccess__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group__1__Impl
	rule__FeatureAccess__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getFullStopKeyword_1()); }

	'.' 

{ after(grammarAccess.getFeatureAccessAccess().getFullStopKeyword_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__FeatureAccess__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getAlternatives_2()); }
(rule__FeatureAccess__Alternatives_2)
{ after(grammarAccess.getFeatureAccessAccess().getAlternatives_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__FeatureAccess__Group_2_0__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_0__0__Impl
	rule__FeatureAccess__Group_2_0__1
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_0__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getValueAssignment_2_0_0()); }
(rule__FeatureAccess__ValueAssignment_2_0_0)
{ after(grammarAccess.getFeatureAccessAccess().getValueAssignment_2_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__FeatureAccess__Group_2_0__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_0__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_0__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getGroup_2_0_1()); }
(rule__FeatureAccess__Group_2_0_1__0)?
{ after(grammarAccess.getFeatureAccessAccess().getGroup_2_0_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__FeatureAccess__Group_2_0_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_0_1__0__Impl
	rule__FeatureAccess__Group_2_0_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_0_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getFullStopKeyword_2_0_1_0()); }

	'.' 

{ after(grammarAccess.getFeatureAccessAccess().getFullStopKeyword_2_0_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__FeatureAccess__Group_2_0_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_0_1__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_0_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getCollectionAccessAssignment_2_0_1_1()); }
(rule__FeatureAccess__CollectionAccessAssignment_2_0_1_1)
{ after(grammarAccess.getFeatureAccessAccess().getCollectionAccessAssignment_2_0_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__FeatureAccess__Group_2_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_1__0__Impl
	rule__FeatureAccess__Group_2_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getValueAssignment_2_1_0()); }
(rule__FeatureAccess__ValueAssignment_2_1_0)
{ after(grammarAccess.getFeatureAccessAccess().getValueAssignment_2_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__FeatureAccess__Group_2_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_1__1__Impl
	rule__FeatureAccess__Group_2_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getLeftParenthesisKeyword_2_1_1()); }

	'(' 

{ after(grammarAccess.getFeatureAccessAccess().getLeftParenthesisKeyword_2_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__FeatureAccess__Group_2_1__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_1__2__Impl
	rule__FeatureAccess__Group_2_1__3
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_1__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getGroup_2_1_2()); }
(rule__FeatureAccess__Group_2_1_2__0)?
{ after(grammarAccess.getFeatureAccessAccess().getGroup_2_1_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__FeatureAccess__Group_2_1__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_1__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_1__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getRightParenthesisKeyword_2_1_3()); }

	')' 

{ after(grammarAccess.getFeatureAccessAccess().getRightParenthesisKeyword_2_1_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}










rule__FeatureAccess__Group_2_1_2__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_1_2__0__Impl
	rule__FeatureAccess__Group_2_1_2__1
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_1_2__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getParametersAssignment_2_1_2_0()); }
(rule__FeatureAccess__ParametersAssignment_2_1_2_0)
{ after(grammarAccess.getFeatureAccessAccess().getParametersAssignment_2_1_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__FeatureAccess__Group_2_1_2__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_1_2__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_1_2__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getGroup_2_1_2_1()); }
(rule__FeatureAccess__Group_2_1_2_1__0)*
{ after(grammarAccess.getFeatureAccessAccess().getGroup_2_1_2_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__FeatureAccess__Group_2_1_2_1__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_1_2_1__0__Impl
	rule__FeatureAccess__Group_2_1_2_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_1_2_1__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getCommaKeyword_2_1_2_1_0()); }

	',' 

{ after(grammarAccess.getFeatureAccessAccess().getCommaKeyword_2_1_2_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__FeatureAccess__Group_2_1_2_1__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__FeatureAccess__Group_2_1_2_1__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__Group_2_1_2_1__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getParametersAssignment_2_1_2_1_1()); }
(rule__FeatureAccess__ParametersAssignment_2_1_2_1_1)
{ after(grammarAccess.getFeatureAccessAccess().getParametersAssignment_2_1_2_1_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}







rule__Document__ImportsAssignment_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDocumentAccess().getImportsImportParserRuleCall_0_0()); }
	ruleImport{ after(grammarAccess.getDocumentAccess().getImportsImportParserRuleCall_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Document__DomainsAssignment_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDocumentAccess().getDomainsEPackageCrossReference_1_1_0()); }
(
{ before(grammarAccess.getDocumentAccess().getDomainsEPackageIDTerminalRuleCall_1_1_0_1()); }
	RULE_ID{ after(grammarAccess.getDocumentAccess().getDomainsEPackageIDTerminalRuleCall_1_1_0_1()); }
)
{ after(grammarAccess.getDocumentAccess().getDomainsEPackageCrossReference_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Document__ExpressionsAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDocumentAccess().getExpressionsExpressionRegionParserRuleCall_2_0()); }
	ruleExpressionRegion{ after(grammarAccess.getDocumentAccess().getExpressionsExpressionRegionParserRuleCall_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Import__ImportURIAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); }
	RULE_STRING{ after(grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ExpressionRegion__ExpressionsAssignment_2_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getExpressionRegionAccess().getExpressionsExpressionOrRegionParserRuleCall_2_0_0()); }
	ruleExpressionOrRegion{ after(grammarAccess.getExpressionRegionAccess().getExpressionsExpressionOrRegionParserRuleCall_2_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}



rule__VariableAssignment__VariableAssignment_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationCrossReference_0_0()); }
(
{ before(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationIDTerminalRuleCall_0_0_1()); }
	RULE_ID{ after(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationIDTerminalRuleCall_0_0_1()); }
)
{ after(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationCrossReference_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__VariableAssignment__ExpressionAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVariableAssignmentAccess().getExpressionExpressionParserRuleCall_2_0()); }
	ruleExpression{ after(grammarAccess.getVariableAssignmentAccess().getExpressionExpressionParserRuleCall_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__TypedVariableDeclaration__TypeAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierCrossReference_1_0()); }
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierIDTerminalRuleCall_1_0_1()); }
	RULE_ID{ after(grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierIDTerminalRuleCall_1_0_1()); }
)
{ after(grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierCrossReference_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__TypedVariableDeclaration__NameAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getNameIDTerminalRuleCall_2_0()); }
	RULE_ID{ after(grammarAccess.getTypedVariableDeclarationAccess().getNameIDTerminalRuleCall_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__TypedVariableDeclaration__ExpressionAssignment_3_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getTypedVariableDeclarationAccess().getExpressionExpressionParserRuleCall_3_1_0()); }
	ruleExpression{ after(grammarAccess.getTypedVariableDeclarationAccess().getExpressionExpressionParserRuleCall_3_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ClockDeclaration__NameAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockDeclarationAccess().getNameIDTerminalRuleCall_2_0()); }
	RULE_ID{ after(grammarAccess.getClockDeclarationAccess().getNameIDTerminalRuleCall_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ClockDeclaration__ExpressionAssignment_3_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockDeclarationAccess().getExpressionIntegerValueParserRuleCall_3_1_0()); }
	ruleIntegerValue{ after(grammarAccess.getClockDeclarationAccess().getExpressionIntegerValueParserRuleCall_3_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ClockAssignment__VariableAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockAssignmentAccess().getVariableClockDeclarationCrossReference_1_0()); }
(
{ before(grammarAccess.getClockAssignmentAccess().getVariableClockDeclarationIDTerminalRuleCall_1_0_1()); }
	RULE_ID{ after(grammarAccess.getClockAssignmentAccess().getVariableClockDeclarationIDTerminalRuleCall_1_0_1()); }
)
{ after(grammarAccess.getClockAssignmentAccess().getVariableClockDeclarationCrossReference_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ClockAssignment__ExpressionAssignment_2_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getClockAssignmentAccess().getExpressionIntegerValueParserRuleCall_2_1_0()); }
	ruleIntegerValue{ after(grammarAccess.getClockAssignmentAccess().getExpressionIntegerValueParserRuleCall_2_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ImplicationExpression__OperatorAssignment_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getImplicationExpressionAccess().getOperatorEqualsSignGreaterThanSignKeyword_1_1_0()); }
(
{ before(grammarAccess.getImplicationExpressionAccess().getOperatorEqualsSignGreaterThanSignKeyword_1_1_0()); }

	'=>' 

{ after(grammarAccess.getImplicationExpressionAccess().getOperatorEqualsSignGreaterThanSignKeyword_1_1_0()); }
)

{ after(grammarAccess.getImplicationExpressionAccess().getOperatorEqualsSignGreaterThanSignKeyword_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ImplicationExpression__RightAssignment_1_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getImplicationExpressionAccess().getRightImplicationExpressionParserRuleCall_1_2_0()); }
	ruleImplicationExpression{ after(grammarAccess.getImplicationExpressionAccess().getRightImplicationExpressionParserRuleCall_1_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__DisjunctionExpression__OperatorAssignment_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineVerticalLineKeyword_1_1_0()); }
(
{ before(grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineVerticalLineKeyword_1_1_0()); }

	'||' 

{ after(grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineVerticalLineKeyword_1_1_0()); }
)

{ after(grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineVerticalLineKeyword_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__DisjunctionExpression__RightAssignment_1_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getDisjunctionExpressionAccess().getRightDisjunctionExpressionParserRuleCall_1_2_0()); }
	ruleDisjunctionExpression{ after(grammarAccess.getDisjunctionExpressionAccess().getRightDisjunctionExpressionParserRuleCall_1_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ConjunctionExpression__OperatorAssignment_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandAmpersandKeyword_1_1_0()); }
(
{ before(grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandAmpersandKeyword_1_1_0()); }

	'&&' 

{ after(grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandAmpersandKeyword_1_1_0()); }
)

{ after(grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandAmpersandKeyword_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ConjunctionExpression__RightAssignment_1_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getConjunctionExpressionAccess().getRightConjunctionExpressionParserRuleCall_1_2_0()); }
	ruleConjunctionExpression{ after(grammarAccess.getConjunctionExpressionAccess().getRightConjunctionExpressionParserRuleCall_1_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__RelationExpression__OperatorAssignment_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRelationExpressionAccess().getOperatorAlternatives_1_1_0()); }
(rule__RelationExpression__OperatorAlternatives_1_1_0)
{ after(grammarAccess.getRelationExpressionAccess().getOperatorAlternatives_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__RelationExpression__RightAssignment_1_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRelationExpressionAccess().getRightRelationExpressionParserRuleCall_1_2_0()); }
	ruleRelationExpression{ after(grammarAccess.getRelationExpressionAccess().getRightRelationExpressionParserRuleCall_1_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}




rule__AdditionExpression__OperatorAssignment_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAdditionExpressionAccess().getOperatorAlternatives_1_1_0()); }
(rule__AdditionExpression__OperatorAlternatives_1_1_0)
{ after(grammarAccess.getAdditionExpressionAccess().getOperatorAlternatives_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__AdditionExpression__RightAssignment_1_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getAdditionExpressionAccess().getRightAdditionExpressionParserRuleCall_1_2_0()); }
	ruleAdditionExpression{ after(grammarAccess.getAdditionExpressionAccess().getRightAdditionExpressionParserRuleCall_1_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__MultiplicationExpression__OperatorAssignment_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getMultiplicationExpressionAccess().getOperatorAlternatives_1_1_0()); }
(rule__MultiplicationExpression__OperatorAlternatives_1_1_0)
{ after(grammarAccess.getMultiplicationExpressionAccess().getOperatorAlternatives_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__MultiplicationExpression__RightAssignment_1_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getMultiplicationExpressionAccess().getRightMultiplicationExpressionParserRuleCall_1_2_0()); }
	ruleMultiplicationExpression{ after(grammarAccess.getMultiplicationExpressionAccess().getRightMultiplicationExpressionParserRuleCall_1_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__NegatedExpression__OperatorAssignment_0_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getNegatedExpressionAccess().getOperatorAlternatives_0_1_0()); }
(rule__NegatedExpression__OperatorAlternatives_0_1_0)
{ after(grammarAccess.getNegatedExpressionAccess().getOperatorAlternatives_0_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__NegatedExpression__OperandAssignment_0_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getNegatedExpressionAccess().getOperandBasicExpressionParserRuleCall_0_2_0()); }
	ruleBasicExpression{ after(grammarAccess.getNegatedExpressionAccess().getOperandBasicExpressionParserRuleCall_0_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__IntegerValue__ValueAssignment
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getIntegerValueAccess().getValueAlternatives_0()); }
(rule__IntegerValue__ValueAlternatives_0)
{ after(grammarAccess.getIntegerValueAccess().getValueAlternatives_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__BooleanValue__ValueAssignment
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getBooleanValueAccess().getValueBOOLTerminalRuleCall_0()); }
	RULE_BOOL{ after(grammarAccess.getBooleanValueAccess().getValueBOOLTerminalRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__StringValue__ValueAssignment
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); }
	RULE_STRING{ after(grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__EnumValue__TypeAssignment_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getEnumValueAccess().getTypeEEnumCrossReference_0_0()); }
(
{ before(grammarAccess.getEnumValueAccess().getTypeEEnumIDTerminalRuleCall_0_0_1()); }
	RULE_ID{ after(grammarAccess.getEnumValueAccess().getTypeEEnumIDTerminalRuleCall_0_0_1()); }
)
{ after(grammarAccess.getEnumValueAccess().getTypeEEnumCrossReference_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__EnumValue__ValueAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getEnumValueAccess().getValueEEnumLiteralCrossReference_2_0()); }
(
{ before(grammarAccess.getEnumValueAccess().getValueEEnumLiteralIDTerminalRuleCall_2_0_1()); }
	RULE_ID{ after(grammarAccess.getEnumValueAccess().getValueEEnumLiteralIDTerminalRuleCall_2_0_1()); }
)
{ after(grammarAccess.getEnumValueAccess().getValueEEnumLiteralCrossReference_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__VariableValue__ValueAssignment
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVariableValueAccess().getValueVariableCrossReference_0()); }
(
{ before(grammarAccess.getVariableValueAccess().getValueVariableIDTerminalRuleCall_0_1()); }
	RULE_ID{ after(grammarAccess.getVariableValueAccess().getValueVariableIDTerminalRuleCall_0_1()); }
)
{ after(grammarAccess.getVariableValueAccess().getValueVariableCrossReference_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__CollectionAccess__CollectionOperationAssignment_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollectionAccessAccess().getCollectionOperationCollectionOperationEnumRuleCall_0_0()); }
	ruleCollectionOperation{ after(grammarAccess.getCollectionAccessAccess().getCollectionOperationCollectionOperationEnumRuleCall_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__CollectionAccess__ParameterAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCollectionAccessAccess().getParameterExpressionParserRuleCall_2_0()); }
	ruleExpression{ after(grammarAccess.getCollectionAccessAccess().getParameterExpressionParserRuleCall_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__TargetAssignment_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getTargetEObjectCrossReference_0_0()); }
(
{ before(grammarAccess.getFeatureAccessAccess().getTargetEObjectIDTerminalRuleCall_0_0_1()); }
	RULE_ID{ after(grammarAccess.getFeatureAccessAccess().getTargetEObjectIDTerminalRuleCall_0_0_1()); }
)
{ after(grammarAccess.getFeatureAccessAccess().getTargetEObjectCrossReference_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__ValueAssignment_2_0_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getValueStructuralFeatureValueParserRuleCall_2_0_0_0()); }
	ruleStructuralFeatureValue{ after(grammarAccess.getFeatureAccessAccess().getValueStructuralFeatureValueParserRuleCall_2_0_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__CollectionAccessAssignment_2_0_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getCollectionAccessCollectionAccessParserRuleCall_2_0_1_1_0()); }
	ruleCollectionAccess{ after(grammarAccess.getFeatureAccessAccess().getCollectionAccessCollectionAccessParserRuleCall_2_0_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__ValueAssignment_2_1_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getValueOperationValueParserRuleCall_2_1_0_0()); }
	ruleOperationValue{ after(grammarAccess.getFeatureAccessAccess().getValueOperationValueParserRuleCall_2_1_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__ParametersAssignment_2_1_2_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getParametersExpressionParserRuleCall_2_1_2_0_0()); }
	ruleExpression{ after(grammarAccess.getFeatureAccessAccess().getParametersExpressionParserRuleCall_2_1_2_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__FeatureAccess__ParametersAssignment_2_1_2_1_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccessAccess().getParametersExpressionParserRuleCall_2_1_2_1_1_0()); }
	ruleExpression{ after(grammarAccess.getFeatureAccessAccess().getParametersExpressionParserRuleCall_2_1_2_1_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__StructuralFeatureValue__ValueAssignment
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureCrossReference_0()); }
(
{ before(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureIDTerminalRuleCall_0_1()); }
	RULE_ID{ after(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureIDTerminalRuleCall_0_1()); }
)
{ after(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureCrossReference_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__OperationValue__ValueAssignment
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getOperationValueAccess().getValueEOperationCrossReference_0()); }
(
{ before(grammarAccess.getOperationValueAccess().getValueEOperationIDTerminalRuleCall_0_1()); }
	RULE_ID{ after(grammarAccess.getOperationValueAccess().getValueEOperationIDTerminalRuleCall_0_1()); }
)
{ after(grammarAccess.getOperationValueAccess().getValueEOperationCrossReference_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


RULE_BOOL : ('false'|'true');

RULE_SIGNEDINT : '-' RULE_INT;

RULE_DOUBLE : '-'? RULE_INT? '.' RULE_INT (('e'|'E') '-'? RULE_INT)?;

RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_INT : ('0'..'9')+;

RULE_STRING : ('"' ('\\' .|~(('\\'|'"')))* '"'|'\'' ('\\' .|~(('\\'|'\'')))* '\'');

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;


