package org.scenariotools.sml.expressions.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.scenariotools.sml.expressions.services.ScenarioExpressionsGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
@SuppressWarnings("all")
public class InternalScenarioExpressionsParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_INT", "RULE_SIGNEDINT", "RULE_ID", "RULE_STRING", "RULE_BOOL", "RULE_DOUBLE", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'=='", "'!='", "'<'", "'>'", "'<='", "'>='", "'+'", "'-'", "'*'", "'/'", "'!'", "'any'", "'contains'", "'containsAll'", "'first'", "'get'", "'isEmpty'", "'last'", "'size'", "'domain'", "'import'", "'{'", "'}'", "';'", "'='", "'var'", "'clock'", "'reset clock'", "'('", "')'", "':'", "'null'", "'.'", "','", "'=>'", "'||'", "'&&'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__14=14;
    public static final int RULE_ID=6;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=4;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=10;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_SIGNEDINT=5;
    public static final int RULE_STRING=7;
    public static final int RULE_SL_COMMENT=11;
    public static final int T__37=37;
    public static final int RULE_DOUBLE=9;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=12;
    public static final int RULE_ANY_OTHER=13;
    public static final int RULE_BOOL=8;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalScenarioExpressionsParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalScenarioExpressionsParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalScenarioExpressionsParser.tokenNames; }
    public String getGrammarFileName() { return "InternalScenarioExpressions.g"; }


     
     	private ScenarioExpressionsGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(ScenarioExpressionsGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleDocument"
    // InternalScenarioExpressions.g:70:1: entryRuleDocument : ruleDocument EOF ;
    public final void entryRuleDocument() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:71:1: ( ruleDocument EOF )
            // InternalScenarioExpressions.g:72:1: ruleDocument EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleDocument();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDocument"


    // $ANTLR start "ruleDocument"
    // InternalScenarioExpressions.g:79:1: ruleDocument : ( ( rule__Document__Group__0 ) ) ;
    public final void ruleDocument() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:83:2: ( ( ( rule__Document__Group__0 ) ) )
            // InternalScenarioExpressions.g:84:1: ( ( rule__Document__Group__0 ) )
            {
            // InternalScenarioExpressions.g:84:1: ( ( rule__Document__Group__0 ) )
            // InternalScenarioExpressions.g:85:1: ( rule__Document__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:86:1: ( rule__Document__Group__0 )
            // InternalScenarioExpressions.g:86:2: rule__Document__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Document__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDocument"


    // $ANTLR start "entryRuleImport"
    // InternalScenarioExpressions.g:98:1: entryRuleImport : ruleImport EOF ;
    public final void entryRuleImport() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:99:1: ( ruleImport EOF )
            // InternalScenarioExpressions.g:100:1: ruleImport EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleImport();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalScenarioExpressions.g:107:1: ruleImport : ( ( rule__Import__Group__0 ) ) ;
    public final void ruleImport() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:111:2: ( ( ( rule__Import__Group__0 ) ) )
            // InternalScenarioExpressions.g:112:1: ( ( rule__Import__Group__0 ) )
            {
            // InternalScenarioExpressions.g:112:1: ( ( rule__Import__Group__0 ) )
            // InternalScenarioExpressions.g:113:1: ( rule__Import__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:114:1: ( rule__Import__Group__0 )
            // InternalScenarioExpressions.g:114:2: rule__Import__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Import__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleExpressionRegion"
    // InternalScenarioExpressions.g:126:1: entryRuleExpressionRegion : ruleExpressionRegion EOF ;
    public final void entryRuleExpressionRegion() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:127:1: ( ruleExpressionRegion EOF )
            // InternalScenarioExpressions.g:128:1: ruleExpressionRegion EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleExpressionRegion();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpressionRegion"


    // $ANTLR start "ruleExpressionRegion"
    // InternalScenarioExpressions.g:135:1: ruleExpressionRegion : ( ( rule__ExpressionRegion__Group__0 ) ) ;
    public final void ruleExpressionRegion() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:139:2: ( ( ( rule__ExpressionRegion__Group__0 ) ) )
            // InternalScenarioExpressions.g:140:1: ( ( rule__ExpressionRegion__Group__0 ) )
            {
            // InternalScenarioExpressions.g:140:1: ( ( rule__ExpressionRegion__Group__0 ) )
            // InternalScenarioExpressions.g:141:1: ( rule__ExpressionRegion__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:142:1: ( rule__ExpressionRegion__Group__0 )
            // InternalScenarioExpressions.g:142:2: rule__ExpressionRegion__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpressionRegion"


    // $ANTLR start "entryRuleExpressionOrRegion"
    // InternalScenarioExpressions.g:154:1: entryRuleExpressionOrRegion : ruleExpressionOrRegion EOF ;
    public final void entryRuleExpressionOrRegion() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:155:1: ( ruleExpressionOrRegion EOF )
            // InternalScenarioExpressions.g:156:1: ruleExpressionOrRegion EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionOrRegionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleExpressionOrRegion();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionOrRegionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpressionOrRegion"


    // $ANTLR start "ruleExpressionOrRegion"
    // InternalScenarioExpressions.g:163:1: ruleExpressionOrRegion : ( ( rule__ExpressionOrRegion__Alternatives ) ) ;
    public final void ruleExpressionOrRegion() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:167:2: ( ( ( rule__ExpressionOrRegion__Alternatives ) ) )
            // InternalScenarioExpressions.g:168:1: ( ( rule__ExpressionOrRegion__Alternatives ) )
            {
            // InternalScenarioExpressions.g:168:1: ( ( rule__ExpressionOrRegion__Alternatives ) )
            // InternalScenarioExpressions.g:169:1: ( rule__ExpressionOrRegion__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionOrRegionAccess().getAlternatives()); 
            }
            // InternalScenarioExpressions.g:170:1: ( rule__ExpressionOrRegion__Alternatives )
            // InternalScenarioExpressions.g:170:2: rule__ExpressionOrRegion__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionOrRegion__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionOrRegionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpressionOrRegion"


    // $ANTLR start "entryRuleExpressionAndVariables"
    // InternalScenarioExpressions.g:182:1: entryRuleExpressionAndVariables : ruleExpressionAndVariables EOF ;
    public final void entryRuleExpressionAndVariables() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:183:1: ( ruleExpressionAndVariables EOF )
            // InternalScenarioExpressions.g:184:1: ruleExpressionAndVariables EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAndVariablesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleExpressionAndVariables();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAndVariablesRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpressionAndVariables"


    // $ANTLR start "ruleExpressionAndVariables"
    // InternalScenarioExpressions.g:191:1: ruleExpressionAndVariables : ( ( rule__ExpressionAndVariables__Alternatives ) ) ;
    public final void ruleExpressionAndVariables() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:195:2: ( ( ( rule__ExpressionAndVariables__Alternatives ) ) )
            // InternalScenarioExpressions.g:196:1: ( ( rule__ExpressionAndVariables__Alternatives ) )
            {
            // InternalScenarioExpressions.g:196:1: ( ( rule__ExpressionAndVariables__Alternatives ) )
            // InternalScenarioExpressions.g:197:1: ( rule__ExpressionAndVariables__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAndVariablesAccess().getAlternatives()); 
            }
            // InternalScenarioExpressions.g:198:1: ( rule__ExpressionAndVariables__Alternatives )
            // InternalScenarioExpressions.g:198:2: rule__ExpressionAndVariables__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionAndVariables__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAndVariablesAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpressionAndVariables"


    // $ANTLR start "entryRuleVariableExpression"
    // InternalScenarioExpressions.g:210:1: entryRuleVariableExpression : ruleVariableExpression EOF ;
    public final void entryRuleVariableExpression() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:211:1: ( ruleVariableExpression EOF )
            // InternalScenarioExpressions.g:212:1: ruleVariableExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleVariableExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariableExpression"


    // $ANTLR start "ruleVariableExpression"
    // InternalScenarioExpressions.g:219:1: ruleVariableExpression : ( ( rule__VariableExpression__Alternatives ) ) ;
    public final void ruleVariableExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:223:2: ( ( ( rule__VariableExpression__Alternatives ) ) )
            // InternalScenarioExpressions.g:224:1: ( ( rule__VariableExpression__Alternatives ) )
            {
            // InternalScenarioExpressions.g:224:1: ( ( rule__VariableExpression__Alternatives ) )
            // InternalScenarioExpressions.g:225:1: ( rule__VariableExpression__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableExpressionAccess().getAlternatives()); 
            }
            // InternalScenarioExpressions.g:226:1: ( rule__VariableExpression__Alternatives )
            // InternalScenarioExpressions.g:226:2: rule__VariableExpression__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableExpression__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableExpressionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariableExpression"


    // $ANTLR start "entryRuleVariableAssignment"
    // InternalScenarioExpressions.g:240:1: entryRuleVariableAssignment : ruleVariableAssignment EOF ;
    public final void entryRuleVariableAssignment() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:241:1: ( ruleVariableAssignment EOF )
            // InternalScenarioExpressions.g:242:1: ruleVariableAssignment EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleVariableAssignment();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariableAssignment"


    // $ANTLR start "ruleVariableAssignment"
    // InternalScenarioExpressions.g:249:1: ruleVariableAssignment : ( ( rule__VariableAssignment__Group__0 ) ) ;
    public final void ruleVariableAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:253:2: ( ( ( rule__VariableAssignment__Group__0 ) ) )
            // InternalScenarioExpressions.g:254:1: ( ( rule__VariableAssignment__Group__0 ) )
            {
            // InternalScenarioExpressions.g:254:1: ( ( rule__VariableAssignment__Group__0 ) )
            // InternalScenarioExpressions.g:255:1: ( rule__VariableAssignment__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:256:1: ( rule__VariableAssignment__Group__0 )
            // InternalScenarioExpressions.g:256:2: rule__VariableAssignment__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableAssignment__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariableAssignment"


    // $ANTLR start "entryRuleTypedVariableDeclaration"
    // InternalScenarioExpressions.g:268:1: entryRuleTypedVariableDeclaration : ruleTypedVariableDeclaration EOF ;
    public final void entryRuleTypedVariableDeclaration() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:269:1: ( ruleTypedVariableDeclaration EOF )
            // InternalScenarioExpressions.g:270:1: ruleTypedVariableDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleTypedVariableDeclaration();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypedVariableDeclaration"


    // $ANTLR start "ruleTypedVariableDeclaration"
    // InternalScenarioExpressions.g:277:1: ruleTypedVariableDeclaration : ( ( rule__TypedVariableDeclaration__Group__0 ) ) ;
    public final void ruleTypedVariableDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:281:2: ( ( ( rule__TypedVariableDeclaration__Group__0 ) ) )
            // InternalScenarioExpressions.g:282:1: ( ( rule__TypedVariableDeclaration__Group__0 ) )
            {
            // InternalScenarioExpressions.g:282:1: ( ( rule__TypedVariableDeclaration__Group__0 ) )
            // InternalScenarioExpressions.g:283:1: ( rule__TypedVariableDeclaration__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:284:1: ( rule__TypedVariableDeclaration__Group__0 )
            // InternalScenarioExpressions.g:284:2: rule__TypedVariableDeclaration__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypedVariableDeclaration"


    // $ANTLR start "entryRuleClockDeclaration"
    // InternalScenarioExpressions.g:296:1: entryRuleClockDeclaration : ruleClockDeclaration EOF ;
    public final void entryRuleClockDeclaration() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:297:1: ( ruleClockDeclaration EOF )
            // InternalScenarioExpressions.g:298:1: ruleClockDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClockDeclarationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleClockDeclaration();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClockDeclarationRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClockDeclaration"


    // $ANTLR start "ruleClockDeclaration"
    // InternalScenarioExpressions.g:305:1: ruleClockDeclaration : ( ( rule__ClockDeclaration__Group__0 ) ) ;
    public final void ruleClockDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:309:2: ( ( ( rule__ClockDeclaration__Group__0 ) ) )
            // InternalScenarioExpressions.g:310:1: ( ( rule__ClockDeclaration__Group__0 ) )
            {
            // InternalScenarioExpressions.g:310:1: ( ( rule__ClockDeclaration__Group__0 ) )
            // InternalScenarioExpressions.g:311:1: ( rule__ClockDeclaration__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClockDeclarationAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:312:1: ( rule__ClockDeclaration__Group__0 )
            // InternalScenarioExpressions.g:312:2: rule__ClockDeclaration__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ClockDeclaration__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClockDeclarationAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClockDeclaration"


    // $ANTLR start "entryRuleClockAssignment"
    // InternalScenarioExpressions.g:326:1: entryRuleClockAssignment : ruleClockAssignment EOF ;
    public final void entryRuleClockAssignment() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:327:1: ( ruleClockAssignment EOF )
            // InternalScenarioExpressions.g:328:1: ruleClockAssignment EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClockAssignmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleClockAssignment();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClockAssignmentRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClockAssignment"


    // $ANTLR start "ruleClockAssignment"
    // InternalScenarioExpressions.g:335:1: ruleClockAssignment : ( ( rule__ClockAssignment__Group__0 ) ) ;
    public final void ruleClockAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:339:2: ( ( ( rule__ClockAssignment__Group__0 ) ) )
            // InternalScenarioExpressions.g:340:1: ( ( rule__ClockAssignment__Group__0 ) )
            {
            // InternalScenarioExpressions.g:340:1: ( ( rule__ClockAssignment__Group__0 ) )
            // InternalScenarioExpressions.g:341:1: ( rule__ClockAssignment__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClockAssignmentAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:342:1: ( rule__ClockAssignment__Group__0 )
            // InternalScenarioExpressions.g:342:2: rule__ClockAssignment__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ClockAssignment__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClockAssignmentAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClockAssignment"


    // $ANTLR start "entryRuleExpression"
    // InternalScenarioExpressions.g:354:1: entryRuleExpression : ruleExpression EOF ;
    public final void entryRuleExpression() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:355:1: ( ruleExpression EOF )
            // InternalScenarioExpressions.g:356:1: ruleExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalScenarioExpressions.g:363:1: ruleExpression : ( ruleImplicationExpression ) ;
    public final void ruleExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:367:2: ( ( ruleImplicationExpression ) )
            // InternalScenarioExpressions.g:368:1: ( ruleImplicationExpression )
            {
            // InternalScenarioExpressions.g:368:1: ( ruleImplicationExpression )
            // InternalScenarioExpressions.g:369:1: ruleImplicationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAccess().getImplicationExpressionParserRuleCall()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleImplicationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAccess().getImplicationExpressionParserRuleCall()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleImplicationExpression"
    // InternalScenarioExpressions.g:382:1: entryRuleImplicationExpression : ruleImplicationExpression EOF ;
    public final void entryRuleImplicationExpression() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:383:1: ( ruleImplicationExpression EOF )
            // InternalScenarioExpressions.g:384:1: ruleImplicationExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImplicationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleImplicationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImplicationExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImplicationExpression"


    // $ANTLR start "ruleImplicationExpression"
    // InternalScenarioExpressions.g:391:1: ruleImplicationExpression : ( ( rule__ImplicationExpression__Group__0 ) ) ;
    public final void ruleImplicationExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:395:2: ( ( ( rule__ImplicationExpression__Group__0 ) ) )
            // InternalScenarioExpressions.g:396:1: ( ( rule__ImplicationExpression__Group__0 ) )
            {
            // InternalScenarioExpressions.g:396:1: ( ( rule__ImplicationExpression__Group__0 ) )
            // InternalScenarioExpressions.g:397:1: ( rule__ImplicationExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImplicationExpressionAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:398:1: ( rule__ImplicationExpression__Group__0 )
            // InternalScenarioExpressions.g:398:2: rule__ImplicationExpression__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ImplicationExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImplicationExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImplicationExpression"


    // $ANTLR start "entryRuleDisjunctionExpression"
    // InternalScenarioExpressions.g:410:1: entryRuleDisjunctionExpression : ruleDisjunctionExpression EOF ;
    public final void entryRuleDisjunctionExpression() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:411:1: ( ruleDisjunctionExpression EOF )
            // InternalScenarioExpressions.g:412:1: ruleDisjunctionExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDisjunctionExpression"


    // $ANTLR start "ruleDisjunctionExpression"
    // InternalScenarioExpressions.g:419:1: ruleDisjunctionExpression : ( ( rule__DisjunctionExpression__Group__0 ) ) ;
    public final void ruleDisjunctionExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:423:2: ( ( ( rule__DisjunctionExpression__Group__0 ) ) )
            // InternalScenarioExpressions.g:424:1: ( ( rule__DisjunctionExpression__Group__0 ) )
            {
            // InternalScenarioExpressions.g:424:1: ( ( rule__DisjunctionExpression__Group__0 ) )
            // InternalScenarioExpressions.g:425:1: ( rule__DisjunctionExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:426:1: ( rule__DisjunctionExpression__Group__0 )
            // InternalScenarioExpressions.g:426:2: rule__DisjunctionExpression__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDisjunctionExpression"


    // $ANTLR start "entryRuleConjunctionExpression"
    // InternalScenarioExpressions.g:438:1: entryRuleConjunctionExpression : ruleConjunctionExpression EOF ;
    public final void entryRuleConjunctionExpression() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:439:1: ( ruleConjunctionExpression EOF )
            // InternalScenarioExpressions.g:440:1: ruleConjunctionExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConjunctionExpression"


    // $ANTLR start "ruleConjunctionExpression"
    // InternalScenarioExpressions.g:447:1: ruleConjunctionExpression : ( ( rule__ConjunctionExpression__Group__0 ) ) ;
    public final void ruleConjunctionExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:451:2: ( ( ( rule__ConjunctionExpression__Group__0 ) ) )
            // InternalScenarioExpressions.g:452:1: ( ( rule__ConjunctionExpression__Group__0 ) )
            {
            // InternalScenarioExpressions.g:452:1: ( ( rule__ConjunctionExpression__Group__0 ) )
            // InternalScenarioExpressions.g:453:1: ( rule__ConjunctionExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:454:1: ( rule__ConjunctionExpression__Group__0 )
            // InternalScenarioExpressions.g:454:2: rule__ConjunctionExpression__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConjunctionExpression"


    // $ANTLR start "entryRuleRelationExpression"
    // InternalScenarioExpressions.g:466:1: entryRuleRelationExpression : ruleRelationExpression EOF ;
    public final void entryRuleRelationExpression() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:467:1: ( ruleRelationExpression EOF )
            // InternalScenarioExpressions.g:468:1: ruleRelationExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleRelationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRelationExpression"


    // $ANTLR start "ruleRelationExpression"
    // InternalScenarioExpressions.g:475:1: ruleRelationExpression : ( ( rule__RelationExpression__Group__0 ) ) ;
    public final void ruleRelationExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:479:2: ( ( ( rule__RelationExpression__Group__0 ) ) )
            // InternalScenarioExpressions.g:480:1: ( ( rule__RelationExpression__Group__0 ) )
            {
            // InternalScenarioExpressions.g:480:1: ( ( rule__RelationExpression__Group__0 ) )
            // InternalScenarioExpressions.g:481:1: ( rule__RelationExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:482:1: ( rule__RelationExpression__Group__0 )
            // InternalScenarioExpressions.g:482:2: rule__RelationExpression__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRelationExpression"


    // $ANTLR start "entryRuleAdditionExpression"
    // InternalScenarioExpressions.g:496:1: entryRuleAdditionExpression : ruleAdditionExpression EOF ;
    public final void entryRuleAdditionExpression() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:497:1: ( ruleAdditionExpression EOF )
            // InternalScenarioExpressions.g:498:1: ruleAdditionExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAdditionExpression"


    // $ANTLR start "ruleAdditionExpression"
    // InternalScenarioExpressions.g:505:1: ruleAdditionExpression : ( ( rule__AdditionExpression__Group__0 ) ) ;
    public final void ruleAdditionExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:509:2: ( ( ( rule__AdditionExpression__Group__0 ) ) )
            // InternalScenarioExpressions.g:510:1: ( ( rule__AdditionExpression__Group__0 ) )
            {
            // InternalScenarioExpressions.g:510:1: ( ( rule__AdditionExpression__Group__0 ) )
            // InternalScenarioExpressions.g:511:1: ( rule__AdditionExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:512:1: ( rule__AdditionExpression__Group__0 )
            // InternalScenarioExpressions.g:512:2: rule__AdditionExpression__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAdditionExpression"


    // $ANTLR start "entryRuleMultiplicationExpression"
    // InternalScenarioExpressions.g:524:1: entryRuleMultiplicationExpression : ruleMultiplicationExpression EOF ;
    public final void entryRuleMultiplicationExpression() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:525:1: ( ruleMultiplicationExpression EOF )
            // InternalScenarioExpressions.g:526:1: ruleMultiplicationExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMultiplicationExpression"


    // $ANTLR start "ruleMultiplicationExpression"
    // InternalScenarioExpressions.g:533:1: ruleMultiplicationExpression : ( ( rule__MultiplicationExpression__Group__0 ) ) ;
    public final void ruleMultiplicationExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:537:2: ( ( ( rule__MultiplicationExpression__Group__0 ) ) )
            // InternalScenarioExpressions.g:538:1: ( ( rule__MultiplicationExpression__Group__0 ) )
            {
            // InternalScenarioExpressions.g:538:1: ( ( rule__MultiplicationExpression__Group__0 ) )
            // InternalScenarioExpressions.g:539:1: ( rule__MultiplicationExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:540:1: ( rule__MultiplicationExpression__Group__0 )
            // InternalScenarioExpressions.g:540:2: rule__MultiplicationExpression__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMultiplicationExpression"


    // $ANTLR start "entryRuleNegatedExpression"
    // InternalScenarioExpressions.g:552:1: entryRuleNegatedExpression : ruleNegatedExpression EOF ;
    public final void entryRuleNegatedExpression() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:553:1: ( ruleNegatedExpression EOF )
            // InternalScenarioExpressions.g:554:1: ruleNegatedExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleNegatedExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNegatedExpression"


    // $ANTLR start "ruleNegatedExpression"
    // InternalScenarioExpressions.g:561:1: ruleNegatedExpression : ( ( rule__NegatedExpression__Alternatives ) ) ;
    public final void ruleNegatedExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:565:2: ( ( ( rule__NegatedExpression__Alternatives ) ) )
            // InternalScenarioExpressions.g:566:1: ( ( rule__NegatedExpression__Alternatives ) )
            {
            // InternalScenarioExpressions.g:566:1: ( ( rule__NegatedExpression__Alternatives ) )
            // InternalScenarioExpressions.g:567:1: ( rule__NegatedExpression__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionAccess().getAlternatives()); 
            }
            // InternalScenarioExpressions.g:568:1: ( rule__NegatedExpression__Alternatives )
            // InternalScenarioExpressions.g:568:2: rule__NegatedExpression__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNegatedExpression"


    // $ANTLR start "entryRuleBasicExpression"
    // InternalScenarioExpressions.g:580:1: entryRuleBasicExpression : ruleBasicExpression EOF ;
    public final void entryRuleBasicExpression() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:581:1: ( ruleBasicExpression EOF )
            // InternalScenarioExpressions.g:582:1: ruleBasicExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleBasicExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBasicExpression"


    // $ANTLR start "ruleBasicExpression"
    // InternalScenarioExpressions.g:589:1: ruleBasicExpression : ( ( rule__BasicExpression__Alternatives ) ) ;
    public final void ruleBasicExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:593:2: ( ( ( rule__BasicExpression__Alternatives ) ) )
            // InternalScenarioExpressions.g:594:1: ( ( rule__BasicExpression__Alternatives ) )
            {
            // InternalScenarioExpressions.g:594:1: ( ( rule__BasicExpression__Alternatives ) )
            // InternalScenarioExpressions.g:595:1: ( rule__BasicExpression__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicExpressionAccess().getAlternatives()); 
            }
            // InternalScenarioExpressions.g:596:1: ( rule__BasicExpression__Alternatives )
            // InternalScenarioExpressions.g:596:2: rule__BasicExpression__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__BasicExpression__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicExpressionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBasicExpression"


    // $ANTLR start "entryRuleValue"
    // InternalScenarioExpressions.g:608:1: entryRuleValue : ruleValue EOF ;
    public final void entryRuleValue() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:609:1: ( ruleValue EOF )
            // InternalScenarioExpressions.g:610:1: ruleValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleValue"


    // $ANTLR start "ruleValue"
    // InternalScenarioExpressions.g:617:1: ruleValue : ( ( rule__Value__Alternatives ) ) ;
    public final void ruleValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:621:2: ( ( ( rule__Value__Alternatives ) ) )
            // InternalScenarioExpressions.g:622:1: ( ( rule__Value__Alternatives ) )
            {
            // InternalScenarioExpressions.g:622:1: ( ( rule__Value__Alternatives ) )
            // InternalScenarioExpressions.g:623:1: ( rule__Value__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getValueAccess().getAlternatives()); 
            }
            // InternalScenarioExpressions.g:624:1: ( rule__Value__Alternatives )
            // InternalScenarioExpressions.g:624:2: rule__Value__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Value__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getValueAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleValue"


    // $ANTLR start "entryRuleIntegerValue"
    // InternalScenarioExpressions.g:636:1: entryRuleIntegerValue : ruleIntegerValue EOF ;
    public final void entryRuleIntegerValue() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:637:1: ( ruleIntegerValue EOF )
            // InternalScenarioExpressions.g:638:1: ruleIntegerValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIntegerValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleIntegerValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getIntegerValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntegerValue"


    // $ANTLR start "ruleIntegerValue"
    // InternalScenarioExpressions.g:645:1: ruleIntegerValue : ( ( rule__IntegerValue__ValueAssignment ) ) ;
    public final void ruleIntegerValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:649:2: ( ( ( rule__IntegerValue__ValueAssignment ) ) )
            // InternalScenarioExpressions.g:650:1: ( ( rule__IntegerValue__ValueAssignment ) )
            {
            // InternalScenarioExpressions.g:650:1: ( ( rule__IntegerValue__ValueAssignment ) )
            // InternalScenarioExpressions.g:651:1: ( rule__IntegerValue__ValueAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIntegerValueAccess().getValueAssignment()); 
            }
            // InternalScenarioExpressions.g:652:1: ( rule__IntegerValue__ValueAssignment )
            // InternalScenarioExpressions.g:652:2: rule__IntegerValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__IntegerValue__ValueAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getIntegerValueAccess().getValueAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntegerValue"


    // $ANTLR start "entryRuleBooleanValue"
    // InternalScenarioExpressions.g:664:1: entryRuleBooleanValue : ruleBooleanValue EOF ;
    public final void entryRuleBooleanValue() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:665:1: ( ruleBooleanValue EOF )
            // InternalScenarioExpressions.g:666:1: ruleBooleanValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBooleanValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleBooleanValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBooleanValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBooleanValue"


    // $ANTLR start "ruleBooleanValue"
    // InternalScenarioExpressions.g:673:1: ruleBooleanValue : ( ( rule__BooleanValue__ValueAssignment ) ) ;
    public final void ruleBooleanValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:677:2: ( ( ( rule__BooleanValue__ValueAssignment ) ) )
            // InternalScenarioExpressions.g:678:1: ( ( rule__BooleanValue__ValueAssignment ) )
            {
            // InternalScenarioExpressions.g:678:1: ( ( rule__BooleanValue__ValueAssignment ) )
            // InternalScenarioExpressions.g:679:1: ( rule__BooleanValue__ValueAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBooleanValueAccess().getValueAssignment()); 
            }
            // InternalScenarioExpressions.g:680:1: ( rule__BooleanValue__ValueAssignment )
            // InternalScenarioExpressions.g:680:2: rule__BooleanValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__BooleanValue__ValueAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBooleanValueAccess().getValueAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooleanValue"


    // $ANTLR start "entryRuleStringValue"
    // InternalScenarioExpressions.g:692:1: entryRuleStringValue : ruleStringValue EOF ;
    public final void entryRuleStringValue() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:693:1: ( ruleStringValue EOF )
            // InternalScenarioExpressions.g:694:1: ruleStringValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStringValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleStringValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStringValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStringValue"


    // $ANTLR start "ruleStringValue"
    // InternalScenarioExpressions.g:701:1: ruleStringValue : ( ( rule__StringValue__ValueAssignment ) ) ;
    public final void ruleStringValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:705:2: ( ( ( rule__StringValue__ValueAssignment ) ) )
            // InternalScenarioExpressions.g:706:1: ( ( rule__StringValue__ValueAssignment ) )
            {
            // InternalScenarioExpressions.g:706:1: ( ( rule__StringValue__ValueAssignment ) )
            // InternalScenarioExpressions.g:707:1: ( rule__StringValue__ValueAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStringValueAccess().getValueAssignment()); 
            }
            // InternalScenarioExpressions.g:708:1: ( rule__StringValue__ValueAssignment )
            // InternalScenarioExpressions.g:708:2: rule__StringValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__StringValue__ValueAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getStringValueAccess().getValueAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStringValue"


    // $ANTLR start "entryRuleEnumValue"
    // InternalScenarioExpressions.g:720:1: entryRuleEnumValue : ruleEnumValue EOF ;
    public final void entryRuleEnumValue() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:721:1: ( ruleEnumValue EOF )
            // InternalScenarioExpressions.g:722:1: ruleEnumValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleEnumValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEnumValue"


    // $ANTLR start "ruleEnumValue"
    // InternalScenarioExpressions.g:729:1: ruleEnumValue : ( ( rule__EnumValue__Group__0 ) ) ;
    public final void ruleEnumValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:733:2: ( ( ( rule__EnumValue__Group__0 ) ) )
            // InternalScenarioExpressions.g:734:1: ( ( rule__EnumValue__Group__0 ) )
            {
            // InternalScenarioExpressions.g:734:1: ( ( rule__EnumValue__Group__0 ) )
            // InternalScenarioExpressions.g:735:1: ( rule__EnumValue__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:736:1: ( rule__EnumValue__Group__0 )
            // InternalScenarioExpressions.g:736:2: rule__EnumValue__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__EnumValue__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEnumValue"


    // $ANTLR start "entryRuleNullValue"
    // InternalScenarioExpressions.g:748:1: entryRuleNullValue : ruleNullValue EOF ;
    public final void entryRuleNullValue() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:749:1: ( ruleNullValue EOF )
            // InternalScenarioExpressions.g:750:1: ruleNullValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNullValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleNullValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNullValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNullValue"


    // $ANTLR start "ruleNullValue"
    // InternalScenarioExpressions.g:757:1: ruleNullValue : ( ( rule__NullValue__Group__0 ) ) ;
    public final void ruleNullValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:761:2: ( ( ( rule__NullValue__Group__0 ) ) )
            // InternalScenarioExpressions.g:762:1: ( ( rule__NullValue__Group__0 ) )
            {
            // InternalScenarioExpressions.g:762:1: ( ( rule__NullValue__Group__0 ) )
            // InternalScenarioExpressions.g:763:1: ( rule__NullValue__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNullValueAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:764:1: ( rule__NullValue__Group__0 )
            // InternalScenarioExpressions.g:764:2: rule__NullValue__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NullValue__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNullValueAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNullValue"


    // $ANTLR start "entryRuleVariableValue"
    // InternalScenarioExpressions.g:776:1: entryRuleVariableValue : ruleVariableValue EOF ;
    public final void entryRuleVariableValue() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:777:1: ( ruleVariableValue EOF )
            // InternalScenarioExpressions.g:778:1: ruleVariableValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleVariableValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariableValue"


    // $ANTLR start "ruleVariableValue"
    // InternalScenarioExpressions.g:785:1: ruleVariableValue : ( ( rule__VariableValue__ValueAssignment ) ) ;
    public final void ruleVariableValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:789:2: ( ( ( rule__VariableValue__ValueAssignment ) ) )
            // InternalScenarioExpressions.g:790:1: ( ( rule__VariableValue__ValueAssignment ) )
            {
            // InternalScenarioExpressions.g:790:1: ( ( rule__VariableValue__ValueAssignment ) )
            // InternalScenarioExpressions.g:791:1: ( rule__VariableValue__ValueAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableValueAccess().getValueAssignment()); 
            }
            // InternalScenarioExpressions.g:792:1: ( rule__VariableValue__ValueAssignment )
            // InternalScenarioExpressions.g:792:2: rule__VariableValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableValue__ValueAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableValueAccess().getValueAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariableValue"


    // $ANTLR start "entryRuleCollectionAccess"
    // InternalScenarioExpressions.g:804:1: entryRuleCollectionAccess : ruleCollectionAccess EOF ;
    public final void entryRuleCollectionAccess() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:805:1: ( ruleCollectionAccess EOF )
            // InternalScenarioExpressions.g:806:1: ruleCollectionAccess EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleCollectionAccess();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCollectionAccess"


    // $ANTLR start "ruleCollectionAccess"
    // InternalScenarioExpressions.g:813:1: ruleCollectionAccess : ( ( rule__CollectionAccess__Group__0 ) ) ;
    public final void ruleCollectionAccess() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:817:2: ( ( ( rule__CollectionAccess__Group__0 ) ) )
            // InternalScenarioExpressions.g:818:1: ( ( rule__CollectionAccess__Group__0 ) )
            {
            // InternalScenarioExpressions.g:818:1: ( ( rule__CollectionAccess__Group__0 ) )
            // InternalScenarioExpressions.g:819:1: ( rule__CollectionAccess__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:820:1: ( rule__CollectionAccess__Group__0 )
            // InternalScenarioExpressions.g:820:2: rule__CollectionAccess__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionAccess__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCollectionAccess"


    // $ANTLR start "entryRuleFeatureAccess"
    // InternalScenarioExpressions.g:832:1: entryRuleFeatureAccess : ruleFeatureAccess EOF ;
    public final void entryRuleFeatureAccess() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:833:1: ( ruleFeatureAccess EOF )
            // InternalScenarioExpressions.g:834:1: ruleFeatureAccess EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleFeatureAccess();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeatureAccess"


    // $ANTLR start "ruleFeatureAccess"
    // InternalScenarioExpressions.g:841:1: ruleFeatureAccess : ( ( rule__FeatureAccess__Group__0 ) ) ;
    public final void ruleFeatureAccess() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:845:2: ( ( ( rule__FeatureAccess__Group__0 ) ) )
            // InternalScenarioExpressions.g:846:1: ( ( rule__FeatureAccess__Group__0 ) )
            {
            // InternalScenarioExpressions.g:846:1: ( ( rule__FeatureAccess__Group__0 ) )
            // InternalScenarioExpressions.g:847:1: ( rule__FeatureAccess__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:848:1: ( rule__FeatureAccess__Group__0 )
            // InternalScenarioExpressions.g:848:2: rule__FeatureAccess__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeatureAccess"


    // $ANTLR start "entryRuleStructuralFeatureValue"
    // InternalScenarioExpressions.g:860:1: entryRuleStructuralFeatureValue : ruleStructuralFeatureValue EOF ;
    public final void entryRuleStructuralFeatureValue() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:861:1: ( ruleStructuralFeatureValue EOF )
            // InternalScenarioExpressions.g:862:1: ruleStructuralFeatureValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStructuralFeatureValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleStructuralFeatureValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStructuralFeatureValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStructuralFeatureValue"


    // $ANTLR start "ruleStructuralFeatureValue"
    // InternalScenarioExpressions.g:869:1: ruleStructuralFeatureValue : ( ( rule__StructuralFeatureValue__ValueAssignment ) ) ;
    public final void ruleStructuralFeatureValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:873:2: ( ( ( rule__StructuralFeatureValue__ValueAssignment ) ) )
            // InternalScenarioExpressions.g:874:1: ( ( rule__StructuralFeatureValue__ValueAssignment ) )
            {
            // InternalScenarioExpressions.g:874:1: ( ( rule__StructuralFeatureValue__ValueAssignment ) )
            // InternalScenarioExpressions.g:875:1: ( rule__StructuralFeatureValue__ValueAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStructuralFeatureValueAccess().getValueAssignment()); 
            }
            // InternalScenarioExpressions.g:876:1: ( rule__StructuralFeatureValue__ValueAssignment )
            // InternalScenarioExpressions.g:876:2: rule__StructuralFeatureValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__StructuralFeatureValue__ValueAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getStructuralFeatureValueAccess().getValueAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStructuralFeatureValue"


    // $ANTLR start "entryRuleOperationValue"
    // InternalScenarioExpressions.g:888:1: entryRuleOperationValue : ruleOperationValue EOF ;
    public final void entryRuleOperationValue() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:889:1: ( ruleOperationValue EOF )
            // InternalScenarioExpressions.g:890:1: ruleOperationValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOperationValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleOperationValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getOperationValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOperationValue"


    // $ANTLR start "ruleOperationValue"
    // InternalScenarioExpressions.g:897:1: ruleOperationValue : ( ( rule__OperationValue__ValueAssignment ) ) ;
    public final void ruleOperationValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:901:2: ( ( ( rule__OperationValue__ValueAssignment ) ) )
            // InternalScenarioExpressions.g:902:1: ( ( rule__OperationValue__ValueAssignment ) )
            {
            // InternalScenarioExpressions.g:902:1: ( ( rule__OperationValue__ValueAssignment ) )
            // InternalScenarioExpressions.g:903:1: ( rule__OperationValue__ValueAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOperationValueAccess().getValueAssignment()); 
            }
            // InternalScenarioExpressions.g:904:1: ( rule__OperationValue__ValueAssignment )
            // InternalScenarioExpressions.g:904:2: rule__OperationValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__OperationValue__ValueAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getOperationValueAccess().getValueAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOperationValue"


    // $ANTLR start "ruleCollectionOperation"
    // InternalScenarioExpressions.g:917:1: ruleCollectionOperation : ( ( rule__CollectionOperation__Alternatives ) ) ;
    public final void ruleCollectionOperation() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:921:1: ( ( ( rule__CollectionOperation__Alternatives ) ) )
            // InternalScenarioExpressions.g:922:1: ( ( rule__CollectionOperation__Alternatives ) )
            {
            // InternalScenarioExpressions.g:922:1: ( ( rule__CollectionOperation__Alternatives ) )
            // InternalScenarioExpressions.g:923:1: ( rule__CollectionOperation__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionOperationAccess().getAlternatives()); 
            }
            // InternalScenarioExpressions.g:924:1: ( rule__CollectionOperation__Alternatives )
            // InternalScenarioExpressions.g:924:2: rule__CollectionOperation__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionOperation__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionOperationAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCollectionOperation"


    // $ANTLR start "rule__ExpressionOrRegion__Alternatives"
    // InternalScenarioExpressions.g:937:1: rule__ExpressionOrRegion__Alternatives : ( ( ruleExpressionRegion ) | ( ruleExpressionAndVariables ) );
    public final void rule__ExpressionOrRegion__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:941:1: ( ( ruleExpressionRegion ) | ( ruleExpressionAndVariables ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==35) ) {
                alt1=1;
            }
            else if ( ((LA1_0>=RULE_INT && LA1_0<=RULE_BOOL)||LA1_0==21||LA1_0==24||(LA1_0>=39 && LA1_0<=42)||LA1_0==45) ) {
                alt1=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalScenarioExpressions.g:942:1: ( ruleExpressionRegion )
                    {
                    // InternalScenarioExpressions.g:942:1: ( ruleExpressionRegion )
                    // InternalScenarioExpressions.g:943:1: ruleExpressionRegion
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getExpressionOrRegionAccess().getExpressionRegionParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleExpressionRegion();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getExpressionOrRegionAccess().getExpressionRegionParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:948:6: ( ruleExpressionAndVariables )
                    {
                    // InternalScenarioExpressions.g:948:6: ( ruleExpressionAndVariables )
                    // InternalScenarioExpressions.g:949:1: ruleExpressionAndVariables
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getExpressionOrRegionAccess().getExpressionAndVariablesParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleExpressionAndVariables();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getExpressionOrRegionAccess().getExpressionAndVariablesParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionOrRegion__Alternatives"


    // $ANTLR start "rule__ExpressionAndVariables__Alternatives"
    // InternalScenarioExpressions.g:959:1: rule__ExpressionAndVariables__Alternatives : ( ( ruleVariableExpression ) | ( ruleExpression ) );
    public final void rule__ExpressionAndVariables__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:963:1: ( ( ruleVariableExpression ) | ( ruleExpression ) )
            int alt2=2;
            switch ( input.LA(1) ) {
            case 39:
            case 40:
            case 41:
                {
                alt2=1;
                }
                break;
            case RULE_ID:
                {
                int LA2_2 = input.LA(2);

                if ( (LA2_2==EOF||(LA2_2>=14 && LA2_2<=23)||LA2_2==37||LA2_2==44||LA2_2==46||(LA2_2>=48 && LA2_2<=50)) ) {
                    alt2=2;
                }
                else if ( (LA2_2==38) ) {
                    alt2=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_INT:
            case RULE_SIGNEDINT:
            case RULE_STRING:
            case RULE_BOOL:
            case 21:
            case 24:
            case 42:
            case 45:
                {
                alt2=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalScenarioExpressions.g:964:1: ( ruleVariableExpression )
                    {
                    // InternalScenarioExpressions.g:964:1: ( ruleVariableExpression )
                    // InternalScenarioExpressions.g:965:1: ruleVariableExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getExpressionAndVariablesAccess().getVariableExpressionParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleVariableExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getExpressionAndVariablesAccess().getVariableExpressionParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:970:6: ( ruleExpression )
                    {
                    // InternalScenarioExpressions.g:970:6: ( ruleExpression )
                    // InternalScenarioExpressions.g:971:1: ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getExpressionAndVariablesAccess().getExpressionParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getExpressionAndVariablesAccess().getExpressionParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionAndVariables__Alternatives"


    // $ANTLR start "rule__VariableExpression__Alternatives"
    // InternalScenarioExpressions.g:981:1: rule__VariableExpression__Alternatives : ( ( ruleTypedVariableDeclaration ) | ( ruleVariableAssignment ) | ( ruleClockDeclaration ) | ( ruleClockAssignment ) );
    public final void rule__VariableExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:985:1: ( ( ruleTypedVariableDeclaration ) | ( ruleVariableAssignment ) | ( ruleClockDeclaration ) | ( ruleClockAssignment ) )
            int alt3=4;
            switch ( input.LA(1) ) {
            case 39:
                {
                alt3=1;
                }
                break;
            case RULE_ID:
                {
                alt3=2;
                }
                break;
            case 40:
                {
                alt3=3;
                }
                break;
            case 41:
                {
                alt3=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalScenarioExpressions.g:986:1: ( ruleTypedVariableDeclaration )
                    {
                    // InternalScenarioExpressions.g:986:1: ( ruleTypedVariableDeclaration )
                    // InternalScenarioExpressions.g:987:1: ruleTypedVariableDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getVariableExpressionAccess().getTypedVariableDeclarationParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleTypedVariableDeclaration();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getVariableExpressionAccess().getTypedVariableDeclarationParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:992:6: ( ruleVariableAssignment )
                    {
                    // InternalScenarioExpressions.g:992:6: ( ruleVariableAssignment )
                    // InternalScenarioExpressions.g:993:1: ruleVariableAssignment
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getVariableExpressionAccess().getVariableAssignmentParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleVariableAssignment();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getVariableExpressionAccess().getVariableAssignmentParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalScenarioExpressions.g:998:6: ( ruleClockDeclaration )
                    {
                    // InternalScenarioExpressions.g:998:6: ( ruleClockDeclaration )
                    // InternalScenarioExpressions.g:999:1: ruleClockDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getVariableExpressionAccess().getClockDeclarationParserRuleCall_2()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleClockDeclaration();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getVariableExpressionAccess().getClockDeclarationParserRuleCall_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalScenarioExpressions.g:1004:6: ( ruleClockAssignment )
                    {
                    // InternalScenarioExpressions.g:1004:6: ( ruleClockAssignment )
                    // InternalScenarioExpressions.g:1005:1: ruleClockAssignment
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getVariableExpressionAccess().getClockAssignmentParserRuleCall_3()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleClockAssignment();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getVariableExpressionAccess().getClockAssignmentParserRuleCall_3()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableExpression__Alternatives"


    // $ANTLR start "rule__RelationExpression__OperatorAlternatives_1_1_0"
    // InternalScenarioExpressions.g:1015:1: rule__RelationExpression__OperatorAlternatives_1_1_0 : ( ( '==' ) | ( '!=' ) | ( '<' ) | ( '>' ) | ( '<=' ) | ( '>=' ) );
    public final void rule__RelationExpression__OperatorAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1019:1: ( ( '==' ) | ( '!=' ) | ( '<' ) | ( '>' ) | ( '<=' ) | ( '>=' ) )
            int alt4=6;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt4=1;
                }
                break;
            case 15:
                {
                alt4=2;
                }
                break;
            case 16:
                {
                alt4=3;
                }
                break;
            case 17:
                {
                alt4=4;
                }
                break;
            case 18:
                {
                alt4=5;
                }
                break;
            case 19:
                {
                alt4=6;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalScenarioExpressions.g:1020:1: ( '==' )
                    {
                    // InternalScenarioExpressions.g:1020:1: ( '==' )
                    // InternalScenarioExpressions.g:1021:1: '=='
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRelationExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_1_0_0()); 
                    }
                    match(input,14,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRelationExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_1_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1028:6: ( '!=' )
                    {
                    // InternalScenarioExpressions.g:1028:6: ( '!=' )
                    // InternalScenarioExpressions.g:1029:1: '!='
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRelationExpressionAccess().getOperatorExclamationMarkEqualsSignKeyword_1_1_0_1()); 
                    }
                    match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRelationExpressionAccess().getOperatorExclamationMarkEqualsSignKeyword_1_1_0_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalScenarioExpressions.g:1036:6: ( '<' )
                    {
                    // InternalScenarioExpressions.g:1036:6: ( '<' )
                    // InternalScenarioExpressions.g:1037:1: '<'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignKeyword_1_1_0_2()); 
                    }
                    match(input,16,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignKeyword_1_1_0_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalScenarioExpressions.g:1044:6: ( '>' )
                    {
                    // InternalScenarioExpressions.g:1044:6: ( '>' )
                    // InternalScenarioExpressions.g:1045:1: '>'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignKeyword_1_1_0_3()); 
                    }
                    match(input,17,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignKeyword_1_1_0_3()); 
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalScenarioExpressions.g:1052:6: ( '<=' )
                    {
                    // InternalScenarioExpressions.g:1052:6: ( '<=' )
                    // InternalScenarioExpressions.g:1053:1: '<='
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_1_0_4()); 
                    }
                    match(input,18,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_1_0_4()); 
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalScenarioExpressions.g:1060:6: ( '>=' )
                    {
                    // InternalScenarioExpressions.g:1060:6: ( '>=' )
                    // InternalScenarioExpressions.g:1061:1: '>='
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_1_0_5()); 
                    }
                    match(input,19,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_1_0_5()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__OperatorAlternatives_1_1_0"


    // $ANTLR start "rule__AdditionExpression__OperatorAlternatives_1_1_0"
    // InternalScenarioExpressions.g:1074:1: rule__AdditionExpression__OperatorAlternatives_1_1_0 : ( ( '+' ) | ( '-' ) );
    public final void rule__AdditionExpression__OperatorAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1078:1: ( ( '+' ) | ( '-' ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==20) ) {
                alt5=1;
            }
            else if ( (LA5_0==21) ) {
                alt5=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalScenarioExpressions.g:1079:1: ( '+' )
                    {
                    // InternalScenarioExpressions.g:1079:1: ( '+' )
                    // InternalScenarioExpressions.g:1080:1: '+'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getAdditionExpressionAccess().getOperatorPlusSignKeyword_1_1_0_0()); 
                    }
                    match(input,20,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getAdditionExpressionAccess().getOperatorPlusSignKeyword_1_1_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1087:6: ( '-' )
                    {
                    // InternalScenarioExpressions.g:1087:6: ( '-' )
                    // InternalScenarioExpressions.g:1088:1: '-'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getAdditionExpressionAccess().getOperatorHyphenMinusKeyword_1_1_0_1()); 
                    }
                    match(input,21,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getAdditionExpressionAccess().getOperatorHyphenMinusKeyword_1_1_0_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__OperatorAlternatives_1_1_0"


    // $ANTLR start "rule__MultiplicationExpression__OperatorAlternatives_1_1_0"
    // InternalScenarioExpressions.g:1100:1: rule__MultiplicationExpression__OperatorAlternatives_1_1_0 : ( ( '*' ) | ( '/' ) );
    public final void rule__MultiplicationExpression__OperatorAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1104:1: ( ( '*' ) | ( '/' ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==22) ) {
                alt6=1;
            }
            else if ( (LA6_0==23) ) {
                alt6=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalScenarioExpressions.g:1105:1: ( '*' )
                    {
                    // InternalScenarioExpressions.g:1105:1: ( '*' )
                    // InternalScenarioExpressions.g:1106:1: '*'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getMultiplicationExpressionAccess().getOperatorAsteriskKeyword_1_1_0_0()); 
                    }
                    match(input,22,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getMultiplicationExpressionAccess().getOperatorAsteriskKeyword_1_1_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1113:6: ( '/' )
                    {
                    // InternalScenarioExpressions.g:1113:6: ( '/' )
                    // InternalScenarioExpressions.g:1114:1: '/'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getMultiplicationExpressionAccess().getOperatorSolidusKeyword_1_1_0_1()); 
                    }
                    match(input,23,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getMultiplicationExpressionAccess().getOperatorSolidusKeyword_1_1_0_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__OperatorAlternatives_1_1_0"


    // $ANTLR start "rule__NegatedExpression__Alternatives"
    // InternalScenarioExpressions.g:1126:1: rule__NegatedExpression__Alternatives : ( ( ( rule__NegatedExpression__Group_0__0 ) ) | ( ruleBasicExpression ) );
    public final void rule__NegatedExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1130:1: ( ( ( rule__NegatedExpression__Group_0__0 ) ) | ( ruleBasicExpression ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==21||LA7_0==24) ) {
                alt7=1;
            }
            else if ( ((LA7_0>=RULE_INT && LA7_0<=RULE_BOOL)||LA7_0==42||LA7_0==45) ) {
                alt7=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalScenarioExpressions.g:1131:1: ( ( rule__NegatedExpression__Group_0__0 ) )
                    {
                    // InternalScenarioExpressions.g:1131:1: ( ( rule__NegatedExpression__Group_0__0 ) )
                    // InternalScenarioExpressions.g:1132:1: ( rule__NegatedExpression__Group_0__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNegatedExpressionAccess().getGroup_0()); 
                    }
                    // InternalScenarioExpressions.g:1133:1: ( rule__NegatedExpression__Group_0__0 )
                    // InternalScenarioExpressions.g:1133:2: rule__NegatedExpression__Group_0__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__NegatedExpression__Group_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNegatedExpressionAccess().getGroup_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1137:6: ( ruleBasicExpression )
                    {
                    // InternalScenarioExpressions.g:1137:6: ( ruleBasicExpression )
                    // InternalScenarioExpressions.g:1138:1: ruleBasicExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNegatedExpressionAccess().getBasicExpressionParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleBasicExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNegatedExpressionAccess().getBasicExpressionParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Alternatives"


    // $ANTLR start "rule__NegatedExpression__OperatorAlternatives_0_1_0"
    // InternalScenarioExpressions.g:1148:1: rule__NegatedExpression__OperatorAlternatives_0_1_0 : ( ( '!' ) | ( '-' ) );
    public final void rule__NegatedExpression__OperatorAlternatives_0_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1152:1: ( ( '!' ) | ( '-' ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==24) ) {
                alt8=1;
            }
            else if ( (LA8_0==21) ) {
                alt8=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalScenarioExpressions.g:1153:1: ( '!' )
                    {
                    // InternalScenarioExpressions.g:1153:1: ( '!' )
                    // InternalScenarioExpressions.g:1154:1: '!'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNegatedExpressionAccess().getOperatorExclamationMarkKeyword_0_1_0_0()); 
                    }
                    match(input,24,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNegatedExpressionAccess().getOperatorExclamationMarkKeyword_0_1_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1161:6: ( '-' )
                    {
                    // InternalScenarioExpressions.g:1161:6: ( '-' )
                    // InternalScenarioExpressions.g:1162:1: '-'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNegatedExpressionAccess().getOperatorHyphenMinusKeyword_0_1_0_1()); 
                    }
                    match(input,21,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNegatedExpressionAccess().getOperatorHyphenMinusKeyword_0_1_0_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__OperatorAlternatives_0_1_0"


    // $ANTLR start "rule__BasicExpression__Alternatives"
    // InternalScenarioExpressions.g:1174:1: rule__BasicExpression__Alternatives : ( ( ruleValue ) | ( ( rule__BasicExpression__Group_1__0 ) ) );
    public final void rule__BasicExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1178:1: ( ( ruleValue ) | ( ( rule__BasicExpression__Group_1__0 ) ) )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( ((LA9_0>=RULE_INT && LA9_0<=RULE_BOOL)||LA9_0==45) ) {
                alt9=1;
            }
            else if ( (LA9_0==42) ) {
                alt9=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalScenarioExpressions.g:1179:1: ( ruleValue )
                    {
                    // InternalScenarioExpressions.g:1179:1: ( ruleValue )
                    // InternalScenarioExpressions.g:1180:1: ruleValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBasicExpressionAccess().getValueParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBasicExpressionAccess().getValueParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1185:6: ( ( rule__BasicExpression__Group_1__0 ) )
                    {
                    // InternalScenarioExpressions.g:1185:6: ( ( rule__BasicExpression__Group_1__0 ) )
                    // InternalScenarioExpressions.g:1186:1: ( rule__BasicExpression__Group_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBasicExpressionAccess().getGroup_1()); 
                    }
                    // InternalScenarioExpressions.g:1187:1: ( rule__BasicExpression__Group_1__0 )
                    // InternalScenarioExpressions.g:1187:2: rule__BasicExpression__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__BasicExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBasicExpressionAccess().getGroup_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Alternatives"


    // $ANTLR start "rule__Value__Alternatives"
    // InternalScenarioExpressions.g:1196:1: rule__Value__Alternatives : ( ( ruleIntegerValue ) | ( ruleBooleanValue ) | ( ruleStringValue ) | ( ruleEnumValue ) | ( ruleNullValue ) | ( ruleVariableValue ) | ( ruleFeatureAccess ) );
    public final void rule__Value__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1200:1: ( ( ruleIntegerValue ) | ( ruleBooleanValue ) | ( ruleStringValue ) | ( ruleEnumValue ) | ( ruleNullValue ) | ( ruleVariableValue ) | ( ruleFeatureAccess ) )
            int alt10=7;
            switch ( input.LA(1) ) {
            case RULE_INT:
            case RULE_SIGNEDINT:
                {
                alt10=1;
                }
                break;
            case RULE_BOOL:
                {
                alt10=2;
                }
                break;
            case RULE_STRING:
                {
                alt10=3;
                }
                break;
            case RULE_ID:
                {
                switch ( input.LA(2) ) {
                case 46:
                    {
                    alt10=7;
                    }
                    break;
                case 44:
                    {
                    alt10=4;
                    }
                    break;
                case EOF:
                case 14:
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 37:
                case 43:
                case 47:
                case 48:
                case 49:
                case 50:
                    {
                    alt10=6;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 10, 4, input);

                    throw nvae;
                }

                }
                break;
            case 45:
                {
                alt10=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalScenarioExpressions.g:1201:1: ( ruleIntegerValue )
                    {
                    // InternalScenarioExpressions.g:1201:1: ( ruleIntegerValue )
                    // InternalScenarioExpressions.g:1202:1: ruleIntegerValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getIntegerValueParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleIntegerValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getIntegerValueParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1207:6: ( ruleBooleanValue )
                    {
                    // InternalScenarioExpressions.g:1207:6: ( ruleBooleanValue )
                    // InternalScenarioExpressions.g:1208:1: ruleBooleanValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getBooleanValueParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleBooleanValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getBooleanValueParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalScenarioExpressions.g:1213:6: ( ruleStringValue )
                    {
                    // InternalScenarioExpressions.g:1213:6: ( ruleStringValue )
                    // InternalScenarioExpressions.g:1214:1: ruleStringValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getStringValueParserRuleCall_2()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleStringValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getStringValueParserRuleCall_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalScenarioExpressions.g:1219:6: ( ruleEnumValue )
                    {
                    // InternalScenarioExpressions.g:1219:6: ( ruleEnumValue )
                    // InternalScenarioExpressions.g:1220:1: ruleEnumValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getEnumValueParserRuleCall_3()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleEnumValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getEnumValueParserRuleCall_3()); 
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalScenarioExpressions.g:1225:6: ( ruleNullValue )
                    {
                    // InternalScenarioExpressions.g:1225:6: ( ruleNullValue )
                    // InternalScenarioExpressions.g:1226:1: ruleNullValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getNullValueParserRuleCall_4()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleNullValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getNullValueParserRuleCall_4()); 
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalScenarioExpressions.g:1231:6: ( ruleVariableValue )
                    {
                    // InternalScenarioExpressions.g:1231:6: ( ruleVariableValue )
                    // InternalScenarioExpressions.g:1232:1: ruleVariableValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getVariableValueParserRuleCall_5()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleVariableValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getVariableValueParserRuleCall_5()); 
                    }

                    }


                    }
                    break;
                case 7 :
                    // InternalScenarioExpressions.g:1237:6: ( ruleFeatureAccess )
                    {
                    // InternalScenarioExpressions.g:1237:6: ( ruleFeatureAccess )
                    // InternalScenarioExpressions.g:1238:1: ruleFeatureAccess
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getFeatureAccessParserRuleCall_6()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleFeatureAccess();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getFeatureAccessParserRuleCall_6()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__Alternatives"


    // $ANTLR start "rule__IntegerValue__ValueAlternatives_0"
    // InternalScenarioExpressions.g:1248:1: rule__IntegerValue__ValueAlternatives_0 : ( ( RULE_INT ) | ( RULE_SIGNEDINT ) );
    public final void rule__IntegerValue__ValueAlternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1252:1: ( ( RULE_INT ) | ( RULE_SIGNEDINT ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==RULE_INT) ) {
                alt11=1;
            }
            else if ( (LA11_0==RULE_SIGNEDINT) ) {
                alt11=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalScenarioExpressions.g:1253:1: ( RULE_INT )
                    {
                    // InternalScenarioExpressions.g:1253:1: ( RULE_INT )
                    // InternalScenarioExpressions.g:1254:1: RULE_INT
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0_0()); 
                    }
                    match(input,RULE_INT,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1259:6: ( RULE_SIGNEDINT )
                    {
                    // InternalScenarioExpressions.g:1259:6: ( RULE_SIGNEDINT )
                    // InternalScenarioExpressions.g:1260:1: RULE_SIGNEDINT
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getIntegerValueAccess().getValueSIGNEDINTTerminalRuleCall_0_1()); 
                    }
                    match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getIntegerValueAccess().getValueSIGNEDINTTerminalRuleCall_0_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerValue__ValueAlternatives_0"


    // $ANTLR start "rule__FeatureAccess__Alternatives_2"
    // InternalScenarioExpressions.g:1270:1: rule__FeatureAccess__Alternatives_2 : ( ( ( rule__FeatureAccess__Group_2_0__0 ) ) | ( ( rule__FeatureAccess__Group_2_1__0 ) ) );
    public final void rule__FeatureAccess__Alternatives_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1274:1: ( ( ( rule__FeatureAccess__Group_2_0__0 ) ) | ( ( rule__FeatureAccess__Group_2_1__0 ) ) )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==RULE_ID) ) {
                int LA12_1 = input.LA(2);

                if ( (LA12_1==EOF||(LA12_1>=14 && LA12_1<=23)||LA12_1==37||LA12_1==43||(LA12_1>=46 && LA12_1<=50)) ) {
                    alt12=1;
                }
                else if ( (LA12_1==42) ) {
                    alt12=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 12, 1, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalScenarioExpressions.g:1275:1: ( ( rule__FeatureAccess__Group_2_0__0 ) )
                    {
                    // InternalScenarioExpressions.g:1275:1: ( ( rule__FeatureAccess__Group_2_0__0 ) )
                    // InternalScenarioExpressions.g:1276:1: ( rule__FeatureAccess__Group_2_0__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getFeatureAccessAccess().getGroup_2_0()); 
                    }
                    // InternalScenarioExpressions.g:1277:1: ( rule__FeatureAccess__Group_2_0__0 )
                    // InternalScenarioExpressions.g:1277:2: rule__FeatureAccess__Group_2_0__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__FeatureAccess__Group_2_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getFeatureAccessAccess().getGroup_2_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1281:6: ( ( rule__FeatureAccess__Group_2_1__0 ) )
                    {
                    // InternalScenarioExpressions.g:1281:6: ( ( rule__FeatureAccess__Group_2_1__0 ) )
                    // InternalScenarioExpressions.g:1282:1: ( rule__FeatureAccess__Group_2_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getFeatureAccessAccess().getGroup_2_1()); 
                    }
                    // InternalScenarioExpressions.g:1283:1: ( rule__FeatureAccess__Group_2_1__0 )
                    // InternalScenarioExpressions.g:1283:2: rule__FeatureAccess__Group_2_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__FeatureAccess__Group_2_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getFeatureAccessAccess().getGroup_2_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Alternatives_2"


    // $ANTLR start "rule__CollectionOperation__Alternatives"
    // InternalScenarioExpressions.g:1292:1: rule__CollectionOperation__Alternatives : ( ( ( 'any' ) ) | ( ( 'contains' ) ) | ( ( 'containsAll' ) ) | ( ( 'first' ) ) | ( ( 'get' ) ) | ( ( 'isEmpty' ) ) | ( ( 'last' ) ) | ( ( 'size' ) ) );
    public final void rule__CollectionOperation__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1296:1: ( ( ( 'any' ) ) | ( ( 'contains' ) ) | ( ( 'containsAll' ) ) | ( ( 'first' ) ) | ( ( 'get' ) ) | ( ( 'isEmpty' ) ) | ( ( 'last' ) ) | ( ( 'size' ) ) )
            int alt13=8;
            switch ( input.LA(1) ) {
            case 25:
                {
                alt13=1;
                }
                break;
            case 26:
                {
                alt13=2;
                }
                break;
            case 27:
                {
                alt13=3;
                }
                break;
            case 28:
                {
                alt13=4;
                }
                break;
            case 29:
                {
                alt13=5;
                }
                break;
            case 30:
                {
                alt13=6;
                }
                break;
            case 31:
                {
                alt13=7;
                }
                break;
            case 32:
                {
                alt13=8;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }

            switch (alt13) {
                case 1 :
                    // InternalScenarioExpressions.g:1297:1: ( ( 'any' ) )
                    {
                    // InternalScenarioExpressions.g:1297:1: ( ( 'any' ) )
                    // InternalScenarioExpressions.g:1298:1: ( 'any' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0()); 
                    }
                    // InternalScenarioExpressions.g:1299:1: ( 'any' )
                    // InternalScenarioExpressions.g:1299:3: 'any'
                    {
                    match(input,25,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1304:6: ( ( 'contains' ) )
                    {
                    // InternalScenarioExpressions.g:1304:6: ( ( 'contains' ) )
                    // InternalScenarioExpressions.g:1305:1: ( 'contains' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1()); 
                    }
                    // InternalScenarioExpressions.g:1306:1: ( 'contains' )
                    // InternalScenarioExpressions.g:1306:3: 'contains'
                    {
                    match(input,26,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalScenarioExpressions.g:1311:6: ( ( 'containsAll' ) )
                    {
                    // InternalScenarioExpressions.g:1311:6: ( ( 'containsAll' ) )
                    // InternalScenarioExpressions.g:1312:1: ( 'containsAll' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2()); 
                    }
                    // InternalScenarioExpressions.g:1313:1: ( 'containsAll' )
                    // InternalScenarioExpressions.g:1313:3: 'containsAll'
                    {
                    match(input,27,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalScenarioExpressions.g:1318:6: ( ( 'first' ) )
                    {
                    // InternalScenarioExpressions.g:1318:6: ( ( 'first' ) )
                    // InternalScenarioExpressions.g:1319:1: ( 'first' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3()); 
                    }
                    // InternalScenarioExpressions.g:1320:1: ( 'first' )
                    // InternalScenarioExpressions.g:1320:3: 'first'
                    {
                    match(input,28,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3()); 
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalScenarioExpressions.g:1325:6: ( ( 'get' ) )
                    {
                    // InternalScenarioExpressions.g:1325:6: ( ( 'get' ) )
                    // InternalScenarioExpressions.g:1326:1: ( 'get' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4()); 
                    }
                    // InternalScenarioExpressions.g:1327:1: ( 'get' )
                    // InternalScenarioExpressions.g:1327:3: 'get'
                    {
                    match(input,29,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4()); 
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalScenarioExpressions.g:1332:6: ( ( 'isEmpty' ) )
                    {
                    // InternalScenarioExpressions.g:1332:6: ( ( 'isEmpty' ) )
                    // InternalScenarioExpressions.g:1333:1: ( 'isEmpty' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5()); 
                    }
                    // InternalScenarioExpressions.g:1334:1: ( 'isEmpty' )
                    // InternalScenarioExpressions.g:1334:3: 'isEmpty'
                    {
                    match(input,30,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5()); 
                    }

                    }


                    }
                    break;
                case 7 :
                    // InternalScenarioExpressions.g:1339:6: ( ( 'last' ) )
                    {
                    // InternalScenarioExpressions.g:1339:6: ( ( 'last' ) )
                    // InternalScenarioExpressions.g:1340:1: ( 'last' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6()); 
                    }
                    // InternalScenarioExpressions.g:1341:1: ( 'last' )
                    // InternalScenarioExpressions.g:1341:3: 'last'
                    {
                    match(input,31,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6()); 
                    }

                    }


                    }
                    break;
                case 8 :
                    // InternalScenarioExpressions.g:1346:6: ( ( 'size' ) )
                    {
                    // InternalScenarioExpressions.g:1346:6: ( ( 'size' ) )
                    // InternalScenarioExpressions.g:1347:1: ( 'size' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7()); 
                    }
                    // InternalScenarioExpressions.g:1348:1: ( 'size' )
                    // InternalScenarioExpressions.g:1348:3: 'size'
                    {
                    match(input,32,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionOperation__Alternatives"


    // $ANTLR start "rule__Document__Group__0"
    // InternalScenarioExpressions.g:1361:1: rule__Document__Group__0 : rule__Document__Group__0__Impl rule__Document__Group__1 ;
    public final void rule__Document__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1365:1: ( rule__Document__Group__0__Impl rule__Document__Group__1 )
            // InternalScenarioExpressions.g:1366:2: rule__Document__Group__0__Impl rule__Document__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_3);
            rule__Document__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Document__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__0"


    // $ANTLR start "rule__Document__Group__0__Impl"
    // InternalScenarioExpressions.g:1373:1: rule__Document__Group__0__Impl : ( ( rule__Document__ImportsAssignment_0 )* ) ;
    public final void rule__Document__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1377:1: ( ( ( rule__Document__ImportsAssignment_0 )* ) )
            // InternalScenarioExpressions.g:1378:1: ( ( rule__Document__ImportsAssignment_0 )* )
            {
            // InternalScenarioExpressions.g:1378:1: ( ( rule__Document__ImportsAssignment_0 )* )
            // InternalScenarioExpressions.g:1379:1: ( rule__Document__ImportsAssignment_0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getImportsAssignment_0()); 
            }
            // InternalScenarioExpressions.g:1380:1: ( rule__Document__ImportsAssignment_0 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==34) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalScenarioExpressions.g:1380:2: rule__Document__ImportsAssignment_0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_4);
            	    rule__Document__ImportsAssignment_0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getImportsAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__0__Impl"


    // $ANTLR start "rule__Document__Group__1"
    // InternalScenarioExpressions.g:1390:1: rule__Document__Group__1 : rule__Document__Group__1__Impl rule__Document__Group__2 ;
    public final void rule__Document__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1394:1: ( rule__Document__Group__1__Impl rule__Document__Group__2 )
            // InternalScenarioExpressions.g:1395:2: rule__Document__Group__1__Impl rule__Document__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_3);
            rule__Document__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Document__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__1"


    // $ANTLR start "rule__Document__Group__1__Impl"
    // InternalScenarioExpressions.g:1402:1: rule__Document__Group__1__Impl : ( ( rule__Document__Group_1__0 )* ) ;
    public final void rule__Document__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1406:1: ( ( ( rule__Document__Group_1__0 )* ) )
            // InternalScenarioExpressions.g:1407:1: ( ( rule__Document__Group_1__0 )* )
            {
            // InternalScenarioExpressions.g:1407:1: ( ( rule__Document__Group_1__0 )* )
            // InternalScenarioExpressions.g:1408:1: ( rule__Document__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getGroup_1()); 
            }
            // InternalScenarioExpressions.g:1409:1: ( rule__Document__Group_1__0 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==33) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalScenarioExpressions.g:1409:2: rule__Document__Group_1__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_5);
            	    rule__Document__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__1__Impl"


    // $ANTLR start "rule__Document__Group__2"
    // InternalScenarioExpressions.g:1419:1: rule__Document__Group__2 : rule__Document__Group__2__Impl ;
    public final void rule__Document__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1423:1: ( rule__Document__Group__2__Impl )
            // InternalScenarioExpressions.g:1424:2: rule__Document__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Document__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__2"


    // $ANTLR start "rule__Document__Group__2__Impl"
    // InternalScenarioExpressions.g:1430:1: rule__Document__Group__2__Impl : ( ( rule__Document__ExpressionsAssignment_2 )* ) ;
    public final void rule__Document__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1434:1: ( ( ( rule__Document__ExpressionsAssignment_2 )* ) )
            // InternalScenarioExpressions.g:1435:1: ( ( rule__Document__ExpressionsAssignment_2 )* )
            {
            // InternalScenarioExpressions.g:1435:1: ( ( rule__Document__ExpressionsAssignment_2 )* )
            // InternalScenarioExpressions.g:1436:1: ( rule__Document__ExpressionsAssignment_2 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getExpressionsAssignment_2()); 
            }
            // InternalScenarioExpressions.g:1437:1: ( rule__Document__ExpressionsAssignment_2 )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==35) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalScenarioExpressions.g:1437:2: rule__Document__ExpressionsAssignment_2
            	    {
            	    pushFollow(FollowSets000.FOLLOW_6);
            	    rule__Document__ExpressionsAssignment_2();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getExpressionsAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__2__Impl"


    // $ANTLR start "rule__Document__Group_1__0"
    // InternalScenarioExpressions.g:1453:1: rule__Document__Group_1__0 : rule__Document__Group_1__0__Impl rule__Document__Group_1__1 ;
    public final void rule__Document__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1457:1: ( rule__Document__Group_1__0__Impl rule__Document__Group_1__1 )
            // InternalScenarioExpressions.g:1458:2: rule__Document__Group_1__0__Impl rule__Document__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_7);
            rule__Document__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Document__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group_1__0"


    // $ANTLR start "rule__Document__Group_1__0__Impl"
    // InternalScenarioExpressions.g:1465:1: rule__Document__Group_1__0__Impl : ( 'domain' ) ;
    public final void rule__Document__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1469:1: ( ( 'domain' ) )
            // InternalScenarioExpressions.g:1470:1: ( 'domain' )
            {
            // InternalScenarioExpressions.g:1470:1: ( 'domain' )
            // InternalScenarioExpressions.g:1471:1: 'domain'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getDomainKeyword_1_0()); 
            }
            match(input,33,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getDomainKeyword_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group_1__0__Impl"


    // $ANTLR start "rule__Document__Group_1__1"
    // InternalScenarioExpressions.g:1484:1: rule__Document__Group_1__1 : rule__Document__Group_1__1__Impl ;
    public final void rule__Document__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1488:1: ( rule__Document__Group_1__1__Impl )
            // InternalScenarioExpressions.g:1489:2: rule__Document__Group_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Document__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group_1__1"


    // $ANTLR start "rule__Document__Group_1__1__Impl"
    // InternalScenarioExpressions.g:1495:1: rule__Document__Group_1__1__Impl : ( ( rule__Document__DomainsAssignment_1_1 ) ) ;
    public final void rule__Document__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1499:1: ( ( ( rule__Document__DomainsAssignment_1_1 ) ) )
            // InternalScenarioExpressions.g:1500:1: ( ( rule__Document__DomainsAssignment_1_1 ) )
            {
            // InternalScenarioExpressions.g:1500:1: ( ( rule__Document__DomainsAssignment_1_1 ) )
            // InternalScenarioExpressions.g:1501:1: ( rule__Document__DomainsAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getDomainsAssignment_1_1()); 
            }
            // InternalScenarioExpressions.g:1502:1: ( rule__Document__DomainsAssignment_1_1 )
            // InternalScenarioExpressions.g:1502:2: rule__Document__DomainsAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Document__DomainsAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getDomainsAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group_1__1__Impl"


    // $ANTLR start "rule__Import__Group__0"
    // InternalScenarioExpressions.g:1516:1: rule__Import__Group__0 : rule__Import__Group__0__Impl rule__Import__Group__1 ;
    public final void rule__Import__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1520:1: ( rule__Import__Group__0__Impl rule__Import__Group__1 )
            // InternalScenarioExpressions.g:1521:2: rule__Import__Group__0__Impl rule__Import__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_8);
            rule__Import__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Import__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0"


    // $ANTLR start "rule__Import__Group__0__Impl"
    // InternalScenarioExpressions.g:1528:1: rule__Import__Group__0__Impl : ( 'import' ) ;
    public final void rule__Import__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1532:1: ( ( 'import' ) )
            // InternalScenarioExpressions.g:1533:1: ( 'import' )
            {
            // InternalScenarioExpressions.g:1533:1: ( 'import' )
            // InternalScenarioExpressions.g:1534:1: 'import'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportAccess().getImportKeyword_0()); 
            }
            match(input,34,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportAccess().getImportKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0__Impl"


    // $ANTLR start "rule__Import__Group__1"
    // InternalScenarioExpressions.g:1547:1: rule__Import__Group__1 : rule__Import__Group__1__Impl ;
    public final void rule__Import__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1551:1: ( rule__Import__Group__1__Impl )
            // InternalScenarioExpressions.g:1552:2: rule__Import__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Import__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1"


    // $ANTLR start "rule__Import__Group__1__Impl"
    // InternalScenarioExpressions.g:1558:1: rule__Import__Group__1__Impl : ( ( rule__Import__ImportURIAssignment_1 ) ) ;
    public final void rule__Import__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1562:1: ( ( ( rule__Import__ImportURIAssignment_1 ) ) )
            // InternalScenarioExpressions.g:1563:1: ( ( rule__Import__ImportURIAssignment_1 ) )
            {
            // InternalScenarioExpressions.g:1563:1: ( ( rule__Import__ImportURIAssignment_1 ) )
            // InternalScenarioExpressions.g:1564:1: ( rule__Import__ImportURIAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportAccess().getImportURIAssignment_1()); 
            }
            // InternalScenarioExpressions.g:1565:1: ( rule__Import__ImportURIAssignment_1 )
            // InternalScenarioExpressions.g:1565:2: rule__Import__ImportURIAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Import__ImportURIAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportAccess().getImportURIAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1__Impl"


    // $ANTLR start "rule__ExpressionRegion__Group__0"
    // InternalScenarioExpressions.g:1579:1: rule__ExpressionRegion__Group__0 : rule__ExpressionRegion__Group__0__Impl rule__ExpressionRegion__Group__1 ;
    public final void rule__ExpressionRegion__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1583:1: ( rule__ExpressionRegion__Group__0__Impl rule__ExpressionRegion__Group__1 )
            // InternalScenarioExpressions.g:1584:2: rule__ExpressionRegion__Group__0__Impl rule__ExpressionRegion__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_9);
            rule__ExpressionRegion__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__0"


    // $ANTLR start "rule__ExpressionRegion__Group__0__Impl"
    // InternalScenarioExpressions.g:1591:1: rule__ExpressionRegion__Group__0__Impl : ( () ) ;
    public final void rule__ExpressionRegion__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1595:1: ( ( () ) )
            // InternalScenarioExpressions.g:1596:1: ( () )
            {
            // InternalScenarioExpressions.g:1596:1: ( () )
            // InternalScenarioExpressions.g:1597:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getExpressionRegionAction_0()); 
            }
            // InternalScenarioExpressions.g:1598:1: ()
            // InternalScenarioExpressions.g:1600:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getExpressionRegionAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__0__Impl"


    // $ANTLR start "rule__ExpressionRegion__Group__1"
    // InternalScenarioExpressions.g:1610:1: rule__ExpressionRegion__Group__1 : rule__ExpressionRegion__Group__1__Impl rule__ExpressionRegion__Group__2 ;
    public final void rule__ExpressionRegion__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1614:1: ( rule__ExpressionRegion__Group__1__Impl rule__ExpressionRegion__Group__2 )
            // InternalScenarioExpressions.g:1615:2: rule__ExpressionRegion__Group__1__Impl rule__ExpressionRegion__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_10);
            rule__ExpressionRegion__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__1"


    // $ANTLR start "rule__ExpressionRegion__Group__1__Impl"
    // InternalScenarioExpressions.g:1622:1: rule__ExpressionRegion__Group__1__Impl : ( '{' ) ;
    public final void rule__ExpressionRegion__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1626:1: ( ( '{' ) )
            // InternalScenarioExpressions.g:1627:1: ( '{' )
            {
            // InternalScenarioExpressions.g:1627:1: ( '{' )
            // InternalScenarioExpressions.g:1628:1: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getLeftCurlyBracketKeyword_1()); 
            }
            match(input,35,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getLeftCurlyBracketKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__1__Impl"


    // $ANTLR start "rule__ExpressionRegion__Group__2"
    // InternalScenarioExpressions.g:1641:1: rule__ExpressionRegion__Group__2 : rule__ExpressionRegion__Group__2__Impl rule__ExpressionRegion__Group__3 ;
    public final void rule__ExpressionRegion__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1645:1: ( rule__ExpressionRegion__Group__2__Impl rule__ExpressionRegion__Group__3 )
            // InternalScenarioExpressions.g:1646:2: rule__ExpressionRegion__Group__2__Impl rule__ExpressionRegion__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_10);
            rule__ExpressionRegion__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__2"


    // $ANTLR start "rule__ExpressionRegion__Group__2__Impl"
    // InternalScenarioExpressions.g:1653:1: rule__ExpressionRegion__Group__2__Impl : ( ( rule__ExpressionRegion__Group_2__0 )* ) ;
    public final void rule__ExpressionRegion__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1657:1: ( ( ( rule__ExpressionRegion__Group_2__0 )* ) )
            // InternalScenarioExpressions.g:1658:1: ( ( rule__ExpressionRegion__Group_2__0 )* )
            {
            // InternalScenarioExpressions.g:1658:1: ( ( rule__ExpressionRegion__Group_2__0 )* )
            // InternalScenarioExpressions.g:1659:1: ( rule__ExpressionRegion__Group_2__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getGroup_2()); 
            }
            // InternalScenarioExpressions.g:1660:1: ( rule__ExpressionRegion__Group_2__0 )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( ((LA17_0>=RULE_INT && LA17_0<=RULE_BOOL)||LA17_0==21||LA17_0==24||LA17_0==35||(LA17_0>=39 && LA17_0<=42)||LA17_0==45) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalScenarioExpressions.g:1660:2: rule__ExpressionRegion__Group_2__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_11);
            	    rule__ExpressionRegion__Group_2__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getGroup_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__2__Impl"


    // $ANTLR start "rule__ExpressionRegion__Group__3"
    // InternalScenarioExpressions.g:1670:1: rule__ExpressionRegion__Group__3 : rule__ExpressionRegion__Group__3__Impl ;
    public final void rule__ExpressionRegion__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1674:1: ( rule__ExpressionRegion__Group__3__Impl )
            // InternalScenarioExpressions.g:1675:2: rule__ExpressionRegion__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__3"


    // $ANTLR start "rule__ExpressionRegion__Group__3__Impl"
    // InternalScenarioExpressions.g:1681:1: rule__ExpressionRegion__Group__3__Impl : ( '}' ) ;
    public final void rule__ExpressionRegion__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1685:1: ( ( '}' ) )
            // InternalScenarioExpressions.g:1686:1: ( '}' )
            {
            // InternalScenarioExpressions.g:1686:1: ( '}' )
            // InternalScenarioExpressions.g:1687:1: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getRightCurlyBracketKeyword_3()); 
            }
            match(input,36,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getRightCurlyBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__3__Impl"


    // $ANTLR start "rule__ExpressionRegion__Group_2__0"
    // InternalScenarioExpressions.g:1708:1: rule__ExpressionRegion__Group_2__0 : rule__ExpressionRegion__Group_2__0__Impl rule__ExpressionRegion__Group_2__1 ;
    public final void rule__ExpressionRegion__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1712:1: ( rule__ExpressionRegion__Group_2__0__Impl rule__ExpressionRegion__Group_2__1 )
            // InternalScenarioExpressions.g:1713:2: rule__ExpressionRegion__Group_2__0__Impl rule__ExpressionRegion__Group_2__1
            {
            pushFollow(FollowSets000.FOLLOW_12);
            rule__ExpressionRegion__Group_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group_2__0"


    // $ANTLR start "rule__ExpressionRegion__Group_2__0__Impl"
    // InternalScenarioExpressions.g:1720:1: rule__ExpressionRegion__Group_2__0__Impl : ( ( rule__ExpressionRegion__ExpressionsAssignment_2_0 ) ) ;
    public final void rule__ExpressionRegion__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1724:1: ( ( ( rule__ExpressionRegion__ExpressionsAssignment_2_0 ) ) )
            // InternalScenarioExpressions.g:1725:1: ( ( rule__ExpressionRegion__ExpressionsAssignment_2_0 ) )
            {
            // InternalScenarioExpressions.g:1725:1: ( ( rule__ExpressionRegion__ExpressionsAssignment_2_0 ) )
            // InternalScenarioExpressions.g:1726:1: ( rule__ExpressionRegion__ExpressionsAssignment_2_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getExpressionsAssignment_2_0()); 
            }
            // InternalScenarioExpressions.g:1727:1: ( rule__ExpressionRegion__ExpressionsAssignment_2_0 )
            // InternalScenarioExpressions.g:1727:2: rule__ExpressionRegion__ExpressionsAssignment_2_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__ExpressionsAssignment_2_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getExpressionsAssignment_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group_2__0__Impl"


    // $ANTLR start "rule__ExpressionRegion__Group_2__1"
    // InternalScenarioExpressions.g:1737:1: rule__ExpressionRegion__Group_2__1 : rule__ExpressionRegion__Group_2__1__Impl ;
    public final void rule__ExpressionRegion__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1741:1: ( rule__ExpressionRegion__Group_2__1__Impl )
            // InternalScenarioExpressions.g:1742:2: rule__ExpressionRegion__Group_2__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group_2__1"


    // $ANTLR start "rule__ExpressionRegion__Group_2__1__Impl"
    // InternalScenarioExpressions.g:1748:1: rule__ExpressionRegion__Group_2__1__Impl : ( ';' ) ;
    public final void rule__ExpressionRegion__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1752:1: ( ( ';' ) )
            // InternalScenarioExpressions.g:1753:1: ( ';' )
            {
            // InternalScenarioExpressions.g:1753:1: ( ';' )
            // InternalScenarioExpressions.g:1754:1: ';'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getSemicolonKeyword_2_1()); 
            }
            match(input,37,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getSemicolonKeyword_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group_2__1__Impl"


    // $ANTLR start "rule__VariableAssignment__Group__0"
    // InternalScenarioExpressions.g:1772:1: rule__VariableAssignment__Group__0 : rule__VariableAssignment__Group__0__Impl rule__VariableAssignment__Group__1 ;
    public final void rule__VariableAssignment__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1776:1: ( rule__VariableAssignment__Group__0__Impl rule__VariableAssignment__Group__1 )
            // InternalScenarioExpressions.g:1777:2: rule__VariableAssignment__Group__0__Impl rule__VariableAssignment__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_13);
            rule__VariableAssignment__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableAssignment__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__Group__0"


    // $ANTLR start "rule__VariableAssignment__Group__0__Impl"
    // InternalScenarioExpressions.g:1784:1: rule__VariableAssignment__Group__0__Impl : ( ( rule__VariableAssignment__VariableAssignment_0 ) ) ;
    public final void rule__VariableAssignment__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1788:1: ( ( ( rule__VariableAssignment__VariableAssignment_0 ) ) )
            // InternalScenarioExpressions.g:1789:1: ( ( rule__VariableAssignment__VariableAssignment_0 ) )
            {
            // InternalScenarioExpressions.g:1789:1: ( ( rule__VariableAssignment__VariableAssignment_0 ) )
            // InternalScenarioExpressions.g:1790:1: ( rule__VariableAssignment__VariableAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getVariableAssignment_0()); 
            }
            // InternalScenarioExpressions.g:1791:1: ( rule__VariableAssignment__VariableAssignment_0 )
            // InternalScenarioExpressions.g:1791:2: rule__VariableAssignment__VariableAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableAssignment__VariableAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getVariableAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__Group__0__Impl"


    // $ANTLR start "rule__VariableAssignment__Group__1"
    // InternalScenarioExpressions.g:1801:1: rule__VariableAssignment__Group__1 : rule__VariableAssignment__Group__1__Impl rule__VariableAssignment__Group__2 ;
    public final void rule__VariableAssignment__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1805:1: ( rule__VariableAssignment__Group__1__Impl rule__VariableAssignment__Group__2 )
            // InternalScenarioExpressions.g:1806:2: rule__VariableAssignment__Group__1__Impl rule__VariableAssignment__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__VariableAssignment__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableAssignment__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__Group__1"


    // $ANTLR start "rule__VariableAssignment__Group__1__Impl"
    // InternalScenarioExpressions.g:1813:1: rule__VariableAssignment__Group__1__Impl : ( '=' ) ;
    public final void rule__VariableAssignment__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1817:1: ( ( '=' ) )
            // InternalScenarioExpressions.g:1818:1: ( '=' )
            {
            // InternalScenarioExpressions.g:1818:1: ( '=' )
            // InternalScenarioExpressions.g:1819:1: '='
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getEqualsSignKeyword_1()); 
            }
            match(input,38,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getEqualsSignKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__Group__1__Impl"


    // $ANTLR start "rule__VariableAssignment__Group__2"
    // InternalScenarioExpressions.g:1832:1: rule__VariableAssignment__Group__2 : rule__VariableAssignment__Group__2__Impl ;
    public final void rule__VariableAssignment__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1836:1: ( rule__VariableAssignment__Group__2__Impl )
            // InternalScenarioExpressions.g:1837:2: rule__VariableAssignment__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableAssignment__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__Group__2"


    // $ANTLR start "rule__VariableAssignment__Group__2__Impl"
    // InternalScenarioExpressions.g:1843:1: rule__VariableAssignment__Group__2__Impl : ( ( rule__VariableAssignment__ExpressionAssignment_2 ) ) ;
    public final void rule__VariableAssignment__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1847:1: ( ( ( rule__VariableAssignment__ExpressionAssignment_2 ) ) )
            // InternalScenarioExpressions.g:1848:1: ( ( rule__VariableAssignment__ExpressionAssignment_2 ) )
            {
            // InternalScenarioExpressions.g:1848:1: ( ( rule__VariableAssignment__ExpressionAssignment_2 ) )
            // InternalScenarioExpressions.g:1849:1: ( rule__VariableAssignment__ExpressionAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getExpressionAssignment_2()); 
            }
            // InternalScenarioExpressions.g:1850:1: ( rule__VariableAssignment__ExpressionAssignment_2 )
            // InternalScenarioExpressions.g:1850:2: rule__VariableAssignment__ExpressionAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableAssignment__ExpressionAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getExpressionAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__Group__2__Impl"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__0"
    // InternalScenarioExpressions.g:1866:1: rule__TypedVariableDeclaration__Group__0 : rule__TypedVariableDeclaration__Group__0__Impl rule__TypedVariableDeclaration__Group__1 ;
    public final void rule__TypedVariableDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1870:1: ( rule__TypedVariableDeclaration__Group__0__Impl rule__TypedVariableDeclaration__Group__1 )
            // InternalScenarioExpressions.g:1871:2: rule__TypedVariableDeclaration__Group__0__Impl rule__TypedVariableDeclaration__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_7);
            rule__TypedVariableDeclaration__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__0"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__0__Impl"
    // InternalScenarioExpressions.g:1878:1: rule__TypedVariableDeclaration__Group__0__Impl : ( 'var' ) ;
    public final void rule__TypedVariableDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1882:1: ( ( 'var' ) )
            // InternalScenarioExpressions.g:1883:1: ( 'var' )
            {
            // InternalScenarioExpressions.g:1883:1: ( 'var' )
            // InternalScenarioExpressions.g:1884:1: 'var'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getVarKeyword_0()); 
            }
            match(input,39,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getVarKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__0__Impl"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__1"
    // InternalScenarioExpressions.g:1897:1: rule__TypedVariableDeclaration__Group__1 : rule__TypedVariableDeclaration__Group__1__Impl rule__TypedVariableDeclaration__Group__2 ;
    public final void rule__TypedVariableDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1901:1: ( rule__TypedVariableDeclaration__Group__1__Impl rule__TypedVariableDeclaration__Group__2 )
            // InternalScenarioExpressions.g:1902:2: rule__TypedVariableDeclaration__Group__1__Impl rule__TypedVariableDeclaration__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_7);
            rule__TypedVariableDeclaration__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__1"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__1__Impl"
    // InternalScenarioExpressions.g:1909:1: rule__TypedVariableDeclaration__Group__1__Impl : ( ( rule__TypedVariableDeclaration__TypeAssignment_1 ) ) ;
    public final void rule__TypedVariableDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1913:1: ( ( ( rule__TypedVariableDeclaration__TypeAssignment_1 ) ) )
            // InternalScenarioExpressions.g:1914:1: ( ( rule__TypedVariableDeclaration__TypeAssignment_1 ) )
            {
            // InternalScenarioExpressions.g:1914:1: ( ( rule__TypedVariableDeclaration__TypeAssignment_1 ) )
            // InternalScenarioExpressions.g:1915:1: ( rule__TypedVariableDeclaration__TypeAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getTypeAssignment_1()); 
            }
            // InternalScenarioExpressions.g:1916:1: ( rule__TypedVariableDeclaration__TypeAssignment_1 )
            // InternalScenarioExpressions.g:1916:2: rule__TypedVariableDeclaration__TypeAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__TypeAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getTypeAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__1__Impl"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__2"
    // InternalScenarioExpressions.g:1926:1: rule__TypedVariableDeclaration__Group__2 : rule__TypedVariableDeclaration__Group__2__Impl rule__TypedVariableDeclaration__Group__3 ;
    public final void rule__TypedVariableDeclaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1930:1: ( rule__TypedVariableDeclaration__Group__2__Impl rule__TypedVariableDeclaration__Group__3 )
            // InternalScenarioExpressions.g:1931:2: rule__TypedVariableDeclaration__Group__2__Impl rule__TypedVariableDeclaration__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_13);
            rule__TypedVariableDeclaration__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__2"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__2__Impl"
    // InternalScenarioExpressions.g:1938:1: rule__TypedVariableDeclaration__Group__2__Impl : ( ( rule__TypedVariableDeclaration__NameAssignment_2 ) ) ;
    public final void rule__TypedVariableDeclaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1942:1: ( ( ( rule__TypedVariableDeclaration__NameAssignment_2 ) ) )
            // InternalScenarioExpressions.g:1943:1: ( ( rule__TypedVariableDeclaration__NameAssignment_2 ) )
            {
            // InternalScenarioExpressions.g:1943:1: ( ( rule__TypedVariableDeclaration__NameAssignment_2 ) )
            // InternalScenarioExpressions.g:1944:1: ( rule__TypedVariableDeclaration__NameAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getNameAssignment_2()); 
            }
            // InternalScenarioExpressions.g:1945:1: ( rule__TypedVariableDeclaration__NameAssignment_2 )
            // InternalScenarioExpressions.g:1945:2: rule__TypedVariableDeclaration__NameAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__NameAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getNameAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__2__Impl"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__3"
    // InternalScenarioExpressions.g:1955:1: rule__TypedVariableDeclaration__Group__3 : rule__TypedVariableDeclaration__Group__3__Impl ;
    public final void rule__TypedVariableDeclaration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1959:1: ( rule__TypedVariableDeclaration__Group__3__Impl )
            // InternalScenarioExpressions.g:1960:2: rule__TypedVariableDeclaration__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__3"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__3__Impl"
    // InternalScenarioExpressions.g:1966:1: rule__TypedVariableDeclaration__Group__3__Impl : ( ( rule__TypedVariableDeclaration__Group_3__0 )? ) ;
    public final void rule__TypedVariableDeclaration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1970:1: ( ( ( rule__TypedVariableDeclaration__Group_3__0 )? ) )
            // InternalScenarioExpressions.g:1971:1: ( ( rule__TypedVariableDeclaration__Group_3__0 )? )
            {
            // InternalScenarioExpressions.g:1971:1: ( ( rule__TypedVariableDeclaration__Group_3__0 )? )
            // InternalScenarioExpressions.g:1972:1: ( rule__TypedVariableDeclaration__Group_3__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getGroup_3()); 
            }
            // InternalScenarioExpressions.g:1973:1: ( rule__TypedVariableDeclaration__Group_3__0 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==38) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalScenarioExpressions.g:1973:2: rule__TypedVariableDeclaration__Group_3__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__TypedVariableDeclaration__Group_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getGroup_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__3__Impl"


    // $ANTLR start "rule__TypedVariableDeclaration__Group_3__0"
    // InternalScenarioExpressions.g:1991:1: rule__TypedVariableDeclaration__Group_3__0 : rule__TypedVariableDeclaration__Group_3__0__Impl rule__TypedVariableDeclaration__Group_3__1 ;
    public final void rule__TypedVariableDeclaration__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1995:1: ( rule__TypedVariableDeclaration__Group_3__0__Impl rule__TypedVariableDeclaration__Group_3__1 )
            // InternalScenarioExpressions.g:1996:2: rule__TypedVariableDeclaration__Group_3__0__Impl rule__TypedVariableDeclaration__Group_3__1
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__TypedVariableDeclaration__Group_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group_3__0"


    // $ANTLR start "rule__TypedVariableDeclaration__Group_3__0__Impl"
    // InternalScenarioExpressions.g:2003:1: rule__TypedVariableDeclaration__Group_3__0__Impl : ( '=' ) ;
    public final void rule__TypedVariableDeclaration__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2007:1: ( ( '=' ) )
            // InternalScenarioExpressions.g:2008:1: ( '=' )
            {
            // InternalScenarioExpressions.g:2008:1: ( '=' )
            // InternalScenarioExpressions.g:2009:1: '='
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getEqualsSignKeyword_3_0()); 
            }
            match(input,38,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getEqualsSignKeyword_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group_3__0__Impl"


    // $ANTLR start "rule__TypedVariableDeclaration__Group_3__1"
    // InternalScenarioExpressions.g:2022:1: rule__TypedVariableDeclaration__Group_3__1 : rule__TypedVariableDeclaration__Group_3__1__Impl ;
    public final void rule__TypedVariableDeclaration__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2026:1: ( rule__TypedVariableDeclaration__Group_3__1__Impl )
            // InternalScenarioExpressions.g:2027:2: rule__TypedVariableDeclaration__Group_3__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group_3__1"


    // $ANTLR start "rule__TypedVariableDeclaration__Group_3__1__Impl"
    // InternalScenarioExpressions.g:2033:1: rule__TypedVariableDeclaration__Group_3__1__Impl : ( ( rule__TypedVariableDeclaration__ExpressionAssignment_3_1 ) ) ;
    public final void rule__TypedVariableDeclaration__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2037:1: ( ( ( rule__TypedVariableDeclaration__ExpressionAssignment_3_1 ) ) )
            // InternalScenarioExpressions.g:2038:1: ( ( rule__TypedVariableDeclaration__ExpressionAssignment_3_1 ) )
            {
            // InternalScenarioExpressions.g:2038:1: ( ( rule__TypedVariableDeclaration__ExpressionAssignment_3_1 ) )
            // InternalScenarioExpressions.g:2039:1: ( rule__TypedVariableDeclaration__ExpressionAssignment_3_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getExpressionAssignment_3_1()); 
            }
            // InternalScenarioExpressions.g:2040:1: ( rule__TypedVariableDeclaration__ExpressionAssignment_3_1 )
            // InternalScenarioExpressions.g:2040:2: rule__TypedVariableDeclaration__ExpressionAssignment_3_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__ExpressionAssignment_3_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getExpressionAssignment_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group_3__1__Impl"


    // $ANTLR start "rule__ClockDeclaration__Group__0"
    // InternalScenarioExpressions.g:2054:1: rule__ClockDeclaration__Group__0 : rule__ClockDeclaration__Group__0__Impl rule__ClockDeclaration__Group__1 ;
    public final void rule__ClockDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2058:1: ( rule__ClockDeclaration__Group__0__Impl rule__ClockDeclaration__Group__1 )
            // InternalScenarioExpressions.g:2059:2: rule__ClockDeclaration__Group__0__Impl rule__ClockDeclaration__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_15);
            rule__ClockDeclaration__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ClockDeclaration__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockDeclaration__Group__0"


    // $ANTLR start "rule__ClockDeclaration__Group__0__Impl"
    // InternalScenarioExpressions.g:2066:1: rule__ClockDeclaration__Group__0__Impl : ( () ) ;
    public final void rule__ClockDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2070:1: ( ( () ) )
            // InternalScenarioExpressions.g:2071:1: ( () )
            {
            // InternalScenarioExpressions.g:2071:1: ( () )
            // InternalScenarioExpressions.g:2072:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClockDeclarationAccess().getClockDeclarationAction_0()); 
            }
            // InternalScenarioExpressions.g:2073:1: ()
            // InternalScenarioExpressions.g:2075:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClockDeclarationAccess().getClockDeclarationAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockDeclaration__Group__0__Impl"


    // $ANTLR start "rule__ClockDeclaration__Group__1"
    // InternalScenarioExpressions.g:2085:1: rule__ClockDeclaration__Group__1 : rule__ClockDeclaration__Group__1__Impl rule__ClockDeclaration__Group__2 ;
    public final void rule__ClockDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2089:1: ( rule__ClockDeclaration__Group__1__Impl rule__ClockDeclaration__Group__2 )
            // InternalScenarioExpressions.g:2090:2: rule__ClockDeclaration__Group__1__Impl rule__ClockDeclaration__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_7);
            rule__ClockDeclaration__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ClockDeclaration__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockDeclaration__Group__1"


    // $ANTLR start "rule__ClockDeclaration__Group__1__Impl"
    // InternalScenarioExpressions.g:2097:1: rule__ClockDeclaration__Group__1__Impl : ( 'clock' ) ;
    public final void rule__ClockDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2101:1: ( ( 'clock' ) )
            // InternalScenarioExpressions.g:2102:1: ( 'clock' )
            {
            // InternalScenarioExpressions.g:2102:1: ( 'clock' )
            // InternalScenarioExpressions.g:2103:1: 'clock'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClockDeclarationAccess().getClockKeyword_1()); 
            }
            match(input,40,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClockDeclarationAccess().getClockKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockDeclaration__Group__1__Impl"


    // $ANTLR start "rule__ClockDeclaration__Group__2"
    // InternalScenarioExpressions.g:2116:1: rule__ClockDeclaration__Group__2 : rule__ClockDeclaration__Group__2__Impl rule__ClockDeclaration__Group__3 ;
    public final void rule__ClockDeclaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2120:1: ( rule__ClockDeclaration__Group__2__Impl rule__ClockDeclaration__Group__3 )
            // InternalScenarioExpressions.g:2121:2: rule__ClockDeclaration__Group__2__Impl rule__ClockDeclaration__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_13);
            rule__ClockDeclaration__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ClockDeclaration__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockDeclaration__Group__2"


    // $ANTLR start "rule__ClockDeclaration__Group__2__Impl"
    // InternalScenarioExpressions.g:2128:1: rule__ClockDeclaration__Group__2__Impl : ( ( rule__ClockDeclaration__NameAssignment_2 ) ) ;
    public final void rule__ClockDeclaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2132:1: ( ( ( rule__ClockDeclaration__NameAssignment_2 ) ) )
            // InternalScenarioExpressions.g:2133:1: ( ( rule__ClockDeclaration__NameAssignment_2 ) )
            {
            // InternalScenarioExpressions.g:2133:1: ( ( rule__ClockDeclaration__NameAssignment_2 ) )
            // InternalScenarioExpressions.g:2134:1: ( rule__ClockDeclaration__NameAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClockDeclarationAccess().getNameAssignment_2()); 
            }
            // InternalScenarioExpressions.g:2135:1: ( rule__ClockDeclaration__NameAssignment_2 )
            // InternalScenarioExpressions.g:2135:2: rule__ClockDeclaration__NameAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ClockDeclaration__NameAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClockDeclarationAccess().getNameAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockDeclaration__Group__2__Impl"


    // $ANTLR start "rule__ClockDeclaration__Group__3"
    // InternalScenarioExpressions.g:2145:1: rule__ClockDeclaration__Group__3 : rule__ClockDeclaration__Group__3__Impl ;
    public final void rule__ClockDeclaration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2149:1: ( rule__ClockDeclaration__Group__3__Impl )
            // InternalScenarioExpressions.g:2150:2: rule__ClockDeclaration__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ClockDeclaration__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockDeclaration__Group__3"


    // $ANTLR start "rule__ClockDeclaration__Group__3__Impl"
    // InternalScenarioExpressions.g:2156:1: rule__ClockDeclaration__Group__3__Impl : ( ( rule__ClockDeclaration__Group_3__0 )? ) ;
    public final void rule__ClockDeclaration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2160:1: ( ( ( rule__ClockDeclaration__Group_3__0 )? ) )
            // InternalScenarioExpressions.g:2161:1: ( ( rule__ClockDeclaration__Group_3__0 )? )
            {
            // InternalScenarioExpressions.g:2161:1: ( ( rule__ClockDeclaration__Group_3__0 )? )
            // InternalScenarioExpressions.g:2162:1: ( rule__ClockDeclaration__Group_3__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClockDeclarationAccess().getGroup_3()); 
            }
            // InternalScenarioExpressions.g:2163:1: ( rule__ClockDeclaration__Group_3__0 )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==38) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalScenarioExpressions.g:2163:2: rule__ClockDeclaration__Group_3__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__ClockDeclaration__Group_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClockDeclarationAccess().getGroup_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockDeclaration__Group__3__Impl"


    // $ANTLR start "rule__ClockDeclaration__Group_3__0"
    // InternalScenarioExpressions.g:2181:1: rule__ClockDeclaration__Group_3__0 : rule__ClockDeclaration__Group_3__0__Impl rule__ClockDeclaration__Group_3__1 ;
    public final void rule__ClockDeclaration__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2185:1: ( rule__ClockDeclaration__Group_3__0__Impl rule__ClockDeclaration__Group_3__1 )
            // InternalScenarioExpressions.g:2186:2: rule__ClockDeclaration__Group_3__0__Impl rule__ClockDeclaration__Group_3__1
            {
            pushFollow(FollowSets000.FOLLOW_16);
            rule__ClockDeclaration__Group_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ClockDeclaration__Group_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockDeclaration__Group_3__0"


    // $ANTLR start "rule__ClockDeclaration__Group_3__0__Impl"
    // InternalScenarioExpressions.g:2193:1: rule__ClockDeclaration__Group_3__0__Impl : ( '=' ) ;
    public final void rule__ClockDeclaration__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2197:1: ( ( '=' ) )
            // InternalScenarioExpressions.g:2198:1: ( '=' )
            {
            // InternalScenarioExpressions.g:2198:1: ( '=' )
            // InternalScenarioExpressions.g:2199:1: '='
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClockDeclarationAccess().getEqualsSignKeyword_3_0()); 
            }
            match(input,38,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClockDeclarationAccess().getEqualsSignKeyword_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockDeclaration__Group_3__0__Impl"


    // $ANTLR start "rule__ClockDeclaration__Group_3__1"
    // InternalScenarioExpressions.g:2212:1: rule__ClockDeclaration__Group_3__1 : rule__ClockDeclaration__Group_3__1__Impl ;
    public final void rule__ClockDeclaration__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2216:1: ( rule__ClockDeclaration__Group_3__1__Impl )
            // InternalScenarioExpressions.g:2217:2: rule__ClockDeclaration__Group_3__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ClockDeclaration__Group_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockDeclaration__Group_3__1"


    // $ANTLR start "rule__ClockDeclaration__Group_3__1__Impl"
    // InternalScenarioExpressions.g:2223:1: rule__ClockDeclaration__Group_3__1__Impl : ( ( rule__ClockDeclaration__ExpressionAssignment_3_1 ) ) ;
    public final void rule__ClockDeclaration__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2227:1: ( ( ( rule__ClockDeclaration__ExpressionAssignment_3_1 ) ) )
            // InternalScenarioExpressions.g:2228:1: ( ( rule__ClockDeclaration__ExpressionAssignment_3_1 ) )
            {
            // InternalScenarioExpressions.g:2228:1: ( ( rule__ClockDeclaration__ExpressionAssignment_3_1 ) )
            // InternalScenarioExpressions.g:2229:1: ( rule__ClockDeclaration__ExpressionAssignment_3_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClockDeclarationAccess().getExpressionAssignment_3_1()); 
            }
            // InternalScenarioExpressions.g:2230:1: ( rule__ClockDeclaration__ExpressionAssignment_3_1 )
            // InternalScenarioExpressions.g:2230:2: rule__ClockDeclaration__ExpressionAssignment_3_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ClockDeclaration__ExpressionAssignment_3_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClockDeclarationAccess().getExpressionAssignment_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockDeclaration__Group_3__1__Impl"


    // $ANTLR start "rule__ClockAssignment__Group__0"
    // InternalScenarioExpressions.g:2245:1: rule__ClockAssignment__Group__0 : rule__ClockAssignment__Group__0__Impl rule__ClockAssignment__Group__1 ;
    public final void rule__ClockAssignment__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2249:1: ( rule__ClockAssignment__Group__0__Impl rule__ClockAssignment__Group__1 )
            // InternalScenarioExpressions.g:2250:2: rule__ClockAssignment__Group__0__Impl rule__ClockAssignment__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_7);
            rule__ClockAssignment__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ClockAssignment__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockAssignment__Group__0"


    // $ANTLR start "rule__ClockAssignment__Group__0__Impl"
    // InternalScenarioExpressions.g:2257:1: rule__ClockAssignment__Group__0__Impl : ( 'reset clock' ) ;
    public final void rule__ClockAssignment__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2261:1: ( ( 'reset clock' ) )
            // InternalScenarioExpressions.g:2262:1: ( 'reset clock' )
            {
            // InternalScenarioExpressions.g:2262:1: ( 'reset clock' )
            // InternalScenarioExpressions.g:2263:1: 'reset clock'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClockAssignmentAccess().getResetClockKeyword_0()); 
            }
            match(input,41,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClockAssignmentAccess().getResetClockKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockAssignment__Group__0__Impl"


    // $ANTLR start "rule__ClockAssignment__Group__1"
    // InternalScenarioExpressions.g:2276:1: rule__ClockAssignment__Group__1 : rule__ClockAssignment__Group__1__Impl rule__ClockAssignment__Group__2 ;
    public final void rule__ClockAssignment__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2280:1: ( rule__ClockAssignment__Group__1__Impl rule__ClockAssignment__Group__2 )
            // InternalScenarioExpressions.g:2281:2: rule__ClockAssignment__Group__1__Impl rule__ClockAssignment__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_13);
            rule__ClockAssignment__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ClockAssignment__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockAssignment__Group__1"


    // $ANTLR start "rule__ClockAssignment__Group__1__Impl"
    // InternalScenarioExpressions.g:2288:1: rule__ClockAssignment__Group__1__Impl : ( ( rule__ClockAssignment__VariableAssignment_1 ) ) ;
    public final void rule__ClockAssignment__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2292:1: ( ( ( rule__ClockAssignment__VariableAssignment_1 ) ) )
            // InternalScenarioExpressions.g:2293:1: ( ( rule__ClockAssignment__VariableAssignment_1 ) )
            {
            // InternalScenarioExpressions.g:2293:1: ( ( rule__ClockAssignment__VariableAssignment_1 ) )
            // InternalScenarioExpressions.g:2294:1: ( rule__ClockAssignment__VariableAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClockAssignmentAccess().getVariableAssignment_1()); 
            }
            // InternalScenarioExpressions.g:2295:1: ( rule__ClockAssignment__VariableAssignment_1 )
            // InternalScenarioExpressions.g:2295:2: rule__ClockAssignment__VariableAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ClockAssignment__VariableAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClockAssignmentAccess().getVariableAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockAssignment__Group__1__Impl"


    // $ANTLR start "rule__ClockAssignment__Group__2"
    // InternalScenarioExpressions.g:2305:1: rule__ClockAssignment__Group__2 : rule__ClockAssignment__Group__2__Impl ;
    public final void rule__ClockAssignment__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2309:1: ( rule__ClockAssignment__Group__2__Impl )
            // InternalScenarioExpressions.g:2310:2: rule__ClockAssignment__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ClockAssignment__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockAssignment__Group__2"


    // $ANTLR start "rule__ClockAssignment__Group__2__Impl"
    // InternalScenarioExpressions.g:2316:1: rule__ClockAssignment__Group__2__Impl : ( ( rule__ClockAssignment__Group_2__0 )? ) ;
    public final void rule__ClockAssignment__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2320:1: ( ( ( rule__ClockAssignment__Group_2__0 )? ) )
            // InternalScenarioExpressions.g:2321:1: ( ( rule__ClockAssignment__Group_2__0 )? )
            {
            // InternalScenarioExpressions.g:2321:1: ( ( rule__ClockAssignment__Group_2__0 )? )
            // InternalScenarioExpressions.g:2322:1: ( rule__ClockAssignment__Group_2__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClockAssignmentAccess().getGroup_2()); 
            }
            // InternalScenarioExpressions.g:2323:1: ( rule__ClockAssignment__Group_2__0 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==38) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalScenarioExpressions.g:2323:2: rule__ClockAssignment__Group_2__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__ClockAssignment__Group_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClockAssignmentAccess().getGroup_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockAssignment__Group__2__Impl"


    // $ANTLR start "rule__ClockAssignment__Group_2__0"
    // InternalScenarioExpressions.g:2339:1: rule__ClockAssignment__Group_2__0 : rule__ClockAssignment__Group_2__0__Impl rule__ClockAssignment__Group_2__1 ;
    public final void rule__ClockAssignment__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2343:1: ( rule__ClockAssignment__Group_2__0__Impl rule__ClockAssignment__Group_2__1 )
            // InternalScenarioExpressions.g:2344:2: rule__ClockAssignment__Group_2__0__Impl rule__ClockAssignment__Group_2__1
            {
            pushFollow(FollowSets000.FOLLOW_16);
            rule__ClockAssignment__Group_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ClockAssignment__Group_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockAssignment__Group_2__0"


    // $ANTLR start "rule__ClockAssignment__Group_2__0__Impl"
    // InternalScenarioExpressions.g:2351:1: rule__ClockAssignment__Group_2__0__Impl : ( '=' ) ;
    public final void rule__ClockAssignment__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2355:1: ( ( '=' ) )
            // InternalScenarioExpressions.g:2356:1: ( '=' )
            {
            // InternalScenarioExpressions.g:2356:1: ( '=' )
            // InternalScenarioExpressions.g:2357:1: '='
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClockAssignmentAccess().getEqualsSignKeyword_2_0()); 
            }
            match(input,38,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClockAssignmentAccess().getEqualsSignKeyword_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockAssignment__Group_2__0__Impl"


    // $ANTLR start "rule__ClockAssignment__Group_2__1"
    // InternalScenarioExpressions.g:2370:1: rule__ClockAssignment__Group_2__1 : rule__ClockAssignment__Group_2__1__Impl ;
    public final void rule__ClockAssignment__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2374:1: ( rule__ClockAssignment__Group_2__1__Impl )
            // InternalScenarioExpressions.g:2375:2: rule__ClockAssignment__Group_2__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ClockAssignment__Group_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockAssignment__Group_2__1"


    // $ANTLR start "rule__ClockAssignment__Group_2__1__Impl"
    // InternalScenarioExpressions.g:2381:1: rule__ClockAssignment__Group_2__1__Impl : ( ( rule__ClockAssignment__ExpressionAssignment_2_1 ) ) ;
    public final void rule__ClockAssignment__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2385:1: ( ( ( rule__ClockAssignment__ExpressionAssignment_2_1 ) ) )
            // InternalScenarioExpressions.g:2386:1: ( ( rule__ClockAssignment__ExpressionAssignment_2_1 ) )
            {
            // InternalScenarioExpressions.g:2386:1: ( ( rule__ClockAssignment__ExpressionAssignment_2_1 ) )
            // InternalScenarioExpressions.g:2387:1: ( rule__ClockAssignment__ExpressionAssignment_2_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClockAssignmentAccess().getExpressionAssignment_2_1()); 
            }
            // InternalScenarioExpressions.g:2388:1: ( rule__ClockAssignment__ExpressionAssignment_2_1 )
            // InternalScenarioExpressions.g:2388:2: rule__ClockAssignment__ExpressionAssignment_2_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ClockAssignment__ExpressionAssignment_2_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClockAssignmentAccess().getExpressionAssignment_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockAssignment__Group_2__1__Impl"


    // $ANTLR start "rule__ImplicationExpression__Group__0"
    // InternalScenarioExpressions.g:2402:1: rule__ImplicationExpression__Group__0 : rule__ImplicationExpression__Group__0__Impl rule__ImplicationExpression__Group__1 ;
    public final void rule__ImplicationExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2406:1: ( rule__ImplicationExpression__Group__0__Impl rule__ImplicationExpression__Group__1 )
            // InternalScenarioExpressions.g:2407:2: rule__ImplicationExpression__Group__0__Impl rule__ImplicationExpression__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_17);
            rule__ImplicationExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ImplicationExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImplicationExpression__Group__0"


    // $ANTLR start "rule__ImplicationExpression__Group__0__Impl"
    // InternalScenarioExpressions.g:2414:1: rule__ImplicationExpression__Group__0__Impl : ( ruleDisjunctionExpression ) ;
    public final void rule__ImplicationExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2418:1: ( ( ruleDisjunctionExpression ) )
            // InternalScenarioExpressions.g:2419:1: ( ruleDisjunctionExpression )
            {
            // InternalScenarioExpressions.g:2419:1: ( ruleDisjunctionExpression )
            // InternalScenarioExpressions.g:2420:1: ruleDisjunctionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImplicationExpressionAccess().getDisjunctionExpressionParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImplicationExpressionAccess().getDisjunctionExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImplicationExpression__Group__0__Impl"


    // $ANTLR start "rule__ImplicationExpression__Group__1"
    // InternalScenarioExpressions.g:2431:1: rule__ImplicationExpression__Group__1 : rule__ImplicationExpression__Group__1__Impl ;
    public final void rule__ImplicationExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2435:1: ( rule__ImplicationExpression__Group__1__Impl )
            // InternalScenarioExpressions.g:2436:2: rule__ImplicationExpression__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ImplicationExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImplicationExpression__Group__1"


    // $ANTLR start "rule__ImplicationExpression__Group__1__Impl"
    // InternalScenarioExpressions.g:2442:1: rule__ImplicationExpression__Group__1__Impl : ( ( rule__ImplicationExpression__Group_1__0 )? ) ;
    public final void rule__ImplicationExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2446:1: ( ( ( rule__ImplicationExpression__Group_1__0 )? ) )
            // InternalScenarioExpressions.g:2447:1: ( ( rule__ImplicationExpression__Group_1__0 )? )
            {
            // InternalScenarioExpressions.g:2447:1: ( ( rule__ImplicationExpression__Group_1__0 )? )
            // InternalScenarioExpressions.g:2448:1: ( rule__ImplicationExpression__Group_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImplicationExpressionAccess().getGroup_1()); 
            }
            // InternalScenarioExpressions.g:2449:1: ( rule__ImplicationExpression__Group_1__0 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==48) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalScenarioExpressions.g:2449:2: rule__ImplicationExpression__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__ImplicationExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImplicationExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImplicationExpression__Group__1__Impl"


    // $ANTLR start "rule__ImplicationExpression__Group_1__0"
    // InternalScenarioExpressions.g:2463:1: rule__ImplicationExpression__Group_1__0 : rule__ImplicationExpression__Group_1__0__Impl rule__ImplicationExpression__Group_1__1 ;
    public final void rule__ImplicationExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2467:1: ( rule__ImplicationExpression__Group_1__0__Impl rule__ImplicationExpression__Group_1__1 )
            // InternalScenarioExpressions.g:2468:2: rule__ImplicationExpression__Group_1__0__Impl rule__ImplicationExpression__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_17);
            rule__ImplicationExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ImplicationExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImplicationExpression__Group_1__0"


    // $ANTLR start "rule__ImplicationExpression__Group_1__0__Impl"
    // InternalScenarioExpressions.g:2475:1: rule__ImplicationExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__ImplicationExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2479:1: ( ( () ) )
            // InternalScenarioExpressions.g:2480:1: ( () )
            {
            // InternalScenarioExpressions.g:2480:1: ( () )
            // InternalScenarioExpressions.g:2481:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }
            // InternalScenarioExpressions.g:2482:1: ()
            // InternalScenarioExpressions.g:2484:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImplicationExpression__Group_1__0__Impl"


    // $ANTLR start "rule__ImplicationExpression__Group_1__1"
    // InternalScenarioExpressions.g:2494:1: rule__ImplicationExpression__Group_1__1 : rule__ImplicationExpression__Group_1__1__Impl rule__ImplicationExpression__Group_1__2 ;
    public final void rule__ImplicationExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2498:1: ( rule__ImplicationExpression__Group_1__1__Impl rule__ImplicationExpression__Group_1__2 )
            // InternalScenarioExpressions.g:2499:2: rule__ImplicationExpression__Group_1__1__Impl rule__ImplicationExpression__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__ImplicationExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ImplicationExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImplicationExpression__Group_1__1"


    // $ANTLR start "rule__ImplicationExpression__Group_1__1__Impl"
    // InternalScenarioExpressions.g:2506:1: rule__ImplicationExpression__Group_1__1__Impl : ( ( rule__ImplicationExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__ImplicationExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2510:1: ( ( ( rule__ImplicationExpression__OperatorAssignment_1_1 ) ) )
            // InternalScenarioExpressions.g:2511:1: ( ( rule__ImplicationExpression__OperatorAssignment_1_1 ) )
            {
            // InternalScenarioExpressions.g:2511:1: ( ( rule__ImplicationExpression__OperatorAssignment_1_1 ) )
            // InternalScenarioExpressions.g:2512:1: ( rule__ImplicationExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImplicationExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalScenarioExpressions.g:2513:1: ( rule__ImplicationExpression__OperatorAssignment_1_1 )
            // InternalScenarioExpressions.g:2513:2: rule__ImplicationExpression__OperatorAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ImplicationExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImplicationExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImplicationExpression__Group_1__1__Impl"


    // $ANTLR start "rule__ImplicationExpression__Group_1__2"
    // InternalScenarioExpressions.g:2523:1: rule__ImplicationExpression__Group_1__2 : rule__ImplicationExpression__Group_1__2__Impl ;
    public final void rule__ImplicationExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2527:1: ( rule__ImplicationExpression__Group_1__2__Impl )
            // InternalScenarioExpressions.g:2528:2: rule__ImplicationExpression__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ImplicationExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImplicationExpression__Group_1__2"


    // $ANTLR start "rule__ImplicationExpression__Group_1__2__Impl"
    // InternalScenarioExpressions.g:2534:1: rule__ImplicationExpression__Group_1__2__Impl : ( ( rule__ImplicationExpression__RightAssignment_1_2 ) ) ;
    public final void rule__ImplicationExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2538:1: ( ( ( rule__ImplicationExpression__RightAssignment_1_2 ) ) )
            // InternalScenarioExpressions.g:2539:1: ( ( rule__ImplicationExpression__RightAssignment_1_2 ) )
            {
            // InternalScenarioExpressions.g:2539:1: ( ( rule__ImplicationExpression__RightAssignment_1_2 ) )
            // InternalScenarioExpressions.g:2540:1: ( rule__ImplicationExpression__RightAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImplicationExpressionAccess().getRightAssignment_1_2()); 
            }
            // InternalScenarioExpressions.g:2541:1: ( rule__ImplicationExpression__RightAssignment_1_2 )
            // InternalScenarioExpressions.g:2541:2: rule__ImplicationExpression__RightAssignment_1_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ImplicationExpression__RightAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImplicationExpressionAccess().getRightAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImplicationExpression__Group_1__2__Impl"


    // $ANTLR start "rule__DisjunctionExpression__Group__0"
    // InternalScenarioExpressions.g:2557:1: rule__DisjunctionExpression__Group__0 : rule__DisjunctionExpression__Group__0__Impl rule__DisjunctionExpression__Group__1 ;
    public final void rule__DisjunctionExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2561:1: ( rule__DisjunctionExpression__Group__0__Impl rule__DisjunctionExpression__Group__1 )
            // InternalScenarioExpressions.g:2562:2: rule__DisjunctionExpression__Group__0__Impl rule__DisjunctionExpression__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_18);
            rule__DisjunctionExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group__0"


    // $ANTLR start "rule__DisjunctionExpression__Group__0__Impl"
    // InternalScenarioExpressions.g:2569:1: rule__DisjunctionExpression__Group__0__Impl : ( ruleConjunctionExpression ) ;
    public final void rule__DisjunctionExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2573:1: ( ( ruleConjunctionExpression ) )
            // InternalScenarioExpressions.g:2574:1: ( ruleConjunctionExpression )
            {
            // InternalScenarioExpressions.g:2574:1: ( ruleConjunctionExpression )
            // InternalScenarioExpressions.g:2575:1: ruleConjunctionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getConjunctionExpressionParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getConjunctionExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group__0__Impl"


    // $ANTLR start "rule__DisjunctionExpression__Group__1"
    // InternalScenarioExpressions.g:2586:1: rule__DisjunctionExpression__Group__1 : rule__DisjunctionExpression__Group__1__Impl ;
    public final void rule__DisjunctionExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2590:1: ( rule__DisjunctionExpression__Group__1__Impl )
            // InternalScenarioExpressions.g:2591:2: rule__DisjunctionExpression__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group__1"


    // $ANTLR start "rule__DisjunctionExpression__Group__1__Impl"
    // InternalScenarioExpressions.g:2597:1: rule__DisjunctionExpression__Group__1__Impl : ( ( rule__DisjunctionExpression__Group_1__0 )? ) ;
    public final void rule__DisjunctionExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2601:1: ( ( ( rule__DisjunctionExpression__Group_1__0 )? ) )
            // InternalScenarioExpressions.g:2602:1: ( ( rule__DisjunctionExpression__Group_1__0 )? )
            {
            // InternalScenarioExpressions.g:2602:1: ( ( rule__DisjunctionExpression__Group_1__0 )? )
            // InternalScenarioExpressions.g:2603:1: ( rule__DisjunctionExpression__Group_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getGroup_1()); 
            }
            // InternalScenarioExpressions.g:2604:1: ( rule__DisjunctionExpression__Group_1__0 )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==49) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalScenarioExpressions.g:2604:2: rule__DisjunctionExpression__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__DisjunctionExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group__1__Impl"


    // $ANTLR start "rule__DisjunctionExpression__Group_1__0"
    // InternalScenarioExpressions.g:2618:1: rule__DisjunctionExpression__Group_1__0 : rule__DisjunctionExpression__Group_1__0__Impl rule__DisjunctionExpression__Group_1__1 ;
    public final void rule__DisjunctionExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2622:1: ( rule__DisjunctionExpression__Group_1__0__Impl rule__DisjunctionExpression__Group_1__1 )
            // InternalScenarioExpressions.g:2623:2: rule__DisjunctionExpression__Group_1__0__Impl rule__DisjunctionExpression__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_18);
            rule__DisjunctionExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group_1__0"


    // $ANTLR start "rule__DisjunctionExpression__Group_1__0__Impl"
    // InternalScenarioExpressions.g:2630:1: rule__DisjunctionExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__DisjunctionExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2634:1: ( ( () ) )
            // InternalScenarioExpressions.g:2635:1: ( () )
            {
            // InternalScenarioExpressions.g:2635:1: ( () )
            // InternalScenarioExpressions.g:2636:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }
            // InternalScenarioExpressions.g:2637:1: ()
            // InternalScenarioExpressions.g:2639:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group_1__0__Impl"


    // $ANTLR start "rule__DisjunctionExpression__Group_1__1"
    // InternalScenarioExpressions.g:2649:1: rule__DisjunctionExpression__Group_1__1 : rule__DisjunctionExpression__Group_1__1__Impl rule__DisjunctionExpression__Group_1__2 ;
    public final void rule__DisjunctionExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2653:1: ( rule__DisjunctionExpression__Group_1__1__Impl rule__DisjunctionExpression__Group_1__2 )
            // InternalScenarioExpressions.g:2654:2: rule__DisjunctionExpression__Group_1__1__Impl rule__DisjunctionExpression__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__DisjunctionExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group_1__1"


    // $ANTLR start "rule__DisjunctionExpression__Group_1__1__Impl"
    // InternalScenarioExpressions.g:2661:1: rule__DisjunctionExpression__Group_1__1__Impl : ( ( rule__DisjunctionExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__DisjunctionExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2665:1: ( ( ( rule__DisjunctionExpression__OperatorAssignment_1_1 ) ) )
            // InternalScenarioExpressions.g:2666:1: ( ( rule__DisjunctionExpression__OperatorAssignment_1_1 ) )
            {
            // InternalScenarioExpressions.g:2666:1: ( ( rule__DisjunctionExpression__OperatorAssignment_1_1 ) )
            // InternalScenarioExpressions.g:2667:1: ( rule__DisjunctionExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalScenarioExpressions.g:2668:1: ( rule__DisjunctionExpression__OperatorAssignment_1_1 )
            // InternalScenarioExpressions.g:2668:2: rule__DisjunctionExpression__OperatorAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group_1__1__Impl"


    // $ANTLR start "rule__DisjunctionExpression__Group_1__2"
    // InternalScenarioExpressions.g:2678:1: rule__DisjunctionExpression__Group_1__2 : rule__DisjunctionExpression__Group_1__2__Impl ;
    public final void rule__DisjunctionExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2682:1: ( rule__DisjunctionExpression__Group_1__2__Impl )
            // InternalScenarioExpressions.g:2683:2: rule__DisjunctionExpression__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group_1__2"


    // $ANTLR start "rule__DisjunctionExpression__Group_1__2__Impl"
    // InternalScenarioExpressions.g:2689:1: rule__DisjunctionExpression__Group_1__2__Impl : ( ( rule__DisjunctionExpression__RightAssignment_1_2 ) ) ;
    public final void rule__DisjunctionExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2693:1: ( ( ( rule__DisjunctionExpression__RightAssignment_1_2 ) ) )
            // InternalScenarioExpressions.g:2694:1: ( ( rule__DisjunctionExpression__RightAssignment_1_2 ) )
            {
            // InternalScenarioExpressions.g:2694:1: ( ( rule__DisjunctionExpression__RightAssignment_1_2 ) )
            // InternalScenarioExpressions.g:2695:1: ( rule__DisjunctionExpression__RightAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getRightAssignment_1_2()); 
            }
            // InternalScenarioExpressions.g:2696:1: ( rule__DisjunctionExpression__RightAssignment_1_2 )
            // InternalScenarioExpressions.g:2696:2: rule__DisjunctionExpression__RightAssignment_1_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__RightAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getRightAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group_1__2__Impl"


    // $ANTLR start "rule__ConjunctionExpression__Group__0"
    // InternalScenarioExpressions.g:2712:1: rule__ConjunctionExpression__Group__0 : rule__ConjunctionExpression__Group__0__Impl rule__ConjunctionExpression__Group__1 ;
    public final void rule__ConjunctionExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2716:1: ( rule__ConjunctionExpression__Group__0__Impl rule__ConjunctionExpression__Group__1 )
            // InternalScenarioExpressions.g:2717:2: rule__ConjunctionExpression__Group__0__Impl rule__ConjunctionExpression__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_19);
            rule__ConjunctionExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group__0"


    // $ANTLR start "rule__ConjunctionExpression__Group__0__Impl"
    // InternalScenarioExpressions.g:2724:1: rule__ConjunctionExpression__Group__0__Impl : ( ruleRelationExpression ) ;
    public final void rule__ConjunctionExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2728:1: ( ( ruleRelationExpression ) )
            // InternalScenarioExpressions.g:2729:1: ( ruleRelationExpression )
            {
            // InternalScenarioExpressions.g:2729:1: ( ruleRelationExpression )
            // InternalScenarioExpressions.g:2730:1: ruleRelationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getRelationExpressionParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleRelationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getRelationExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group__0__Impl"


    // $ANTLR start "rule__ConjunctionExpression__Group__1"
    // InternalScenarioExpressions.g:2741:1: rule__ConjunctionExpression__Group__1 : rule__ConjunctionExpression__Group__1__Impl ;
    public final void rule__ConjunctionExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2745:1: ( rule__ConjunctionExpression__Group__1__Impl )
            // InternalScenarioExpressions.g:2746:2: rule__ConjunctionExpression__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group__1"


    // $ANTLR start "rule__ConjunctionExpression__Group__1__Impl"
    // InternalScenarioExpressions.g:2752:1: rule__ConjunctionExpression__Group__1__Impl : ( ( rule__ConjunctionExpression__Group_1__0 )? ) ;
    public final void rule__ConjunctionExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2756:1: ( ( ( rule__ConjunctionExpression__Group_1__0 )? ) )
            // InternalScenarioExpressions.g:2757:1: ( ( rule__ConjunctionExpression__Group_1__0 )? )
            {
            // InternalScenarioExpressions.g:2757:1: ( ( rule__ConjunctionExpression__Group_1__0 )? )
            // InternalScenarioExpressions.g:2758:1: ( rule__ConjunctionExpression__Group_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getGroup_1()); 
            }
            // InternalScenarioExpressions.g:2759:1: ( rule__ConjunctionExpression__Group_1__0 )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==50) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalScenarioExpressions.g:2759:2: rule__ConjunctionExpression__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__ConjunctionExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group__1__Impl"


    // $ANTLR start "rule__ConjunctionExpression__Group_1__0"
    // InternalScenarioExpressions.g:2773:1: rule__ConjunctionExpression__Group_1__0 : rule__ConjunctionExpression__Group_1__0__Impl rule__ConjunctionExpression__Group_1__1 ;
    public final void rule__ConjunctionExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2777:1: ( rule__ConjunctionExpression__Group_1__0__Impl rule__ConjunctionExpression__Group_1__1 )
            // InternalScenarioExpressions.g:2778:2: rule__ConjunctionExpression__Group_1__0__Impl rule__ConjunctionExpression__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_19);
            rule__ConjunctionExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group_1__0"


    // $ANTLR start "rule__ConjunctionExpression__Group_1__0__Impl"
    // InternalScenarioExpressions.g:2785:1: rule__ConjunctionExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__ConjunctionExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2789:1: ( ( () ) )
            // InternalScenarioExpressions.g:2790:1: ( () )
            {
            // InternalScenarioExpressions.g:2790:1: ( () )
            // InternalScenarioExpressions.g:2791:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }
            // InternalScenarioExpressions.g:2792:1: ()
            // InternalScenarioExpressions.g:2794:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group_1__0__Impl"


    // $ANTLR start "rule__ConjunctionExpression__Group_1__1"
    // InternalScenarioExpressions.g:2804:1: rule__ConjunctionExpression__Group_1__1 : rule__ConjunctionExpression__Group_1__1__Impl rule__ConjunctionExpression__Group_1__2 ;
    public final void rule__ConjunctionExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2808:1: ( rule__ConjunctionExpression__Group_1__1__Impl rule__ConjunctionExpression__Group_1__2 )
            // InternalScenarioExpressions.g:2809:2: rule__ConjunctionExpression__Group_1__1__Impl rule__ConjunctionExpression__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__ConjunctionExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group_1__1"


    // $ANTLR start "rule__ConjunctionExpression__Group_1__1__Impl"
    // InternalScenarioExpressions.g:2816:1: rule__ConjunctionExpression__Group_1__1__Impl : ( ( rule__ConjunctionExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__ConjunctionExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2820:1: ( ( ( rule__ConjunctionExpression__OperatorAssignment_1_1 ) ) )
            // InternalScenarioExpressions.g:2821:1: ( ( rule__ConjunctionExpression__OperatorAssignment_1_1 ) )
            {
            // InternalScenarioExpressions.g:2821:1: ( ( rule__ConjunctionExpression__OperatorAssignment_1_1 ) )
            // InternalScenarioExpressions.g:2822:1: ( rule__ConjunctionExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalScenarioExpressions.g:2823:1: ( rule__ConjunctionExpression__OperatorAssignment_1_1 )
            // InternalScenarioExpressions.g:2823:2: rule__ConjunctionExpression__OperatorAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group_1__1__Impl"


    // $ANTLR start "rule__ConjunctionExpression__Group_1__2"
    // InternalScenarioExpressions.g:2833:1: rule__ConjunctionExpression__Group_1__2 : rule__ConjunctionExpression__Group_1__2__Impl ;
    public final void rule__ConjunctionExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2837:1: ( rule__ConjunctionExpression__Group_1__2__Impl )
            // InternalScenarioExpressions.g:2838:2: rule__ConjunctionExpression__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group_1__2"


    // $ANTLR start "rule__ConjunctionExpression__Group_1__2__Impl"
    // InternalScenarioExpressions.g:2844:1: rule__ConjunctionExpression__Group_1__2__Impl : ( ( rule__ConjunctionExpression__RightAssignment_1_2 ) ) ;
    public final void rule__ConjunctionExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2848:1: ( ( ( rule__ConjunctionExpression__RightAssignment_1_2 ) ) )
            // InternalScenarioExpressions.g:2849:1: ( ( rule__ConjunctionExpression__RightAssignment_1_2 ) )
            {
            // InternalScenarioExpressions.g:2849:1: ( ( rule__ConjunctionExpression__RightAssignment_1_2 ) )
            // InternalScenarioExpressions.g:2850:1: ( rule__ConjunctionExpression__RightAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getRightAssignment_1_2()); 
            }
            // InternalScenarioExpressions.g:2851:1: ( rule__ConjunctionExpression__RightAssignment_1_2 )
            // InternalScenarioExpressions.g:2851:2: rule__ConjunctionExpression__RightAssignment_1_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__RightAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getRightAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group_1__2__Impl"


    // $ANTLR start "rule__RelationExpression__Group__0"
    // InternalScenarioExpressions.g:2867:1: rule__RelationExpression__Group__0 : rule__RelationExpression__Group__0__Impl rule__RelationExpression__Group__1 ;
    public final void rule__RelationExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2871:1: ( rule__RelationExpression__Group__0__Impl rule__RelationExpression__Group__1 )
            // InternalScenarioExpressions.g:2872:2: rule__RelationExpression__Group__0__Impl rule__RelationExpression__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_20);
            rule__RelationExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group__0"


    // $ANTLR start "rule__RelationExpression__Group__0__Impl"
    // InternalScenarioExpressions.g:2879:1: rule__RelationExpression__Group__0__Impl : ( ruleAdditionExpression ) ;
    public final void rule__RelationExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2883:1: ( ( ruleAdditionExpression ) )
            // InternalScenarioExpressions.g:2884:1: ( ruleAdditionExpression )
            {
            // InternalScenarioExpressions.g:2884:1: ( ruleAdditionExpression )
            // InternalScenarioExpressions.g:2885:1: ruleAdditionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getAdditionExpressionParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getAdditionExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group__0__Impl"


    // $ANTLR start "rule__RelationExpression__Group__1"
    // InternalScenarioExpressions.g:2896:1: rule__RelationExpression__Group__1 : rule__RelationExpression__Group__1__Impl ;
    public final void rule__RelationExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2900:1: ( rule__RelationExpression__Group__1__Impl )
            // InternalScenarioExpressions.g:2901:2: rule__RelationExpression__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group__1"


    // $ANTLR start "rule__RelationExpression__Group__1__Impl"
    // InternalScenarioExpressions.g:2907:1: rule__RelationExpression__Group__1__Impl : ( ( rule__RelationExpression__Group_1__0 )? ) ;
    public final void rule__RelationExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2911:1: ( ( ( rule__RelationExpression__Group_1__0 )? ) )
            // InternalScenarioExpressions.g:2912:1: ( ( rule__RelationExpression__Group_1__0 )? )
            {
            // InternalScenarioExpressions.g:2912:1: ( ( rule__RelationExpression__Group_1__0 )? )
            // InternalScenarioExpressions.g:2913:1: ( rule__RelationExpression__Group_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getGroup_1()); 
            }
            // InternalScenarioExpressions.g:2914:1: ( rule__RelationExpression__Group_1__0 )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( ((LA24_0>=14 && LA24_0<=19)) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalScenarioExpressions.g:2914:2: rule__RelationExpression__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__RelationExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group__1__Impl"


    // $ANTLR start "rule__RelationExpression__Group_1__0"
    // InternalScenarioExpressions.g:2928:1: rule__RelationExpression__Group_1__0 : rule__RelationExpression__Group_1__0__Impl rule__RelationExpression__Group_1__1 ;
    public final void rule__RelationExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2932:1: ( rule__RelationExpression__Group_1__0__Impl rule__RelationExpression__Group_1__1 )
            // InternalScenarioExpressions.g:2933:2: rule__RelationExpression__Group_1__0__Impl rule__RelationExpression__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_20);
            rule__RelationExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group_1__0"


    // $ANTLR start "rule__RelationExpression__Group_1__0__Impl"
    // InternalScenarioExpressions.g:2940:1: rule__RelationExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__RelationExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2944:1: ( ( () ) )
            // InternalScenarioExpressions.g:2945:1: ( () )
            {
            // InternalScenarioExpressions.g:2945:1: ( () )
            // InternalScenarioExpressions.g:2946:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }
            // InternalScenarioExpressions.g:2947:1: ()
            // InternalScenarioExpressions.g:2949:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group_1__0__Impl"


    // $ANTLR start "rule__RelationExpression__Group_1__1"
    // InternalScenarioExpressions.g:2959:1: rule__RelationExpression__Group_1__1 : rule__RelationExpression__Group_1__1__Impl rule__RelationExpression__Group_1__2 ;
    public final void rule__RelationExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2963:1: ( rule__RelationExpression__Group_1__1__Impl rule__RelationExpression__Group_1__2 )
            // InternalScenarioExpressions.g:2964:2: rule__RelationExpression__Group_1__1__Impl rule__RelationExpression__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__RelationExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group_1__1"


    // $ANTLR start "rule__RelationExpression__Group_1__1__Impl"
    // InternalScenarioExpressions.g:2971:1: rule__RelationExpression__Group_1__1__Impl : ( ( rule__RelationExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__RelationExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2975:1: ( ( ( rule__RelationExpression__OperatorAssignment_1_1 ) ) )
            // InternalScenarioExpressions.g:2976:1: ( ( rule__RelationExpression__OperatorAssignment_1_1 ) )
            {
            // InternalScenarioExpressions.g:2976:1: ( ( rule__RelationExpression__OperatorAssignment_1_1 ) )
            // InternalScenarioExpressions.g:2977:1: ( rule__RelationExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalScenarioExpressions.g:2978:1: ( rule__RelationExpression__OperatorAssignment_1_1 )
            // InternalScenarioExpressions.g:2978:2: rule__RelationExpression__OperatorAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group_1__1__Impl"


    // $ANTLR start "rule__RelationExpression__Group_1__2"
    // InternalScenarioExpressions.g:2988:1: rule__RelationExpression__Group_1__2 : rule__RelationExpression__Group_1__2__Impl ;
    public final void rule__RelationExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2992:1: ( rule__RelationExpression__Group_1__2__Impl )
            // InternalScenarioExpressions.g:2993:2: rule__RelationExpression__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group_1__2"


    // $ANTLR start "rule__RelationExpression__Group_1__2__Impl"
    // InternalScenarioExpressions.g:2999:1: rule__RelationExpression__Group_1__2__Impl : ( ( rule__RelationExpression__RightAssignment_1_2 ) ) ;
    public final void rule__RelationExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3003:1: ( ( ( rule__RelationExpression__RightAssignment_1_2 ) ) )
            // InternalScenarioExpressions.g:3004:1: ( ( rule__RelationExpression__RightAssignment_1_2 ) )
            {
            // InternalScenarioExpressions.g:3004:1: ( ( rule__RelationExpression__RightAssignment_1_2 ) )
            // InternalScenarioExpressions.g:3005:1: ( rule__RelationExpression__RightAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getRightAssignment_1_2()); 
            }
            // InternalScenarioExpressions.g:3006:1: ( rule__RelationExpression__RightAssignment_1_2 )
            // InternalScenarioExpressions.g:3006:2: rule__RelationExpression__RightAssignment_1_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__RightAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getRightAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group_1__2__Impl"


    // $ANTLR start "rule__AdditionExpression__Group__0"
    // InternalScenarioExpressions.g:3023:1: rule__AdditionExpression__Group__0 : rule__AdditionExpression__Group__0__Impl rule__AdditionExpression__Group__1 ;
    public final void rule__AdditionExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3027:1: ( rule__AdditionExpression__Group__0__Impl rule__AdditionExpression__Group__1 )
            // InternalScenarioExpressions.g:3028:2: rule__AdditionExpression__Group__0__Impl rule__AdditionExpression__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_21);
            rule__AdditionExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group__0"


    // $ANTLR start "rule__AdditionExpression__Group__0__Impl"
    // InternalScenarioExpressions.g:3035:1: rule__AdditionExpression__Group__0__Impl : ( ruleMultiplicationExpression ) ;
    public final void rule__AdditionExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3039:1: ( ( ruleMultiplicationExpression ) )
            // InternalScenarioExpressions.g:3040:1: ( ruleMultiplicationExpression )
            {
            // InternalScenarioExpressions.g:3040:1: ( ruleMultiplicationExpression )
            // InternalScenarioExpressions.g:3041:1: ruleMultiplicationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getMultiplicationExpressionParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getMultiplicationExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group__0__Impl"


    // $ANTLR start "rule__AdditionExpression__Group__1"
    // InternalScenarioExpressions.g:3052:1: rule__AdditionExpression__Group__1 : rule__AdditionExpression__Group__1__Impl ;
    public final void rule__AdditionExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3056:1: ( rule__AdditionExpression__Group__1__Impl )
            // InternalScenarioExpressions.g:3057:2: rule__AdditionExpression__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group__1"


    // $ANTLR start "rule__AdditionExpression__Group__1__Impl"
    // InternalScenarioExpressions.g:3063:1: rule__AdditionExpression__Group__1__Impl : ( ( rule__AdditionExpression__Group_1__0 )? ) ;
    public final void rule__AdditionExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3067:1: ( ( ( rule__AdditionExpression__Group_1__0 )? ) )
            // InternalScenarioExpressions.g:3068:1: ( ( rule__AdditionExpression__Group_1__0 )? )
            {
            // InternalScenarioExpressions.g:3068:1: ( ( rule__AdditionExpression__Group_1__0 )? )
            // InternalScenarioExpressions.g:3069:1: ( rule__AdditionExpression__Group_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getGroup_1()); 
            }
            // InternalScenarioExpressions.g:3070:1: ( rule__AdditionExpression__Group_1__0 )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( ((LA25_0>=20 && LA25_0<=21)) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalScenarioExpressions.g:3070:2: rule__AdditionExpression__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__AdditionExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group__1__Impl"


    // $ANTLR start "rule__AdditionExpression__Group_1__0"
    // InternalScenarioExpressions.g:3084:1: rule__AdditionExpression__Group_1__0 : rule__AdditionExpression__Group_1__0__Impl rule__AdditionExpression__Group_1__1 ;
    public final void rule__AdditionExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3088:1: ( rule__AdditionExpression__Group_1__0__Impl rule__AdditionExpression__Group_1__1 )
            // InternalScenarioExpressions.g:3089:2: rule__AdditionExpression__Group_1__0__Impl rule__AdditionExpression__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_21);
            rule__AdditionExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group_1__0"


    // $ANTLR start "rule__AdditionExpression__Group_1__0__Impl"
    // InternalScenarioExpressions.g:3096:1: rule__AdditionExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__AdditionExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3100:1: ( ( () ) )
            // InternalScenarioExpressions.g:3101:1: ( () )
            {
            // InternalScenarioExpressions.g:3101:1: ( () )
            // InternalScenarioExpressions.g:3102:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }
            // InternalScenarioExpressions.g:3103:1: ()
            // InternalScenarioExpressions.g:3105:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group_1__0__Impl"


    // $ANTLR start "rule__AdditionExpression__Group_1__1"
    // InternalScenarioExpressions.g:3115:1: rule__AdditionExpression__Group_1__1 : rule__AdditionExpression__Group_1__1__Impl rule__AdditionExpression__Group_1__2 ;
    public final void rule__AdditionExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3119:1: ( rule__AdditionExpression__Group_1__1__Impl rule__AdditionExpression__Group_1__2 )
            // InternalScenarioExpressions.g:3120:2: rule__AdditionExpression__Group_1__1__Impl rule__AdditionExpression__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__AdditionExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group_1__1"


    // $ANTLR start "rule__AdditionExpression__Group_1__1__Impl"
    // InternalScenarioExpressions.g:3127:1: rule__AdditionExpression__Group_1__1__Impl : ( ( rule__AdditionExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__AdditionExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3131:1: ( ( ( rule__AdditionExpression__OperatorAssignment_1_1 ) ) )
            // InternalScenarioExpressions.g:3132:1: ( ( rule__AdditionExpression__OperatorAssignment_1_1 ) )
            {
            // InternalScenarioExpressions.g:3132:1: ( ( rule__AdditionExpression__OperatorAssignment_1_1 ) )
            // InternalScenarioExpressions.g:3133:1: ( rule__AdditionExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalScenarioExpressions.g:3134:1: ( rule__AdditionExpression__OperatorAssignment_1_1 )
            // InternalScenarioExpressions.g:3134:2: rule__AdditionExpression__OperatorAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group_1__1__Impl"


    // $ANTLR start "rule__AdditionExpression__Group_1__2"
    // InternalScenarioExpressions.g:3144:1: rule__AdditionExpression__Group_1__2 : rule__AdditionExpression__Group_1__2__Impl ;
    public final void rule__AdditionExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3148:1: ( rule__AdditionExpression__Group_1__2__Impl )
            // InternalScenarioExpressions.g:3149:2: rule__AdditionExpression__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group_1__2"


    // $ANTLR start "rule__AdditionExpression__Group_1__2__Impl"
    // InternalScenarioExpressions.g:3155:1: rule__AdditionExpression__Group_1__2__Impl : ( ( rule__AdditionExpression__RightAssignment_1_2 ) ) ;
    public final void rule__AdditionExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3159:1: ( ( ( rule__AdditionExpression__RightAssignment_1_2 ) ) )
            // InternalScenarioExpressions.g:3160:1: ( ( rule__AdditionExpression__RightAssignment_1_2 ) )
            {
            // InternalScenarioExpressions.g:3160:1: ( ( rule__AdditionExpression__RightAssignment_1_2 ) )
            // InternalScenarioExpressions.g:3161:1: ( rule__AdditionExpression__RightAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getRightAssignment_1_2()); 
            }
            // InternalScenarioExpressions.g:3162:1: ( rule__AdditionExpression__RightAssignment_1_2 )
            // InternalScenarioExpressions.g:3162:2: rule__AdditionExpression__RightAssignment_1_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__RightAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getRightAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group_1__2__Impl"


    // $ANTLR start "rule__MultiplicationExpression__Group__0"
    // InternalScenarioExpressions.g:3178:1: rule__MultiplicationExpression__Group__0 : rule__MultiplicationExpression__Group__0__Impl rule__MultiplicationExpression__Group__1 ;
    public final void rule__MultiplicationExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3182:1: ( rule__MultiplicationExpression__Group__0__Impl rule__MultiplicationExpression__Group__1 )
            // InternalScenarioExpressions.g:3183:2: rule__MultiplicationExpression__Group__0__Impl rule__MultiplicationExpression__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_22);
            rule__MultiplicationExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group__0"


    // $ANTLR start "rule__MultiplicationExpression__Group__0__Impl"
    // InternalScenarioExpressions.g:3190:1: rule__MultiplicationExpression__Group__0__Impl : ( ruleNegatedExpression ) ;
    public final void rule__MultiplicationExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3194:1: ( ( ruleNegatedExpression ) )
            // InternalScenarioExpressions.g:3195:1: ( ruleNegatedExpression )
            {
            // InternalScenarioExpressions.g:3195:1: ( ruleNegatedExpression )
            // InternalScenarioExpressions.g:3196:1: ruleNegatedExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getNegatedExpressionParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleNegatedExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getNegatedExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group__0__Impl"


    // $ANTLR start "rule__MultiplicationExpression__Group__1"
    // InternalScenarioExpressions.g:3207:1: rule__MultiplicationExpression__Group__1 : rule__MultiplicationExpression__Group__1__Impl ;
    public final void rule__MultiplicationExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3211:1: ( rule__MultiplicationExpression__Group__1__Impl )
            // InternalScenarioExpressions.g:3212:2: rule__MultiplicationExpression__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group__1"


    // $ANTLR start "rule__MultiplicationExpression__Group__1__Impl"
    // InternalScenarioExpressions.g:3218:1: rule__MultiplicationExpression__Group__1__Impl : ( ( rule__MultiplicationExpression__Group_1__0 )? ) ;
    public final void rule__MultiplicationExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3222:1: ( ( ( rule__MultiplicationExpression__Group_1__0 )? ) )
            // InternalScenarioExpressions.g:3223:1: ( ( rule__MultiplicationExpression__Group_1__0 )? )
            {
            // InternalScenarioExpressions.g:3223:1: ( ( rule__MultiplicationExpression__Group_1__0 )? )
            // InternalScenarioExpressions.g:3224:1: ( rule__MultiplicationExpression__Group_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getGroup_1()); 
            }
            // InternalScenarioExpressions.g:3225:1: ( rule__MultiplicationExpression__Group_1__0 )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( ((LA26_0>=22 && LA26_0<=23)) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalScenarioExpressions.g:3225:2: rule__MultiplicationExpression__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__MultiplicationExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group__1__Impl"


    // $ANTLR start "rule__MultiplicationExpression__Group_1__0"
    // InternalScenarioExpressions.g:3239:1: rule__MultiplicationExpression__Group_1__0 : rule__MultiplicationExpression__Group_1__0__Impl rule__MultiplicationExpression__Group_1__1 ;
    public final void rule__MultiplicationExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3243:1: ( rule__MultiplicationExpression__Group_1__0__Impl rule__MultiplicationExpression__Group_1__1 )
            // InternalScenarioExpressions.g:3244:2: rule__MultiplicationExpression__Group_1__0__Impl rule__MultiplicationExpression__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_22);
            rule__MultiplicationExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group_1__0"


    // $ANTLR start "rule__MultiplicationExpression__Group_1__0__Impl"
    // InternalScenarioExpressions.g:3251:1: rule__MultiplicationExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__MultiplicationExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3255:1: ( ( () ) )
            // InternalScenarioExpressions.g:3256:1: ( () )
            {
            // InternalScenarioExpressions.g:3256:1: ( () )
            // InternalScenarioExpressions.g:3257:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }
            // InternalScenarioExpressions.g:3258:1: ()
            // InternalScenarioExpressions.g:3260:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group_1__0__Impl"


    // $ANTLR start "rule__MultiplicationExpression__Group_1__1"
    // InternalScenarioExpressions.g:3270:1: rule__MultiplicationExpression__Group_1__1 : rule__MultiplicationExpression__Group_1__1__Impl rule__MultiplicationExpression__Group_1__2 ;
    public final void rule__MultiplicationExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3274:1: ( rule__MultiplicationExpression__Group_1__1__Impl rule__MultiplicationExpression__Group_1__2 )
            // InternalScenarioExpressions.g:3275:2: rule__MultiplicationExpression__Group_1__1__Impl rule__MultiplicationExpression__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__MultiplicationExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group_1__1"


    // $ANTLR start "rule__MultiplicationExpression__Group_1__1__Impl"
    // InternalScenarioExpressions.g:3282:1: rule__MultiplicationExpression__Group_1__1__Impl : ( ( rule__MultiplicationExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__MultiplicationExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3286:1: ( ( ( rule__MultiplicationExpression__OperatorAssignment_1_1 ) ) )
            // InternalScenarioExpressions.g:3287:1: ( ( rule__MultiplicationExpression__OperatorAssignment_1_1 ) )
            {
            // InternalScenarioExpressions.g:3287:1: ( ( rule__MultiplicationExpression__OperatorAssignment_1_1 ) )
            // InternalScenarioExpressions.g:3288:1: ( rule__MultiplicationExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalScenarioExpressions.g:3289:1: ( rule__MultiplicationExpression__OperatorAssignment_1_1 )
            // InternalScenarioExpressions.g:3289:2: rule__MultiplicationExpression__OperatorAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group_1__1__Impl"


    // $ANTLR start "rule__MultiplicationExpression__Group_1__2"
    // InternalScenarioExpressions.g:3299:1: rule__MultiplicationExpression__Group_1__2 : rule__MultiplicationExpression__Group_1__2__Impl ;
    public final void rule__MultiplicationExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3303:1: ( rule__MultiplicationExpression__Group_1__2__Impl )
            // InternalScenarioExpressions.g:3304:2: rule__MultiplicationExpression__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group_1__2"


    // $ANTLR start "rule__MultiplicationExpression__Group_1__2__Impl"
    // InternalScenarioExpressions.g:3310:1: rule__MultiplicationExpression__Group_1__2__Impl : ( ( rule__MultiplicationExpression__RightAssignment_1_2 ) ) ;
    public final void rule__MultiplicationExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3314:1: ( ( ( rule__MultiplicationExpression__RightAssignment_1_2 ) ) )
            // InternalScenarioExpressions.g:3315:1: ( ( rule__MultiplicationExpression__RightAssignment_1_2 ) )
            {
            // InternalScenarioExpressions.g:3315:1: ( ( rule__MultiplicationExpression__RightAssignment_1_2 ) )
            // InternalScenarioExpressions.g:3316:1: ( rule__MultiplicationExpression__RightAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getRightAssignment_1_2()); 
            }
            // InternalScenarioExpressions.g:3317:1: ( rule__MultiplicationExpression__RightAssignment_1_2 )
            // InternalScenarioExpressions.g:3317:2: rule__MultiplicationExpression__RightAssignment_1_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__RightAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getRightAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group_1__2__Impl"


    // $ANTLR start "rule__NegatedExpression__Group_0__0"
    // InternalScenarioExpressions.g:3333:1: rule__NegatedExpression__Group_0__0 : rule__NegatedExpression__Group_0__0__Impl rule__NegatedExpression__Group_0__1 ;
    public final void rule__NegatedExpression__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3337:1: ( rule__NegatedExpression__Group_0__0__Impl rule__NegatedExpression__Group_0__1 )
            // InternalScenarioExpressions.g:3338:2: rule__NegatedExpression__Group_0__0__Impl rule__NegatedExpression__Group_0__1
            {
            pushFollow(FollowSets000.FOLLOW_23);
            rule__NegatedExpression__Group_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__Group_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Group_0__0"


    // $ANTLR start "rule__NegatedExpression__Group_0__0__Impl"
    // InternalScenarioExpressions.g:3345:1: rule__NegatedExpression__Group_0__0__Impl : ( () ) ;
    public final void rule__NegatedExpression__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3349:1: ( ( () ) )
            // InternalScenarioExpressions.g:3350:1: ( () )
            {
            // InternalScenarioExpressions.g:3350:1: ( () )
            // InternalScenarioExpressions.g:3351:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionAccess().getUnaryOperationExpressionAction_0_0()); 
            }
            // InternalScenarioExpressions.g:3352:1: ()
            // InternalScenarioExpressions.g:3354:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionAccess().getUnaryOperationExpressionAction_0_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Group_0__0__Impl"


    // $ANTLR start "rule__NegatedExpression__Group_0__1"
    // InternalScenarioExpressions.g:3364:1: rule__NegatedExpression__Group_0__1 : rule__NegatedExpression__Group_0__1__Impl rule__NegatedExpression__Group_0__2 ;
    public final void rule__NegatedExpression__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3368:1: ( rule__NegatedExpression__Group_0__1__Impl rule__NegatedExpression__Group_0__2 )
            // InternalScenarioExpressions.g:3369:2: rule__NegatedExpression__Group_0__1__Impl rule__NegatedExpression__Group_0__2
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__NegatedExpression__Group_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__Group_0__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Group_0__1"


    // $ANTLR start "rule__NegatedExpression__Group_0__1__Impl"
    // InternalScenarioExpressions.g:3376:1: rule__NegatedExpression__Group_0__1__Impl : ( ( rule__NegatedExpression__OperatorAssignment_0_1 ) ) ;
    public final void rule__NegatedExpression__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3380:1: ( ( ( rule__NegatedExpression__OperatorAssignment_0_1 ) ) )
            // InternalScenarioExpressions.g:3381:1: ( ( rule__NegatedExpression__OperatorAssignment_0_1 ) )
            {
            // InternalScenarioExpressions.g:3381:1: ( ( rule__NegatedExpression__OperatorAssignment_0_1 ) )
            // InternalScenarioExpressions.g:3382:1: ( rule__NegatedExpression__OperatorAssignment_0_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionAccess().getOperatorAssignment_0_1()); 
            }
            // InternalScenarioExpressions.g:3383:1: ( rule__NegatedExpression__OperatorAssignment_0_1 )
            // InternalScenarioExpressions.g:3383:2: rule__NegatedExpression__OperatorAssignment_0_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__OperatorAssignment_0_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionAccess().getOperatorAssignment_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Group_0__1__Impl"


    // $ANTLR start "rule__NegatedExpression__Group_0__2"
    // InternalScenarioExpressions.g:3393:1: rule__NegatedExpression__Group_0__2 : rule__NegatedExpression__Group_0__2__Impl ;
    public final void rule__NegatedExpression__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3397:1: ( rule__NegatedExpression__Group_0__2__Impl )
            // InternalScenarioExpressions.g:3398:2: rule__NegatedExpression__Group_0__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__Group_0__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Group_0__2"


    // $ANTLR start "rule__NegatedExpression__Group_0__2__Impl"
    // InternalScenarioExpressions.g:3404:1: rule__NegatedExpression__Group_0__2__Impl : ( ( rule__NegatedExpression__OperandAssignment_0_2 ) ) ;
    public final void rule__NegatedExpression__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3408:1: ( ( ( rule__NegatedExpression__OperandAssignment_0_2 ) ) )
            // InternalScenarioExpressions.g:3409:1: ( ( rule__NegatedExpression__OperandAssignment_0_2 ) )
            {
            // InternalScenarioExpressions.g:3409:1: ( ( rule__NegatedExpression__OperandAssignment_0_2 ) )
            // InternalScenarioExpressions.g:3410:1: ( rule__NegatedExpression__OperandAssignment_0_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionAccess().getOperandAssignment_0_2()); 
            }
            // InternalScenarioExpressions.g:3411:1: ( rule__NegatedExpression__OperandAssignment_0_2 )
            // InternalScenarioExpressions.g:3411:2: rule__NegatedExpression__OperandAssignment_0_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__OperandAssignment_0_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionAccess().getOperandAssignment_0_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Group_0__2__Impl"


    // $ANTLR start "rule__BasicExpression__Group_1__0"
    // InternalScenarioExpressions.g:3427:1: rule__BasicExpression__Group_1__0 : rule__BasicExpression__Group_1__0__Impl rule__BasicExpression__Group_1__1 ;
    public final void rule__BasicExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3431:1: ( rule__BasicExpression__Group_1__0__Impl rule__BasicExpression__Group_1__1 )
            // InternalScenarioExpressions.g:3432:2: rule__BasicExpression__Group_1__0__Impl rule__BasicExpression__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__BasicExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__BasicExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Group_1__0"


    // $ANTLR start "rule__BasicExpression__Group_1__0__Impl"
    // InternalScenarioExpressions.g:3439:1: rule__BasicExpression__Group_1__0__Impl : ( '(' ) ;
    public final void rule__BasicExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3443:1: ( ( '(' ) )
            // InternalScenarioExpressions.g:3444:1: ( '(' )
            {
            // InternalScenarioExpressions.g:3444:1: ( '(' )
            // InternalScenarioExpressions.g:3445:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0()); 
            }
            match(input,42,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Group_1__0__Impl"


    // $ANTLR start "rule__BasicExpression__Group_1__1"
    // InternalScenarioExpressions.g:3458:1: rule__BasicExpression__Group_1__1 : rule__BasicExpression__Group_1__1__Impl rule__BasicExpression__Group_1__2 ;
    public final void rule__BasicExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3462:1: ( rule__BasicExpression__Group_1__1__Impl rule__BasicExpression__Group_1__2 )
            // InternalScenarioExpressions.g:3463:2: rule__BasicExpression__Group_1__1__Impl rule__BasicExpression__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_24);
            rule__BasicExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__BasicExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Group_1__1"


    // $ANTLR start "rule__BasicExpression__Group_1__1__Impl"
    // InternalScenarioExpressions.g:3470:1: rule__BasicExpression__Group_1__1__Impl : ( ruleExpression ) ;
    public final void rule__BasicExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3474:1: ( ( ruleExpression ) )
            // InternalScenarioExpressions.g:3475:1: ( ruleExpression )
            {
            // InternalScenarioExpressions.g:3475:1: ( ruleExpression )
            // InternalScenarioExpressions.g:3476:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicExpressionAccess().getExpressionParserRuleCall_1_1()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicExpressionAccess().getExpressionParserRuleCall_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Group_1__1__Impl"


    // $ANTLR start "rule__BasicExpression__Group_1__2"
    // InternalScenarioExpressions.g:3487:1: rule__BasicExpression__Group_1__2 : rule__BasicExpression__Group_1__2__Impl ;
    public final void rule__BasicExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3491:1: ( rule__BasicExpression__Group_1__2__Impl )
            // InternalScenarioExpressions.g:3492:2: rule__BasicExpression__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__BasicExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Group_1__2"


    // $ANTLR start "rule__BasicExpression__Group_1__2__Impl"
    // InternalScenarioExpressions.g:3498:1: rule__BasicExpression__Group_1__2__Impl : ( ')' ) ;
    public final void rule__BasicExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3502:1: ( ( ')' ) )
            // InternalScenarioExpressions.g:3503:1: ( ')' )
            {
            // InternalScenarioExpressions.g:3503:1: ( ')' )
            // InternalScenarioExpressions.g:3504:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicExpressionAccess().getRightParenthesisKeyword_1_2()); 
            }
            match(input,43,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicExpressionAccess().getRightParenthesisKeyword_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Group_1__2__Impl"


    // $ANTLR start "rule__EnumValue__Group__0"
    // InternalScenarioExpressions.g:3523:1: rule__EnumValue__Group__0 : rule__EnumValue__Group__0__Impl rule__EnumValue__Group__1 ;
    public final void rule__EnumValue__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3527:1: ( rule__EnumValue__Group__0__Impl rule__EnumValue__Group__1 )
            // InternalScenarioExpressions.g:3528:2: rule__EnumValue__Group__0__Impl rule__EnumValue__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_25);
            rule__EnumValue__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__EnumValue__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__0"


    // $ANTLR start "rule__EnumValue__Group__0__Impl"
    // InternalScenarioExpressions.g:3535:1: rule__EnumValue__Group__0__Impl : ( ( rule__EnumValue__TypeAssignment_0 ) ) ;
    public final void rule__EnumValue__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3539:1: ( ( ( rule__EnumValue__TypeAssignment_0 ) ) )
            // InternalScenarioExpressions.g:3540:1: ( ( rule__EnumValue__TypeAssignment_0 ) )
            {
            // InternalScenarioExpressions.g:3540:1: ( ( rule__EnumValue__TypeAssignment_0 ) )
            // InternalScenarioExpressions.g:3541:1: ( rule__EnumValue__TypeAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getTypeAssignment_0()); 
            }
            // InternalScenarioExpressions.g:3542:1: ( rule__EnumValue__TypeAssignment_0 )
            // InternalScenarioExpressions.g:3542:2: rule__EnumValue__TypeAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__EnumValue__TypeAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getTypeAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__0__Impl"


    // $ANTLR start "rule__EnumValue__Group__1"
    // InternalScenarioExpressions.g:3552:1: rule__EnumValue__Group__1 : rule__EnumValue__Group__1__Impl rule__EnumValue__Group__2 ;
    public final void rule__EnumValue__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3556:1: ( rule__EnumValue__Group__1__Impl rule__EnumValue__Group__2 )
            // InternalScenarioExpressions.g:3557:2: rule__EnumValue__Group__1__Impl rule__EnumValue__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_7);
            rule__EnumValue__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__EnumValue__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__1"


    // $ANTLR start "rule__EnumValue__Group__1__Impl"
    // InternalScenarioExpressions.g:3564:1: rule__EnumValue__Group__1__Impl : ( ':' ) ;
    public final void rule__EnumValue__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3568:1: ( ( ':' ) )
            // InternalScenarioExpressions.g:3569:1: ( ':' )
            {
            // InternalScenarioExpressions.g:3569:1: ( ':' )
            // InternalScenarioExpressions.g:3570:1: ':'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getColonKeyword_1()); 
            }
            match(input,44,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getColonKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__1__Impl"


    // $ANTLR start "rule__EnumValue__Group__2"
    // InternalScenarioExpressions.g:3583:1: rule__EnumValue__Group__2 : rule__EnumValue__Group__2__Impl ;
    public final void rule__EnumValue__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3587:1: ( rule__EnumValue__Group__2__Impl )
            // InternalScenarioExpressions.g:3588:2: rule__EnumValue__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__EnumValue__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__2"


    // $ANTLR start "rule__EnumValue__Group__2__Impl"
    // InternalScenarioExpressions.g:3594:1: rule__EnumValue__Group__2__Impl : ( ( rule__EnumValue__ValueAssignment_2 ) ) ;
    public final void rule__EnumValue__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3598:1: ( ( ( rule__EnumValue__ValueAssignment_2 ) ) )
            // InternalScenarioExpressions.g:3599:1: ( ( rule__EnumValue__ValueAssignment_2 ) )
            {
            // InternalScenarioExpressions.g:3599:1: ( ( rule__EnumValue__ValueAssignment_2 ) )
            // InternalScenarioExpressions.g:3600:1: ( rule__EnumValue__ValueAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getValueAssignment_2()); 
            }
            // InternalScenarioExpressions.g:3601:1: ( rule__EnumValue__ValueAssignment_2 )
            // InternalScenarioExpressions.g:3601:2: rule__EnumValue__ValueAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__EnumValue__ValueAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getValueAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__2__Impl"


    // $ANTLR start "rule__NullValue__Group__0"
    // InternalScenarioExpressions.g:3617:1: rule__NullValue__Group__0 : rule__NullValue__Group__0__Impl rule__NullValue__Group__1 ;
    public final void rule__NullValue__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3621:1: ( rule__NullValue__Group__0__Impl rule__NullValue__Group__1 )
            // InternalScenarioExpressions.g:3622:2: rule__NullValue__Group__0__Impl rule__NullValue__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_26);
            rule__NullValue__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NullValue__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NullValue__Group__0"


    // $ANTLR start "rule__NullValue__Group__0__Impl"
    // InternalScenarioExpressions.g:3629:1: rule__NullValue__Group__0__Impl : ( () ) ;
    public final void rule__NullValue__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3633:1: ( ( () ) )
            // InternalScenarioExpressions.g:3634:1: ( () )
            {
            // InternalScenarioExpressions.g:3634:1: ( () )
            // InternalScenarioExpressions.g:3635:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNullValueAccess().getNullValueAction_0()); 
            }
            // InternalScenarioExpressions.g:3636:1: ()
            // InternalScenarioExpressions.g:3638:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNullValueAccess().getNullValueAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NullValue__Group__0__Impl"


    // $ANTLR start "rule__NullValue__Group__1"
    // InternalScenarioExpressions.g:3648:1: rule__NullValue__Group__1 : rule__NullValue__Group__1__Impl ;
    public final void rule__NullValue__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3652:1: ( rule__NullValue__Group__1__Impl )
            // InternalScenarioExpressions.g:3653:2: rule__NullValue__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NullValue__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NullValue__Group__1"


    // $ANTLR start "rule__NullValue__Group__1__Impl"
    // InternalScenarioExpressions.g:3659:1: rule__NullValue__Group__1__Impl : ( 'null' ) ;
    public final void rule__NullValue__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3663:1: ( ( 'null' ) )
            // InternalScenarioExpressions.g:3664:1: ( 'null' )
            {
            // InternalScenarioExpressions.g:3664:1: ( 'null' )
            // InternalScenarioExpressions.g:3665:1: 'null'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNullValueAccess().getNullKeyword_1()); 
            }
            match(input,45,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNullValueAccess().getNullKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NullValue__Group__1__Impl"


    // $ANTLR start "rule__CollectionAccess__Group__0"
    // InternalScenarioExpressions.g:3682:1: rule__CollectionAccess__Group__0 : rule__CollectionAccess__Group__0__Impl rule__CollectionAccess__Group__1 ;
    public final void rule__CollectionAccess__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3686:1: ( rule__CollectionAccess__Group__0__Impl rule__CollectionAccess__Group__1 )
            // InternalScenarioExpressions.g:3687:2: rule__CollectionAccess__Group__0__Impl rule__CollectionAccess__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_27);
            rule__CollectionAccess__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionAccess__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__0"


    // $ANTLR start "rule__CollectionAccess__Group__0__Impl"
    // InternalScenarioExpressions.g:3694:1: rule__CollectionAccess__Group__0__Impl : ( ( rule__CollectionAccess__CollectionOperationAssignment_0 ) ) ;
    public final void rule__CollectionAccess__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3698:1: ( ( ( rule__CollectionAccess__CollectionOperationAssignment_0 ) ) )
            // InternalScenarioExpressions.g:3699:1: ( ( rule__CollectionAccess__CollectionOperationAssignment_0 ) )
            {
            // InternalScenarioExpressions.g:3699:1: ( ( rule__CollectionAccess__CollectionOperationAssignment_0 ) )
            // InternalScenarioExpressions.g:3700:1: ( rule__CollectionAccess__CollectionOperationAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getCollectionOperationAssignment_0()); 
            }
            // InternalScenarioExpressions.g:3701:1: ( rule__CollectionAccess__CollectionOperationAssignment_0 )
            // InternalScenarioExpressions.g:3701:2: rule__CollectionAccess__CollectionOperationAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionAccess__CollectionOperationAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getCollectionOperationAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__0__Impl"


    // $ANTLR start "rule__CollectionAccess__Group__1"
    // InternalScenarioExpressions.g:3711:1: rule__CollectionAccess__Group__1 : rule__CollectionAccess__Group__1__Impl rule__CollectionAccess__Group__2 ;
    public final void rule__CollectionAccess__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3715:1: ( rule__CollectionAccess__Group__1__Impl rule__CollectionAccess__Group__2 )
            // InternalScenarioExpressions.g:3716:2: rule__CollectionAccess__Group__1__Impl rule__CollectionAccess__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_28);
            rule__CollectionAccess__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionAccess__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__1"


    // $ANTLR start "rule__CollectionAccess__Group__1__Impl"
    // InternalScenarioExpressions.g:3723:1: rule__CollectionAccess__Group__1__Impl : ( '(' ) ;
    public final void rule__CollectionAccess__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3727:1: ( ( '(' ) )
            // InternalScenarioExpressions.g:3728:1: ( '(' )
            {
            // InternalScenarioExpressions.g:3728:1: ( '(' )
            // InternalScenarioExpressions.g:3729:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getLeftParenthesisKeyword_1()); 
            }
            match(input,42,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getLeftParenthesisKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__1__Impl"


    // $ANTLR start "rule__CollectionAccess__Group__2"
    // InternalScenarioExpressions.g:3742:1: rule__CollectionAccess__Group__2 : rule__CollectionAccess__Group__2__Impl rule__CollectionAccess__Group__3 ;
    public final void rule__CollectionAccess__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3746:1: ( rule__CollectionAccess__Group__2__Impl rule__CollectionAccess__Group__3 )
            // InternalScenarioExpressions.g:3747:2: rule__CollectionAccess__Group__2__Impl rule__CollectionAccess__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_28);
            rule__CollectionAccess__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionAccess__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__2"


    // $ANTLR start "rule__CollectionAccess__Group__2__Impl"
    // InternalScenarioExpressions.g:3754:1: rule__CollectionAccess__Group__2__Impl : ( ( rule__CollectionAccess__ParameterAssignment_2 )? ) ;
    public final void rule__CollectionAccess__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3758:1: ( ( ( rule__CollectionAccess__ParameterAssignment_2 )? ) )
            // InternalScenarioExpressions.g:3759:1: ( ( rule__CollectionAccess__ParameterAssignment_2 )? )
            {
            // InternalScenarioExpressions.g:3759:1: ( ( rule__CollectionAccess__ParameterAssignment_2 )? )
            // InternalScenarioExpressions.g:3760:1: ( rule__CollectionAccess__ParameterAssignment_2 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getParameterAssignment_2()); 
            }
            // InternalScenarioExpressions.g:3761:1: ( rule__CollectionAccess__ParameterAssignment_2 )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( ((LA27_0>=RULE_INT && LA27_0<=RULE_BOOL)||LA27_0==21||LA27_0==24||LA27_0==42||LA27_0==45) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalScenarioExpressions.g:3761:2: rule__CollectionAccess__ParameterAssignment_2
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__CollectionAccess__ParameterAssignment_2();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getParameterAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__2__Impl"


    // $ANTLR start "rule__CollectionAccess__Group__3"
    // InternalScenarioExpressions.g:3771:1: rule__CollectionAccess__Group__3 : rule__CollectionAccess__Group__3__Impl ;
    public final void rule__CollectionAccess__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3775:1: ( rule__CollectionAccess__Group__3__Impl )
            // InternalScenarioExpressions.g:3776:2: rule__CollectionAccess__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionAccess__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__3"


    // $ANTLR start "rule__CollectionAccess__Group__3__Impl"
    // InternalScenarioExpressions.g:3782:1: rule__CollectionAccess__Group__3__Impl : ( ')' ) ;
    public final void rule__CollectionAccess__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3786:1: ( ( ')' ) )
            // InternalScenarioExpressions.g:3787:1: ( ')' )
            {
            // InternalScenarioExpressions.g:3787:1: ( ')' )
            // InternalScenarioExpressions.g:3788:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getRightParenthesisKeyword_3()); 
            }
            match(input,43,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getRightParenthesisKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__3__Impl"


    // $ANTLR start "rule__FeatureAccess__Group__0"
    // InternalScenarioExpressions.g:3809:1: rule__FeatureAccess__Group__0 : rule__FeatureAccess__Group__0__Impl rule__FeatureAccess__Group__1 ;
    public final void rule__FeatureAccess__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3813:1: ( rule__FeatureAccess__Group__0__Impl rule__FeatureAccess__Group__1 )
            // InternalScenarioExpressions.g:3814:2: rule__FeatureAccess__Group__0__Impl rule__FeatureAccess__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_29);
            rule__FeatureAccess__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__0"


    // $ANTLR start "rule__FeatureAccess__Group__0__Impl"
    // InternalScenarioExpressions.g:3821:1: rule__FeatureAccess__Group__0__Impl : ( ( rule__FeatureAccess__TargetAssignment_0 ) ) ;
    public final void rule__FeatureAccess__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3825:1: ( ( ( rule__FeatureAccess__TargetAssignment_0 ) ) )
            // InternalScenarioExpressions.g:3826:1: ( ( rule__FeatureAccess__TargetAssignment_0 ) )
            {
            // InternalScenarioExpressions.g:3826:1: ( ( rule__FeatureAccess__TargetAssignment_0 ) )
            // InternalScenarioExpressions.g:3827:1: ( rule__FeatureAccess__TargetAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getTargetAssignment_0()); 
            }
            // InternalScenarioExpressions.g:3828:1: ( rule__FeatureAccess__TargetAssignment_0 )
            // InternalScenarioExpressions.g:3828:2: rule__FeatureAccess__TargetAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__TargetAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getTargetAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__0__Impl"


    // $ANTLR start "rule__FeatureAccess__Group__1"
    // InternalScenarioExpressions.g:3838:1: rule__FeatureAccess__Group__1 : rule__FeatureAccess__Group__1__Impl rule__FeatureAccess__Group__2 ;
    public final void rule__FeatureAccess__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3842:1: ( rule__FeatureAccess__Group__1__Impl rule__FeatureAccess__Group__2 )
            // InternalScenarioExpressions.g:3843:2: rule__FeatureAccess__Group__1__Impl rule__FeatureAccess__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_7);
            rule__FeatureAccess__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__1"


    // $ANTLR start "rule__FeatureAccess__Group__1__Impl"
    // InternalScenarioExpressions.g:3850:1: rule__FeatureAccess__Group__1__Impl : ( '.' ) ;
    public final void rule__FeatureAccess__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3854:1: ( ( '.' ) )
            // InternalScenarioExpressions.g:3855:1: ( '.' )
            {
            // InternalScenarioExpressions.g:3855:1: ( '.' )
            // InternalScenarioExpressions.g:3856:1: '.'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getFullStopKeyword_1()); 
            }
            match(input,46,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getFullStopKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__1__Impl"


    // $ANTLR start "rule__FeatureAccess__Group__2"
    // InternalScenarioExpressions.g:3869:1: rule__FeatureAccess__Group__2 : rule__FeatureAccess__Group__2__Impl ;
    public final void rule__FeatureAccess__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3873:1: ( rule__FeatureAccess__Group__2__Impl )
            // InternalScenarioExpressions.g:3874:2: rule__FeatureAccess__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__2"


    // $ANTLR start "rule__FeatureAccess__Group__2__Impl"
    // InternalScenarioExpressions.g:3880:1: rule__FeatureAccess__Group__2__Impl : ( ( rule__FeatureAccess__Alternatives_2 ) ) ;
    public final void rule__FeatureAccess__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3884:1: ( ( ( rule__FeatureAccess__Alternatives_2 ) ) )
            // InternalScenarioExpressions.g:3885:1: ( ( rule__FeatureAccess__Alternatives_2 ) )
            {
            // InternalScenarioExpressions.g:3885:1: ( ( rule__FeatureAccess__Alternatives_2 ) )
            // InternalScenarioExpressions.g:3886:1: ( rule__FeatureAccess__Alternatives_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getAlternatives_2()); 
            }
            // InternalScenarioExpressions.g:3887:1: ( rule__FeatureAccess__Alternatives_2 )
            // InternalScenarioExpressions.g:3887:2: rule__FeatureAccess__Alternatives_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Alternatives_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getAlternatives_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__2__Impl"


    // $ANTLR start "rule__FeatureAccess__Group_2_0__0"
    // InternalScenarioExpressions.g:3903:1: rule__FeatureAccess__Group_2_0__0 : rule__FeatureAccess__Group_2_0__0__Impl rule__FeatureAccess__Group_2_0__1 ;
    public final void rule__FeatureAccess__Group_2_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3907:1: ( rule__FeatureAccess__Group_2_0__0__Impl rule__FeatureAccess__Group_2_0__1 )
            // InternalScenarioExpressions.g:3908:2: rule__FeatureAccess__Group_2_0__0__Impl rule__FeatureAccess__Group_2_0__1
            {
            pushFollow(FollowSets000.FOLLOW_29);
            rule__FeatureAccess__Group_2_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group_2_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_0__0"


    // $ANTLR start "rule__FeatureAccess__Group_2_0__0__Impl"
    // InternalScenarioExpressions.g:3915:1: rule__FeatureAccess__Group_2_0__0__Impl : ( ( rule__FeatureAccess__ValueAssignment_2_0_0 ) ) ;
    public final void rule__FeatureAccess__Group_2_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3919:1: ( ( ( rule__FeatureAccess__ValueAssignment_2_0_0 ) ) )
            // InternalScenarioExpressions.g:3920:1: ( ( rule__FeatureAccess__ValueAssignment_2_0_0 ) )
            {
            // InternalScenarioExpressions.g:3920:1: ( ( rule__FeatureAccess__ValueAssignment_2_0_0 ) )
            // InternalScenarioExpressions.g:3921:1: ( rule__FeatureAccess__ValueAssignment_2_0_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getValueAssignment_2_0_0()); 
            }
            // InternalScenarioExpressions.g:3922:1: ( rule__FeatureAccess__ValueAssignment_2_0_0 )
            // InternalScenarioExpressions.g:3922:2: rule__FeatureAccess__ValueAssignment_2_0_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__ValueAssignment_2_0_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getValueAssignment_2_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_0__0__Impl"


    // $ANTLR start "rule__FeatureAccess__Group_2_0__1"
    // InternalScenarioExpressions.g:3932:1: rule__FeatureAccess__Group_2_0__1 : rule__FeatureAccess__Group_2_0__1__Impl ;
    public final void rule__FeatureAccess__Group_2_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3936:1: ( rule__FeatureAccess__Group_2_0__1__Impl )
            // InternalScenarioExpressions.g:3937:2: rule__FeatureAccess__Group_2_0__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group_2_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_0__1"


    // $ANTLR start "rule__FeatureAccess__Group_2_0__1__Impl"
    // InternalScenarioExpressions.g:3943:1: rule__FeatureAccess__Group_2_0__1__Impl : ( ( rule__FeatureAccess__Group_2_0_1__0 )? ) ;
    public final void rule__FeatureAccess__Group_2_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3947:1: ( ( ( rule__FeatureAccess__Group_2_0_1__0 )? ) )
            // InternalScenarioExpressions.g:3948:1: ( ( rule__FeatureAccess__Group_2_0_1__0 )? )
            {
            // InternalScenarioExpressions.g:3948:1: ( ( rule__FeatureAccess__Group_2_0_1__0 )? )
            // InternalScenarioExpressions.g:3949:1: ( rule__FeatureAccess__Group_2_0_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getGroup_2_0_1()); 
            }
            // InternalScenarioExpressions.g:3950:1: ( rule__FeatureAccess__Group_2_0_1__0 )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==46) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalScenarioExpressions.g:3950:2: rule__FeatureAccess__Group_2_0_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__FeatureAccess__Group_2_0_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getGroup_2_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_0__1__Impl"


    // $ANTLR start "rule__FeatureAccess__Group_2_0_1__0"
    // InternalScenarioExpressions.g:3964:1: rule__FeatureAccess__Group_2_0_1__0 : rule__FeatureAccess__Group_2_0_1__0__Impl rule__FeatureAccess__Group_2_0_1__1 ;
    public final void rule__FeatureAccess__Group_2_0_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3968:1: ( rule__FeatureAccess__Group_2_0_1__0__Impl rule__FeatureAccess__Group_2_0_1__1 )
            // InternalScenarioExpressions.g:3969:2: rule__FeatureAccess__Group_2_0_1__0__Impl rule__FeatureAccess__Group_2_0_1__1
            {
            pushFollow(FollowSets000.FOLLOW_30);
            rule__FeatureAccess__Group_2_0_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group_2_0_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_0_1__0"


    // $ANTLR start "rule__FeatureAccess__Group_2_0_1__0__Impl"
    // InternalScenarioExpressions.g:3976:1: rule__FeatureAccess__Group_2_0_1__0__Impl : ( '.' ) ;
    public final void rule__FeatureAccess__Group_2_0_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3980:1: ( ( '.' ) )
            // InternalScenarioExpressions.g:3981:1: ( '.' )
            {
            // InternalScenarioExpressions.g:3981:1: ( '.' )
            // InternalScenarioExpressions.g:3982:1: '.'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getFullStopKeyword_2_0_1_0()); 
            }
            match(input,46,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getFullStopKeyword_2_0_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_0_1__0__Impl"


    // $ANTLR start "rule__FeatureAccess__Group_2_0_1__1"
    // InternalScenarioExpressions.g:3995:1: rule__FeatureAccess__Group_2_0_1__1 : rule__FeatureAccess__Group_2_0_1__1__Impl ;
    public final void rule__FeatureAccess__Group_2_0_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3999:1: ( rule__FeatureAccess__Group_2_0_1__1__Impl )
            // InternalScenarioExpressions.g:4000:2: rule__FeatureAccess__Group_2_0_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group_2_0_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_0_1__1"


    // $ANTLR start "rule__FeatureAccess__Group_2_0_1__1__Impl"
    // InternalScenarioExpressions.g:4006:1: rule__FeatureAccess__Group_2_0_1__1__Impl : ( ( rule__FeatureAccess__CollectionAccessAssignment_2_0_1_1 ) ) ;
    public final void rule__FeatureAccess__Group_2_0_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4010:1: ( ( ( rule__FeatureAccess__CollectionAccessAssignment_2_0_1_1 ) ) )
            // InternalScenarioExpressions.g:4011:1: ( ( rule__FeatureAccess__CollectionAccessAssignment_2_0_1_1 ) )
            {
            // InternalScenarioExpressions.g:4011:1: ( ( rule__FeatureAccess__CollectionAccessAssignment_2_0_1_1 ) )
            // InternalScenarioExpressions.g:4012:1: ( rule__FeatureAccess__CollectionAccessAssignment_2_0_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getCollectionAccessAssignment_2_0_1_1()); 
            }
            // InternalScenarioExpressions.g:4013:1: ( rule__FeatureAccess__CollectionAccessAssignment_2_0_1_1 )
            // InternalScenarioExpressions.g:4013:2: rule__FeatureAccess__CollectionAccessAssignment_2_0_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__CollectionAccessAssignment_2_0_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getCollectionAccessAssignment_2_0_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_0_1__1__Impl"


    // $ANTLR start "rule__FeatureAccess__Group_2_1__0"
    // InternalScenarioExpressions.g:4027:1: rule__FeatureAccess__Group_2_1__0 : rule__FeatureAccess__Group_2_1__0__Impl rule__FeatureAccess__Group_2_1__1 ;
    public final void rule__FeatureAccess__Group_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4031:1: ( rule__FeatureAccess__Group_2_1__0__Impl rule__FeatureAccess__Group_2_1__1 )
            // InternalScenarioExpressions.g:4032:2: rule__FeatureAccess__Group_2_1__0__Impl rule__FeatureAccess__Group_2_1__1
            {
            pushFollow(FollowSets000.FOLLOW_27);
            rule__FeatureAccess__Group_2_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group_2_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_1__0"


    // $ANTLR start "rule__FeatureAccess__Group_2_1__0__Impl"
    // InternalScenarioExpressions.g:4039:1: rule__FeatureAccess__Group_2_1__0__Impl : ( ( rule__FeatureAccess__ValueAssignment_2_1_0 ) ) ;
    public final void rule__FeatureAccess__Group_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4043:1: ( ( ( rule__FeatureAccess__ValueAssignment_2_1_0 ) ) )
            // InternalScenarioExpressions.g:4044:1: ( ( rule__FeatureAccess__ValueAssignment_2_1_0 ) )
            {
            // InternalScenarioExpressions.g:4044:1: ( ( rule__FeatureAccess__ValueAssignment_2_1_0 ) )
            // InternalScenarioExpressions.g:4045:1: ( rule__FeatureAccess__ValueAssignment_2_1_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getValueAssignment_2_1_0()); 
            }
            // InternalScenarioExpressions.g:4046:1: ( rule__FeatureAccess__ValueAssignment_2_1_0 )
            // InternalScenarioExpressions.g:4046:2: rule__FeatureAccess__ValueAssignment_2_1_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__ValueAssignment_2_1_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getValueAssignment_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_1__0__Impl"


    // $ANTLR start "rule__FeatureAccess__Group_2_1__1"
    // InternalScenarioExpressions.g:4056:1: rule__FeatureAccess__Group_2_1__1 : rule__FeatureAccess__Group_2_1__1__Impl rule__FeatureAccess__Group_2_1__2 ;
    public final void rule__FeatureAccess__Group_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4060:1: ( rule__FeatureAccess__Group_2_1__1__Impl rule__FeatureAccess__Group_2_1__2 )
            // InternalScenarioExpressions.g:4061:2: rule__FeatureAccess__Group_2_1__1__Impl rule__FeatureAccess__Group_2_1__2
            {
            pushFollow(FollowSets000.FOLLOW_28);
            rule__FeatureAccess__Group_2_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group_2_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_1__1"


    // $ANTLR start "rule__FeatureAccess__Group_2_1__1__Impl"
    // InternalScenarioExpressions.g:4068:1: rule__FeatureAccess__Group_2_1__1__Impl : ( '(' ) ;
    public final void rule__FeatureAccess__Group_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4072:1: ( ( '(' ) )
            // InternalScenarioExpressions.g:4073:1: ( '(' )
            {
            // InternalScenarioExpressions.g:4073:1: ( '(' )
            // InternalScenarioExpressions.g:4074:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getLeftParenthesisKeyword_2_1_1()); 
            }
            match(input,42,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getLeftParenthesisKeyword_2_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_1__1__Impl"


    // $ANTLR start "rule__FeatureAccess__Group_2_1__2"
    // InternalScenarioExpressions.g:4087:1: rule__FeatureAccess__Group_2_1__2 : rule__FeatureAccess__Group_2_1__2__Impl rule__FeatureAccess__Group_2_1__3 ;
    public final void rule__FeatureAccess__Group_2_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4091:1: ( rule__FeatureAccess__Group_2_1__2__Impl rule__FeatureAccess__Group_2_1__3 )
            // InternalScenarioExpressions.g:4092:2: rule__FeatureAccess__Group_2_1__2__Impl rule__FeatureAccess__Group_2_1__3
            {
            pushFollow(FollowSets000.FOLLOW_28);
            rule__FeatureAccess__Group_2_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group_2_1__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_1__2"


    // $ANTLR start "rule__FeatureAccess__Group_2_1__2__Impl"
    // InternalScenarioExpressions.g:4099:1: rule__FeatureAccess__Group_2_1__2__Impl : ( ( rule__FeatureAccess__Group_2_1_2__0 )? ) ;
    public final void rule__FeatureAccess__Group_2_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4103:1: ( ( ( rule__FeatureAccess__Group_2_1_2__0 )? ) )
            // InternalScenarioExpressions.g:4104:1: ( ( rule__FeatureAccess__Group_2_1_2__0 )? )
            {
            // InternalScenarioExpressions.g:4104:1: ( ( rule__FeatureAccess__Group_2_1_2__0 )? )
            // InternalScenarioExpressions.g:4105:1: ( rule__FeatureAccess__Group_2_1_2__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getGroup_2_1_2()); 
            }
            // InternalScenarioExpressions.g:4106:1: ( rule__FeatureAccess__Group_2_1_2__0 )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( ((LA29_0>=RULE_INT && LA29_0<=RULE_BOOL)||LA29_0==21||LA29_0==24||LA29_0==42||LA29_0==45) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalScenarioExpressions.g:4106:2: rule__FeatureAccess__Group_2_1_2__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__FeatureAccess__Group_2_1_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getGroup_2_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_1__2__Impl"


    // $ANTLR start "rule__FeatureAccess__Group_2_1__3"
    // InternalScenarioExpressions.g:4116:1: rule__FeatureAccess__Group_2_1__3 : rule__FeatureAccess__Group_2_1__3__Impl ;
    public final void rule__FeatureAccess__Group_2_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4120:1: ( rule__FeatureAccess__Group_2_1__3__Impl )
            // InternalScenarioExpressions.g:4121:2: rule__FeatureAccess__Group_2_1__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group_2_1__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_1__3"


    // $ANTLR start "rule__FeatureAccess__Group_2_1__3__Impl"
    // InternalScenarioExpressions.g:4127:1: rule__FeatureAccess__Group_2_1__3__Impl : ( ')' ) ;
    public final void rule__FeatureAccess__Group_2_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4131:1: ( ( ')' ) )
            // InternalScenarioExpressions.g:4132:1: ( ')' )
            {
            // InternalScenarioExpressions.g:4132:1: ( ')' )
            // InternalScenarioExpressions.g:4133:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getRightParenthesisKeyword_2_1_3()); 
            }
            match(input,43,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getRightParenthesisKeyword_2_1_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_1__3__Impl"


    // $ANTLR start "rule__FeatureAccess__Group_2_1_2__0"
    // InternalScenarioExpressions.g:4154:1: rule__FeatureAccess__Group_2_1_2__0 : rule__FeatureAccess__Group_2_1_2__0__Impl rule__FeatureAccess__Group_2_1_2__1 ;
    public final void rule__FeatureAccess__Group_2_1_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4158:1: ( rule__FeatureAccess__Group_2_1_2__0__Impl rule__FeatureAccess__Group_2_1_2__1 )
            // InternalScenarioExpressions.g:4159:2: rule__FeatureAccess__Group_2_1_2__0__Impl rule__FeatureAccess__Group_2_1_2__1
            {
            pushFollow(FollowSets000.FOLLOW_31);
            rule__FeatureAccess__Group_2_1_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group_2_1_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_1_2__0"


    // $ANTLR start "rule__FeatureAccess__Group_2_1_2__0__Impl"
    // InternalScenarioExpressions.g:4166:1: rule__FeatureAccess__Group_2_1_2__0__Impl : ( ( rule__FeatureAccess__ParametersAssignment_2_1_2_0 ) ) ;
    public final void rule__FeatureAccess__Group_2_1_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4170:1: ( ( ( rule__FeatureAccess__ParametersAssignment_2_1_2_0 ) ) )
            // InternalScenarioExpressions.g:4171:1: ( ( rule__FeatureAccess__ParametersAssignment_2_1_2_0 ) )
            {
            // InternalScenarioExpressions.g:4171:1: ( ( rule__FeatureAccess__ParametersAssignment_2_1_2_0 ) )
            // InternalScenarioExpressions.g:4172:1: ( rule__FeatureAccess__ParametersAssignment_2_1_2_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getParametersAssignment_2_1_2_0()); 
            }
            // InternalScenarioExpressions.g:4173:1: ( rule__FeatureAccess__ParametersAssignment_2_1_2_0 )
            // InternalScenarioExpressions.g:4173:2: rule__FeatureAccess__ParametersAssignment_2_1_2_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__ParametersAssignment_2_1_2_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getParametersAssignment_2_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_1_2__0__Impl"


    // $ANTLR start "rule__FeatureAccess__Group_2_1_2__1"
    // InternalScenarioExpressions.g:4183:1: rule__FeatureAccess__Group_2_1_2__1 : rule__FeatureAccess__Group_2_1_2__1__Impl ;
    public final void rule__FeatureAccess__Group_2_1_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4187:1: ( rule__FeatureAccess__Group_2_1_2__1__Impl )
            // InternalScenarioExpressions.g:4188:2: rule__FeatureAccess__Group_2_1_2__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group_2_1_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_1_2__1"


    // $ANTLR start "rule__FeatureAccess__Group_2_1_2__1__Impl"
    // InternalScenarioExpressions.g:4194:1: rule__FeatureAccess__Group_2_1_2__1__Impl : ( ( rule__FeatureAccess__Group_2_1_2_1__0 )* ) ;
    public final void rule__FeatureAccess__Group_2_1_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4198:1: ( ( ( rule__FeatureAccess__Group_2_1_2_1__0 )* ) )
            // InternalScenarioExpressions.g:4199:1: ( ( rule__FeatureAccess__Group_2_1_2_1__0 )* )
            {
            // InternalScenarioExpressions.g:4199:1: ( ( rule__FeatureAccess__Group_2_1_2_1__0 )* )
            // InternalScenarioExpressions.g:4200:1: ( rule__FeatureAccess__Group_2_1_2_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getGroup_2_1_2_1()); 
            }
            // InternalScenarioExpressions.g:4201:1: ( rule__FeatureAccess__Group_2_1_2_1__0 )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==47) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalScenarioExpressions.g:4201:2: rule__FeatureAccess__Group_2_1_2_1__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_32);
            	    rule__FeatureAccess__Group_2_1_2_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getGroup_2_1_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_1_2__1__Impl"


    // $ANTLR start "rule__FeatureAccess__Group_2_1_2_1__0"
    // InternalScenarioExpressions.g:4215:1: rule__FeatureAccess__Group_2_1_2_1__0 : rule__FeatureAccess__Group_2_1_2_1__0__Impl rule__FeatureAccess__Group_2_1_2_1__1 ;
    public final void rule__FeatureAccess__Group_2_1_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4219:1: ( rule__FeatureAccess__Group_2_1_2_1__0__Impl rule__FeatureAccess__Group_2_1_2_1__1 )
            // InternalScenarioExpressions.g:4220:2: rule__FeatureAccess__Group_2_1_2_1__0__Impl rule__FeatureAccess__Group_2_1_2_1__1
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__FeatureAccess__Group_2_1_2_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group_2_1_2_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_1_2_1__0"


    // $ANTLR start "rule__FeatureAccess__Group_2_1_2_1__0__Impl"
    // InternalScenarioExpressions.g:4227:1: rule__FeatureAccess__Group_2_1_2_1__0__Impl : ( ',' ) ;
    public final void rule__FeatureAccess__Group_2_1_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4231:1: ( ( ',' ) )
            // InternalScenarioExpressions.g:4232:1: ( ',' )
            {
            // InternalScenarioExpressions.g:4232:1: ( ',' )
            // InternalScenarioExpressions.g:4233:1: ','
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getCommaKeyword_2_1_2_1_0()); 
            }
            match(input,47,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getCommaKeyword_2_1_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_1_2_1__0__Impl"


    // $ANTLR start "rule__FeatureAccess__Group_2_1_2_1__1"
    // InternalScenarioExpressions.g:4246:1: rule__FeatureAccess__Group_2_1_2_1__1 : rule__FeatureAccess__Group_2_1_2_1__1__Impl ;
    public final void rule__FeatureAccess__Group_2_1_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4250:1: ( rule__FeatureAccess__Group_2_1_2_1__1__Impl )
            // InternalScenarioExpressions.g:4251:2: rule__FeatureAccess__Group_2_1_2_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group_2_1_2_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_1_2_1__1"


    // $ANTLR start "rule__FeatureAccess__Group_2_1_2_1__1__Impl"
    // InternalScenarioExpressions.g:4257:1: rule__FeatureAccess__Group_2_1_2_1__1__Impl : ( ( rule__FeatureAccess__ParametersAssignment_2_1_2_1_1 ) ) ;
    public final void rule__FeatureAccess__Group_2_1_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4261:1: ( ( ( rule__FeatureAccess__ParametersAssignment_2_1_2_1_1 ) ) )
            // InternalScenarioExpressions.g:4262:1: ( ( rule__FeatureAccess__ParametersAssignment_2_1_2_1_1 ) )
            {
            // InternalScenarioExpressions.g:4262:1: ( ( rule__FeatureAccess__ParametersAssignment_2_1_2_1_1 ) )
            // InternalScenarioExpressions.g:4263:1: ( rule__FeatureAccess__ParametersAssignment_2_1_2_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getParametersAssignment_2_1_2_1_1()); 
            }
            // InternalScenarioExpressions.g:4264:1: ( rule__FeatureAccess__ParametersAssignment_2_1_2_1_1 )
            // InternalScenarioExpressions.g:4264:2: rule__FeatureAccess__ParametersAssignment_2_1_2_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__ParametersAssignment_2_1_2_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getParametersAssignment_2_1_2_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_2_1_2_1__1__Impl"


    // $ANTLR start "rule__Document__ImportsAssignment_0"
    // InternalScenarioExpressions.g:4279:1: rule__Document__ImportsAssignment_0 : ( ruleImport ) ;
    public final void rule__Document__ImportsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4283:1: ( ( ruleImport ) )
            // InternalScenarioExpressions.g:4284:1: ( ruleImport )
            {
            // InternalScenarioExpressions.g:4284:1: ( ruleImport )
            // InternalScenarioExpressions.g:4285:1: ruleImport
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getImportsImportParserRuleCall_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleImport();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getImportsImportParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__ImportsAssignment_0"


    // $ANTLR start "rule__Document__DomainsAssignment_1_1"
    // InternalScenarioExpressions.g:4294:1: rule__Document__DomainsAssignment_1_1 : ( ( RULE_ID ) ) ;
    public final void rule__Document__DomainsAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4298:1: ( ( ( RULE_ID ) ) )
            // InternalScenarioExpressions.g:4299:1: ( ( RULE_ID ) )
            {
            // InternalScenarioExpressions.g:4299:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:4300:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getDomainsEPackageCrossReference_1_1_0()); 
            }
            // InternalScenarioExpressions.g:4301:1: ( RULE_ID )
            // InternalScenarioExpressions.g:4302:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getDomainsEPackageIDTerminalRuleCall_1_1_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getDomainsEPackageIDTerminalRuleCall_1_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getDomainsEPackageCrossReference_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__DomainsAssignment_1_1"


    // $ANTLR start "rule__Document__ExpressionsAssignment_2"
    // InternalScenarioExpressions.g:4313:1: rule__Document__ExpressionsAssignment_2 : ( ruleExpressionRegion ) ;
    public final void rule__Document__ExpressionsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4317:1: ( ( ruleExpressionRegion ) )
            // InternalScenarioExpressions.g:4318:1: ( ruleExpressionRegion )
            {
            // InternalScenarioExpressions.g:4318:1: ( ruleExpressionRegion )
            // InternalScenarioExpressions.g:4319:1: ruleExpressionRegion
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getExpressionsExpressionRegionParserRuleCall_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpressionRegion();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getExpressionsExpressionRegionParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__ExpressionsAssignment_2"


    // $ANTLR start "rule__Import__ImportURIAssignment_1"
    // InternalScenarioExpressions.g:4328:1: rule__Import__ImportURIAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Import__ImportURIAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4332:1: ( ( RULE_STRING ) )
            // InternalScenarioExpressions.g:4333:1: ( RULE_STRING )
            {
            // InternalScenarioExpressions.g:4333:1: ( RULE_STRING )
            // InternalScenarioExpressions.g:4334:1: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
            }
            match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__ImportURIAssignment_1"


    // $ANTLR start "rule__ExpressionRegion__ExpressionsAssignment_2_0"
    // InternalScenarioExpressions.g:4343:1: rule__ExpressionRegion__ExpressionsAssignment_2_0 : ( ruleExpressionOrRegion ) ;
    public final void rule__ExpressionRegion__ExpressionsAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4347:1: ( ( ruleExpressionOrRegion ) )
            // InternalScenarioExpressions.g:4348:1: ( ruleExpressionOrRegion )
            {
            // InternalScenarioExpressions.g:4348:1: ( ruleExpressionOrRegion )
            // InternalScenarioExpressions.g:4349:1: ruleExpressionOrRegion
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getExpressionsExpressionOrRegionParserRuleCall_2_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpressionOrRegion();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getExpressionsExpressionOrRegionParserRuleCall_2_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__ExpressionsAssignment_2_0"


    // $ANTLR start "rule__VariableAssignment__VariableAssignment_0"
    // InternalScenarioExpressions.g:4360:1: rule__VariableAssignment__VariableAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__VariableAssignment__VariableAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4364:1: ( ( ( RULE_ID ) ) )
            // InternalScenarioExpressions.g:4365:1: ( ( RULE_ID ) )
            {
            // InternalScenarioExpressions.g:4365:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:4366:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationCrossReference_0_0()); 
            }
            // InternalScenarioExpressions.g:4367:1: ( RULE_ID )
            // InternalScenarioExpressions.g:4368:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationIDTerminalRuleCall_0_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationIDTerminalRuleCall_0_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationCrossReference_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__VariableAssignment_0"


    // $ANTLR start "rule__VariableAssignment__ExpressionAssignment_2"
    // InternalScenarioExpressions.g:4379:1: rule__VariableAssignment__ExpressionAssignment_2 : ( ruleExpression ) ;
    public final void rule__VariableAssignment__ExpressionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4383:1: ( ( ruleExpression ) )
            // InternalScenarioExpressions.g:4384:1: ( ruleExpression )
            {
            // InternalScenarioExpressions.g:4384:1: ( ruleExpression )
            // InternalScenarioExpressions.g:4385:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getExpressionExpressionParserRuleCall_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getExpressionExpressionParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__ExpressionAssignment_2"


    // $ANTLR start "rule__TypedVariableDeclaration__TypeAssignment_1"
    // InternalScenarioExpressions.g:4394:1: rule__TypedVariableDeclaration__TypeAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__TypedVariableDeclaration__TypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4398:1: ( ( ( RULE_ID ) ) )
            // InternalScenarioExpressions.g:4399:1: ( ( RULE_ID ) )
            {
            // InternalScenarioExpressions.g:4399:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:4400:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierCrossReference_1_0()); 
            }
            // InternalScenarioExpressions.g:4401:1: ( RULE_ID )
            // InternalScenarioExpressions.g:4402:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierIDTerminalRuleCall_1_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierIDTerminalRuleCall_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierCrossReference_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__TypeAssignment_1"


    // $ANTLR start "rule__TypedVariableDeclaration__NameAssignment_2"
    // InternalScenarioExpressions.g:4413:1: rule__TypedVariableDeclaration__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__TypedVariableDeclaration__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4417:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:4418:1: ( RULE_ID )
            {
            // InternalScenarioExpressions.g:4418:1: ( RULE_ID )
            // InternalScenarioExpressions.g:4419:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__NameAssignment_2"


    // $ANTLR start "rule__TypedVariableDeclaration__ExpressionAssignment_3_1"
    // InternalScenarioExpressions.g:4428:1: rule__TypedVariableDeclaration__ExpressionAssignment_3_1 : ( ruleExpression ) ;
    public final void rule__TypedVariableDeclaration__ExpressionAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4432:1: ( ( ruleExpression ) )
            // InternalScenarioExpressions.g:4433:1: ( ruleExpression )
            {
            // InternalScenarioExpressions.g:4433:1: ( ruleExpression )
            // InternalScenarioExpressions.g:4434:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getExpressionExpressionParserRuleCall_3_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getExpressionExpressionParserRuleCall_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__ExpressionAssignment_3_1"


    // $ANTLR start "rule__ClockDeclaration__NameAssignment_2"
    // InternalScenarioExpressions.g:4443:1: rule__ClockDeclaration__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__ClockDeclaration__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4447:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:4448:1: ( RULE_ID )
            {
            // InternalScenarioExpressions.g:4448:1: ( RULE_ID )
            // InternalScenarioExpressions.g:4449:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClockDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClockDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockDeclaration__NameAssignment_2"


    // $ANTLR start "rule__ClockDeclaration__ExpressionAssignment_3_1"
    // InternalScenarioExpressions.g:4458:1: rule__ClockDeclaration__ExpressionAssignment_3_1 : ( ruleIntegerValue ) ;
    public final void rule__ClockDeclaration__ExpressionAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4462:1: ( ( ruleIntegerValue ) )
            // InternalScenarioExpressions.g:4463:1: ( ruleIntegerValue )
            {
            // InternalScenarioExpressions.g:4463:1: ( ruleIntegerValue )
            // InternalScenarioExpressions.g:4464:1: ruleIntegerValue
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClockDeclarationAccess().getExpressionIntegerValueParserRuleCall_3_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleIntegerValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClockDeclarationAccess().getExpressionIntegerValueParserRuleCall_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockDeclaration__ExpressionAssignment_3_1"


    // $ANTLR start "rule__ClockAssignment__VariableAssignment_1"
    // InternalScenarioExpressions.g:4478:1: rule__ClockAssignment__VariableAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__ClockAssignment__VariableAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4482:1: ( ( ( RULE_ID ) ) )
            // InternalScenarioExpressions.g:4483:1: ( ( RULE_ID ) )
            {
            // InternalScenarioExpressions.g:4483:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:4484:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClockAssignmentAccess().getVariableClockDeclarationCrossReference_1_0()); 
            }
            // InternalScenarioExpressions.g:4485:1: ( RULE_ID )
            // InternalScenarioExpressions.g:4486:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClockAssignmentAccess().getVariableClockDeclarationIDTerminalRuleCall_1_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClockAssignmentAccess().getVariableClockDeclarationIDTerminalRuleCall_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClockAssignmentAccess().getVariableClockDeclarationCrossReference_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockAssignment__VariableAssignment_1"


    // $ANTLR start "rule__ClockAssignment__ExpressionAssignment_2_1"
    // InternalScenarioExpressions.g:4497:1: rule__ClockAssignment__ExpressionAssignment_2_1 : ( ruleIntegerValue ) ;
    public final void rule__ClockAssignment__ExpressionAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4501:1: ( ( ruleIntegerValue ) )
            // InternalScenarioExpressions.g:4502:1: ( ruleIntegerValue )
            {
            // InternalScenarioExpressions.g:4502:1: ( ruleIntegerValue )
            // InternalScenarioExpressions.g:4503:1: ruleIntegerValue
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClockAssignmentAccess().getExpressionIntegerValueParserRuleCall_2_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleIntegerValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClockAssignmentAccess().getExpressionIntegerValueParserRuleCall_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClockAssignment__ExpressionAssignment_2_1"


    // $ANTLR start "rule__ImplicationExpression__OperatorAssignment_1_1"
    // InternalScenarioExpressions.g:4512:1: rule__ImplicationExpression__OperatorAssignment_1_1 : ( ( '=>' ) ) ;
    public final void rule__ImplicationExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4516:1: ( ( ( '=>' ) ) )
            // InternalScenarioExpressions.g:4517:1: ( ( '=>' ) )
            {
            // InternalScenarioExpressions.g:4517:1: ( ( '=>' ) )
            // InternalScenarioExpressions.g:4518:1: ( '=>' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImplicationExpressionAccess().getOperatorEqualsSignGreaterThanSignKeyword_1_1_0()); 
            }
            // InternalScenarioExpressions.g:4519:1: ( '=>' )
            // InternalScenarioExpressions.g:4520:1: '=>'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImplicationExpressionAccess().getOperatorEqualsSignGreaterThanSignKeyword_1_1_0()); 
            }
            match(input,48,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImplicationExpressionAccess().getOperatorEqualsSignGreaterThanSignKeyword_1_1_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImplicationExpressionAccess().getOperatorEqualsSignGreaterThanSignKeyword_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImplicationExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__ImplicationExpression__RightAssignment_1_2"
    // InternalScenarioExpressions.g:4535:1: rule__ImplicationExpression__RightAssignment_1_2 : ( ruleImplicationExpression ) ;
    public final void rule__ImplicationExpression__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4539:1: ( ( ruleImplicationExpression ) )
            // InternalScenarioExpressions.g:4540:1: ( ruleImplicationExpression )
            {
            // InternalScenarioExpressions.g:4540:1: ( ruleImplicationExpression )
            // InternalScenarioExpressions.g:4541:1: ruleImplicationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImplicationExpressionAccess().getRightImplicationExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleImplicationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImplicationExpressionAccess().getRightImplicationExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImplicationExpression__RightAssignment_1_2"


    // $ANTLR start "rule__DisjunctionExpression__OperatorAssignment_1_1"
    // InternalScenarioExpressions.g:4550:1: rule__DisjunctionExpression__OperatorAssignment_1_1 : ( ( '||' ) ) ;
    public final void rule__DisjunctionExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4554:1: ( ( ( '||' ) ) )
            // InternalScenarioExpressions.g:4555:1: ( ( '||' ) )
            {
            // InternalScenarioExpressions.g:4555:1: ( ( '||' ) )
            // InternalScenarioExpressions.g:4556:1: ( '||' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineVerticalLineKeyword_1_1_0()); 
            }
            // InternalScenarioExpressions.g:4557:1: ( '||' )
            // InternalScenarioExpressions.g:4558:1: '||'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineVerticalLineKeyword_1_1_0()); 
            }
            match(input,49,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineVerticalLineKeyword_1_1_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineVerticalLineKeyword_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__DisjunctionExpression__RightAssignment_1_2"
    // InternalScenarioExpressions.g:4573:1: rule__DisjunctionExpression__RightAssignment_1_2 : ( ruleDisjunctionExpression ) ;
    public final void rule__DisjunctionExpression__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4577:1: ( ( ruleDisjunctionExpression ) )
            // InternalScenarioExpressions.g:4578:1: ( ruleDisjunctionExpression )
            {
            // InternalScenarioExpressions.g:4578:1: ( ruleDisjunctionExpression )
            // InternalScenarioExpressions.g:4579:1: ruleDisjunctionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getRightDisjunctionExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getRightDisjunctionExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__RightAssignment_1_2"


    // $ANTLR start "rule__ConjunctionExpression__OperatorAssignment_1_1"
    // InternalScenarioExpressions.g:4588:1: rule__ConjunctionExpression__OperatorAssignment_1_1 : ( ( '&&' ) ) ;
    public final void rule__ConjunctionExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4592:1: ( ( ( '&&' ) ) )
            // InternalScenarioExpressions.g:4593:1: ( ( '&&' ) )
            {
            // InternalScenarioExpressions.g:4593:1: ( ( '&&' ) )
            // InternalScenarioExpressions.g:4594:1: ( '&&' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandAmpersandKeyword_1_1_0()); 
            }
            // InternalScenarioExpressions.g:4595:1: ( '&&' )
            // InternalScenarioExpressions.g:4596:1: '&&'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandAmpersandKeyword_1_1_0()); 
            }
            match(input,50,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandAmpersandKeyword_1_1_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandAmpersandKeyword_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__ConjunctionExpression__RightAssignment_1_2"
    // InternalScenarioExpressions.g:4611:1: rule__ConjunctionExpression__RightAssignment_1_2 : ( ruleConjunctionExpression ) ;
    public final void rule__ConjunctionExpression__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4615:1: ( ( ruleConjunctionExpression ) )
            // InternalScenarioExpressions.g:4616:1: ( ruleConjunctionExpression )
            {
            // InternalScenarioExpressions.g:4616:1: ( ruleConjunctionExpression )
            // InternalScenarioExpressions.g:4617:1: ruleConjunctionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getRightConjunctionExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getRightConjunctionExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__RightAssignment_1_2"


    // $ANTLR start "rule__RelationExpression__OperatorAssignment_1_1"
    // InternalScenarioExpressions.g:4626:1: rule__RelationExpression__OperatorAssignment_1_1 : ( ( rule__RelationExpression__OperatorAlternatives_1_1_0 ) ) ;
    public final void rule__RelationExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4630:1: ( ( ( rule__RelationExpression__OperatorAlternatives_1_1_0 ) ) )
            // InternalScenarioExpressions.g:4631:1: ( ( rule__RelationExpression__OperatorAlternatives_1_1_0 ) )
            {
            // InternalScenarioExpressions.g:4631:1: ( ( rule__RelationExpression__OperatorAlternatives_1_1_0 ) )
            // InternalScenarioExpressions.g:4632:1: ( rule__RelationExpression__OperatorAlternatives_1_1_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getOperatorAlternatives_1_1_0()); 
            }
            // InternalScenarioExpressions.g:4633:1: ( rule__RelationExpression__OperatorAlternatives_1_1_0 )
            // InternalScenarioExpressions.g:4633:2: rule__RelationExpression__OperatorAlternatives_1_1_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__OperatorAlternatives_1_1_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getOperatorAlternatives_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__RelationExpression__RightAssignment_1_2"
    // InternalScenarioExpressions.g:4642:1: rule__RelationExpression__RightAssignment_1_2 : ( ruleRelationExpression ) ;
    public final void rule__RelationExpression__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4646:1: ( ( ruleRelationExpression ) )
            // InternalScenarioExpressions.g:4647:1: ( ruleRelationExpression )
            {
            // InternalScenarioExpressions.g:4647:1: ( ruleRelationExpression )
            // InternalScenarioExpressions.g:4648:1: ruleRelationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getRightRelationExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleRelationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getRightRelationExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__RightAssignment_1_2"


    // $ANTLR start "rule__AdditionExpression__OperatorAssignment_1_1"
    // InternalScenarioExpressions.g:4660:1: rule__AdditionExpression__OperatorAssignment_1_1 : ( ( rule__AdditionExpression__OperatorAlternatives_1_1_0 ) ) ;
    public final void rule__AdditionExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4664:1: ( ( ( rule__AdditionExpression__OperatorAlternatives_1_1_0 ) ) )
            // InternalScenarioExpressions.g:4665:1: ( ( rule__AdditionExpression__OperatorAlternatives_1_1_0 ) )
            {
            // InternalScenarioExpressions.g:4665:1: ( ( rule__AdditionExpression__OperatorAlternatives_1_1_0 ) )
            // InternalScenarioExpressions.g:4666:1: ( rule__AdditionExpression__OperatorAlternatives_1_1_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getOperatorAlternatives_1_1_0()); 
            }
            // InternalScenarioExpressions.g:4667:1: ( rule__AdditionExpression__OperatorAlternatives_1_1_0 )
            // InternalScenarioExpressions.g:4667:2: rule__AdditionExpression__OperatorAlternatives_1_1_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__OperatorAlternatives_1_1_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getOperatorAlternatives_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__AdditionExpression__RightAssignment_1_2"
    // InternalScenarioExpressions.g:4676:1: rule__AdditionExpression__RightAssignment_1_2 : ( ruleAdditionExpression ) ;
    public final void rule__AdditionExpression__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4680:1: ( ( ruleAdditionExpression ) )
            // InternalScenarioExpressions.g:4681:1: ( ruleAdditionExpression )
            {
            // InternalScenarioExpressions.g:4681:1: ( ruleAdditionExpression )
            // InternalScenarioExpressions.g:4682:1: ruleAdditionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getRightAdditionExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getRightAdditionExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__RightAssignment_1_2"


    // $ANTLR start "rule__MultiplicationExpression__OperatorAssignment_1_1"
    // InternalScenarioExpressions.g:4691:1: rule__MultiplicationExpression__OperatorAssignment_1_1 : ( ( rule__MultiplicationExpression__OperatorAlternatives_1_1_0 ) ) ;
    public final void rule__MultiplicationExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4695:1: ( ( ( rule__MultiplicationExpression__OperatorAlternatives_1_1_0 ) ) )
            // InternalScenarioExpressions.g:4696:1: ( ( rule__MultiplicationExpression__OperatorAlternatives_1_1_0 ) )
            {
            // InternalScenarioExpressions.g:4696:1: ( ( rule__MultiplicationExpression__OperatorAlternatives_1_1_0 ) )
            // InternalScenarioExpressions.g:4697:1: ( rule__MultiplicationExpression__OperatorAlternatives_1_1_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getOperatorAlternatives_1_1_0()); 
            }
            // InternalScenarioExpressions.g:4698:1: ( rule__MultiplicationExpression__OperatorAlternatives_1_1_0 )
            // InternalScenarioExpressions.g:4698:2: rule__MultiplicationExpression__OperatorAlternatives_1_1_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__OperatorAlternatives_1_1_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getOperatorAlternatives_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__MultiplicationExpression__RightAssignment_1_2"
    // InternalScenarioExpressions.g:4707:1: rule__MultiplicationExpression__RightAssignment_1_2 : ( ruleMultiplicationExpression ) ;
    public final void rule__MultiplicationExpression__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4711:1: ( ( ruleMultiplicationExpression ) )
            // InternalScenarioExpressions.g:4712:1: ( ruleMultiplicationExpression )
            {
            // InternalScenarioExpressions.g:4712:1: ( ruleMultiplicationExpression )
            // InternalScenarioExpressions.g:4713:1: ruleMultiplicationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getRightMultiplicationExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getRightMultiplicationExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__RightAssignment_1_2"


    // $ANTLR start "rule__NegatedExpression__OperatorAssignment_0_1"
    // InternalScenarioExpressions.g:4722:1: rule__NegatedExpression__OperatorAssignment_0_1 : ( ( rule__NegatedExpression__OperatorAlternatives_0_1_0 ) ) ;
    public final void rule__NegatedExpression__OperatorAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4726:1: ( ( ( rule__NegatedExpression__OperatorAlternatives_0_1_0 ) ) )
            // InternalScenarioExpressions.g:4727:1: ( ( rule__NegatedExpression__OperatorAlternatives_0_1_0 ) )
            {
            // InternalScenarioExpressions.g:4727:1: ( ( rule__NegatedExpression__OperatorAlternatives_0_1_0 ) )
            // InternalScenarioExpressions.g:4728:1: ( rule__NegatedExpression__OperatorAlternatives_0_1_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionAccess().getOperatorAlternatives_0_1_0()); 
            }
            // InternalScenarioExpressions.g:4729:1: ( rule__NegatedExpression__OperatorAlternatives_0_1_0 )
            // InternalScenarioExpressions.g:4729:2: rule__NegatedExpression__OperatorAlternatives_0_1_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__OperatorAlternatives_0_1_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionAccess().getOperatorAlternatives_0_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__OperatorAssignment_0_1"


    // $ANTLR start "rule__NegatedExpression__OperandAssignment_0_2"
    // InternalScenarioExpressions.g:4738:1: rule__NegatedExpression__OperandAssignment_0_2 : ( ruleBasicExpression ) ;
    public final void rule__NegatedExpression__OperandAssignment_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4742:1: ( ( ruleBasicExpression ) )
            // InternalScenarioExpressions.g:4743:1: ( ruleBasicExpression )
            {
            // InternalScenarioExpressions.g:4743:1: ( ruleBasicExpression )
            // InternalScenarioExpressions.g:4744:1: ruleBasicExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionAccess().getOperandBasicExpressionParserRuleCall_0_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleBasicExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionAccess().getOperandBasicExpressionParserRuleCall_0_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__OperandAssignment_0_2"


    // $ANTLR start "rule__IntegerValue__ValueAssignment"
    // InternalScenarioExpressions.g:4753:1: rule__IntegerValue__ValueAssignment : ( ( rule__IntegerValue__ValueAlternatives_0 ) ) ;
    public final void rule__IntegerValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4757:1: ( ( ( rule__IntegerValue__ValueAlternatives_0 ) ) )
            // InternalScenarioExpressions.g:4758:1: ( ( rule__IntegerValue__ValueAlternatives_0 ) )
            {
            // InternalScenarioExpressions.g:4758:1: ( ( rule__IntegerValue__ValueAlternatives_0 ) )
            // InternalScenarioExpressions.g:4759:1: ( rule__IntegerValue__ValueAlternatives_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIntegerValueAccess().getValueAlternatives_0()); 
            }
            // InternalScenarioExpressions.g:4760:1: ( rule__IntegerValue__ValueAlternatives_0 )
            // InternalScenarioExpressions.g:4760:2: rule__IntegerValue__ValueAlternatives_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__IntegerValue__ValueAlternatives_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getIntegerValueAccess().getValueAlternatives_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerValue__ValueAssignment"


    // $ANTLR start "rule__BooleanValue__ValueAssignment"
    // InternalScenarioExpressions.g:4769:1: rule__BooleanValue__ValueAssignment : ( RULE_BOOL ) ;
    public final void rule__BooleanValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4773:1: ( ( RULE_BOOL ) )
            // InternalScenarioExpressions.g:4774:1: ( RULE_BOOL )
            {
            // InternalScenarioExpressions.g:4774:1: ( RULE_BOOL )
            // InternalScenarioExpressions.g:4775:1: RULE_BOOL
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBooleanValueAccess().getValueBOOLTerminalRuleCall_0()); 
            }
            match(input,RULE_BOOL,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBooleanValueAccess().getValueBOOLTerminalRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanValue__ValueAssignment"


    // $ANTLR start "rule__StringValue__ValueAssignment"
    // InternalScenarioExpressions.g:4784:1: rule__StringValue__ValueAssignment : ( RULE_STRING ) ;
    public final void rule__StringValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4788:1: ( ( RULE_STRING ) )
            // InternalScenarioExpressions.g:4789:1: ( RULE_STRING )
            {
            // InternalScenarioExpressions.g:4789:1: ( RULE_STRING )
            // InternalScenarioExpressions.g:4790:1: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
            }
            match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringValue__ValueAssignment"


    // $ANTLR start "rule__EnumValue__TypeAssignment_0"
    // InternalScenarioExpressions.g:4799:1: rule__EnumValue__TypeAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__EnumValue__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4803:1: ( ( ( RULE_ID ) ) )
            // InternalScenarioExpressions.g:4804:1: ( ( RULE_ID ) )
            {
            // InternalScenarioExpressions.g:4804:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:4805:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getTypeEEnumCrossReference_0_0()); 
            }
            // InternalScenarioExpressions.g:4806:1: ( RULE_ID )
            // InternalScenarioExpressions.g:4807:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getTypeEEnumIDTerminalRuleCall_0_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getTypeEEnumIDTerminalRuleCall_0_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getTypeEEnumCrossReference_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__TypeAssignment_0"


    // $ANTLR start "rule__EnumValue__ValueAssignment_2"
    // InternalScenarioExpressions.g:4818:1: rule__EnumValue__ValueAssignment_2 : ( ( RULE_ID ) ) ;
    public final void rule__EnumValue__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4822:1: ( ( ( RULE_ID ) ) )
            // InternalScenarioExpressions.g:4823:1: ( ( RULE_ID ) )
            {
            // InternalScenarioExpressions.g:4823:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:4824:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getValueEEnumLiteralCrossReference_2_0()); 
            }
            // InternalScenarioExpressions.g:4825:1: ( RULE_ID )
            // InternalScenarioExpressions.g:4826:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getValueEEnumLiteralIDTerminalRuleCall_2_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getValueEEnumLiteralIDTerminalRuleCall_2_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getValueEEnumLiteralCrossReference_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__ValueAssignment_2"


    // $ANTLR start "rule__VariableValue__ValueAssignment"
    // InternalScenarioExpressions.g:4837:1: rule__VariableValue__ValueAssignment : ( ( RULE_ID ) ) ;
    public final void rule__VariableValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4841:1: ( ( ( RULE_ID ) ) )
            // InternalScenarioExpressions.g:4842:1: ( ( RULE_ID ) )
            {
            // InternalScenarioExpressions.g:4842:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:4843:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableValueAccess().getValueVariableCrossReference_0()); 
            }
            // InternalScenarioExpressions.g:4844:1: ( RULE_ID )
            // InternalScenarioExpressions.g:4845:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableValueAccess().getValueVariableIDTerminalRuleCall_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableValueAccess().getValueVariableIDTerminalRuleCall_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableValueAccess().getValueVariableCrossReference_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableValue__ValueAssignment"


    // $ANTLR start "rule__CollectionAccess__CollectionOperationAssignment_0"
    // InternalScenarioExpressions.g:4856:1: rule__CollectionAccess__CollectionOperationAssignment_0 : ( ruleCollectionOperation ) ;
    public final void rule__CollectionAccess__CollectionOperationAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4860:1: ( ( ruleCollectionOperation ) )
            // InternalScenarioExpressions.g:4861:1: ( ruleCollectionOperation )
            {
            // InternalScenarioExpressions.g:4861:1: ( ruleCollectionOperation )
            // InternalScenarioExpressions.g:4862:1: ruleCollectionOperation
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getCollectionOperationCollectionOperationEnumRuleCall_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleCollectionOperation();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getCollectionOperationCollectionOperationEnumRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__CollectionOperationAssignment_0"


    // $ANTLR start "rule__CollectionAccess__ParameterAssignment_2"
    // InternalScenarioExpressions.g:4871:1: rule__CollectionAccess__ParameterAssignment_2 : ( ruleExpression ) ;
    public final void rule__CollectionAccess__ParameterAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4875:1: ( ( ruleExpression ) )
            // InternalScenarioExpressions.g:4876:1: ( ruleExpression )
            {
            // InternalScenarioExpressions.g:4876:1: ( ruleExpression )
            // InternalScenarioExpressions.g:4877:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getParameterExpressionParserRuleCall_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getParameterExpressionParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__ParameterAssignment_2"


    // $ANTLR start "rule__FeatureAccess__TargetAssignment_0"
    // InternalScenarioExpressions.g:4886:1: rule__FeatureAccess__TargetAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__FeatureAccess__TargetAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4890:1: ( ( ( RULE_ID ) ) )
            // InternalScenarioExpressions.g:4891:1: ( ( RULE_ID ) )
            {
            // InternalScenarioExpressions.g:4891:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:4892:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getTargetEObjectCrossReference_0_0()); 
            }
            // InternalScenarioExpressions.g:4893:1: ( RULE_ID )
            // InternalScenarioExpressions.g:4894:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getTargetEObjectIDTerminalRuleCall_0_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getTargetEObjectIDTerminalRuleCall_0_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getTargetEObjectCrossReference_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__TargetAssignment_0"


    // $ANTLR start "rule__FeatureAccess__ValueAssignment_2_0_0"
    // InternalScenarioExpressions.g:4905:1: rule__FeatureAccess__ValueAssignment_2_0_0 : ( ruleStructuralFeatureValue ) ;
    public final void rule__FeatureAccess__ValueAssignment_2_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4909:1: ( ( ruleStructuralFeatureValue ) )
            // InternalScenarioExpressions.g:4910:1: ( ruleStructuralFeatureValue )
            {
            // InternalScenarioExpressions.g:4910:1: ( ruleStructuralFeatureValue )
            // InternalScenarioExpressions.g:4911:1: ruleStructuralFeatureValue
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getValueStructuralFeatureValueParserRuleCall_2_0_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleStructuralFeatureValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getValueStructuralFeatureValueParserRuleCall_2_0_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__ValueAssignment_2_0_0"


    // $ANTLR start "rule__FeatureAccess__CollectionAccessAssignment_2_0_1_1"
    // InternalScenarioExpressions.g:4920:1: rule__FeatureAccess__CollectionAccessAssignment_2_0_1_1 : ( ruleCollectionAccess ) ;
    public final void rule__FeatureAccess__CollectionAccessAssignment_2_0_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4924:1: ( ( ruleCollectionAccess ) )
            // InternalScenarioExpressions.g:4925:1: ( ruleCollectionAccess )
            {
            // InternalScenarioExpressions.g:4925:1: ( ruleCollectionAccess )
            // InternalScenarioExpressions.g:4926:1: ruleCollectionAccess
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getCollectionAccessCollectionAccessParserRuleCall_2_0_1_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleCollectionAccess();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getCollectionAccessCollectionAccessParserRuleCall_2_0_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__CollectionAccessAssignment_2_0_1_1"


    // $ANTLR start "rule__FeatureAccess__ValueAssignment_2_1_0"
    // InternalScenarioExpressions.g:4935:1: rule__FeatureAccess__ValueAssignment_2_1_0 : ( ruleOperationValue ) ;
    public final void rule__FeatureAccess__ValueAssignment_2_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4939:1: ( ( ruleOperationValue ) )
            // InternalScenarioExpressions.g:4940:1: ( ruleOperationValue )
            {
            // InternalScenarioExpressions.g:4940:1: ( ruleOperationValue )
            // InternalScenarioExpressions.g:4941:1: ruleOperationValue
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getValueOperationValueParserRuleCall_2_1_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleOperationValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getValueOperationValueParserRuleCall_2_1_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__ValueAssignment_2_1_0"


    // $ANTLR start "rule__FeatureAccess__ParametersAssignment_2_1_2_0"
    // InternalScenarioExpressions.g:4950:1: rule__FeatureAccess__ParametersAssignment_2_1_2_0 : ( ruleExpression ) ;
    public final void rule__FeatureAccess__ParametersAssignment_2_1_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4954:1: ( ( ruleExpression ) )
            // InternalScenarioExpressions.g:4955:1: ( ruleExpression )
            {
            // InternalScenarioExpressions.g:4955:1: ( ruleExpression )
            // InternalScenarioExpressions.g:4956:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getParametersExpressionParserRuleCall_2_1_2_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getParametersExpressionParserRuleCall_2_1_2_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__ParametersAssignment_2_1_2_0"


    // $ANTLR start "rule__FeatureAccess__ParametersAssignment_2_1_2_1_1"
    // InternalScenarioExpressions.g:4965:1: rule__FeatureAccess__ParametersAssignment_2_1_2_1_1 : ( ruleExpression ) ;
    public final void rule__FeatureAccess__ParametersAssignment_2_1_2_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4969:1: ( ( ruleExpression ) )
            // InternalScenarioExpressions.g:4970:1: ( ruleExpression )
            {
            // InternalScenarioExpressions.g:4970:1: ( ruleExpression )
            // InternalScenarioExpressions.g:4971:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getParametersExpressionParserRuleCall_2_1_2_1_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getParametersExpressionParserRuleCall_2_1_2_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__ParametersAssignment_2_1_2_1_1"


    // $ANTLR start "rule__StructuralFeatureValue__ValueAssignment"
    // InternalScenarioExpressions.g:4980:1: rule__StructuralFeatureValue__ValueAssignment : ( ( RULE_ID ) ) ;
    public final void rule__StructuralFeatureValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:4984:1: ( ( ( RULE_ID ) ) )
            // InternalScenarioExpressions.g:4985:1: ( ( RULE_ID ) )
            {
            // InternalScenarioExpressions.g:4985:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:4986:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureCrossReference_0()); 
            }
            // InternalScenarioExpressions.g:4987:1: ( RULE_ID )
            // InternalScenarioExpressions.g:4988:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureIDTerminalRuleCall_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureIDTerminalRuleCall_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureCrossReference_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuralFeatureValue__ValueAssignment"


    // $ANTLR start "rule__OperationValue__ValueAssignment"
    // InternalScenarioExpressions.g:4999:1: rule__OperationValue__ValueAssignment : ( ( RULE_ID ) ) ;
    public final void rule__OperationValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:5003:1: ( ( ( RULE_ID ) ) )
            // InternalScenarioExpressions.g:5004:1: ( ( RULE_ID ) )
            {
            // InternalScenarioExpressions.g:5004:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:5005:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOperationValueAccess().getValueEOperationCrossReference_0()); 
            }
            // InternalScenarioExpressions.g:5006:1: ( RULE_ID )
            // InternalScenarioExpressions.g:5007:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOperationValueAccess().getValueEOperationIDTerminalRuleCall_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getOperationValueAccess().getValueEOperationIDTerminalRuleCall_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getOperationValueAccess().getValueEOperationCrossReference_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OperationValue__ValueAssignment"

    // Delegated rules


 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000A00000000L});
        public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000400000002L});
        public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000200000002L});
        public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000800000002L});
        public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000080L});
        public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000800000000L});
        public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x00002798012001F0L});
        public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x00002788012001F2L});
        public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000002000000000L});
        public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000004000000000L});
        public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x00002788012001F0L});
        public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000010000000000L});
        public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0001000000000000L});
        public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0002000000000000L});
        public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0004000000000000L});
        public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x00000000000FC000L});
        public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000300000L});
        public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000C00000L});
        public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000001200000L});
        public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000080000000000L});
        public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000100000000000L});
        public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000200000000000L});
        public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000040000000000L});
        public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x00002F88012001F0L});
        public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000400000000000L});
        public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x00000001FE000000L});
        public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000800000000000L});
        public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000800000000002L});
    }


}