/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.expressions.ui.contentassist.antlr;

import java.util.Collection;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.RecognitionException;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.ui.editor.contentassist.antlr.AbstractContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.FollowElement;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;

import com.google.inject.Inject;

import org.scenariotools.sml.expressions.services.ScenarioExpressionsGrammarAccess;

public class ScenarioExpressionsParser extends AbstractContentAssistParser {
	
	@Inject
	private ScenarioExpressionsGrammarAccess grammarAccess;
	
	private Map<AbstractElement, String> nameMappings;
	
	@Override
	protected org.scenariotools.sml.expressions.ui.contentassist.antlr.internal.InternalScenarioExpressionsParser createParser() {
		org.scenariotools.sml.expressions.ui.contentassist.antlr.internal.InternalScenarioExpressionsParser result = new org.scenariotools.sml.expressions.ui.contentassist.antlr.internal.InternalScenarioExpressionsParser(null);
		result.setGrammarAccess(grammarAccess);
		return result;
	}
	
	@Override
	protected String getRuleName(AbstractElement element) {
		if (nameMappings == null) {
			nameMappings = new HashMap<AbstractElement, String>() {
				private static final long serialVersionUID = 1L;
				{
					put(grammarAccess.getExpressionOrRegionAccess().getAlternatives(), "rule__ExpressionOrRegion__Alternatives");
					put(grammarAccess.getExpressionAndVariablesAccess().getAlternatives(), "rule__ExpressionAndVariables__Alternatives");
					put(grammarAccess.getVariableExpressionAccess().getAlternatives(), "rule__VariableExpression__Alternatives");
					put(grammarAccess.getRelationExpressionAccess().getOperatorAlternatives_1_1_0(), "rule__RelationExpression__OperatorAlternatives_1_1_0");
					put(grammarAccess.getTimedExpressionAccess().getOperatorAlternatives_1_0(), "rule__TimedExpression__OperatorAlternatives_1_0");
					put(grammarAccess.getAdditionExpressionAccess().getOperatorAlternatives_1_1_0(), "rule__AdditionExpression__OperatorAlternatives_1_1_0");
					put(grammarAccess.getMultiplicationExpressionAccess().getOperatorAlternatives_1_1_0(), "rule__MultiplicationExpression__OperatorAlternatives_1_1_0");
					put(grammarAccess.getNegatedExpressionAccess().getAlternatives(), "rule__NegatedExpression__Alternatives");
					put(grammarAccess.getNegatedExpressionAccess().getOperatorAlternatives_0_1_0(), "rule__NegatedExpression__OperatorAlternatives_0_1_0");
					put(grammarAccess.getBasicExpressionAccess().getAlternatives(), "rule__BasicExpression__Alternatives");
					put(grammarAccess.getValueAccess().getAlternatives(), "rule__Value__Alternatives");
					put(grammarAccess.getIntegerValueAccess().getValueAlternatives_0(), "rule__IntegerValue__ValueAlternatives_0");
					put(grammarAccess.getFeatureAccessAccess().getAlternatives_2(), "rule__FeatureAccess__Alternatives_2");
					put(grammarAccess.getCollectionOperationAccess().getAlternatives(), "rule__CollectionOperation__Alternatives");
					put(grammarAccess.getCollectionModificationAccess().getAlternatives(), "rule__CollectionModification__Alternatives");
					put(grammarAccess.getDocumentAccess().getGroup(), "rule__Document__Group__0");
					put(grammarAccess.getDocumentAccess().getGroup_1(), "rule__Document__Group_1__0");
					put(grammarAccess.getImportAccess().getGroup(), "rule__Import__Group__0");
					put(grammarAccess.getExpressionRegionAccess().getGroup(), "rule__ExpressionRegion__Group__0");
					put(grammarAccess.getExpressionRegionAccess().getGroup_2(), "rule__ExpressionRegion__Group_2__0");
					put(grammarAccess.getVariableDeclarationAccess().getGroup(), "rule__VariableDeclaration__Group__0");
					put(grammarAccess.getVariableAssignmentAccess().getGroup(), "rule__VariableAssignment__Group__0");
					put(grammarAccess.getTypedVariableDeclarationAccess().getGroup(), "rule__TypedVariableDeclaration__Group__0");
					put(grammarAccess.getTypedVariableDeclarationAccess().getGroup_3(), "rule__TypedVariableDeclaration__Group_3__0");
					put(grammarAccess.getClockDeclarationAccess().getGroup(), "rule__ClockDeclaration__Group__0");
					put(grammarAccess.getClockDeclarationAccess().getGroup_3(), "rule__ClockDeclaration__Group_3__0");
					put(grammarAccess.getClockAccess().getGroup(), "rule__Clock__Group__0");
					put(grammarAccess.getClockAssignmentAccess().getGroup(), "rule__ClockAssignment__Group__0");
					put(grammarAccess.getClockAssignmentAccess().getGroup_2(), "rule__ClockAssignment__Group_2__0");
					put(grammarAccess.getImplicationExpressionAccess().getGroup(), "rule__ImplicationExpression__Group__0");
					put(grammarAccess.getImplicationExpressionAccess().getGroup_1(), "rule__ImplicationExpression__Group_1__0");
					put(grammarAccess.getDisjunctionExpressionAccess().getGroup(), "rule__DisjunctionExpression__Group__0");
					put(grammarAccess.getDisjunctionExpressionAccess().getGroup_1(), "rule__DisjunctionExpression__Group_1__0");
					put(grammarAccess.getConjunctionExpressionAccess().getGroup(), "rule__ConjunctionExpression__Group__0");
					put(grammarAccess.getConjunctionExpressionAccess().getGroup_1(), "rule__ConjunctionExpression__Group_1__0");
					put(grammarAccess.getRelationExpressionAccess().getGroup(), "rule__RelationExpression__Group__0");
					put(grammarAccess.getRelationExpressionAccess().getGroup_1(), "rule__RelationExpression__Group_1__0");
					put(grammarAccess.getTimedExpressionAccess().getGroup(), "rule__TimedExpression__Group__0");
					put(grammarAccess.getAdditionExpressionAccess().getGroup(), "rule__AdditionExpression__Group__0");
					put(grammarAccess.getAdditionExpressionAccess().getGroup_1(), "rule__AdditionExpression__Group_1__0");
					put(grammarAccess.getMultiplicationExpressionAccess().getGroup(), "rule__MultiplicationExpression__Group__0");
					put(grammarAccess.getMultiplicationExpressionAccess().getGroup_1(), "rule__MultiplicationExpression__Group_1__0");
					put(grammarAccess.getNegatedExpressionAccess().getGroup_0(), "rule__NegatedExpression__Group_0__0");
					put(grammarAccess.getBasicExpressionAccess().getGroup_1(), "rule__BasicExpression__Group_1__0");
					put(grammarAccess.getEnumValueAccess().getGroup(), "rule__EnumValue__Group__0");
					put(grammarAccess.getNullValueAccess().getGroup(), "rule__NullValue__Group__0");
					put(grammarAccess.getCollectionAccessAccess().getGroup(), "rule__CollectionAccess__Group__0");
					put(grammarAccess.getFeatureAccessAccess().getGroup(), "rule__FeatureAccess__Group__0");
					put(grammarAccess.getFeatureAccessAccess().getGroup_2_0(), "rule__FeatureAccess__Group_2_0__0");
					put(grammarAccess.getFeatureAccessAccess().getGroup_2_0_1(), "rule__FeatureAccess__Group_2_0_1__0");
					put(grammarAccess.getFeatureAccessAccess().getGroup_2_1(), "rule__FeatureAccess__Group_2_1__0");
					put(grammarAccess.getFeatureAccessAccess().getGroup_2_1_2(), "rule__FeatureAccess__Group_2_1_2__0");
					put(grammarAccess.getFeatureAccessAccess().getGroup_2_1_2_1(), "rule__FeatureAccess__Group_2_1_2_1__0");
					put(grammarAccess.getDocumentAccess().getImportsAssignment_0(), "rule__Document__ImportsAssignment_0");
					put(grammarAccess.getDocumentAccess().getDomainsAssignment_1_1(), "rule__Document__DomainsAssignment_1_1");
					put(grammarAccess.getDocumentAccess().getExpressionsAssignment_2(), "rule__Document__ExpressionsAssignment_2");
					put(grammarAccess.getImportAccess().getImportURIAssignment_1(), "rule__Import__ImportURIAssignment_1");
					put(grammarAccess.getExpressionRegionAccess().getExpressionsAssignment_2_0(), "rule__ExpressionRegion__ExpressionsAssignment_2_0");
					put(grammarAccess.getVariableDeclarationAccess().getNameAssignment_1(), "rule__VariableDeclaration__NameAssignment_1");
					put(grammarAccess.getVariableDeclarationAccess().getExpressionAssignment_3(), "rule__VariableDeclaration__ExpressionAssignment_3");
					put(grammarAccess.getVariableAssignmentAccess().getVariableAssignment_0(), "rule__VariableAssignment__VariableAssignment_0");
					put(grammarAccess.getVariableAssignmentAccess().getExpressionAssignment_2(), "rule__VariableAssignment__ExpressionAssignment_2");
					put(grammarAccess.getTypedVariableDeclarationAccess().getTypeAssignment_1(), "rule__TypedVariableDeclaration__TypeAssignment_1");
					put(grammarAccess.getTypedVariableDeclarationAccess().getNameAssignment_2(), "rule__TypedVariableDeclaration__NameAssignment_2");
					put(grammarAccess.getTypedVariableDeclarationAccess().getExpressionAssignment_3_1(), "rule__TypedVariableDeclaration__ExpressionAssignment_3_1");
					put(grammarAccess.getClockDeclarationAccess().getNameAssignment_2(), "rule__ClockDeclaration__NameAssignment_2");
					put(grammarAccess.getClockDeclarationAccess().getExpressionAssignment_3_1(), "rule__ClockDeclaration__ExpressionAssignment_3_1");
					put(grammarAccess.getClockAccess().getNameAssignment_0(), "rule__Clock__NameAssignment_0");
					put(grammarAccess.getClockAccess().getLeftIncludedAssignment_1(), "rule__Clock__LeftIncludedAssignment_1");
					put(grammarAccess.getClockAccess().getRightIncludedAssignment_2(), "rule__Clock__RightIncludedAssignment_2");
					put(grammarAccess.getClockAccess().getLeftValueAssignment_3(), "rule__Clock__LeftValueAssignment_3");
					put(grammarAccess.getClockAccess().getRightValueAssignment_4(), "rule__Clock__RightValueAssignment_4");
					put(grammarAccess.getClockAssignmentAccess().getVariableAssignment_1(), "rule__ClockAssignment__VariableAssignment_1");
					put(grammarAccess.getClockAssignmentAccess().getExpressionAssignment_2_1(), "rule__ClockAssignment__ExpressionAssignment_2_1");
					put(grammarAccess.getImplicationExpressionAccess().getOperatorAssignment_1_1(), "rule__ImplicationExpression__OperatorAssignment_1_1");
					put(grammarAccess.getImplicationExpressionAccess().getRightAssignment_1_2(), "rule__ImplicationExpression__RightAssignment_1_2");
					put(grammarAccess.getDisjunctionExpressionAccess().getOperatorAssignment_1_1(), "rule__DisjunctionExpression__OperatorAssignment_1_1");
					put(grammarAccess.getDisjunctionExpressionAccess().getRightAssignment_1_2(), "rule__DisjunctionExpression__RightAssignment_1_2");
					put(grammarAccess.getConjunctionExpressionAccess().getOperatorAssignment_1_1(), "rule__ConjunctionExpression__OperatorAssignment_1_1");
					put(grammarAccess.getConjunctionExpressionAccess().getRightAssignment_1_2(), "rule__ConjunctionExpression__RightAssignment_1_2");
					put(grammarAccess.getRelationExpressionAccess().getOperatorAssignment_1_1(), "rule__RelationExpression__OperatorAssignment_1_1");
					put(grammarAccess.getRelationExpressionAccess().getRightAssignment_1_2(), "rule__RelationExpression__RightAssignment_1_2");
					put(grammarAccess.getTimedExpressionAccess().getClockAssignment_0(), "rule__TimedExpression__ClockAssignment_0");
					put(grammarAccess.getTimedExpressionAccess().getOperatorAssignment_1(), "rule__TimedExpression__OperatorAssignment_1");
					put(grammarAccess.getTimedExpressionAccess().getValueAssignment_2(), "rule__TimedExpression__ValueAssignment_2");
					put(grammarAccess.getAdditionExpressionAccess().getOperatorAssignment_1_1(), "rule__AdditionExpression__OperatorAssignment_1_1");
					put(grammarAccess.getAdditionExpressionAccess().getRightAssignment_1_2(), "rule__AdditionExpression__RightAssignment_1_2");
					put(grammarAccess.getMultiplicationExpressionAccess().getOperatorAssignment_1_1(), "rule__MultiplicationExpression__OperatorAssignment_1_1");
					put(grammarAccess.getMultiplicationExpressionAccess().getRightAssignment_1_2(), "rule__MultiplicationExpression__RightAssignment_1_2");
					put(grammarAccess.getNegatedExpressionAccess().getOperatorAssignment_0_1(), "rule__NegatedExpression__OperatorAssignment_0_1");
					put(grammarAccess.getNegatedExpressionAccess().getOperandAssignment_0_2(), "rule__NegatedExpression__OperandAssignment_0_2");
					put(grammarAccess.getIntegerValueAccess().getValueAssignment(), "rule__IntegerValue__ValueAssignment");
					put(grammarAccess.getBooleanValueAccess().getValueAssignment(), "rule__BooleanValue__ValueAssignment");
					put(grammarAccess.getStringValueAccess().getValueAssignment(), "rule__StringValue__ValueAssignment");
					put(grammarAccess.getEnumValueAccess().getTypeAssignment_0(), "rule__EnumValue__TypeAssignment_0");
					put(grammarAccess.getEnumValueAccess().getValueAssignment_2(), "rule__EnumValue__ValueAssignment_2");
					put(grammarAccess.getVariableValueAccess().getValueAssignment(), "rule__VariableValue__ValueAssignment");
					put(grammarAccess.getCollectionAccessAccess().getCollectionOperationAssignment_0(), "rule__CollectionAccess__CollectionOperationAssignment_0");
					put(grammarAccess.getCollectionAccessAccess().getParameterAssignment_2(), "rule__CollectionAccess__ParameterAssignment_2");
					put(grammarAccess.getFeatureAccessAccess().getTargetAssignment_0(), "rule__FeatureAccess__TargetAssignment_0");
					put(grammarAccess.getFeatureAccessAccess().getValueAssignment_2_0_0(), "rule__FeatureAccess__ValueAssignment_2_0_0");
					put(grammarAccess.getFeatureAccessAccess().getCollectionAccessAssignment_2_0_1_1(), "rule__FeatureAccess__CollectionAccessAssignment_2_0_1_1");
					put(grammarAccess.getFeatureAccessAccess().getValueAssignment_2_1_0(), "rule__FeatureAccess__ValueAssignment_2_1_0");
					put(grammarAccess.getFeatureAccessAccess().getParametersAssignment_2_1_2_0(), "rule__FeatureAccess__ParametersAssignment_2_1_2_0");
					put(grammarAccess.getFeatureAccessAccess().getParametersAssignment_2_1_2_1_1(), "rule__FeatureAccess__ParametersAssignment_2_1_2_1_1");
					put(grammarAccess.getStructuralFeatureValueAccess().getValueAssignment(), "rule__StructuralFeatureValue__ValueAssignment");
					put(grammarAccess.getOperationValueAccess().getValueAssignment(), "rule__OperationValue__ValueAssignment");
				}
			};
		}
		return nameMappings.get(element);
	}
	
	@Override
	protected Collection<FollowElement> getFollowElements(AbstractInternalContentAssistParser parser) {
		try {
			org.scenariotools.sml.expressions.ui.contentassist.antlr.internal.InternalScenarioExpressionsParser typedParser = (org.scenariotools.sml.expressions.ui.contentassist.antlr.internal.InternalScenarioExpressionsParser) parser;
			typedParser.entryRuleDocument();
			return typedParser.getFollowElements();
		} catch(RecognitionException ex) {
			throw new RuntimeException(ex);
		}		
	}
	
	@Override
	protected String[] getInitialHiddenTokens() {
		return new String[] { "RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT" };
	}
	
	public ScenarioExpressionsGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}
	
	public void setGrammarAccess(ScenarioExpressionsGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
