/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * ScenarioTools-URL: www.scenariotools.org
 *
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete 
 */
package org.scenariotools.objectsystem.ui.views;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.compare.CompareEditorInput;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.ContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IReusableEditor;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.ViewPart;
import org.scenariotools.objectsystem.ui.jobs.ShowStateObjectSystemJob;
import org.scenariotools.objectsystem.ui.jobs.CompareSourceAndTargetObjectSystemsJob;
import org.scenariotools.objectsystem.ui.preferences.PreferenceConstants;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.synthesis.stategraph.ui.Activator;

/**
 * This sample class demonstrates how to plug-in a new workbench view. The view
 * shows data obtained from the model. The sample creates a dummy model on the
 * fly, but a real implementation would connect to the model available either in
 * this or another plug-in (e.g. the workspace). The view is connected to the
 * model using a content provider.
 * <p>
 * The view uses a label provider to define how model objects should be
 * presented in the view. Each view can present the same model objects using
 * different labels and icons, if needed. Alternatively, a single label provider
 * can be shared between views in order to ensure that objects of the same type
 * are presented in the same way everywhere.
 * <p>
 * 
 * @author Eric Wete
 * @date 10.11.2016
 * @description This is the Objectsystem's view
 * 
 */

public class ObjectSystemView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "org.scenariotools.objectsystem.ui.views.ObjectSystemView";

	public static final String ZOOM_IN_ACTION_TITLE = "Zoom in";
	public static final String ZOOM_OUT_ACTION_TITLE = "Zoom out";
	public static final String FIT_TO_CANVAS_ACTION_TITLE = "Fit to canvas";
	public static final String SHOW_100_PERCENT_ACTION_TITLE = "Show 100 %";
	public static final String EXPORT_TO_PDF_ACTION_TITLE = "Export to PDF";

	private SWTImageCanvas imageCanvas;
	private String objectStringDot;

	private ObjectSystemLayoutManager objectSystemLayoutManager;

	private ActionContributionItem zoomInPullDown;
	private ActionContributionItem zoomOutPullDown;
	private ActionContributionItem fitCanvasPullDown;
	private ActionContributionItem showOriginalPullDown;
	private ActionContributionItem changeLayoutActionPullDown;
	private ActionContributionItem exportToPDFActionPullDown;

	private ActionContributionItem zoomInToolBar;
	private ActionContributionItem zoomOutToolBar;
	private ActionContributionItem fitCanvasToolBar;
	private ActionContributionItem showOriginalToolBar;
	private ActionContributionItem changeLayoutActionToolBar;
	private ActionContributionItem exportToPDFActionToolBar;

	private ISelectionListener selectionListener = new ISelectionListener() {

		@Override
		public void selectionChanged(IWorkbenchPart part, ISelection selection) {
			if (selection == null || selection.isEmpty()) {
				return;
			}
			if (selection instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selection).getFirstElement();
				if (firstElement instanceof SMLRuntimeState) {
					selectionChangedJob((SMLRuntimeState) firstElement);
				}
			}
		}

		private void selectionChangedJob(SMLRuntimeState smlRuntimeState) {
			ShowStateObjectSystemJob jobState = new ShowStateObjectSystemJob(ObjectSystemView.this, smlRuntimeState);
			IJobManager manager = Job.getJobManager();
			manager.cancel(ShowStateObjectSystemJob.RUNTIME_STATE_JOB_NAME);
			jobState.schedule();
		}

		private void selectionChangedJob(SMLRuntimeState src, SMLRuntimeState tar) {
			CompareSourceAndTargetObjectSystemsJob jobTransition = new CompareSourceAndTargetObjectSystemsJob(
					ObjectSystemView.this.getSite().getPage(), src, tar);
			IJobManager manager = Job.getJobManager();
			manager.cancel(CompareSourceAndTargetObjectSystemsJob.TRANSITION_JOB_NAME);
			jobTransition.schedule();
		}
	};

	/**
	 * Refresh the Objectsystem's View accordingly a DOT code
	 * 
	 * @param objectStringDot
	 *            The DOT code to parse and display
	 * @see org.scenariotools.objectsystem.ui.views.ObjectSystemView#refreshView()
	 */
	public void refreshView(String objectStringDot) {
		this.objectStringDot = objectStringDot;
		refreshView();
	}

	/**
	 * Refresh the Objectsystem's View accordingly the current DOT code
	 * 
	 * @see org.scenariotools.objectsystem.ui.views.ObjectSystemView#refreshView(String
	 *      objectStringDot)
	 */
	public void refreshView() {
		if (objectStringDot == null || objectStringDot.trim().isEmpty()) {
			return;
		}
		// DotImport dotImport = new DotImport();
		// Graph digraph = dotImport.importDot(objectStringDot);
		// set graph type
		// DotAttributes._setType(digraph, DotAttributes._TYPE__G__DIGRAPH);
		// specify layout algorithm
		// DotAttributes.setLayout(digraph, Layout.DOT.toString());
		// DotAttributes.setLayout(digraph,
		// objectSystemLayoutManager.getCurrentLayout().toString());
		// export the dot string and call the dot executable to add layout
		// info to it.
		// DotExport dotExport = new DotExport();
		// String dot = dotExport.exportDot(digraph);
		try {
			String dotExecutablePath = getGraphvizPath();
			if (dotExecutablePath == null) {
				displayNoGraphvizError();
				return;
			}

			ProcessBuilder pb = new ProcessBuilder(dotExecutablePath + "/dot.exe", "-y", "-Tbmp",
					"-K" + objectSystemLayoutManager.getCurrentLayout().toString());
			Process p = pb.start();
			p.getOutputStream().write(objectStringDot.getBytes());
			p.getOutputStream().flush();
			ImageData data = new ImageData(p.getInputStream());
			p.getOutputStream().close();

			if (p.waitFor() == 0) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						ObjectSystemView.this.imageCanvas.loadImage(data);
					}
				});
			} else {
				IStatus status = new Status(IStatus.ERROR, Activator.getDefault().getBundle().getSymbolicName(),
						IStatus.ERROR, "Dot execution failed.", new Exception("Dot execution failed."));
				Activator.getDefault().getLog().log(status);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void displayNoGraphvizError() {
		Display display = Display.getDefault();
		display.syncExec(new Runnable() {
			@Override
			public void run() {
				Shell shell = new Shell(display);
				MessageDialog.openInformation(shell, "No dot.exe file", "The file dot.exe was not founded.");
			}
		});		
	}
	
	private void exportToPDF() {
		if (objectStringDot == null || objectStringDot.trim().isEmpty()) {
			return;
		}

		String dotExecutablePath = getGraphvizPath();
		if (dotExecutablePath == null) {
 			displayNoGraphvizError();
			return;
		}
			
		Display display = Display.getDefault();
		display.syncExec(new Runnable() {
			@Override
			public void run() {
				FileDialog dialog = new FileDialog(new Shell(display), SWT.SAVE);
				dialog.setFileName("objectsystem.pdf");
				dialog.setFilterExtensions(new String[] {"*.pdf"});
				dialog.setFilterNames(new String[] {"PDF (*.pdf)"});
				dialog.setOverwrite(true); // prompt for overwrite if file exists
				String path = dialog.open();
				if(path != null) {
					try {
						ProcessBuilder pb = new ProcessBuilder(dotExecutablePath + "/dot.exe", "-y", "-Tpdf",
							"-K" + objectSystemLayoutManager.getCurrentLayout().toString(), "-o" + path);
						Process p = pb.start();
						p.getOutputStream().write(objectStringDot.getBytes());
						p.getOutputStream().flush();
						p.getOutputStream().close();

						if (p.waitFor() != 0) {
							IStatus status = new Status(IStatus.ERROR, Activator.getDefault().getBundle().getSymbolicName(),
									IStatus.ERROR, "Dot execution failed.", new Exception("Dot execution failed."));
							Activator.getDefault().getLog().log(status);
						}
					} catch(Exception e) {
						e.printStackTrace();
					}
				}					
			}
		});						
	}

	public ObjectSystemView() {
	}

	/**
	 * Get the Graphivz's installation path i.e. The dot.exe folder location
	 * 
	 * @return The Graphivz's installation path
	 */
	private String getGraphvizPath() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		String dotFolderPath = store.getString(PreferenceConstants.P_PATH).trim();
		if (dotFolderPath != null && !dotFolderPath.isEmpty()) {
			File folder = new File(dotFolderPath);
			if (folder.exists() && folder.isDirectory()) {
				File dotFile = new File(folder, "dot.exe");
				if (dotFile != null && dotFile.exists() && dotFile.isFile()) {
					return folder.getAbsolutePath();
				}
			}
		}
		// open file chooser to choose dot.exe file
		Display display = Display.getDefault();
		display.syncExec(new Runnable() {
			DirectoryDialog dirDialog;

			@Override
			public void run() {
				Shell shell = new Shell(display);
				dirDialog = new DirectoryDialog(shell, SWT.OPEN);
				dirDialog.setMessage(
						"Please select the location of Graphivz installation folder containing the dot.exe file (usually the 'bin' subfolder)");
				String fileFilterPath = new File(".").getParent();
				dirDialog.setFilterPath(fileFilterPath);
				chooseGraphivzDirectory(shell);
			}

			private void chooseGraphivzDirectory(Shell shell) {
				String selectedDir = dirDialog.open();
				if (selectedDir != null) {
					File file = new File(selectedDir, "dot.exe");
					if (file != null && file.exists() && file.isFile()) {
						store.putValue(PreferenceConstants.P_PATH, selectedDir);
					}
				}
			}
		});
		dotFolderPath = store.getString(PreferenceConstants.P_PATH).trim();
		if (dotFolderPath != null && !dotFolderPath.isEmpty()) {
			return dotFolderPath;
		}
		return null;
	}

	public void createPartControl(Composite parent) {
		this.imageCanvas = new SWTImageCanvas(parent);
		this.objectSystemLayoutManager = ObjectSystemLayoutManager.getInstance(this);

		makeActions();

		hookSelectionListener();
	}

	/**
	 * Initializes the View's actions
	 */
	private void makeActions() {
		Action zoomInAction = new Action(ZOOM_IN_ACTION_TITLE, Action.AS_PUSH_BUTTON) {
			public void run() {
				imageCanvas.zoomIn();
			}
		};
		zoomInAction.setToolTipText(ZOOM_IN_ACTION_TITLE);
		zoomInAction.setImageDescriptor(Activator.getImageDescriptor(Activator.IMAGES_FOLDER + "ZoomIn16.gif"));

		Action zoomOutAction = new Action(ZOOM_OUT_ACTION_TITLE, Action.AS_PUSH_BUTTON) {
			public void run() {
				imageCanvas.zoomOut();
			}
		};
		zoomOutAction.setToolTipText(ZOOM_OUT_ACTION_TITLE);
		zoomOutAction.setImageDescriptor(Activator.getImageDescriptor(Activator.IMAGES_FOLDER + "ZoomOut16.gif"));

		Action fitCanvasAction = new Action(FIT_TO_CANVAS_ACTION_TITLE, Action.AS_PUSH_BUTTON) {
			public void run() {
				imageCanvas.fitCanvas();
			}
		};
		fitCanvasAction.setToolTipText(FIT_TO_CANVAS_ACTION_TITLE);
		fitCanvasAction.setImageDescriptor(Activator.getImageDescriptor(Activator.IMAGES_FOLDER + "Fit16.gif"));

		Action showOriginalAction = new Action(SHOW_100_PERCENT_ACTION_TITLE, Action.AS_PUSH_BUTTON) {
			public void run() {
				imageCanvas.showOriginal();
			}
		};
		showOriginalAction.setToolTipText(SHOW_100_PERCENT_ACTION_TITLE);
		showOriginalAction.setImageDescriptor(Activator.getImageDescriptor(Activator.IMAGES_FOLDER + "Original16.gif"));

		Action changeLayoutAction = objectSystemLayoutManager.getChangeLayoutAction();
		
		Action exportToPDFAction = new Action(EXPORT_TO_PDF_ACTION_TITLE, Action.AS_PUSH_BUTTON) {
			public void run() {
				exportToPDF();
			}			
		};
		exportToPDFAction.setToolTipText(EXPORT_TO_PDF_ACTION_TITLE);
		exportToPDFAction.setImageDescriptor(Activator.getImageDescriptor(Activator.IMAGES_FOLDER + "show-system-object.png"));

		zoomInPullDown = new ActionContributionItem(zoomInAction);
		zoomOutPullDown = new ActionContributionItem(zoomOutAction);
		fitCanvasPullDown = new ActionContributionItem(fitCanvasAction);
		showOriginalPullDown = new ActionContributionItem(showOriginalAction);
		changeLayoutActionPullDown = new ActionContributionItem(changeLayoutAction);
		exportToPDFActionPullDown = new ActionContributionItem(exportToPDFAction);

		zoomInToolBar = new ActionContributionItem(zoomInAction);
		zoomOutToolBar = new ActionContributionItem(zoomOutAction);
		fitCanvasToolBar = new ActionContributionItem(fitCanvasAction);
		showOriginalToolBar = new ActionContributionItem(showOriginalAction);
		changeLayoutActionToolBar = new ActionContributionItem(changeLayoutAction);
		exportToPDFActionToolBar = new ActionContributionItem(exportToPDFAction);

		contributeActionBarsItemsForView();

	}

	/**
	 * Adds buttons to the Actions' bar
	 */
	private void contributeActionBarsItemsForView() {
		IActionBars bars = getViewSite().getActionBars();
		IMenuManager menuManager = bars.getMenuManager();
		IToolBarManager toolBarManager = bars.getToolBarManager();

		List<ContributionItem> activePullDownActionsFromView = getPullDownActions();
		List<ContributionItem> activeToolBarActionsFromView = getToolBarActions();

		for (ContributionItem item : activePullDownActionsFromView) {
			menuManager.add(item);
		}
		for (ContributionItem item : activeToolBarActionsFromView) {
			toolBarManager.add(item);
		}
		bars.updateActionBars();
	}

	/**
	 * Get buttons' list of Actions' bar
	 * 
	 * @return The buttons' list of Actions' bar
	 */
	protected List<ContributionItem> getPullDownActions() {
		List<ContributionItem> result = new ArrayList<ContributionItem>();
		// result.add(fitCanvasPullDown);
		result.add(zoomInPullDown);
		result.add(zoomOutPullDown);
		result.add(showOriginalPullDown);
		result.add(new Separator());
		result.add(changeLayoutActionPullDown);
		result.add(new Separator());
		result.add(exportToPDFActionPullDown);
		return result;
	}

	/**
	 * Get buttons' list of Toolbar
	 * 
	 * @return The buttons' list of Toolbar
	 */
	protected List<ContributionItem> getToolBarActions() {
		List<ContributionItem> result = new ArrayList<ContributionItem>();
		// result.add(fitCanvasToolBar);
		result.add(zoomInToolBar);
		result.add(zoomOutToolBar);
		result.add(showOriginalToolBar);
		result.add(new Separator());
		result.add(changeLayoutActionToolBar);
		result.add(new Separator());
		result.add(exportToPDFActionToolBar);
		return result;
	}

	public void setFocus() {
		imageCanvas.setFocus();
	}

	public void dispose() {
		imageCanvas.dispose();
		disposeSelectionListener();
		cancelJobs();
		super.dispose();
	}

	public static void cancelJobs() {
		IJobManager manager = Job.getJobManager();
		manager.cancel(CompareSourceAndTargetObjectSystemsJob.TRANSITION_JOB_NAME);
		manager.cancel(ShowStateObjectSystemJob.RUNTIME_STATE_JOB_NAME);
	}

	/**
	 * Adds selection listener
	 */
	protected void hookSelectionListener() {
		getSite().getWorkbenchWindow().getSelectionService().addSelectionListener(selectionListener);
	}

	/**
	 * Removes selection listener
	 */
	private void disposeSelectionListener() {
		getSite().getWorkbenchWindow().getSelectionService().removeSelectionListener(selectionListener);
	}
}
