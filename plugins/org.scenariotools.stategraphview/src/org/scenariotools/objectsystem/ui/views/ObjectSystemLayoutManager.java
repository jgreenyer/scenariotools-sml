/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * ScenarioTools-URL: www.scenariotools.org
 *
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete 
 */
package org.scenariotools.objectsystem.ui.views;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.dot.internal.language.layout.Layout;
import org.eclipse.jface.action.Action;
import org.scenariotools.sml.synthesis.stategraph.ui.Activator;

/**
 * @author Eric Wete
 * @date 16.11.2016
 * @description This is the layout manager for the Objectsystem's view
 */
public class ObjectSystemLayoutManager {

	private Layout currentLayout;
	private Action changeLayoutAction;
	private List<Layout> layoutsList;
	private static ObjectSystemLayoutManager instance;
	private ObjectSystemView objectSystemView;
	public static final String CHANGE_LAYOUT_ACTION_TITLE = "Change Layout";

	private ObjectSystemLayoutManager(ObjectSystemView objectSystemView) {
		this.objectSystemView = objectSystemView;

		layoutsList = new ArrayList();
		layoutsList.add(Layout.DOT);
		layoutsList.add(Layout.CIRCO);
		layoutsList.add(Layout.FDP);
		layoutsList.add(Layout.NEATO);
		layoutsList.add(Layout.OSAGE);
		layoutsList.add(Layout.SFDP);
		layoutsList.add(Layout.TWOPI);

		currentLayout = Layout.DOT;

		changeLayoutAction = new Action(CHANGE_LAYOUT_ACTION_TITLE, Action.AS_PUSH_BUTTON) {
			public void run() {
				changeLayout();
			}
		};
		changeLayoutAction.setToolTipText(CHANGE_LAYOUT_ACTION_TITLE + "\nCurrent: " + getLayoutName(currentLayout)
				+ "\nNext: "
				+ getLayoutName(layoutsList.get((layoutsList.indexOf(currentLayout) + 1) % layoutsList.size())));
		changeLayoutAction
				.setImageDescriptor(Activator.getImageDescriptor(Activator.IMAGES_FOLDER + "change-layout.png"));
	}

	/**
	 * Changes the layout of the Objectsystem's view
	 */
	protected void changeLayout() {
		currentLayout = layoutsList.get((layoutsList.indexOf(currentLayout) + 1) % layoutsList.size());
		objectSystemView.refreshView();
		changeLayoutAction.setToolTipText(CHANGE_LAYOUT_ACTION_TITLE + "\nCurrent: " + getLayoutName(currentLayout)
				+ "\nNext: "
				+ getLayoutName(layoutsList.get((layoutsList.indexOf(currentLayout) + 1) % layoutsList.size())));
	}

	/**
	 * Gets the layout's name
	 * 
	 * @param layout
	 *            the layout
	 * @return The layout's name of the passed argument
	 */
	private String getLayoutName(Layout layout) {
		return layout.toString();
	}

	/**
	 * Get the single instance of this class
	 * 
	 * @param objectSystemView
	 *            The Objectsystem's view
	 * @return The layout manager of the passed Objectsystem's view
	 */

	public static ObjectSystemLayoutManager getInstance(ObjectSystemView objectSystemView) {
		if (instance == null) {
			instance = new ObjectSystemLayoutManager(objectSystemView);
		}
		return instance;
	}

	/**
	 * Gets the change layout's action
	 * 
	 * @return The change layout's action
	 */
	public Action getChangeLayoutAction() {
		return changeLayoutAction;
	}

	/**
	 * Get the current layout
	 * 
	 * @return The current layout
	 */
	public Layout getCurrentLayout() {
		return currentLayout;
	}
}
