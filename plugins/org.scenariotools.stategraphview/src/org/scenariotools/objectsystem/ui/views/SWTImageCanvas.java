/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * ScenarioTools-URL: www.scenariotools.org
 *
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete 
 */
package org.scenariotools.objectsystem.ui.views;

import java.awt.geom.AffineTransform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ScrollBar;

/**
 * @author Eric Wete
 * @date 11.11.2016
 * @description This is the canvas class for ObjectSystemView
 * @see org.scenariotools.objectsystem.ui.views.ObjectSystemView
 */
public class SWTImageCanvas extends Canvas {
	final float ZOOMIN_RATE = 1.1F;
	final float ZOOMOUT_RATE = 0.9F;
	private Image sourceImage;
	private Image screenImage;
	private AffineTransform transform = new AffineTransform();
	private Cursor handCursor;

	public SWTImageCanvas(Composite parent) {
		this(parent, 0);
	}

	public SWTImageCanvas(Composite parent, int style) {
		super(parent, style | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.NO_BACKGROUND);

		addControlListener(new ControlAdapter() {
			public void controlResized(ControlEvent event) {
				SWTImageCanvas.this.syncScrollBars();
			}
		});
		addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent event) {
				SWTImageCanvas.this.paint(event.gc);
			}
		});
		addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {
				int dx = 0;
				int dy = 0;
				switch (e.keyCode) {
				case 16777218:
					dy = 10;
					break;
				case 16777217:
					dy = -10;
					break;
				case 16777222:
					dy = 100;
					break;
				case 16777221:
					dy = -100;
					break;
				case 16777219:
					dx = -10;
					break;
				case 16777220:
					dx = 10;
					break;
				}
				if ((dx != 0) || (dy != 0)) {
					SWTImageCanvas.this.pan(new Point(dx, dy));
				}
			}

			public void keyReleased(KeyEvent e) {
			}
		});
		PanHandler panHandler = new PanHandler(null);
		addMouseListener(panHandler);
		addMouseMoveListener(panHandler);
		this.handCursor = new Cursor(parent.getDisplay(), 21);
		setCursor(this.handCursor);
		initScrollBars();
	}

	private Point panPoint = null;
	private Point panDelta = null;

	private class PanHandler implements MouseListener, MouseMoveListener {
		private PanHandler() {
		}

		PanHandler(PanHandler panhandler) {
			this();
		}

		public void mouseDoubleClick(MouseEvent e) {
		}

		public void mouseDown(MouseEvent e) {
			SWTImageCanvas.this.panPoint = new Point(e.x, e.y);
			SWTImageCanvas.this.panDelta = new Point(0, 0);
		}

		public void mouseMove(MouseEvent e) {
			if ((SWTImageCanvas.this.panPoint != null) && (SWTImageCanvas.this.panDelta != null)) {
				SWTImageCanvas.this.panDelta.x += SWTImageCanvas.this.panPoint.x - e.x;
				SWTImageCanvas.this.panDelta.y += SWTImageCanvas.this.panPoint.y - e.y;
				SWTImageCanvas.this.panPoint.x = e.x;
				SWTImageCanvas.this.panPoint.y = e.y;
				SWTImageCanvas.this.pan(SWTImageCanvas.this.panDelta);
			}
		}

		public void mouseUp(MouseEvent e) {
			SWTImageCanvas.this.panPoint = null;
		}
	}

	public void dispose() {
		if ((this.sourceImage != null) && (!this.sourceImage.isDisposed())) {
			this.sourceImage.dispose();
		}
		if ((this.screenImage != null) && (!this.screenImage.isDisposed())) {
			this.screenImage.dispose();
		}
		if ((this.handCursor != null) && (!this.handCursor.isDisposed())) {
			this.handCursor.dispose();
		}
	}

	private void paint(GC gc) {
		Rectangle clientRect = getClientArea();
		if (this.sourceImage != null) {
			Rectangle imageRect = SWTUtil.inverseTransformRect(this.transform, clientRect);
			int gap = 2;
			imageRect.x -= gap;
			imageRect.y -= gap;
			imageRect.width += 2 * gap;
			imageRect.height += 2 * gap;

			Rectangle imageBound = this.sourceImage.getBounds();
			imageRect = imageRect.intersection(imageBound);
			Rectangle destRect = SWTUtil.transformRect(this.transform, imageRect);
			if (this.screenImage != null) {
				this.screenImage.dispose();
			}
			this.screenImage = new Image(getDisplay(), clientRect.width, clientRect.height);
			GC newGC = new GC(this.screenImage);
			newGC.setClipping(clientRect);
			newGC.drawImage(this.sourceImage, imageRect.x, imageRect.y, imageRect.width, imageRect.height, destRect.x,
					destRect.y, destRect.width, destRect.height);
			newGC.dispose();

			gc.drawImage(this.screenImage, 0, 0);
		} else {
			gc.setClipping(clientRect);
			gc.fillRectangle(clientRect);
			initScrollBars();
		}
	}

	private void initScrollBars() {
		ScrollBar horizontal = getHorizontalBar();
		horizontal.setEnabled(false);
		horizontal.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				SWTImageCanvas.this.scrollHorizontally((ScrollBar) event.widget);
			}
		});
		ScrollBar vertical = getVerticalBar();
		vertical.setEnabled(false);
		vertical.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				SWTImageCanvas.this.scrollVertically((ScrollBar) event.widget);
			}
		});
	}

	private void scrollHorizontally(ScrollBar scrollBar) {
		if (this.sourceImage == null) {
			return;
		}
		AffineTransform af = this.transform;
		double tx = af.getTranslateX();
		double select = -scrollBar.getSelection();
		af.preConcatenate(AffineTransform.getTranslateInstance(select - tx, 0.0D));
		this.transform = af;
		syncScrollBars();
	}

	private void scrollVertically(ScrollBar scrollBar) {
		if (this.sourceImage == null) {
			return;
		}
		AffineTransform af = this.transform;
		double ty = af.getTranslateY();
		double select = -scrollBar.getSelection();
		af.preConcatenate(AffineTransform.getTranslateInstance(0.0D, select - ty));
		this.transform = af;
		syncScrollBars();
	}

	public void syncScrollBars() {
		if (this.sourceImage == null) {
			redraw();
			return;
		}
		AffineTransform af = this.transform;
		double sx = af.getScaleX();
		double sy = af.getScaleY();
		double tx = af.getTranslateX();
		double ty = af.getTranslateY();
		if (tx > 0.0D) {
			tx = 0.0D;
		}
		if (ty > 0.0D) {
			ty = 0.0D;
		}
		ScrollBar horizontal = getHorizontalBar();
		horizontal.setIncrement(getClientArea().width / 20);
		horizontal.setPageIncrement(getClientArea().width);
		Rectangle imageBound = this.sourceImage.getBounds();
		int cw = getClientArea().width;
		int ch = getClientArea().height;
		if (imageBound.width * sx > cw) {
			horizontal.setMaximum((int) (imageBound.width * sx));
			horizontal.setEnabled(true);
			if ((int) -tx > horizontal.getMaximum() - cw) {
				tx = -horizontal.getMaximum() + cw;
			}
		} else {
			horizontal.setEnabled(false);
			tx = (cw - imageBound.width * sx) / 2.0D;
		}
		horizontal.setSelection((int) -tx);
		horizontal.setThumb(getClientArea().width);

		ScrollBar vertical = getVerticalBar();
		vertical.setIncrement(getClientArea().height / 20);
		vertical.setPageIncrement(getClientArea().height);
		if (imageBound.height * sy > ch) {
			vertical.setMaximum((int) (imageBound.height * sy));
			vertical.setEnabled(true);
			if ((int) -ty > vertical.getMaximum() - ch) {
				ty = -vertical.getMaximum() + ch;
			}
		} else {
			vertical.setEnabled(false);
			ty = (ch - imageBound.height * sy) / 2.0D;
		}
		vertical.setSelection((int) -ty);
		vertical.setThumb(getClientArea().height);

		af = AffineTransform.getScaleInstance(sx, sy);
		af.preConcatenate(AffineTransform.getTranslateInstance(tx, ty));
		this.transform = af;

		redraw();
	}

	public Image loadImage(ImageData imageData) {
		if ((this.sourceImage != null) && (!this.sourceImage.isDisposed())) {
			this.sourceImage.dispose();
			this.sourceImage = null;
		}
		this.sourceImage = new Image(getDisplay(), imageData);
		showOriginal();
		return this.sourceImage;
	}

	public void setImageData(ImageData data) {
		if (this.sourceImage != null) {
			this.sourceImage.dispose();
		}
		if (data != null) {
			this.sourceImage = new Image(getDisplay(), data);
		}
		syncScrollBars();
	}

	public void fitCanvas() {
		if (this.sourceImage == null) {
			return;
		}
		Rectangle imageBound = this.sourceImage.getBounds();
		Rectangle destRect = getClientArea();
		double sx = destRect.width / imageBound.width;
		double sy = destRect.height / imageBound.height;
		double s = Math.min(sx, sy);
		double dx = 0.5D * destRect.width;
		double dy = 0.5D * destRect.height;
		centerZoom(dx, dy, s, new AffineTransform());
	}

	public void showOriginal() {
		if (this.sourceImage == null) {
			return;
		}
		this.transform = new AffineTransform();
		syncScrollBars();
	}

	public void centerZoom(double dx, double dy, double scale, AffineTransform af) {
		af.preConcatenate(AffineTransform.getTranslateInstance(-dx, -dy));
		af.preConcatenate(AffineTransform.getScaleInstance(scale, scale));
		af.preConcatenate(AffineTransform.getTranslateInstance(dx, dy));
		this.transform = af;
		syncScrollBars();
	}

	public void zoomIn() {
		if (this.sourceImage == null) {
			return;
		}
		Rectangle rect = getClientArea();
		int w = rect.width;
		int h = rect.height;
		double dx = w / 2.0D;
		double dy = h / 2.0D;
		centerZoom(dx, dy, 1.100000023841858D, this.transform);
	}

	public void zoomOut() {
		if (this.sourceImage == null) {
			return;
		}
		Rectangle rect = getClientArea();
		int w = rect.width;
		int h = rect.height;
		double dx = w / 2.0D;
		double dy = h / 2.0D;
		centerZoom(dx, dy, 0.8999999761581421D, this.transform);
	}

	private void pan(Point p) {
		if (this.screenImage == null) {
			return;
		}
		Rectangle bounds = this.screenImage.getBounds();
		ScrollBar hBar = getHorizontalBar();
		ScrollBar vBar = getVerticalBar();
		Rectangle barBounds = bounds;

		int sdx = p.x * barBounds.width / bounds.width;
		int sdy = p.y * barBounds.height / bounds.height;

		p.x -= sdx * bounds.width / barBounds.width;
		p.y -= sdy * bounds.height / barBounds.height;
		if (sdx != 0) {
			hBar.setSelection(hBar.getSelection() + sdx);
			scrollHorizontally(hBar);
		}
		if (sdy != 0) {
			vBar.setSelection(vBar.getSelection() + sdy);
			scrollVertically(vBar);
		}
	}
}
