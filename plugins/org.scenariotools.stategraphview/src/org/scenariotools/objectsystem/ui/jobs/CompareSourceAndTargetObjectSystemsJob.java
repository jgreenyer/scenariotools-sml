/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * ScenarioTools-URL: www.scenariotools.org
 *
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete 
 */
package org.scenariotools.objectsystem.ui.jobs;

import org.eclipse.compare.CompareConfiguration;
import org.eclipse.compare.CompareEditorInput;
import org.eclipse.compare.CompareUI;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.command.ICompareCommandStack;
import org.eclipse.emf.compare.command.impl.CompareCommandStack;
import org.eclipse.emf.compare.domain.ICompareEditingDomain;
import org.eclipse.emf.compare.domain.impl.EMFCompareEditingDomain;
import org.eclipse.emf.compare.ide.ui.internal.configuration.EMFCompareConfiguration;
import org.eclipse.emf.compare.ide.ui.internal.editor.ComparisonEditorInput;
import org.eclipse.emf.compare.scope.DefaultComparisonScope;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceFactoryImpl;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IReusableEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.util.SMLRuntimeStateUtil;

/**
 * @author Eric Wete
 * @date 30.11.2016
 * @description This job will be executed when a Transition is selected
 */
public class CompareSourceAndTargetObjectSystemsJob extends Job {

	public static final String TRANSITION_JOB_NAME = "TransitionJob";

	private IWorkbenchPage page;
	private SMLRuntimeState src;
	private SMLRuntimeState tar;

	/**
	 * @param name
	 *            Job name
	 */
	public CompareSourceAndTargetObjectSystemsJob(String name) {
		super(name);
	}

	/**
	 * @param page
	 *            IWorkbenchPage in which the compare view should be showed
	 * @param src
	 *            SMLRuntimeState source of the Transition which contains the
	 *            Objectsystem
	 * @param tar
	 *            SMLRuntimeState target of the Transition which contains the
	 *            Objectsystem
	 */
	public CompareSourceAndTargetObjectSystemsJob(IWorkbenchPage page, SMLRuntimeState src, SMLRuntimeState tar) {
		super(TRANSITION_JOB_NAME);
		this.page = page;
		this.src = src;
		this.tar = tar;
	}

	@Override
	protected void canceling() {
		if (page != null) {
			for (IEditorReference editorReference : page.getEditorReferences()) {
				try {
					if (editorReference.getEditorInput() instanceof CompareEditorInput) {
						IEditorPart editorPart = editorReference.getEditor(false);
						if (editorPart instanceof IReusableEditor) {
							editorPart.dispose();
							break;
						}
					}
				} catch (PartInitException e) {
					e.printStackTrace();
				}
			}
		}
		super.canceling();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.jobs.Job#run(org.eclipse.core.runtime.
	 * IProgressMonitor)
	 */
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		EObject srcObjectSystem = null;
		EObject tarObjectSystem = null;
		for (EObject rootElt : src.getDynamicObjectContainer().getRootObjects()) {
			srcObjectSystem = rootElt;
		}
		for (EObject rootElt : tar.getDynamicObjectContainer().getRootObjects()) {
			tarObjectSystem = rootElt;
		}
		if (srcObjectSystem == null || tarObjectSystem == null) {
			return Status.OK_STATUS;
		}
		ResourceSet resourceSetSrc = new ResourceSetImpl();
		resourceSetSrc.getResourceFactoryRegistry().getExtensionToFactoryMap().put("*", new XMLResourceFactoryImpl());
		String resNameSrc = "./resourceSetSrc" + System.currentTimeMillis() + ".xmi";
		Resource resourceSrc = resourceSetSrc.createResource(URI.createURI(resNameSrc));
		resourceSrc.getContents().add(EcoreUtil.copy(srcObjectSystem));

		ResourceSet resourceSetTar = new ResourceSetImpl();
		resourceSetTar.getResourceFactoryRegistry().getExtensionToFactoryMap().put("*", new XMLResourceFactoryImpl());
		String resNameTar = "./resourceSetTar" + System.currentTimeMillis() + ".xmi";
		Resource resourceTar = resourceSetTar.createResource(URI.createURI(resNameTar));
		resourceTar.getContents().add(EcoreUtil.copy(tarObjectSystem));

		if (monitor.isCanceled()) {
			return Status.CANCEL_STATUS;
		}
		this.emfCompareAndShow(resourceSetSrc, resourceSetTar);
		return Status.OK_STATUS;
	}

	public boolean belongsTo(Object family) {
		return TRANSITION_JOB_NAME.equals(family);
	}

	/**
	 * Performs EMF Compare operation for two ResourceSet and show the results
	 * 
	 * @param resourceSetSrc
	 *            The ResourceSet containing the Objectsystem of the source's
	 *            SMLRuntimeState
	 * @param resourceSetTar
	 *            The ResourceSet containing the Objectsystem of the target's
	 *            SMLRuntimeState
	 */
	private void emfCompareAndShow(ResourceSet resourceSetSrc, ResourceSet resourceSetTar) {
		IComparisonScope scope = new DefaultComparisonScope(resourceSetSrc, resourceSetTar, resourceSetSrc);
		Comparison comparison = EMFCompare.builder().build().compare(scope);
		ICompareCommandStack commandStack = new CompareCommandStack(new BasicCommandStack());
		ICompareEditingDomain editingDomain = new EMFCompareEditingDomain(resourceSetSrc, resourceSetTar,
				resourceSetSrc, commandStack);
		AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		EMFCompareConfiguration configuration = new EMFCompareConfiguration(new CompareConfiguration());
		configuration.setLeftEditable(false);
		configuration.setRightEditable(false);
		CompareEditorInput input = new ComparisonEditorInput(configuration, comparison, editingDomain, adapterFactory);
		StringBuilder sbTitle = new StringBuilder("Compare State ");
		sbTitle.append(SMLRuntimeStateUtil.getPassedIndex(src));
		sbTitle.append(" and State ");
		sbTitle.append(SMLRuntimeStateUtil.getPassedIndex(tar));
		input.setTitle(sbTitle.toString());
		if (page != null) {
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					try {
						boolean shouldEnd = false;
						for (IEditorReference editorReference : page.getEditorReferences()) {
							try {
								if (editorReference.getEditorInput() instanceof CompareEditorInput) {
									IEditorPart editorPart = editorReference.getEditor(false);
									if (editorPart instanceof IReusableEditor) {
										CompareUI.reuseCompareEditor(input, (IReusableEditor) editorPart);
										editorPart.setFocus();
										shouldEnd = true;
										break;
									}
								}
							} catch (PartInitException e) {
								e.printStackTrace();
							}
						}
						if (!shouldEnd) {
							CompareUI.openCompareEditorOnPage(input, page);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			return;
		}
		IWorkbench wb = PlatformUI.getWorkbench();
		if (wb == null) {
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					CompareUI.openCompareDialog(input);
				}
			});
			return;
		}
		IWorkbenchWindow window = wb.getActiveWorkbenchWindow();
		if (window == null) {
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					CompareUI.openCompareDialog(input);
				}
			});
			return;
		}
		for (IWorkbenchPage pageCurrent : window.getPages()) {
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					CompareUI.openCompareEditorOnPage(input, pageCurrent);
				}
			});
			break;
		}
	}

}
