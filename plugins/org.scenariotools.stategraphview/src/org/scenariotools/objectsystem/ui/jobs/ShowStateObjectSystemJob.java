/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * ScenarioTools-URL: www.scenariotools.org
 *
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete 
 */
package org.scenariotools.objectsystem.ui.jobs;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.emf.objectmodellanguage.eoml.util.OsmlGeneratorUtils;
import org.scenariotools.objectsystem.ui.views.ObjectSystemView;
import org.scenariotools.sml.runtime.SMLRuntimeState;

/**
 * @author Eric Wete
 * @date 30.11.2016
 * @description This job will be executed when a SMLRuntimeState is selected
 */
public class ShowStateObjectSystemJob extends Job {

	public static final String RUNTIME_STATE_JOB_NAME = "RuntimeStateJob";

	private ObjectSystemView objectSystemView;
	private SMLRuntimeState smlRuntimeState;

	/**
	 * @param name
	 *            Job name
	 */
	public ShowStateObjectSystemJob(String name) {
		super(name);
	}

	/**
	 * @param objectSystemView
	 *            LTS View which show Objectsystem graphically
	 * @param smlRuntimeState
	 *            SMLRuntimeState which contains the Objectsystem to show
	 */
	public ShowStateObjectSystemJob(ObjectSystemView objectSystemView, SMLRuntimeState smlRuntimeState) {
		super(RUNTIME_STATE_JOB_NAME);
		this.objectSystemView = objectSystemView;
		this.smlRuntimeState = smlRuntimeState;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.jobs.Job#run(org.eclipse.core.runtime.
	 * IProgressMonitor)
	 */
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		if (smlRuntimeState.getDynamicObjectContainer() == null)
			return Status.OK_STATUS;
		for (EObject rootElt : smlRuntimeState.getDynamicObjectContainer().getRootObjects()) {
			// Joel->Eric: why EcoreUtil.copy the state?
			// String objectStringDot =
			// OsmlGeneratorUtils.generateStringDOTFromRootObject(EcoreUtil.copy(rootElt));
			String objectStringDot = OsmlGeneratorUtils.generateStringDOTFromRootObject(rootElt);
			if (monitor.isCanceled()) {
				return Status.CANCEL_STATUS;
			}
			this.objectSystemView.refreshView(objectStringDot);
		}
		return Status.OK_STATUS;
	}

	public boolean belongsTo(Object family) {
		return RUNTIME_STATE_JOB_NAME.equals(family);
	}

}
