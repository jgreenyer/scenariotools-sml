/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.synthesis.stategraph.ui.viewprovider;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.synthesis.stategraph.ui.views.StateGraphViewPart;
import org.scenariotools.sml.runtime.Transition;

public class SubGraphContentProvider extends FullGraphContentProvider {

	private int numberOfNeighbors;
	
	protected SubGraphContentProvider(StateGraphViewPart stateGraphView) {
		super(stateGraphView);
		this.numberOfNeighbors = 1;
	}
	
	SubGraphContentProvider(StateGraphViewPart stateGraphView, int numAncestors) {
		super(stateGraphView);
		this.numberOfNeighbors = numAncestors;
	}

	@Override
	public Collection<SMLRuntimeState> getNodes() {
		Collection<SMLRuntimeState> smlRuntimeStates = super.getNodes();
		
		//BFS forward and backward
		Collection<SMLRuntimeState> closed = new HashSet<SMLRuntimeState>();
		SMLRuntimeState currentState = getCurrentState();
		
		if(smlRuntimeStates.contains(currentState)){
			if (numberOfNeighbors > 0){
				Queue<SMLRuntimeState> succFw = new LinkedList<SMLRuntimeState>();
				Queue<SMLRuntimeState> openFw = new LinkedList<SMLRuntimeState>();
				Queue<SMLRuntimeState> succBw = new LinkedList<SMLRuntimeState>();
				Queue<SMLRuntimeState> openBw = new LinkedList<SMLRuntimeState>();
				openFw.add(currentState);
				openBw.add(currentState);
				for(int i = 0; i <= numberOfNeighbors; i++){
					//BFS forward
					while(!openFw.isEmpty()){
						closed.add(openFw.peek());
						for(Transition t : openFw.remove().getOutgoingTransition()){
							SMLRuntimeState s = (SMLRuntimeState) t.getTargetState();
							succFw.add(s);
						}
					}
					openFw.addAll(succFw);
					//BFS backward
					while(!openBw.isEmpty()){
						closed.add(openBw.peek());
						for(Transition t : openBw.remove().getIncomingTransition()){
							SMLRuntimeState s = (SMLRuntimeState) t.getSourceState();
							succBw.add(s);
						}
					}
					openBw.addAll(succBw);
				}
			}else if (numberOfNeighbors == 0){
				closed.add(currentState);
				for(Transition t : currentState.getOutgoingTransition()){
					SMLRuntimeState s = (SMLRuntimeState) t.getTargetState();
					closed.add(s);
				}
			}
		}
		return (Collection<SMLRuntimeState>)closed;
	}

}
