/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.synthesis.stategraph.ui.viewprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.synthesis.stategraph.ui.views.StateGraphViewPart;
import org.scenariotools.sml.runtime.Transition;

/**
 * The content provider class is responsible for providing objects to the
 * view. It can wrap existing objects in adapters or simply return objects
 * as-is. These objects may be sensitive to the current input of the view,
 * or ignore it and always show the same content (like Task List, for
 * example).
 */
public class FullGraphContentProvider extends ArrayContentProvider implements ViewContentProvider{

	private final StateGraphViewPart stateGraphView;

	private final SMLRuntimeStateGraph smlRuntimeStateGraph;
	
	protected FullGraphContentProvider(StateGraphViewPart stateGraphView) {
		this.stateGraphView = stateGraphView;
		this.smlRuntimeStateGraph = stateGraphView.getCurrentSMLRuntimeStateGraph();
	}

	@Override
	public Collection<SMLRuntimeState> getNodes() {
		if (stateGraphView.getCurrentSMLRuntimeStateGraph() == null){
			return new ArrayList<SMLRuntimeState>();
		}
		// protect states list from StateGraph against changes
		Collection<SMLRuntimeState> temp = new ArrayList<SMLRuntimeState>();
		Collection<SMLRuntimeState> states =  (Collection) stateGraphView.getCurrentSMLRuntimeStateGraph().getStates();
	
		//TODO if graph is too big, force filtering to a reduced number of states (for performance reasons)
		
//		if (stateFilterPredicate != null){
//			temp = states.stream().filter(stateFilterPredicate).collect(Collectors.toSet());
//		}else{
			temp.addAll(states);
//		}

		return temp;
	}
	
	@Override
	public SMLRuntimeState getCurrentState(){
		if(this.stateGraphView.getCurrentSMLRuntimeStateGraph() == null){
			return null;
		}
		return this.stateGraphView.getCurrentSMLRuntimeState();
	}

	@Override
	public Object[] getRelationships(Object source, Object dest) {
		List<Transition> rels = new ArrayList<Transition>();

//		if (source instanceof SMLRuntimeState && destination instanceof NextSMLRuntimeState) {
//			SMLRuntimeState src = (SMLRuntimeState) source;
//			NextSMLRuntimeState dest = (NextSMLRuntimeState) destination;
//			for (Transition t : dest.getIncomingTransition()) {
//				if (t.getSourceState() != null && t.getSourceState().equals(src)) {
//					rels.add(t);
//				}
//			}
//		} else 
		if (source instanceof SMLRuntimeState && dest instanceof SMLRuntimeState) {
			SMLRuntimeState src = (SMLRuntimeState) source;
			for (Transition t : src.getOutgoingTransition()) {
				if (t.getTargetState() != null && t.getTargetState().equals(dest)) {
					rels.add(t);
				}
			}
			
//			if(findNextEnvironmentStates().contains(destination))
//				rels.add(t);
			
		}
		return rels.toArray();
	}

	@Override
	public SMLRuntimeStateGraph getCurrentSMLRuntimeStateGraph() {
		return smlRuntimeStateGraph;
	}
}