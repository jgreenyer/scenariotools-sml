/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.synthesis.stategraph.ui.views;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.widgets.Display;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.zest.core.viewers.ISelfStyleProvider;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;
import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.WaitEvent;
import org.scenariotools.sml.Alternative;
import org.scenariotools.sml.Interaction;
import org.scenariotools.sml.InteractionFragment;
import org.scenariotools.sml.Loop;
import org.scenariotools.sml.ModalMessage;
import org.scenariotools.sml.Parallel;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.ScenarioKind;
import org.scenariotools.sml.WaitCondition;
import org.scenariotools.sml.runtime.ActiveCase;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.MessageEventBlockedInformation;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.util.SMLRuntimeStateUtil;
import org.scenariotools.sml.runtime.util.TransitionUtil;
import org.scenariotools.sml.synthesis.stategraph.ui.model.TransitionSequence;
import org.scenariotools.sml.runtime.Transition;

public class StateGraphLabelProvider extends LabelProvider implements ISelfStyleProvider{

	private final StateGraphViewPart stateGraphViewPart;
	
	public static final String guaranteeViolationColor = "#ff9999";
	public static final String assumptionViolationColor = "#d2a0ff";
	public static final String requirementViolationColor = "#ff7ca4";
	public static final String deadlockColor = "#ff8888";
	public static final String winColorFill = "#c8ffc8";
	public static final String loseColorFill = "#ffc8c8";
	public static final String winColorBorder = "#00BB00";
	public static final String loseColorBorder = "#BB0000";
	public static final String assumptionViolationBorder = "#d2a0ff";
	public static final String deadlockBorder = "#ff2222";
	public static final String assumptionRequestedEventColor = "#ebd6ff";
	public static final String guaranteeRequestedEventColor = "#d6e3ff";
	public static final String requirementRequestedEventColor = "#fff4d6";
	public static final String systemStepBlockedButAssumptionRequestedEventsColor = "#d996ff";
	public static final String currentStateColor = "#fff600";
	public static final String defaultStateColor = "#f1f4f6";
	

	protected StateGraphLabelProvider(StateGraphViewPart stateGraphViewPart) {
		this.stateGraphViewPart = stateGraphViewPart;
	}

	public boolean showImage = false;
	
	@Override
	public String getText(Object element) {
		if(element instanceof SMLRuntimeState) {
			SMLRuntimeState state = (SMLRuntimeState) element;
			String result = SMLRuntimeStateUtil.getPassedIndex(state);
			if (result == null)
				result = "not explored";
			
			if(stateGraphViewPart.getInformationDisplayLevel() > 0){
				String iterationInWhichStateWasExplored = SMLRuntimeStateUtil.getIterationInWhichStateWasExplored(state);
				if (iterationInWhichStateWasExplored != null) {
					result += "\n" + "explored in iteration: " + iterationInWhichStateWasExplored; 
				}
				
				int distanceFromStart = SMLRuntimeStateUtil.getDistance(state);
				if(distanceFromStart > 0)
					result += "\n" + "distance from start: " + distanceFromStart; 
				int distanceToNearestSafetyViolation = SMLRuntimeStateUtil.getDistanceToNearestSafetyViolation(state);
				if(distanceToNearestSafetyViolation > 0)
					result += "\n" + "nearest safety violation in: " + distanceToNearestSafetyViolation; 
				
				
				if(stateGraphViewPart.getInformationDisplayLevel() == 1) {
					long numberOfAssumptionScenarios = state.getActiveScenarios().stream().filter(a -> a.getScenario().getKind() == ScenarioKind.ASSUMPTION).count();
					long numberOfGuaranteeScenarios = state.getActiveScenarios().stream().filter(a -> a.getScenario().getKind() == ScenarioKind.GUARANTEE).count();
					
					result += "\n" + "active scenarios: A:" + numberOfAssumptionScenarios + " G:" + numberOfGuaranteeScenarios; 
				}				
				
				if (stateGraphViewPart.getInformationDisplayLevel() > 3){
					if (!state.getMessageEventBlockedInformation().isEmpty()){
						result += "\nBlocked message events:";
						for (MessageEventBlockedInformation messageEventBlockedInformation : state.getMessageEventBlockedInformation()) {
							result += "\n     " + messageEventBlockedInformation.getMessageEventString() 
									+ " -- " + messageEventBlockedInformation.getDescription();							
						}
					}
				}
				if(state.isSafetyViolationOccurredInGuarantees()){
					result += "\nViolation in guarantee secenarios:";
					String annotation = state.getStringToStringAnnotationMap().get("violatedGuaranteeScenario");
					if (annotation != null){
						result += ":\n   " + annotation;
					}
				}
				if(state.isSafetyViolationOccurredInAssumptions() ){
					result += "\nViolation in assumption scenarios:";
					String annotation = state.getStringToStringAnnotationMap().get("violatedAssumptionScenario");
					if (annotation != null){
						result += ":\n   " + annotation;
					}
				}
				
				String systemStepBlockedInfo = SMLRuntimeStateUtil.getSystemStepBlockedButAssumptionRequestedEventsString(state);
				if (systemStepBlockedInfo != null){
					result += ":\n" + "System step blocked (safety violation): " + systemStepBlockedInfo + "\nBut assumptions w requested events remaining";					
				}
				
				String deadlockInfo = state.getStringToStringAnnotationMap().get("deadlock");
				if(deadlockInfo != null){
					result += "\nDeadlock state: ";
					result += deadlockInfo;
				}

				if (stateGraphViewPart.getInformationDisplayLevel() > 1){
					result += "\n"+getActiveScenariosInformationString(state);
				}

			}
			
			return result;
		}
		if (element instanceof TransitionSequence) {
			TransitionSequence transitionSequence = (TransitionSequence) element;
			return new ArrayList<Transition>(transitionSequence.getTransitionSequence()).stream().map(t -> t.getEvent().toString()).collect(Collectors.joining(",\n"));
		}
		if (element instanceof Transition) {
			Transition transition = (Transition) element;
			String result = "";
			String passedIndex = TransitionUtil.getPassedIndex(transition); 
			if (passedIndex != null) {
				result += "("+ passedIndex +") ";
			}
			if(transition.getEvent() != null)
				result += transition.getEvent().toString();
			String iterationInWhichTransitionWasExplored = TransitionUtil.getIterationInWhichTransitionWasExplored(transition);
			if (iterationInWhichTransitionWasExplored != null) {
				result += " (iteration: " + iterationInWhichTransitionWasExplored + ")"; 
			}
			if(stateGraphViewPart.getInformationDisplayLevel() > 2){
				String s = TransitionUtil.getAttractorStrategyInformation(transition);
				if (s != null)
					result += "\n ----\n" + s;  
			}

			return result;
		}
		throw new RuntimeException("Wrong type: " + element.getClass().toString());
	}
	
//		@Override
//		public Image getImage(Object element) {
//			if(!showImage) return super.getImage(element);
//			
//			if (element instanceof Transition) {
//				Transition t = (Transition) element;
//				
//				MessageEvent messageEvent = (MessageEvent) t.getEvent();
//				
////				return MSDModalMessageEventsIconProvider
////						.getImageForCondensatedMessageEvent(
////								msdModalMessageEvent, /*Set value dynamically if required*/false);
//			}else{
//				return super.getImage(element);	
//			}
//		}

	@Override
	public void selfStyleConnection(Object element,
			GraphConnection connection) {

		if (element instanceof TransitionSequence) {
			TransitionSequence ts = (TransitionSequence) element;
			connection.setLineStyle(Graphics.LINE_DASH);
			connection.setLineWidth(2);
			connection.setLineColor(new Color(Display.getCurrent(), 50, 50, 50));
			if (isStrategyTransitionSequence(ts)){
				connection.setLineColor(createColorFromHex(Display.getCurrent(), winColorBorder));
			}else if (isCounterStrategyTransitionSequence(ts)){
				connection.setLineWidth(4);
				connection.setLineColor(createColorFromHex(Display.getCurrent(), loseColorBorder));
			}
		}else if (element instanceof Transition) {
			Transition t = (Transition) element;
			try {
				if ((TransitionUtil.isSetControllableTransition(t) && !TransitionUtil.isControllableTransition(t)) 
						|| (t.getEvent() instanceof MessageEvent && ((SMLRuntimeState) t.getSourceState()).getObjectSystem().isEnvironmentMessageEvent((MessageEvent) t.getEvent()))) {
					connection.setLineStyle(Graphics.LINE_DASH);
				} else {
					connection.setLineStyle(Graphics.LINE_SOLID);
				}
			} catch (NullPointerException e) {
				connection.setLineStyle(Graphics.LINE_SOLID);
			}
			// Existential Scenarios?
			if(t.getStringToBooleanAnnotationMap().isEmpty()){
				connection.setLineWidth(2);
				connection.setLineColor(new Color(Display.getCurrent(), 50, 50, 50));
			}else{
				StringBuilder terminatedExistentialScenarios = new StringBuilder();
				terminatedExistentialScenarios.append("Terminated Existential Scenarios:");
				for(Entry<String, Boolean> entry :t.getStringToBooleanAnnotationMap()){
					terminatedExistentialScenarios.append("\n" + entry.getKey());
				}
				connection.setTooltip(new Label(terminatedExistentialScenarios.toString()));
				connection.setLineWidth(2);
				connection.setLineColor(new Color(Display.getCurrent(), 0, 0, 0));
			}
			if (isStrategyTransition(t)){
				connection.setLineColor(createColorFromHex(Display.getCurrent(), winColorBorder));
//				if(SMLRuntimeStateUtil.isSpanningTreeTransition(t))
//					connection.setLineWidth(4);
			}else if (isCounterStrategyTransition(t)){
				connection.setLineColor(createColorFromHex(Display.getCurrent(), loseColorBorder));
				if(SMLRuntimeStateUtil.isTransitionLeadsToGuaranteeSafetyViolation(t))
					connection.setLineWidth(4);
			}
		}
	}
	
	private boolean isStrategyTransitionSequence(TransitionSequence ts){
		for (Transition t : ts.getTransitionSequence()) {
			if (!isStrategyTransition(t)) return false;
		}
		return true;
	}
	private boolean isCounterStrategyTransitionSequence(TransitionSequence ts){
		for (Transition t : ts.getTransitionSequence()) {
			if (!isCounterStrategyTransition(t)
					|| !SMLRuntimeStateUtil.isTransitionLeadsToGuaranteeSafetyViolation(t)
				) return false;
		}
		return true;
	}
	
	private boolean isStrategyTransition(Transition t){
		return TransitionUtil.isStrategyTransition(t); 
//				(SMLRuntimeStateUtil.isStrategyState((RuntimeState) t.getSourceState())	
//				&& SMLRuntimeStateUtil.isStrategyState((RuntimeState) t.getTargetState())
//				);
	}

	private boolean isCounterStrategyTransition(Transition t){
		return TransitionUtil.isCounterStrategyTransition(t);
//				(SMLRuntimeStateUtil.isCounterStrategyState((RuntimeState) t.getSourceState())	
//				&& SMLRuntimeStateUtil.isCounterStrategyState((RuntimeState) t.getTargetState())
//				);
	}

	@Override
	public void selfStyleNode(Object element, GraphNode node) {
		if(element instanceof SMLRuntimeState){
			
			SMLRuntimeState state = (SMLRuntimeState) element;
			if(state.equals(this.stateGraphViewPart.getCurrentSMLRuntimeState())){
				node.setBackgroundColor(createColorFromHex(Display.getCurrent(), currentStateColor));
			}else if (state.isSafetyViolationOccurredInAssumptions()){
				node.setBackgroundColor(createColorFromHex(Display.getCurrent(), assumptionViolationColor)); 					
			}else if (state.isSafetyViolationOccurredInGuarantees()){
				node.setBackgroundColor(createColorFromHex(Display.getCurrent(), guaranteeViolationColor)); 					
			}else if(false){//state.isSafetyViolationOccurredInRequirements()){
				node.setBackgroundColor(createColorFromHex(Display.getCurrent(), requirementViolationColor)); 					
			}else if(state.getStringToStringAnnotationMap().get("deadlock") != null){
				node.setBackgroundColor(createColorFromHex(Display.getCurrent(), deadlockColor)); 					
			}else{
				node.setBackgroundColor(createColorFromHex(Display.getCurrent(), defaultStateColor));
			}
			
			Boolean strategy = state.getStringToBooleanAnnotationMap().get("strategy");
			Boolean counterstrategy = state.getStringToBooleanAnnotationMap().get("counterstrategy");
			
			if(strategy != null	&& strategy.booleanValue()){
				node.setBorderColor(createColorFromHex(Display.getCurrent(), winColorBorder)); 					
				node.setBorderWidth(2);
			}
			if(counterstrategy != null && counterstrategy.booleanValue()){
				node.setBorderColor(createColorFromHex(Display.getCurrent(), loseColorBorder)); 					
				node.setBorderWidth(2);
			}
			
			if (SMLRuntimeStateUtil.isGoal(state))
				node.setBorderWidth(5);
				
			
//			node.setTooltip(new Label(getSMLStateString(state)));
			node.setTooltip(new Label(getSMLStateHistory(state)));

		}
	}
	
	private String getSMLStateHistory(SMLRuntimeState state) {
		String result = "";
		
		Set<SMLRuntimeState> visited = new HashSet<SMLRuntimeState>();
		
		while (state != null){
			
			Transition transitionFromParent = SMLRuntimeStateUtil.getTransitionFromParent(state);
			if (visited.contains(state))
				return "there is a problem with cyclic relationship in spanning tree"; 
			visited.add(state);
			if (transitionFromParent != null){
				result = "   " + (SMLRuntimeStateUtil.getDistance(state)) + " " + transitionFromParent.getLabel() + "\n" + result;
				state = transitionFromParent.getSourceState();
			}else{
				state = null;
			}
		}
		return "Shortest event sequence leading here:\n" + result;
	}

	
	protected String getActiveScenariosInformationString(SMLRuntimeState state) {
		StringBuilder result = new StringBuilder();
		for(ActiveScenario activeScenario : state.getActiveScenarios()){
			result.append("[" + getCutCoordinates(activeScenario.getMainActiveInteraction()) + "] ");
			result.append( getCutLabel(activeScenario.getMainActiveInteraction()) );
			if(activeScenario.getScenario().getKind() == ScenarioKind.ASSUMPTION)
				result.append(" A ");
			else
				result.append(" G ");
			result.append(activeScenario.getScenario().getName());

			// print role bindings
			if (stateGraphViewPart.getInformationDisplayLevel() > 2){
				result.append("[");
				EMap<Role, EObject> roleBindings = activeScenario.getRoleBindings().getRoleBindings();
				for (Entry<Role, EObject> roleBinding : roleBindings) {
					result.append("(" + SMLRuntimeStateUtil.getEObjectName(roleBinding.getValue()) + ":");
					result.append(roleBinding.getKey().getName() + ")");
				}
				result.append("]");
			}

			result.append("\n");
		}
		if(state.getActiveScenarios().isEmpty()){
			result.append("no active scenarios");
		}
		return result.toString();
	}

	protected String getCutLabel(ActivePart part) {
		StringBuilder result = new StringBuilder();
		result.append("<");
		EList<ActivePart> enabledNestedActiveParts = part.getEnabledNestedActiveInteractions();
		for (ActivePart activePart : enabledNestedActiveParts) {
			result.append(getEnabledInteractionFragmentText(activePart));
		}
		result.append(">");
		return result.toString();
	}
	
	/**
	 * Prints the text for the enabled interaction fragments 
	 * @param activePart
	 * @return
	 */
	private String getEnabledInteractionFragmentText(ActivePart activePart){
		try{
			
			InteractionFragment interactionFragment = activePart.getInteractionFragment();
			if(interactionFragment instanceof ModalMessage){
				String text = NodeModelUtils.getNode(interactionFragment).getText().trim();
				text = text.replace("\t", "");
				return text;
			}else if(interactionFragment instanceof WaitCondition){
				String text = NodeModelUtils.getNode(interactionFragment).getText().trim();
				text = text.replace("\t", ""); // remove tabs
				return text;			
			}else if (interactionFragment instanceof Interaction){
				return getEnabledInteractionFragmentText(activePart.getEnabledNestedActiveInteractions().get(0));			
			}else if (interactionFragment instanceof Loop){
				return "while / " + getEnabledInteractionFragmentText(activePart.getEnabledNestedActiveInteractions().get(0));			
			}else if (interactionFragment instanceof Parallel){
				String text = "parallel / <";
				for (int i = 0; i < activePart.getEnabledNestedActiveInteractions().size(); i++) {
					ActivePart nestedActivePart = activePart.getEnabledNestedActiveInteractions().get(i);
					text = text + "\n  (par" + (i+1) +  ") " + getEnabledInteractionFragmentText(nestedActivePart);
					
				}
				return text + ">";		
			}else if (interactionFragment instanceof Alternative){
				return "alternative / " + getEnabledInteractionFragmentText(activePart.getEnabledNestedActiveInteractions().get(0));			
			}
			return "implement getInteractionFragmentText for type " + interactionFragment.eClass().getName();
		}catch(NullPointerException e){
			return "Details currently not available.";
		}
	}

	
	protected String getCutCoordinates(ActivePart part) {
		String coordinateInParent = "";
		
		if (part.getInteractionFragment() != null)
			coordinateInParent = getCoordinateInInteraction(part.getInteractionFragment());
		
		if ("".equals(coordinateInParent))
			return getEnabledNestedCoordinates(part); // "" implies they are nested.
		else {
			if (!getEnabledNestedCoordinates(part).isEmpty()) 
				return coordinateInParent + ":" + getEnabledNestedCoordinates(part);
			else
				return coordinateInParent;
		}
	}

	protected String getEnabledNestedCoordinates(ActivePart part) {
		if (part.getEnabledNestedActiveInteractions().size() == 1)
			return getCutCoordinates(part.getEnabledNestedActiveInteractions().get(0));
		else {
			return part.getEnabledNestedActiveInteractions().stream().map(activePart -> getCutCoordinates(activePart)).collect(Collectors.joining(", "));
		}
	}

	protected String getCutCoordinates(ActiveCase activeCase) {
		return getCutCoordinates(activeCase.getEnabledNestedActiveInteractions().get(0));
	}
	
	/**
	 * Returns the index of the fragment in the interaction containing this fragment. If
	 * the fragment is not contained by an interaction, return the empty string.
	 */
	protected String getCoordinateInInteraction(InteractionFragment f) {
		if (f.eContainer() instanceof Interaction)
			return Integer.toString(((Interaction)f.eContainer()).getFragments().indexOf(f));
		else
			return "";
	}
	
	public static Color createColorFromHex(Device device, String colorStr) {
	    return new Color(
	    		device,
	            Integer.valueOf( colorStr.substring( 1, 3 ), 16 ),
	            Integer.valueOf( colorStr.substring( 3, 5 ), 16 ),
	            Integer.valueOf( colorStr.substring( 5, 7 ), 16 ) );
	}



}