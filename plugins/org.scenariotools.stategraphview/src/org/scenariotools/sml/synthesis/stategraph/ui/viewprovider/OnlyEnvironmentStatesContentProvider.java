/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.synthesis.stategraph.ui.viewprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.scenariotools.sml.runtime.MessageEvent;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.synthesis.stategraph.ui.model.TransitionSequence;
import org.scenariotools.sml.synthesis.stategraph.ui.views.StateGraphViewPart;
import org.scenariotools.sml.runtime.Transition;

public class OnlyEnvironmentStatesContentProvider extends FullGraphContentProvider {

	private Map<SMLRuntimeState, List<TransitionSequence>> stateToOutgoingTransitionSequenceMap;
	private Map<SMLRuntimeState, List<TransitionSequence>> stateToIncomingTransitionSequenceMap;
	
	private Collection<SMLRuntimeState> environmentOrDeadllockStates;
	
	protected OnlyEnvironmentStatesContentProvider(StateGraphViewPart stateGraphView) {
		super(stateGraphView);
		stateToOutgoingTransitionSequenceMap = new HashMap<SMLRuntimeState, List<TransitionSequence>>(); 
	}

	@Override
	public Collection<SMLRuntimeState> getNodes() {
		Collection<SMLRuntimeState> smlRuntimeStates = super.getNodes();

		environmentOrDeadllockStates = smlRuntimeStates.stream().filter(s -> isEnvironmentOrDeadlockState(s)).collect(Collectors.toSet());
		
		stateToOutgoingTransitionSequenceMap = new HashMap<SMLRuntimeState, List<TransitionSequence>>(); 
		stateToIncomingTransitionSequenceMap = new HashMap<SMLRuntimeState, List<TransitionSequence>>(); 

		// initializes stateToOutgoingTransitionSequenceMap
		for (SMLRuntimeState source : environmentOrDeadllockStates) {
			TransitionSequence transitionSequence = new TransitionSequence();
			transitionSequence.setSource(source);
			visitedStates = new HashSet<SMLRuntimeState>();
			buildSuperStepTransitionSequenceRelationships(source, transitionSequence);
		}

		return environmentOrDeadllockStates;
		
	}
	
	Set<SMLRuntimeState> visitedStates;

	private void buildSuperStepTransitionSequenceRelationships(SMLRuntimeState state, TransitionSequence transitionSequenceToBuild) {
		
		visitedStates.add(state);

//		System.out.println("state has so many outgoing transitions: " + state.getOutgoingTransition().size());

		for (Transition t : state.getOutgoingTransition()) {
			TransitionSequence extendedTransitionSequence = new TransitionSequence(transitionSequenceToBuild, t);
			SMLRuntimeState targetState = (SMLRuntimeState) t.getTargetState();
			if(environmentOrDeadllockStates.contains(targetState)){
				if (extendedTransitionSequence.getTransitionSequence().size() == 1) {
					continue; // this means that there is a direct transition, so no transition sequence must be stored.
				}else{
					extendedTransitionSequence.setTarget(targetState);
					putToStateToTransitionSequenceMap(extendedTransitionSequence.getSource(), extendedTransitionSequence, stateToOutgoingTransitionSequenceMap);
					putToStateToTransitionSequenceMap(targetState, extendedTransitionSequence, stateToIncomingTransitionSequenceMap);
				}
			}else{
				if (!visitedStates.contains(targetState))
					buildSuperStepTransitionSequenceRelationships(
						targetState, 
						extendedTransitionSequence);
			}
		}
		
	}



	@Override
	public Object[] getRelationships(Object source, Object dest) {
		
		List<Object> rels = new ArrayList<Object>();
		
		if (source instanceof SMLRuntimeState && dest instanceof SMLRuntimeState) {
			SMLRuntimeState src = (SMLRuntimeState) source;
			
			List<TransitionSequence> tsList = stateToOutgoingTransitionSequenceMap.get(src);
			if(tsList != null){
				for (TransitionSequence ts : tsList) {
					if (ts.getTarget().equals(dest)){
						rels.add(ts);
						break; // only show one transition sequence to reduce visual complexity.
					}
				}
			}
			for (Transition t : src.getOutgoingTransition()) {
				if (t.getTargetState() != null &&
						t.getTargetState().equals(dest)) 
					rels.add(t);
			}
		}
		return rels.toArray();
	}


	
	
	final protected Map<SMLRuntimeState, List<TransitionSequence>> getStateToOutgoingTransitionSequenceMap(){
		return stateToOutgoingTransitionSequenceMap;
	}
	final protected Map<SMLRuntimeState, List<TransitionSequence>> getStateToIncomingTransitionSequenceMap(){
		return stateToIncomingTransitionSequenceMap;
	}

	protected boolean isEnvironmentOrDeadlockState(SMLRuntimeState runtimeState){
		return hasEnvironmentMessageEvents(runtimeState) 
				|| runtimeState.getOutgoingTransition().isEmpty();
	}
	
	protected boolean hasEnvironmentMessageEvents(SMLRuntimeState runtimeState) {
		for (Transition transition : runtimeState.getOutgoingTransition()) {
			if (isEnvironmentTransition(transition)){
				return true;
			}			
		}
		return false;
	}
	
	protected boolean isEnvironmentTransition(Transition transition){
		return ((SMLRuntimeState)transition.getSourceState()).getObjectSystem().isEnvironmentMessageEvent((MessageEvent) transition.getEvent());
	}

	
	
	protected void putToStateToTransitionSequenceMap(SMLRuntimeState state, TransitionSequence ts, Map<SMLRuntimeState, List<TransitionSequence>> stateToTransitionSequenceMap){
		List<TransitionSequence> tsList = stateToTransitionSequenceMap.get(state);
		if(tsList == null){
			tsList= new ArrayList<TransitionSequence>();
			stateToTransitionSequenceMap.put(state, tsList);
		}
		tsList.add(ts);

	}
	
}
