/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.synthesis.stategraph.ui.model;

import java.util.ArrayList;
import java.util.List;

import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.Transition;

public class TransitionSequence {

	private List<Transition> transitionSequence;
	
	public TransitionSequence(){
		transitionSequence = new ArrayList<Transition>();
	}
	
	public TransitionSequence(TransitionSequence transitionSequence){
		this.transitionSequence = new ArrayList<Transition>(transitionSequence.getTransitionSequence());
		if (transitionSequence.getSource() != null)
			setSource(transitionSequence.getSource());
		if (transitionSequence.getTarget() != null)
			setSource(transitionSequence.getTarget());
	}

	public TransitionSequence(TransitionSequence transitionSequence, Transition transition){
		this(transitionSequence);
		getTransitionSequence().add(transition);
	}

	
	public List<Transition> getTransitionSequence() {
		return transitionSequence;
	}
	public void setTransitionSequence(List<Transition> transitionSequence) {
		this.transitionSequence = transitionSequence;
	}
	public SMLRuntimeState getSource() {
		return source;
	}
	public void setSource(SMLRuntimeState source) {
		this.source = source;
	}
	public SMLRuntimeState getTarget() {
		return target;
	}
	public void setTarget(SMLRuntimeState target) {
		this.target = target;
	}
	private SMLRuntimeState source;
	private SMLRuntimeState target;
	
}
