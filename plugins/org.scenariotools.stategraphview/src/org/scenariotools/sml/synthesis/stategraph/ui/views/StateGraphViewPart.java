/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 *     Eric Wete
 */
package org.scenariotools.sml.synthesis.stategraph.ui.views;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.ContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.zest.core.viewers.AbstractZoomableViewer;
import org.eclipse.zest.core.viewers.GraphViewer;
import org.eclipse.zest.core.viewers.IZoomableWorkbenchPart;
import org.eclipse.zest.core.viewers.ZoomContributionViewItem;
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;
import org.eclipse.zest.core.widgets.ZestStyles;
import org.scenariotools.emf.objectmodellanguage.eoml.util.OsmlGeneratorUtils;
import org.scenariotools.objectsystem.ui.jobs.CompareSourceAndTargetObjectSystemsJob;
import org.scenariotools.objectsystem.ui.views.ObjectSystemView;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.util.SMLRuntimeStateUtil;
import org.scenariotools.sml.synthesis.stategraph.ui.Activator;
import org.scenariotools.sml.synthesis.stategraph.ui.model.TransitionSequence;
import org.scenariotools.sml.runtime.Transition;

public class StateGraphViewPart extends ViewPart implements IZoomableWorkbenchPart {

	public static String SIMULATION_GRAPH = "org.scenariotools.sml.debug.ui.historyviews.SimulationGraphView";

	public static final String SHOW_SYSTEM_OBJECT_POPUP_ACTION_TITLE = "Export selected state's objectsystem to text file";
	public static final String SHOW_SYSTEM_OBJECT_ACTION_TITLE = SHOW_SYSTEM_OBJECT_POPUP_ACTION_TITLE + ".";
	public static final String SHOW_COMPARE_POPUP_ACTION_TITLE = "Show Compare Diff view of the states";
	
	private static final String OBJECTSYSTEM_OUPUT_FOLDER_NAME = "out-objectsystem";
	private static final String OBJECTSYSTEM_FILE_EXTENSION = ".eoml";

	private GraphViewer viewer;

	private Action jumpToNextSafetyViolatingStateAction;

	private int informationDisplayLevel;

	// ListenerList listeners = new ListenerList();

	protected IFile lastFileSelected;

	private ISelectionListener selectionListener = new ISelectionListener() {

		@Override
		public void selectionChanged(IWorkbenchPart part, ISelection selection) {
			if (selection instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selection).getFirstElement();
				if (firstElement instanceof IFile) {
					try {
						final IFile file = (IFile) firstElement;
						if (file == lastFileSelected)
							return;
						else
							lastFileSelected = file;

						final ResourceSet resourceSet = new ResourceSetImpl();
						resourceSet.getURIConverter().getURIMap().putAll(EcorePlugin.computePlatformURIMap(true));
						final Resource stategraphResource = resourceSet
								.getResource(URI.createPlatformResourceURI(file.getFullPath().toString(), true), true);
						final SMLRuntimeStateGraph smlRuntimeStateGraph = (SMLRuntimeStateGraph) stategraphResource
								.getContents().get(0);

						selectionChanged(smlRuntimeStateGraph);
					} catch (Exception e) {
						selectionChanged(RuntimeFactory.eINSTANCE.createSMLRuntimeStateGraph());
					}
				}
				if (firstElement instanceof SMLRuntimeStateGraph) {
					selectionChanged((SMLRuntimeStateGraph) firstElement);
				}
			}
		}

		private void selectionChanged(SMLRuntimeStateGraph smlRuntimeStateGraph) {
			setCurrentSMLRuntimeStateGraph(smlRuntimeStateGraph);
			if (smlRuntimeStateGraph.getStartState() != null)
				setCurrentSMLRuntimeState((SMLRuntimeState) smlRuntimeStateGraph.getStartState());

			refreshActions();
			viewContentManager.refreshViewContentProvider();
			getViewer().refresh();
		}

	};

	private IDoubleClickListener changeActiveStateDoubleClickListener = new IDoubleClickListener() {
		@Override
		public void doubleClick(DoubleClickEvent event) {
			Object obj = ((IStructuredSelection) event.getSelection()).getFirstElement();
			if (obj instanceof SMLRuntimeState) {
				SMLRuntimeState state = (SMLRuntimeState) ((IStructuredSelection) event.getSelection())
						.getFirstElement();
				currentSMLRuntimeState = state;
				currentSMLRuntimeStateGraph = (SMLRuntimeStateGraph) state.getStateGraph();

				refreshActions();

				viewContentManager.refreshViewContentProvider();

				getViewer().refresh();

			}
		}
	};

	protected void refreshActions() {
		if (jumpToNextSafetyViolatingStateAction != null) {
			if (SMLRuntimeStateUtil.getDistanceToNearestSafetyViolation(getCurrentSMLRuntimeState()) > 0) {
				jumpToNextSafetyViolatingStateAction.setEnabled(true);
			} else {
				jumpToNextSafetyViolatingStateAction.setEnabled(false);
			}
		}
		if (getCurrentSMLRuntimeStateGraph().getStates().size() > ViewContentManager.MAXSTATESFORFULLRENDERING) {
			viewContentManager.getShowSubGraphAction().setEnabled(false);
			viewContentManager.getShowSubGraphAction().setToolTipText(viewContentManager.getShowSubGraphAction()
					.getToolTipText() + "\n(full state graph rendering is disabled for state graphs with more than "
					+ ViewContentManager.MAXSTATESFORFULLRENDERING
					+ " states.\n Use the plus button instead to increase level of predecessor/successor states shown.)");
		} else {
			viewContentManager.getShowSubGraphAction().setEnabled(true);
		}
	}

	private StateGraphLayoutManager stateGraphLayoutManager;
	private ViewContentManager viewContentManager;

	private SMLRuntimeStateGraph currentSMLRuntimeStateGraph;
	private SMLRuntimeState currentSMLRuntimeState;

	public void createPartControl(Composite parent) {

		viewer = new GraphViewer(parent, SWT.NONE);
		viewer.setConnectionStyle(ZestStyles.CONNECTIONS_DIRECTED);

		viewContentManager = new ViewContentManager(this);

		viewer.setLabelProvider(new StateGraphLabelProvider(this));
		viewer.setContentProvider(viewContentManager.getEntityRelationshipContentProvider());
		viewer.setInput(viewContentManager.getNodes());
		stateGraphLayoutManager = new StateGraphLayoutManager(viewer);
		stateGraphLayoutManager.applyLayout();

		makeActions();

		createContextMenu();

		viewer.addDoubleClickListener(changeActiveStateDoubleClickListener);

		hookSelectionListener();

		getSite().setSelectionProvider(viewer);
	}

	public void createContextMenu() {
		final Graph graph = viewer.getGraphControl();
		graph.addMenuDetectListener(new MenuDetectListener() {

			@Override
			public void menuDetected(MenuDetectEvent e) {
				Point point = graph.toControl(e.x, e.y);
				IFigure fig = graph.getFigureAt(point.x, point.y);
				Shell shell = Display.getDefault().getActiveShell();

				if (fig != null) {
					Menu menu = new Menu(shell, SWT.POP_UP);
					if (fig instanceof Label) {
						MenuItem exportToFile = new MenuItem(menu, SWT.NONE);
						exportToFile.setText(SHOW_SYSTEM_OBJECT_POPUP_ACTION_TITLE);
						exportToFile.addSelectionListener(new SelectionListener() {

							@Override
							public void widgetSelected(SelectionEvent e) {
								for (int i = 0; i < viewer.getGraphControl().getNodes().size(); i++) {
									GraphNode n = (GraphNode) viewer.getGraphControl().getNodes().get(i);
									if (n.getNodeFigure().equals(fig)) {
										SMLRuntimeState state = (SMLRuntimeState) n.getData();
										StateGraphViewPart.this.showSystemObjectFromSelectedState(state);
										break;
									}
								}
							}

							@Override
							public void widgetDefaultSelected(SelectionEvent e) {
							}
						});
					} else if (fig instanceof PolylineConnection) {
						MenuItem showCompare = new MenuItem(menu, SWT.NONE);
						showCompare.setText(SHOW_COMPARE_POPUP_ACTION_TITLE);
						showCompare.addSelectionListener(new SelectionListener() {

							@Override
							public void widgetSelected(SelectionEvent e) {
								for (int i = 0; i < viewer.getGraphControl().getConnections().size(); i++) {
									GraphConnection n = (GraphConnection) viewer.getGraphControl().getConnections()
											.get(i);
									if (n.getConnectionFigure().equals(fig)) {
										SMLRuntimeState src = null;
										SMLRuntimeState tar = null;
										if (n.getData() instanceof Transition) {
											Transition tr = (Transition) n.getData();
											src = (SMLRuntimeState) tr.getSourceState();
											tar = (SMLRuntimeState) tr.getTargetState();
										} else if (n.getData() instanceof TransitionSequence) {
											TransitionSequence tr = (TransitionSequence) n.getData();
											src = (SMLRuntimeState) tr.getSource();
											tar = (SMLRuntimeState) tr.getTarget();
										}
										if (src != null && tar != null) {
											CompareSourceAndTargetObjectSystemsJob jobTransition = new CompareSourceAndTargetObjectSystemsJob(
													StateGraphViewPart.this.getSite().getPage(), src, tar);
											IJobManager manager = Job.getJobManager();
											manager.cancel(CompareSourceAndTargetObjectSystemsJob.TRANSITION_JOB_NAME);
											jobTransition.schedule();
										}
										break;
									}
								}
							}

							@Override
							public void widgetDefaultSelected(SelectionEvent e) {
							}
						});
					}

					menu.setVisible(true);
				}
			}
		});
	}

	@Override
	public AbstractZoomableViewer getZoomableViewer() {
		return viewer;
	}

	protected GraphViewer getViewer() {
		return viewer;
	}

	public void setFocus() {
		// viewer.getControl().setFocus();
	}

	private void setCurrentSMLRuntimeStateGraph(SMLRuntimeStateGraph smlRuntimeStateGraph) {
		this.currentSMLRuntimeStateGraph = smlRuntimeStateGraph;
	}

	public SMLRuntimeStateGraph getCurrentSMLRuntimeStateGraph() {
		return currentSMLRuntimeStateGraph;
	}

	private void setCurrentSMLRuntimeState(SMLRuntimeState smlRuntimeState) {
		this.currentSMLRuntimeState = smlRuntimeState;
	}

	public SMLRuntimeState getCurrentSMLRuntimeState() {
		return currentSMLRuntimeState;
	}

	private void setInput(Collection<SMLRuntimeState> smlRuntimeStates) {
		try {
			getViewer().setInput(smlRuntimeStates);
			getViewer().refresh();
		} catch (Exception e) {
			//System.out.println("Disposed " + this.toString());
		}
	}

	protected void hookSelectionListener() {
		getSite().getWorkbenchWindow().getSelectionService().addSelectionListener(selectionListener);
	}

	private void disposeSelectionListener() {
		getSite().getWorkbenchWindow().getSelectionService().removeSelectionListener(selectionListener);
	}

	private void disposeDoubleClickListener() {
		viewer.removeDoubleClickListener(changeActiveStateDoubleClickListener);
	}

	@Override
	public void dispose() {
		disposeDoubleClickListener();
		disposeSelectionListener();
		ObjectSystemView.cancelJobs();
		super.dispose();
	}

	public void refreshCurrentStateGraph() {
		getViewer().setContentProvider(viewContentManager.getEntityRelationshipContentProvider());
		setInput(viewContentManager.getNodes());
	}

	// Menu
	private ZoomContributionViewItem toolbarZoomContributionViewItem;
	private ActionContributionItem toggleShowSubGraph;
	private ActionContributionItem toggleShowReducedToEnvironmentStates;
	private ActionContributionItem refresh;
	private ActionContributionItem changeLayout;
	// private ActionContributionItem toggleShowEnabledMessagesOnAllStates;
	// private ActionContributionItem toggleShowEnabledMessagesOnCurrentState;
	private ActionContributionItem toggleShowScenarioInformationInStates;
	private ActionContributionItem increaseSubGraph;
	private ActionContributionItem decreaseSubGraph;
	private ActionContributionItem jumpToNextSafetyViolatingState;

	private ActionContributionItem showSelectedSystemObject;

	// ToolBar
	private ActionContributionItem toggleShowSubGraph2;
	private ActionContributionItem toggleShowReducedToEnvironmentStates2;
	private ActionContributionItem refresh2;
	private ActionContributionItem changeLayout2;
	// private ActionContributionItem toggleShowEnabledMessagesOnAllStates2;
	// private ActionContributionItem toggleShowEnabledMessagesOnCurrentState2;
	private ActionContributionItem toggleShowScenarioInformationInStates2;
	private ActionContributionItem increaseSubGraph2;
	private ActionContributionItem displaySizeOfSubGraph;
	private ActionContributionItem decreaseSubGraph2;
	private ActionContributionItem jumpToNextSafetyViolatingState2;

	private ActionContributionItem showSelectedSystemObject2;

	private void contributeActionBarsItemsForView() {
		IActionBars bars = getViewSite().getActionBars();
		IMenuManager menuManager = bars.getMenuManager();
		IToolBarManager toolBarManager = bars.getToolBarManager();

		List<ContributionItem> activePullDownActionsFromSimulationGraphView = getPullDownActions();
		List<ContributionItem> activeToolBarActionsFromSimulationGraphView = getToolBarActions();

		for (ContributionItem item : activePullDownActionsFromSimulationGraphView) {
			menuManager.add(item);
		}
		for (ContributionItem item : activeToolBarActionsFromSimulationGraphView) {
			toolBarManager.add(item);
		}

		bars.updateActionBars();
	}

	public int getInformationDisplayLevel() {
		return informationDisplayLevel;
	}

	public Action getShowScenarioInformationInStatesAction() {
		informationDisplayLevel = 0;
		Action toggleShowScenarioInformationInStates = new Action("Show scenario information in state.",
				Action.AS_PUSH_BUTTON) {
			public void run() {
				if (informationDisplayLevel > 3)
					informationDisplayLevel = 0;
				else
					informationDisplayLevel++;

				refreshCurrentSimulationGraph();
			}

		};
		toggleShowScenarioInformationInStates.setToolTipText("Change level of information displayed about the states.");
		toggleShowScenarioInformationInStates
				.setImageDescriptor(Activator.getImageDescriptor("img/showActiveScenariosIcon.png"));

		return toggleShowScenarioInformationInStates;
	}

	public Action getJumpToNextSafetyViolatingStateAction() {
		jumpToNextSafetyViolatingStateAction = new Action("Jump to next safety-violating state.",
				Action.AS_PUSH_BUTTON) {
			@Override
			public void run() {
				SMLRuntimeState currentState = getCurrentSMLRuntimeState();
				if (currentState == null)
					return;

				while (SMLRuntimeStateUtil.getDistanceToNearestSafetyViolation(currentState) > 0) {
					for (Transition t : currentState.getOutgoingTransition()) {
						if (SMLRuntimeStateUtil.isTransitionLeadsToGuaranteeSafetyViolation(t)) {
							currentState = (SMLRuntimeState) t.getTargetState();
						}
					}
				}
				setCurrentSMLRuntimeState(currentState);
				refreshCurrentSimulationGraph();
			}
		};
		jumpToNextSafetyViolatingStateAction.setToolTipText(
				"Jump to next safety-violating state in a counter-strategy if there is any such successor state of the currently selected one.");

		jumpToNextSafetyViolatingStateAction
				.setImageDescriptor(Activator.getImageDescriptor("img/jump-safety-violation.png"));

		jumpToNextSafetyViolatingStateAction.setEnabled(false);

		return jumpToNextSafetyViolatingStateAction;
	}

	protected void makeActions() {
		toolbarZoomContributionViewItem = new ZoomContributionViewItem(this);

		Action refreshAction = new Action("Refresh Simulation Graph", Action.AS_PUSH_BUTTON) {
			public void run() {
				StateGraphViewPart.this.refreshCurrentSimulationGraph();
			}
		};
		refreshAction.setToolTipText("Refresh Simulation Graph");
		refreshAction.setImageDescriptor(Activator.getImageDescriptor("img/refresh.png"));

		Action changeLayoutAction = stateGraphLayoutManager.getChangeLayoutAction();
		Action toggleShowSubGraphAction = viewContentManager.getShowSubGraphAction();
		Action toggleShowReducedToEnvironmentStatesAction = viewContentManager
				.getShowReducedToEnvironmentStatesAction();
		// Action toggleShowEnabledMessagesOnAllStatesAction =
		// viewContentManager.getShowEnabledMessagesOnAllStatesAction();
		// Action toggleShowEnabledMessagesOnCurrentStateAction =
		// viewContentManager.getShowEnabledMessagesOnCurrentStateAction();
		Action toggleShowScenarioInformationInStatesAction = getShowScenarioInformationInStatesAction();
		Action increaseSubGraphAction = viewContentManager.getIncreaseSubGraphAction();
		Action decreaseSubGraphAction = viewContentManager.getDecreaseSubGraphAction();
		Action displaySizeOfSubGraphAction = viewContentManager.getDisplaySizeOfSubGraphAction();
		Action jumpToNextSafetyViolatingStateAction = getJumpToNextSafetyViolatingStateAction();

		Action showSelectedSystemObjectAction = new Action(SHOW_SYSTEM_OBJECT_ACTION_TITLE, Action.AS_PUSH_BUTTON) {
			public void run() {
				StateGraphViewPart.this.showSystemObjectFromSelectedState(getCurrentSMLRuntimeState());
			}
		};
		showSelectedSystemObjectAction.setToolTipText(SHOW_SYSTEM_OBJECT_ACTION_TITLE);
		showSelectedSystemObjectAction.setImageDescriptor(Activator.getImageDescriptor("img/show-system-object.png"));

		toggleShowSubGraph = new ActionContributionItem(toggleShowSubGraphAction);
		toggleShowSubGraph2 = new ActionContributionItem(toggleShowSubGraphAction);
		toggleShowReducedToEnvironmentStates = new ActionContributionItem(toggleShowReducedToEnvironmentStatesAction);
		toggleShowReducedToEnvironmentStates2 = new ActionContributionItem(toggleShowReducedToEnvironmentStatesAction);
		refresh = new ActionContributionItem(refreshAction);
		refresh2 = new ActionContributionItem(refreshAction);
		changeLayout = new ActionContributionItem(changeLayoutAction);
		changeLayout2 = new ActionContributionItem(changeLayoutAction);
		toggleShowScenarioInformationInStates = new ActionContributionItem(toggleShowScenarioInformationInStatesAction);
		toggleShowScenarioInformationInStates2 = new ActionContributionItem(
				toggleShowScenarioInformationInStatesAction);
		// toggleShowEnabledMessagesOnAllStates = new
		// ActionContributionItem(toggleShowEnabledMessagesOnAllStatesAction);
		// toggleShowEnabledMessagesOnAllStates2 = new
		// ActionContributionItem(toggleShowEnabledMessagesOnAllStatesAction);
		// toggleShowEnabledMessagesOnCurrentState = new
		// ActionContributionItem(toggleShowEnabledMessagesOnCurrentStateAction);
		// toggleShowEnabledMessagesOnCurrentState2 = new
		// ActionContributionItem(toggleShowEnabledMessagesOnCurrentStateAction);
		increaseSubGraph = new ActionContributionItem(increaseSubGraphAction);
		increaseSubGraph2 = new ActionContributionItem(increaseSubGraphAction);
		displaySizeOfSubGraph = new ActionContributionItem(displaySizeOfSubGraphAction);
		decreaseSubGraph = new ActionContributionItem(decreaseSubGraphAction);
		decreaseSubGraph2 = new ActionContributionItem(decreaseSubGraphAction);
		jumpToNextSafetyViolatingState = new ActionContributionItem(jumpToNextSafetyViolatingStateAction);
		jumpToNextSafetyViolatingState2 = new ActionContributionItem(jumpToNextSafetyViolatingStateAction);

		showSelectedSystemObject = new ActionContributionItem(showSelectedSystemObjectAction);
		showSelectedSystemObject2 = new ActionContributionItem(showSelectedSystemObjectAction);

		contributeActionBarsItemsForView();
	}

	protected List<ContributionItem> getPullDownActions() {
		List<ContributionItem> result = new ArrayList<ContributionItem>();
		result.add(toolbarZoomContributionViewItem);
		result.add(toggleShowSubGraph);
		result.add(decreaseSubGraph);
		result.add(increaseSubGraph);
		result.add(new Separator());
		result.add(toggleShowReducedToEnvironmentStates);
		result.add(toggleShowScenarioInformationInStates);
		result.add(new Separator());
		result.add(refresh);
		result.add(changeLayout);
		// result.add(toggleShowEnabledMessagesOnAllStates);
		// result.add(toggleShowEnabledMessagesOnCurrentState);
		result.add(jumpToNextSafetyViolatingState);

		result.add(showSelectedSystemObject);
		return result;
	}

	protected List<ContributionItem> getToolBarActions() {
		List<ContributionItem> result = new ArrayList<ContributionItem>();
		result.add(toggleShowSubGraph2);
		result.add(decreaseSubGraph2);
		result.add(displaySizeOfSubGraph);
		result.add(increaseSubGraph2);
		result.add(new Separator());
		result.add(toggleShowReducedToEnvironmentStates2);
		result.add(toggleShowScenarioInformationInStates2);
		result.add(new Separator());
		result.add(refresh2);
		result.add(changeLayout2);
		// result.add(toggleShowEnabledMessagesOnAllStates2);
		// result.add(toggleShowEnabledMessagesOnCurrentState2);
		result.add(jumpToNextSafetyViolatingState2);

		result.add(showSelectedSystemObject2);
		return result;
	}

	public void refreshCurrentSimulationGraph() {
		setInput(viewContentManager.getNodes());
	}

	/**
	 * Exports the Objectsystem of the selected SMLRuntimeState to a text file
	 * and shows it
	 */
	public void showSystemObjectFromSelectedState(SMLRuntimeState smlRuntimeState) {
		if (smlRuntimeState == null) {
			return;
		}
		String index = SMLRuntimeStateUtil.getPassedIndex(smlRuntimeState);
		for (EObject rootElt : smlRuntimeState.getDynamicObjectContainer().getRootObjects()) {
			String objectStringTextual = OsmlGeneratorUtils.generateStringFromObject(rootElt);
			IPath selectedFileIPath = OsmlGeneratorUtils.getCurrentStateGraphFileIPath(rootElt);
			if (selectedFileIPath == null) {
				return;
			}
			File selectedFileFolder = selectedFileIPath.removeLastSegments(1).toFile();
			File outputFolder = new File(selectedFileFolder, OBJECTSYSTEM_OUPUT_FOLDER_NAME);
			if (!outputFolder.exists()) {
				outputFolder.mkdirs();
			}
			String outputFileNameTextual = selectedFileIPath.lastSegment().concat("_" + index).concat(OBJECTSYSTEM_FILE_EXTENSION);
			File outputFileTextual = new File(outputFolder, outputFileNameTextual);
			try {
				// save file : Textual
				OutputStream fosTextual = new FileOutputStream(outputFileTextual);
				fosTextual.write(objectStringTextual.getBytes());
				fosTextual.close();
				// open file : Textual
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				IFileStore fileStore = EFS.getLocalFileSystem().getStore(outputFileTextual.toURI());
				IDE.openEditorOnFileStore(page, fileStore);
				// format file
				IEditorPart editor = page.getActiveEditor();
				XtextEditor xed = (XtextEditor) editor;
				((SourceViewer) xed.getInternalSourceViewer()).doOperation(ISourceViewer.FORMAT);
				// save file without asking to the user : false
				page.saveEditor(editor, false);
			} catch (IOException | PartInitException e) {
				e.printStackTrace();
			}
		}
	}

	public Control getControl() {
		return getViewer().getControl();
	}

	static int id = 0;
	private String name = "";

	@Override
	public String toString() {
		if (name == "")
			name = "StateGraphView #" + id++;
		return name;
	}
}
