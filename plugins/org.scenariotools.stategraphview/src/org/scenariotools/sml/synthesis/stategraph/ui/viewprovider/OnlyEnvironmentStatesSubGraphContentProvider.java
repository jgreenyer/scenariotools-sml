/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.synthesis.stategraph.ui.viewprovider;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.synthesis.stategraph.ui.model.TransitionSequence;
import org.scenariotools.sml.synthesis.stategraph.ui.views.StateGraphViewPart;
import org.scenariotools.sml.runtime.Transition;

public class OnlyEnvironmentStatesSubGraphContentProvider extends OnlyEnvironmentStatesContentProvider {

	private int numberOfNeighbors;
	
	protected OnlyEnvironmentStatesSubGraphContentProvider(StateGraphViewPart stateGraphView) {
		this(stateGraphView,1);
	}
	
	OnlyEnvironmentStatesSubGraphContentProvider(StateGraphViewPart stateGraphView, int numAncestors) {
		super(stateGraphView);
		this.numberOfNeighbors = numAncestors;
	}
	
	public void setNumberOfNeighbors(int numberOfNeighbors) {
		this.numberOfNeighbors = numberOfNeighbors;
	}
	
	@Override
	public Collection<SMLRuntimeState> getNodes() {
		Collection<SMLRuntimeState> smlRuntimeStates = super.getNodes();
		
		//BFS forward and backward
		Collection<SMLRuntimeState> closed = new HashSet<SMLRuntimeState>();
		SMLRuntimeState currentState = getCurrentState();
		
		
		visitedStates = new HashSet<SMLRuntimeState>();
		SMLRuntimeState nextEnvironmentState = getNextEnvironmentStatePredecessor(currentState);
		if (nextEnvironmentState == null)
			nextEnvironmentState = currentState;
		
		if(smlRuntimeStates.contains(nextEnvironmentState)){
			if (numberOfNeighbors > 0){
				Queue<SMLRuntimeState> succFw = new LinkedList<SMLRuntimeState>();
				Queue<SMLRuntimeState> openFw = new LinkedList<SMLRuntimeState>();
				Queue<SMLRuntimeState> succBw = new LinkedList<SMLRuntimeState>();
				Queue<SMLRuntimeState> openBw = new LinkedList<SMLRuntimeState>();
				
				openFw.add(nextEnvironmentState);
				openBw.add(nextEnvironmentState);
				
				for(int i = 0; i <= numberOfNeighbors; i++){
					//BFS forward
					while(!openFw.isEmpty()){
						if (isEnvironmentOrDeadlockState(openFw.peek()))
							closed.add(openFw.peek());
						SMLRuntimeState src = openFw.remove();
						for(Transition t : src.getOutgoingTransition()){
							if (!isEnvironmentTransition(t)) continue;
							SMLRuntimeState s = (SMLRuntimeState) t.getTargetState();
							succFw.add(s);
						}
						List<TransitionSequence> tsList = getStateToOutgoingTransitionSequenceMap().get(src);
						if(tsList != null){
							for(TransitionSequence ts : tsList){
								SMLRuntimeState s = (SMLRuntimeState) ts.getTarget();
								succFw.add(s);
							}
						}
					}
					openFw.addAll(succFw);
					//BFS backward
					while(!openBw.isEmpty()){
						if (isEnvironmentOrDeadlockState(openBw.peek()))
							closed.add(openBw.peek());
						SMLRuntimeState dest = openBw.remove();
						for(Transition t : dest.getIncomingTransition()){
							if (!isEnvironmentTransition(t)) continue;
							SMLRuntimeState s = (SMLRuntimeState) t.getSourceState();
							succBw.add(s);
						}
						List<TransitionSequence> tsList = getStateToIncomingTransitionSequenceMap().get(dest);
						if(tsList != null){
							for(TransitionSequence ts : tsList){
								SMLRuntimeState s = (SMLRuntimeState) ts.getSource();
								succBw.add(s);
							}
						}
					}
					openBw.addAll(succBw);
				}
			} else if (numberOfNeighbors == 0){ // only show level 1 successors
				closed.add(nextEnvironmentState);
				for(Transition t : nextEnvironmentState.getOutgoingTransition()){
					if (!isEnvironmentTransition(t)) continue;
					SMLRuntimeState s = (SMLRuntimeState) t.getTargetState();
					closed.add(s);
				}
				List<TransitionSequence> tsList = getStateToOutgoingTransitionSequenceMap().get(nextEnvironmentState);
				if(tsList != null){
					for(TransitionSequence ts : tsList){
						SMLRuntimeState s = (SMLRuntimeState) ts.getTarget();
						closed.add(s);
					}
				}
			}
		}
		return (Collection<SMLRuntimeState>)closed;
	}
	
	private Set<SMLRuntimeState> visitedStates;
	private SMLRuntimeState getNextEnvironmentStatePredecessor(SMLRuntimeState state){
		visitedStates.add(state);
		if(isEnvironmentOrDeadlockState(state)) 
			return state;
		else{
			for (Transition transition : state.getIncomingTransition()) {
				if(!visitedStates.contains(transition.getSourceState()))
					return getNextEnvironmentStatePredecessor((SMLRuntimeState) transition.getSourceState());
			}
		}
		return null;
	}

}
