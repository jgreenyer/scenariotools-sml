/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.synthesis.stategraph.ui.viewprovider;

import java.util.Collection;

import org.eclipse.zest.core.viewers.IGraphEntityRelationshipContentProvider;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.synthesis.stategraph.ui.views.StateGraphViewPart;


public interface ViewContentProvider extends IGraphEntityRelationshipContentProvider {

	Collection<SMLRuntimeState> getNodes();

	SMLRuntimeState getCurrentState();

	SMLRuntimeStateGraph getCurrentSMLRuntimeStateGraph();
	
	
	public static class Factory{
		
		public static ViewContentProvider getFullGraph(StateGraphViewPart stateGraphView){
			return new FullGraphContentProvider(stateGraphView);
		}

//		public static ViewContentProvider getFullGraph(StateGraphViewPart stateGraphView, Predicate<SMLRuntimeState> predicate){
//			return new FullGraphContentProvider(stateGraphView, predicate);
//		}

		
//		public static ViewContentProvider getSubGraph(StateGraphView simulationGraphView){
//			return new SubGraphContentProvider(simulationGraphView);
//		}
		
		public static ViewContentProvider getSubGraph(StateGraphViewPart stateGraphView, int size){
			return new SubGraphContentProvider(stateGraphView, size);
		}
		
		public static ViewContentProvider getOnlyEnvironmentStates(StateGraphViewPart stateGraphView){
			return new OnlyEnvironmentStatesContentProvider(stateGraphView);
		}
		
		public static ViewContentProvider getOnlyEnvironmentStatesSubGraphContentProvider(StateGraphViewPart stateGraphView, int size){
			return new OnlyEnvironmentStatesSubGraphContentProvider(stateGraphView, size);
		}

//		
//		public static ViewContentProvider getExtendActualStateOnFullGraph(StateGraphView simulationGraphView){
//			ViewContentProvider baseViewContentProvider = new FullGraphContentProvider(simulationGraphView);
//			return new ExtendActualStateWithEnabledMessageContentProvider(baseViewContentProvider);
//		}
//		
//		public static ViewContentProvider getExtendActualStateOnSubGraph(StateGraphView simulationGraphView){
//			ViewContentProvider baseViewContentProvider = new SubGraphContentProvider(simulationGraphView);
//			return new ExtendActualStateWithEnabledMessageContentProvider(baseViewContentProvider);
//		}
//		
//		public static ViewContentProvider getExtendActualStateOnSubGraph(StateGraphView simulationGraphView, int size){
//			ViewContentProvider baseViewContentProvider = new SubGraphContentProvider(simulationGraphView, size);
//			return new ExtendActualStateWithEnabledMessageContentProvider(baseViewContentProvider);
//		}
//		
//		public static ViewContentProvider getExtendAllStatesOnFullGraph(StateGraphView simulationGraphView){
//			ViewContentProvider baseViewContentProvider = new FullGraphContentProvider(simulationGraphView);
//			return new ExtendAllStatesWithEnabledMessageContentProvider(baseViewContentProvider);
//		}
//		
//		public static ViewContentProvider getExtendAllStatesOnSubGraph(StateGraphView simulationGraphView){
//			ViewContentProvider baseViewContentProvider = new SubGraphContentProvider(simulationGraphView);
//			return new ExtendAllStatesWithEnabledMessageContentProvider(baseViewContentProvider);
//		}
//		
//		public static ViewContentProvider getExtendAllStatesOnSubGraph(StateGraphView simulationGraphView, int size){
//			ViewContentProvider baseViewContentProvider = new SubGraphContentProvider(simulationGraphView, size);
//			return new ExtendAllStatesWithEnabledMessageContentProvider(baseViewContentProvider);
//		}
	}
}	