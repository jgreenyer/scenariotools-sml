/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.synthesis.stategraph.ui.views;

import java.util.Collection;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.swt.widgets.Display;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.synthesis.stategraph.ui.Activator;
import org.scenariotools.sml.synthesis.stategraph.ui.viewprovider.OnlyEnvironmentStatesContentProvider;
import org.scenariotools.sml.synthesis.stategraph.ui.viewprovider.OnlyEnvironmentStatesSubGraphContentProvider;
import org.scenariotools.sml.synthesis.stategraph.ui.viewprovider.ViewContentProvider;

public class ViewContentManager {

	private Action toggleShowSubGraph;
	private Action toggleShowReducedToEnvironmentStates;
	private Action increaseSubGraph;
	private Action decreaseSubGraph;
	private Action displaySizeOfSubGraph;

	private boolean showSubGraph;
	private boolean showReducedToEnvironmentStates;
	private int numberOfNeighbors;
	
	public final static int MAXSTATESFORFULLRENDERING = 100;
	
	private ViewContentProvider viewContentProvider;
	private StateGraphViewPart stateGraphView;
	
	public ViewContentManager(StateGraphViewPart stateGraphView) {
		this.stateGraphView = stateGraphView;
		this.viewContentProvider = ViewContentProvider.Factory.getFullGraph(stateGraphView);
		this.showSubGraph = true;
		this.numberOfNeighbors = 1;
	}
	
	private void toggleShowSubGraph() {
		showSubGraph = !showSubGraph;
		refreshViewContentProvider();
	}

	private void toggleShowReducedToEnvironmentStates() {
		showReducedToEnvironmentStates = !showReducedToEnvironmentStates;
		refreshViewContentProvider();
	}

	private void increaseSubGraph() {	
		numberOfNeighbors++;
		refreshShowSubGraphDescription();
		refreshViewContentProvider();
	}

	private void decreaseSubGraph() {
		if(numberOfNeighbors > 0){
			numberOfNeighbors--;
			refreshShowSubGraphDescription();
			refreshViewContentProvider();
		}
	}

	private void refreshShowSubGraphDescription() {
		if (numberOfNeighbors > 0){
			toggleShowSubGraph.setDescription("Show only successors and antecessors up to a depth of " + numberOfNeighbors +".");
			toggleShowSubGraph.setToolTipText("Show only successors and antecessors up to a depth of " + numberOfNeighbors +".");
		}else if (numberOfNeighbors == 0){
			toggleShowSubGraph.setDescription("Show only direct successors of current node.");
			toggleShowSubGraph.setToolTipText("Show only direct successors of current node.");
		}
		displaySizeOfSubGraph.setText("#" + Integer.toString(numberOfNeighbors));
	}
		
	public Action getShowReducedToEnvironmentStatesAction() {
		toggleShowReducedToEnvironmentStates = new Action("Show only states with outgoing environment events.", 
				Action.AS_CHECK_BOX) {
			public void run() {
				toggleShowReducedToEnvironmentStates();
			}
		};
		toggleShowReducedToEnvironmentStates.setToolTipText("Show only states with outgoing environment events.");
		toggleShowReducedToEnvironmentStates.setImageDescriptor(Activator.getImageDescriptor("img/env-msgs.png"));
		
		return toggleShowReducedToEnvironmentStates;
	}
	
	public Action getShowSubGraphAction(){
		if (toggleShowSubGraph == null){
			toggleShowSubGraph = new Action("Show only successors and antecessors up to a depth of x.", 
					Action.AS_CHECK_BOX) {
					public void run() {
						toggleShowSubGraph();
					}
				};
			toggleShowSubGraph.setImageDescriptor(Activator.getImageDescriptor("img/subtree.png"));
		}
		if (numberOfNeighbors > 0)
			toggleShowSubGraph
					.setToolTipText("Show only successors and antecessors up to a depth of " + numberOfNeighbors + ".");
		else if (numberOfNeighbors == 0)
			toggleShowSubGraph
			.setToolTipText("Show only direct successors of current node.");
		
		return toggleShowSubGraph;
	}
	
	public Action getIncreaseSubGraphAction(){
		increaseSubGraph = new Action("Increase number of successors and antecessors.", 
				Action.AS_PUSH_BUTTON) {
			public void run() {
				increaseSubGraph();
			}
		};
		increaseSubGraph.setToolTipText("Increase number of successors and antecessor.");
		increaseSubGraph.setImageDescriptor(Activator.getImageDescriptor("img/subtreeSizePlus.png"));
		
		return increaseSubGraph;
	}

	public Action getDecreaseSubGraphAction(){
		decreaseSubGraph = new Action("Decrease number of successors and antecessor.", 
				Action.AS_PUSH_BUTTON) {
			public void run() {
				decreaseSubGraph();
			}
		};
		decreaseSubGraph.setToolTipText("Decrease number of successors and antecessor.");
		decreaseSubGraph.setImageDescriptor(Activator.getImageDescriptor("img/subtreeSizeMinus.png"));
		
		return decreaseSubGraph;
	}
	
	public Action getDisplaySizeOfSubGraphAction(){
		displaySizeOfSubGraph = new Action("Number of successors and antecessor.", Action.AS_PUSH_BUTTON){};
		displaySizeOfSubGraph.setToolTipText("Number of successors and antecessor.");
		displaySizeOfSubGraph.setText("#" + Integer.toString(numberOfNeighbors));
		displaySizeOfSubGraph.setEnabled(false);
		
		return displaySizeOfSubGraph;
	}	
	
	public Collection<SMLRuntimeState> getNodes(){
		return viewContentProvider.getNodes();
	}
	
	private void refreshCurrentStateGraph(){
		stateGraphView.refreshCurrentStateGraph();
	}
	
	private OnlyEnvironmentStatesSubGraphContentProvider cachedOnlyEnvironmentStatesSubGraphContentProvider;
	private OnlyEnvironmentStatesContentProvider cachedOnlyEnvironmentStatesContentProvider;
	
	protected void refreshViewContentProvider(){
		
		if(showReducedToEnvironmentStates){
			if (showSubGraph()){
				if (cachedOnlyEnvironmentStatesSubGraphContentProvider == null
				|| !cachedOnlyEnvironmentStatesSubGraphContentProvider.getCurrentSMLRuntimeStateGraph().equals(stateGraphView.getCurrentSMLRuntimeStateGraph())){
					cachedOnlyEnvironmentStatesSubGraphContentProvider = 
							(OnlyEnvironmentStatesSubGraphContentProvider) 
							ViewContentProvider.Factory.getOnlyEnvironmentStatesSubGraphContentProvider(stateGraphView, numberOfNeighbors);
				}
				cachedOnlyEnvironmentStatesSubGraphContentProvider.setNumberOfNeighbors(numberOfNeighbors);
				viewContentProvider = cachedOnlyEnvironmentStatesSubGraphContentProvider;
			}else{
				if (cachedOnlyEnvironmentStatesContentProvider == null
				|| !cachedOnlyEnvironmentStatesContentProvider.getCurrentSMLRuntimeStateGraph().equals(stateGraphView.getCurrentSMLRuntimeStateGraph())){
					cachedOnlyEnvironmentStatesContentProvider = 
							(OnlyEnvironmentStatesContentProvider) 
							ViewContentProvider.Factory.getOnlyEnvironmentStates(stateGraphView);
				}
				viewContentProvider = cachedOnlyEnvironmentStatesContentProvider; 
			}

		} else if (showSubGraph()){
			viewContentProvider = ViewContentProvider.Factory.getSubGraph(stateGraphView, numberOfNeighbors);
		} else {
			viewContentProvider = ViewContentProvider.Factory.getFullGraph(stateGraphView);
		}
		
		Display.getCurrent().asyncExec(new Runnable() {
			@Override
			public void run() {
				ViewContentManager.this.refreshCurrentStateGraph();
			}
		});

	}

	private boolean showSubGraph() {
		return showSubGraph 
				|| stateGraphView.getCurrentSMLRuntimeStateGraph().getStates().size() > MAXSTATESFORFULLRENDERING;
	}
		
	public IContentProvider getEntityRelationshipContentProvider() {
		return viewContentProvider;
	}

}
