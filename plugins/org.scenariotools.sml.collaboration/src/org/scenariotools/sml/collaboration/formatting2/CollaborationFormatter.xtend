/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.collaboration.formatting2;

import com.google.inject.Inject
import org.eclipse.xtext.formatting2.IFormattableDocument
import org.scenariotools.sml.Alternative
import org.scenariotools.sml.Case
import org.scenariotools.sml.Collaboration
import org.scenariotools.sml.Condition
import org.scenariotools.sml.ConditionExpression
import org.scenariotools.sml.ConstraintBlock
import org.scenariotools.sml.ValueParameterExpression
import org.scenariotools.sml.FeatureAccessBindingExpression
import org.scenariotools.sml.Interaction
import org.scenariotools.sml.InteractionFragment
import org.scenariotools.sml.InterruptCondition
import org.scenariotools.sml.Loop
import org.scenariotools.sml.Message
import org.scenariotools.sml.ModalMessage
import org.scenariotools.sml.Parallel
import org.scenariotools.sml.ParameterBinding
import org.scenariotools.sml.RoleBindingConstraint
import org.scenariotools.sml.Scenario
import org.scenariotools.sml.TimedInterruptCondition
import org.scenariotools.sml.TimedViolationCondition
import org.scenariotools.sml.TimedWaitCondition
import org.scenariotools.sml.VariableFragment
import org.scenariotools.sml.ViolationCondition
import org.scenariotools.sml.WaitCondition
import org.scenariotools.sml.collaboration.services.CollaborationGrammarAccess
import org.scenariotools.sml.expressions.formatting2.ScenarioExpressionsFormatter
import org.scenariotools.sml.expressions.scenarioExpressions.Import
import org.eclipse.xtext.formatting2.regionaccess.ISemanticRegion
import org.scenariotools.sml.SmlPackage
import org.eclipse.emf.ecore.EObject
import org.scenariotools.sml.VariableBindingParameterExpression

class CollaborationFormatter extends ScenarioExpressionsFormatter {

	@Inject extension CollaborationGrammarAccess
	
	def dispatch void format(Collaboration collaboration, extension IFormattableDocument document) {
		for (Import imports : collaboration.getImports()) {
			format(imports, document);
		}
		
		indentInterior(collaboration, document)

		collaboration.regionFor.keyword("domain").prepend[newLines = 2]
		formatCollaboration(collaboration, document)
	}

	def void formatCollaboration(Collaboration collaboration, extension IFormattableDocument document) {
		collaboration.regionFor.keyword("collaboration").prepend[newLines = 2]

		for (var i = 0; i < collaboration.roles.size; i++) {
			val role = collaboration.roles.get(i)

			if (i == 0)
				role.prepend[newLines = 2]
			else
				role.prepend[newLine]
		}

		for (Scenario scenarios : collaboration.getScenarios()) {
			format(scenarios, document);
		}

		collaboration.semanticRegions.get(collaboration.semanticRegions.size - 1).prepend[newLine]
	}

	def dispatch void format(Scenario scenario, extension IFormattableDocument document) {
		var region = scenario.semanticRegions.get(0)
		
		region.prepend[newLines = 2]

		for (RoleBindingConstraint roleBindings : scenario.getRoleBindings()) {
			format(roleBindings, document);
		}
		format(scenario.getOwnedInteraction(), document);
	}

	def dispatch void format(RoleBindingConstraint rolebindingconstraint, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		format(rolebindingconstraint.getBindingExpression(), document);
	}

	def dispatch void format(FeatureAccessBindingExpression featureaccessbindingexpression, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		format(featureaccessbindingexpression.getFeatureaccess(), document);
	}

	def dispatch void format(VariableFragment variablefragment, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		format(variablefragment.getExpression(), document);
	}

	def dispatch void format(Interaction interaction, extension IFormattableDocument document) {
		indentInterior(interaction, document)

		for (InteractionFragment fragment : interaction.getFragments()) {
			fragment.prepend[newLine]
			format(fragment, document);
		}

		interaction.regionFor.keyword("}").prepend[newLine]

		format(interaction.getConstraints(), document);
	}

	def dispatch void format(ModalMessage modalmessage, extension IFormattableDocument document) {
		var region = modalmessage.semanticRegions.get(1)
		 while(region.text != "->") {
			region.prepend[oneSpace]
			region = region.nextSemanticRegion
		}
		region.surround[noSpace] // "->"
		
		region = region.nextSemanticRegion.nextSemanticRegion // this is '.'
		region.surround[noSpace]
		
		for(pair : modalmessage.regionFor.keywordPairs("(", ")")) {
			pair.key.surround[noSpace]
			pair.value.prepend[noSpace]
		}

		for (ParameterBinding parameters : modalmessage.getParameters()) {
			format(parameters, document);
		}
	}

	def dispatch void format(ParameterBinding parameterbinding, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		format(parameterbinding.getBindingExpression(), document);
	}

	def dispatch void format(ValueParameterExpression valueParameterExpression, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		format(valueParameterExpression.getValue(), document);
	}

	def dispatch void format(VariableBindingParameterExpression variableBindingParameterExpression, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		format(variableBindingParameterExpression.getVariable(), document);
	}

	def dispatch void format(Alternative alternative, extension IFormattableDocument document) {
		for(region : alternative.regionFor.keywords("or"))
			region.surround[oneSpace]
		
//		println(alternative.regionFor.keyword("alternative").text)
		for (Case cases : alternative.getCases()) {
			format(cases, document);
		}
	}

	def dispatch void format(Case theCase, extension IFormattableDocument document) {
		format(theCase.getCaseCondition(), document);
		format(theCase.getCaseInteraction(), document);
	}

	def dispatch void format(Loop loop, extension IFormattableDocument document) {
		format(loop.getLoopCondition(), document);
		format(loop.getBodyInteraction(), document);
	}

	def dispatch void format(Parallel parallel, extension IFormattableDocument document) {
		for (Interaction parallelInteraction : parallel.getParallelInteraction()) {
			format(parallelInteraction, document);
		}
	}

	def dispatch void format(WaitCondition waitcondition, extension IFormattableDocument document) {
		waitcondition.regionFor.keyword("wait").append[oneSpace]
		waitcondition.regionFor.feature(SmlPackage.Literals.WAIT_CONDITION__STRICT).surround[oneSpace]
		waitcondition.regionFor.feature(SmlPackage.Literals.WAIT_CONDITION__REQUESTED).surround[oneSpace]
		format(waitcondition.getConditionExpression(), document);
	}

	def dispatch void format(InterruptCondition interruptcondition, extension IFormattableDocument document) {
		interruptcondition.regionFor.keyword("interrupt").append[oneSpace]
		format(interruptcondition.getConditionExpression(), document);
	}

	def dispatch void format(ViolationCondition violationcondition, extension IFormattableDocument document) {
		violationcondition.regionFor.keyword("violation").append[oneSpace] 
		format(violationcondition.getConditionExpression(), document);
	}

	def dispatch void format(TimedWaitCondition timedwaitcondition, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		format(timedwaitcondition.getTimedConditionExpression(), document);
	}

	def dispatch void format(TimedViolationCondition timedviolationcondition, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		format(timedviolationcondition.getTimedConditionExpression(), document);
	}

	def dispatch void format(TimedInterruptCondition timedinterruptcondition, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		format(timedinterruptcondition.getTimedConditionExpression(), document);
	}

	def dispatch void format(Condition condition, extension IFormattableDocument document) {
		condition.surround[oneSpace] 
		format(condition.getConditionExpression(), document);
	}

	def dispatch void format(ConditionExpression conditionexpression, extension IFormattableDocument document) {
		conditionexpression.surround[noSpace]
		format(conditionexpression.getExpression(), document);
	}

	def dispatch void format(ConstraintBlock constraintblock, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		for (Message consider : constraintblock.getConsider()) {
			format(consider, document);
		}
		for (Message ignore : constraintblock.getIgnore()) {
			format(ignore, document);
		}
		for (Message forbidden : constraintblock.getForbidden()) {
			format(forbidden, document);
		}
		for (Message interrupt : constraintblock.getInterrupt()) {
			format(interrupt, document);
		}
	}

	def dispatch void format(Message message, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		for (ParameterBinding parameters : message.getParameters()) {
			format(parameters, document);
		}
	}
	
	def void indentInterior(EObject obj, extension IFormattableDocument document) {
		for(regionPair : obj.regionFor.keywordPairs("{", "}"))
			regionPair.interior[indent]
	}
}
