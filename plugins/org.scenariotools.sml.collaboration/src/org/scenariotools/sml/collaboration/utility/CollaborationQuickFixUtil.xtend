/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.sml.collaboration.utility

import java.util.ArrayList
import java.util.List
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EReference
import org.scenariotools.sml.Collaboration
import org.scenariotools.sml.Role
import org.scenariotools.sml.Scenario
import org.scenariotools.sml.SmlFactory
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsFactory
import org.scenariotools.sml.expressions.utility.EClassUtil
import org.scenariotools.sml.utility.CollaborationUtil

class CollaborationQuickFixUtil {

	def static addRoleBinding(Role role, Scenario scenario, Role targetRole, EReference feature) {
		
		// Create StructuralFeatureValue for feature
		val structuralFeatureValue = ScenarioExpressionsFactory.eINSTANCE.createStructuralFeatureValue()
		structuralFeatureValue.value = feature
		
		// Create FeatureAccess for targetRole
		val featureAccess = ScenarioExpressionsFactory.eINSTANCE.createFeatureAccess()
		featureAccess.target = targetRole
		featureAccess.value = structuralFeatureValue
		
		// Create BindingExpression
		val expression = SmlFactory.eINSTANCE.createFeatureAccessBindingExpression()
		expression.featureaccess = featureAccess
		
		// Create BindingConstraint
		val binding = SmlFactory.eINSTANCE.createRoleBindingConstraint()
		binding.role = role
		binding.bindingExpression = expression
		
		// Add BindingConstraint to Scenario
		scenario.roleBindings.add(binding)
	}

	def static EReference retrieveEReferenceByName(Role role, String refName) {
		val type = role.type as EClass
		for (EReference ref : EClassUtil.retrieveAllEReferencesOf(type)) {
			if (ref.name.equals(refName)) {
				return ref
			}
		}
		return null
	}

	def static Role retrieveRoleByName(Collaboration collaboration, String roleName) {
		for (Role role: collaboration.roles) {
			if (role.name.equals(roleName)) {
				return role
			}
		}
		return null
	}

	def static List<String> retrieveSolutionsForRoleBindings(Role role) {
		val solutions = new ArrayList<String>()

		val collaboration = CollaborationUtil.getContainingCollaboration(role) as Collaboration
		val roles = new ArrayList<Role>()
		roles.addAll(collaboration.roles)
		roles.remove(role)

		for (Role r : roles) {
			val eclass = r.type as EClass
			for (EReference ref : EClassUtil.retrieveAllEReferencesOf(eclass)) {
				if (ref.getEType == role.type) {
					solutions.add(r.name)
					solutions.add(ref.name)
				}
			}
		}

		return solutions
	}

}