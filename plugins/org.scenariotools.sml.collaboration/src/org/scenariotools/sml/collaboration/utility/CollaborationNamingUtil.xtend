/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.sml.collaboration.utility

import com.google.common.base.Function
import org.eclipse.emf.ecore.EModelElement
import org.eclipse.emf.ecore.EOperation
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.xtext.naming.QualifiedName
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.common.util.BasicEList

class CollaborationNamingUtil {

	static def String getGetterName(EModelElement feature) {
		if (feature instanceof EStructuralFeature)
			return getGetterName(feature)
		else if (feature instanceof EOperation)
			return getGetterName(feature)
	}

	static def String getGetterName(EStructuralFeature feature) {
		if (feature.getEType.name.equals("EBoolean"))
			return 'is' + feature.name.toFirstUpper
		else
			return 'get' + feature.name.toFirstUpper
	}

	static def String getGetterName(EOperation feature) {
		return feature.name
	}

	static def String getSetterName(EStructuralFeature feature) {
		return 'set' + feature.name.toFirstUpper
	}

	static public var Function<EModelElement, QualifiedName> nameComputationForFeatureGetter = [
		QualifiedName::create(it.getGetterName)
	]

	static public var Function<EModelElement, QualifiedName> nameComputationForModelElement = [
		if (it instanceof EStructuralFeature) {
			if (it.upperBound == 1)
				QualifiedName::create(it.setterName)
			else 
				QualifiedName::create(it.name)
		} else if (it instanceof EOperation)
			QualifiedName::create(it.name)
	]

	static public var Function<EPackage, QualifiedName> nameComputationForEPackage = [
		var nameList = new BasicEList<String>
		nameList.add(it.name)
		var epackage = it.eContainer
		while (epackage instanceof EPackage) {
			nameList.add(0, epackage.name)
			epackage = epackage.eContainer
		}
		QualifiedName::create(nameList)
	]

}
