/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.collaboration.serializer;

import com.google.inject.Inject;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AbstractElementAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.GroupAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.TokenAlias;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynNavigable;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynTransition;
import org.eclipse.xtext.serializer.sequencer.AbstractSyntacticSequencer;
import org.scenariotools.sml.collaboration.services.CollaborationGrammarAccess;

@SuppressWarnings("all")
public abstract class AbstractCollaborationSyntacticSequencer extends AbstractSyntacticSequencer {

	protected CollaborationGrammarAccess grammarAccess;
	protected AbstractElementAlias match_BasicExpression_LeftParenthesisKeyword_1_0_a;
	protected AbstractElementAlias match_BasicExpression_LeftParenthesisKeyword_1_0_p;
	protected AbstractElementAlias match_ModalMessage___LeftParenthesisKeyword_8_0_RightParenthesisKeyword_8_2__q;
	protected AbstractElementAlias match_Scenario___BindingsKeyword_6_0_LeftSquareBracketKeyword_6_1_RightSquareBracketKeyword_6_3__q;
	
	@Inject
	protected void init(IGrammarAccess access) {
		grammarAccess = (CollaborationGrammarAccess) access;
		match_BasicExpression_LeftParenthesisKeyword_1_0_a = new TokenAlias(true, true, grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0());
		match_BasicExpression_LeftParenthesisKeyword_1_0_p = new TokenAlias(true, false, grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0());
		match_ModalMessage___LeftParenthesisKeyword_8_0_RightParenthesisKeyword_8_2__q = new GroupAlias(false, true, new TokenAlias(false, false, grammarAccess.getModalMessageAccess().getLeftParenthesisKeyword_8_0()), new TokenAlias(false, false, grammarAccess.getModalMessageAccess().getRightParenthesisKeyword_8_2()));
		match_Scenario___BindingsKeyword_6_0_LeftSquareBracketKeyword_6_1_RightSquareBracketKeyword_6_3__q = new GroupAlias(false, true, new TokenAlias(false, false, grammarAccess.getScenarioAccess().getBindingsKeyword_6_0()), new TokenAlias(false, false, grammarAccess.getScenarioAccess().getLeftSquareBracketKeyword_6_1()), new TokenAlias(false, false, grammarAccess.getScenarioAccess().getRightSquareBracketKeyword_6_3()));
	}
	
	@Override
	protected String getUnassignedRuleCallToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		return "";
	}
	
	
	@Override
	protected void emitUnassignedTokens(EObject semanticObject, ISynTransition transition, INode fromNode, INode toNode) {
		if (transition.getAmbiguousSyntaxes().isEmpty()) return;
		List<INode> transitionNodes = collectNodes(fromNode, toNode);
		for (AbstractElementAlias syntax : transition.getAmbiguousSyntaxes()) {
			List<INode> syntaxNodes = getNodesFor(transitionNodes, syntax);
			if (match_BasicExpression_LeftParenthesisKeyword_1_0_a.equals(syntax))
				emit_BasicExpression_LeftParenthesisKeyword_1_0_a(semanticObject, getLastNavigableState(), syntaxNodes);
			else if (match_BasicExpression_LeftParenthesisKeyword_1_0_p.equals(syntax))
				emit_BasicExpression_LeftParenthesisKeyword_1_0_p(semanticObject, getLastNavigableState(), syntaxNodes);
			else if (match_ModalMessage___LeftParenthesisKeyword_8_0_RightParenthesisKeyword_8_2__q.equals(syntax))
				emit_ModalMessage___LeftParenthesisKeyword_8_0_RightParenthesisKeyword_8_2__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if (match_Scenario___BindingsKeyword_6_0_LeftSquareBracketKeyword_6_1_RightSquareBracketKeyword_6_3__q.equals(syntax))
				emit_Scenario___BindingsKeyword_6_0_LeftSquareBracketKeyword_6_1_RightSquareBracketKeyword_6_3__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else acceptNodes(getLastNavigableState(), syntaxNodes);
		}
	}

	/**
	 * Ambiguous syntax:
	 *     '('*
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) (ambiguity) 'null' (rule start)
	 *     (rule start) (ambiguity) operator='!'
	 *     (rule start) (ambiguity) operator='-'
	 *     (rule start) (ambiguity) target=[EObject|ID]
	 *     (rule start) (ambiguity) type=[EEnum|ID]
	 *     (rule start) (ambiguity) value=BOOL
	 *     (rule start) (ambiguity) value=INT
	 *     (rule start) (ambiguity) value=SIGNEDINT
	 *     (rule start) (ambiguity) value=STRING
	 *     (rule start) (ambiguity) value=[Variable|ID]
	 *     (rule start) (ambiguity) {BinaryOperationExpression.left=}
	 */
	protected void emit_BasicExpression_LeftParenthesisKeyword_1_0_a(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     '('+
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) (ambiguity) operator='!'
	 *     (rule start) (ambiguity) operator='-'
	 *     (rule start) (ambiguity) {BinaryOperationExpression.left=}
	 */
	protected void emit_BasicExpression_LeftParenthesisKeyword_1_0_p(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     ('(' ')')?
	 *
	 * This ambiguous syntax occurs at:
	 *     collectionModification=CollectionModification (ambiguity) (rule end)
	 *     modelElement=[ETypedElement|ID] (ambiguity) (rule end)
	 */
	protected void emit_ModalMessage___LeftParenthesisKeyword_8_0_RightParenthesisKeyword_8_2__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     ('bindings' '[' ']')?
	 *
	 * This ambiguous syntax occurs at:
	 *     contexts+=[EClass|ID] (ambiguity) ownedInteraction=Interaction
	 *     cost=DOUBLE ']' (ambiguity) ownedInteraction=Interaction
	 *     name=ID (ambiguity) ownedInteraction=Interaction
	 *     optimizeCost?='optimize' 'cost' (ambiguity) ownedInteraction=Interaction
	 */
	protected void emit_Scenario___BindingsKeyword_6_0_LeftSquareBracketKeyword_6_1_RightSquareBracketKeyword_6_3__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
}
