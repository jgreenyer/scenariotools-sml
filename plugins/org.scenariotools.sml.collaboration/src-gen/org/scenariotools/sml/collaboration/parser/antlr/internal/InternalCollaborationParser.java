package org.scenariotools.sml.collaboration.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.scenariotools.sml.collaboration.services.CollaborationGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
@SuppressWarnings("all")
public class InternalCollaborationParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_DOUBLE", "RULE_STRING", "RULE_INT", "RULE_SIGNEDINT", "RULE_BOOL", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'domain'", "'contexts'", "'collaboration'", "'{'", "'}'", "'.'", "'static'", "'dynamic'", "'multi'", "'role'", "'singular'", "'scenario'", "'optimize'", "'cost'", "'['", "']'", "'context'", "','", "'bindings'", "'='", "'strict'", "'monitored'", "'->'", "'('", "')'", "'*'", "'bind'", "'alternative'", "'or'", "'while'", "'parallel'", "'and'", "'wait'", "'eventually'", "'interrupt'", "'violation'", "'timed'", "'constraints'", "'consider'", "'ignore'", "'forbidden'", "'import'", "';'", "'var'", "'clock'", "'reset clock'", "'=>'", "'||'", "'&&'", "'=='", "'!='", "'<'", "'>'", "'<='", "'>='", "'+'", "'-'", "'/'", "'!'", "':'", "'null'", "'assumption'", "'guarantee'", "'existential'", "'requested'", "'urgent'", "'committed'", "'any'", "'contains'", "'containsAll'", "'first'", "'get'", "'isEmpty'", "'last'", "'size'", "'add'", "'addToFront'", "'clear'", "'remove'"
    };
    public static final int T__50=50;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=4;
    public static final int RULE_INT=7;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=10;
    public static final int T__67=67;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__64=64;
    public static final int T__65=65;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_BOOL=9;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__91=91;
    public static final int T__92=92;
    public static final int T__90=90;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__14=14;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_SIGNEDINT=8;
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=11;
    public static final int RULE_DOUBLE=5;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__74=74;
    public static final int T__75=75;
    public static final int T__76=76;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int RULE_WS=12;
    public static final int RULE_ANY_OTHER=13;
    public static final int T__88=88;
    public static final int T__89=89;
    public static final int T__84=84;
    public static final int T__85=85;
    public static final int T__86=86;
    public static final int T__87=87;

    // delegates
    // delegators


        public InternalCollaborationParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalCollaborationParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalCollaborationParser.tokenNames; }
    public String getGrammarFileName() { return "InternalCollaboration.g"; }



     	private CollaborationGrammarAccess grammarAccess;
     	
        public InternalCollaborationParser(TokenStream input, CollaborationGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Collaboration";	
       	}
       	
       	@Override
       	protected CollaborationGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleCollaboration"
    // InternalCollaboration.g:77:1: entryRuleCollaboration returns [EObject current=null] : iv_ruleCollaboration= ruleCollaboration EOF ;
    public final EObject entryRuleCollaboration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCollaboration = null;


        try {
            // InternalCollaboration.g:78:2: (iv_ruleCollaboration= ruleCollaboration EOF )
            // InternalCollaboration.g:79:2: iv_ruleCollaboration= ruleCollaboration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCollaborationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCollaboration=ruleCollaboration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCollaboration; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCollaboration"


    // $ANTLR start "ruleCollaboration"
    // InternalCollaboration.g:86:1: ruleCollaboration returns [EObject current=null] : ( ( (lv_imports_0_0= ruleImport ) )* (otherlv_1= 'domain' ( ( ruleFQN ) ) )* (otherlv_3= 'contexts' ( ( ruleFQN ) ) )* otherlv_5= 'collaboration' ( (lv_name_6_0= RULE_ID ) ) otherlv_7= '{' ( (lv_roles_8_0= ruleRole ) )* ( (lv_scenarios_9_0= ruleScenario ) )* otherlv_10= '}' ) ;
    public final EObject ruleCollaboration() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token lv_name_6_0=null;
        Token otherlv_7=null;
        Token otherlv_10=null;
        EObject lv_imports_0_0 = null;

        EObject lv_roles_8_0 = null;

        EObject lv_scenarios_9_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:89:28: ( ( ( (lv_imports_0_0= ruleImport ) )* (otherlv_1= 'domain' ( ( ruleFQN ) ) )* (otherlv_3= 'contexts' ( ( ruleFQN ) ) )* otherlv_5= 'collaboration' ( (lv_name_6_0= RULE_ID ) ) otherlv_7= '{' ( (lv_roles_8_0= ruleRole ) )* ( (lv_scenarios_9_0= ruleScenario ) )* otherlv_10= '}' ) )
            // InternalCollaboration.g:90:1: ( ( (lv_imports_0_0= ruleImport ) )* (otherlv_1= 'domain' ( ( ruleFQN ) ) )* (otherlv_3= 'contexts' ( ( ruleFQN ) ) )* otherlv_5= 'collaboration' ( (lv_name_6_0= RULE_ID ) ) otherlv_7= '{' ( (lv_roles_8_0= ruleRole ) )* ( (lv_scenarios_9_0= ruleScenario ) )* otherlv_10= '}' )
            {
            // InternalCollaboration.g:90:1: ( ( (lv_imports_0_0= ruleImport ) )* (otherlv_1= 'domain' ( ( ruleFQN ) ) )* (otherlv_3= 'contexts' ( ( ruleFQN ) ) )* otherlv_5= 'collaboration' ( (lv_name_6_0= RULE_ID ) ) otherlv_7= '{' ( (lv_roles_8_0= ruleRole ) )* ( (lv_scenarios_9_0= ruleScenario ) )* otherlv_10= '}' )
            // InternalCollaboration.g:90:2: ( (lv_imports_0_0= ruleImport ) )* (otherlv_1= 'domain' ( ( ruleFQN ) ) )* (otherlv_3= 'contexts' ( ( ruleFQN ) ) )* otherlv_5= 'collaboration' ( (lv_name_6_0= RULE_ID ) ) otherlv_7= '{' ( (lv_roles_8_0= ruleRole ) )* ( (lv_scenarios_9_0= ruleScenario ) )* otherlv_10= '}'
            {
            // InternalCollaboration.g:90:2: ( (lv_imports_0_0= ruleImport ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==55) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalCollaboration.g:91:1: (lv_imports_0_0= ruleImport )
            	    {
            	    // InternalCollaboration.g:91:1: (lv_imports_0_0= ruleImport )
            	    // InternalCollaboration.g:92:3: lv_imports_0_0= ruleImport
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getCollaborationAccess().getImportsImportParserRuleCall_0_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_3);
            	    lv_imports_0_0=ruleImport();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getCollaborationRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"imports",
            	              		lv_imports_0_0, 
            	              		"org.scenariotools.sml.expressions.ScenarioExpressions.Import");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalCollaboration.g:108:3: (otherlv_1= 'domain' ( ( ruleFQN ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==14) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalCollaboration.g:108:5: otherlv_1= 'domain' ( ( ruleFQN ) )
            	    {
            	    otherlv_1=(Token)match(input,14,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_1, grammarAccess.getCollaborationAccess().getDomainKeyword_1_0());
            	          
            	    }
            	    // InternalCollaboration.g:112:1: ( ( ruleFQN ) )
            	    // InternalCollaboration.g:113:1: ( ruleFQN )
            	    {
            	    // InternalCollaboration.g:113:1: ( ruleFQN )
            	    // InternalCollaboration.g:114:3: ruleFQN
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getCollaborationRule());
            	      	        }
            	              
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getCollaborationAccess().getDomainsEPackageCrossReference_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_5);
            	    ruleFQN();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {
            	       
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // InternalCollaboration.g:127:4: (otherlv_3= 'contexts' ( ( ruleFQN ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==15) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalCollaboration.g:127:6: otherlv_3= 'contexts' ( ( ruleFQN ) )
            	    {
            	    otherlv_3=(Token)match(input,15,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getCollaborationAccess().getContextsKeyword_2_0());
            	          
            	    }
            	    // InternalCollaboration.g:131:1: ( ( ruleFQN ) )
            	    // InternalCollaboration.g:132:1: ( ruleFQN )
            	    {
            	    // InternalCollaboration.g:132:1: ( ruleFQN )
            	    // InternalCollaboration.g:133:3: ruleFQN
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getCollaborationRule());
            	      	        }
            	              
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getCollaborationAccess().getContextsEPackageCrossReference_2_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_6);
            	    ruleFQN();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {
            	       
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_5=(Token)match(input,16,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getCollaborationAccess().getCollaborationKeyword_3());
                  
            }
            // InternalCollaboration.g:150:1: ( (lv_name_6_0= RULE_ID ) )
            // InternalCollaboration.g:151:1: (lv_name_6_0= RULE_ID )
            {
            // InternalCollaboration.g:151:1: (lv_name_6_0= RULE_ID )
            // InternalCollaboration.g:152:3: lv_name_6_0= RULE_ID
            {
            lv_name_6_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_6_0, grammarAccess.getCollaborationAccess().getNameIDTerminalRuleCall_4_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getCollaborationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_6_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            otherlv_7=(Token)match(input,17,FollowSets000.FOLLOW_8); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_7, grammarAccess.getCollaborationAccess().getLeftCurlyBracketKeyword_5());
                  
            }
            // InternalCollaboration.g:172:1: ( (lv_roles_8_0= ruleRole ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>=20 && LA4_0<=21)) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalCollaboration.g:173:1: (lv_roles_8_0= ruleRole )
            	    {
            	    // InternalCollaboration.g:173:1: (lv_roles_8_0= ruleRole )
            	    // InternalCollaboration.g:174:3: lv_roles_8_0= ruleRole
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getCollaborationAccess().getRolesRoleParserRuleCall_6_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_8);
            	    lv_roles_8_0=ruleRole();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getCollaborationRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"roles",
            	              		lv_roles_8_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.Role");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            // InternalCollaboration.g:190:3: ( (lv_scenarios_9_0= ruleScenario ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==24||(LA5_0>=75 && LA5_0<=77)) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalCollaboration.g:191:1: (lv_scenarios_9_0= ruleScenario )
            	    {
            	    // InternalCollaboration.g:191:1: (lv_scenarios_9_0= ruleScenario )
            	    // InternalCollaboration.g:192:3: lv_scenarios_9_0= ruleScenario
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getCollaborationAccess().getScenariosScenarioParserRuleCall_7_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_9);
            	    lv_scenarios_9_0=ruleScenario();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getCollaborationRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"scenarios",
            	              		lv_scenarios_9_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.Scenario");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            otherlv_10=(Token)match(input,18,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_10, grammarAccess.getCollaborationAccess().getRightCurlyBracketKeyword_8());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollaboration"


    // $ANTLR start "entryRuleFQN"
    // InternalCollaboration.g:220:1: entryRuleFQN returns [String current=null] : iv_ruleFQN= ruleFQN EOF ;
    public final String entryRuleFQN() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFQN = null;


        try {
            // InternalCollaboration.g:221:2: (iv_ruleFQN= ruleFQN EOF )
            // InternalCollaboration.g:222:2: iv_ruleFQN= ruleFQN EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFQNRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleFQN=ruleFQN();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFQN.getText(); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFQN"


    // $ANTLR start "ruleFQN"
    // InternalCollaboration.g:229:1: ruleFQN returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleFQN() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:232:28: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalCollaboration.g:233:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalCollaboration.g:233:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalCollaboration.g:233:6: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_10); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current.merge(this_ID_0);
                  
            }
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_ID_0, grammarAccess.getFQNAccess().getIDTerminalRuleCall_0()); 
                  
            }
            // InternalCollaboration.g:240:1: (kw= '.' this_ID_2= RULE_ID )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==19) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalCollaboration.g:241:2: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,19,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	              current.merge(kw);
            	              newLeafNode(kw, grammarAccess.getFQNAccess().getFullStopKeyword_1_0()); 
            	          
            	    }
            	    this_ID_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_10); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		current.merge(this_ID_2);
            	          
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	          newLeafNode(this_ID_2, grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1()); 
            	          
            	    }

            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFQN"


    // $ANTLR start "entryRuleRole"
    // InternalCollaboration.g:261:1: entryRuleRole returns [EObject current=null] : iv_ruleRole= ruleRole EOF ;
    public final EObject entryRuleRole() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRole = null;


        try {
            // InternalCollaboration.g:262:2: (iv_ruleRole= ruleRole EOF )
            // InternalCollaboration.g:263:2: iv_ruleRole= ruleRole EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRoleRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRole=ruleRole();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRole; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRole"


    // $ANTLR start "ruleRole"
    // InternalCollaboration.g:270:1: ruleRole returns [EObject current=null] : ( ( ( (lv_static_0_0= 'static' ) ) | (otherlv_1= 'dynamic' ( (lv_multiRole_2_0= 'multi' ) )? ) ) otherlv_3= 'role' ( (otherlv_4= RULE_ID ) ) ( (lv_name_5_0= RULE_ID ) ) ) ;
    public final EObject ruleRole() throws RecognitionException {
        EObject current = null;

        Token lv_static_0_0=null;
        Token otherlv_1=null;
        Token lv_multiRole_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_name_5_0=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:273:28: ( ( ( ( (lv_static_0_0= 'static' ) ) | (otherlv_1= 'dynamic' ( (lv_multiRole_2_0= 'multi' ) )? ) ) otherlv_3= 'role' ( (otherlv_4= RULE_ID ) ) ( (lv_name_5_0= RULE_ID ) ) ) )
            // InternalCollaboration.g:274:1: ( ( ( (lv_static_0_0= 'static' ) ) | (otherlv_1= 'dynamic' ( (lv_multiRole_2_0= 'multi' ) )? ) ) otherlv_3= 'role' ( (otherlv_4= RULE_ID ) ) ( (lv_name_5_0= RULE_ID ) ) )
            {
            // InternalCollaboration.g:274:1: ( ( ( (lv_static_0_0= 'static' ) ) | (otherlv_1= 'dynamic' ( (lv_multiRole_2_0= 'multi' ) )? ) ) otherlv_3= 'role' ( (otherlv_4= RULE_ID ) ) ( (lv_name_5_0= RULE_ID ) ) )
            // InternalCollaboration.g:274:2: ( ( (lv_static_0_0= 'static' ) ) | (otherlv_1= 'dynamic' ( (lv_multiRole_2_0= 'multi' ) )? ) ) otherlv_3= 'role' ( (otherlv_4= RULE_ID ) ) ( (lv_name_5_0= RULE_ID ) )
            {
            // InternalCollaboration.g:274:2: ( ( (lv_static_0_0= 'static' ) ) | (otherlv_1= 'dynamic' ( (lv_multiRole_2_0= 'multi' ) )? ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==20) ) {
                alt8=1;
            }
            else if ( (LA8_0==21) ) {
                alt8=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalCollaboration.g:274:3: ( (lv_static_0_0= 'static' ) )
                    {
                    // InternalCollaboration.g:274:3: ( (lv_static_0_0= 'static' ) )
                    // InternalCollaboration.g:275:1: (lv_static_0_0= 'static' )
                    {
                    // InternalCollaboration.g:275:1: (lv_static_0_0= 'static' )
                    // InternalCollaboration.g:276:3: lv_static_0_0= 'static'
                    {
                    lv_static_0_0=(Token)match(input,20,FollowSets000.FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_static_0_0, grammarAccess.getRoleAccess().getStaticStaticKeyword_0_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getRoleRule());
                      	        }
                             		setWithLastConsumed(current, "static", true, "static");
                      	    
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:290:6: (otherlv_1= 'dynamic' ( (lv_multiRole_2_0= 'multi' ) )? )
                    {
                    // InternalCollaboration.g:290:6: (otherlv_1= 'dynamic' ( (lv_multiRole_2_0= 'multi' ) )? )
                    // InternalCollaboration.g:290:8: otherlv_1= 'dynamic' ( (lv_multiRole_2_0= 'multi' ) )?
                    {
                    otherlv_1=(Token)match(input,21,FollowSets000.FOLLOW_12); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getRoleAccess().getDynamicKeyword_0_1_0());
                          
                    }
                    // InternalCollaboration.g:294:1: ( (lv_multiRole_2_0= 'multi' ) )?
                    int alt7=2;
                    int LA7_0 = input.LA(1);

                    if ( (LA7_0==22) ) {
                        alt7=1;
                    }
                    switch (alt7) {
                        case 1 :
                            // InternalCollaboration.g:295:1: (lv_multiRole_2_0= 'multi' )
                            {
                            // InternalCollaboration.g:295:1: (lv_multiRole_2_0= 'multi' )
                            // InternalCollaboration.g:296:3: lv_multiRole_2_0= 'multi'
                            {
                            lv_multiRole_2_0=(Token)match(input,22,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_multiRole_2_0, grammarAccess.getRoleAccess().getMultiRoleMultiKeyword_0_1_1_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRoleRule());
                              	        }
                                     		setWithLastConsumed(current, "multiRole", true, "multi");
                              	    
                            }

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,23,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getRoleAccess().getRoleKeyword_1());
                  
            }
            // InternalCollaboration.g:313:1: ( (otherlv_4= RULE_ID ) )
            // InternalCollaboration.g:314:1: (otherlv_4= RULE_ID )
            {
            // InternalCollaboration.g:314:1: (otherlv_4= RULE_ID )
            // InternalCollaboration.g:315:3: otherlv_4= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getRoleRule());
              	        }
                      
            }
            otherlv_4=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_4, grammarAccess.getRoleAccess().getTypeEClassCrossReference_2_0()); 
              	
            }

            }


            }

            // InternalCollaboration.g:326:2: ( (lv_name_5_0= RULE_ID ) )
            // InternalCollaboration.g:327:1: (lv_name_5_0= RULE_ID )
            {
            // InternalCollaboration.g:327:1: (lv_name_5_0= RULE_ID )
            // InternalCollaboration.g:328:3: lv_name_5_0= RULE_ID
            {
            lv_name_5_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_5_0, grammarAccess.getRoleAccess().getNameIDTerminalRuleCall_3_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getRoleRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_5_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRole"


    // $ANTLR start "entryRuleScenario"
    // InternalCollaboration.g:352:1: entryRuleScenario returns [EObject current=null] : iv_ruleScenario= ruleScenario EOF ;
    public final EObject entryRuleScenario() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleScenario = null;


        try {
            // InternalCollaboration.g:353:2: (iv_ruleScenario= ruleScenario EOF )
            // InternalCollaboration.g:354:2: iv_ruleScenario= ruleScenario EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getScenarioRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleScenario=ruleScenario();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleScenario; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleScenario"


    // $ANTLR start "ruleScenario"
    // InternalCollaboration.g:361:1: ruleScenario returns [EObject current=null] : ( ( (lv_singular_0_0= 'singular' ) )? ( (lv_kind_1_0= ruleScenarioKind ) ) otherlv_2= 'scenario' ( (lv_name_3_0= RULE_ID ) ) ( ( ( (lv_optimizeCost_4_0= 'optimize' ) ) otherlv_5= 'cost' ) | (otherlv_6= 'cost' otherlv_7= '[' ( (lv_cost_8_0= RULE_DOUBLE ) ) otherlv_9= ']' ) )? (otherlv_10= 'context' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* )? (otherlv_14= 'bindings' otherlv_15= '[' ( (lv_roleBindings_16_0= ruleRoleBindingConstraint ) )* otherlv_17= ']' )? ( (lv_ownedInteraction_18_0= ruleInteraction ) ) ) ;
    public final EObject ruleScenario() throws RecognitionException {
        EObject current = null;

        Token lv_singular_0_0=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token lv_optimizeCost_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token lv_cost_8_0=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Enumerator lv_kind_1_0 = null;

        EObject lv_roleBindings_16_0 = null;

        EObject lv_ownedInteraction_18_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:364:28: ( ( ( (lv_singular_0_0= 'singular' ) )? ( (lv_kind_1_0= ruleScenarioKind ) ) otherlv_2= 'scenario' ( (lv_name_3_0= RULE_ID ) ) ( ( ( (lv_optimizeCost_4_0= 'optimize' ) ) otherlv_5= 'cost' ) | (otherlv_6= 'cost' otherlv_7= '[' ( (lv_cost_8_0= RULE_DOUBLE ) ) otherlv_9= ']' ) )? (otherlv_10= 'context' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* )? (otherlv_14= 'bindings' otherlv_15= '[' ( (lv_roleBindings_16_0= ruleRoleBindingConstraint ) )* otherlv_17= ']' )? ( (lv_ownedInteraction_18_0= ruleInteraction ) ) ) )
            // InternalCollaboration.g:365:1: ( ( (lv_singular_0_0= 'singular' ) )? ( (lv_kind_1_0= ruleScenarioKind ) ) otherlv_2= 'scenario' ( (lv_name_3_0= RULE_ID ) ) ( ( ( (lv_optimizeCost_4_0= 'optimize' ) ) otherlv_5= 'cost' ) | (otherlv_6= 'cost' otherlv_7= '[' ( (lv_cost_8_0= RULE_DOUBLE ) ) otherlv_9= ']' ) )? (otherlv_10= 'context' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* )? (otherlv_14= 'bindings' otherlv_15= '[' ( (lv_roleBindings_16_0= ruleRoleBindingConstraint ) )* otherlv_17= ']' )? ( (lv_ownedInteraction_18_0= ruleInteraction ) ) )
            {
            // InternalCollaboration.g:365:1: ( ( (lv_singular_0_0= 'singular' ) )? ( (lv_kind_1_0= ruleScenarioKind ) ) otherlv_2= 'scenario' ( (lv_name_3_0= RULE_ID ) ) ( ( ( (lv_optimizeCost_4_0= 'optimize' ) ) otherlv_5= 'cost' ) | (otherlv_6= 'cost' otherlv_7= '[' ( (lv_cost_8_0= RULE_DOUBLE ) ) otherlv_9= ']' ) )? (otherlv_10= 'context' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* )? (otherlv_14= 'bindings' otherlv_15= '[' ( (lv_roleBindings_16_0= ruleRoleBindingConstraint ) )* otherlv_17= ']' )? ( (lv_ownedInteraction_18_0= ruleInteraction ) ) )
            // InternalCollaboration.g:365:2: ( (lv_singular_0_0= 'singular' ) )? ( (lv_kind_1_0= ruleScenarioKind ) ) otherlv_2= 'scenario' ( (lv_name_3_0= RULE_ID ) ) ( ( ( (lv_optimizeCost_4_0= 'optimize' ) ) otherlv_5= 'cost' ) | (otherlv_6= 'cost' otherlv_7= '[' ( (lv_cost_8_0= RULE_DOUBLE ) ) otherlv_9= ']' ) )? (otherlv_10= 'context' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* )? (otherlv_14= 'bindings' otherlv_15= '[' ( (lv_roleBindings_16_0= ruleRoleBindingConstraint ) )* otherlv_17= ']' )? ( (lv_ownedInteraction_18_0= ruleInteraction ) )
            {
            // InternalCollaboration.g:365:2: ( (lv_singular_0_0= 'singular' ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==24) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalCollaboration.g:366:1: (lv_singular_0_0= 'singular' )
                    {
                    // InternalCollaboration.g:366:1: (lv_singular_0_0= 'singular' )
                    // InternalCollaboration.g:367:3: lv_singular_0_0= 'singular'
                    {
                    lv_singular_0_0=(Token)match(input,24,FollowSets000.FOLLOW_13); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_singular_0_0, grammarAccess.getScenarioAccess().getSingularSingularKeyword_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getScenarioRule());
                      	        }
                             		setWithLastConsumed(current, "singular", true, "singular");
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalCollaboration.g:380:3: ( (lv_kind_1_0= ruleScenarioKind ) )
            // InternalCollaboration.g:381:1: (lv_kind_1_0= ruleScenarioKind )
            {
            // InternalCollaboration.g:381:1: (lv_kind_1_0= ruleScenarioKind )
            // InternalCollaboration.g:382:3: lv_kind_1_0= ruleScenarioKind
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getScenarioAccess().getKindScenarioKindEnumRuleCall_1_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_14);
            lv_kind_1_0=ruleScenarioKind();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getScenarioRule());
              	        }
                     		set(
                     			current, 
                     			"kind",
                      		lv_kind_1_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ScenarioKind");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,25,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getScenarioAccess().getScenarioKeyword_2());
                  
            }
            // InternalCollaboration.g:402:1: ( (lv_name_3_0= RULE_ID ) )
            // InternalCollaboration.g:403:1: (lv_name_3_0= RULE_ID )
            {
            // InternalCollaboration.g:403:1: (lv_name_3_0= RULE_ID )
            // InternalCollaboration.g:404:3: lv_name_3_0= RULE_ID
            {
            lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_15); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_3_0, grammarAccess.getScenarioAccess().getNameIDTerminalRuleCall_3_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getScenarioRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_3_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalCollaboration.g:420:2: ( ( ( (lv_optimizeCost_4_0= 'optimize' ) ) otherlv_5= 'cost' ) | (otherlv_6= 'cost' otherlv_7= '[' ( (lv_cost_8_0= RULE_DOUBLE ) ) otherlv_9= ']' ) )?
            int alt10=3;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==26) ) {
                alt10=1;
            }
            else if ( (LA10_0==27) ) {
                alt10=2;
            }
            switch (alt10) {
                case 1 :
                    // InternalCollaboration.g:420:3: ( ( (lv_optimizeCost_4_0= 'optimize' ) ) otherlv_5= 'cost' )
                    {
                    // InternalCollaboration.g:420:3: ( ( (lv_optimizeCost_4_0= 'optimize' ) ) otherlv_5= 'cost' )
                    // InternalCollaboration.g:420:4: ( (lv_optimizeCost_4_0= 'optimize' ) ) otherlv_5= 'cost'
                    {
                    // InternalCollaboration.g:420:4: ( (lv_optimizeCost_4_0= 'optimize' ) )
                    // InternalCollaboration.g:421:1: (lv_optimizeCost_4_0= 'optimize' )
                    {
                    // InternalCollaboration.g:421:1: (lv_optimizeCost_4_0= 'optimize' )
                    // InternalCollaboration.g:422:3: lv_optimizeCost_4_0= 'optimize'
                    {
                    lv_optimizeCost_4_0=(Token)match(input,26,FollowSets000.FOLLOW_16); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_optimizeCost_4_0, grammarAccess.getScenarioAccess().getOptimizeCostOptimizeKeyword_4_0_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getScenarioRule());
                      	        }
                             		setWithLastConsumed(current, "optimizeCost", true, "optimize");
                      	    
                    }

                    }


                    }

                    otherlv_5=(Token)match(input,27,FollowSets000.FOLLOW_15); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_5, grammarAccess.getScenarioAccess().getCostKeyword_4_0_1());
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:440:6: (otherlv_6= 'cost' otherlv_7= '[' ( (lv_cost_8_0= RULE_DOUBLE ) ) otherlv_9= ']' )
                    {
                    // InternalCollaboration.g:440:6: (otherlv_6= 'cost' otherlv_7= '[' ( (lv_cost_8_0= RULE_DOUBLE ) ) otherlv_9= ']' )
                    // InternalCollaboration.g:440:8: otherlv_6= 'cost' otherlv_7= '[' ( (lv_cost_8_0= RULE_DOUBLE ) ) otherlv_9= ']'
                    {
                    otherlv_6=(Token)match(input,27,FollowSets000.FOLLOW_17); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_6, grammarAccess.getScenarioAccess().getCostKeyword_4_1_0());
                          
                    }
                    otherlv_7=(Token)match(input,28,FollowSets000.FOLLOW_18); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_7, grammarAccess.getScenarioAccess().getLeftSquareBracketKeyword_4_1_1());
                          
                    }
                    // InternalCollaboration.g:448:1: ( (lv_cost_8_0= RULE_DOUBLE ) )
                    // InternalCollaboration.g:449:1: (lv_cost_8_0= RULE_DOUBLE )
                    {
                    // InternalCollaboration.g:449:1: (lv_cost_8_0= RULE_DOUBLE )
                    // InternalCollaboration.g:450:3: lv_cost_8_0= RULE_DOUBLE
                    {
                    lv_cost_8_0=(Token)match(input,RULE_DOUBLE,FollowSets000.FOLLOW_19); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_cost_8_0, grammarAccess.getScenarioAccess().getCostDOUBLETerminalRuleCall_4_1_2_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getScenarioRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"cost",
                              		lv_cost_8_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.DOUBLE");
                      	    
                    }

                    }


                    }

                    otherlv_9=(Token)match(input,29,FollowSets000.FOLLOW_15); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_9, grammarAccess.getScenarioAccess().getRightSquareBracketKeyword_4_1_3());
                          
                    }

                    }


                    }
                    break;

            }

            // InternalCollaboration.g:470:4: (otherlv_10= 'context' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==30) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalCollaboration.g:470:6: otherlv_10= 'context' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )*
                    {
                    otherlv_10=(Token)match(input,30,FollowSets000.FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_10, grammarAccess.getScenarioAccess().getContextKeyword_5_0());
                          
                    }
                    // InternalCollaboration.g:474:1: ( (otherlv_11= RULE_ID ) )
                    // InternalCollaboration.g:475:1: (otherlv_11= RULE_ID )
                    {
                    // InternalCollaboration.g:475:1: (otherlv_11= RULE_ID )
                    // InternalCollaboration.g:476:3: otherlv_11= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      			if (current==null) {
                      	            current = createModelElement(grammarAccess.getScenarioRule());
                      	        }
                              
                    }
                    otherlv_11=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_20); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		newLeafNode(otherlv_11, grammarAccess.getScenarioAccess().getContextsEClassCrossReference_5_1_0()); 
                      	
                    }

                    }


                    }

                    // InternalCollaboration.g:487:2: (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )*
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( (LA11_0==31) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // InternalCollaboration.g:487:4: otherlv_12= ',' ( (otherlv_13= RULE_ID ) )
                    	    {
                    	    otherlv_12=(Token)match(input,31,FollowSets000.FOLLOW_4); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_12, grammarAccess.getScenarioAccess().getCommaKeyword_5_2_0());
                    	          
                    	    }
                    	    // InternalCollaboration.g:491:1: ( (otherlv_13= RULE_ID ) )
                    	    // InternalCollaboration.g:492:1: (otherlv_13= RULE_ID )
                    	    {
                    	    // InternalCollaboration.g:492:1: (otherlv_13= RULE_ID )
                    	    // InternalCollaboration.g:493:3: otherlv_13= RULE_ID
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      			if (current==null) {
                    	      	            current = createModelElement(grammarAccess.getScenarioRule());
                    	      	        }
                    	              
                    	    }
                    	    otherlv_13=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_20); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      		newLeafNode(otherlv_13, grammarAccess.getScenarioAccess().getContextsEClassCrossReference_5_2_1_0()); 
                    	      	
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop11;
                        }
                    } while (true);


                    }
                    break;

            }

            // InternalCollaboration.g:504:6: (otherlv_14= 'bindings' otherlv_15= '[' ( (lv_roleBindings_16_0= ruleRoleBindingConstraint ) )* otherlv_17= ']' )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==32) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalCollaboration.g:504:8: otherlv_14= 'bindings' otherlv_15= '[' ( (lv_roleBindings_16_0= ruleRoleBindingConstraint ) )* otherlv_17= ']'
                    {
                    otherlv_14=(Token)match(input,32,FollowSets000.FOLLOW_17); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_14, grammarAccess.getScenarioAccess().getBindingsKeyword_6_0());
                          
                    }
                    otherlv_15=(Token)match(input,28,FollowSets000.FOLLOW_21); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_15, grammarAccess.getScenarioAccess().getLeftSquareBracketKeyword_6_1());
                          
                    }
                    // InternalCollaboration.g:512:1: ( (lv_roleBindings_16_0= ruleRoleBindingConstraint ) )*
                    loop13:
                    do {
                        int alt13=2;
                        int LA13_0 = input.LA(1);

                        if ( (LA13_0==RULE_ID) ) {
                            alt13=1;
                        }


                        switch (alt13) {
                    	case 1 :
                    	    // InternalCollaboration.g:513:1: (lv_roleBindings_16_0= ruleRoleBindingConstraint )
                    	    {
                    	    // InternalCollaboration.g:513:1: (lv_roleBindings_16_0= ruleRoleBindingConstraint )
                    	    // InternalCollaboration.g:514:3: lv_roleBindings_16_0= ruleRoleBindingConstraint
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getScenarioAccess().getRoleBindingsRoleBindingConstraintParserRuleCall_6_2_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_21);
                    	    lv_roleBindings_16_0=ruleRoleBindingConstraint();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getScenarioRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"roleBindings",
                    	              		lv_roleBindings_16_0, 
                    	              		"org.scenariotools.sml.collaboration.Collaboration.RoleBindingConstraint");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop13;
                        }
                    } while (true);

                    otherlv_17=(Token)match(input,29,FollowSets000.FOLLOW_15); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_17, grammarAccess.getScenarioAccess().getRightSquareBracketKeyword_6_3());
                          
                    }

                    }
                    break;

            }

            // InternalCollaboration.g:534:3: ( (lv_ownedInteraction_18_0= ruleInteraction ) )
            // InternalCollaboration.g:535:1: (lv_ownedInteraction_18_0= ruleInteraction )
            {
            // InternalCollaboration.g:535:1: (lv_ownedInteraction_18_0= ruleInteraction )
            // InternalCollaboration.g:536:3: lv_ownedInteraction_18_0= ruleInteraction
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getScenarioAccess().getOwnedInteractionInteractionParserRuleCall_7_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_ownedInteraction_18_0=ruleInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getScenarioRule());
              	        }
                     		set(
                     			current, 
                     			"ownedInteraction",
                      		lv_ownedInteraction_18_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleScenario"


    // $ANTLR start "entryRuleRoleBindingConstraint"
    // InternalCollaboration.g:560:1: entryRuleRoleBindingConstraint returns [EObject current=null] : iv_ruleRoleBindingConstraint= ruleRoleBindingConstraint EOF ;
    public final EObject entryRuleRoleBindingConstraint() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRoleBindingConstraint = null;


        try {
            // InternalCollaboration.g:561:2: (iv_ruleRoleBindingConstraint= ruleRoleBindingConstraint EOF )
            // InternalCollaboration.g:562:2: iv_ruleRoleBindingConstraint= ruleRoleBindingConstraint EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRoleBindingConstraintRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRoleBindingConstraint=ruleRoleBindingConstraint();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRoleBindingConstraint; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoleBindingConstraint"


    // $ANTLR start "ruleRoleBindingConstraint"
    // InternalCollaboration.g:569:1: ruleRoleBindingConstraint returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_bindingExpression_2_0= ruleBindingExpression ) ) ) ;
    public final EObject ruleRoleBindingConstraint() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_bindingExpression_2_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:572:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_bindingExpression_2_0= ruleBindingExpression ) ) ) )
            // InternalCollaboration.g:573:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_bindingExpression_2_0= ruleBindingExpression ) ) )
            {
            // InternalCollaboration.g:573:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_bindingExpression_2_0= ruleBindingExpression ) ) )
            // InternalCollaboration.g:573:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_bindingExpression_2_0= ruleBindingExpression ) )
            {
            // InternalCollaboration.g:573:2: ( (otherlv_0= RULE_ID ) )
            // InternalCollaboration.g:574:1: (otherlv_0= RULE_ID )
            {
            // InternalCollaboration.g:574:1: (otherlv_0= RULE_ID )
            // InternalCollaboration.g:575:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getRoleBindingConstraintRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_22); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getRoleBindingConstraintAccess().getRoleRoleCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,33,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getRoleBindingConstraintAccess().getEqualsSignKeyword_1());
                  
            }
            // InternalCollaboration.g:590:1: ( (lv_bindingExpression_2_0= ruleBindingExpression ) )
            // InternalCollaboration.g:591:1: (lv_bindingExpression_2_0= ruleBindingExpression )
            {
            // InternalCollaboration.g:591:1: (lv_bindingExpression_2_0= ruleBindingExpression )
            // InternalCollaboration.g:592:3: lv_bindingExpression_2_0= ruleBindingExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getRoleBindingConstraintAccess().getBindingExpressionBindingExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_bindingExpression_2_0=ruleBindingExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getRoleBindingConstraintRule());
              	        }
                     		set(
                     			current, 
                     			"bindingExpression",
                      		lv_bindingExpression_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.BindingExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoleBindingConstraint"


    // $ANTLR start "entryRuleBindingExpression"
    // InternalCollaboration.g:616:1: entryRuleBindingExpression returns [EObject current=null] : iv_ruleBindingExpression= ruleBindingExpression EOF ;
    public final EObject entryRuleBindingExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBindingExpression = null;


        try {
            // InternalCollaboration.g:617:2: (iv_ruleBindingExpression= ruleBindingExpression EOF )
            // InternalCollaboration.g:618:2: iv_ruleBindingExpression= ruleBindingExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBindingExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBindingExpression=ruleBindingExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBindingExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBindingExpression"


    // $ANTLR start "ruleBindingExpression"
    // InternalCollaboration.g:625:1: ruleBindingExpression returns [EObject current=null] : this_FeatureAccessBindingExpression_0= ruleFeatureAccessBindingExpression ;
    public final EObject ruleBindingExpression() throws RecognitionException {
        EObject current = null;

        EObject this_FeatureAccessBindingExpression_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:628:28: (this_FeatureAccessBindingExpression_0= ruleFeatureAccessBindingExpression )
            // InternalCollaboration.g:630:5: this_FeatureAccessBindingExpression_0= ruleFeatureAccessBindingExpression
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getBindingExpressionAccess().getFeatureAccessBindingExpressionParserRuleCall()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_2);
            this_FeatureAccessBindingExpression_0=ruleFeatureAccessBindingExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_FeatureAccessBindingExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBindingExpression"


    // $ANTLR start "entryRuleFeatureAccessBindingExpression"
    // InternalCollaboration.g:646:1: entryRuleFeatureAccessBindingExpression returns [EObject current=null] : iv_ruleFeatureAccessBindingExpression= ruleFeatureAccessBindingExpression EOF ;
    public final EObject entryRuleFeatureAccessBindingExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureAccessBindingExpression = null;


        try {
            // InternalCollaboration.g:647:2: (iv_ruleFeatureAccessBindingExpression= ruleFeatureAccessBindingExpression EOF )
            // InternalCollaboration.g:648:2: iv_ruleFeatureAccessBindingExpression= ruleFeatureAccessBindingExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFeatureAccessBindingExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleFeatureAccessBindingExpression=ruleFeatureAccessBindingExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFeatureAccessBindingExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureAccessBindingExpression"


    // $ANTLR start "ruleFeatureAccessBindingExpression"
    // InternalCollaboration.g:655:1: ruleFeatureAccessBindingExpression returns [EObject current=null] : ( (lv_featureaccess_0_0= ruleFeatureAccess ) ) ;
    public final EObject ruleFeatureAccessBindingExpression() throws RecognitionException {
        EObject current = null;

        EObject lv_featureaccess_0_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:658:28: ( ( (lv_featureaccess_0_0= ruleFeatureAccess ) ) )
            // InternalCollaboration.g:659:1: ( (lv_featureaccess_0_0= ruleFeatureAccess ) )
            {
            // InternalCollaboration.g:659:1: ( (lv_featureaccess_0_0= ruleFeatureAccess ) )
            // InternalCollaboration.g:660:1: (lv_featureaccess_0_0= ruleFeatureAccess )
            {
            // InternalCollaboration.g:660:1: (lv_featureaccess_0_0= ruleFeatureAccess )
            // InternalCollaboration.g:661:3: lv_featureaccess_0_0= ruleFeatureAccess
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getFeatureAccessBindingExpressionAccess().getFeatureaccessFeatureAccessParserRuleCall_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_featureaccess_0_0=ruleFeatureAccess();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getFeatureAccessBindingExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"featureaccess",
                      		lv_featureaccess_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.FeatureAccess");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureAccessBindingExpression"


    // $ANTLR start "entryRuleInteractionFragment"
    // InternalCollaboration.g:685:1: entryRuleInteractionFragment returns [EObject current=null] : iv_ruleInteractionFragment= ruleInteractionFragment EOF ;
    public final EObject entryRuleInteractionFragment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInteractionFragment = null;


        try {
            // InternalCollaboration.g:686:2: (iv_ruleInteractionFragment= ruleInteractionFragment EOF )
            // InternalCollaboration.g:687:2: iv_ruleInteractionFragment= ruleInteractionFragment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInteractionFragmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleInteractionFragment=ruleInteractionFragment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInteractionFragment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInteractionFragment"


    // $ANTLR start "ruleInteractionFragment"
    // InternalCollaboration.g:694:1: ruleInteractionFragment returns [EObject current=null] : (this_Interaction_0= ruleInteraction | this_ModalMessage_1= ruleModalMessage | this_Alternative_2= ruleAlternative | this_Loop_3= ruleLoop | this_Parallel_4= ruleParallel | this_ConditionFragment_5= ruleConditionFragment | this_TimedConditionFragment_6= ruleTimedConditionFragment | this_VariableFragment_7= ruleVariableFragment ) ;
    public final EObject ruleInteractionFragment() throws RecognitionException {
        EObject current = null;

        EObject this_Interaction_0 = null;

        EObject this_ModalMessage_1 = null;

        EObject this_Alternative_2 = null;

        EObject this_Loop_3 = null;

        EObject this_Parallel_4 = null;

        EObject this_ConditionFragment_5 = null;

        EObject this_TimedConditionFragment_6 = null;

        EObject this_VariableFragment_7 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:697:28: ( (this_Interaction_0= ruleInteraction | this_ModalMessage_1= ruleModalMessage | this_Alternative_2= ruleAlternative | this_Loop_3= ruleLoop | this_Parallel_4= ruleParallel | this_ConditionFragment_5= ruleConditionFragment | this_TimedConditionFragment_6= ruleTimedConditionFragment | this_VariableFragment_7= ruleVariableFragment ) )
            // InternalCollaboration.g:698:1: (this_Interaction_0= ruleInteraction | this_ModalMessage_1= ruleModalMessage | this_Alternative_2= ruleAlternative | this_Loop_3= ruleLoop | this_Parallel_4= ruleParallel | this_ConditionFragment_5= ruleConditionFragment | this_TimedConditionFragment_6= ruleTimedConditionFragment | this_VariableFragment_7= ruleVariableFragment )
            {
            // InternalCollaboration.g:698:1: (this_Interaction_0= ruleInteraction | this_ModalMessage_1= ruleModalMessage | this_Alternative_2= ruleAlternative | this_Loop_3= ruleLoop | this_Parallel_4= ruleParallel | this_ConditionFragment_5= ruleConditionFragment | this_TimedConditionFragment_6= ruleTimedConditionFragment | this_VariableFragment_7= ruleVariableFragment )
            int alt15=8;
            alt15 = dfa15.predict(input);
            switch (alt15) {
                case 1 :
                    // InternalCollaboration.g:699:5: this_Interaction_0= ruleInteraction
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getInteractionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Interaction_0=ruleInteraction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Interaction_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:709:5: this_ModalMessage_1= ruleModalMessage
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getModalMessageParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ModalMessage_1=ruleModalMessage();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ModalMessage_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:719:5: this_Alternative_2= ruleAlternative
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getAlternativeParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Alternative_2=ruleAlternative();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Alternative_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // InternalCollaboration.g:729:5: this_Loop_3= ruleLoop
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getLoopParserRuleCall_3()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Loop_3=ruleLoop();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Loop_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // InternalCollaboration.g:739:5: this_Parallel_4= ruleParallel
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getParallelParserRuleCall_4()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Parallel_4=ruleParallel();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Parallel_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 6 :
                    // InternalCollaboration.g:749:5: this_ConditionFragment_5= ruleConditionFragment
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getConditionFragmentParserRuleCall_5()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ConditionFragment_5=ruleConditionFragment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ConditionFragment_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 7 :
                    // InternalCollaboration.g:759:5: this_TimedConditionFragment_6= ruleTimedConditionFragment
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getTimedConditionFragmentParserRuleCall_6()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_TimedConditionFragment_6=ruleTimedConditionFragment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TimedConditionFragment_6; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 8 :
                    // InternalCollaboration.g:769:5: this_VariableFragment_7= ruleVariableFragment
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getVariableFragmentParserRuleCall_7()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableFragment_7=ruleVariableFragment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableFragment_7; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInteractionFragment"


    // $ANTLR start "entryRuleVariableFragment"
    // InternalCollaboration.g:785:1: entryRuleVariableFragment returns [EObject current=null] : iv_ruleVariableFragment= ruleVariableFragment EOF ;
    public final EObject entryRuleVariableFragment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableFragment = null;


        try {
            // InternalCollaboration.g:786:2: (iv_ruleVariableFragment= ruleVariableFragment EOF )
            // InternalCollaboration.g:787:2: iv_ruleVariableFragment= ruleVariableFragment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableFragmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableFragment=ruleVariableFragment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableFragment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableFragment"


    // $ANTLR start "ruleVariableFragment"
    // InternalCollaboration.g:794:1: ruleVariableFragment returns [EObject current=null] : ( ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment | lv_expression_0_3= ruleClockDeclaration | lv_expression_0_4= ruleClockAssignment ) ) ) ;
    public final EObject ruleVariableFragment() throws RecognitionException {
        EObject current = null;

        EObject lv_expression_0_1 = null;

        EObject lv_expression_0_2 = null;

        EObject lv_expression_0_3 = null;

        EObject lv_expression_0_4 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:797:28: ( ( ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment | lv_expression_0_3= ruleClockDeclaration | lv_expression_0_4= ruleClockAssignment ) ) ) )
            // InternalCollaboration.g:798:1: ( ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment | lv_expression_0_3= ruleClockDeclaration | lv_expression_0_4= ruleClockAssignment ) ) )
            {
            // InternalCollaboration.g:798:1: ( ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment | lv_expression_0_3= ruleClockDeclaration | lv_expression_0_4= ruleClockAssignment ) ) )
            // InternalCollaboration.g:799:1: ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment | lv_expression_0_3= ruleClockDeclaration | lv_expression_0_4= ruleClockAssignment ) )
            {
            // InternalCollaboration.g:799:1: ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment | lv_expression_0_3= ruleClockDeclaration | lv_expression_0_4= ruleClockAssignment ) )
            // InternalCollaboration.g:800:1: (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment | lv_expression_0_3= ruleClockDeclaration | lv_expression_0_4= ruleClockAssignment )
            {
            // InternalCollaboration.g:800:1: (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment | lv_expression_0_3= ruleClockDeclaration | lv_expression_0_4= ruleClockAssignment )
            int alt16=4;
            switch ( input.LA(1) ) {
            case 57:
                {
                alt16=1;
                }
                break;
            case RULE_ID:
                {
                alt16=2;
                }
                break;
            case 58:
                {
                alt16=3;
                }
                break;
            case 59:
                {
                alt16=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }

            switch (alt16) {
                case 1 :
                    // InternalCollaboration.g:801:3: lv_expression_0_1= ruleTypedVariableDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getVariableFragmentAccess().getExpressionTypedVariableDeclarationParserRuleCall_0_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_0_1=ruleTypedVariableDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getVariableFragmentRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_0_1, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.TypedVariableDeclaration");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:816:8: lv_expression_0_2= ruleVariableAssignment
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getVariableFragmentAccess().getExpressionVariableAssignmentParserRuleCall_0_1()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_0_2=ruleVariableAssignment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getVariableFragmentRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_0_2, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.VariableAssignment");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:831:8: lv_expression_0_3= ruleClockDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getVariableFragmentAccess().getExpressionClockDeclarationParserRuleCall_0_2()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_0_3=ruleClockDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getVariableFragmentRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_0_3, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.ClockDeclaration");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }
                    break;
                case 4 :
                    // InternalCollaboration.g:846:8: lv_expression_0_4= ruleClockAssignment
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getVariableFragmentAccess().getExpressionClockAssignmentParserRuleCall_0_3()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_0_4=ruleClockAssignment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getVariableFragmentRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_0_4, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.ClockAssignment");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableFragment"


    // $ANTLR start "entryRuleInteraction"
    // InternalCollaboration.g:872:1: entryRuleInteraction returns [EObject current=null] : iv_ruleInteraction= ruleInteraction EOF ;
    public final EObject entryRuleInteraction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInteraction = null;


        try {
            // InternalCollaboration.g:873:2: (iv_ruleInteraction= ruleInteraction EOF )
            // InternalCollaboration.g:874:2: iv_ruleInteraction= ruleInteraction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInteractionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleInteraction=ruleInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInteraction; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInteraction"


    // $ANTLR start "ruleInteraction"
    // InternalCollaboration.g:881:1: ruleInteraction returns [EObject current=null] : ( () otherlv_1= '{' ( (lv_fragments_2_0= ruleInteractionFragment ) )* otherlv_3= '}' ( (lv_constraints_4_0= ruleConstraintBlock ) )? ) ;
    public final EObject ruleInteraction() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_fragments_2_0 = null;

        EObject lv_constraints_4_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:884:28: ( ( () otherlv_1= '{' ( (lv_fragments_2_0= ruleInteractionFragment ) )* otherlv_3= '}' ( (lv_constraints_4_0= ruleConstraintBlock ) )? ) )
            // InternalCollaboration.g:885:1: ( () otherlv_1= '{' ( (lv_fragments_2_0= ruleInteractionFragment ) )* otherlv_3= '}' ( (lv_constraints_4_0= ruleConstraintBlock ) )? )
            {
            // InternalCollaboration.g:885:1: ( () otherlv_1= '{' ( (lv_fragments_2_0= ruleInteractionFragment ) )* otherlv_3= '}' ( (lv_constraints_4_0= ruleConstraintBlock ) )? )
            // InternalCollaboration.g:885:2: () otherlv_1= '{' ( (lv_fragments_2_0= ruleInteractionFragment ) )* otherlv_3= '}' ( (lv_constraints_4_0= ruleConstraintBlock ) )?
            {
            // InternalCollaboration.g:885:2: ()
            // InternalCollaboration.g:886:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getInteractionAccess().getInteractionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,17,FollowSets000.FOLLOW_23); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getInteractionAccess().getLeftCurlyBracketKeyword_1());
                  
            }
            // InternalCollaboration.g:895:1: ( (lv_fragments_2_0= ruleInteractionFragment ) )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==RULE_ID||LA17_0==17||(LA17_0>=34 && LA17_0<=35)||LA17_0==41||(LA17_0>=43 && LA17_0<=44)||(LA17_0>=46 && LA17_0<=50)||(LA17_0>=57 && LA17_0<=59)||(LA17_0>=78 && LA17_0<=80)) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalCollaboration.g:896:1: (lv_fragments_2_0= ruleInteractionFragment )
            	    {
            	    // InternalCollaboration.g:896:1: (lv_fragments_2_0= ruleInteractionFragment )
            	    // InternalCollaboration.g:897:3: lv_fragments_2_0= ruleInteractionFragment
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getInteractionAccess().getFragmentsInteractionFragmentParserRuleCall_2_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_23);
            	    lv_fragments_2_0=ruleInteractionFragment();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getInteractionRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"fragments",
            	              		lv_fragments_2_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.InteractionFragment");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

            otherlv_3=(Token)match(input,18,FollowSets000.FOLLOW_24); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getInteractionAccess().getRightCurlyBracketKeyword_3());
                  
            }
            // InternalCollaboration.g:917:1: ( (lv_constraints_4_0= ruleConstraintBlock ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==51) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalCollaboration.g:918:1: (lv_constraints_4_0= ruleConstraintBlock )
                    {
                    // InternalCollaboration.g:918:1: (lv_constraints_4_0= ruleConstraintBlock )
                    // InternalCollaboration.g:919:3: lv_constraints_4_0= ruleConstraintBlock
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getInteractionAccess().getConstraintsConstraintBlockParserRuleCall_4_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_constraints_4_0=ruleConstraintBlock();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getInteractionRule());
                      	        }
                             		set(
                             			current, 
                             			"constraints",
                              		lv_constraints_4_0, 
                              		"org.scenariotools.sml.collaboration.Collaboration.ConstraintBlock");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInteraction"


    // $ANTLR start "entryRuleModalMessage"
    // InternalCollaboration.g:943:1: entryRuleModalMessage returns [EObject current=null] : iv_ruleModalMessage= ruleModalMessage EOF ;
    public final EObject entryRuleModalMessage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModalMessage = null;


        try {
            // InternalCollaboration.g:944:2: (iv_ruleModalMessage= ruleModalMessage EOF )
            // InternalCollaboration.g:945:2: iv_ruleModalMessage= ruleModalMessage EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getModalMessageRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleModalMessage=ruleModalMessage();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleModalMessage; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModalMessage"


    // $ANTLR start "ruleModalMessage"
    // InternalCollaboration.g:952:1: ruleModalMessage returns [EObject current=null] : ( ( (lv_strict_0_0= 'strict' ) )? ( ( (lv_monitored_1_0= 'monitored' ) )? ( (lv_expectationKind_2_0= ruleExpectationKind ) ) )? ( (otherlv_3= RULE_ID ) ) otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) otherlv_6= '.' ( (otherlv_7= RULE_ID ) ) (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )? (otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')' )? ) ;
    public final EObject ruleModalMessage() throws RecognitionException {
        EObject current = null;

        Token lv_strict_0_0=null;
        Token lv_monitored_1_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Enumerator lv_expectationKind_2_0 = null;

        Enumerator lv_collectionModification_9_0 = null;

        EObject lv_parameters_11_0 = null;

        EObject lv_parameters_13_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:955:28: ( ( ( (lv_strict_0_0= 'strict' ) )? ( ( (lv_monitored_1_0= 'monitored' ) )? ( (lv_expectationKind_2_0= ruleExpectationKind ) ) )? ( (otherlv_3= RULE_ID ) ) otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) otherlv_6= '.' ( (otherlv_7= RULE_ID ) ) (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )? (otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')' )? ) )
            // InternalCollaboration.g:956:1: ( ( (lv_strict_0_0= 'strict' ) )? ( ( (lv_monitored_1_0= 'monitored' ) )? ( (lv_expectationKind_2_0= ruleExpectationKind ) ) )? ( (otherlv_3= RULE_ID ) ) otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) otherlv_6= '.' ( (otherlv_7= RULE_ID ) ) (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )? (otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')' )? )
            {
            // InternalCollaboration.g:956:1: ( ( (lv_strict_0_0= 'strict' ) )? ( ( (lv_monitored_1_0= 'monitored' ) )? ( (lv_expectationKind_2_0= ruleExpectationKind ) ) )? ( (otherlv_3= RULE_ID ) ) otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) otherlv_6= '.' ( (otherlv_7= RULE_ID ) ) (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )? (otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')' )? )
            // InternalCollaboration.g:956:2: ( (lv_strict_0_0= 'strict' ) )? ( ( (lv_monitored_1_0= 'monitored' ) )? ( (lv_expectationKind_2_0= ruleExpectationKind ) ) )? ( (otherlv_3= RULE_ID ) ) otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) otherlv_6= '.' ( (otherlv_7= RULE_ID ) ) (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )? (otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')' )?
            {
            // InternalCollaboration.g:956:2: ( (lv_strict_0_0= 'strict' ) )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==34) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalCollaboration.g:957:1: (lv_strict_0_0= 'strict' )
                    {
                    // InternalCollaboration.g:957:1: (lv_strict_0_0= 'strict' )
                    // InternalCollaboration.g:958:3: lv_strict_0_0= 'strict'
                    {
                    lv_strict_0_0=(Token)match(input,34,FollowSets000.FOLLOW_25); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_strict_0_0, grammarAccess.getModalMessageAccess().getStrictStrictKeyword_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getModalMessageRule());
                      	        }
                             		setWithLastConsumed(current, "strict", true, "strict");
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalCollaboration.g:971:3: ( ( (lv_monitored_1_0= 'monitored' ) )? ( (lv_expectationKind_2_0= ruleExpectationKind ) ) )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==35||LA21_0==47||(LA21_0>=78 && LA21_0<=80)) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalCollaboration.g:971:4: ( (lv_monitored_1_0= 'monitored' ) )? ( (lv_expectationKind_2_0= ruleExpectationKind ) )
                    {
                    // InternalCollaboration.g:971:4: ( (lv_monitored_1_0= 'monitored' ) )?
                    int alt20=2;
                    int LA20_0 = input.LA(1);

                    if ( (LA20_0==35) ) {
                        alt20=1;
                    }
                    switch (alt20) {
                        case 1 :
                            // InternalCollaboration.g:972:1: (lv_monitored_1_0= 'monitored' )
                            {
                            // InternalCollaboration.g:972:1: (lv_monitored_1_0= 'monitored' )
                            // InternalCollaboration.g:973:3: lv_monitored_1_0= 'monitored'
                            {
                            lv_monitored_1_0=(Token)match(input,35,FollowSets000.FOLLOW_26); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_monitored_1_0, grammarAccess.getModalMessageAccess().getMonitoredMonitoredKeyword_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getModalMessageRule());
                              	        }
                                     		setWithLastConsumed(current, "monitored", true, "monitored");
                              	    
                            }

                            }


                            }
                            break;

                    }

                    // InternalCollaboration.g:986:3: ( (lv_expectationKind_2_0= ruleExpectationKind ) )
                    // InternalCollaboration.g:987:1: (lv_expectationKind_2_0= ruleExpectationKind )
                    {
                    // InternalCollaboration.g:987:1: (lv_expectationKind_2_0= ruleExpectationKind )
                    // InternalCollaboration.g:988:3: lv_expectationKind_2_0= ruleExpectationKind
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getModalMessageAccess().getExpectationKindExpectationKindEnumRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_4);
                    lv_expectationKind_2_0=ruleExpectationKind();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getModalMessageRule());
                      	        }
                             		set(
                             			current, 
                             			"expectationKind",
                              		lv_expectationKind_2_0, 
                              		"org.scenariotools.sml.collaboration.Collaboration.ExpectationKind");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalCollaboration.g:1004:4: ( (otherlv_3= RULE_ID ) )
            // InternalCollaboration.g:1005:1: (otherlv_3= RULE_ID )
            {
            // InternalCollaboration.g:1005:1: (otherlv_3= RULE_ID )
            // InternalCollaboration.g:1006:3: otherlv_3= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getModalMessageRule());
              	        }
                      
            }
            otherlv_3=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_3, grammarAccess.getModalMessageAccess().getSenderRoleCrossReference_2_0()); 
              	
            }

            }


            }

            otherlv_4=(Token)match(input,36,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getModalMessageAccess().getHyphenMinusGreaterThanSignKeyword_3());
                  
            }
            // InternalCollaboration.g:1021:1: ( (otherlv_5= RULE_ID ) )
            // InternalCollaboration.g:1022:1: (otherlv_5= RULE_ID )
            {
            // InternalCollaboration.g:1022:1: (otherlv_5= RULE_ID )
            // InternalCollaboration.g:1023:3: otherlv_5= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getModalMessageRule());
              	        }
                      
            }
            otherlv_5=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_28); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_5, grammarAccess.getModalMessageAccess().getReceiverRoleCrossReference_4_0()); 
              	
            }

            }


            }

            otherlv_6=(Token)match(input,19,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getModalMessageAccess().getFullStopKeyword_5());
                  
            }
            // InternalCollaboration.g:1038:1: ( (otherlv_7= RULE_ID ) )
            // InternalCollaboration.g:1039:1: (otherlv_7= RULE_ID )
            {
            // InternalCollaboration.g:1039:1: (otherlv_7= RULE_ID )
            // InternalCollaboration.g:1040:3: otherlv_7= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getModalMessageRule());
              	        }
                      
            }
            otherlv_7=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_29); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_7, grammarAccess.getModalMessageAccess().getModelElementETypedElementCrossReference_6_0()); 
              	
            }

            }


            }

            // InternalCollaboration.g:1051:2: (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==19) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalCollaboration.g:1051:4: otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) )
                    {
                    otherlv_8=(Token)match(input,19,FollowSets000.FOLLOW_30); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_8, grammarAccess.getModalMessageAccess().getFullStopKeyword_7_0());
                          
                    }
                    // InternalCollaboration.g:1055:1: ( (lv_collectionModification_9_0= ruleCollectionModification ) )
                    // InternalCollaboration.g:1056:1: (lv_collectionModification_9_0= ruleCollectionModification )
                    {
                    // InternalCollaboration.g:1056:1: (lv_collectionModification_9_0= ruleCollectionModification )
                    // InternalCollaboration.g:1057:3: lv_collectionModification_9_0= ruleCollectionModification
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getModalMessageAccess().getCollectionModificationCollectionModificationEnumRuleCall_7_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_31);
                    lv_collectionModification_9_0=ruleCollectionModification();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getModalMessageRule());
                      	        }
                             		set(
                             			current, 
                             			"collectionModification",
                              		lv_collectionModification_9_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionModification");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalCollaboration.g:1073:4: (otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')' )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==37) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalCollaboration.g:1073:6: otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')'
                    {
                    otherlv_10=(Token)match(input,37,FollowSets000.FOLLOW_32); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_10, grammarAccess.getModalMessageAccess().getLeftParenthesisKeyword_8_0());
                          
                    }
                    // InternalCollaboration.g:1077:1: ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )?
                    int alt24=2;
                    int LA24_0 = input.LA(1);

                    if ( (LA24_0==RULE_ID||(LA24_0>=RULE_STRING && LA24_0<=RULE_BOOL)||LA24_0==37||(LA24_0>=39 && LA24_0<=40)||LA24_0==70||LA24_0==72||LA24_0==74) ) {
                        alt24=1;
                    }
                    switch (alt24) {
                        case 1 :
                            // InternalCollaboration.g:1077:2: ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )*
                            {
                            // InternalCollaboration.g:1077:2: ( (lv_parameters_11_0= ruleParameterBinding ) )
                            // InternalCollaboration.g:1078:1: (lv_parameters_11_0= ruleParameterBinding )
                            {
                            // InternalCollaboration.g:1078:1: (lv_parameters_11_0= ruleParameterBinding )
                            // InternalCollaboration.g:1079:3: lv_parameters_11_0= ruleParameterBinding
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getModalMessageAccess().getParametersParameterBindingParserRuleCall_8_1_0_0()); 
                              	    
                            }
                            pushFollow(FollowSets000.FOLLOW_33);
                            lv_parameters_11_0=ruleParameterBinding();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getModalMessageRule());
                              	        }
                                     		add(
                                     			current, 
                                     			"parameters",
                                      		lv_parameters_11_0, 
                                      		"org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }

                            // InternalCollaboration.g:1095:2: (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )*
                            loop23:
                            do {
                                int alt23=2;
                                int LA23_0 = input.LA(1);

                                if ( (LA23_0==31) ) {
                                    alt23=1;
                                }


                                switch (alt23) {
                            	case 1 :
                            	    // InternalCollaboration.g:1095:4: otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) )
                            	    {
                            	    otherlv_12=(Token)match(input,31,FollowSets000.FOLLOW_34); if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	          	newLeafNode(otherlv_12, grammarAccess.getModalMessageAccess().getCommaKeyword_8_1_1_0());
                            	          
                            	    }
                            	    // InternalCollaboration.g:1099:1: ( (lv_parameters_13_0= ruleParameterBinding ) )
                            	    // InternalCollaboration.g:1100:1: (lv_parameters_13_0= ruleParameterBinding )
                            	    {
                            	    // InternalCollaboration.g:1100:1: (lv_parameters_13_0= ruleParameterBinding )
                            	    // InternalCollaboration.g:1101:3: lv_parameters_13_0= ruleParameterBinding
                            	    {
                            	    if ( state.backtracking==0 ) {
                            	       
                            	      	        newCompositeNode(grammarAccess.getModalMessageAccess().getParametersParameterBindingParserRuleCall_8_1_1_1_0()); 
                            	      	    
                            	    }
                            	    pushFollow(FollowSets000.FOLLOW_33);
                            	    lv_parameters_13_0=ruleParameterBinding();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      	        if (current==null) {
                            	      	            current = createModelElementForParent(grammarAccess.getModalMessageRule());
                            	      	        }
                            	             		add(
                            	             			current, 
                            	             			"parameters",
                            	              		lv_parameters_13_0, 
                            	              		"org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
                            	      	        afterParserOrEnumRuleCall();
                            	      	    
                            	    }

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop23;
                                }
                            } while (true);


                            }
                            break;

                    }

                    otherlv_14=(Token)match(input,38,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_14, grammarAccess.getModalMessageAccess().getRightParenthesisKeyword_8_2());
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModalMessage"


    // $ANTLR start "entryRuleParameterBinding"
    // InternalCollaboration.g:1129:1: entryRuleParameterBinding returns [EObject current=null] : iv_ruleParameterBinding= ruleParameterBinding EOF ;
    public final EObject entryRuleParameterBinding() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameterBinding = null;


        try {
            // InternalCollaboration.g:1130:2: (iv_ruleParameterBinding= ruleParameterBinding EOF )
            // InternalCollaboration.g:1131:2: iv_ruleParameterBinding= ruleParameterBinding EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParameterBindingRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleParameterBinding=ruleParameterBinding();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParameterBinding; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameterBinding"


    // $ANTLR start "ruleParameterBinding"
    // InternalCollaboration.g:1138:1: ruleParameterBinding returns [EObject current=null] : ( (lv_bindingExpression_0_0= ruleParameterExpression ) ) ;
    public final EObject ruleParameterBinding() throws RecognitionException {
        EObject current = null;

        EObject lv_bindingExpression_0_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1141:28: ( ( (lv_bindingExpression_0_0= ruleParameterExpression ) ) )
            // InternalCollaboration.g:1142:1: ( (lv_bindingExpression_0_0= ruleParameterExpression ) )
            {
            // InternalCollaboration.g:1142:1: ( (lv_bindingExpression_0_0= ruleParameterExpression ) )
            // InternalCollaboration.g:1143:1: (lv_bindingExpression_0_0= ruleParameterExpression )
            {
            // InternalCollaboration.g:1143:1: (lv_bindingExpression_0_0= ruleParameterExpression )
            // InternalCollaboration.g:1144:3: lv_bindingExpression_0_0= ruleParameterExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getParameterBindingAccess().getBindingExpressionParameterExpressionParserRuleCall_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_bindingExpression_0_0=ruleParameterExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getParameterBindingRule());
              	        }
                     		set(
                     			current, 
                     			"bindingExpression",
                      		lv_bindingExpression_0_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ParameterExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameterBinding"


    // $ANTLR start "entryRuleParameterExpression"
    // InternalCollaboration.g:1168:1: entryRuleParameterExpression returns [EObject current=null] : iv_ruleParameterExpression= ruleParameterExpression EOF ;
    public final EObject entryRuleParameterExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameterExpression = null;


        try {
            // InternalCollaboration.g:1169:2: (iv_ruleParameterExpression= ruleParameterExpression EOF )
            // InternalCollaboration.g:1170:2: iv_ruleParameterExpression= ruleParameterExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParameterExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleParameterExpression=ruleParameterExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParameterExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameterExpression"


    // $ANTLR start "ruleParameterExpression"
    // InternalCollaboration.g:1177:1: ruleParameterExpression returns [EObject current=null] : (this_WildcardParameterExpression_0= ruleWildcardParameterExpression | this_ValueParameterExpression_1= ruleValueParameterExpression | this_VariableBindingParameterExpression_2= ruleVariableBindingParameterExpression ) ;
    public final EObject ruleParameterExpression() throws RecognitionException {
        EObject current = null;

        EObject this_WildcardParameterExpression_0 = null;

        EObject this_ValueParameterExpression_1 = null;

        EObject this_VariableBindingParameterExpression_2 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1180:28: ( (this_WildcardParameterExpression_0= ruleWildcardParameterExpression | this_ValueParameterExpression_1= ruleValueParameterExpression | this_VariableBindingParameterExpression_2= ruleVariableBindingParameterExpression ) )
            // InternalCollaboration.g:1181:1: (this_WildcardParameterExpression_0= ruleWildcardParameterExpression | this_ValueParameterExpression_1= ruleValueParameterExpression | this_VariableBindingParameterExpression_2= ruleVariableBindingParameterExpression )
            {
            // InternalCollaboration.g:1181:1: (this_WildcardParameterExpression_0= ruleWildcardParameterExpression | this_ValueParameterExpression_1= ruleValueParameterExpression | this_VariableBindingParameterExpression_2= ruleVariableBindingParameterExpression )
            int alt26=3;
            switch ( input.LA(1) ) {
            case 39:
                {
                alt26=1;
                }
                break;
            case RULE_ID:
            case RULE_STRING:
            case RULE_INT:
            case RULE_SIGNEDINT:
            case RULE_BOOL:
            case 37:
            case 70:
            case 72:
            case 74:
                {
                alt26=2;
                }
                break;
            case 40:
                {
                alt26=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;
            }

            switch (alt26) {
                case 1 :
                    // InternalCollaboration.g:1182:5: this_WildcardParameterExpression_0= ruleWildcardParameterExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getParameterExpressionAccess().getWildcardParameterExpressionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_WildcardParameterExpression_0=ruleWildcardParameterExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_WildcardParameterExpression_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:1192:5: this_ValueParameterExpression_1= ruleValueParameterExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getParameterExpressionAccess().getValueParameterExpressionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ValueParameterExpression_1=ruleValueParameterExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ValueParameterExpression_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:1202:5: this_VariableBindingParameterExpression_2= ruleVariableBindingParameterExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getParameterExpressionAccess().getVariableBindingParameterExpressionParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableBindingParameterExpression_2=ruleVariableBindingParameterExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableBindingParameterExpression_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameterExpression"


    // $ANTLR start "entryRuleWildcardParameterExpression"
    // InternalCollaboration.g:1218:1: entryRuleWildcardParameterExpression returns [EObject current=null] : iv_ruleWildcardParameterExpression= ruleWildcardParameterExpression EOF ;
    public final EObject entryRuleWildcardParameterExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWildcardParameterExpression = null;


        try {
            // InternalCollaboration.g:1219:2: (iv_ruleWildcardParameterExpression= ruleWildcardParameterExpression EOF )
            // InternalCollaboration.g:1220:2: iv_ruleWildcardParameterExpression= ruleWildcardParameterExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getWildcardParameterExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleWildcardParameterExpression=ruleWildcardParameterExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleWildcardParameterExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWildcardParameterExpression"


    // $ANTLR start "ruleWildcardParameterExpression"
    // InternalCollaboration.g:1227:1: ruleWildcardParameterExpression returns [EObject current=null] : ( () otherlv_1= '*' ) ;
    public final EObject ruleWildcardParameterExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:1230:28: ( ( () otherlv_1= '*' ) )
            // InternalCollaboration.g:1231:1: ( () otherlv_1= '*' )
            {
            // InternalCollaboration.g:1231:1: ( () otherlv_1= '*' )
            // InternalCollaboration.g:1231:2: () otherlv_1= '*'
            {
            // InternalCollaboration.g:1231:2: ()
            // InternalCollaboration.g:1232:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getWildcardParameterExpressionAccess().getWildcardParameterExpressionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,39,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getWildcardParameterExpressionAccess().getAsteriskKeyword_1());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWildcardParameterExpression"


    // $ANTLR start "entryRuleValueParameterExpression"
    // InternalCollaboration.g:1249:1: entryRuleValueParameterExpression returns [EObject current=null] : iv_ruleValueParameterExpression= ruleValueParameterExpression EOF ;
    public final EObject entryRuleValueParameterExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleValueParameterExpression = null;


        try {
            // InternalCollaboration.g:1250:2: (iv_ruleValueParameterExpression= ruleValueParameterExpression EOF )
            // InternalCollaboration.g:1251:2: iv_ruleValueParameterExpression= ruleValueParameterExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getValueParameterExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleValueParameterExpression=ruleValueParameterExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleValueParameterExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValueParameterExpression"


    // $ANTLR start "ruleValueParameterExpression"
    // InternalCollaboration.g:1258:1: ruleValueParameterExpression returns [EObject current=null] : ( (lv_value_0_0= ruleExpression ) ) ;
    public final EObject ruleValueParameterExpression() throws RecognitionException {
        EObject current = null;

        EObject lv_value_0_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1261:28: ( ( (lv_value_0_0= ruleExpression ) ) )
            // InternalCollaboration.g:1262:1: ( (lv_value_0_0= ruleExpression ) )
            {
            // InternalCollaboration.g:1262:1: ( (lv_value_0_0= ruleExpression ) )
            // InternalCollaboration.g:1263:1: (lv_value_0_0= ruleExpression )
            {
            // InternalCollaboration.g:1263:1: (lv_value_0_0= ruleExpression )
            // InternalCollaboration.g:1264:3: lv_value_0_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getValueParameterExpressionAccess().getValueExpressionParserRuleCall_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_value_0_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getValueParameterExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValueParameterExpression"


    // $ANTLR start "entryRuleVariableBindingParameterExpression"
    // InternalCollaboration.g:1288:1: entryRuleVariableBindingParameterExpression returns [EObject current=null] : iv_ruleVariableBindingParameterExpression= ruleVariableBindingParameterExpression EOF ;
    public final EObject entryRuleVariableBindingParameterExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableBindingParameterExpression = null;


        try {
            // InternalCollaboration.g:1289:2: (iv_ruleVariableBindingParameterExpression= ruleVariableBindingParameterExpression EOF )
            // InternalCollaboration.g:1290:2: iv_ruleVariableBindingParameterExpression= ruleVariableBindingParameterExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableBindingParameterExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableBindingParameterExpression=ruleVariableBindingParameterExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableBindingParameterExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableBindingParameterExpression"


    // $ANTLR start "ruleVariableBindingParameterExpression"
    // InternalCollaboration.g:1297:1: ruleVariableBindingParameterExpression returns [EObject current=null] : (otherlv_0= 'bind' ( (lv_variable_1_0= ruleVariableValue ) ) ) ;
    public final EObject ruleVariableBindingParameterExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_variable_1_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1300:28: ( (otherlv_0= 'bind' ( (lv_variable_1_0= ruleVariableValue ) ) ) )
            // InternalCollaboration.g:1301:1: (otherlv_0= 'bind' ( (lv_variable_1_0= ruleVariableValue ) ) )
            {
            // InternalCollaboration.g:1301:1: (otherlv_0= 'bind' ( (lv_variable_1_0= ruleVariableValue ) ) )
            // InternalCollaboration.g:1301:3: otherlv_0= 'bind' ( (lv_variable_1_0= ruleVariableValue ) )
            {
            otherlv_0=(Token)match(input,40,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getVariableBindingParameterExpressionAccess().getBindKeyword_0());
                  
            }
            // InternalCollaboration.g:1305:1: ( (lv_variable_1_0= ruleVariableValue ) )
            // InternalCollaboration.g:1306:1: (lv_variable_1_0= ruleVariableValue )
            {
            // InternalCollaboration.g:1306:1: (lv_variable_1_0= ruleVariableValue )
            // InternalCollaboration.g:1307:3: lv_variable_1_0= ruleVariableValue
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getVariableBindingParameterExpressionAccess().getVariableVariableValueParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_variable_1_0=ruleVariableValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getVariableBindingParameterExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"variable",
                      		lv_variable_1_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.VariableValue");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableBindingParameterExpression"


    // $ANTLR start "entryRuleAlternative"
    // InternalCollaboration.g:1331:1: entryRuleAlternative returns [EObject current=null] : iv_ruleAlternative= ruleAlternative EOF ;
    public final EObject entryRuleAlternative() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAlternative = null;


        try {
            // InternalCollaboration.g:1332:2: (iv_ruleAlternative= ruleAlternative EOF )
            // InternalCollaboration.g:1333:2: iv_ruleAlternative= ruleAlternative EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAlternativeRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAlternative=ruleAlternative();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAlternative; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAlternative"


    // $ANTLR start "ruleAlternative"
    // InternalCollaboration.g:1340:1: ruleAlternative returns [EObject current=null] : ( () otherlv_1= 'alternative' ( (lv_cases_2_0= ruleCase ) ) (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )* ) ;
    public final EObject ruleAlternative() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_cases_2_0 = null;

        EObject lv_cases_4_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1343:28: ( ( () otherlv_1= 'alternative' ( (lv_cases_2_0= ruleCase ) ) (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )* ) )
            // InternalCollaboration.g:1344:1: ( () otherlv_1= 'alternative' ( (lv_cases_2_0= ruleCase ) ) (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )* )
            {
            // InternalCollaboration.g:1344:1: ( () otherlv_1= 'alternative' ( (lv_cases_2_0= ruleCase ) ) (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )* )
            // InternalCollaboration.g:1344:2: () otherlv_1= 'alternative' ( (lv_cases_2_0= ruleCase ) ) (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )*
            {
            // InternalCollaboration.g:1344:2: ()
            // InternalCollaboration.g:1345:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getAlternativeAccess().getAlternativeAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,41,FollowSets000.FOLLOW_35); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getAlternativeAccess().getAlternativeKeyword_1());
                  
            }
            // InternalCollaboration.g:1354:1: ( (lv_cases_2_0= ruleCase ) )
            // InternalCollaboration.g:1355:1: (lv_cases_2_0= ruleCase )
            {
            // InternalCollaboration.g:1355:1: (lv_cases_2_0= ruleCase )
            // InternalCollaboration.g:1356:3: lv_cases_2_0= ruleCase
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getAlternativeAccess().getCasesCaseParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_36);
            lv_cases_2_0=ruleCase();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getAlternativeRule());
              	        }
                     		add(
                     			current, 
                     			"cases",
                      		lv_cases_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.Case");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalCollaboration.g:1372:2: (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==42) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // InternalCollaboration.g:1372:4: otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) )
            	    {
            	    otherlv_3=(Token)match(input,42,FollowSets000.FOLLOW_35); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getAlternativeAccess().getOrKeyword_3_0());
            	          
            	    }
            	    // InternalCollaboration.g:1376:1: ( (lv_cases_4_0= ruleCase ) )
            	    // InternalCollaboration.g:1377:1: (lv_cases_4_0= ruleCase )
            	    {
            	    // InternalCollaboration.g:1377:1: (lv_cases_4_0= ruleCase )
            	    // InternalCollaboration.g:1378:3: lv_cases_4_0= ruleCase
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getAlternativeAccess().getCasesCaseParserRuleCall_3_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_36);
            	    lv_cases_4_0=ruleCase();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getAlternativeRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"cases",
            	              		lv_cases_4_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.Case");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAlternative"


    // $ANTLR start "entryRuleCase"
    // InternalCollaboration.g:1402:1: entryRuleCase returns [EObject current=null] : iv_ruleCase= ruleCase EOF ;
    public final EObject entryRuleCase() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCase = null;


        try {
            // InternalCollaboration.g:1403:2: (iv_ruleCase= ruleCase EOF )
            // InternalCollaboration.g:1404:2: iv_ruleCase= ruleCase EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCaseRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCase=ruleCase();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCase; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCase"


    // $ANTLR start "ruleCase"
    // InternalCollaboration.g:1411:1: ruleCase returns [EObject current=null] : ( () ( (lv_caseCondition_1_0= ruleCondition ) )? ( (lv_caseInteraction_2_0= ruleInteraction ) ) ) ;
    public final EObject ruleCase() throws RecognitionException {
        EObject current = null;

        EObject lv_caseCondition_1_0 = null;

        EObject lv_caseInteraction_2_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1414:28: ( ( () ( (lv_caseCondition_1_0= ruleCondition ) )? ( (lv_caseInteraction_2_0= ruleInteraction ) ) ) )
            // InternalCollaboration.g:1415:1: ( () ( (lv_caseCondition_1_0= ruleCondition ) )? ( (lv_caseInteraction_2_0= ruleInteraction ) ) )
            {
            // InternalCollaboration.g:1415:1: ( () ( (lv_caseCondition_1_0= ruleCondition ) )? ( (lv_caseInteraction_2_0= ruleInteraction ) ) )
            // InternalCollaboration.g:1415:2: () ( (lv_caseCondition_1_0= ruleCondition ) )? ( (lv_caseInteraction_2_0= ruleInteraction ) )
            {
            // InternalCollaboration.g:1415:2: ()
            // InternalCollaboration.g:1416:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getCaseAccess().getCaseAction_0(),
                          current);
                  
            }

            }

            // InternalCollaboration.g:1421:2: ( (lv_caseCondition_1_0= ruleCondition ) )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==28) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalCollaboration.g:1422:1: (lv_caseCondition_1_0= ruleCondition )
                    {
                    // InternalCollaboration.g:1422:1: (lv_caseCondition_1_0= ruleCondition )
                    // InternalCollaboration.g:1423:3: lv_caseCondition_1_0= ruleCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getCaseAccess().getCaseConditionConditionParserRuleCall_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_15);
                    lv_caseCondition_1_0=ruleCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getCaseRule());
                      	        }
                             		set(
                             			current, 
                             			"caseCondition",
                              		lv_caseCondition_1_0, 
                              		"org.scenariotools.sml.collaboration.Collaboration.Condition");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalCollaboration.g:1439:3: ( (lv_caseInteraction_2_0= ruleInteraction ) )
            // InternalCollaboration.g:1440:1: (lv_caseInteraction_2_0= ruleInteraction )
            {
            // InternalCollaboration.g:1440:1: (lv_caseInteraction_2_0= ruleInteraction )
            // InternalCollaboration.g:1441:3: lv_caseInteraction_2_0= ruleInteraction
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getCaseAccess().getCaseInteractionInteractionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_caseInteraction_2_0=ruleInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getCaseRule());
              	        }
                     		set(
                     			current, 
                     			"caseInteraction",
                      		lv_caseInteraction_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCase"


    // $ANTLR start "entryRuleLoop"
    // InternalCollaboration.g:1465:1: entryRuleLoop returns [EObject current=null] : iv_ruleLoop= ruleLoop EOF ;
    public final EObject entryRuleLoop() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLoop = null;


        try {
            // InternalCollaboration.g:1466:2: (iv_ruleLoop= ruleLoop EOF )
            // InternalCollaboration.g:1467:2: iv_ruleLoop= ruleLoop EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLoopRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleLoop=ruleLoop();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLoop; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLoop"


    // $ANTLR start "ruleLoop"
    // InternalCollaboration.g:1474:1: ruleLoop returns [EObject current=null] : (otherlv_0= 'while' ( (lv_loopCondition_1_0= ruleCondition ) )? ( (lv_bodyInteraction_2_0= ruleInteraction ) ) ) ;
    public final EObject ruleLoop() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_loopCondition_1_0 = null;

        EObject lv_bodyInteraction_2_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1477:28: ( (otherlv_0= 'while' ( (lv_loopCondition_1_0= ruleCondition ) )? ( (lv_bodyInteraction_2_0= ruleInteraction ) ) ) )
            // InternalCollaboration.g:1478:1: (otherlv_0= 'while' ( (lv_loopCondition_1_0= ruleCondition ) )? ( (lv_bodyInteraction_2_0= ruleInteraction ) ) )
            {
            // InternalCollaboration.g:1478:1: (otherlv_0= 'while' ( (lv_loopCondition_1_0= ruleCondition ) )? ( (lv_bodyInteraction_2_0= ruleInteraction ) ) )
            // InternalCollaboration.g:1478:3: otherlv_0= 'while' ( (lv_loopCondition_1_0= ruleCondition ) )? ( (lv_bodyInteraction_2_0= ruleInteraction ) )
            {
            otherlv_0=(Token)match(input,43,FollowSets000.FOLLOW_35); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getLoopAccess().getWhileKeyword_0());
                  
            }
            // InternalCollaboration.g:1482:1: ( (lv_loopCondition_1_0= ruleCondition ) )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==28) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalCollaboration.g:1483:1: (lv_loopCondition_1_0= ruleCondition )
                    {
                    // InternalCollaboration.g:1483:1: (lv_loopCondition_1_0= ruleCondition )
                    // InternalCollaboration.g:1484:3: lv_loopCondition_1_0= ruleCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getLoopAccess().getLoopConditionConditionParserRuleCall_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_15);
                    lv_loopCondition_1_0=ruleCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getLoopRule());
                      	        }
                             		set(
                             			current, 
                             			"loopCondition",
                              		lv_loopCondition_1_0, 
                              		"org.scenariotools.sml.collaboration.Collaboration.Condition");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalCollaboration.g:1500:3: ( (lv_bodyInteraction_2_0= ruleInteraction ) )
            // InternalCollaboration.g:1501:1: (lv_bodyInteraction_2_0= ruleInteraction )
            {
            // InternalCollaboration.g:1501:1: (lv_bodyInteraction_2_0= ruleInteraction )
            // InternalCollaboration.g:1502:3: lv_bodyInteraction_2_0= ruleInteraction
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLoopAccess().getBodyInteractionInteractionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_bodyInteraction_2_0=ruleInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLoopRule());
              	        }
                     		set(
                     			current, 
                     			"bodyInteraction",
                      		lv_bodyInteraction_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLoop"


    // $ANTLR start "entryRuleParallel"
    // InternalCollaboration.g:1526:1: entryRuleParallel returns [EObject current=null] : iv_ruleParallel= ruleParallel EOF ;
    public final EObject entryRuleParallel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParallel = null;


        try {
            // InternalCollaboration.g:1527:2: (iv_ruleParallel= ruleParallel EOF )
            // InternalCollaboration.g:1528:2: iv_ruleParallel= ruleParallel EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParallelRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleParallel=ruleParallel();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParallel; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParallel"


    // $ANTLR start "ruleParallel"
    // InternalCollaboration.g:1535:1: ruleParallel returns [EObject current=null] : ( () otherlv_1= 'parallel' ( (lv_parallelInteraction_2_0= ruleInteraction ) ) (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )* ) ;
    public final EObject ruleParallel() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_parallelInteraction_2_0 = null;

        EObject lv_parallelInteraction_4_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1538:28: ( ( () otherlv_1= 'parallel' ( (lv_parallelInteraction_2_0= ruleInteraction ) ) (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )* ) )
            // InternalCollaboration.g:1539:1: ( () otherlv_1= 'parallel' ( (lv_parallelInteraction_2_0= ruleInteraction ) ) (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )* )
            {
            // InternalCollaboration.g:1539:1: ( () otherlv_1= 'parallel' ( (lv_parallelInteraction_2_0= ruleInteraction ) ) (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )* )
            // InternalCollaboration.g:1539:2: () otherlv_1= 'parallel' ( (lv_parallelInteraction_2_0= ruleInteraction ) ) (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )*
            {
            // InternalCollaboration.g:1539:2: ()
            // InternalCollaboration.g:1540:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getParallelAccess().getParallelAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,44,FollowSets000.FOLLOW_15); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getParallelAccess().getParallelKeyword_1());
                  
            }
            // InternalCollaboration.g:1549:1: ( (lv_parallelInteraction_2_0= ruleInteraction ) )
            // InternalCollaboration.g:1550:1: (lv_parallelInteraction_2_0= ruleInteraction )
            {
            // InternalCollaboration.g:1550:1: (lv_parallelInteraction_2_0= ruleInteraction )
            // InternalCollaboration.g:1551:3: lv_parallelInteraction_2_0= ruleInteraction
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getParallelAccess().getParallelInteractionInteractionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_37);
            lv_parallelInteraction_2_0=ruleInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getParallelRule());
              	        }
                     		add(
                     			current, 
                     			"parallelInteraction",
                      		lv_parallelInteraction_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalCollaboration.g:1567:2: (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==45) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalCollaboration.g:1567:4: otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) )
            	    {
            	    otherlv_3=(Token)match(input,45,FollowSets000.FOLLOW_15); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getParallelAccess().getAndKeyword_3_0());
            	          
            	    }
            	    // InternalCollaboration.g:1571:1: ( (lv_parallelInteraction_4_0= ruleInteraction ) )
            	    // InternalCollaboration.g:1572:1: (lv_parallelInteraction_4_0= ruleInteraction )
            	    {
            	    // InternalCollaboration.g:1572:1: (lv_parallelInteraction_4_0= ruleInteraction )
            	    // InternalCollaboration.g:1573:3: lv_parallelInteraction_4_0= ruleInteraction
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getParallelAccess().getParallelInteractionInteractionParserRuleCall_3_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_37);
            	    lv_parallelInteraction_4_0=ruleInteraction();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getParallelRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"parallelInteraction",
            	              		lv_parallelInteraction_4_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParallel"


    // $ANTLR start "entryRuleTimedConditionFragment"
    // InternalCollaboration.g:1597:1: entryRuleTimedConditionFragment returns [EObject current=null] : iv_ruleTimedConditionFragment= ruleTimedConditionFragment EOF ;
    public final EObject entryRuleTimedConditionFragment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimedConditionFragment = null;


        try {
            // InternalCollaboration.g:1598:2: (iv_ruleTimedConditionFragment= ruleTimedConditionFragment EOF )
            // InternalCollaboration.g:1599:2: iv_ruleTimedConditionFragment= ruleTimedConditionFragment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimedConditionFragmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleTimedConditionFragment=ruleTimedConditionFragment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTimedConditionFragment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimedConditionFragment"


    // $ANTLR start "ruleTimedConditionFragment"
    // InternalCollaboration.g:1606:1: ruleTimedConditionFragment returns [EObject current=null] : (this_TimedWaitCondition_0= ruleTimedWaitCondition | this_TimedInterruptCondition_1= ruleTimedInterruptCondition | this_TimedViolationCondition_2= ruleTimedViolationCondition ) ;
    public final EObject ruleTimedConditionFragment() throws RecognitionException {
        EObject current = null;

        EObject this_TimedWaitCondition_0 = null;

        EObject this_TimedInterruptCondition_1 = null;

        EObject this_TimedViolationCondition_2 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1609:28: ( (this_TimedWaitCondition_0= ruleTimedWaitCondition | this_TimedInterruptCondition_1= ruleTimedInterruptCondition | this_TimedViolationCondition_2= ruleTimedViolationCondition ) )
            // InternalCollaboration.g:1610:1: (this_TimedWaitCondition_0= ruleTimedWaitCondition | this_TimedInterruptCondition_1= ruleTimedInterruptCondition | this_TimedViolationCondition_2= ruleTimedViolationCondition )
            {
            // InternalCollaboration.g:1610:1: (this_TimedWaitCondition_0= ruleTimedWaitCondition | this_TimedInterruptCondition_1= ruleTimedInterruptCondition | this_TimedViolationCondition_2= ruleTimedViolationCondition )
            int alt31=3;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==50) ) {
                switch ( input.LA(2) ) {
                case 46:
                    {
                    alt31=1;
                    }
                    break;
                case 48:
                    {
                    alt31=2;
                    }
                    break;
                case 49:
                    {
                    alt31=3;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 31, 1, input);

                    throw nvae;
                }

            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }
            switch (alt31) {
                case 1 :
                    // InternalCollaboration.g:1611:5: this_TimedWaitCondition_0= ruleTimedWaitCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTimedConditionFragmentAccess().getTimedWaitConditionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_TimedWaitCondition_0=ruleTimedWaitCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TimedWaitCondition_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:1621:5: this_TimedInterruptCondition_1= ruleTimedInterruptCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTimedConditionFragmentAccess().getTimedInterruptConditionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_TimedInterruptCondition_1=ruleTimedInterruptCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TimedInterruptCondition_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:1631:5: this_TimedViolationCondition_2= ruleTimedViolationCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTimedConditionFragmentAccess().getTimedViolationConditionParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_TimedViolationCondition_2=ruleTimedViolationCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TimedViolationCondition_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimedConditionFragment"


    // $ANTLR start "entryRuleConditionFragment"
    // InternalCollaboration.g:1647:1: entryRuleConditionFragment returns [EObject current=null] : iv_ruleConditionFragment= ruleConditionFragment EOF ;
    public final EObject entryRuleConditionFragment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConditionFragment = null;


        try {
            // InternalCollaboration.g:1648:2: (iv_ruleConditionFragment= ruleConditionFragment EOF )
            // InternalCollaboration.g:1649:2: iv_ruleConditionFragment= ruleConditionFragment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConditionFragmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConditionFragment=ruleConditionFragment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConditionFragment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConditionFragment"


    // $ANTLR start "ruleConditionFragment"
    // InternalCollaboration.g:1656:1: ruleConditionFragment returns [EObject current=null] : (this_WaitCondition_0= ruleWaitCondition | this_InterruptCondition_1= ruleInterruptCondition | this_ViolationCondition_2= ruleViolationCondition ) ;
    public final EObject ruleConditionFragment() throws RecognitionException {
        EObject current = null;

        EObject this_WaitCondition_0 = null;

        EObject this_InterruptCondition_1 = null;

        EObject this_ViolationCondition_2 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1659:28: ( (this_WaitCondition_0= ruleWaitCondition | this_InterruptCondition_1= ruleInterruptCondition | this_ViolationCondition_2= ruleViolationCondition ) )
            // InternalCollaboration.g:1660:1: (this_WaitCondition_0= ruleWaitCondition | this_InterruptCondition_1= ruleInterruptCondition | this_ViolationCondition_2= ruleViolationCondition )
            {
            // InternalCollaboration.g:1660:1: (this_WaitCondition_0= ruleWaitCondition | this_InterruptCondition_1= ruleInterruptCondition | this_ViolationCondition_2= ruleViolationCondition )
            int alt32=3;
            switch ( input.LA(1) ) {
            case 46:
                {
                alt32=1;
                }
                break;
            case 48:
                {
                alt32=2;
                }
                break;
            case 49:
                {
                alt32=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 32, 0, input);

                throw nvae;
            }

            switch (alt32) {
                case 1 :
                    // InternalCollaboration.g:1661:5: this_WaitCondition_0= ruleWaitCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getConditionFragmentAccess().getWaitConditionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_WaitCondition_0=ruleWaitCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_WaitCondition_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:1671:5: this_InterruptCondition_1= ruleInterruptCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getConditionFragmentAccess().getInterruptConditionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_InterruptCondition_1=ruleInterruptCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_InterruptCondition_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:1681:5: this_ViolationCondition_2= ruleViolationCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getConditionFragmentAccess().getViolationConditionParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ViolationCondition_2=ruleViolationCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ViolationCondition_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConditionFragment"


    // $ANTLR start "entryRuleWaitCondition"
    // InternalCollaboration.g:1697:1: entryRuleWaitCondition returns [EObject current=null] : iv_ruleWaitCondition= ruleWaitCondition EOF ;
    public final EObject entryRuleWaitCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWaitCondition = null;


        try {
            // InternalCollaboration.g:1698:2: (iv_ruleWaitCondition= ruleWaitCondition EOF )
            // InternalCollaboration.g:1699:2: iv_ruleWaitCondition= ruleWaitCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getWaitConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleWaitCondition=ruleWaitCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleWaitCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWaitCondition"


    // $ANTLR start "ruleWaitCondition"
    // InternalCollaboration.g:1706:1: ruleWaitCondition returns [EObject current=null] : (otherlv_0= 'wait' ( (lv_strict_1_0= 'strict' ) )? ( (lv_requested_2_0= 'eventually' ) )? otherlv_3= '[' ( (lv_conditionExpression_4_0= ruleConditionExpression ) ) otherlv_5= ']' ) ;
    public final EObject ruleWaitCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_strict_1_0=null;
        Token lv_requested_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_conditionExpression_4_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1709:28: ( (otherlv_0= 'wait' ( (lv_strict_1_0= 'strict' ) )? ( (lv_requested_2_0= 'eventually' ) )? otherlv_3= '[' ( (lv_conditionExpression_4_0= ruleConditionExpression ) ) otherlv_5= ']' ) )
            // InternalCollaboration.g:1710:1: (otherlv_0= 'wait' ( (lv_strict_1_0= 'strict' ) )? ( (lv_requested_2_0= 'eventually' ) )? otherlv_3= '[' ( (lv_conditionExpression_4_0= ruleConditionExpression ) ) otherlv_5= ']' )
            {
            // InternalCollaboration.g:1710:1: (otherlv_0= 'wait' ( (lv_strict_1_0= 'strict' ) )? ( (lv_requested_2_0= 'eventually' ) )? otherlv_3= '[' ( (lv_conditionExpression_4_0= ruleConditionExpression ) ) otherlv_5= ']' )
            // InternalCollaboration.g:1710:3: otherlv_0= 'wait' ( (lv_strict_1_0= 'strict' ) )? ( (lv_requested_2_0= 'eventually' ) )? otherlv_3= '[' ( (lv_conditionExpression_4_0= ruleConditionExpression ) ) otherlv_5= ']'
            {
            otherlv_0=(Token)match(input,46,FollowSets000.FOLLOW_38); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getWaitConditionAccess().getWaitKeyword_0());
                  
            }
            // InternalCollaboration.g:1714:1: ( (lv_strict_1_0= 'strict' ) )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==34) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalCollaboration.g:1715:1: (lv_strict_1_0= 'strict' )
                    {
                    // InternalCollaboration.g:1715:1: (lv_strict_1_0= 'strict' )
                    // InternalCollaboration.g:1716:3: lv_strict_1_0= 'strict'
                    {
                    lv_strict_1_0=(Token)match(input,34,FollowSets000.FOLLOW_39); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_strict_1_0, grammarAccess.getWaitConditionAccess().getStrictStrictKeyword_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getWaitConditionRule());
                      	        }
                             		setWithLastConsumed(current, "strict", true, "strict");
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalCollaboration.g:1729:3: ( (lv_requested_2_0= 'eventually' ) )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==47) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalCollaboration.g:1730:1: (lv_requested_2_0= 'eventually' )
                    {
                    // InternalCollaboration.g:1730:1: (lv_requested_2_0= 'eventually' )
                    // InternalCollaboration.g:1731:3: lv_requested_2_0= 'eventually'
                    {
                    lv_requested_2_0=(Token)match(input,47,FollowSets000.FOLLOW_17); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_requested_2_0, grammarAccess.getWaitConditionAccess().getRequestedEventuallyKeyword_2_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getWaitConditionRule());
                      	        }
                             		setWithLastConsumed(current, "requested", true, "eventually");
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,28,FollowSets000.FOLLOW_40); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getWaitConditionAccess().getLeftSquareBracketKeyword_3());
                  
            }
            // InternalCollaboration.g:1748:1: ( (lv_conditionExpression_4_0= ruleConditionExpression ) )
            // InternalCollaboration.g:1749:1: (lv_conditionExpression_4_0= ruleConditionExpression )
            {
            // InternalCollaboration.g:1749:1: (lv_conditionExpression_4_0= ruleConditionExpression )
            // InternalCollaboration.g:1750:3: lv_conditionExpression_4_0= ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getWaitConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_4_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_19);
            lv_conditionExpression_4_0=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getWaitConditionRule());
              	        }
                     		set(
                     			current, 
                     			"conditionExpression",
                      		lv_conditionExpression_4_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_5=(Token)match(input,29,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getWaitConditionAccess().getRightSquareBracketKeyword_5());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWaitCondition"


    // $ANTLR start "entryRuleInterruptCondition"
    // InternalCollaboration.g:1778:1: entryRuleInterruptCondition returns [EObject current=null] : iv_ruleInterruptCondition= ruleInterruptCondition EOF ;
    public final EObject entryRuleInterruptCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInterruptCondition = null;


        try {
            // InternalCollaboration.g:1779:2: (iv_ruleInterruptCondition= ruleInterruptCondition EOF )
            // InternalCollaboration.g:1780:2: iv_ruleInterruptCondition= ruleInterruptCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInterruptConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleInterruptCondition=ruleInterruptCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInterruptCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInterruptCondition"


    // $ANTLR start "ruleInterruptCondition"
    // InternalCollaboration.g:1787:1: ruleInterruptCondition returns [EObject current=null] : (otherlv_0= 'interrupt' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' ) ;
    public final EObject ruleInterruptCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_conditionExpression_2_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1790:28: ( (otherlv_0= 'interrupt' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' ) )
            // InternalCollaboration.g:1791:1: (otherlv_0= 'interrupt' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' )
            {
            // InternalCollaboration.g:1791:1: (otherlv_0= 'interrupt' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' )
            // InternalCollaboration.g:1791:3: otherlv_0= 'interrupt' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']'
            {
            otherlv_0=(Token)match(input,48,FollowSets000.FOLLOW_17); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getInterruptConditionAccess().getInterruptKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,28,FollowSets000.FOLLOW_40); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getInterruptConditionAccess().getLeftSquareBracketKeyword_1());
                  
            }
            // InternalCollaboration.g:1799:1: ( (lv_conditionExpression_2_0= ruleConditionExpression ) )
            // InternalCollaboration.g:1800:1: (lv_conditionExpression_2_0= ruleConditionExpression )
            {
            // InternalCollaboration.g:1800:1: (lv_conditionExpression_2_0= ruleConditionExpression )
            // InternalCollaboration.g:1801:3: lv_conditionExpression_2_0= ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getInterruptConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_19);
            lv_conditionExpression_2_0=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getInterruptConditionRule());
              	        }
                     		set(
                     			current, 
                     			"conditionExpression",
                      		lv_conditionExpression_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,29,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getInterruptConditionAccess().getRightSquareBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInterruptCondition"


    // $ANTLR start "entryRuleViolationCondition"
    // InternalCollaboration.g:1829:1: entryRuleViolationCondition returns [EObject current=null] : iv_ruleViolationCondition= ruleViolationCondition EOF ;
    public final EObject entryRuleViolationCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleViolationCondition = null;


        try {
            // InternalCollaboration.g:1830:2: (iv_ruleViolationCondition= ruleViolationCondition EOF )
            // InternalCollaboration.g:1831:2: iv_ruleViolationCondition= ruleViolationCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getViolationConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleViolationCondition=ruleViolationCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleViolationCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleViolationCondition"


    // $ANTLR start "ruleViolationCondition"
    // InternalCollaboration.g:1838:1: ruleViolationCondition returns [EObject current=null] : (otherlv_0= 'violation' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' ) ;
    public final EObject ruleViolationCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_conditionExpression_2_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1841:28: ( (otherlv_0= 'violation' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' ) )
            // InternalCollaboration.g:1842:1: (otherlv_0= 'violation' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' )
            {
            // InternalCollaboration.g:1842:1: (otherlv_0= 'violation' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' )
            // InternalCollaboration.g:1842:3: otherlv_0= 'violation' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']'
            {
            otherlv_0=(Token)match(input,49,FollowSets000.FOLLOW_17); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getViolationConditionAccess().getViolationKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,28,FollowSets000.FOLLOW_40); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getViolationConditionAccess().getLeftSquareBracketKeyword_1());
                  
            }
            // InternalCollaboration.g:1850:1: ( (lv_conditionExpression_2_0= ruleConditionExpression ) )
            // InternalCollaboration.g:1851:1: (lv_conditionExpression_2_0= ruleConditionExpression )
            {
            // InternalCollaboration.g:1851:1: (lv_conditionExpression_2_0= ruleConditionExpression )
            // InternalCollaboration.g:1852:3: lv_conditionExpression_2_0= ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getViolationConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_19);
            lv_conditionExpression_2_0=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getViolationConditionRule());
              	        }
                     		set(
                     			current, 
                     			"conditionExpression",
                      		lv_conditionExpression_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,29,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getViolationConditionAccess().getRightSquareBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleViolationCondition"


    // $ANTLR start "entryRuleTimedWaitCondition"
    // InternalCollaboration.g:1880:1: entryRuleTimedWaitCondition returns [EObject current=null] : iv_ruleTimedWaitCondition= ruleTimedWaitCondition EOF ;
    public final EObject entryRuleTimedWaitCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimedWaitCondition = null;


        try {
            // InternalCollaboration.g:1881:2: (iv_ruleTimedWaitCondition= ruleTimedWaitCondition EOF )
            // InternalCollaboration.g:1882:2: iv_ruleTimedWaitCondition= ruleTimedWaitCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimedWaitConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleTimedWaitCondition=ruleTimedWaitCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTimedWaitCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimedWaitCondition"


    // $ANTLR start "ruleTimedWaitCondition"
    // InternalCollaboration.g:1889:1: ruleTimedWaitCondition returns [EObject current=null] : (otherlv_0= 'timed' otherlv_1= 'wait' ( (lv_strict_2_0= 'strict' ) )? ( (lv_requested_3_0= 'eventually' ) )? otherlv_4= '[' ( (lv_timedConditionExpression_5_0= ruleTimedExpression ) ) otherlv_6= ']' ) ;
    public final EObject ruleTimedWaitCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_strict_2_0=null;
        Token lv_requested_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_timedConditionExpression_5_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1892:28: ( (otherlv_0= 'timed' otherlv_1= 'wait' ( (lv_strict_2_0= 'strict' ) )? ( (lv_requested_3_0= 'eventually' ) )? otherlv_4= '[' ( (lv_timedConditionExpression_5_0= ruleTimedExpression ) ) otherlv_6= ']' ) )
            // InternalCollaboration.g:1893:1: (otherlv_0= 'timed' otherlv_1= 'wait' ( (lv_strict_2_0= 'strict' ) )? ( (lv_requested_3_0= 'eventually' ) )? otherlv_4= '[' ( (lv_timedConditionExpression_5_0= ruleTimedExpression ) ) otherlv_6= ']' )
            {
            // InternalCollaboration.g:1893:1: (otherlv_0= 'timed' otherlv_1= 'wait' ( (lv_strict_2_0= 'strict' ) )? ( (lv_requested_3_0= 'eventually' ) )? otherlv_4= '[' ( (lv_timedConditionExpression_5_0= ruleTimedExpression ) ) otherlv_6= ']' )
            // InternalCollaboration.g:1893:3: otherlv_0= 'timed' otherlv_1= 'wait' ( (lv_strict_2_0= 'strict' ) )? ( (lv_requested_3_0= 'eventually' ) )? otherlv_4= '[' ( (lv_timedConditionExpression_5_0= ruleTimedExpression ) ) otherlv_6= ']'
            {
            otherlv_0=(Token)match(input,50,FollowSets000.FOLLOW_41); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getTimedWaitConditionAccess().getTimedKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,46,FollowSets000.FOLLOW_38); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getTimedWaitConditionAccess().getWaitKeyword_1());
                  
            }
            // InternalCollaboration.g:1901:1: ( (lv_strict_2_0= 'strict' ) )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==34) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalCollaboration.g:1902:1: (lv_strict_2_0= 'strict' )
                    {
                    // InternalCollaboration.g:1902:1: (lv_strict_2_0= 'strict' )
                    // InternalCollaboration.g:1903:3: lv_strict_2_0= 'strict'
                    {
                    lv_strict_2_0=(Token)match(input,34,FollowSets000.FOLLOW_39); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_strict_2_0, grammarAccess.getTimedWaitConditionAccess().getStrictStrictKeyword_2_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getTimedWaitConditionRule());
                      	        }
                             		setWithLastConsumed(current, "strict", true, "strict");
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalCollaboration.g:1916:3: ( (lv_requested_3_0= 'eventually' ) )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==47) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalCollaboration.g:1917:1: (lv_requested_3_0= 'eventually' )
                    {
                    // InternalCollaboration.g:1917:1: (lv_requested_3_0= 'eventually' )
                    // InternalCollaboration.g:1918:3: lv_requested_3_0= 'eventually'
                    {
                    lv_requested_3_0=(Token)match(input,47,FollowSets000.FOLLOW_17); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_requested_3_0, grammarAccess.getTimedWaitConditionAccess().getRequestedEventuallyKeyword_3_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getTimedWaitConditionRule());
                      	        }
                             		setWithLastConsumed(current, "requested", true, "eventually");
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,28,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getTimedWaitConditionAccess().getLeftSquareBracketKeyword_4());
                  
            }
            // InternalCollaboration.g:1935:1: ( (lv_timedConditionExpression_5_0= ruleTimedExpression ) )
            // InternalCollaboration.g:1936:1: (lv_timedConditionExpression_5_0= ruleTimedExpression )
            {
            // InternalCollaboration.g:1936:1: (lv_timedConditionExpression_5_0= ruleTimedExpression )
            // InternalCollaboration.g:1937:3: lv_timedConditionExpression_5_0= ruleTimedExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getTimedWaitConditionAccess().getTimedConditionExpressionTimedExpressionParserRuleCall_5_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_19);
            lv_timedConditionExpression_5_0=ruleTimedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getTimedWaitConditionRule());
              	        }
                     		set(
                     			current, 
                     			"timedConditionExpression",
                      		lv_timedConditionExpression_5_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.TimedExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_6=(Token)match(input,29,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getTimedWaitConditionAccess().getRightSquareBracketKeyword_6());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimedWaitCondition"


    // $ANTLR start "entryRuleTimedViolationCondition"
    // InternalCollaboration.g:1965:1: entryRuleTimedViolationCondition returns [EObject current=null] : iv_ruleTimedViolationCondition= ruleTimedViolationCondition EOF ;
    public final EObject entryRuleTimedViolationCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimedViolationCondition = null;


        try {
            // InternalCollaboration.g:1966:2: (iv_ruleTimedViolationCondition= ruleTimedViolationCondition EOF )
            // InternalCollaboration.g:1967:2: iv_ruleTimedViolationCondition= ruleTimedViolationCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimedViolationConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleTimedViolationCondition=ruleTimedViolationCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTimedViolationCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimedViolationCondition"


    // $ANTLR start "ruleTimedViolationCondition"
    // InternalCollaboration.g:1974:1: ruleTimedViolationCondition returns [EObject current=null] : (otherlv_0= 'timed' otherlv_1= 'violation' otherlv_2= '[' ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) ) otherlv_4= ']' ) ;
    public final EObject ruleTimedViolationCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_timedConditionExpression_3_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1977:28: ( (otherlv_0= 'timed' otherlv_1= 'violation' otherlv_2= '[' ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) ) otherlv_4= ']' ) )
            // InternalCollaboration.g:1978:1: (otherlv_0= 'timed' otherlv_1= 'violation' otherlv_2= '[' ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) ) otherlv_4= ']' )
            {
            // InternalCollaboration.g:1978:1: (otherlv_0= 'timed' otherlv_1= 'violation' otherlv_2= '[' ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) ) otherlv_4= ']' )
            // InternalCollaboration.g:1978:3: otherlv_0= 'timed' otherlv_1= 'violation' otherlv_2= '[' ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) ) otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,50,FollowSets000.FOLLOW_42); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getTimedViolationConditionAccess().getTimedKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,49,FollowSets000.FOLLOW_17); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getTimedViolationConditionAccess().getViolationKeyword_1());
                  
            }
            otherlv_2=(Token)match(input,28,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getTimedViolationConditionAccess().getLeftSquareBracketKeyword_2());
                  
            }
            // InternalCollaboration.g:1990:1: ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) )
            // InternalCollaboration.g:1991:1: (lv_timedConditionExpression_3_0= ruleTimedExpression )
            {
            // InternalCollaboration.g:1991:1: (lv_timedConditionExpression_3_0= ruleTimedExpression )
            // InternalCollaboration.g:1992:3: lv_timedConditionExpression_3_0= ruleTimedExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getTimedViolationConditionAccess().getTimedConditionExpressionTimedExpressionParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_19);
            lv_timedConditionExpression_3_0=ruleTimedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getTimedViolationConditionRule());
              	        }
                     		set(
                     			current, 
                     			"timedConditionExpression",
                      		lv_timedConditionExpression_3_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.TimedExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,29,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getTimedViolationConditionAccess().getRightSquareBracketKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimedViolationCondition"


    // $ANTLR start "entryRuleTimedInterruptCondition"
    // InternalCollaboration.g:2020:1: entryRuleTimedInterruptCondition returns [EObject current=null] : iv_ruleTimedInterruptCondition= ruleTimedInterruptCondition EOF ;
    public final EObject entryRuleTimedInterruptCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimedInterruptCondition = null;


        try {
            // InternalCollaboration.g:2021:2: (iv_ruleTimedInterruptCondition= ruleTimedInterruptCondition EOF )
            // InternalCollaboration.g:2022:2: iv_ruleTimedInterruptCondition= ruleTimedInterruptCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimedInterruptConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleTimedInterruptCondition=ruleTimedInterruptCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTimedInterruptCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimedInterruptCondition"


    // $ANTLR start "ruleTimedInterruptCondition"
    // InternalCollaboration.g:2029:1: ruleTimedInterruptCondition returns [EObject current=null] : (otherlv_0= 'timed' otherlv_1= 'interrupt' otherlv_2= '[' ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) ) otherlv_4= ']' ) ;
    public final EObject ruleTimedInterruptCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_timedConditionExpression_3_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2032:28: ( (otherlv_0= 'timed' otherlv_1= 'interrupt' otherlv_2= '[' ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) ) otherlv_4= ']' ) )
            // InternalCollaboration.g:2033:1: (otherlv_0= 'timed' otherlv_1= 'interrupt' otherlv_2= '[' ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) ) otherlv_4= ']' )
            {
            // InternalCollaboration.g:2033:1: (otherlv_0= 'timed' otherlv_1= 'interrupt' otherlv_2= '[' ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) ) otherlv_4= ']' )
            // InternalCollaboration.g:2033:3: otherlv_0= 'timed' otherlv_1= 'interrupt' otherlv_2= '[' ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) ) otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,50,FollowSets000.FOLLOW_43); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getTimedInterruptConditionAccess().getTimedKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,48,FollowSets000.FOLLOW_17); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getTimedInterruptConditionAccess().getInterruptKeyword_1());
                  
            }
            otherlv_2=(Token)match(input,28,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getTimedInterruptConditionAccess().getLeftSquareBracketKeyword_2());
                  
            }
            // InternalCollaboration.g:2045:1: ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) )
            // InternalCollaboration.g:2046:1: (lv_timedConditionExpression_3_0= ruleTimedExpression )
            {
            // InternalCollaboration.g:2046:1: (lv_timedConditionExpression_3_0= ruleTimedExpression )
            // InternalCollaboration.g:2047:3: lv_timedConditionExpression_3_0= ruleTimedExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getTimedInterruptConditionAccess().getTimedConditionExpressionTimedExpressionParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_19);
            lv_timedConditionExpression_3_0=ruleTimedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getTimedInterruptConditionRule());
              	        }
                     		set(
                     			current, 
                     			"timedConditionExpression",
                      		lv_timedConditionExpression_3_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.TimedExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,29,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getTimedInterruptConditionAccess().getRightSquareBracketKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimedInterruptCondition"


    // $ANTLR start "entryRuleCondition"
    // InternalCollaboration.g:2075:1: entryRuleCondition returns [EObject current=null] : iv_ruleCondition= ruleCondition EOF ;
    public final EObject entryRuleCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCondition = null;


        try {
            // InternalCollaboration.g:2076:2: (iv_ruleCondition= ruleCondition EOF )
            // InternalCollaboration.g:2077:2: iv_ruleCondition= ruleCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCondition=ruleCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCondition"


    // $ANTLR start "ruleCondition"
    // InternalCollaboration.g:2084:1: ruleCondition returns [EObject current=null] : (otherlv_0= '[' ( (lv_conditionExpression_1_0= ruleConditionExpression ) ) otherlv_2= ']' ) ;
    public final EObject ruleCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_conditionExpression_1_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2087:28: ( (otherlv_0= '[' ( (lv_conditionExpression_1_0= ruleConditionExpression ) ) otherlv_2= ']' ) )
            // InternalCollaboration.g:2088:1: (otherlv_0= '[' ( (lv_conditionExpression_1_0= ruleConditionExpression ) ) otherlv_2= ']' )
            {
            // InternalCollaboration.g:2088:1: (otherlv_0= '[' ( (lv_conditionExpression_1_0= ruleConditionExpression ) ) otherlv_2= ']' )
            // InternalCollaboration.g:2088:3: otherlv_0= '[' ( (lv_conditionExpression_1_0= ruleConditionExpression ) ) otherlv_2= ']'
            {
            otherlv_0=(Token)match(input,28,FollowSets000.FOLLOW_40); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getConditionAccess().getLeftSquareBracketKeyword_0());
                  
            }
            // InternalCollaboration.g:2092:1: ( (lv_conditionExpression_1_0= ruleConditionExpression ) )
            // InternalCollaboration.g:2093:1: (lv_conditionExpression_1_0= ruleConditionExpression )
            {
            // InternalCollaboration.g:2093:1: (lv_conditionExpression_1_0= ruleConditionExpression )
            // InternalCollaboration.g:2094:3: lv_conditionExpression_1_0= ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_19);
            lv_conditionExpression_1_0=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getConditionRule());
              	        }
                     		set(
                     			current, 
                     			"conditionExpression",
                      		lv_conditionExpression_1_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,29,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getConditionAccess().getRightSquareBracketKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCondition"


    // $ANTLR start "entryRuleConditionExpression"
    // InternalCollaboration.g:2122:1: entryRuleConditionExpression returns [EObject current=null] : iv_ruleConditionExpression= ruleConditionExpression EOF ;
    public final EObject entryRuleConditionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConditionExpression = null;


        try {
            // InternalCollaboration.g:2123:2: (iv_ruleConditionExpression= ruleConditionExpression EOF )
            // InternalCollaboration.g:2124:2: iv_ruleConditionExpression= ruleConditionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConditionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConditionExpression=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConditionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConditionExpression"


    // $ANTLR start "ruleConditionExpression"
    // InternalCollaboration.g:2131:1: ruleConditionExpression returns [EObject current=null] : ( (lv_expression_0_0= ruleExpression ) ) ;
    public final EObject ruleConditionExpression() throws RecognitionException {
        EObject current = null;

        EObject lv_expression_0_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2134:28: ( ( (lv_expression_0_0= ruleExpression ) ) )
            // InternalCollaboration.g:2135:1: ( (lv_expression_0_0= ruleExpression ) )
            {
            // InternalCollaboration.g:2135:1: ( (lv_expression_0_0= ruleExpression ) )
            // InternalCollaboration.g:2136:1: (lv_expression_0_0= ruleExpression )
            {
            // InternalCollaboration.g:2136:1: (lv_expression_0_0= ruleExpression )
            // InternalCollaboration.g:2137:3: lv_expression_0_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getConditionExpressionAccess().getExpressionExpressionParserRuleCall_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_expression_0_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getConditionExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConditionExpression"


    // $ANTLR start "entryRuleConstraintBlock"
    // InternalCollaboration.g:2161:1: entryRuleConstraintBlock returns [EObject current=null] : iv_ruleConstraintBlock= ruleConstraintBlock EOF ;
    public final EObject entryRuleConstraintBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstraintBlock = null;


        try {
            // InternalCollaboration.g:2162:2: (iv_ruleConstraintBlock= ruleConstraintBlock EOF )
            // InternalCollaboration.g:2163:2: iv_ruleConstraintBlock= ruleConstraintBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConstraintBlockRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConstraintBlock=ruleConstraintBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConstraintBlock; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstraintBlock"


    // $ANTLR start "ruleConstraintBlock"
    // InternalCollaboration.g:2170:1: ruleConstraintBlock returns [EObject current=null] : ( () otherlv_1= 'constraints' otherlv_2= '[' ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )* otherlv_11= ']' ) ;
    public final EObject ruleConstraintBlock() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        EObject lv_consider_4_0 = null;

        EObject lv_ignore_6_0 = null;

        EObject lv_forbidden_8_0 = null;

        EObject lv_interrupt_10_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2173:28: ( ( () otherlv_1= 'constraints' otherlv_2= '[' ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )* otherlv_11= ']' ) )
            // InternalCollaboration.g:2174:1: ( () otherlv_1= 'constraints' otherlv_2= '[' ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )* otherlv_11= ']' )
            {
            // InternalCollaboration.g:2174:1: ( () otherlv_1= 'constraints' otherlv_2= '[' ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )* otherlv_11= ']' )
            // InternalCollaboration.g:2174:2: () otherlv_1= 'constraints' otherlv_2= '[' ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )* otherlv_11= ']'
            {
            // InternalCollaboration.g:2174:2: ()
            // InternalCollaboration.g:2175:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getConstraintBlockAccess().getConstraintBlockAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,51,FollowSets000.FOLLOW_17); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getConstraintBlockAccess().getConstraintsKeyword_1());
                  
            }
            otherlv_2=(Token)match(input,28,FollowSets000.FOLLOW_44); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getConstraintBlockAccess().getLeftSquareBracketKeyword_2());
                  
            }
            // InternalCollaboration.g:2188:1: ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )*
            loop37:
            do {
                int alt37=5;
                switch ( input.LA(1) ) {
                case 52:
                    {
                    alt37=1;
                    }
                    break;
                case 53:
                    {
                    alt37=2;
                    }
                    break;
                case 54:
                    {
                    alt37=3;
                    }
                    break;
                case 48:
                    {
                    alt37=4;
                    }
                    break;

                }

                switch (alt37) {
            	case 1 :
            	    // InternalCollaboration.g:2188:2: (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) )
            	    {
            	    // InternalCollaboration.g:2188:2: (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) )
            	    // InternalCollaboration.g:2188:4: otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) )
            	    {
            	    otherlv_3=(Token)match(input,52,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getConstraintBlockAccess().getConsiderKeyword_3_0_0());
            	          
            	    }
            	    // InternalCollaboration.g:2192:1: ( (lv_consider_4_0= ruleConstraintMessage ) )
            	    // InternalCollaboration.g:2193:1: (lv_consider_4_0= ruleConstraintMessage )
            	    {
            	    // InternalCollaboration.g:2193:1: (lv_consider_4_0= ruleConstraintMessage )
            	    // InternalCollaboration.g:2194:3: lv_consider_4_0= ruleConstraintMessage
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getConstraintBlockAccess().getConsiderConstraintMessageParserRuleCall_3_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_44);
            	    lv_consider_4_0=ruleConstraintMessage();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getConstraintBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"consider",
            	              		lv_consider_4_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalCollaboration.g:2211:6: (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) )
            	    {
            	    // InternalCollaboration.g:2211:6: (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) )
            	    // InternalCollaboration.g:2211:8: otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) )
            	    {
            	    otherlv_5=(Token)match(input,53,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_5, grammarAccess.getConstraintBlockAccess().getIgnoreKeyword_3_1_0());
            	          
            	    }
            	    // InternalCollaboration.g:2215:1: ( (lv_ignore_6_0= ruleConstraintMessage ) )
            	    // InternalCollaboration.g:2216:1: (lv_ignore_6_0= ruleConstraintMessage )
            	    {
            	    // InternalCollaboration.g:2216:1: (lv_ignore_6_0= ruleConstraintMessage )
            	    // InternalCollaboration.g:2217:3: lv_ignore_6_0= ruleConstraintMessage
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getConstraintBlockAccess().getIgnoreConstraintMessageParserRuleCall_3_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_44);
            	    lv_ignore_6_0=ruleConstraintMessage();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getConstraintBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"ignore",
            	              		lv_ignore_6_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalCollaboration.g:2234:6: (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) )
            	    {
            	    // InternalCollaboration.g:2234:6: (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) )
            	    // InternalCollaboration.g:2234:8: otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) )
            	    {
            	    otherlv_7=(Token)match(input,54,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_7, grammarAccess.getConstraintBlockAccess().getForbiddenKeyword_3_2_0());
            	          
            	    }
            	    // InternalCollaboration.g:2238:1: ( (lv_forbidden_8_0= ruleConstraintMessage ) )
            	    // InternalCollaboration.g:2239:1: (lv_forbidden_8_0= ruleConstraintMessage )
            	    {
            	    // InternalCollaboration.g:2239:1: (lv_forbidden_8_0= ruleConstraintMessage )
            	    // InternalCollaboration.g:2240:3: lv_forbidden_8_0= ruleConstraintMessage
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getConstraintBlockAccess().getForbiddenConstraintMessageParserRuleCall_3_2_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_44);
            	    lv_forbidden_8_0=ruleConstraintMessage();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getConstraintBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"forbidden",
            	              		lv_forbidden_8_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalCollaboration.g:2257:6: (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) )
            	    {
            	    // InternalCollaboration.g:2257:6: (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) )
            	    // InternalCollaboration.g:2257:8: otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) )
            	    {
            	    otherlv_9=(Token)match(input,48,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_9, grammarAccess.getConstraintBlockAccess().getInterruptKeyword_3_3_0());
            	          
            	    }
            	    // InternalCollaboration.g:2261:1: ( (lv_interrupt_10_0= ruleConstraintMessage ) )
            	    // InternalCollaboration.g:2262:1: (lv_interrupt_10_0= ruleConstraintMessage )
            	    {
            	    // InternalCollaboration.g:2262:1: (lv_interrupt_10_0= ruleConstraintMessage )
            	    // InternalCollaboration.g:2263:3: lv_interrupt_10_0= ruleConstraintMessage
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getConstraintBlockAccess().getInterruptConstraintMessageParserRuleCall_3_3_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_44);
            	    lv_interrupt_10_0=ruleConstraintMessage();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getConstraintBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"interrupt",
            	              		lv_interrupt_10_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop37;
                }
            } while (true);

            otherlv_11=(Token)match(input,29,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_11, grammarAccess.getConstraintBlockAccess().getRightSquareBracketKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstraintBlock"


    // $ANTLR start "entryRuleConstraintMessage"
    // InternalCollaboration.g:2291:1: entryRuleConstraintMessage returns [EObject current=null] : iv_ruleConstraintMessage= ruleConstraintMessage EOF ;
    public final EObject entryRuleConstraintMessage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstraintMessage = null;


        try {
            // InternalCollaboration.g:2292:2: (iv_ruleConstraintMessage= ruleConstraintMessage EOF )
            // InternalCollaboration.g:2293:2: iv_ruleConstraintMessage= ruleConstraintMessage EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConstraintMessageRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConstraintMessage=ruleConstraintMessage();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConstraintMessage; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstraintMessage"


    // $ANTLR start "ruleConstraintMessage"
    // InternalCollaboration.g:2300:1: ruleConstraintMessage returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) otherlv_3= '.' ( (otherlv_4= RULE_ID ) ) (otherlv_5= '.' ( (lv_collectionModification_6_0= ruleCollectionModification ) ) )? otherlv_7= '(' ( ( (lv_parameters_8_0= ruleParameterBinding ) ) (otherlv_9= ',' ( (lv_parameters_10_0= ruleParameterBinding ) ) )* )? otherlv_11= ')' ) ;
    public final EObject ruleConstraintMessage() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Enumerator lv_collectionModification_6_0 = null;

        EObject lv_parameters_8_0 = null;

        EObject lv_parameters_10_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2303:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) otherlv_3= '.' ( (otherlv_4= RULE_ID ) ) (otherlv_5= '.' ( (lv_collectionModification_6_0= ruleCollectionModification ) ) )? otherlv_7= '(' ( ( (lv_parameters_8_0= ruleParameterBinding ) ) (otherlv_9= ',' ( (lv_parameters_10_0= ruleParameterBinding ) ) )* )? otherlv_11= ')' ) )
            // InternalCollaboration.g:2304:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) otherlv_3= '.' ( (otherlv_4= RULE_ID ) ) (otherlv_5= '.' ( (lv_collectionModification_6_0= ruleCollectionModification ) ) )? otherlv_7= '(' ( ( (lv_parameters_8_0= ruleParameterBinding ) ) (otherlv_9= ',' ( (lv_parameters_10_0= ruleParameterBinding ) ) )* )? otherlv_11= ')' )
            {
            // InternalCollaboration.g:2304:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) otherlv_3= '.' ( (otherlv_4= RULE_ID ) ) (otherlv_5= '.' ( (lv_collectionModification_6_0= ruleCollectionModification ) ) )? otherlv_7= '(' ( ( (lv_parameters_8_0= ruleParameterBinding ) ) (otherlv_9= ',' ( (lv_parameters_10_0= ruleParameterBinding ) ) )* )? otherlv_11= ')' )
            // InternalCollaboration.g:2304:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) otherlv_3= '.' ( (otherlv_4= RULE_ID ) ) (otherlv_5= '.' ( (lv_collectionModification_6_0= ruleCollectionModification ) ) )? otherlv_7= '(' ( ( (lv_parameters_8_0= ruleParameterBinding ) ) (otherlv_9= ',' ( (lv_parameters_10_0= ruleParameterBinding ) ) )* )? otherlv_11= ')'
            {
            // InternalCollaboration.g:2304:2: ( (otherlv_0= RULE_ID ) )
            // InternalCollaboration.g:2305:1: (otherlv_0= RULE_ID )
            {
            // InternalCollaboration.g:2305:1: (otherlv_0= RULE_ID )
            // InternalCollaboration.g:2306:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getConstraintMessageRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getConstraintMessageAccess().getSenderRoleCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,36,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getConstraintMessageAccess().getHyphenMinusGreaterThanSignKeyword_1());
                  
            }
            // InternalCollaboration.g:2321:1: ( (otherlv_2= RULE_ID ) )
            // InternalCollaboration.g:2322:1: (otherlv_2= RULE_ID )
            {
            // InternalCollaboration.g:2322:1: (otherlv_2= RULE_ID )
            // InternalCollaboration.g:2323:3: otherlv_2= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getConstraintMessageRule());
              	        }
                      
            }
            otherlv_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_28); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_2, grammarAccess.getConstraintMessageAccess().getReceiverRoleCrossReference_2_0()); 
              	
            }

            }


            }

            otherlv_3=(Token)match(input,19,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getConstraintMessageAccess().getFullStopKeyword_3());
                  
            }
            // InternalCollaboration.g:2338:1: ( (otherlv_4= RULE_ID ) )
            // InternalCollaboration.g:2339:1: (otherlv_4= RULE_ID )
            {
            // InternalCollaboration.g:2339:1: (otherlv_4= RULE_ID )
            // InternalCollaboration.g:2340:3: otherlv_4= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getConstraintMessageRule());
              	        }
                      
            }
            otherlv_4=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_45); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_4, grammarAccess.getConstraintMessageAccess().getModelElementETypedElementCrossReference_4_0()); 
              	
            }

            }


            }

            // InternalCollaboration.g:2351:2: (otherlv_5= '.' ( (lv_collectionModification_6_0= ruleCollectionModification ) ) )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==19) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // InternalCollaboration.g:2351:4: otherlv_5= '.' ( (lv_collectionModification_6_0= ruleCollectionModification ) )
                    {
                    otherlv_5=(Token)match(input,19,FollowSets000.FOLLOW_30); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_5, grammarAccess.getConstraintMessageAccess().getFullStopKeyword_5_0());
                          
                    }
                    // InternalCollaboration.g:2355:1: ( (lv_collectionModification_6_0= ruleCollectionModification ) )
                    // InternalCollaboration.g:2356:1: (lv_collectionModification_6_0= ruleCollectionModification )
                    {
                    // InternalCollaboration.g:2356:1: (lv_collectionModification_6_0= ruleCollectionModification )
                    // InternalCollaboration.g:2357:3: lv_collectionModification_6_0= ruleCollectionModification
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getConstraintMessageAccess().getCollectionModificationCollectionModificationEnumRuleCall_5_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_46);
                    lv_collectionModification_6_0=ruleCollectionModification();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getConstraintMessageRule());
                      	        }
                             		set(
                             			current, 
                             			"collectionModification",
                              		lv_collectionModification_6_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionModification");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_7=(Token)match(input,37,FollowSets000.FOLLOW_32); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_7, grammarAccess.getConstraintMessageAccess().getLeftParenthesisKeyword_6());
                  
            }
            // InternalCollaboration.g:2377:1: ( ( (lv_parameters_8_0= ruleParameterBinding ) ) (otherlv_9= ',' ( (lv_parameters_10_0= ruleParameterBinding ) ) )* )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==RULE_ID||(LA40_0>=RULE_STRING && LA40_0<=RULE_BOOL)||LA40_0==37||(LA40_0>=39 && LA40_0<=40)||LA40_0==70||LA40_0==72||LA40_0==74) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalCollaboration.g:2377:2: ( (lv_parameters_8_0= ruleParameterBinding ) ) (otherlv_9= ',' ( (lv_parameters_10_0= ruleParameterBinding ) ) )*
                    {
                    // InternalCollaboration.g:2377:2: ( (lv_parameters_8_0= ruleParameterBinding ) )
                    // InternalCollaboration.g:2378:1: (lv_parameters_8_0= ruleParameterBinding )
                    {
                    // InternalCollaboration.g:2378:1: (lv_parameters_8_0= ruleParameterBinding )
                    // InternalCollaboration.g:2379:3: lv_parameters_8_0= ruleParameterBinding
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getConstraintMessageAccess().getParametersParameterBindingParserRuleCall_7_0_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_33);
                    lv_parameters_8_0=ruleParameterBinding();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getConstraintMessageRule());
                      	        }
                             		add(
                             			current, 
                             			"parameters",
                              		lv_parameters_8_0, 
                              		"org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // InternalCollaboration.g:2395:2: (otherlv_9= ',' ( (lv_parameters_10_0= ruleParameterBinding ) ) )*
                    loop39:
                    do {
                        int alt39=2;
                        int LA39_0 = input.LA(1);

                        if ( (LA39_0==31) ) {
                            alt39=1;
                        }


                        switch (alt39) {
                    	case 1 :
                    	    // InternalCollaboration.g:2395:4: otherlv_9= ',' ( (lv_parameters_10_0= ruleParameterBinding ) )
                    	    {
                    	    otherlv_9=(Token)match(input,31,FollowSets000.FOLLOW_34); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_9, grammarAccess.getConstraintMessageAccess().getCommaKeyword_7_1_0());
                    	          
                    	    }
                    	    // InternalCollaboration.g:2399:1: ( (lv_parameters_10_0= ruleParameterBinding ) )
                    	    // InternalCollaboration.g:2400:1: (lv_parameters_10_0= ruleParameterBinding )
                    	    {
                    	    // InternalCollaboration.g:2400:1: (lv_parameters_10_0= ruleParameterBinding )
                    	    // InternalCollaboration.g:2401:3: lv_parameters_10_0= ruleParameterBinding
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getConstraintMessageAccess().getParametersParameterBindingParserRuleCall_7_1_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_33);
                    	    lv_parameters_10_0=ruleParameterBinding();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getConstraintMessageRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"parameters",
                    	              		lv_parameters_10_0, 
                    	              		"org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop39;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_11=(Token)match(input,38,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_11, grammarAccess.getConstraintMessageAccess().getRightParenthesisKeyword_8());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstraintMessage"


    // $ANTLR start "entryRuleImport"
    // InternalCollaboration.g:2431:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalCollaboration.g:2432:2: (iv_ruleImport= ruleImport EOF )
            // InternalCollaboration.g:2433:2: iv_ruleImport= ruleImport EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getImportRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleImport; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalCollaboration.g:2440:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_importURI_1_0=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:2443:28: ( (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) )
            // InternalCollaboration.g:2444:1: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            {
            // InternalCollaboration.g:2444:1: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            // InternalCollaboration.g:2444:3: otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,55,FollowSets000.FOLLOW_47); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
                  
            }
            // InternalCollaboration.g:2448:1: ( (lv_importURI_1_0= RULE_STRING ) )
            // InternalCollaboration.g:2449:1: (lv_importURI_1_0= RULE_STRING )
            {
            // InternalCollaboration.g:2449:1: (lv_importURI_1_0= RULE_STRING )
            // InternalCollaboration.g:2450:3: lv_importURI_1_0= RULE_STRING
            {
            lv_importURI_1_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_importURI_1_0, grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getImportRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"importURI",
                      		lv_importURI_1_0, 
                      		"org.eclipse.xtext.common.Terminals.STRING");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleExpressionRegion"
    // InternalCollaboration.g:2474:1: entryRuleExpressionRegion returns [EObject current=null] : iv_ruleExpressionRegion= ruleExpressionRegion EOF ;
    public final EObject entryRuleExpressionRegion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionRegion = null;


        try {
            // InternalCollaboration.g:2475:2: (iv_ruleExpressionRegion= ruleExpressionRegion EOF )
            // InternalCollaboration.g:2476:2: iv_ruleExpressionRegion= ruleExpressionRegion EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionRegionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpressionRegion=ruleExpressionRegion();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionRegion; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionRegion"


    // $ANTLR start "ruleExpressionRegion"
    // InternalCollaboration.g:2483:1: ruleExpressionRegion returns [EObject current=null] : ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' ) ;
    public final EObject ruleExpressionRegion() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject lv_expressions_2_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2486:28: ( ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' ) )
            // InternalCollaboration.g:2487:1: ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' )
            {
            // InternalCollaboration.g:2487:1: ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' )
            // InternalCollaboration.g:2487:2: () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}'
            {
            // InternalCollaboration.g:2487:2: ()
            // InternalCollaboration.g:2488:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getExpressionRegionAccess().getExpressionRegionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,17,FollowSets000.FOLLOW_48); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getExpressionRegionAccess().getLeftCurlyBracketKeyword_1());
                  
            }
            // InternalCollaboration.g:2497:1: ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )*
            loop41:
            do {
                int alt41=2;
                int LA41_0 = input.LA(1);

                if ( (LA41_0==RULE_ID||(LA41_0>=RULE_STRING && LA41_0<=RULE_BOOL)||LA41_0==17||LA41_0==37||(LA41_0>=57 && LA41_0<=59)||LA41_0==70||LA41_0==72||LA41_0==74) ) {
                    alt41=1;
                }


                switch (alt41) {
            	case 1 :
            	    // InternalCollaboration.g:2497:2: ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';'
            	    {
            	    // InternalCollaboration.g:2497:2: ( (lv_expressions_2_0= ruleExpressionOrRegion ) )
            	    // InternalCollaboration.g:2498:1: (lv_expressions_2_0= ruleExpressionOrRegion )
            	    {
            	    // InternalCollaboration.g:2498:1: (lv_expressions_2_0= ruleExpressionOrRegion )
            	    // InternalCollaboration.g:2499:3: lv_expressions_2_0= ruleExpressionOrRegion
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getExpressionRegionAccess().getExpressionsExpressionOrRegionParserRuleCall_2_0_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_49);
            	    lv_expressions_2_0=ruleExpressionOrRegion();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getExpressionRegionRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"expressions",
            	              		lv_expressions_2_0, 
            	              		"org.scenariotools.sml.expressions.ScenarioExpressions.ExpressionOrRegion");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }

            	    otherlv_3=(Token)match(input,56,FollowSets000.FOLLOW_48); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getExpressionRegionAccess().getSemicolonKeyword_2_1());
            	          
            	    }

            	    }
            	    break;

            	default :
            	    break loop41;
                }
            } while (true);

            otherlv_4=(Token)match(input,18,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getExpressionRegionAccess().getRightCurlyBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionRegion"


    // $ANTLR start "entryRuleExpressionOrRegion"
    // InternalCollaboration.g:2531:1: entryRuleExpressionOrRegion returns [EObject current=null] : iv_ruleExpressionOrRegion= ruleExpressionOrRegion EOF ;
    public final EObject entryRuleExpressionOrRegion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionOrRegion = null;


        try {
            // InternalCollaboration.g:2532:2: (iv_ruleExpressionOrRegion= ruleExpressionOrRegion EOF )
            // InternalCollaboration.g:2533:2: iv_ruleExpressionOrRegion= ruleExpressionOrRegion EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionOrRegionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpressionOrRegion=ruleExpressionOrRegion();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionOrRegion; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionOrRegion"


    // $ANTLR start "ruleExpressionOrRegion"
    // InternalCollaboration.g:2540:1: ruleExpressionOrRegion returns [EObject current=null] : (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables ) ;
    public final EObject ruleExpressionOrRegion() throws RecognitionException {
        EObject current = null;

        EObject this_ExpressionRegion_0 = null;

        EObject this_ExpressionAndVariables_1 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2543:28: ( (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables ) )
            // InternalCollaboration.g:2544:1: (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables )
            {
            // InternalCollaboration.g:2544:1: (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables )
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==17) ) {
                alt42=1;
            }
            else if ( (LA42_0==RULE_ID||(LA42_0>=RULE_STRING && LA42_0<=RULE_BOOL)||LA42_0==37||(LA42_0>=57 && LA42_0<=59)||LA42_0==70||LA42_0==72||LA42_0==74) ) {
                alt42=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 42, 0, input);

                throw nvae;
            }
            switch (alt42) {
                case 1 :
                    // InternalCollaboration.g:2545:5: this_ExpressionRegion_0= ruleExpressionRegion
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionOrRegionAccess().getExpressionRegionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ExpressionRegion_0=ruleExpressionRegion();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ExpressionRegion_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:2555:5: this_ExpressionAndVariables_1= ruleExpressionAndVariables
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionOrRegionAccess().getExpressionAndVariablesParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ExpressionAndVariables_1=ruleExpressionAndVariables();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ExpressionAndVariables_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionOrRegion"


    // $ANTLR start "entryRuleExpressionAndVariables"
    // InternalCollaboration.g:2571:1: entryRuleExpressionAndVariables returns [EObject current=null] : iv_ruleExpressionAndVariables= ruleExpressionAndVariables EOF ;
    public final EObject entryRuleExpressionAndVariables() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionAndVariables = null;


        try {
            // InternalCollaboration.g:2572:2: (iv_ruleExpressionAndVariables= ruleExpressionAndVariables EOF )
            // InternalCollaboration.g:2573:2: iv_ruleExpressionAndVariables= ruleExpressionAndVariables EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionAndVariablesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpressionAndVariables=ruleExpressionAndVariables();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionAndVariables; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionAndVariables"


    // $ANTLR start "ruleExpressionAndVariables"
    // InternalCollaboration.g:2580:1: ruleExpressionAndVariables returns [EObject current=null] : (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression ) ;
    public final EObject ruleExpressionAndVariables() throws RecognitionException {
        EObject current = null;

        EObject this_VariableExpression_0 = null;

        EObject this_Expression_1 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2583:28: ( (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression ) )
            // InternalCollaboration.g:2584:1: (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression )
            {
            // InternalCollaboration.g:2584:1: (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression )
            int alt43=2;
            switch ( input.LA(1) ) {
            case 57:
            case 58:
            case 59:
                {
                alt43=1;
                }
                break;
            case RULE_ID:
                {
                int LA43_2 = input.LA(2);

                if ( (LA43_2==EOF||LA43_2==19||LA43_2==39||LA43_2==56||(LA43_2>=60 && LA43_2<=71)||LA43_2==73) ) {
                    alt43=2;
                }
                else if ( (LA43_2==33) ) {
                    alt43=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 43, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_STRING:
            case RULE_INT:
            case RULE_SIGNEDINT:
            case RULE_BOOL:
            case 37:
            case 70:
            case 72:
            case 74:
                {
                alt43=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 43, 0, input);

                throw nvae;
            }

            switch (alt43) {
                case 1 :
                    // InternalCollaboration.g:2585:5: this_VariableExpression_0= ruleVariableExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionAndVariablesAccess().getVariableExpressionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableExpression_0=ruleVariableExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableExpression_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:2595:5: this_Expression_1= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionAndVariablesAccess().getExpressionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Expression_1=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Expression_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionAndVariables"


    // $ANTLR start "entryRuleVariableExpression"
    // InternalCollaboration.g:2611:1: entryRuleVariableExpression returns [EObject current=null] : iv_ruleVariableExpression= ruleVariableExpression EOF ;
    public final EObject entryRuleVariableExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableExpression = null;


        try {
            // InternalCollaboration.g:2612:2: (iv_ruleVariableExpression= ruleVariableExpression EOF )
            // InternalCollaboration.g:2613:2: iv_ruleVariableExpression= ruleVariableExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableExpression=ruleVariableExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableExpression"


    // $ANTLR start "ruleVariableExpression"
    // InternalCollaboration.g:2620:1: ruleVariableExpression returns [EObject current=null] : (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment | this_ClockDeclaration_2= ruleClockDeclaration | this_ClockAssignment_3= ruleClockAssignment ) ;
    public final EObject ruleVariableExpression() throws RecognitionException {
        EObject current = null;

        EObject this_TypedVariableDeclaration_0 = null;

        EObject this_VariableAssignment_1 = null;

        EObject this_ClockDeclaration_2 = null;

        EObject this_ClockAssignment_3 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2623:28: ( (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment | this_ClockDeclaration_2= ruleClockDeclaration | this_ClockAssignment_3= ruleClockAssignment ) )
            // InternalCollaboration.g:2624:1: (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment | this_ClockDeclaration_2= ruleClockDeclaration | this_ClockAssignment_3= ruleClockAssignment )
            {
            // InternalCollaboration.g:2624:1: (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment | this_ClockDeclaration_2= ruleClockDeclaration | this_ClockAssignment_3= ruleClockAssignment )
            int alt44=4;
            switch ( input.LA(1) ) {
            case 57:
                {
                alt44=1;
                }
                break;
            case RULE_ID:
                {
                alt44=2;
                }
                break;
            case 58:
                {
                alt44=3;
                }
                break;
            case 59:
                {
                alt44=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 44, 0, input);

                throw nvae;
            }

            switch (alt44) {
                case 1 :
                    // InternalCollaboration.g:2625:5: this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getVariableExpressionAccess().getTypedVariableDeclarationParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_TypedVariableDeclaration_0=ruleTypedVariableDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TypedVariableDeclaration_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:2635:5: this_VariableAssignment_1= ruleVariableAssignment
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getVariableExpressionAccess().getVariableAssignmentParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableAssignment_1=ruleVariableAssignment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableAssignment_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:2645:5: this_ClockDeclaration_2= ruleClockDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getVariableExpressionAccess().getClockDeclarationParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ClockDeclaration_2=ruleClockDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ClockDeclaration_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // InternalCollaboration.g:2655:5: this_ClockAssignment_3= ruleClockAssignment
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getVariableExpressionAccess().getClockAssignmentParserRuleCall_3()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ClockAssignment_3=ruleClockAssignment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ClockAssignment_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableExpression"


    // $ANTLR start "entryRuleVariableAssignment"
    // InternalCollaboration.g:2673:1: entryRuleVariableAssignment returns [EObject current=null] : iv_ruleVariableAssignment= ruleVariableAssignment EOF ;
    public final EObject entryRuleVariableAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableAssignment = null;


        try {
            // InternalCollaboration.g:2674:2: (iv_ruleVariableAssignment= ruleVariableAssignment EOF )
            // InternalCollaboration.g:2675:2: iv_ruleVariableAssignment= ruleVariableAssignment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableAssignmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableAssignment=ruleVariableAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableAssignment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableAssignment"


    // $ANTLR start "ruleVariableAssignment"
    // InternalCollaboration.g:2682:1: ruleVariableAssignment returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) ) ;
    public final EObject ruleVariableAssignment() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_expression_2_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2685:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) ) )
            // InternalCollaboration.g:2686:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) )
            {
            // InternalCollaboration.g:2686:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) )
            // InternalCollaboration.g:2686:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) )
            {
            // InternalCollaboration.g:2686:2: ( (otherlv_0= RULE_ID ) )
            // InternalCollaboration.g:2687:1: (otherlv_0= RULE_ID )
            {
            // InternalCollaboration.g:2687:1: (otherlv_0= RULE_ID )
            // InternalCollaboration.g:2688:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getVariableAssignmentRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_22); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,33,FollowSets000.FOLLOW_40); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getVariableAssignmentAccess().getEqualsSignKeyword_1());
                  
            }
            // InternalCollaboration.g:2703:1: ( (lv_expression_2_0= ruleExpression ) )
            // InternalCollaboration.g:2704:1: (lv_expression_2_0= ruleExpression )
            {
            // InternalCollaboration.g:2704:1: (lv_expression_2_0= ruleExpression )
            // InternalCollaboration.g:2705:3: lv_expression_2_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getVariableAssignmentAccess().getExpressionExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_expression_2_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getVariableAssignmentRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_2_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableAssignment"


    // $ANTLR start "entryRuleTypedVariableDeclaration"
    // InternalCollaboration.g:2729:1: entryRuleTypedVariableDeclaration returns [EObject current=null] : iv_ruleTypedVariableDeclaration= ruleTypedVariableDeclaration EOF ;
    public final EObject entryRuleTypedVariableDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypedVariableDeclaration = null;


        try {
            // InternalCollaboration.g:2730:2: (iv_ruleTypedVariableDeclaration= ruleTypedVariableDeclaration EOF )
            // InternalCollaboration.g:2731:2: iv_ruleTypedVariableDeclaration= ruleTypedVariableDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypedVariableDeclarationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleTypedVariableDeclaration=ruleTypedVariableDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTypedVariableDeclaration; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypedVariableDeclaration"


    // $ANTLR start "ruleTypedVariableDeclaration"
    // InternalCollaboration.g:2738:1: ruleTypedVariableDeclaration returns [EObject current=null] : (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? ) ;
    public final EObject ruleTypedVariableDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        EObject lv_expression_4_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2741:28: ( (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? ) )
            // InternalCollaboration.g:2742:1: (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? )
            {
            // InternalCollaboration.g:2742:1: (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? )
            // InternalCollaboration.g:2742:3: otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )?
            {
            otherlv_0=(Token)match(input,57,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getTypedVariableDeclarationAccess().getVarKeyword_0());
                  
            }
            // InternalCollaboration.g:2746:1: ( (otherlv_1= RULE_ID ) )
            // InternalCollaboration.g:2747:1: (otherlv_1= RULE_ID )
            {
            // InternalCollaboration.g:2747:1: (otherlv_1= RULE_ID )
            // InternalCollaboration.g:2748:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getTypedVariableDeclarationRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierCrossReference_1_0()); 
              	
            }

            }


            }

            // InternalCollaboration.g:2759:2: ( (lv_name_2_0= RULE_ID ) )
            // InternalCollaboration.g:2760:1: (lv_name_2_0= RULE_ID )
            {
            // InternalCollaboration.g:2760:1: (lv_name_2_0= RULE_ID )
            // InternalCollaboration.g:2761:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_50); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getTypedVariableDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getTypedVariableDeclarationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalCollaboration.g:2777:2: (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )?
            int alt45=2;
            int LA45_0 = input.LA(1);

            if ( (LA45_0==33) ) {
                alt45=1;
            }
            switch (alt45) {
                case 1 :
                    // InternalCollaboration.g:2777:4: otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) )
                    {
                    otherlv_3=(Token)match(input,33,FollowSets000.FOLLOW_40); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getTypedVariableDeclarationAccess().getEqualsSignKeyword_3_0());
                          
                    }
                    // InternalCollaboration.g:2781:1: ( (lv_expression_4_0= ruleExpression ) )
                    // InternalCollaboration.g:2782:1: (lv_expression_4_0= ruleExpression )
                    {
                    // InternalCollaboration.g:2782:1: (lv_expression_4_0= ruleExpression )
                    // InternalCollaboration.g:2783:3: lv_expression_4_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getTypedVariableDeclarationAccess().getExpressionExpressionParserRuleCall_3_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_4_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getTypedVariableDeclarationRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_4_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypedVariableDeclaration"


    // $ANTLR start "entryRuleClockDeclaration"
    // InternalCollaboration.g:2807:1: entryRuleClockDeclaration returns [EObject current=null] : iv_ruleClockDeclaration= ruleClockDeclaration EOF ;
    public final EObject entryRuleClockDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClockDeclaration = null;


        try {
            // InternalCollaboration.g:2808:2: (iv_ruleClockDeclaration= ruleClockDeclaration EOF )
            // InternalCollaboration.g:2809:2: iv_ruleClockDeclaration= ruleClockDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClockDeclarationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleClockDeclaration=ruleClockDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClockDeclaration; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClockDeclaration"


    // $ANTLR start "ruleClockDeclaration"
    // InternalCollaboration.g:2816:1: ruleClockDeclaration returns [EObject current=null] : ( () otherlv_1= 'clock' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) ) )? ) ;
    public final EObject ruleClockDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        EObject lv_expression_4_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2819:28: ( ( () otherlv_1= 'clock' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) ) )? ) )
            // InternalCollaboration.g:2820:1: ( () otherlv_1= 'clock' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) ) )? )
            {
            // InternalCollaboration.g:2820:1: ( () otherlv_1= 'clock' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) ) )? )
            // InternalCollaboration.g:2820:2: () otherlv_1= 'clock' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) ) )?
            {
            // InternalCollaboration.g:2820:2: ()
            // InternalCollaboration.g:2821:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getClockDeclarationAccess().getClockDeclarationAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,58,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getClockDeclarationAccess().getClockKeyword_1());
                  
            }
            // InternalCollaboration.g:2830:1: ( (lv_name_2_0= RULE_ID ) )
            // InternalCollaboration.g:2831:1: (lv_name_2_0= RULE_ID )
            {
            // InternalCollaboration.g:2831:1: (lv_name_2_0= RULE_ID )
            // InternalCollaboration.g:2832:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_50); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getClockDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getClockDeclarationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalCollaboration.g:2848:2: (otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) ) )?
            int alt46=2;
            int LA46_0 = input.LA(1);

            if ( (LA46_0==33) ) {
                alt46=1;
            }
            switch (alt46) {
                case 1 :
                    // InternalCollaboration.g:2848:4: otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) )
                    {
                    otherlv_3=(Token)match(input,33,FollowSets000.FOLLOW_51); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getClockDeclarationAccess().getEqualsSignKeyword_3_0());
                          
                    }
                    // InternalCollaboration.g:2852:1: ( (lv_expression_4_0= ruleIntegerValue ) )
                    // InternalCollaboration.g:2853:1: (lv_expression_4_0= ruleIntegerValue )
                    {
                    // InternalCollaboration.g:2853:1: (lv_expression_4_0= ruleIntegerValue )
                    // InternalCollaboration.g:2854:3: lv_expression_4_0= ruleIntegerValue
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getClockDeclarationAccess().getExpressionIntegerValueParserRuleCall_3_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_4_0=ruleIntegerValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getClockDeclarationRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_4_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.IntegerValue");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClockDeclaration"


    // $ANTLR start "entryRuleClockAssignment"
    // InternalCollaboration.g:2880:1: entryRuleClockAssignment returns [EObject current=null] : iv_ruleClockAssignment= ruleClockAssignment EOF ;
    public final EObject entryRuleClockAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClockAssignment = null;


        try {
            // InternalCollaboration.g:2881:2: (iv_ruleClockAssignment= ruleClockAssignment EOF )
            // InternalCollaboration.g:2882:2: iv_ruleClockAssignment= ruleClockAssignment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClockAssignmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleClockAssignment=ruleClockAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClockAssignment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClockAssignment"


    // $ANTLR start "ruleClockAssignment"
    // InternalCollaboration.g:2889:1: ruleClockAssignment returns [EObject current=null] : (otherlv_0= 'reset clock' ( (otherlv_1= RULE_ID ) ) (otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) ) )? ) ;
    public final EObject ruleClockAssignment() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_expression_3_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2892:28: ( (otherlv_0= 'reset clock' ( (otherlv_1= RULE_ID ) ) (otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) ) )? ) )
            // InternalCollaboration.g:2893:1: (otherlv_0= 'reset clock' ( (otherlv_1= RULE_ID ) ) (otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) ) )? )
            {
            // InternalCollaboration.g:2893:1: (otherlv_0= 'reset clock' ( (otherlv_1= RULE_ID ) ) (otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) ) )? )
            // InternalCollaboration.g:2893:3: otherlv_0= 'reset clock' ( (otherlv_1= RULE_ID ) ) (otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) ) )?
            {
            otherlv_0=(Token)match(input,59,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getClockAssignmentAccess().getResetClockKeyword_0());
                  
            }
            // InternalCollaboration.g:2897:1: ( (otherlv_1= RULE_ID ) )
            // InternalCollaboration.g:2898:1: (otherlv_1= RULE_ID )
            {
            // InternalCollaboration.g:2898:1: (otherlv_1= RULE_ID )
            // InternalCollaboration.g:2899:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getClockAssignmentRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_50); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getClockAssignmentAccess().getVariableClockDeclarationCrossReference_1_0()); 
              	
            }

            }


            }

            // InternalCollaboration.g:2910:2: (otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) ) )?
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( (LA47_0==33) ) {
                alt47=1;
            }
            switch (alt47) {
                case 1 :
                    // InternalCollaboration.g:2910:4: otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) )
                    {
                    otherlv_2=(Token)match(input,33,FollowSets000.FOLLOW_51); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getClockAssignmentAccess().getEqualsSignKeyword_2_0());
                          
                    }
                    // InternalCollaboration.g:2914:1: ( (lv_expression_3_0= ruleIntegerValue ) )
                    // InternalCollaboration.g:2915:1: (lv_expression_3_0= ruleIntegerValue )
                    {
                    // InternalCollaboration.g:2915:1: (lv_expression_3_0= ruleIntegerValue )
                    // InternalCollaboration.g:2916:3: lv_expression_3_0= ruleIntegerValue
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getClockAssignmentAccess().getExpressionIntegerValueParserRuleCall_2_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_3_0=ruleIntegerValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getClockAssignmentRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.IntegerValue");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClockAssignment"


    // $ANTLR start "entryRuleExpression"
    // InternalCollaboration.g:2940:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // InternalCollaboration.g:2941:2: (iv_ruleExpression= ruleExpression EOF )
            // InternalCollaboration.g:2942:2: iv_ruleExpression= ruleExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpression=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalCollaboration.g:2949:1: ruleExpression returns [EObject current=null] : this_ImplicationExpression_0= ruleImplicationExpression ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_ImplicationExpression_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2952:28: (this_ImplicationExpression_0= ruleImplicationExpression )
            // InternalCollaboration.g:2954:5: this_ImplicationExpression_0= ruleImplicationExpression
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getExpressionAccess().getImplicationExpressionParserRuleCall()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_2);
            this_ImplicationExpression_0=ruleImplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_ImplicationExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleImplicationExpression"
    // InternalCollaboration.g:2970:1: entryRuleImplicationExpression returns [EObject current=null] : iv_ruleImplicationExpression= ruleImplicationExpression EOF ;
    public final EObject entryRuleImplicationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImplicationExpression = null;


        try {
            // InternalCollaboration.g:2971:2: (iv_ruleImplicationExpression= ruleImplicationExpression EOF )
            // InternalCollaboration.g:2972:2: iv_ruleImplicationExpression= ruleImplicationExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getImplicationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleImplicationExpression=ruleImplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleImplicationExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImplicationExpression"


    // $ANTLR start "ruleImplicationExpression"
    // InternalCollaboration.g:2979:1: ruleImplicationExpression returns [EObject current=null] : (this_DisjunctionExpression_0= ruleDisjunctionExpression ( () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) ) )? ) ;
    public final EObject ruleImplicationExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_0=null;
        EObject this_DisjunctionExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2982:28: ( (this_DisjunctionExpression_0= ruleDisjunctionExpression ( () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) ) )? ) )
            // InternalCollaboration.g:2983:1: (this_DisjunctionExpression_0= ruleDisjunctionExpression ( () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) ) )? )
            {
            // InternalCollaboration.g:2983:1: (this_DisjunctionExpression_0= ruleDisjunctionExpression ( () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) ) )? )
            // InternalCollaboration.g:2984:5: this_DisjunctionExpression_0= ruleDisjunctionExpression ( () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getImplicationExpressionAccess().getDisjunctionExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_52);
            this_DisjunctionExpression_0=ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_DisjunctionExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalCollaboration.g:2992:1: ( () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) ) )?
            int alt48=2;
            int LA48_0 = input.LA(1);

            if ( (LA48_0==60) ) {
                alt48=1;
            }
            switch (alt48) {
                case 1 :
                    // InternalCollaboration.g:2992:2: () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) )
                    {
                    // InternalCollaboration.g:2992:2: ()
                    // InternalCollaboration.g:2993:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getImplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalCollaboration.g:2998:2: ( (lv_operator_2_0= '=>' ) )
                    // InternalCollaboration.g:2999:1: (lv_operator_2_0= '=>' )
                    {
                    // InternalCollaboration.g:2999:1: (lv_operator_2_0= '=>' )
                    // InternalCollaboration.g:3000:3: lv_operator_2_0= '=>'
                    {
                    lv_operator_2_0=(Token)match(input,60,FollowSets000.FOLLOW_40); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_2_0, grammarAccess.getImplicationExpressionAccess().getOperatorEqualsSignGreaterThanSignKeyword_1_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getImplicationExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_2_0, "=>");
                      	    
                    }

                    }


                    }

                    // InternalCollaboration.g:3013:2: ( (lv_right_3_0= ruleImplicationExpression ) )
                    // InternalCollaboration.g:3014:1: (lv_right_3_0= ruleImplicationExpression )
                    {
                    // InternalCollaboration.g:3014:1: (lv_right_3_0= ruleImplicationExpression )
                    // InternalCollaboration.g:3015:3: lv_right_3_0= ruleImplicationExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getImplicationExpressionAccess().getRightImplicationExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleImplicationExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getImplicationExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.ImplicationExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImplicationExpression"


    // $ANTLR start "entryRuleDisjunctionExpression"
    // InternalCollaboration.g:3039:1: entryRuleDisjunctionExpression returns [EObject current=null] : iv_ruleDisjunctionExpression= ruleDisjunctionExpression EOF ;
    public final EObject entryRuleDisjunctionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDisjunctionExpression = null;


        try {
            // InternalCollaboration.g:3040:2: (iv_ruleDisjunctionExpression= ruleDisjunctionExpression EOF )
            // InternalCollaboration.g:3041:2: iv_ruleDisjunctionExpression= ruleDisjunctionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDisjunctionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleDisjunctionExpression=ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDisjunctionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDisjunctionExpression"


    // $ANTLR start "ruleDisjunctionExpression"
    // InternalCollaboration.g:3048:1: ruleDisjunctionExpression returns [EObject current=null] : (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? ) ;
    public final EObject ruleDisjunctionExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_0=null;
        EObject this_ConjunctionExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:3051:28: ( (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? ) )
            // InternalCollaboration.g:3052:1: (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? )
            {
            // InternalCollaboration.g:3052:1: (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? )
            // InternalCollaboration.g:3053:5: this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getDisjunctionExpressionAccess().getConjunctionExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_53);
            this_ConjunctionExpression_0=ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_ConjunctionExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalCollaboration.g:3061:1: ( () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )?
            int alt49=2;
            int LA49_0 = input.LA(1);

            if ( (LA49_0==61) ) {
                alt49=1;
            }
            switch (alt49) {
                case 1 :
                    // InternalCollaboration.g:3061:2: () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) )
                    {
                    // InternalCollaboration.g:3061:2: ()
                    // InternalCollaboration.g:3062:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getDisjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalCollaboration.g:3067:2: ( (lv_operator_2_0= '||' ) )
                    // InternalCollaboration.g:3068:1: (lv_operator_2_0= '||' )
                    {
                    // InternalCollaboration.g:3068:1: (lv_operator_2_0= '||' )
                    // InternalCollaboration.g:3069:3: lv_operator_2_0= '||'
                    {
                    lv_operator_2_0=(Token)match(input,61,FollowSets000.FOLLOW_40); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_2_0, grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineVerticalLineKeyword_1_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getDisjunctionExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_2_0, "||");
                      	    
                    }

                    }


                    }

                    // InternalCollaboration.g:3082:2: ( (lv_right_3_0= ruleDisjunctionExpression ) )
                    // InternalCollaboration.g:3083:1: (lv_right_3_0= ruleDisjunctionExpression )
                    {
                    // InternalCollaboration.g:3083:1: (lv_right_3_0= ruleDisjunctionExpression )
                    // InternalCollaboration.g:3084:3: lv_right_3_0= ruleDisjunctionExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getDisjunctionExpressionAccess().getRightDisjunctionExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleDisjunctionExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getDisjunctionExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.DisjunctionExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDisjunctionExpression"


    // $ANTLR start "entryRuleConjunctionExpression"
    // InternalCollaboration.g:3108:1: entryRuleConjunctionExpression returns [EObject current=null] : iv_ruleConjunctionExpression= ruleConjunctionExpression EOF ;
    public final EObject entryRuleConjunctionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConjunctionExpression = null;


        try {
            // InternalCollaboration.g:3109:2: (iv_ruleConjunctionExpression= ruleConjunctionExpression EOF )
            // InternalCollaboration.g:3110:2: iv_ruleConjunctionExpression= ruleConjunctionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConjunctionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConjunctionExpression=ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConjunctionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConjunctionExpression"


    // $ANTLR start "ruleConjunctionExpression"
    // InternalCollaboration.g:3117:1: ruleConjunctionExpression returns [EObject current=null] : (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? ) ;
    public final EObject ruleConjunctionExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_0=null;
        EObject this_RelationExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:3120:28: ( (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? ) )
            // InternalCollaboration.g:3121:1: (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? )
            {
            // InternalCollaboration.g:3121:1: (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? )
            // InternalCollaboration.g:3122:5: this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getConjunctionExpressionAccess().getRelationExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_54);
            this_RelationExpression_0=ruleRelationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_RelationExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalCollaboration.g:3130:1: ( () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )?
            int alt50=2;
            int LA50_0 = input.LA(1);

            if ( (LA50_0==62) ) {
                alt50=1;
            }
            switch (alt50) {
                case 1 :
                    // InternalCollaboration.g:3130:2: () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) )
                    {
                    // InternalCollaboration.g:3130:2: ()
                    // InternalCollaboration.g:3131:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getConjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalCollaboration.g:3136:2: ( (lv_operator_2_0= '&&' ) )
                    // InternalCollaboration.g:3137:1: (lv_operator_2_0= '&&' )
                    {
                    // InternalCollaboration.g:3137:1: (lv_operator_2_0= '&&' )
                    // InternalCollaboration.g:3138:3: lv_operator_2_0= '&&'
                    {
                    lv_operator_2_0=(Token)match(input,62,FollowSets000.FOLLOW_40); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_2_0, grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandAmpersandKeyword_1_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getConjunctionExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_2_0, "&&");
                      	    
                    }

                    }


                    }

                    // InternalCollaboration.g:3151:2: ( (lv_right_3_0= ruleConjunctionExpression ) )
                    // InternalCollaboration.g:3152:1: (lv_right_3_0= ruleConjunctionExpression )
                    {
                    // InternalCollaboration.g:3152:1: (lv_right_3_0= ruleConjunctionExpression )
                    // InternalCollaboration.g:3153:3: lv_right_3_0= ruleConjunctionExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getConjunctionExpressionAccess().getRightConjunctionExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleConjunctionExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getConjunctionExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.ConjunctionExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConjunctionExpression"


    // $ANTLR start "entryRuleRelationExpression"
    // InternalCollaboration.g:3177:1: entryRuleRelationExpression returns [EObject current=null] : iv_ruleRelationExpression= ruleRelationExpression EOF ;
    public final EObject entryRuleRelationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRelationExpression = null;


        try {
            // InternalCollaboration.g:3178:2: (iv_ruleRelationExpression= ruleRelationExpression EOF )
            // InternalCollaboration.g:3179:2: iv_ruleRelationExpression= ruleRelationExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRelationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRelationExpression=ruleRelationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRelationExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelationExpression"


    // $ANTLR start "ruleRelationExpression"
    // InternalCollaboration.g:3186:1: ruleRelationExpression returns [EObject current=null] : (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? ) ;
    public final EObject ruleRelationExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        Token lv_operator_2_3=null;
        Token lv_operator_2_4=null;
        Token lv_operator_2_5=null;
        Token lv_operator_2_6=null;
        EObject this_AdditionExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:3189:28: ( (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? ) )
            // InternalCollaboration.g:3190:1: (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? )
            {
            // InternalCollaboration.g:3190:1: (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? )
            // InternalCollaboration.g:3191:5: this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getRelationExpressionAccess().getAdditionExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_55);
            this_AdditionExpression_0=ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_AdditionExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalCollaboration.g:3199:1: ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )?
            int alt52=2;
            int LA52_0 = input.LA(1);

            if ( ((LA52_0>=63 && LA52_0<=68)) ) {
                alt52=1;
            }
            switch (alt52) {
                case 1 :
                    // InternalCollaboration.g:3199:2: () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) )
                    {
                    // InternalCollaboration.g:3199:2: ()
                    // InternalCollaboration.g:3200:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getRelationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalCollaboration.g:3205:2: ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) )
                    // InternalCollaboration.g:3206:1: ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) )
                    {
                    // InternalCollaboration.g:3206:1: ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) )
                    // InternalCollaboration.g:3207:1: (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' )
                    {
                    // InternalCollaboration.g:3207:1: (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' )
                    int alt51=6;
                    switch ( input.LA(1) ) {
                    case 63:
                        {
                        alt51=1;
                        }
                        break;
                    case 64:
                        {
                        alt51=2;
                        }
                        break;
                    case 65:
                        {
                        alt51=3;
                        }
                        break;
                    case 66:
                        {
                        alt51=4;
                        }
                        break;
                    case 67:
                        {
                        alt51=5;
                        }
                        break;
                    case 68:
                        {
                        alt51=6;
                        }
                        break;
                    default:
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 51, 0, input);

                        throw nvae;
                    }

                    switch (alt51) {
                        case 1 :
                            // InternalCollaboration.g:3208:3: lv_operator_2_1= '=='
                            {
                            lv_operator_2_1=(Token)match(input,63,FollowSets000.FOLLOW_40); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_1, grammarAccess.getRelationExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalCollaboration.g:3220:8: lv_operator_2_2= '!='
                            {
                            lv_operator_2_2=(Token)match(input,64,FollowSets000.FOLLOW_40); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_2, grammarAccess.getRelationExpressionAccess().getOperatorExclamationMarkEqualsSignKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
                              	    
                            }

                            }
                            break;
                        case 3 :
                            // InternalCollaboration.g:3232:8: lv_operator_2_3= '<'
                            {
                            lv_operator_2_3=(Token)match(input,65,FollowSets000.FOLLOW_40); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_3, grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignKeyword_1_1_0_2());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_3, null);
                              	    
                            }

                            }
                            break;
                        case 4 :
                            // InternalCollaboration.g:3244:8: lv_operator_2_4= '>'
                            {
                            lv_operator_2_4=(Token)match(input,66,FollowSets000.FOLLOW_40); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_4, grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignKeyword_1_1_0_3());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_4, null);
                              	    
                            }

                            }
                            break;
                        case 5 :
                            // InternalCollaboration.g:3256:8: lv_operator_2_5= '<='
                            {
                            lv_operator_2_5=(Token)match(input,67,FollowSets000.FOLLOW_40); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_5, grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_1_0_4());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_5, null);
                              	    
                            }

                            }
                            break;
                        case 6 :
                            // InternalCollaboration.g:3268:8: lv_operator_2_6= '>='
                            {
                            lv_operator_2_6=(Token)match(input,68,FollowSets000.FOLLOW_40); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_6, grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_1_0_5());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_6, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalCollaboration.g:3283:2: ( (lv_right_3_0= ruleRelationExpression ) )
                    // InternalCollaboration.g:3284:1: (lv_right_3_0= ruleRelationExpression )
                    {
                    // InternalCollaboration.g:3284:1: (lv_right_3_0= ruleRelationExpression )
                    // InternalCollaboration.g:3285:3: lv_right_3_0= ruleRelationExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getRelationExpressionAccess().getRightRelationExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleRelationExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getRelationExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.RelationExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelationExpression"


    // $ANTLR start "entryRuleTimedExpression"
    // InternalCollaboration.g:3309:1: entryRuleTimedExpression returns [EObject current=null] : iv_ruleTimedExpression= ruleTimedExpression EOF ;
    public final EObject entryRuleTimedExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimedExpression = null;


        try {
            // InternalCollaboration.g:3310:2: (iv_ruleTimedExpression= ruleTimedExpression EOF )
            // InternalCollaboration.g:3311:2: iv_ruleTimedExpression= ruleTimedExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimedExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleTimedExpression=ruleTimedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTimedExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimedExpression"


    // $ANTLR start "ruleTimedExpression"
    // InternalCollaboration.g:3318:1: ruleTimedExpression returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) ( ( (lv_operator_1_1= '==' | lv_operator_1_2= '<' | lv_operator_1_3= '>' | lv_operator_1_4= '<=' | lv_operator_1_5= '>=' ) ) ) ( (lv_value_2_0= RULE_INT ) ) ) ;
    public final EObject ruleTimedExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_operator_1_1=null;
        Token lv_operator_1_2=null;
        Token lv_operator_1_3=null;
        Token lv_operator_1_4=null;
        Token lv_operator_1_5=null;
        Token lv_value_2_0=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:3321:28: ( ( ( (otherlv_0= RULE_ID ) ) ( ( (lv_operator_1_1= '==' | lv_operator_1_2= '<' | lv_operator_1_3= '>' | lv_operator_1_4= '<=' | lv_operator_1_5= '>=' ) ) ) ( (lv_value_2_0= RULE_INT ) ) ) )
            // InternalCollaboration.g:3322:1: ( ( (otherlv_0= RULE_ID ) ) ( ( (lv_operator_1_1= '==' | lv_operator_1_2= '<' | lv_operator_1_3= '>' | lv_operator_1_4= '<=' | lv_operator_1_5= '>=' ) ) ) ( (lv_value_2_0= RULE_INT ) ) )
            {
            // InternalCollaboration.g:3322:1: ( ( (otherlv_0= RULE_ID ) ) ( ( (lv_operator_1_1= '==' | lv_operator_1_2= '<' | lv_operator_1_3= '>' | lv_operator_1_4= '<=' | lv_operator_1_5= '>=' ) ) ) ( (lv_value_2_0= RULE_INT ) ) )
            // InternalCollaboration.g:3322:2: ( (otherlv_0= RULE_ID ) ) ( ( (lv_operator_1_1= '==' | lv_operator_1_2= '<' | lv_operator_1_3= '>' | lv_operator_1_4= '<=' | lv_operator_1_5= '>=' ) ) ) ( (lv_value_2_0= RULE_INT ) )
            {
            // InternalCollaboration.g:3322:2: ( (otherlv_0= RULE_ID ) )
            // InternalCollaboration.g:3323:1: (otherlv_0= RULE_ID )
            {
            // InternalCollaboration.g:3323:1: (otherlv_0= RULE_ID )
            // InternalCollaboration.g:3324:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getTimedExpressionRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_56); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getTimedExpressionAccess().getClockClockDeclarationCrossReference_0_0()); 
              	
            }

            }


            }

            // InternalCollaboration.g:3335:2: ( ( (lv_operator_1_1= '==' | lv_operator_1_2= '<' | lv_operator_1_3= '>' | lv_operator_1_4= '<=' | lv_operator_1_5= '>=' ) ) )
            // InternalCollaboration.g:3336:1: ( (lv_operator_1_1= '==' | lv_operator_1_2= '<' | lv_operator_1_3= '>' | lv_operator_1_4= '<=' | lv_operator_1_5= '>=' ) )
            {
            // InternalCollaboration.g:3336:1: ( (lv_operator_1_1= '==' | lv_operator_1_2= '<' | lv_operator_1_3= '>' | lv_operator_1_4= '<=' | lv_operator_1_5= '>=' ) )
            // InternalCollaboration.g:3337:1: (lv_operator_1_1= '==' | lv_operator_1_2= '<' | lv_operator_1_3= '>' | lv_operator_1_4= '<=' | lv_operator_1_5= '>=' )
            {
            // InternalCollaboration.g:3337:1: (lv_operator_1_1= '==' | lv_operator_1_2= '<' | lv_operator_1_3= '>' | lv_operator_1_4= '<=' | lv_operator_1_5= '>=' )
            int alt53=5;
            switch ( input.LA(1) ) {
            case 63:
                {
                alt53=1;
                }
                break;
            case 65:
                {
                alt53=2;
                }
                break;
            case 66:
                {
                alt53=3;
                }
                break;
            case 67:
                {
                alt53=4;
                }
                break;
            case 68:
                {
                alt53=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 53, 0, input);

                throw nvae;
            }

            switch (alt53) {
                case 1 :
                    // InternalCollaboration.g:3338:3: lv_operator_1_1= '=='
                    {
                    lv_operator_1_1=(Token)match(input,63,FollowSets000.FOLLOW_57); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_1_1, grammarAccess.getTimedExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getTimedExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_1_1, null);
                      	    
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:3350:8: lv_operator_1_2= '<'
                    {
                    lv_operator_1_2=(Token)match(input,65,FollowSets000.FOLLOW_57); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_1_2, grammarAccess.getTimedExpressionAccess().getOperatorLessThanSignKeyword_1_0_1());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getTimedExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_1_2, null);
                      	    
                    }

                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:3362:8: lv_operator_1_3= '>'
                    {
                    lv_operator_1_3=(Token)match(input,66,FollowSets000.FOLLOW_57); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_1_3, grammarAccess.getTimedExpressionAccess().getOperatorGreaterThanSignKeyword_1_0_2());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getTimedExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_1_3, null);
                      	    
                    }

                    }
                    break;
                case 4 :
                    // InternalCollaboration.g:3374:8: lv_operator_1_4= '<='
                    {
                    lv_operator_1_4=(Token)match(input,67,FollowSets000.FOLLOW_57); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_1_4, grammarAccess.getTimedExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_0_3());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getTimedExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_1_4, null);
                      	    
                    }

                    }
                    break;
                case 5 :
                    // InternalCollaboration.g:3386:8: lv_operator_1_5= '>='
                    {
                    lv_operator_1_5=(Token)match(input,68,FollowSets000.FOLLOW_57); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_1_5, grammarAccess.getTimedExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_0_4());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getTimedExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_1_5, null);
                      	    
                    }

                    }
                    break;

            }


            }


            }

            // InternalCollaboration.g:3401:2: ( (lv_value_2_0= RULE_INT ) )
            // InternalCollaboration.g:3402:1: (lv_value_2_0= RULE_INT )
            {
            // InternalCollaboration.g:3402:1: (lv_value_2_0= RULE_INT )
            // InternalCollaboration.g:3403:3: lv_value_2_0= RULE_INT
            {
            lv_value_2_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_2_0, grammarAccess.getTimedExpressionAccess().getValueINTTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getTimedExpressionRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_2_0, 
                      		"org.eclipse.xtext.common.Terminals.INT");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimedExpression"


    // $ANTLR start "entryRuleAdditionExpression"
    // InternalCollaboration.g:3427:1: entryRuleAdditionExpression returns [EObject current=null] : iv_ruleAdditionExpression= ruleAdditionExpression EOF ;
    public final EObject entryRuleAdditionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAdditionExpression = null;


        try {
            // InternalCollaboration.g:3428:2: (iv_ruleAdditionExpression= ruleAdditionExpression EOF )
            // InternalCollaboration.g:3429:2: iv_ruleAdditionExpression= ruleAdditionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAdditionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAdditionExpression=ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAdditionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAdditionExpression"


    // $ANTLR start "ruleAdditionExpression"
    // InternalCollaboration.g:3436:1: ruleAdditionExpression returns [EObject current=null] : (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? ) ;
    public final EObject ruleAdditionExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        EObject this_MultiplicationExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:3439:28: ( (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? ) )
            // InternalCollaboration.g:3440:1: (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? )
            {
            // InternalCollaboration.g:3440:1: (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? )
            // InternalCollaboration.g:3441:5: this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getAdditionExpressionAccess().getMultiplicationExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_58);
            this_MultiplicationExpression_0=ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_MultiplicationExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalCollaboration.g:3449:1: ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )?
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( ((LA55_0>=69 && LA55_0<=70)) ) {
                alt55=1;
            }
            switch (alt55) {
                case 1 :
                    // InternalCollaboration.g:3449:2: () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) )
                    {
                    // InternalCollaboration.g:3449:2: ()
                    // InternalCollaboration.g:3450:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getAdditionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalCollaboration.g:3455:2: ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) )
                    // InternalCollaboration.g:3456:1: ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) )
                    {
                    // InternalCollaboration.g:3456:1: ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) )
                    // InternalCollaboration.g:3457:1: (lv_operator_2_1= '+' | lv_operator_2_2= '-' )
                    {
                    // InternalCollaboration.g:3457:1: (lv_operator_2_1= '+' | lv_operator_2_2= '-' )
                    int alt54=2;
                    int LA54_0 = input.LA(1);

                    if ( (LA54_0==69) ) {
                        alt54=1;
                    }
                    else if ( (LA54_0==70) ) {
                        alt54=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 54, 0, input);

                        throw nvae;
                    }
                    switch (alt54) {
                        case 1 :
                            // InternalCollaboration.g:3458:3: lv_operator_2_1= '+'
                            {
                            lv_operator_2_1=(Token)match(input,69,FollowSets000.FOLLOW_40); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_1, grammarAccess.getAdditionExpressionAccess().getOperatorPlusSignKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getAdditionExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalCollaboration.g:3470:8: lv_operator_2_2= '-'
                            {
                            lv_operator_2_2=(Token)match(input,70,FollowSets000.FOLLOW_40); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_2, grammarAccess.getAdditionExpressionAccess().getOperatorHyphenMinusKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getAdditionExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalCollaboration.g:3485:2: ( (lv_right_3_0= ruleAdditionExpression ) )
                    // InternalCollaboration.g:3486:1: (lv_right_3_0= ruleAdditionExpression )
                    {
                    // InternalCollaboration.g:3486:1: (lv_right_3_0= ruleAdditionExpression )
                    // InternalCollaboration.g:3487:3: lv_right_3_0= ruleAdditionExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getAdditionExpressionAccess().getRightAdditionExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleAdditionExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getAdditionExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.AdditionExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdditionExpression"


    // $ANTLR start "entryRuleMultiplicationExpression"
    // InternalCollaboration.g:3511:1: entryRuleMultiplicationExpression returns [EObject current=null] : iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF ;
    public final EObject entryRuleMultiplicationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMultiplicationExpression = null;


        try {
            // InternalCollaboration.g:3512:2: (iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF )
            // InternalCollaboration.g:3513:2: iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMultiplicationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleMultiplicationExpression=ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMultiplicationExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultiplicationExpression"


    // $ANTLR start "ruleMultiplicationExpression"
    // InternalCollaboration.g:3520:1: ruleMultiplicationExpression returns [EObject current=null] : (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? ) ;
    public final EObject ruleMultiplicationExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        EObject this_NegatedExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:3523:28: ( (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? ) )
            // InternalCollaboration.g:3524:1: (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? )
            {
            // InternalCollaboration.g:3524:1: (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? )
            // InternalCollaboration.g:3525:5: this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getMultiplicationExpressionAccess().getNegatedExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_59);
            this_NegatedExpression_0=ruleNegatedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_NegatedExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalCollaboration.g:3533:1: ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )?
            int alt57=2;
            int LA57_0 = input.LA(1);

            if ( (LA57_0==39||LA57_0==71) ) {
                alt57=1;
            }
            switch (alt57) {
                case 1 :
                    // InternalCollaboration.g:3533:2: () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) )
                    {
                    // InternalCollaboration.g:3533:2: ()
                    // InternalCollaboration.g:3534:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getMultiplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalCollaboration.g:3539:2: ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) )
                    // InternalCollaboration.g:3540:1: ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) )
                    {
                    // InternalCollaboration.g:3540:1: ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) )
                    // InternalCollaboration.g:3541:1: (lv_operator_2_1= '*' | lv_operator_2_2= '/' )
                    {
                    // InternalCollaboration.g:3541:1: (lv_operator_2_1= '*' | lv_operator_2_2= '/' )
                    int alt56=2;
                    int LA56_0 = input.LA(1);

                    if ( (LA56_0==39) ) {
                        alt56=1;
                    }
                    else if ( (LA56_0==71) ) {
                        alt56=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 56, 0, input);

                        throw nvae;
                    }
                    switch (alt56) {
                        case 1 :
                            // InternalCollaboration.g:3542:3: lv_operator_2_1= '*'
                            {
                            lv_operator_2_1=(Token)match(input,39,FollowSets000.FOLLOW_40); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_1, grammarAccess.getMultiplicationExpressionAccess().getOperatorAsteriskKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getMultiplicationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalCollaboration.g:3554:8: lv_operator_2_2= '/'
                            {
                            lv_operator_2_2=(Token)match(input,71,FollowSets000.FOLLOW_40); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_2, grammarAccess.getMultiplicationExpressionAccess().getOperatorSolidusKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getMultiplicationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalCollaboration.g:3569:2: ( (lv_right_3_0= ruleMultiplicationExpression ) )
                    // InternalCollaboration.g:3570:1: (lv_right_3_0= ruleMultiplicationExpression )
                    {
                    // InternalCollaboration.g:3570:1: (lv_right_3_0= ruleMultiplicationExpression )
                    // InternalCollaboration.g:3571:3: lv_right_3_0= ruleMultiplicationExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getMultiplicationExpressionAccess().getRightMultiplicationExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleMultiplicationExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getMultiplicationExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.MultiplicationExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicationExpression"


    // $ANTLR start "entryRuleNegatedExpression"
    // InternalCollaboration.g:3595:1: entryRuleNegatedExpression returns [EObject current=null] : iv_ruleNegatedExpression= ruleNegatedExpression EOF ;
    public final EObject entryRuleNegatedExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNegatedExpression = null;


        try {
            // InternalCollaboration.g:3596:2: (iv_ruleNegatedExpression= ruleNegatedExpression EOF )
            // InternalCollaboration.g:3597:2: iv_ruleNegatedExpression= ruleNegatedExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNegatedExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNegatedExpression=ruleNegatedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNegatedExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNegatedExpression"


    // $ANTLR start "ruleNegatedExpression"
    // InternalCollaboration.g:3604:1: ruleNegatedExpression returns [EObject current=null] : ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression ) ;
    public final EObject ruleNegatedExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_1_1=null;
        Token lv_operator_1_2=null;
        EObject lv_operand_2_0 = null;

        EObject this_BasicExpression_3 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:3607:28: ( ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression ) )
            // InternalCollaboration.g:3608:1: ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression )
            {
            // InternalCollaboration.g:3608:1: ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression )
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==70||LA59_0==72) ) {
                alt59=1;
            }
            else if ( (LA59_0==RULE_ID||(LA59_0>=RULE_STRING && LA59_0<=RULE_BOOL)||LA59_0==37||LA59_0==74) ) {
                alt59=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 59, 0, input);

                throw nvae;
            }
            switch (alt59) {
                case 1 :
                    // InternalCollaboration.g:3608:2: ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) )
                    {
                    // InternalCollaboration.g:3608:2: ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) )
                    // InternalCollaboration.g:3608:3: () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) )
                    {
                    // InternalCollaboration.g:3608:3: ()
                    // InternalCollaboration.g:3609:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getNegatedExpressionAccess().getUnaryOperationExpressionAction_0_0(),
                                  current);
                          
                    }

                    }

                    // InternalCollaboration.g:3614:2: ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) )
                    // InternalCollaboration.g:3614:3: ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) )
                    {
                    // InternalCollaboration.g:3627:1: ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) )
                    // InternalCollaboration.g:3628:1: (lv_operator_1_1= '!' | lv_operator_1_2= '-' )
                    {
                    // InternalCollaboration.g:3628:1: (lv_operator_1_1= '!' | lv_operator_1_2= '-' )
                    int alt58=2;
                    int LA58_0 = input.LA(1);

                    if ( (LA58_0==72) ) {
                        alt58=1;
                    }
                    else if ( (LA58_0==70) ) {
                        alt58=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 58, 0, input);

                        throw nvae;
                    }
                    switch (alt58) {
                        case 1 :
                            // InternalCollaboration.g:3629:3: lv_operator_1_1= '!'
                            {
                            lv_operator_1_1=(Token)match(input,72,FollowSets000.FOLLOW_40); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_1_1, grammarAccess.getNegatedExpressionAccess().getOperatorExclamationMarkKeyword_0_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getNegatedExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_1_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalCollaboration.g:3641:8: lv_operator_1_2= '-'
                            {
                            lv_operator_1_2=(Token)match(input,70,FollowSets000.FOLLOW_40); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_1_2, grammarAccess.getNegatedExpressionAccess().getOperatorHyphenMinusKeyword_0_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getNegatedExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_1_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalCollaboration.g:3656:2: ( (lv_operand_2_0= ruleBasicExpression ) )
                    // InternalCollaboration.g:3657:1: (lv_operand_2_0= ruleBasicExpression )
                    {
                    // InternalCollaboration.g:3657:1: (lv_operand_2_0= ruleBasicExpression )
                    // InternalCollaboration.g:3658:3: lv_operand_2_0= ruleBasicExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getNegatedExpressionAccess().getOperandBasicExpressionParserRuleCall_0_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_operand_2_0=ruleBasicExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getNegatedExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"operand",
                              		lv_operand_2_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.BasicExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:3676:5: this_BasicExpression_3= ruleBasicExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getNegatedExpressionAccess().getBasicExpressionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_BasicExpression_3=ruleBasicExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BasicExpression_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNegatedExpression"


    // $ANTLR start "entryRuleBasicExpression"
    // InternalCollaboration.g:3692:1: entryRuleBasicExpression returns [EObject current=null] : iv_ruleBasicExpression= ruleBasicExpression EOF ;
    public final EObject entryRuleBasicExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBasicExpression = null;


        try {
            // InternalCollaboration.g:3693:2: (iv_ruleBasicExpression= ruleBasicExpression EOF )
            // InternalCollaboration.g:3694:2: iv_ruleBasicExpression= ruleBasicExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBasicExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBasicExpression=ruleBasicExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBasicExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBasicExpression"


    // $ANTLR start "ruleBasicExpression"
    // InternalCollaboration.g:3701:1: ruleBasicExpression returns [EObject current=null] : (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) ) ;
    public final EObject ruleBasicExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject this_Value_0 = null;

        EObject this_Expression_2 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:3704:28: ( (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) ) )
            // InternalCollaboration.g:3705:1: (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) )
            {
            // InternalCollaboration.g:3705:1: (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) )
            int alt60=2;
            int LA60_0 = input.LA(1);

            if ( (LA60_0==RULE_ID||(LA60_0>=RULE_STRING && LA60_0<=RULE_BOOL)||LA60_0==74) ) {
                alt60=1;
            }
            else if ( (LA60_0==37) ) {
                alt60=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 60, 0, input);

                throw nvae;
            }
            switch (alt60) {
                case 1 :
                    // InternalCollaboration.g:3706:5: this_Value_0= ruleValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBasicExpressionAccess().getValueParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Value_0=ruleValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Value_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:3715:6: (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' )
                    {
                    // InternalCollaboration.g:3715:6: (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' )
                    // InternalCollaboration.g:3715:8: otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')'
                    {
                    otherlv_1=(Token)match(input,37,FollowSets000.FOLLOW_40); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBasicExpressionAccess().getExpressionParserRuleCall_1_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_60);
                    this_Expression_2=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Expression_2; 
                              afterParserOrEnumRuleCall();
                          
                    }
                    otherlv_3=(Token)match(input,38,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getBasicExpressionAccess().getRightParenthesisKeyword_1_2());
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBasicExpression"


    // $ANTLR start "entryRuleValue"
    // InternalCollaboration.g:3740:1: entryRuleValue returns [EObject current=null] : iv_ruleValue= ruleValue EOF ;
    public final EObject entryRuleValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleValue = null;


        try {
            // InternalCollaboration.g:3741:2: (iv_ruleValue= ruleValue EOF )
            // InternalCollaboration.g:3742:2: iv_ruleValue= ruleValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleValue=ruleValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValue"


    // $ANTLR start "ruleValue"
    // InternalCollaboration.g:3749:1: ruleValue returns [EObject current=null] : (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess ) ;
    public final EObject ruleValue() throws RecognitionException {
        EObject current = null;

        EObject this_IntegerValue_0 = null;

        EObject this_BooleanValue_1 = null;

        EObject this_StringValue_2 = null;

        EObject this_EnumValue_3 = null;

        EObject this_NullValue_4 = null;

        EObject this_VariableValue_5 = null;

        EObject this_FeatureAccess_6 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:3752:28: ( (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess ) )
            // InternalCollaboration.g:3753:1: (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess )
            {
            // InternalCollaboration.g:3753:1: (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess )
            int alt61=7;
            switch ( input.LA(1) ) {
            case RULE_INT:
            case RULE_SIGNEDINT:
                {
                alt61=1;
                }
                break;
            case RULE_BOOL:
                {
                alt61=2;
                }
                break;
            case RULE_STRING:
                {
                alt61=3;
                }
                break;
            case RULE_ID:
                {
                switch ( input.LA(2) ) {
                case 73:
                    {
                    alt61=4;
                    }
                    break;
                case EOF:
                case RULE_ID:
                case 17:
                case 18:
                case 29:
                case 31:
                case 34:
                case 35:
                case 38:
                case 39:
                case 41:
                case 43:
                case 44:
                case 46:
                case 47:
                case 48:
                case 49:
                case 50:
                case 56:
                case 57:
                case 58:
                case 59:
                case 60:
                case 61:
                case 62:
                case 63:
                case 64:
                case 65:
                case 66:
                case 67:
                case 68:
                case 69:
                case 70:
                case 71:
                case 78:
                case 79:
                case 80:
                    {
                    alt61=6;
                    }
                    break;
                case 19:
                    {
                    alt61=7;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 61, 4, input);

                    throw nvae;
                }

                }
                break;
            case 74:
                {
                alt61=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 61, 0, input);

                throw nvae;
            }

            switch (alt61) {
                case 1 :
                    // InternalCollaboration.g:3754:5: this_IntegerValue_0= ruleIntegerValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getIntegerValueParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntegerValue_0=ruleIntegerValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IntegerValue_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:3764:5: this_BooleanValue_1= ruleBooleanValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getBooleanValueParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_BooleanValue_1=ruleBooleanValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BooleanValue_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:3774:5: this_StringValue_2= ruleStringValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getStringValueParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_StringValue_2=ruleStringValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_StringValue_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // InternalCollaboration.g:3784:5: this_EnumValue_3= ruleEnumValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getEnumValueParserRuleCall_3()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_EnumValue_3=ruleEnumValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_EnumValue_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // InternalCollaboration.g:3794:5: this_NullValue_4= ruleNullValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getNullValueParserRuleCall_4()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_NullValue_4=ruleNullValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_NullValue_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 6 :
                    // InternalCollaboration.g:3804:5: this_VariableValue_5= ruleVariableValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getVariableValueParserRuleCall_5()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableValue_5=ruleVariableValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableValue_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 7 :
                    // InternalCollaboration.g:3814:5: this_FeatureAccess_6= ruleFeatureAccess
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getFeatureAccessParserRuleCall_6()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_FeatureAccess_6=ruleFeatureAccess();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_FeatureAccess_6; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValue"


    // $ANTLR start "entryRuleIntegerValue"
    // InternalCollaboration.g:3830:1: entryRuleIntegerValue returns [EObject current=null] : iv_ruleIntegerValue= ruleIntegerValue EOF ;
    public final EObject entryRuleIntegerValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerValue = null;


        try {
            // InternalCollaboration.g:3831:2: (iv_ruleIntegerValue= ruleIntegerValue EOF )
            // InternalCollaboration.g:3832:2: iv_ruleIntegerValue= ruleIntegerValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntegerValue=ruleIntegerValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerValue"


    // $ANTLR start "ruleIntegerValue"
    // InternalCollaboration.g:3839:1: ruleIntegerValue returns [EObject current=null] : ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) ) ;
    public final EObject ruleIntegerValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_1=null;
        Token lv_value_0_2=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:3842:28: ( ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) ) )
            // InternalCollaboration.g:3843:1: ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) )
            {
            // InternalCollaboration.g:3843:1: ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) )
            // InternalCollaboration.g:3844:1: ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) )
            {
            // InternalCollaboration.g:3844:1: ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) )
            // InternalCollaboration.g:3845:1: (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT )
            {
            // InternalCollaboration.g:3845:1: (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT )
            int alt62=2;
            int LA62_0 = input.LA(1);

            if ( (LA62_0==RULE_INT) ) {
                alt62=1;
            }
            else if ( (LA62_0==RULE_SIGNEDINT) ) {
                alt62=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 62, 0, input);

                throw nvae;
            }
            switch (alt62) {
                case 1 :
                    // InternalCollaboration.g:3846:3: lv_value_0_1= RULE_INT
                    {
                    lv_value_0_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_value_0_1, grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getIntegerValueRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"value",
                              		lv_value_0_1, 
                              		"org.eclipse.xtext.common.Terminals.INT");
                      	    
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:3861:8: lv_value_0_2= RULE_SIGNEDINT
                    {
                    lv_value_0_2=(Token)match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_value_0_2, grammarAccess.getIntegerValueAccess().getValueSIGNEDINTTerminalRuleCall_0_1()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getIntegerValueRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"value",
                              		lv_value_0_2, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
                      	    
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerValue"


    // $ANTLR start "entryRuleBooleanValue"
    // InternalCollaboration.g:3887:1: entryRuleBooleanValue returns [EObject current=null] : iv_ruleBooleanValue= ruleBooleanValue EOF ;
    public final EObject entryRuleBooleanValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanValue = null;


        try {
            // InternalCollaboration.g:3888:2: (iv_ruleBooleanValue= ruleBooleanValue EOF )
            // InternalCollaboration.g:3889:2: iv_ruleBooleanValue= ruleBooleanValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBooleanValue=ruleBooleanValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanValue"


    // $ANTLR start "ruleBooleanValue"
    // InternalCollaboration.g:3896:1: ruleBooleanValue returns [EObject current=null] : ( (lv_value_0_0= RULE_BOOL ) ) ;
    public final EObject ruleBooleanValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:3899:28: ( ( (lv_value_0_0= RULE_BOOL ) ) )
            // InternalCollaboration.g:3900:1: ( (lv_value_0_0= RULE_BOOL ) )
            {
            // InternalCollaboration.g:3900:1: ( (lv_value_0_0= RULE_BOOL ) )
            // InternalCollaboration.g:3901:1: (lv_value_0_0= RULE_BOOL )
            {
            // InternalCollaboration.g:3901:1: (lv_value_0_0= RULE_BOOL )
            // InternalCollaboration.g:3902:3: lv_value_0_0= RULE_BOOL
            {
            lv_value_0_0=(Token)match(input,RULE_BOOL,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_0_0, grammarAccess.getBooleanValueAccess().getValueBOOLTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getBooleanValueRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.BOOL");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanValue"


    // $ANTLR start "entryRuleStringValue"
    // InternalCollaboration.g:3926:1: entryRuleStringValue returns [EObject current=null] : iv_ruleStringValue= ruleStringValue EOF ;
    public final EObject entryRuleStringValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringValue = null;


        try {
            // InternalCollaboration.g:3927:2: (iv_ruleStringValue= ruleStringValue EOF )
            // InternalCollaboration.g:3928:2: iv_ruleStringValue= ruleStringValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStringValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStringValue=ruleStringValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStringValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringValue"


    // $ANTLR start "ruleStringValue"
    // InternalCollaboration.g:3935:1: ruleStringValue returns [EObject current=null] : ( (lv_value_0_0= RULE_STRING ) ) ;
    public final EObject ruleStringValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:3938:28: ( ( (lv_value_0_0= RULE_STRING ) ) )
            // InternalCollaboration.g:3939:1: ( (lv_value_0_0= RULE_STRING ) )
            {
            // InternalCollaboration.g:3939:1: ( (lv_value_0_0= RULE_STRING ) )
            // InternalCollaboration.g:3940:1: (lv_value_0_0= RULE_STRING )
            {
            // InternalCollaboration.g:3940:1: (lv_value_0_0= RULE_STRING )
            // InternalCollaboration.g:3941:3: lv_value_0_0= RULE_STRING
            {
            lv_value_0_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_0_0, grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getStringValueRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"org.eclipse.xtext.common.Terminals.STRING");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringValue"


    // $ANTLR start "entryRuleEnumValue"
    // InternalCollaboration.g:3965:1: entryRuleEnumValue returns [EObject current=null] : iv_ruleEnumValue= ruleEnumValue EOF ;
    public final EObject entryRuleEnumValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumValue = null;


        try {
            // InternalCollaboration.g:3966:2: (iv_ruleEnumValue= ruleEnumValue EOF )
            // InternalCollaboration.g:3967:2: iv_ruleEnumValue= ruleEnumValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEnumValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleEnumValue=ruleEnumValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEnumValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumValue"


    // $ANTLR start "ruleEnumValue"
    // InternalCollaboration.g:3974:1: ruleEnumValue returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) ) ;
    public final EObject ruleEnumValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:3977:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) ) )
            // InternalCollaboration.g:3978:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) )
            {
            // InternalCollaboration.g:3978:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) )
            // InternalCollaboration.g:3978:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) )
            {
            // InternalCollaboration.g:3978:2: ( (otherlv_0= RULE_ID ) )
            // InternalCollaboration.g:3979:1: (otherlv_0= RULE_ID )
            {
            // InternalCollaboration.g:3979:1: (otherlv_0= RULE_ID )
            // InternalCollaboration.g:3980:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getEnumValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_61); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getEnumValueAccess().getTypeEEnumCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,73,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getEnumValueAccess().getColonKeyword_1());
                  
            }
            // InternalCollaboration.g:3995:1: ( (otherlv_2= RULE_ID ) )
            // InternalCollaboration.g:3996:1: (otherlv_2= RULE_ID )
            {
            // InternalCollaboration.g:3996:1: (otherlv_2= RULE_ID )
            // InternalCollaboration.g:3997:3: otherlv_2= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getEnumValueRule());
              	        }
                      
            }
            otherlv_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_2, grammarAccess.getEnumValueAccess().getValueEEnumLiteralCrossReference_2_0()); 
              	
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumValue"


    // $ANTLR start "entryRuleNullValue"
    // InternalCollaboration.g:4016:1: entryRuleNullValue returns [EObject current=null] : iv_ruleNullValue= ruleNullValue EOF ;
    public final EObject entryRuleNullValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNullValue = null;


        try {
            // InternalCollaboration.g:4017:2: (iv_ruleNullValue= ruleNullValue EOF )
            // InternalCollaboration.g:4018:2: iv_ruleNullValue= ruleNullValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNullValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNullValue=ruleNullValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNullValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNullValue"


    // $ANTLR start "ruleNullValue"
    // InternalCollaboration.g:4025:1: ruleNullValue returns [EObject current=null] : ( () otherlv_1= 'null' ) ;
    public final EObject ruleNullValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:4028:28: ( ( () otherlv_1= 'null' ) )
            // InternalCollaboration.g:4029:1: ( () otherlv_1= 'null' )
            {
            // InternalCollaboration.g:4029:1: ( () otherlv_1= 'null' )
            // InternalCollaboration.g:4029:2: () otherlv_1= 'null'
            {
            // InternalCollaboration.g:4029:2: ()
            // InternalCollaboration.g:4030:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getNullValueAccess().getNullValueAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,74,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getNullValueAccess().getNullKeyword_1());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNullValue"


    // $ANTLR start "entryRuleVariableValue"
    // InternalCollaboration.g:4047:1: entryRuleVariableValue returns [EObject current=null] : iv_ruleVariableValue= ruleVariableValue EOF ;
    public final EObject entryRuleVariableValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableValue = null;


        try {
            // InternalCollaboration.g:4048:2: (iv_ruleVariableValue= ruleVariableValue EOF )
            // InternalCollaboration.g:4049:2: iv_ruleVariableValue= ruleVariableValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableValue=ruleVariableValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableValue"


    // $ANTLR start "ruleVariableValue"
    // InternalCollaboration.g:4056:1: ruleVariableValue returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleVariableValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:4059:28: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalCollaboration.g:4060:1: ( (otherlv_0= RULE_ID ) )
            {
            // InternalCollaboration.g:4060:1: ( (otherlv_0= RULE_ID ) )
            // InternalCollaboration.g:4061:1: (otherlv_0= RULE_ID )
            {
            // InternalCollaboration.g:4061:1: (otherlv_0= RULE_ID )
            // InternalCollaboration.g:4062:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getVariableValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getVariableValueAccess().getValueVariableCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableValue"


    // $ANTLR start "entryRuleCollectionAccess"
    // InternalCollaboration.g:4081:1: entryRuleCollectionAccess returns [EObject current=null] : iv_ruleCollectionAccess= ruleCollectionAccess EOF ;
    public final EObject entryRuleCollectionAccess() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCollectionAccess = null;


        try {
            // InternalCollaboration.g:4082:2: (iv_ruleCollectionAccess= ruleCollectionAccess EOF )
            // InternalCollaboration.g:4083:2: iv_ruleCollectionAccess= ruleCollectionAccess EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCollectionAccessRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCollectionAccess=ruleCollectionAccess();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCollectionAccess; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCollectionAccess"


    // $ANTLR start "ruleCollectionAccess"
    // InternalCollaboration.g:4090:1: ruleCollectionAccess returns [EObject current=null] : ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' ) ;
    public final EObject ruleCollectionAccess() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Enumerator lv_collectionOperation_0_0 = null;

        EObject lv_parameter_2_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:4093:28: ( ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' ) )
            // InternalCollaboration.g:4094:1: ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' )
            {
            // InternalCollaboration.g:4094:1: ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' )
            // InternalCollaboration.g:4094:2: ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')'
            {
            // InternalCollaboration.g:4094:2: ( (lv_collectionOperation_0_0= ruleCollectionOperation ) )
            // InternalCollaboration.g:4095:1: (lv_collectionOperation_0_0= ruleCollectionOperation )
            {
            // InternalCollaboration.g:4095:1: (lv_collectionOperation_0_0= ruleCollectionOperation )
            // InternalCollaboration.g:4096:3: lv_collectionOperation_0_0= ruleCollectionOperation
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getCollectionAccessAccess().getCollectionOperationCollectionOperationEnumRuleCall_0_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_46);
            lv_collectionOperation_0_0=ruleCollectionOperation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getCollectionAccessRule());
              	        }
                     		set(
                     			current, 
                     			"collectionOperation",
                      		lv_collectionOperation_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionOperation");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,37,FollowSets000.FOLLOW_62); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getCollectionAccessAccess().getLeftParenthesisKeyword_1());
                  
            }
            // InternalCollaboration.g:4116:1: ( (lv_parameter_2_0= ruleExpression ) )?
            int alt63=2;
            int LA63_0 = input.LA(1);

            if ( (LA63_0==RULE_ID||(LA63_0>=RULE_STRING && LA63_0<=RULE_BOOL)||LA63_0==37||LA63_0==70||LA63_0==72||LA63_0==74) ) {
                alt63=1;
            }
            switch (alt63) {
                case 1 :
                    // InternalCollaboration.g:4117:1: (lv_parameter_2_0= ruleExpression )
                    {
                    // InternalCollaboration.g:4117:1: (lv_parameter_2_0= ruleExpression )
                    // InternalCollaboration.g:4118:3: lv_parameter_2_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getCollectionAccessAccess().getParameterExpressionParserRuleCall_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_60);
                    lv_parameter_2_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getCollectionAccessRule());
                      	        }
                             		set(
                             			current, 
                             			"parameter",
                              		lv_parameter_2_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,38,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getCollectionAccessAccess().getRightParenthesisKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollectionAccess"


    // $ANTLR start "entryRuleFeatureAccess"
    // InternalCollaboration.g:4146:1: entryRuleFeatureAccess returns [EObject current=null] : iv_ruleFeatureAccess= ruleFeatureAccess EOF ;
    public final EObject entryRuleFeatureAccess() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureAccess = null;


        try {
            // InternalCollaboration.g:4147:2: (iv_ruleFeatureAccess= ruleFeatureAccess EOF )
            // InternalCollaboration.g:4148:2: iv_ruleFeatureAccess= ruleFeatureAccess EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFeatureAccessRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleFeatureAccess=ruleFeatureAccess();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFeatureAccess; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureAccess"


    // $ANTLR start "ruleFeatureAccess"
    // InternalCollaboration.g:4155:1: ruleFeatureAccess returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) | ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' ) ) ) ;
    public final EObject ruleFeatureAccess() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        EObject lv_value_2_0 = null;

        EObject lv_collectionAccess_4_0 = null;

        EObject lv_value_5_0 = null;

        EObject lv_parameters_7_0 = null;

        EObject lv_parameters_9_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:4158:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) | ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' ) ) ) )
            // InternalCollaboration.g:4159:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) | ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' ) ) )
            {
            // InternalCollaboration.g:4159:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) | ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' ) ) )
            // InternalCollaboration.g:4159:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) | ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' ) )
            {
            // InternalCollaboration.g:4159:2: ( (otherlv_0= RULE_ID ) )
            // InternalCollaboration.g:4160:1: (otherlv_0= RULE_ID )
            {
            // InternalCollaboration.g:4160:1: (otherlv_0= RULE_ID )
            // InternalCollaboration.g:4161:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getFeatureAccessRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_28); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getFeatureAccessAccess().getTargetEObjectCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,19,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getFeatureAccessAccess().getFullStopKeyword_1());
                  
            }
            // InternalCollaboration.g:4176:1: ( ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) | ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' ) )
            int alt67=2;
            int LA67_0 = input.LA(1);

            if ( (LA67_0==RULE_ID) ) {
                int LA67_1 = input.LA(2);

                if ( (LA67_1==37) ) {
                    alt67=2;
                }
                else if ( (LA67_1==EOF||LA67_1==RULE_ID||(LA67_1>=17 && LA67_1<=19)||LA67_1==29||LA67_1==31||(LA67_1>=34 && LA67_1<=35)||(LA67_1>=38 && LA67_1<=39)||LA67_1==41||(LA67_1>=43 && LA67_1<=44)||(LA67_1>=46 && LA67_1<=50)||(LA67_1>=56 && LA67_1<=71)||(LA67_1>=78 && LA67_1<=80)) ) {
                    alt67=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 67, 1, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 67, 0, input);

                throw nvae;
            }
            switch (alt67) {
                case 1 :
                    // InternalCollaboration.g:4176:2: ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? )
                    {
                    // InternalCollaboration.g:4176:2: ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? )
                    // InternalCollaboration.g:4176:3: ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )?
                    {
                    // InternalCollaboration.g:4176:3: ( (lv_value_2_0= ruleStructuralFeatureValue ) )
                    // InternalCollaboration.g:4177:1: (lv_value_2_0= ruleStructuralFeatureValue )
                    {
                    // InternalCollaboration.g:4177:1: (lv_value_2_0= ruleStructuralFeatureValue )
                    // InternalCollaboration.g:4178:3: lv_value_2_0= ruleStructuralFeatureValue
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getValueStructuralFeatureValueParserRuleCall_2_0_0_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_10);
                    lv_value_2_0=ruleStructuralFeatureValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
                      	        }
                             		set(
                             			current, 
                             			"value",
                              		lv_value_2_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.StructuralFeatureValue");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // InternalCollaboration.g:4194:2: (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )?
                    int alt64=2;
                    int LA64_0 = input.LA(1);

                    if ( (LA64_0==19) ) {
                        alt64=1;
                    }
                    switch (alt64) {
                        case 1 :
                            // InternalCollaboration.g:4194:4: otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) )
                            {
                            otherlv_3=(Token)match(input,19,FollowSets000.FOLLOW_63); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                  	newLeafNode(otherlv_3, grammarAccess.getFeatureAccessAccess().getFullStopKeyword_2_0_1_0());
                                  
                            }
                            // InternalCollaboration.g:4198:1: ( (lv_collectionAccess_4_0= ruleCollectionAccess ) )
                            // InternalCollaboration.g:4199:1: (lv_collectionAccess_4_0= ruleCollectionAccess )
                            {
                            // InternalCollaboration.g:4199:1: (lv_collectionAccess_4_0= ruleCollectionAccess )
                            // InternalCollaboration.g:4200:3: lv_collectionAccess_4_0= ruleCollectionAccess
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getCollectionAccessCollectionAccessParserRuleCall_2_0_1_1_0()); 
                              	    
                            }
                            pushFollow(FollowSets000.FOLLOW_2);
                            lv_collectionAccess_4_0=ruleCollectionAccess();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"collectionAccess",
                                      		lv_collectionAccess_4_0, 
                                      		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionAccess");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:4217:6: ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' )
                    {
                    // InternalCollaboration.g:4217:6: ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' )
                    // InternalCollaboration.g:4217:7: ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')'
                    {
                    // InternalCollaboration.g:4217:7: ( (lv_value_5_0= ruleOperationValue ) )
                    // InternalCollaboration.g:4218:1: (lv_value_5_0= ruleOperationValue )
                    {
                    // InternalCollaboration.g:4218:1: (lv_value_5_0= ruleOperationValue )
                    // InternalCollaboration.g:4219:3: lv_value_5_0= ruleOperationValue
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getValueOperationValueParserRuleCall_2_1_0_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_46);
                    lv_value_5_0=ruleOperationValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
                      	        }
                             		set(
                             			current, 
                             			"value",
                              		lv_value_5_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.OperationValue");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    otherlv_6=(Token)match(input,37,FollowSets000.FOLLOW_62); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_6, grammarAccess.getFeatureAccessAccess().getLeftParenthesisKeyword_2_1_1());
                          
                    }
                    // InternalCollaboration.g:4239:1: ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )?
                    int alt66=2;
                    int LA66_0 = input.LA(1);

                    if ( (LA66_0==RULE_ID||(LA66_0>=RULE_STRING && LA66_0<=RULE_BOOL)||LA66_0==37||LA66_0==70||LA66_0==72||LA66_0==74) ) {
                        alt66=1;
                    }
                    switch (alt66) {
                        case 1 :
                            // InternalCollaboration.g:4239:2: ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )*
                            {
                            // InternalCollaboration.g:4239:2: ( (lv_parameters_7_0= ruleExpression ) )
                            // InternalCollaboration.g:4240:1: (lv_parameters_7_0= ruleExpression )
                            {
                            // InternalCollaboration.g:4240:1: (lv_parameters_7_0= ruleExpression )
                            // InternalCollaboration.g:4241:3: lv_parameters_7_0= ruleExpression
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getParametersExpressionParserRuleCall_2_1_2_0_0()); 
                              	    
                            }
                            pushFollow(FollowSets000.FOLLOW_33);
                            lv_parameters_7_0=ruleExpression();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
                              	        }
                                     		add(
                                     			current, 
                                     			"parameters",
                                      		lv_parameters_7_0, 
                                      		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }

                            // InternalCollaboration.g:4257:2: (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )*
                            loop65:
                            do {
                                int alt65=2;
                                int LA65_0 = input.LA(1);

                                if ( (LA65_0==31) ) {
                                    alt65=1;
                                }


                                switch (alt65) {
                            	case 1 :
                            	    // InternalCollaboration.g:4257:4: otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) )
                            	    {
                            	    otherlv_8=(Token)match(input,31,FollowSets000.FOLLOW_40); if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	          	newLeafNode(otherlv_8, grammarAccess.getFeatureAccessAccess().getCommaKeyword_2_1_2_1_0());
                            	          
                            	    }
                            	    // InternalCollaboration.g:4261:1: ( (lv_parameters_9_0= ruleExpression ) )
                            	    // InternalCollaboration.g:4262:1: (lv_parameters_9_0= ruleExpression )
                            	    {
                            	    // InternalCollaboration.g:4262:1: (lv_parameters_9_0= ruleExpression )
                            	    // InternalCollaboration.g:4263:3: lv_parameters_9_0= ruleExpression
                            	    {
                            	    if ( state.backtracking==0 ) {
                            	       
                            	      	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getParametersExpressionParserRuleCall_2_1_2_1_1_0()); 
                            	      	    
                            	    }
                            	    pushFollow(FollowSets000.FOLLOW_33);
                            	    lv_parameters_9_0=ruleExpression();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      	        if (current==null) {
                            	      	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
                            	      	        }
                            	             		add(
                            	             			current, 
                            	             			"parameters",
                            	              		lv_parameters_9_0, 
                            	              		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
                            	      	        afterParserOrEnumRuleCall();
                            	      	    
                            	    }

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop65;
                                }
                            } while (true);


                            }
                            break;

                    }

                    otherlv_10=(Token)match(input,38,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_10, grammarAccess.getFeatureAccessAccess().getRightParenthesisKeyword_2_1_3());
                          
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureAccess"


    // $ANTLR start "entryRuleStructuralFeatureValue"
    // InternalCollaboration.g:4291:1: entryRuleStructuralFeatureValue returns [EObject current=null] : iv_ruleStructuralFeatureValue= ruleStructuralFeatureValue EOF ;
    public final EObject entryRuleStructuralFeatureValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStructuralFeatureValue = null;


        try {
            // InternalCollaboration.g:4292:2: (iv_ruleStructuralFeatureValue= ruleStructuralFeatureValue EOF )
            // InternalCollaboration.g:4293:2: iv_ruleStructuralFeatureValue= ruleStructuralFeatureValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStructuralFeatureValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStructuralFeatureValue=ruleStructuralFeatureValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStructuralFeatureValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStructuralFeatureValue"


    // $ANTLR start "ruleStructuralFeatureValue"
    // InternalCollaboration.g:4300:1: ruleStructuralFeatureValue returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleStructuralFeatureValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:4303:28: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalCollaboration.g:4304:1: ( (otherlv_0= RULE_ID ) )
            {
            // InternalCollaboration.g:4304:1: ( (otherlv_0= RULE_ID ) )
            // InternalCollaboration.g:4305:1: (otherlv_0= RULE_ID )
            {
            // InternalCollaboration.g:4305:1: (otherlv_0= RULE_ID )
            // InternalCollaboration.g:4306:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getStructuralFeatureValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStructuralFeatureValue"


    // $ANTLR start "entryRuleOperationValue"
    // InternalCollaboration.g:4325:1: entryRuleOperationValue returns [EObject current=null] : iv_ruleOperationValue= ruleOperationValue EOF ;
    public final EObject entryRuleOperationValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOperationValue = null;


        try {
            // InternalCollaboration.g:4326:2: (iv_ruleOperationValue= ruleOperationValue EOF )
            // InternalCollaboration.g:4327:2: iv_ruleOperationValue= ruleOperationValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOperationValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleOperationValue=ruleOperationValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOperationValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOperationValue"


    // $ANTLR start "ruleOperationValue"
    // InternalCollaboration.g:4334:1: ruleOperationValue returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleOperationValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:4337:28: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalCollaboration.g:4338:1: ( (otherlv_0= RULE_ID ) )
            {
            // InternalCollaboration.g:4338:1: ( (otherlv_0= RULE_ID ) )
            // InternalCollaboration.g:4339:1: (otherlv_0= RULE_ID )
            {
            // InternalCollaboration.g:4339:1: (otherlv_0= RULE_ID )
            // InternalCollaboration.g:4340:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getOperationValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getOperationValueAccess().getValueEOperationCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOperationValue"


    // $ANTLR start "ruleScenarioKind"
    // InternalCollaboration.g:4359:1: ruleScenarioKind returns [Enumerator current=null] : ( (enumLiteral_0= 'assumption' ) | (enumLiteral_1= 'guarantee' ) | (enumLiteral_2= 'existential' ) ) ;
    public final Enumerator ruleScenarioKind() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;

         enterRule(); 
        try {
            // InternalCollaboration.g:4361:28: ( ( (enumLiteral_0= 'assumption' ) | (enumLiteral_1= 'guarantee' ) | (enumLiteral_2= 'existential' ) ) )
            // InternalCollaboration.g:4362:1: ( (enumLiteral_0= 'assumption' ) | (enumLiteral_1= 'guarantee' ) | (enumLiteral_2= 'existential' ) )
            {
            // InternalCollaboration.g:4362:1: ( (enumLiteral_0= 'assumption' ) | (enumLiteral_1= 'guarantee' ) | (enumLiteral_2= 'existential' ) )
            int alt68=3;
            switch ( input.LA(1) ) {
            case 75:
                {
                alt68=1;
                }
                break;
            case 76:
                {
                alt68=2;
                }
                break;
            case 77:
                {
                alt68=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 68, 0, input);

                throw nvae;
            }

            switch (alt68) {
                case 1 :
                    // InternalCollaboration.g:4362:2: (enumLiteral_0= 'assumption' )
                    {
                    // InternalCollaboration.g:4362:2: (enumLiteral_0= 'assumption' )
                    // InternalCollaboration.g:4362:4: enumLiteral_0= 'assumption'
                    {
                    enumLiteral_0=(Token)match(input,75,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getScenarioKindAccess().getAssumptionEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getScenarioKindAccess().getAssumptionEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:4368:6: (enumLiteral_1= 'guarantee' )
                    {
                    // InternalCollaboration.g:4368:6: (enumLiteral_1= 'guarantee' )
                    // InternalCollaboration.g:4368:8: enumLiteral_1= 'guarantee'
                    {
                    enumLiteral_1=(Token)match(input,76,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getScenarioKindAccess().getGuaranteeEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getScenarioKindAccess().getGuaranteeEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:4374:6: (enumLiteral_2= 'existential' )
                    {
                    // InternalCollaboration.g:4374:6: (enumLiteral_2= 'existential' )
                    // InternalCollaboration.g:4374:8: enumLiteral_2= 'existential'
                    {
                    enumLiteral_2=(Token)match(input,77,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getScenarioKindAccess().getExistentialEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getScenarioKindAccess().getExistentialEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleScenarioKind"


    // $ANTLR start "ruleExpectationKind"
    // InternalCollaboration.g:4384:1: ruleExpectationKind returns [Enumerator current=null] : ( (enumLiteral_0= 'eventually' ) | (enumLiteral_1= 'requested' ) | (enumLiteral_2= 'urgent' ) | (enumLiteral_3= 'committed' ) ) ;
    public final Enumerator ruleExpectationKind() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;

         enterRule(); 
        try {
            // InternalCollaboration.g:4386:28: ( ( (enumLiteral_0= 'eventually' ) | (enumLiteral_1= 'requested' ) | (enumLiteral_2= 'urgent' ) | (enumLiteral_3= 'committed' ) ) )
            // InternalCollaboration.g:4387:1: ( (enumLiteral_0= 'eventually' ) | (enumLiteral_1= 'requested' ) | (enumLiteral_2= 'urgent' ) | (enumLiteral_3= 'committed' ) )
            {
            // InternalCollaboration.g:4387:1: ( (enumLiteral_0= 'eventually' ) | (enumLiteral_1= 'requested' ) | (enumLiteral_2= 'urgent' ) | (enumLiteral_3= 'committed' ) )
            int alt69=4;
            switch ( input.LA(1) ) {
            case 47:
                {
                alt69=1;
                }
                break;
            case 78:
                {
                alt69=2;
                }
                break;
            case 79:
                {
                alt69=3;
                }
                break;
            case 80:
                {
                alt69=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 69, 0, input);

                throw nvae;
            }

            switch (alt69) {
                case 1 :
                    // InternalCollaboration.g:4387:2: (enumLiteral_0= 'eventually' )
                    {
                    // InternalCollaboration.g:4387:2: (enumLiteral_0= 'eventually' )
                    // InternalCollaboration.g:4387:4: enumLiteral_0= 'eventually'
                    {
                    enumLiteral_0=(Token)match(input,47,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getExpectationKindAccess().getEventuallyEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getExpectationKindAccess().getEventuallyEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:4393:6: (enumLiteral_1= 'requested' )
                    {
                    // InternalCollaboration.g:4393:6: (enumLiteral_1= 'requested' )
                    // InternalCollaboration.g:4393:8: enumLiteral_1= 'requested'
                    {
                    enumLiteral_1=(Token)match(input,78,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getExpectationKindAccess().getRequestedEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getExpectationKindAccess().getRequestedEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:4399:6: (enumLiteral_2= 'urgent' )
                    {
                    // InternalCollaboration.g:4399:6: (enumLiteral_2= 'urgent' )
                    // InternalCollaboration.g:4399:8: enumLiteral_2= 'urgent'
                    {
                    enumLiteral_2=(Token)match(input,79,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getExpectationKindAccess().getUrgentEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getExpectationKindAccess().getUrgentEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalCollaboration.g:4405:6: (enumLiteral_3= 'committed' )
                    {
                    // InternalCollaboration.g:4405:6: (enumLiteral_3= 'committed' )
                    // InternalCollaboration.g:4405:8: enumLiteral_3= 'committed'
                    {
                    enumLiteral_3=(Token)match(input,80,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getExpectationKindAccess().getCommittedEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_3, grammarAccess.getExpectationKindAccess().getCommittedEnumLiteralDeclaration_3()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpectationKind"


    // $ANTLR start "ruleCollectionOperation"
    // InternalCollaboration.g:4415:1: ruleCollectionOperation returns [Enumerator current=null] : ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) ) ;
    public final Enumerator ruleCollectionOperation() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;
        Token enumLiteral_7=null;

         enterRule(); 
        try {
            // InternalCollaboration.g:4417:28: ( ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) ) )
            // InternalCollaboration.g:4418:1: ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) )
            {
            // InternalCollaboration.g:4418:1: ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) )
            int alt70=8;
            switch ( input.LA(1) ) {
            case 81:
                {
                alt70=1;
                }
                break;
            case 82:
                {
                alt70=2;
                }
                break;
            case 83:
                {
                alt70=3;
                }
                break;
            case 84:
                {
                alt70=4;
                }
                break;
            case 85:
                {
                alt70=5;
                }
                break;
            case 86:
                {
                alt70=6;
                }
                break;
            case 87:
                {
                alt70=7;
                }
                break;
            case 88:
                {
                alt70=8;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 70, 0, input);

                throw nvae;
            }

            switch (alt70) {
                case 1 :
                    // InternalCollaboration.g:4418:2: (enumLiteral_0= 'any' )
                    {
                    // InternalCollaboration.g:4418:2: (enumLiteral_0= 'any' )
                    // InternalCollaboration.g:4418:4: enumLiteral_0= 'any'
                    {
                    enumLiteral_0=(Token)match(input,81,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:4424:6: (enumLiteral_1= 'contains' )
                    {
                    // InternalCollaboration.g:4424:6: (enumLiteral_1= 'contains' )
                    // InternalCollaboration.g:4424:8: enumLiteral_1= 'contains'
                    {
                    enumLiteral_1=(Token)match(input,82,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:4430:6: (enumLiteral_2= 'containsAll' )
                    {
                    // InternalCollaboration.g:4430:6: (enumLiteral_2= 'containsAll' )
                    // InternalCollaboration.g:4430:8: enumLiteral_2= 'containsAll'
                    {
                    enumLiteral_2=(Token)match(input,83,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalCollaboration.g:4436:6: (enumLiteral_3= 'first' )
                    {
                    // InternalCollaboration.g:4436:6: (enumLiteral_3= 'first' )
                    // InternalCollaboration.g:4436:8: enumLiteral_3= 'first'
                    {
                    enumLiteral_3=(Token)match(input,84,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_3, grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3()); 
                          
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalCollaboration.g:4442:6: (enumLiteral_4= 'get' )
                    {
                    // InternalCollaboration.g:4442:6: (enumLiteral_4= 'get' )
                    // InternalCollaboration.g:4442:8: enumLiteral_4= 'get'
                    {
                    enumLiteral_4=(Token)match(input,85,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_4, grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4()); 
                          
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalCollaboration.g:4448:6: (enumLiteral_5= 'isEmpty' )
                    {
                    // InternalCollaboration.g:4448:6: (enumLiteral_5= 'isEmpty' )
                    // InternalCollaboration.g:4448:8: enumLiteral_5= 'isEmpty'
                    {
                    enumLiteral_5=(Token)match(input,86,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_5, grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5()); 
                          
                    }

                    }


                    }
                    break;
                case 7 :
                    // InternalCollaboration.g:4454:6: (enumLiteral_6= 'last' )
                    {
                    // InternalCollaboration.g:4454:6: (enumLiteral_6= 'last' )
                    // InternalCollaboration.g:4454:8: enumLiteral_6= 'last'
                    {
                    enumLiteral_6=(Token)match(input,87,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_6, grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6()); 
                          
                    }

                    }


                    }
                    break;
                case 8 :
                    // InternalCollaboration.g:4460:6: (enumLiteral_7= 'size' )
                    {
                    // InternalCollaboration.g:4460:6: (enumLiteral_7= 'size' )
                    // InternalCollaboration.g:4460:8: enumLiteral_7= 'size'
                    {
                    enumLiteral_7=(Token)match(input,88,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_7, grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollectionOperation"


    // $ANTLR start "ruleCollectionModification"
    // InternalCollaboration.g:4470:1: ruleCollectionModification returns [Enumerator current=null] : ( (enumLiteral_0= 'add' ) | (enumLiteral_1= 'addToFront' ) | (enumLiteral_2= 'clear' ) | (enumLiteral_3= 'remove' ) ) ;
    public final Enumerator ruleCollectionModification() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;

         enterRule(); 
        try {
            // InternalCollaboration.g:4472:28: ( ( (enumLiteral_0= 'add' ) | (enumLiteral_1= 'addToFront' ) | (enumLiteral_2= 'clear' ) | (enumLiteral_3= 'remove' ) ) )
            // InternalCollaboration.g:4473:1: ( (enumLiteral_0= 'add' ) | (enumLiteral_1= 'addToFront' ) | (enumLiteral_2= 'clear' ) | (enumLiteral_3= 'remove' ) )
            {
            // InternalCollaboration.g:4473:1: ( (enumLiteral_0= 'add' ) | (enumLiteral_1= 'addToFront' ) | (enumLiteral_2= 'clear' ) | (enumLiteral_3= 'remove' ) )
            int alt71=4;
            switch ( input.LA(1) ) {
            case 89:
                {
                alt71=1;
                }
                break;
            case 90:
                {
                alt71=2;
                }
                break;
            case 91:
                {
                alt71=3;
                }
                break;
            case 92:
                {
                alt71=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 71, 0, input);

                throw nvae;
            }

            switch (alt71) {
                case 1 :
                    // InternalCollaboration.g:4473:2: (enumLiteral_0= 'add' )
                    {
                    // InternalCollaboration.g:4473:2: (enumLiteral_0= 'add' )
                    // InternalCollaboration.g:4473:4: enumLiteral_0= 'add'
                    {
                    enumLiteral_0=(Token)match(input,89,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionModificationAccess().getAddEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getCollectionModificationAccess().getAddEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:4479:6: (enumLiteral_1= 'addToFront' )
                    {
                    // InternalCollaboration.g:4479:6: (enumLiteral_1= 'addToFront' )
                    // InternalCollaboration.g:4479:8: enumLiteral_1= 'addToFront'
                    {
                    enumLiteral_1=(Token)match(input,90,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionModificationAccess().getAddToFrontEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getCollectionModificationAccess().getAddToFrontEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:4485:6: (enumLiteral_2= 'clear' )
                    {
                    // InternalCollaboration.g:4485:6: (enumLiteral_2= 'clear' )
                    // InternalCollaboration.g:4485:8: enumLiteral_2= 'clear'
                    {
                    enumLiteral_2=(Token)match(input,91,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionModificationAccess().getClearEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getCollectionModificationAccess().getClearEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalCollaboration.g:4491:6: (enumLiteral_3= 'remove' )
                    {
                    // InternalCollaboration.g:4491:6: (enumLiteral_3= 'remove' )
                    // InternalCollaboration.g:4491:8: enumLiteral_3= 'remove'
                    {
                    enumLiteral_3=(Token)match(input,92,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionModificationAccess().getRemoveEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_3, grammarAccess.getCollectionModificationAccess().getRemoveEnumLiteralDeclaration_3()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollectionModification"

    // Delegated rules


    protected DFA15 dfa15 = new DFA15(this);
    static final String dfa_1s = "\12\uffff";
    static final String dfa_2s = "\1\4\2\uffff\1\41\6\uffff";
    static final String dfa_3s = "\1\120\2\uffff\1\44\6\uffff";
    static final String dfa_4s = "\1\uffff\1\1\1\2\1\uffff\1\3\1\4\1\5\1\6\1\7\1\10";
    static final String dfa_5s = "\12\uffff}>";
    static final String[] dfa_6s = {
            "\1\3\14\uffff\1\1\20\uffff\2\2\5\uffff\1\4\1\uffff\1\5\1\6\1\uffff\1\7\1\2\2\7\1\10\6\uffff\3\11\22\uffff\3\2",
            "",
            "",
            "\1\11\2\uffff\1\2",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA15 extends DFA {

        public DFA15(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 15;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "698:1: (this_Interaction_0= ruleInteraction | this_ModalMessage_1= ruleModalMessage | this_Alternative_2= ruleAlternative | this_Loop_3= ruleLoop | this_Parallel_4= ruleParallel | this_ConditionFragment_5= ruleConditionFragment | this_TimedConditionFragment_6= ruleTimedConditionFragment | this_VariableFragment_7= ruleVariableFragment )";
        }
    }
 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x008000000001C000L});
        public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000000000001C000L});
        public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000018000L});
        public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000001340000L,0x0000000000003800L});
        public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000001040000L,0x0000000000003800L});
        public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000080002L});
        public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000800000L});
        public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000C00000L});
        public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000001000000L,0x0000000000003800L});
        public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x000000014C020000L});
        public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000008000000L});
        public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000010000000L});
        public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000020000000L});
        public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x00000001CC020000L});
        public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000020000010L});
        public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000200000000L});
        public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0E07DA0D4C060010L,0x000000000001C000L});
        public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0008000000000002L});
        public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000800800000010L,0x000000000001C000L});
        public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000800800000000L,0x000000000001C000L});
        public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000001000000000L});
        public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000000080000L});
        public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000002000080002L});
        public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000000000000L,0x000000001E000000L});
        public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000002000000002L});
        public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x000001E0000003D0L,0x0000000000000540L});
        public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000004080000000L});
        public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x000001A0000003D0L,0x0000000000000540L});
        public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x000000015C020000L});
        public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000040000000002L});
        public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000200000000002L});
        public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000800410000000L});
        public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000800010000000L});
        public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x00000020000003D0L,0x0000000000000540L});
        public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000400000000000L});
        public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0002000000000000L});
        public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0001000000000000L});
        public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0071000020000000L});
        public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0000002000080000L});
        public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000002000000000L});
        public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0E07DA2D4C0603D0L,0x000000000001C540L});
        public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0100000000000000L});
        public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0000000200000002L});
        public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0000000000000180L});
        public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x1000000000000002L});
        public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x2000000000000002L});
        public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x4000000000000002L});
        public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x8000000000000002L,0x000000000000001FL});
        public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x8000000000000000L,0x000000000000001EL});
        public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x0000000000000080L});
        public static final BitSet FOLLOW_58 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000060L});
        public static final BitSet FOLLOW_59 = new BitSet(new long[]{0x0000008000000002L,0x0000000000000080L});
        public static final BitSet FOLLOW_60 = new BitSet(new long[]{0x0000004000000000L});
        public static final BitSet FOLLOW_61 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
        public static final BitSet FOLLOW_62 = new BitSet(new long[]{0x00000060000003D0L,0x0000000000000540L});
        public static final BitSet FOLLOW_63 = new BitSet(new long[]{0x0000000000000000L,0x0000000001FE0000L});
    }


}