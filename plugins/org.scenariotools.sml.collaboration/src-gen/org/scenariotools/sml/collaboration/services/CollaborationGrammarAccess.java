/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.collaboration.services;

import com.google.inject.Singleton;
import com.google.inject.Inject;

import java.util.List;

import org.eclipse.xtext.*;
import org.eclipse.xtext.service.GrammarProvider;
import org.eclipse.xtext.service.AbstractElementFinder.*;

import org.scenariotools.sml.expressions.services.ScenarioExpressionsGrammarAccess;
import org.eclipse.xtext.common.services.TerminalsGrammarAccess;

@Singleton
public class CollaborationGrammarAccess extends AbstractGrammarElementFinder {
	
	
	public class CollaborationElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.Collaboration");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cImportsAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final RuleCall cImportsImportParserRuleCall_0_0 = (RuleCall)cImportsAssignment_0.eContents().get(0);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Keyword cDomainKeyword_1_0 = (Keyword)cGroup_1.eContents().get(0);
		private final Assignment cDomainsAssignment_1_1 = (Assignment)cGroup_1.eContents().get(1);
		private final CrossReference cDomainsEPackageCrossReference_1_1_0 = (CrossReference)cDomainsAssignment_1_1.eContents().get(0);
		private final RuleCall cDomainsEPackageFQNParserRuleCall_1_1_0_1 = (RuleCall)cDomainsEPackageCrossReference_1_1_0.eContents().get(1);
		private final Group cGroup_2 = (Group)cGroup.eContents().get(2);
		private final Keyword cContextsKeyword_2_0 = (Keyword)cGroup_2.eContents().get(0);
		private final Assignment cContextsAssignment_2_1 = (Assignment)cGroup_2.eContents().get(1);
		private final CrossReference cContextsEPackageCrossReference_2_1_0 = (CrossReference)cContextsAssignment_2_1.eContents().get(0);
		private final RuleCall cContextsEPackageFQNParserRuleCall_2_1_0_1 = (RuleCall)cContextsEPackageCrossReference_2_1_0.eContents().get(1);
		private final Keyword cCollaborationKeyword_3 = (Keyword)cGroup.eContents().get(3);
		private final Assignment cNameAssignment_4 = (Assignment)cGroup.eContents().get(4);
		private final RuleCall cNameIDTerminalRuleCall_4_0 = (RuleCall)cNameAssignment_4.eContents().get(0);
		private final Keyword cLeftCurlyBracketKeyword_5 = (Keyword)cGroup.eContents().get(5);
		private final Assignment cRolesAssignment_6 = (Assignment)cGroup.eContents().get(6);
		private final RuleCall cRolesRoleParserRuleCall_6_0 = (RuleCall)cRolesAssignment_6.eContents().get(0);
		private final Assignment cScenariosAssignment_7 = (Assignment)cGroup.eContents().get(7);
		private final RuleCall cScenariosScenarioParserRuleCall_7_0 = (RuleCall)cScenariosAssignment_7.eContents().get(0);
		private final Keyword cRightCurlyBracketKeyword_8 = (Keyword)cGroup.eContents().get(8);
		
		//Collaboration:
		//	imports+=Import* ('domain' domains+=[EPackage|FQN])* ('contexts' contexts+=[EPackage|FQN])* 'collaboration' name=ID
		//	'{' roles+=Role* scenarios+=Scenario* '}';
		@Override public ParserRule getRule() { return rule; }

		//imports+=Import* ('domain' domains+=[EPackage|FQN])* ('contexts' contexts+=[EPackage|FQN])* 'collaboration' name=ID '{'
		//roles+=Role* scenarios+=Scenario* '}'
		public Group getGroup() { return cGroup; }

		//imports+=Import*
		public Assignment getImportsAssignment_0() { return cImportsAssignment_0; }

		//Import
		public RuleCall getImportsImportParserRuleCall_0_0() { return cImportsImportParserRuleCall_0_0; }

		//('domain' domains+=[EPackage|FQN])*
		public Group getGroup_1() { return cGroup_1; }

		//'domain'
		public Keyword getDomainKeyword_1_0() { return cDomainKeyword_1_0; }

		//domains+=[EPackage|FQN]
		public Assignment getDomainsAssignment_1_1() { return cDomainsAssignment_1_1; }

		//[EPackage|FQN]
		public CrossReference getDomainsEPackageCrossReference_1_1_0() { return cDomainsEPackageCrossReference_1_1_0; }

		//FQN
		public RuleCall getDomainsEPackageFQNParserRuleCall_1_1_0_1() { return cDomainsEPackageFQNParserRuleCall_1_1_0_1; }

		//('contexts' contexts+=[EPackage|FQN])*
		public Group getGroup_2() { return cGroup_2; }

		//'contexts'
		public Keyword getContextsKeyword_2_0() { return cContextsKeyword_2_0; }

		//contexts+=[EPackage|FQN]
		public Assignment getContextsAssignment_2_1() { return cContextsAssignment_2_1; }

		//[EPackage|FQN]
		public CrossReference getContextsEPackageCrossReference_2_1_0() { return cContextsEPackageCrossReference_2_1_0; }

		//FQN
		public RuleCall getContextsEPackageFQNParserRuleCall_2_1_0_1() { return cContextsEPackageFQNParserRuleCall_2_1_0_1; }

		//'collaboration'
		public Keyword getCollaborationKeyword_3() { return cCollaborationKeyword_3; }

		//name=ID
		public Assignment getNameAssignment_4() { return cNameAssignment_4; }

		//ID
		public RuleCall getNameIDTerminalRuleCall_4_0() { return cNameIDTerminalRuleCall_4_0; }

		//'{'
		public Keyword getLeftCurlyBracketKeyword_5() { return cLeftCurlyBracketKeyword_5; }

		//roles+=Role*
		public Assignment getRolesAssignment_6() { return cRolesAssignment_6; }

		//Role
		public RuleCall getRolesRoleParserRuleCall_6_0() { return cRolesRoleParserRuleCall_6_0; }

		//scenarios+=Scenario*
		public Assignment getScenariosAssignment_7() { return cScenariosAssignment_7; }

		//Scenario
		public RuleCall getScenariosScenarioParserRuleCall_7_0() { return cScenariosScenarioParserRuleCall_7_0; }

		//'}'
		public Keyword getRightCurlyBracketKeyword_8() { return cRightCurlyBracketKeyword_8; }
	}

	public class FQNElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.FQN");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final RuleCall cIDTerminalRuleCall_0 = (RuleCall)cGroup.eContents().get(0);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Keyword cFullStopKeyword_1_0 = (Keyword)cGroup_1.eContents().get(0);
		private final RuleCall cIDTerminalRuleCall_1_1 = (RuleCall)cGroup_1.eContents().get(1);
		
		//// Terminal-Definitions
		// // ------------------------------
		// FQN:
		//	ID ('.' ID)*;
		@Override public ParserRule getRule() { return rule; }

		//ID ('.' ID)*
		public Group getGroup() { return cGroup; }

		//ID
		public RuleCall getIDTerminalRuleCall_0() { return cIDTerminalRuleCall_0; }

		//('.' ID)*
		public Group getGroup_1() { return cGroup_1; }

		//'.'
		public Keyword getFullStopKeyword_1_0() { return cFullStopKeyword_1_0; }

		//ID
		public RuleCall getIDTerminalRuleCall_1_1() { return cIDTerminalRuleCall_1_1; }
	}

	public class RoleElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.Role");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Alternatives cAlternatives_0 = (Alternatives)cGroup.eContents().get(0);
		private final Assignment cStaticAssignment_0_0 = (Assignment)cAlternatives_0.eContents().get(0);
		private final Keyword cStaticStaticKeyword_0_0_0 = (Keyword)cStaticAssignment_0_0.eContents().get(0);
		private final Group cGroup_0_1 = (Group)cAlternatives_0.eContents().get(1);
		private final Keyword cDynamicKeyword_0_1_0 = (Keyword)cGroup_0_1.eContents().get(0);
		private final Assignment cMultiRoleAssignment_0_1_1 = (Assignment)cGroup_0_1.eContents().get(1);
		private final Keyword cMultiRoleMultiKeyword_0_1_1_0 = (Keyword)cMultiRoleAssignment_0_1_1.eContents().get(0);
		private final Keyword cRoleKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cTypeAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final CrossReference cTypeEClassCrossReference_2_0 = (CrossReference)cTypeAssignment_2.eContents().get(0);
		private final RuleCall cTypeEClassIDTerminalRuleCall_2_0_1 = (RuleCall)cTypeEClassCrossReference_2_0.eContents().get(1);
		private final Assignment cNameAssignment_3 = (Assignment)cGroup.eContents().get(3);
		private final RuleCall cNameIDTerminalRuleCall_3_0 = (RuleCall)cNameAssignment_3.eContents().get(0);
		
		//Role:
		//	(static?='static' | 'dynamic' multiRole?='multi'?) 'role' type=[EClass] name=ID;
		@Override public ParserRule getRule() { return rule; }

		//(static?='static' | 'dynamic' multiRole?='multi'?) 'role' type=[EClass] name=ID
		public Group getGroup() { return cGroup; }

		//static?='static' | 'dynamic' multiRole?='multi'?
		public Alternatives getAlternatives_0() { return cAlternatives_0; }

		//static?='static'
		public Assignment getStaticAssignment_0_0() { return cStaticAssignment_0_0; }

		//'static'
		public Keyword getStaticStaticKeyword_0_0_0() { return cStaticStaticKeyword_0_0_0; }

		//'dynamic' multiRole?='multi'?
		public Group getGroup_0_1() { return cGroup_0_1; }

		//'dynamic'
		public Keyword getDynamicKeyword_0_1_0() { return cDynamicKeyword_0_1_0; }

		//multiRole?='multi'?
		public Assignment getMultiRoleAssignment_0_1_1() { return cMultiRoleAssignment_0_1_1; }

		//'multi'
		public Keyword getMultiRoleMultiKeyword_0_1_1_0() { return cMultiRoleMultiKeyword_0_1_1_0; }

		//'role'
		public Keyword getRoleKeyword_1() { return cRoleKeyword_1; }

		//type=[EClass]
		public Assignment getTypeAssignment_2() { return cTypeAssignment_2; }

		//[EClass]
		public CrossReference getTypeEClassCrossReference_2_0() { return cTypeEClassCrossReference_2_0; }

		//ID
		public RuleCall getTypeEClassIDTerminalRuleCall_2_0_1() { return cTypeEClassIDTerminalRuleCall_2_0_1; }

		//name=ID
		public Assignment getNameAssignment_3() { return cNameAssignment_3; }

		//ID
		public RuleCall getNameIDTerminalRuleCall_3_0() { return cNameIDTerminalRuleCall_3_0; }
	}

	public class ScenarioElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.Scenario");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cSingularAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final Keyword cSingularSingularKeyword_0_0 = (Keyword)cSingularAssignment_0.eContents().get(0);
		private final Assignment cKindAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cKindScenarioKindEnumRuleCall_1_0 = (RuleCall)cKindAssignment_1.eContents().get(0);
		private final Keyword cScenarioKeyword_2 = (Keyword)cGroup.eContents().get(2);
		private final Assignment cNameAssignment_3 = (Assignment)cGroup.eContents().get(3);
		private final RuleCall cNameIDTerminalRuleCall_3_0 = (RuleCall)cNameAssignment_3.eContents().get(0);
		private final Alternatives cAlternatives_4 = (Alternatives)cGroup.eContents().get(4);
		private final Group cGroup_4_0 = (Group)cAlternatives_4.eContents().get(0);
		private final Assignment cOptimizeCostAssignment_4_0_0 = (Assignment)cGroup_4_0.eContents().get(0);
		private final Keyword cOptimizeCostOptimizeKeyword_4_0_0_0 = (Keyword)cOptimizeCostAssignment_4_0_0.eContents().get(0);
		private final Keyword cCostKeyword_4_0_1 = (Keyword)cGroup_4_0.eContents().get(1);
		private final Group cGroup_4_1 = (Group)cAlternatives_4.eContents().get(1);
		private final Keyword cCostKeyword_4_1_0 = (Keyword)cGroup_4_1.eContents().get(0);
		private final Keyword cLeftSquareBracketKeyword_4_1_1 = (Keyword)cGroup_4_1.eContents().get(1);
		private final Assignment cCostAssignment_4_1_2 = (Assignment)cGroup_4_1.eContents().get(2);
		private final RuleCall cCostDOUBLETerminalRuleCall_4_1_2_0 = (RuleCall)cCostAssignment_4_1_2.eContents().get(0);
		private final Keyword cRightSquareBracketKeyword_4_1_3 = (Keyword)cGroup_4_1.eContents().get(3);
		private final Group cGroup_5 = (Group)cGroup.eContents().get(5);
		private final Keyword cContextKeyword_5_0 = (Keyword)cGroup_5.eContents().get(0);
		private final Assignment cContextsAssignment_5_1 = (Assignment)cGroup_5.eContents().get(1);
		private final CrossReference cContextsEClassCrossReference_5_1_0 = (CrossReference)cContextsAssignment_5_1.eContents().get(0);
		private final RuleCall cContextsEClassIDTerminalRuleCall_5_1_0_1 = (RuleCall)cContextsEClassCrossReference_5_1_0.eContents().get(1);
		private final Group cGroup_5_2 = (Group)cGroup_5.eContents().get(2);
		private final Keyword cCommaKeyword_5_2_0 = (Keyword)cGroup_5_2.eContents().get(0);
		private final Assignment cContextsAssignment_5_2_1 = (Assignment)cGroup_5_2.eContents().get(1);
		private final CrossReference cContextsEClassCrossReference_5_2_1_0 = (CrossReference)cContextsAssignment_5_2_1.eContents().get(0);
		private final RuleCall cContextsEClassIDTerminalRuleCall_5_2_1_0_1 = (RuleCall)cContextsEClassCrossReference_5_2_1_0.eContents().get(1);
		private final Group cGroup_6 = (Group)cGroup.eContents().get(6);
		private final Keyword cBindingsKeyword_6_0 = (Keyword)cGroup_6.eContents().get(0);
		private final Keyword cLeftSquareBracketKeyword_6_1 = (Keyword)cGroup_6.eContents().get(1);
		private final Assignment cRoleBindingsAssignment_6_2 = (Assignment)cGroup_6.eContents().get(2);
		private final RuleCall cRoleBindingsRoleBindingConstraintParserRuleCall_6_2_0 = (RuleCall)cRoleBindingsAssignment_6_2.eContents().get(0);
		private final Keyword cRightSquareBracketKeyword_6_3 = (Keyword)cGroup_6.eContents().get(3);
		private final Assignment cOwnedInteractionAssignment_7 = (Assignment)cGroup.eContents().get(7);
		private final RuleCall cOwnedInteractionInteractionParserRuleCall_7_0 = (RuleCall)cOwnedInteractionAssignment_7.eContents().get(0);
		
		//// Scenario
		// // ------------------------------
		// Scenario:
		//	singular?='singular'? kind=ScenarioKind 'scenario' name=ID (optimizeCost?='optimize' 'cost' | 'cost' '[' cost=DOUBLE
		//	']')? ('context' contexts+=[EClass] (',' contexts+=[EClass])*)? ('bindings' '[' roleBindings+=RoleBindingConstraint*
		//	']')? ownedInteraction=Interaction;
		@Override public ParserRule getRule() { return rule; }

		//singular?='singular'? kind=ScenarioKind 'scenario' name=ID (optimizeCost?='optimize' 'cost' | 'cost' '[' cost=DOUBLE
		//']')? ('context' contexts+=[EClass] (',' contexts+=[EClass])*)? ('bindings' '[' roleBindings+=RoleBindingConstraint*
		//']')? ownedInteraction=Interaction
		public Group getGroup() { return cGroup; }

		//singular?='singular'?
		public Assignment getSingularAssignment_0() { return cSingularAssignment_0; }

		//'singular'
		public Keyword getSingularSingularKeyword_0_0() { return cSingularSingularKeyword_0_0; }

		//kind=ScenarioKind
		public Assignment getKindAssignment_1() { return cKindAssignment_1; }

		//ScenarioKind
		public RuleCall getKindScenarioKindEnumRuleCall_1_0() { return cKindScenarioKindEnumRuleCall_1_0; }

		//'scenario'
		public Keyword getScenarioKeyword_2() { return cScenarioKeyword_2; }

		//name=ID
		public Assignment getNameAssignment_3() { return cNameAssignment_3; }

		//ID
		public RuleCall getNameIDTerminalRuleCall_3_0() { return cNameIDTerminalRuleCall_3_0; }

		//(optimizeCost?='optimize' 'cost' | 'cost' '[' cost=DOUBLE ']')?
		public Alternatives getAlternatives_4() { return cAlternatives_4; }

		//optimizeCost?='optimize' 'cost'
		public Group getGroup_4_0() { return cGroup_4_0; }

		//optimizeCost?='optimize'
		public Assignment getOptimizeCostAssignment_4_0_0() { return cOptimizeCostAssignment_4_0_0; }

		//'optimize'
		public Keyword getOptimizeCostOptimizeKeyword_4_0_0_0() { return cOptimizeCostOptimizeKeyword_4_0_0_0; }

		//'cost'
		public Keyword getCostKeyword_4_0_1() { return cCostKeyword_4_0_1; }

		//'cost' '[' cost=DOUBLE ']'
		public Group getGroup_4_1() { return cGroup_4_1; }

		//'cost'
		public Keyword getCostKeyword_4_1_0() { return cCostKeyword_4_1_0; }

		//'['
		public Keyword getLeftSquareBracketKeyword_4_1_1() { return cLeftSquareBracketKeyword_4_1_1; }

		//cost=DOUBLE
		public Assignment getCostAssignment_4_1_2() { return cCostAssignment_4_1_2; }

		//DOUBLE
		public RuleCall getCostDOUBLETerminalRuleCall_4_1_2_0() { return cCostDOUBLETerminalRuleCall_4_1_2_0; }

		//']'
		public Keyword getRightSquareBracketKeyword_4_1_3() { return cRightSquareBracketKeyword_4_1_3; }

		//('context' contexts+=[EClass] (',' contexts+=[EClass])*)?
		public Group getGroup_5() { return cGroup_5; }

		//'context'
		public Keyword getContextKeyword_5_0() { return cContextKeyword_5_0; }

		//contexts+=[EClass]
		public Assignment getContextsAssignment_5_1() { return cContextsAssignment_5_1; }

		//[EClass]
		public CrossReference getContextsEClassCrossReference_5_1_0() { return cContextsEClassCrossReference_5_1_0; }

		//ID
		public RuleCall getContextsEClassIDTerminalRuleCall_5_1_0_1() { return cContextsEClassIDTerminalRuleCall_5_1_0_1; }

		//(',' contexts+=[EClass])*
		public Group getGroup_5_2() { return cGroup_5_2; }

		//','
		public Keyword getCommaKeyword_5_2_0() { return cCommaKeyword_5_2_0; }

		//contexts+=[EClass]
		public Assignment getContextsAssignment_5_2_1() { return cContextsAssignment_5_2_1; }

		//[EClass]
		public CrossReference getContextsEClassCrossReference_5_2_1_0() { return cContextsEClassCrossReference_5_2_1_0; }

		//ID
		public RuleCall getContextsEClassIDTerminalRuleCall_5_2_1_0_1() { return cContextsEClassIDTerminalRuleCall_5_2_1_0_1; }

		//('bindings' '[' roleBindings+=RoleBindingConstraint* ']')?
		public Group getGroup_6() { return cGroup_6; }

		//'bindings'
		public Keyword getBindingsKeyword_6_0() { return cBindingsKeyword_6_0; }

		//'['
		public Keyword getLeftSquareBracketKeyword_6_1() { return cLeftSquareBracketKeyword_6_1; }

		//roleBindings+=RoleBindingConstraint*
		public Assignment getRoleBindingsAssignment_6_2() { return cRoleBindingsAssignment_6_2; }

		//RoleBindingConstraint
		public RuleCall getRoleBindingsRoleBindingConstraintParserRuleCall_6_2_0() { return cRoleBindingsRoleBindingConstraintParserRuleCall_6_2_0; }

		//']'
		public Keyword getRightSquareBracketKeyword_6_3() { return cRightSquareBracketKeyword_6_3; }

		//ownedInteraction=Interaction
		public Assignment getOwnedInteractionAssignment_7() { return cOwnedInteractionAssignment_7; }

		//Interaction
		public RuleCall getOwnedInteractionInteractionParserRuleCall_7_0() { return cOwnedInteractionInteractionParserRuleCall_7_0; }
	}

	public class RoleBindingConstraintElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.RoleBindingConstraint");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cRoleAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final CrossReference cRoleRoleCrossReference_0_0 = (CrossReference)cRoleAssignment_0.eContents().get(0);
		private final RuleCall cRoleRoleIDTerminalRuleCall_0_0_1 = (RuleCall)cRoleRoleCrossReference_0_0.eContents().get(1);
		private final Keyword cEqualsSignKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cBindingExpressionAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cBindingExpressionBindingExpressionParserRuleCall_2_0 = (RuleCall)cBindingExpressionAssignment_2.eContents().get(0);
		
		//RoleBindingConstraint:
		//	role=[Role] '=' bindingExpression=BindingExpression;
		@Override public ParserRule getRule() { return rule; }

		//role=[Role] '=' bindingExpression=BindingExpression
		public Group getGroup() { return cGroup; }

		//role=[Role]
		public Assignment getRoleAssignment_0() { return cRoleAssignment_0; }

		//[Role]
		public CrossReference getRoleRoleCrossReference_0_0() { return cRoleRoleCrossReference_0_0; }

		//ID
		public RuleCall getRoleRoleIDTerminalRuleCall_0_0_1() { return cRoleRoleIDTerminalRuleCall_0_0_1; }

		//'='
		public Keyword getEqualsSignKeyword_1() { return cEqualsSignKeyword_1; }

		//bindingExpression=BindingExpression
		public Assignment getBindingExpressionAssignment_2() { return cBindingExpressionAssignment_2; }

		//BindingExpression
		public RuleCall getBindingExpressionBindingExpressionParserRuleCall_2_0() { return cBindingExpressionBindingExpressionParserRuleCall_2_0; }
	}

	public class BindingExpressionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.BindingExpression");
		private final RuleCall cFeatureAccessBindingExpressionParserRuleCall = (RuleCall)rule.eContents().get(1);
		
		//// Binding-Expressions
		// // ------------------------------
		// BindingExpression:
		//	FeatureAccessBindingExpression;
		@Override public ParserRule getRule() { return rule; }

		//FeatureAccessBindingExpression
		public RuleCall getFeatureAccessBindingExpressionParserRuleCall() { return cFeatureAccessBindingExpressionParserRuleCall; }
	}

	public class FeatureAccessBindingExpressionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.FeatureAccessBindingExpression");
		private final Assignment cFeatureaccessAssignment = (Assignment)rule.eContents().get(1);
		private final RuleCall cFeatureaccessFeatureAccessParserRuleCall_0 = (RuleCall)cFeatureaccessAssignment.eContents().get(0);
		
		//FeatureAccessBindingExpression:
		//	featureaccess=FeatureAccess;
		@Override public ParserRule getRule() { return rule; }

		//featureaccess=FeatureAccess
		public Assignment getFeatureaccessAssignment() { return cFeatureaccessAssignment; }

		//FeatureAccess
		public RuleCall getFeatureaccessFeatureAccessParserRuleCall_0() { return cFeatureaccessFeatureAccessParserRuleCall_0; }
	}

	public class InteractionFragmentElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.InteractionFragment");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cInteractionParserRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final RuleCall cModalMessageParserRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		private final RuleCall cAlternativeParserRuleCall_2 = (RuleCall)cAlternatives.eContents().get(2);
		private final RuleCall cLoopParserRuleCall_3 = (RuleCall)cAlternatives.eContents().get(3);
		private final RuleCall cParallelParserRuleCall_4 = (RuleCall)cAlternatives.eContents().get(4);
		private final RuleCall cConditionFragmentParserRuleCall_5 = (RuleCall)cAlternatives.eContents().get(5);
		private final RuleCall cTimedConditionFragmentParserRuleCall_6 = (RuleCall)cAlternatives.eContents().get(6);
		private final RuleCall cVariableFragmentParserRuleCall_7 = (RuleCall)cAlternatives.eContents().get(7);
		
		//// Interaction
		// // ------------------------------
		// InteractionFragment:
		//	Interaction | ModalMessage | Alternative | Loop | Parallel | ConditionFragment | TimedConditionFragment |
		//	VariableFragment;
		@Override public ParserRule getRule() { return rule; }

		//Interaction | ModalMessage | Alternative | Loop | Parallel | ConditionFragment | TimedConditionFragment |
		//VariableFragment
		public Alternatives getAlternatives() { return cAlternatives; }

		//Interaction
		public RuleCall getInteractionParserRuleCall_0() { return cInteractionParserRuleCall_0; }

		//ModalMessage
		public RuleCall getModalMessageParserRuleCall_1() { return cModalMessageParserRuleCall_1; }

		//Alternative
		public RuleCall getAlternativeParserRuleCall_2() { return cAlternativeParserRuleCall_2; }

		//Loop
		public RuleCall getLoopParserRuleCall_3() { return cLoopParserRuleCall_3; }

		//Parallel
		public RuleCall getParallelParserRuleCall_4() { return cParallelParserRuleCall_4; }

		//ConditionFragment
		public RuleCall getConditionFragmentParserRuleCall_5() { return cConditionFragmentParserRuleCall_5; }

		//TimedConditionFragment
		public RuleCall getTimedConditionFragmentParserRuleCall_6() { return cTimedConditionFragmentParserRuleCall_6; }

		//VariableFragment
		public RuleCall getVariableFragmentParserRuleCall_7() { return cVariableFragmentParserRuleCall_7; }
	}

	public class VariableFragmentElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.VariableFragment");
		private final Assignment cExpressionAssignment = (Assignment)rule.eContents().get(1);
		private final Alternatives cExpressionAlternatives_0 = (Alternatives)cExpressionAssignment.eContents().get(0);
		private final RuleCall cExpressionTypedVariableDeclarationParserRuleCall_0_0 = (RuleCall)cExpressionAlternatives_0.eContents().get(0);
		private final RuleCall cExpressionVariableAssignmentParserRuleCall_0_1 = (RuleCall)cExpressionAlternatives_0.eContents().get(1);
		private final RuleCall cExpressionClockDeclarationParserRuleCall_0_2 = (RuleCall)cExpressionAlternatives_0.eContents().get(2);
		private final RuleCall cExpressionClockAssignmentParserRuleCall_0_3 = (RuleCall)cExpressionAlternatives_0.eContents().get(3);
		
		//VariableFragment:
		//	expression=(TypedVariableDeclaration | VariableAssignment | ClockDeclaration | ClockAssignment);
		@Override public ParserRule getRule() { return rule; }

		//expression=(TypedVariableDeclaration | VariableAssignment | ClockDeclaration | ClockAssignment)
		public Assignment getExpressionAssignment() { return cExpressionAssignment; }

		//(TypedVariableDeclaration | VariableAssignment | ClockDeclaration | ClockAssignment)
		public Alternatives getExpressionAlternatives_0() { return cExpressionAlternatives_0; }

		//TypedVariableDeclaration
		public RuleCall getExpressionTypedVariableDeclarationParserRuleCall_0_0() { return cExpressionTypedVariableDeclarationParserRuleCall_0_0; }

		//VariableAssignment
		public RuleCall getExpressionVariableAssignmentParserRuleCall_0_1() { return cExpressionVariableAssignmentParserRuleCall_0_1; }

		//ClockDeclaration
		public RuleCall getExpressionClockDeclarationParserRuleCall_0_2() { return cExpressionClockDeclarationParserRuleCall_0_2; }

		//ClockAssignment
		public RuleCall getExpressionClockAssignmentParserRuleCall_0_3() { return cExpressionClockAssignmentParserRuleCall_0_3; }
	}

	public class InteractionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.Interaction");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cInteractionAction_0 = (Action)cGroup.eContents().get(0);
		private final Keyword cLeftCurlyBracketKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cFragmentsAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cFragmentsInteractionFragmentParserRuleCall_2_0 = (RuleCall)cFragmentsAssignment_2.eContents().get(0);
		private final Keyword cRightCurlyBracketKeyword_3 = (Keyword)cGroup.eContents().get(3);
		private final Assignment cConstraintsAssignment_4 = (Assignment)cGroup.eContents().get(4);
		private final RuleCall cConstraintsConstraintBlockParserRuleCall_4_0 = (RuleCall)cConstraintsAssignment_4.eContents().get(0);
		
		//Interaction:
		//	{Interaction} '{' fragments+=InteractionFragment* '}' constraints=ConstraintBlock?;
		@Override public ParserRule getRule() { return rule; }

		//{Interaction} '{' fragments+=InteractionFragment* '}' constraints=ConstraintBlock?
		public Group getGroup() { return cGroup; }

		//{Interaction}
		public Action getInteractionAction_0() { return cInteractionAction_0; }

		//'{'
		public Keyword getLeftCurlyBracketKeyword_1() { return cLeftCurlyBracketKeyword_1; }

		//fragments+=InteractionFragment*
		public Assignment getFragmentsAssignment_2() { return cFragmentsAssignment_2; }

		//InteractionFragment
		public RuleCall getFragmentsInteractionFragmentParserRuleCall_2_0() { return cFragmentsInteractionFragmentParserRuleCall_2_0; }

		//'}'
		public Keyword getRightCurlyBracketKeyword_3() { return cRightCurlyBracketKeyword_3; }

		//constraints=ConstraintBlock?
		public Assignment getConstraintsAssignment_4() { return cConstraintsAssignment_4; }

		//ConstraintBlock
		public RuleCall getConstraintsConstraintBlockParserRuleCall_4_0() { return cConstraintsConstraintBlockParserRuleCall_4_0; }
	}

	public class ModalMessageElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.ModalMessage");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cStrictAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final Keyword cStrictStrictKeyword_0_0 = (Keyword)cStrictAssignment_0.eContents().get(0);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Assignment cMonitoredAssignment_1_0 = (Assignment)cGroup_1.eContents().get(0);
		private final Keyword cMonitoredMonitoredKeyword_1_0_0 = (Keyword)cMonitoredAssignment_1_0.eContents().get(0);
		private final Assignment cExpectationKindAssignment_1_1 = (Assignment)cGroup_1.eContents().get(1);
		private final RuleCall cExpectationKindExpectationKindEnumRuleCall_1_1_0 = (RuleCall)cExpectationKindAssignment_1_1.eContents().get(0);
		private final Assignment cSenderAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final CrossReference cSenderRoleCrossReference_2_0 = (CrossReference)cSenderAssignment_2.eContents().get(0);
		private final RuleCall cSenderRoleIDTerminalRuleCall_2_0_1 = (RuleCall)cSenderRoleCrossReference_2_0.eContents().get(1);
		private final Keyword cHyphenMinusGreaterThanSignKeyword_3 = (Keyword)cGroup.eContents().get(3);
		private final Assignment cReceiverAssignment_4 = (Assignment)cGroup.eContents().get(4);
		private final CrossReference cReceiverRoleCrossReference_4_0 = (CrossReference)cReceiverAssignment_4.eContents().get(0);
		private final RuleCall cReceiverRoleIDTerminalRuleCall_4_0_1 = (RuleCall)cReceiverRoleCrossReference_4_0.eContents().get(1);
		private final Keyword cFullStopKeyword_5 = (Keyword)cGroup.eContents().get(5);
		private final Assignment cModelElementAssignment_6 = (Assignment)cGroup.eContents().get(6);
		private final CrossReference cModelElementETypedElementCrossReference_6_0 = (CrossReference)cModelElementAssignment_6.eContents().get(0);
		private final RuleCall cModelElementETypedElementIDTerminalRuleCall_6_0_1 = (RuleCall)cModelElementETypedElementCrossReference_6_0.eContents().get(1);
		private final Group cGroup_7 = (Group)cGroup.eContents().get(7);
		private final Keyword cFullStopKeyword_7_0 = (Keyword)cGroup_7.eContents().get(0);
		private final Assignment cCollectionModificationAssignment_7_1 = (Assignment)cGroup_7.eContents().get(1);
		private final RuleCall cCollectionModificationCollectionModificationEnumRuleCall_7_1_0 = (RuleCall)cCollectionModificationAssignment_7_1.eContents().get(0);
		private final Group cGroup_8 = (Group)cGroup.eContents().get(8);
		private final Keyword cLeftParenthesisKeyword_8_0 = (Keyword)cGroup_8.eContents().get(0);
		private final Group cGroup_8_1 = (Group)cGroup_8.eContents().get(1);
		private final Assignment cParametersAssignment_8_1_0 = (Assignment)cGroup_8_1.eContents().get(0);
		private final RuleCall cParametersParameterBindingParserRuleCall_8_1_0_0 = (RuleCall)cParametersAssignment_8_1_0.eContents().get(0);
		private final Group cGroup_8_1_1 = (Group)cGroup_8_1.eContents().get(1);
		private final Keyword cCommaKeyword_8_1_1_0 = (Keyword)cGroup_8_1_1.eContents().get(0);
		private final Assignment cParametersAssignment_8_1_1_1 = (Assignment)cGroup_8_1_1.eContents().get(1);
		private final RuleCall cParametersParameterBindingParserRuleCall_8_1_1_1_0 = (RuleCall)cParametersAssignment_8_1_1_1.eContents().get(0);
		private final Keyword cRightParenthesisKeyword_8_2 = (Keyword)cGroup_8.eContents().get(2);
		
		//ModalMessage:
		//	strict?='strict'? (monitored?='monitored'? expectationKind=ExpectationKind)? sender=[Role] '->' receiver=[Role] '.'
		//	modelElement=[ETypedElement] ('.' collectionModification=CollectionModification)? ('(' (parameters+=ParameterBinding
		//	(',' parameters+=ParameterBinding)*)? ')')?;
		@Override public ParserRule getRule() { return rule; }

		//strict?='strict'? (monitored?='monitored'? expectationKind=ExpectationKind)? sender=[Role] '->' receiver=[Role] '.'
		//modelElement=[ETypedElement] ('.' collectionModification=CollectionModification)? ('(' (parameters+=ParameterBinding
		//(',' parameters+=ParameterBinding)*)? ')')?
		public Group getGroup() { return cGroup; }

		//strict?='strict'?
		public Assignment getStrictAssignment_0() { return cStrictAssignment_0; }

		//'strict'
		public Keyword getStrictStrictKeyword_0_0() { return cStrictStrictKeyword_0_0; }

		//(monitored?='monitored'? expectationKind=ExpectationKind)?
		public Group getGroup_1() { return cGroup_1; }

		//monitored?='monitored'?
		public Assignment getMonitoredAssignment_1_0() { return cMonitoredAssignment_1_0; }

		//'monitored'
		public Keyword getMonitoredMonitoredKeyword_1_0_0() { return cMonitoredMonitoredKeyword_1_0_0; }

		//expectationKind=ExpectationKind
		public Assignment getExpectationKindAssignment_1_1() { return cExpectationKindAssignment_1_1; }

		//ExpectationKind
		public RuleCall getExpectationKindExpectationKindEnumRuleCall_1_1_0() { return cExpectationKindExpectationKindEnumRuleCall_1_1_0; }

		//sender=[Role]
		public Assignment getSenderAssignment_2() { return cSenderAssignment_2; }

		//[Role]
		public CrossReference getSenderRoleCrossReference_2_0() { return cSenderRoleCrossReference_2_0; }

		//ID
		public RuleCall getSenderRoleIDTerminalRuleCall_2_0_1() { return cSenderRoleIDTerminalRuleCall_2_0_1; }

		//'->'
		public Keyword getHyphenMinusGreaterThanSignKeyword_3() { return cHyphenMinusGreaterThanSignKeyword_3; }

		//receiver=[Role]
		public Assignment getReceiverAssignment_4() { return cReceiverAssignment_4; }

		//[Role]
		public CrossReference getReceiverRoleCrossReference_4_0() { return cReceiverRoleCrossReference_4_0; }

		//ID
		public RuleCall getReceiverRoleIDTerminalRuleCall_4_0_1() { return cReceiverRoleIDTerminalRuleCall_4_0_1; }

		//'.'
		public Keyword getFullStopKeyword_5() { return cFullStopKeyword_5; }

		//modelElement=[ETypedElement]
		public Assignment getModelElementAssignment_6() { return cModelElementAssignment_6; }

		//[ETypedElement]
		public CrossReference getModelElementETypedElementCrossReference_6_0() { return cModelElementETypedElementCrossReference_6_0; }

		//ID
		public RuleCall getModelElementETypedElementIDTerminalRuleCall_6_0_1() { return cModelElementETypedElementIDTerminalRuleCall_6_0_1; }

		//('.' collectionModification=CollectionModification)?
		public Group getGroup_7() { return cGroup_7; }

		//'.'
		public Keyword getFullStopKeyword_7_0() { return cFullStopKeyword_7_0; }

		//collectionModification=CollectionModification
		public Assignment getCollectionModificationAssignment_7_1() { return cCollectionModificationAssignment_7_1; }

		//CollectionModification
		public RuleCall getCollectionModificationCollectionModificationEnumRuleCall_7_1_0() { return cCollectionModificationCollectionModificationEnumRuleCall_7_1_0; }

		//('(' (parameters+=ParameterBinding (',' parameters+=ParameterBinding)*)? ')')?
		public Group getGroup_8() { return cGroup_8; }

		//'('
		public Keyword getLeftParenthesisKeyword_8_0() { return cLeftParenthesisKeyword_8_0; }

		//(parameters+=ParameterBinding (',' parameters+=ParameterBinding)*)?
		public Group getGroup_8_1() { return cGroup_8_1; }

		//parameters+=ParameterBinding
		public Assignment getParametersAssignment_8_1_0() { return cParametersAssignment_8_1_0; }

		//ParameterBinding
		public RuleCall getParametersParameterBindingParserRuleCall_8_1_0_0() { return cParametersParameterBindingParserRuleCall_8_1_0_0; }

		//(',' parameters+=ParameterBinding)*
		public Group getGroup_8_1_1() { return cGroup_8_1_1; }

		//','
		public Keyword getCommaKeyword_8_1_1_0() { return cCommaKeyword_8_1_1_0; }

		//parameters+=ParameterBinding
		public Assignment getParametersAssignment_8_1_1_1() { return cParametersAssignment_8_1_1_1; }

		//ParameterBinding
		public RuleCall getParametersParameterBindingParserRuleCall_8_1_1_1_0() { return cParametersParameterBindingParserRuleCall_8_1_1_1_0; }

		//')'
		public Keyword getRightParenthesisKeyword_8_2() { return cRightParenthesisKeyword_8_2; }
	}

	public class ParameterBindingElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
		private final Assignment cBindingExpressionAssignment = (Assignment)rule.eContents().get(1);
		private final RuleCall cBindingExpressionParameterExpressionParserRuleCall_0 = (RuleCall)cBindingExpressionAssignment.eContents().get(0);
		
		//ParameterBinding:
		//	bindingExpression=ParameterExpression;
		@Override public ParserRule getRule() { return rule; }

		//bindingExpression=ParameterExpression
		public Assignment getBindingExpressionAssignment() { return cBindingExpressionAssignment; }

		//ParameterExpression
		public RuleCall getBindingExpressionParameterExpressionParserRuleCall_0() { return cBindingExpressionParameterExpressionParserRuleCall_0; }
	}

	public class ParameterExpressionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.ParameterExpression");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cWildcardParameterExpressionParserRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final RuleCall cValueParameterExpressionParserRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		private final RuleCall cVariableBindingParameterExpressionParserRuleCall_2 = (RuleCall)cAlternatives.eContents().get(2);
		
		//ParameterExpression:
		//	WildcardParameterExpression | ValueParameterExpression | VariableBindingParameterExpression;
		@Override public ParserRule getRule() { return rule; }

		//WildcardParameterExpression | ValueParameterExpression | VariableBindingParameterExpression
		public Alternatives getAlternatives() { return cAlternatives; }

		//WildcardParameterExpression
		public RuleCall getWildcardParameterExpressionParserRuleCall_0() { return cWildcardParameterExpressionParserRuleCall_0; }

		//ValueParameterExpression
		public RuleCall getValueParameterExpressionParserRuleCall_1() { return cValueParameterExpressionParserRuleCall_1; }

		//VariableBindingParameterExpression
		public RuleCall getVariableBindingParameterExpressionParserRuleCall_2() { return cVariableBindingParameterExpressionParserRuleCall_2; }
	}

	public class WildcardParameterExpressionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.WildcardParameterExpression");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cWildcardParameterExpressionAction_0 = (Action)cGroup.eContents().get(0);
		private final Keyword cAsteriskKeyword_1 = (Keyword)cGroup.eContents().get(1);
		
		//WildcardParameterExpression:
		//	{WildcardParameterExpression} '*';
		@Override public ParserRule getRule() { return rule; }

		//{WildcardParameterExpression} '*'
		public Group getGroup() { return cGroup; }

		//{WildcardParameterExpression}
		public Action getWildcardParameterExpressionAction_0() { return cWildcardParameterExpressionAction_0; }

		//'*'
		public Keyword getAsteriskKeyword_1() { return cAsteriskKeyword_1; }
	}

	public class ValueParameterExpressionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.ValueParameterExpression");
		private final Assignment cValueAssignment = (Assignment)rule.eContents().get(1);
		private final RuleCall cValueExpressionParserRuleCall_0 = (RuleCall)cValueAssignment.eContents().get(0);
		
		//ValueParameterExpression:
		//	value=Expression;
		@Override public ParserRule getRule() { return rule; }

		//value=Expression
		public Assignment getValueAssignment() { return cValueAssignment; }

		//Expression
		public RuleCall getValueExpressionParserRuleCall_0() { return cValueExpressionParserRuleCall_0; }
	}

	public class VariableBindingParameterExpressionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.VariableBindingParameterExpression");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cBindKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cVariableAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cVariableVariableValueParserRuleCall_1_0 = (RuleCall)cVariableAssignment_1.eContents().get(0);
		
		//VariableBindingParameterExpression:
		//	'bind' variable=VariableValue;
		@Override public ParserRule getRule() { return rule; }

		//'bind' variable=VariableValue
		public Group getGroup() { return cGroup; }

		//'bind'
		public Keyword getBindKeyword_0() { return cBindKeyword_0; }

		//variable=VariableValue
		public Assignment getVariableAssignment_1() { return cVariableAssignment_1; }

		//VariableValue
		public RuleCall getVariableVariableValueParserRuleCall_1_0() { return cVariableVariableValueParserRuleCall_1_0; }
	}

	public class AlternativeElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.Alternative");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cAlternativeAction_0 = (Action)cGroup.eContents().get(0);
		private final Keyword cAlternativeKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cCasesAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cCasesCaseParserRuleCall_2_0 = (RuleCall)cCasesAssignment_2.eContents().get(0);
		private final Group cGroup_3 = (Group)cGroup.eContents().get(3);
		private final Keyword cOrKeyword_3_0 = (Keyword)cGroup_3.eContents().get(0);
		private final Assignment cCasesAssignment_3_1 = (Assignment)cGroup_3.eContents().get(1);
		private final RuleCall cCasesCaseParserRuleCall_3_1_0 = (RuleCall)cCasesAssignment_3_1.eContents().get(0);
		
		//Alternative:
		//	{Alternative} 'alternative' cases+=Case ('or' cases+=Case)*;
		@Override public ParserRule getRule() { return rule; }

		//{Alternative} 'alternative' cases+=Case ('or' cases+=Case)*
		public Group getGroup() { return cGroup; }

		//{Alternative}
		public Action getAlternativeAction_0() { return cAlternativeAction_0; }

		//'alternative'
		public Keyword getAlternativeKeyword_1() { return cAlternativeKeyword_1; }

		//cases+=Case
		public Assignment getCasesAssignment_2() { return cCasesAssignment_2; }

		//Case
		public RuleCall getCasesCaseParserRuleCall_2_0() { return cCasesCaseParserRuleCall_2_0; }

		//('or' cases+=Case)*
		public Group getGroup_3() { return cGroup_3; }

		//'or'
		public Keyword getOrKeyword_3_0() { return cOrKeyword_3_0; }

		//cases+=Case
		public Assignment getCasesAssignment_3_1() { return cCasesAssignment_3_1; }

		//Case
		public RuleCall getCasesCaseParserRuleCall_3_1_0() { return cCasesCaseParserRuleCall_3_1_0; }
	}

	public class CaseElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.Case");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cCaseAction_0 = (Action)cGroup.eContents().get(0);
		private final Assignment cCaseConditionAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cCaseConditionConditionParserRuleCall_1_0 = (RuleCall)cCaseConditionAssignment_1.eContents().get(0);
		private final Assignment cCaseInteractionAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cCaseInteractionInteractionParserRuleCall_2_0 = (RuleCall)cCaseInteractionAssignment_2.eContents().get(0);
		
		//Case:
		//	{Case} caseCondition=Condition? caseInteraction=Interaction;
		@Override public ParserRule getRule() { return rule; }

		//{Case} caseCondition=Condition? caseInteraction=Interaction
		public Group getGroup() { return cGroup; }

		//{Case}
		public Action getCaseAction_0() { return cCaseAction_0; }

		//caseCondition=Condition?
		public Assignment getCaseConditionAssignment_1() { return cCaseConditionAssignment_1; }

		//Condition
		public RuleCall getCaseConditionConditionParserRuleCall_1_0() { return cCaseConditionConditionParserRuleCall_1_0; }

		//caseInteraction=Interaction
		public Assignment getCaseInteractionAssignment_2() { return cCaseInteractionAssignment_2; }

		//Interaction
		public RuleCall getCaseInteractionInteractionParserRuleCall_2_0() { return cCaseInteractionInteractionParserRuleCall_2_0; }
	}

	public class LoopElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.Loop");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cWhileKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cLoopConditionAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cLoopConditionConditionParserRuleCall_1_0 = (RuleCall)cLoopConditionAssignment_1.eContents().get(0);
		private final Assignment cBodyInteractionAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cBodyInteractionInteractionParserRuleCall_2_0 = (RuleCall)cBodyInteractionAssignment_2.eContents().get(0);
		
		//Loop:
		//	'while' loopCondition=Condition? bodyInteraction=Interaction;
		@Override public ParserRule getRule() { return rule; }

		//'while' loopCondition=Condition? bodyInteraction=Interaction
		public Group getGroup() { return cGroup; }

		//'while'
		public Keyword getWhileKeyword_0() { return cWhileKeyword_0; }

		//loopCondition=Condition?
		public Assignment getLoopConditionAssignment_1() { return cLoopConditionAssignment_1; }

		//Condition
		public RuleCall getLoopConditionConditionParserRuleCall_1_0() { return cLoopConditionConditionParserRuleCall_1_0; }

		//bodyInteraction=Interaction
		public Assignment getBodyInteractionAssignment_2() { return cBodyInteractionAssignment_2; }

		//Interaction
		public RuleCall getBodyInteractionInteractionParserRuleCall_2_0() { return cBodyInteractionInteractionParserRuleCall_2_0; }
	}

	public class ParallelElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.Parallel");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cParallelAction_0 = (Action)cGroup.eContents().get(0);
		private final Keyword cParallelKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cParallelInteractionAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cParallelInteractionInteractionParserRuleCall_2_0 = (RuleCall)cParallelInteractionAssignment_2.eContents().get(0);
		private final Group cGroup_3 = (Group)cGroup.eContents().get(3);
		private final Keyword cAndKeyword_3_0 = (Keyword)cGroup_3.eContents().get(0);
		private final Assignment cParallelInteractionAssignment_3_1 = (Assignment)cGroup_3.eContents().get(1);
		private final RuleCall cParallelInteractionInteractionParserRuleCall_3_1_0 = (RuleCall)cParallelInteractionAssignment_3_1.eContents().get(0);
		
		//Parallel:
		//	{Parallel} 'parallel' parallelInteraction+=Interaction ('and' parallelInteraction+=Interaction)*;
		@Override public ParserRule getRule() { return rule; }

		//{Parallel} 'parallel' parallelInteraction+=Interaction ('and' parallelInteraction+=Interaction)*
		public Group getGroup() { return cGroup; }

		//{Parallel}
		public Action getParallelAction_0() { return cParallelAction_0; }

		//'parallel'
		public Keyword getParallelKeyword_1() { return cParallelKeyword_1; }

		//parallelInteraction+=Interaction
		public Assignment getParallelInteractionAssignment_2() { return cParallelInteractionAssignment_2; }

		//Interaction
		public RuleCall getParallelInteractionInteractionParserRuleCall_2_0() { return cParallelInteractionInteractionParserRuleCall_2_0; }

		//('and' parallelInteraction+=Interaction)*
		public Group getGroup_3() { return cGroup_3; }

		//'and'
		public Keyword getAndKeyword_3_0() { return cAndKeyword_3_0; }

		//parallelInteraction+=Interaction
		public Assignment getParallelInteractionAssignment_3_1() { return cParallelInteractionAssignment_3_1; }

		//Interaction
		public RuleCall getParallelInteractionInteractionParserRuleCall_3_1_0() { return cParallelInteractionInteractionParserRuleCall_3_1_0; }
	}

	public class TimedConditionFragmentElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.TimedConditionFragment");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cTimedWaitConditionParserRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final RuleCall cTimedInterruptConditionParserRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		private final RuleCall cTimedViolationConditionParserRuleCall_2 = (RuleCall)cAlternatives.eContents().get(2);
		
		//// Condition
		// // ------------------------------
		// TimedConditionFragment:
		//	TimedWaitCondition | TimedInterruptCondition | TimedViolationCondition;
		@Override public ParserRule getRule() { return rule; }

		//TimedWaitCondition | TimedInterruptCondition | TimedViolationCondition
		public Alternatives getAlternatives() { return cAlternatives; }

		//TimedWaitCondition
		public RuleCall getTimedWaitConditionParserRuleCall_0() { return cTimedWaitConditionParserRuleCall_0; }

		//TimedInterruptCondition
		public RuleCall getTimedInterruptConditionParserRuleCall_1() { return cTimedInterruptConditionParserRuleCall_1; }

		//TimedViolationCondition
		public RuleCall getTimedViolationConditionParserRuleCall_2() { return cTimedViolationConditionParserRuleCall_2; }
	}

	public class ConditionFragmentElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.ConditionFragment");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cWaitConditionParserRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final RuleCall cInterruptConditionParserRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		private final RuleCall cViolationConditionParserRuleCall_2 = (RuleCall)cAlternatives.eContents().get(2);
		
		//ConditionFragment:
		//	WaitCondition | InterruptCondition | ViolationCondition;
		@Override public ParserRule getRule() { return rule; }

		//WaitCondition | InterruptCondition | ViolationCondition
		public Alternatives getAlternatives() { return cAlternatives; }

		//WaitCondition
		public RuleCall getWaitConditionParserRuleCall_0() { return cWaitConditionParserRuleCall_0; }

		//InterruptCondition
		public RuleCall getInterruptConditionParserRuleCall_1() { return cInterruptConditionParserRuleCall_1; }

		//ViolationCondition
		public RuleCall getViolationConditionParserRuleCall_2() { return cViolationConditionParserRuleCall_2; }
	}

	public class WaitConditionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.WaitCondition");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cWaitKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cStrictAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final Keyword cStrictStrictKeyword_1_0 = (Keyword)cStrictAssignment_1.eContents().get(0);
		private final Assignment cRequestedAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final Keyword cRequestedEventuallyKeyword_2_0 = (Keyword)cRequestedAssignment_2.eContents().get(0);
		private final Keyword cLeftSquareBracketKeyword_3 = (Keyword)cGroup.eContents().get(3);
		private final Assignment cConditionExpressionAssignment_4 = (Assignment)cGroup.eContents().get(4);
		private final RuleCall cConditionExpressionConditionExpressionParserRuleCall_4_0 = (RuleCall)cConditionExpressionAssignment_4.eContents().get(0);
		private final Keyword cRightSquareBracketKeyword_5 = (Keyword)cGroup.eContents().get(5);
		
		//WaitCondition:
		//	'wait' strict?='strict'? requested?='eventually'? '[' conditionExpression=ConditionExpression ']';
		@Override public ParserRule getRule() { return rule; }

		//'wait' strict?='strict'? requested?='eventually'? '[' conditionExpression=ConditionExpression ']'
		public Group getGroup() { return cGroup; }

		//'wait'
		public Keyword getWaitKeyword_0() { return cWaitKeyword_0; }

		//strict?='strict'?
		public Assignment getStrictAssignment_1() { return cStrictAssignment_1; }

		//'strict'
		public Keyword getStrictStrictKeyword_1_0() { return cStrictStrictKeyword_1_0; }

		//requested?='eventually'?
		public Assignment getRequestedAssignment_2() { return cRequestedAssignment_2; }

		//'eventually'
		public Keyword getRequestedEventuallyKeyword_2_0() { return cRequestedEventuallyKeyword_2_0; }

		//'['
		public Keyword getLeftSquareBracketKeyword_3() { return cLeftSquareBracketKeyword_3; }

		//conditionExpression=ConditionExpression
		public Assignment getConditionExpressionAssignment_4() { return cConditionExpressionAssignment_4; }

		//ConditionExpression
		public RuleCall getConditionExpressionConditionExpressionParserRuleCall_4_0() { return cConditionExpressionConditionExpressionParserRuleCall_4_0; }

		//']'
		public Keyword getRightSquareBracketKeyword_5() { return cRightSquareBracketKeyword_5; }
	}

	public class InterruptConditionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.InterruptCondition");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cInterruptKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Keyword cLeftSquareBracketKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cConditionExpressionAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cConditionExpressionConditionExpressionParserRuleCall_2_0 = (RuleCall)cConditionExpressionAssignment_2.eContents().get(0);
		private final Keyword cRightSquareBracketKeyword_3 = (Keyword)cGroup.eContents().get(3);
		
		//InterruptCondition:
		//	'interrupt' '[' conditionExpression=ConditionExpression ']';
		@Override public ParserRule getRule() { return rule; }

		//'interrupt' '[' conditionExpression=ConditionExpression ']'
		public Group getGroup() { return cGroup; }

		//'interrupt'
		public Keyword getInterruptKeyword_0() { return cInterruptKeyword_0; }

		//'['
		public Keyword getLeftSquareBracketKeyword_1() { return cLeftSquareBracketKeyword_1; }

		//conditionExpression=ConditionExpression
		public Assignment getConditionExpressionAssignment_2() { return cConditionExpressionAssignment_2; }

		//ConditionExpression
		public RuleCall getConditionExpressionConditionExpressionParserRuleCall_2_0() { return cConditionExpressionConditionExpressionParserRuleCall_2_0; }

		//']'
		public Keyword getRightSquareBracketKeyword_3() { return cRightSquareBracketKeyword_3; }
	}

	public class ViolationConditionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.ViolationCondition");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cViolationKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Keyword cLeftSquareBracketKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cConditionExpressionAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cConditionExpressionConditionExpressionParserRuleCall_2_0 = (RuleCall)cConditionExpressionAssignment_2.eContents().get(0);
		private final Keyword cRightSquareBracketKeyword_3 = (Keyword)cGroup.eContents().get(3);
		
		//ViolationCondition:
		//	'violation' '[' conditionExpression=ConditionExpression ']';
		@Override public ParserRule getRule() { return rule; }

		//'violation' '[' conditionExpression=ConditionExpression ']'
		public Group getGroup() { return cGroup; }

		//'violation'
		public Keyword getViolationKeyword_0() { return cViolationKeyword_0; }

		//'['
		public Keyword getLeftSquareBracketKeyword_1() { return cLeftSquareBracketKeyword_1; }

		//conditionExpression=ConditionExpression
		public Assignment getConditionExpressionAssignment_2() { return cConditionExpressionAssignment_2; }

		//ConditionExpression
		public RuleCall getConditionExpressionConditionExpressionParserRuleCall_2_0() { return cConditionExpressionConditionExpressionParserRuleCall_2_0; }

		//']'
		public Keyword getRightSquareBracketKeyword_3() { return cRightSquareBracketKeyword_3; }
	}

	public class TimedWaitConditionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.TimedWaitCondition");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cTimedKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Keyword cWaitKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cStrictAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final Keyword cStrictStrictKeyword_2_0 = (Keyword)cStrictAssignment_2.eContents().get(0);
		private final Assignment cRequestedAssignment_3 = (Assignment)cGroup.eContents().get(3);
		private final Keyword cRequestedEventuallyKeyword_3_0 = (Keyword)cRequestedAssignment_3.eContents().get(0);
		private final Keyword cLeftSquareBracketKeyword_4 = (Keyword)cGroup.eContents().get(4);
		private final Assignment cTimedConditionExpressionAssignment_5 = (Assignment)cGroup.eContents().get(5);
		private final RuleCall cTimedConditionExpressionTimedExpressionParserRuleCall_5_0 = (RuleCall)cTimedConditionExpressionAssignment_5.eContents().get(0);
		private final Keyword cRightSquareBracketKeyword_6 = (Keyword)cGroup.eContents().get(6);
		
		//TimedWaitCondition:
		//	'timed' 'wait' strict?='strict'? requested?='eventually'? '[' timedConditionExpression=TimedExpression ']';
		@Override public ParserRule getRule() { return rule; }

		//'timed' 'wait' strict?='strict'? requested?='eventually'? '[' timedConditionExpression=TimedExpression ']'
		public Group getGroup() { return cGroup; }

		//'timed'
		public Keyword getTimedKeyword_0() { return cTimedKeyword_0; }

		//'wait'
		public Keyword getWaitKeyword_1() { return cWaitKeyword_1; }

		//strict?='strict'?
		public Assignment getStrictAssignment_2() { return cStrictAssignment_2; }

		//'strict'
		public Keyword getStrictStrictKeyword_2_0() { return cStrictStrictKeyword_2_0; }

		//requested?='eventually'?
		public Assignment getRequestedAssignment_3() { return cRequestedAssignment_3; }

		//'eventually'
		public Keyword getRequestedEventuallyKeyword_3_0() { return cRequestedEventuallyKeyword_3_0; }

		//'['
		public Keyword getLeftSquareBracketKeyword_4() { return cLeftSquareBracketKeyword_4; }

		//timedConditionExpression=TimedExpression
		public Assignment getTimedConditionExpressionAssignment_5() { return cTimedConditionExpressionAssignment_5; }

		//TimedExpression
		public RuleCall getTimedConditionExpressionTimedExpressionParserRuleCall_5_0() { return cTimedConditionExpressionTimedExpressionParserRuleCall_5_0; }

		//']'
		public Keyword getRightSquareBracketKeyword_6() { return cRightSquareBracketKeyword_6; }
	}

	public class TimedViolationConditionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.TimedViolationCondition");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cTimedKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Keyword cViolationKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Keyword cLeftSquareBracketKeyword_2 = (Keyword)cGroup.eContents().get(2);
		private final Assignment cTimedConditionExpressionAssignment_3 = (Assignment)cGroup.eContents().get(3);
		private final RuleCall cTimedConditionExpressionTimedExpressionParserRuleCall_3_0 = (RuleCall)cTimedConditionExpressionAssignment_3.eContents().get(0);
		private final Keyword cRightSquareBracketKeyword_4 = (Keyword)cGroup.eContents().get(4);
		
		//TimedViolationCondition:
		//	'timed' 'violation' '[' timedConditionExpression=TimedExpression ']';
		@Override public ParserRule getRule() { return rule; }

		//'timed' 'violation' '[' timedConditionExpression=TimedExpression ']'
		public Group getGroup() { return cGroup; }

		//'timed'
		public Keyword getTimedKeyword_0() { return cTimedKeyword_0; }

		//'violation'
		public Keyword getViolationKeyword_1() { return cViolationKeyword_1; }

		//'['
		public Keyword getLeftSquareBracketKeyword_2() { return cLeftSquareBracketKeyword_2; }

		//timedConditionExpression=TimedExpression
		public Assignment getTimedConditionExpressionAssignment_3() { return cTimedConditionExpressionAssignment_3; }

		//TimedExpression
		public RuleCall getTimedConditionExpressionTimedExpressionParserRuleCall_3_0() { return cTimedConditionExpressionTimedExpressionParserRuleCall_3_0; }

		//']'
		public Keyword getRightSquareBracketKeyword_4() { return cRightSquareBracketKeyword_4; }
	}

	public class TimedInterruptConditionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.TimedInterruptCondition");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cTimedKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Keyword cInterruptKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Keyword cLeftSquareBracketKeyword_2 = (Keyword)cGroup.eContents().get(2);
		private final Assignment cTimedConditionExpressionAssignment_3 = (Assignment)cGroup.eContents().get(3);
		private final RuleCall cTimedConditionExpressionTimedExpressionParserRuleCall_3_0 = (RuleCall)cTimedConditionExpressionAssignment_3.eContents().get(0);
		private final Keyword cRightSquareBracketKeyword_4 = (Keyword)cGroup.eContents().get(4);
		
		//TimedInterruptCondition:
		//	'timed' 'interrupt' '[' timedConditionExpression=TimedExpression ']';
		@Override public ParserRule getRule() { return rule; }

		//'timed' 'interrupt' '[' timedConditionExpression=TimedExpression ']'
		public Group getGroup() { return cGroup; }

		//'timed'
		public Keyword getTimedKeyword_0() { return cTimedKeyword_0; }

		//'interrupt'
		public Keyword getInterruptKeyword_1() { return cInterruptKeyword_1; }

		//'['
		public Keyword getLeftSquareBracketKeyword_2() { return cLeftSquareBracketKeyword_2; }

		//timedConditionExpression=TimedExpression
		public Assignment getTimedConditionExpressionAssignment_3() { return cTimedConditionExpressionAssignment_3; }

		//TimedExpression
		public RuleCall getTimedConditionExpressionTimedExpressionParserRuleCall_3_0() { return cTimedConditionExpressionTimedExpressionParserRuleCall_3_0; }

		//']'
		public Keyword getRightSquareBracketKeyword_4() { return cRightSquareBracketKeyword_4; }
	}

	public class ConditionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.Condition");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cLeftSquareBracketKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cConditionExpressionAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cConditionExpressionConditionExpressionParserRuleCall_1_0 = (RuleCall)cConditionExpressionAssignment_1.eContents().get(0);
		private final Keyword cRightSquareBracketKeyword_2 = (Keyword)cGroup.eContents().get(2);
		
		//Condition:
		//	'[' conditionExpression=ConditionExpression ']';
		@Override public ParserRule getRule() { return rule; }

		//'[' conditionExpression=ConditionExpression ']'
		public Group getGroup() { return cGroup; }

		//'['
		public Keyword getLeftSquareBracketKeyword_0() { return cLeftSquareBracketKeyword_0; }

		//conditionExpression=ConditionExpression
		public Assignment getConditionExpressionAssignment_1() { return cConditionExpressionAssignment_1; }

		//ConditionExpression
		public RuleCall getConditionExpressionConditionExpressionParserRuleCall_1_0() { return cConditionExpressionConditionExpressionParserRuleCall_1_0; }

		//']'
		public Keyword getRightSquareBracketKeyword_2() { return cRightSquareBracketKeyword_2; }
	}

	public class ConditionExpressionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
		private final Assignment cExpressionAssignment = (Assignment)rule.eContents().get(1);
		private final RuleCall cExpressionExpressionParserRuleCall_0 = (RuleCall)cExpressionAssignment.eContents().get(0);
		
		//ConditionExpression:
		//	expression=Expression;
		@Override public ParserRule getRule() { return rule; }

		//expression=Expression
		public Assignment getExpressionAssignment() { return cExpressionAssignment; }

		//Expression
		public RuleCall getExpressionExpressionParserRuleCall_0() { return cExpressionExpressionParserRuleCall_0; }
	}

	public class ConstraintBlockElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.ConstraintBlock");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cConstraintBlockAction_0 = (Action)cGroup.eContents().get(0);
		private final Keyword cConstraintsKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Keyword cLeftSquareBracketKeyword_2 = (Keyword)cGroup.eContents().get(2);
		private final Alternatives cAlternatives_3 = (Alternatives)cGroup.eContents().get(3);
		private final Group cGroup_3_0 = (Group)cAlternatives_3.eContents().get(0);
		private final Keyword cConsiderKeyword_3_0_0 = (Keyword)cGroup_3_0.eContents().get(0);
		private final Assignment cConsiderAssignment_3_0_1 = (Assignment)cGroup_3_0.eContents().get(1);
		private final RuleCall cConsiderConstraintMessageParserRuleCall_3_0_1_0 = (RuleCall)cConsiderAssignment_3_0_1.eContents().get(0);
		private final Group cGroup_3_1 = (Group)cAlternatives_3.eContents().get(1);
		private final Keyword cIgnoreKeyword_3_1_0 = (Keyword)cGroup_3_1.eContents().get(0);
		private final Assignment cIgnoreAssignment_3_1_1 = (Assignment)cGroup_3_1.eContents().get(1);
		private final RuleCall cIgnoreConstraintMessageParserRuleCall_3_1_1_0 = (RuleCall)cIgnoreAssignment_3_1_1.eContents().get(0);
		private final Group cGroup_3_2 = (Group)cAlternatives_3.eContents().get(2);
		private final Keyword cForbiddenKeyword_3_2_0 = (Keyword)cGroup_3_2.eContents().get(0);
		private final Assignment cForbiddenAssignment_3_2_1 = (Assignment)cGroup_3_2.eContents().get(1);
		private final RuleCall cForbiddenConstraintMessageParserRuleCall_3_2_1_0 = (RuleCall)cForbiddenAssignment_3_2_1.eContents().get(0);
		private final Group cGroup_3_3 = (Group)cAlternatives_3.eContents().get(3);
		private final Keyword cInterruptKeyword_3_3_0 = (Keyword)cGroup_3_3.eContents().get(0);
		private final Assignment cInterruptAssignment_3_3_1 = (Assignment)cGroup_3_3.eContents().get(1);
		private final RuleCall cInterruptConstraintMessageParserRuleCall_3_3_1_0 = (RuleCall)cInterruptAssignment_3_3_1.eContents().get(0);
		private final Keyword cRightSquareBracketKeyword_4 = (Keyword)cGroup.eContents().get(4);
		
		//// Constraints
		// // ------------------------------
		// ConstraintBlock:
		//	{ConstraintBlock} 'constraints' '[' ('consider' consider+=ConstraintMessage | 'ignore' ignore+=ConstraintMessage |
		//	'forbidden' forbidden+=ConstraintMessage | 'interrupt' interrupt+=ConstraintMessage)* ']';
		@Override public ParserRule getRule() { return rule; }

		//{ConstraintBlock} 'constraints' '[' ('consider' consider+=ConstraintMessage | 'ignore' ignore+=ConstraintMessage |
		//'forbidden' forbidden+=ConstraintMessage | 'interrupt' interrupt+=ConstraintMessage)* ']'
		public Group getGroup() { return cGroup; }

		//{ConstraintBlock}
		public Action getConstraintBlockAction_0() { return cConstraintBlockAction_0; }

		//'constraints'
		public Keyword getConstraintsKeyword_1() { return cConstraintsKeyword_1; }

		//'['
		public Keyword getLeftSquareBracketKeyword_2() { return cLeftSquareBracketKeyword_2; }

		//('consider' consider+=ConstraintMessage | 'ignore' ignore+=ConstraintMessage | 'forbidden' forbidden+=ConstraintMessage
		//| 'interrupt' interrupt+=ConstraintMessage)*
		public Alternatives getAlternatives_3() { return cAlternatives_3; }

		//'consider' consider+=ConstraintMessage
		public Group getGroup_3_0() { return cGroup_3_0; }

		//'consider'
		public Keyword getConsiderKeyword_3_0_0() { return cConsiderKeyword_3_0_0; }

		//consider+=ConstraintMessage
		public Assignment getConsiderAssignment_3_0_1() { return cConsiderAssignment_3_0_1; }

		//ConstraintMessage
		public RuleCall getConsiderConstraintMessageParserRuleCall_3_0_1_0() { return cConsiderConstraintMessageParserRuleCall_3_0_1_0; }

		//'ignore' ignore+=ConstraintMessage
		public Group getGroup_3_1() { return cGroup_3_1; }

		//'ignore'
		public Keyword getIgnoreKeyword_3_1_0() { return cIgnoreKeyword_3_1_0; }

		//ignore+=ConstraintMessage
		public Assignment getIgnoreAssignment_3_1_1() { return cIgnoreAssignment_3_1_1; }

		//ConstraintMessage
		public RuleCall getIgnoreConstraintMessageParserRuleCall_3_1_1_0() { return cIgnoreConstraintMessageParserRuleCall_3_1_1_0; }

		//'forbidden' forbidden+=ConstraintMessage
		public Group getGroup_3_2() { return cGroup_3_2; }

		//'forbidden'
		public Keyword getForbiddenKeyword_3_2_0() { return cForbiddenKeyword_3_2_0; }

		//forbidden+=ConstraintMessage
		public Assignment getForbiddenAssignment_3_2_1() { return cForbiddenAssignment_3_2_1; }

		//ConstraintMessage
		public RuleCall getForbiddenConstraintMessageParserRuleCall_3_2_1_0() { return cForbiddenConstraintMessageParserRuleCall_3_2_1_0; }

		//'interrupt' interrupt+=ConstraintMessage
		public Group getGroup_3_3() { return cGroup_3_3; }

		//'interrupt'
		public Keyword getInterruptKeyword_3_3_0() { return cInterruptKeyword_3_3_0; }

		//interrupt+=ConstraintMessage
		public Assignment getInterruptAssignment_3_3_1() { return cInterruptAssignment_3_3_1; }

		//ConstraintMessage
		public RuleCall getInterruptConstraintMessageParserRuleCall_3_3_1_0() { return cInterruptConstraintMessageParserRuleCall_3_3_1_0; }

		//']'
		public Keyword getRightSquareBracketKeyword_4() { return cRightSquareBracketKeyword_4; }
	}

	public class ConstraintMessageElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cSenderAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final CrossReference cSenderRoleCrossReference_0_0 = (CrossReference)cSenderAssignment_0.eContents().get(0);
		private final RuleCall cSenderRoleIDTerminalRuleCall_0_0_1 = (RuleCall)cSenderRoleCrossReference_0_0.eContents().get(1);
		private final Keyword cHyphenMinusGreaterThanSignKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cReceiverAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final CrossReference cReceiverRoleCrossReference_2_0 = (CrossReference)cReceiverAssignment_2.eContents().get(0);
		private final RuleCall cReceiverRoleIDTerminalRuleCall_2_0_1 = (RuleCall)cReceiverRoleCrossReference_2_0.eContents().get(1);
		private final Keyword cFullStopKeyword_3 = (Keyword)cGroup.eContents().get(3);
		private final Assignment cModelElementAssignment_4 = (Assignment)cGroup.eContents().get(4);
		private final CrossReference cModelElementETypedElementCrossReference_4_0 = (CrossReference)cModelElementAssignment_4.eContents().get(0);
		private final RuleCall cModelElementETypedElementIDTerminalRuleCall_4_0_1 = (RuleCall)cModelElementETypedElementCrossReference_4_0.eContents().get(1);
		private final Group cGroup_5 = (Group)cGroup.eContents().get(5);
		private final Keyword cFullStopKeyword_5_0 = (Keyword)cGroup_5.eContents().get(0);
		private final Assignment cCollectionModificationAssignment_5_1 = (Assignment)cGroup_5.eContents().get(1);
		private final RuleCall cCollectionModificationCollectionModificationEnumRuleCall_5_1_0 = (RuleCall)cCollectionModificationAssignment_5_1.eContents().get(0);
		private final Keyword cLeftParenthesisKeyword_6 = (Keyword)cGroup.eContents().get(6);
		private final Group cGroup_7 = (Group)cGroup.eContents().get(7);
		private final Assignment cParametersAssignment_7_0 = (Assignment)cGroup_7.eContents().get(0);
		private final RuleCall cParametersParameterBindingParserRuleCall_7_0_0 = (RuleCall)cParametersAssignment_7_0.eContents().get(0);
		private final Group cGroup_7_1 = (Group)cGroup_7.eContents().get(1);
		private final Keyword cCommaKeyword_7_1_0 = (Keyword)cGroup_7_1.eContents().get(0);
		private final Assignment cParametersAssignment_7_1_1 = (Assignment)cGroup_7_1.eContents().get(1);
		private final RuleCall cParametersParameterBindingParserRuleCall_7_1_1_0 = (RuleCall)cParametersAssignment_7_1_1.eContents().get(0);
		private final Keyword cRightParenthesisKeyword_8 = (Keyword)cGroup.eContents().get(8);
		
		//ConstraintMessage Message:
		//	sender=[Role] '->' receiver=[Role] '.' modelElement=[ETypedElement] ('.'
		//	collectionModification=CollectionModification)? '(' (parameters+=ParameterBinding (','
		//	parameters+=ParameterBinding)*)? ')';
		@Override public ParserRule getRule() { return rule; }

		//sender=[Role] '->' receiver=[Role] '.' modelElement=[ETypedElement] ('.' collectionModification=CollectionModification)?
		//'(' (parameters+=ParameterBinding (',' parameters+=ParameterBinding)*)? ')'
		public Group getGroup() { return cGroup; }

		//sender=[Role]
		public Assignment getSenderAssignment_0() { return cSenderAssignment_0; }

		//[Role]
		public CrossReference getSenderRoleCrossReference_0_0() { return cSenderRoleCrossReference_0_0; }

		//ID
		public RuleCall getSenderRoleIDTerminalRuleCall_0_0_1() { return cSenderRoleIDTerminalRuleCall_0_0_1; }

		//'->'
		public Keyword getHyphenMinusGreaterThanSignKeyword_1() { return cHyphenMinusGreaterThanSignKeyword_1; }

		//receiver=[Role]
		public Assignment getReceiverAssignment_2() { return cReceiverAssignment_2; }

		//[Role]
		public CrossReference getReceiverRoleCrossReference_2_0() { return cReceiverRoleCrossReference_2_0; }

		//ID
		public RuleCall getReceiverRoleIDTerminalRuleCall_2_0_1() { return cReceiverRoleIDTerminalRuleCall_2_0_1; }

		//'.'
		public Keyword getFullStopKeyword_3() { return cFullStopKeyword_3; }

		//modelElement=[ETypedElement]
		public Assignment getModelElementAssignment_4() { return cModelElementAssignment_4; }

		//[ETypedElement]
		public CrossReference getModelElementETypedElementCrossReference_4_0() { return cModelElementETypedElementCrossReference_4_0; }

		//ID
		public RuleCall getModelElementETypedElementIDTerminalRuleCall_4_0_1() { return cModelElementETypedElementIDTerminalRuleCall_4_0_1; }

		//('.' collectionModification=CollectionModification)?
		public Group getGroup_5() { return cGroup_5; }

		//'.'
		public Keyword getFullStopKeyword_5_0() { return cFullStopKeyword_5_0; }

		//collectionModification=CollectionModification
		public Assignment getCollectionModificationAssignment_5_1() { return cCollectionModificationAssignment_5_1; }

		//CollectionModification
		public RuleCall getCollectionModificationCollectionModificationEnumRuleCall_5_1_0() { return cCollectionModificationCollectionModificationEnumRuleCall_5_1_0; }

		//'('
		public Keyword getLeftParenthesisKeyword_6() { return cLeftParenthesisKeyword_6; }

		//(parameters+=ParameterBinding (',' parameters+=ParameterBinding)*)?
		public Group getGroup_7() { return cGroup_7; }

		//parameters+=ParameterBinding
		public Assignment getParametersAssignment_7_0() { return cParametersAssignment_7_0; }

		//ParameterBinding
		public RuleCall getParametersParameterBindingParserRuleCall_7_0_0() { return cParametersParameterBindingParserRuleCall_7_0_0; }

		//(',' parameters+=ParameterBinding)*
		public Group getGroup_7_1() { return cGroup_7_1; }

		//','
		public Keyword getCommaKeyword_7_1_0() { return cCommaKeyword_7_1_0; }

		//parameters+=ParameterBinding
		public Assignment getParametersAssignment_7_1_1() { return cParametersAssignment_7_1_1; }

		//ParameterBinding
		public RuleCall getParametersParameterBindingParserRuleCall_7_1_1_0() { return cParametersParameterBindingParserRuleCall_7_1_1_0; }

		//')'
		public Keyword getRightParenthesisKeyword_8() { return cRightParenthesisKeyword_8; }
	}
	
	
	public class ScenarioKindElements extends AbstractEnumRuleElementFinder {
		private final EnumRule rule = (EnumRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.ScenarioKind");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final EnumLiteralDeclaration cAssumptionEnumLiteralDeclaration_0 = (EnumLiteralDeclaration)cAlternatives.eContents().get(0);
		private final Keyword cAssumptionAssumptionKeyword_0_0 = (Keyword)cAssumptionEnumLiteralDeclaration_0.eContents().get(0);
		private final EnumLiteralDeclaration cGuaranteeEnumLiteralDeclaration_1 = (EnumLiteralDeclaration)cAlternatives.eContents().get(1);
		private final Keyword cGuaranteeGuaranteeKeyword_1_0 = (Keyword)cGuaranteeEnumLiteralDeclaration_1.eContents().get(0);
		private final EnumLiteralDeclaration cExistentialEnumLiteralDeclaration_2 = (EnumLiteralDeclaration)cAlternatives.eContents().get(2);
		private final Keyword cExistentialExistentialKeyword_2_0 = (Keyword)cExistentialEnumLiteralDeclaration_2.eContents().get(0);
		
		//enum ScenarioKind:
		//	assumption | guarantee | existential;
		public EnumRule getRule() { return rule; }

		//assumption | guarantee | existential
		public Alternatives getAlternatives() { return cAlternatives; }

		//assumption
		public EnumLiteralDeclaration getAssumptionEnumLiteralDeclaration_0() { return cAssumptionEnumLiteralDeclaration_0; }

		//'assumption'
		public Keyword getAssumptionAssumptionKeyword_0_0() { return cAssumptionAssumptionKeyword_0_0; }

		//guarantee
		public EnumLiteralDeclaration getGuaranteeEnumLiteralDeclaration_1() { return cGuaranteeEnumLiteralDeclaration_1; }

		//'guarantee'
		public Keyword getGuaranteeGuaranteeKeyword_1_0() { return cGuaranteeGuaranteeKeyword_1_0; }

		//existential
		public EnumLiteralDeclaration getExistentialEnumLiteralDeclaration_2() { return cExistentialEnumLiteralDeclaration_2; }

		//'existential'
		public Keyword getExistentialExistentialKeyword_2_0() { return cExistentialExistentialKeyword_2_0; }
	}

	public class ExpectationKindElements extends AbstractEnumRuleElementFinder {
		private final EnumRule rule = (EnumRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.collaboration.Collaboration.ExpectationKind");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final EnumLiteralDeclaration cEventuallyEnumLiteralDeclaration_0 = (EnumLiteralDeclaration)cAlternatives.eContents().get(0);
		private final Keyword cEventuallyEventuallyKeyword_0_0 = (Keyword)cEventuallyEnumLiteralDeclaration_0.eContents().get(0);
		private final EnumLiteralDeclaration cRequestedEnumLiteralDeclaration_1 = (EnumLiteralDeclaration)cAlternatives.eContents().get(1);
		private final Keyword cRequestedRequestedKeyword_1_0 = (Keyword)cRequestedEnumLiteralDeclaration_1.eContents().get(0);
		private final EnumLiteralDeclaration cUrgentEnumLiteralDeclaration_2 = (EnumLiteralDeclaration)cAlternatives.eContents().get(2);
		private final Keyword cUrgentUrgentKeyword_2_0 = (Keyword)cUrgentEnumLiteralDeclaration_2.eContents().get(0);
		private final EnumLiteralDeclaration cCommittedEnumLiteralDeclaration_3 = (EnumLiteralDeclaration)cAlternatives.eContents().get(3);
		private final Keyword cCommittedCommittedKeyword_3_0 = (Keyword)cCommittedEnumLiteralDeclaration_3.eContents().get(0);
		
		//enum ExpectationKind:
		//	eventually | requested | urgent | committed;
		public EnumRule getRule() { return rule; }

		//eventually | requested | urgent | committed
		public Alternatives getAlternatives() { return cAlternatives; }

		//eventually
		public EnumLiteralDeclaration getEventuallyEnumLiteralDeclaration_0() { return cEventuallyEnumLiteralDeclaration_0; }

		//'eventually'
		public Keyword getEventuallyEventuallyKeyword_0_0() { return cEventuallyEventuallyKeyword_0_0; }

		//requested
		public EnumLiteralDeclaration getRequestedEnumLiteralDeclaration_1() { return cRequestedEnumLiteralDeclaration_1; }

		//'requested'
		public Keyword getRequestedRequestedKeyword_1_0() { return cRequestedRequestedKeyword_1_0; }

		//urgent
		public EnumLiteralDeclaration getUrgentEnumLiteralDeclaration_2() { return cUrgentEnumLiteralDeclaration_2; }

		//'urgent'
		public Keyword getUrgentUrgentKeyword_2_0() { return cUrgentUrgentKeyword_2_0; }

		//committed
		public EnumLiteralDeclaration getCommittedEnumLiteralDeclaration_3() { return cCommittedEnumLiteralDeclaration_3; }

		//'committed'
		public Keyword getCommittedCommittedKeyword_3_0() { return cCommittedCommittedKeyword_3_0; }
	}
	
	private final CollaborationElements pCollaboration;
	private final FQNElements pFQN;
	private final RoleElements pRole;
	private final ScenarioElements pScenario;
	private final ScenarioKindElements eScenarioKind;
	private final RoleBindingConstraintElements pRoleBindingConstraint;
	private final BindingExpressionElements pBindingExpression;
	private final FeatureAccessBindingExpressionElements pFeatureAccessBindingExpression;
	private final InteractionFragmentElements pInteractionFragment;
	private final VariableFragmentElements pVariableFragment;
	private final InteractionElements pInteraction;
	private final ExpectationKindElements eExpectationKind;
	private final ModalMessageElements pModalMessage;
	private final ParameterBindingElements pParameterBinding;
	private final ParameterExpressionElements pParameterExpression;
	private final WildcardParameterExpressionElements pWildcardParameterExpression;
	private final ValueParameterExpressionElements pValueParameterExpression;
	private final VariableBindingParameterExpressionElements pVariableBindingParameterExpression;
	private final AlternativeElements pAlternative;
	private final CaseElements pCase;
	private final LoopElements pLoop;
	private final ParallelElements pParallel;
	private final TimedConditionFragmentElements pTimedConditionFragment;
	private final ConditionFragmentElements pConditionFragment;
	private final WaitConditionElements pWaitCondition;
	private final InterruptConditionElements pInterruptCondition;
	private final ViolationConditionElements pViolationCondition;
	private final TimedWaitConditionElements pTimedWaitCondition;
	private final TimedViolationConditionElements pTimedViolationCondition;
	private final TimedInterruptConditionElements pTimedInterruptCondition;
	private final ConditionElements pCondition;
	private final ConditionExpressionElements pConditionExpression;
	private final ConstraintBlockElements pConstraintBlock;
	private final ConstraintMessageElements pConstraintMessage;
	
	private final Grammar grammar;

	private final ScenarioExpressionsGrammarAccess gaScenarioExpressions;

	private final TerminalsGrammarAccess gaTerminals;

	@Inject
	public CollaborationGrammarAccess(GrammarProvider grammarProvider,
		ScenarioExpressionsGrammarAccess gaScenarioExpressions,
		TerminalsGrammarAccess gaTerminals) {
		this.grammar = internalFindGrammar(grammarProvider);
		this.gaScenarioExpressions = gaScenarioExpressions;
		this.gaTerminals = gaTerminals;
		this.pCollaboration = new CollaborationElements();
		this.pFQN = new FQNElements();
		this.pRole = new RoleElements();
		this.pScenario = new ScenarioElements();
		this.eScenarioKind = new ScenarioKindElements();
		this.pRoleBindingConstraint = new RoleBindingConstraintElements();
		this.pBindingExpression = new BindingExpressionElements();
		this.pFeatureAccessBindingExpression = new FeatureAccessBindingExpressionElements();
		this.pInteractionFragment = new InteractionFragmentElements();
		this.pVariableFragment = new VariableFragmentElements();
		this.pInteraction = new InteractionElements();
		this.eExpectationKind = new ExpectationKindElements();
		this.pModalMessage = new ModalMessageElements();
		this.pParameterBinding = new ParameterBindingElements();
		this.pParameterExpression = new ParameterExpressionElements();
		this.pWildcardParameterExpression = new WildcardParameterExpressionElements();
		this.pValueParameterExpression = new ValueParameterExpressionElements();
		this.pVariableBindingParameterExpression = new VariableBindingParameterExpressionElements();
		this.pAlternative = new AlternativeElements();
		this.pCase = new CaseElements();
		this.pLoop = new LoopElements();
		this.pParallel = new ParallelElements();
		this.pTimedConditionFragment = new TimedConditionFragmentElements();
		this.pConditionFragment = new ConditionFragmentElements();
		this.pWaitCondition = new WaitConditionElements();
		this.pInterruptCondition = new InterruptConditionElements();
		this.pViolationCondition = new ViolationConditionElements();
		this.pTimedWaitCondition = new TimedWaitConditionElements();
		this.pTimedViolationCondition = new TimedViolationConditionElements();
		this.pTimedInterruptCondition = new TimedInterruptConditionElements();
		this.pCondition = new ConditionElements();
		this.pConditionExpression = new ConditionExpressionElements();
		this.pConstraintBlock = new ConstraintBlockElements();
		this.pConstraintMessage = new ConstraintMessageElements();
	}
	
	protected Grammar internalFindGrammar(GrammarProvider grammarProvider) {
		Grammar grammar = grammarProvider.getGrammar(this);
		while (grammar != null) {
			if ("org.scenariotools.sml.collaboration.Collaboration".equals(grammar.getName())) {
				return grammar;
			}
			List<Grammar> grammars = grammar.getUsedGrammars();
			if (!grammars.isEmpty()) {
				grammar = grammars.iterator().next();
			} else {
				return null;
			}
		}
		return grammar;
	}
	
	@Override
	public Grammar getGrammar() {
		return grammar;
	}
	

	public ScenarioExpressionsGrammarAccess getScenarioExpressionsGrammarAccess() {
		return gaScenarioExpressions;
	}

	public TerminalsGrammarAccess getTerminalsGrammarAccess() {
		return gaTerminals;
	}

	
	//Collaboration:
	//	imports+=Import* ('domain' domains+=[EPackage|FQN])* ('contexts' contexts+=[EPackage|FQN])* 'collaboration' name=ID
	//	'{' roles+=Role* scenarios+=Scenario* '}';
	public CollaborationElements getCollaborationAccess() {
		return pCollaboration;
	}
	
	public ParserRule getCollaborationRule() {
		return getCollaborationAccess().getRule();
	}

	//// Terminal-Definitions
	// // ------------------------------
	// FQN:
	//	ID ('.' ID)*;
	public FQNElements getFQNAccess() {
		return pFQN;
	}
	
	public ParserRule getFQNRule() {
		return getFQNAccess().getRule();
	}

	//Role:
	//	(static?='static' | 'dynamic' multiRole?='multi'?) 'role' type=[EClass] name=ID;
	public RoleElements getRoleAccess() {
		return pRole;
	}
	
	public ParserRule getRoleRule() {
		return getRoleAccess().getRule();
	}

	//// Scenario
	// // ------------------------------
	// Scenario:
	//	singular?='singular'? kind=ScenarioKind 'scenario' name=ID (optimizeCost?='optimize' 'cost' | 'cost' '[' cost=DOUBLE
	//	']')? ('context' contexts+=[EClass] (',' contexts+=[EClass])*)? ('bindings' '[' roleBindings+=RoleBindingConstraint*
	//	']')? ownedInteraction=Interaction;
	public ScenarioElements getScenarioAccess() {
		return pScenario;
	}
	
	public ParserRule getScenarioRule() {
		return getScenarioAccess().getRule();
	}

	//enum ScenarioKind:
	//	assumption | guarantee | existential;
	public ScenarioKindElements getScenarioKindAccess() {
		return eScenarioKind;
	}
	
	public EnumRule getScenarioKindRule() {
		return getScenarioKindAccess().getRule();
	}

	//RoleBindingConstraint:
	//	role=[Role] '=' bindingExpression=BindingExpression;
	public RoleBindingConstraintElements getRoleBindingConstraintAccess() {
		return pRoleBindingConstraint;
	}
	
	public ParserRule getRoleBindingConstraintRule() {
		return getRoleBindingConstraintAccess().getRule();
	}

	//// Binding-Expressions
	// // ------------------------------
	// BindingExpression:
	//	FeatureAccessBindingExpression;
	public BindingExpressionElements getBindingExpressionAccess() {
		return pBindingExpression;
	}
	
	public ParserRule getBindingExpressionRule() {
		return getBindingExpressionAccess().getRule();
	}

	//FeatureAccessBindingExpression:
	//	featureaccess=FeatureAccess;
	public FeatureAccessBindingExpressionElements getFeatureAccessBindingExpressionAccess() {
		return pFeatureAccessBindingExpression;
	}
	
	public ParserRule getFeatureAccessBindingExpressionRule() {
		return getFeatureAccessBindingExpressionAccess().getRule();
	}

	//// Interaction
	// // ------------------------------
	// InteractionFragment:
	//	Interaction | ModalMessage | Alternative | Loop | Parallel | ConditionFragment | TimedConditionFragment |
	//	VariableFragment;
	public InteractionFragmentElements getInteractionFragmentAccess() {
		return pInteractionFragment;
	}
	
	public ParserRule getInteractionFragmentRule() {
		return getInteractionFragmentAccess().getRule();
	}

	//VariableFragment:
	//	expression=(TypedVariableDeclaration | VariableAssignment | ClockDeclaration | ClockAssignment);
	public VariableFragmentElements getVariableFragmentAccess() {
		return pVariableFragment;
	}
	
	public ParserRule getVariableFragmentRule() {
		return getVariableFragmentAccess().getRule();
	}

	//Interaction:
	//	{Interaction} '{' fragments+=InteractionFragment* '}' constraints=ConstraintBlock?;
	public InteractionElements getInteractionAccess() {
		return pInteraction;
	}
	
	public ParserRule getInteractionRule() {
		return getInteractionAccess().getRule();
	}

	//enum ExpectationKind:
	//	eventually | requested | urgent | committed;
	public ExpectationKindElements getExpectationKindAccess() {
		return eExpectationKind;
	}
	
	public EnumRule getExpectationKindRule() {
		return getExpectationKindAccess().getRule();
	}

	//ModalMessage:
	//	strict?='strict'? (monitored?='monitored'? expectationKind=ExpectationKind)? sender=[Role] '->' receiver=[Role] '.'
	//	modelElement=[ETypedElement] ('.' collectionModification=CollectionModification)? ('(' (parameters+=ParameterBinding
	//	(',' parameters+=ParameterBinding)*)? ')')?;
	public ModalMessageElements getModalMessageAccess() {
		return pModalMessage;
	}
	
	public ParserRule getModalMessageRule() {
		return getModalMessageAccess().getRule();
	}

	//ParameterBinding:
	//	bindingExpression=ParameterExpression;
	public ParameterBindingElements getParameterBindingAccess() {
		return pParameterBinding;
	}
	
	public ParserRule getParameterBindingRule() {
		return getParameterBindingAccess().getRule();
	}

	//ParameterExpression:
	//	WildcardParameterExpression | ValueParameterExpression | VariableBindingParameterExpression;
	public ParameterExpressionElements getParameterExpressionAccess() {
		return pParameterExpression;
	}
	
	public ParserRule getParameterExpressionRule() {
		return getParameterExpressionAccess().getRule();
	}

	//WildcardParameterExpression:
	//	{WildcardParameterExpression} '*';
	public WildcardParameterExpressionElements getWildcardParameterExpressionAccess() {
		return pWildcardParameterExpression;
	}
	
	public ParserRule getWildcardParameterExpressionRule() {
		return getWildcardParameterExpressionAccess().getRule();
	}

	//ValueParameterExpression:
	//	value=Expression;
	public ValueParameterExpressionElements getValueParameterExpressionAccess() {
		return pValueParameterExpression;
	}
	
	public ParserRule getValueParameterExpressionRule() {
		return getValueParameterExpressionAccess().getRule();
	}

	//VariableBindingParameterExpression:
	//	'bind' variable=VariableValue;
	public VariableBindingParameterExpressionElements getVariableBindingParameterExpressionAccess() {
		return pVariableBindingParameterExpression;
	}
	
	public ParserRule getVariableBindingParameterExpressionRule() {
		return getVariableBindingParameterExpressionAccess().getRule();
	}

	//Alternative:
	//	{Alternative} 'alternative' cases+=Case ('or' cases+=Case)*;
	public AlternativeElements getAlternativeAccess() {
		return pAlternative;
	}
	
	public ParserRule getAlternativeRule() {
		return getAlternativeAccess().getRule();
	}

	//Case:
	//	{Case} caseCondition=Condition? caseInteraction=Interaction;
	public CaseElements getCaseAccess() {
		return pCase;
	}
	
	public ParserRule getCaseRule() {
		return getCaseAccess().getRule();
	}

	//Loop:
	//	'while' loopCondition=Condition? bodyInteraction=Interaction;
	public LoopElements getLoopAccess() {
		return pLoop;
	}
	
	public ParserRule getLoopRule() {
		return getLoopAccess().getRule();
	}

	//Parallel:
	//	{Parallel} 'parallel' parallelInteraction+=Interaction ('and' parallelInteraction+=Interaction)*;
	public ParallelElements getParallelAccess() {
		return pParallel;
	}
	
	public ParserRule getParallelRule() {
		return getParallelAccess().getRule();
	}

	//// Condition
	// // ------------------------------
	// TimedConditionFragment:
	//	TimedWaitCondition | TimedInterruptCondition | TimedViolationCondition;
	public TimedConditionFragmentElements getTimedConditionFragmentAccess() {
		return pTimedConditionFragment;
	}
	
	public ParserRule getTimedConditionFragmentRule() {
		return getTimedConditionFragmentAccess().getRule();
	}

	//ConditionFragment:
	//	WaitCondition | InterruptCondition | ViolationCondition;
	public ConditionFragmentElements getConditionFragmentAccess() {
		return pConditionFragment;
	}
	
	public ParserRule getConditionFragmentRule() {
		return getConditionFragmentAccess().getRule();
	}

	//WaitCondition:
	//	'wait' strict?='strict'? requested?='eventually'? '[' conditionExpression=ConditionExpression ']';
	public WaitConditionElements getWaitConditionAccess() {
		return pWaitCondition;
	}
	
	public ParserRule getWaitConditionRule() {
		return getWaitConditionAccess().getRule();
	}

	//InterruptCondition:
	//	'interrupt' '[' conditionExpression=ConditionExpression ']';
	public InterruptConditionElements getInterruptConditionAccess() {
		return pInterruptCondition;
	}
	
	public ParserRule getInterruptConditionRule() {
		return getInterruptConditionAccess().getRule();
	}

	//ViolationCondition:
	//	'violation' '[' conditionExpression=ConditionExpression ']';
	public ViolationConditionElements getViolationConditionAccess() {
		return pViolationCondition;
	}
	
	public ParserRule getViolationConditionRule() {
		return getViolationConditionAccess().getRule();
	}

	//TimedWaitCondition:
	//	'timed' 'wait' strict?='strict'? requested?='eventually'? '[' timedConditionExpression=TimedExpression ']';
	public TimedWaitConditionElements getTimedWaitConditionAccess() {
		return pTimedWaitCondition;
	}
	
	public ParserRule getTimedWaitConditionRule() {
		return getTimedWaitConditionAccess().getRule();
	}

	//TimedViolationCondition:
	//	'timed' 'violation' '[' timedConditionExpression=TimedExpression ']';
	public TimedViolationConditionElements getTimedViolationConditionAccess() {
		return pTimedViolationCondition;
	}
	
	public ParserRule getTimedViolationConditionRule() {
		return getTimedViolationConditionAccess().getRule();
	}

	//TimedInterruptCondition:
	//	'timed' 'interrupt' '[' timedConditionExpression=TimedExpression ']';
	public TimedInterruptConditionElements getTimedInterruptConditionAccess() {
		return pTimedInterruptCondition;
	}
	
	public ParserRule getTimedInterruptConditionRule() {
		return getTimedInterruptConditionAccess().getRule();
	}

	//Condition:
	//	'[' conditionExpression=ConditionExpression ']';
	public ConditionElements getConditionAccess() {
		return pCondition;
	}
	
	public ParserRule getConditionRule() {
		return getConditionAccess().getRule();
	}

	//ConditionExpression:
	//	expression=Expression;
	public ConditionExpressionElements getConditionExpressionAccess() {
		return pConditionExpression;
	}
	
	public ParserRule getConditionExpressionRule() {
		return getConditionExpressionAccess().getRule();
	}

	//// Constraints
	// // ------------------------------
	// ConstraintBlock:
	//	{ConstraintBlock} 'constraints' '[' ('consider' consider+=ConstraintMessage | 'ignore' ignore+=ConstraintMessage |
	//	'forbidden' forbidden+=ConstraintMessage | 'interrupt' interrupt+=ConstraintMessage)* ']';
	public ConstraintBlockElements getConstraintBlockAccess() {
		return pConstraintBlock;
	}
	
	public ParserRule getConstraintBlockRule() {
		return getConstraintBlockAccess().getRule();
	}

	//ConstraintMessage Message:
	//	sender=[Role] '->' receiver=[Role] '.' modelElement=[ETypedElement] ('.'
	//	collectionModification=CollectionModification)? '(' (parameters+=ParameterBinding (','
	//	parameters+=ParameterBinding)*)? ')';
	public ConstraintMessageElements getConstraintMessageAccess() {
		return pConstraintMessage;
	}
	
	public ParserRule getConstraintMessageRule() {
		return getConstraintMessageAccess().getRule();
	}

	//Document:
	//	imports+=Import* ('domain' domains+=[EPackage])* expressions+=ExpressionRegion*;
	public ScenarioExpressionsGrammarAccess.DocumentElements getDocumentAccess() {
		return gaScenarioExpressions.getDocumentAccess();
	}
	
	public ParserRule getDocumentRule() {
		return getDocumentAccess().getRule();
	}

	//Import:
	//	'import' importURI=STRING;
	public ScenarioExpressionsGrammarAccess.ImportElements getImportAccess() {
		return gaScenarioExpressions.getImportAccess();
	}
	
	public ParserRule getImportRule() {
		return getImportAccess().getRule();
	}

	//ExpressionRegion:
	//	{ExpressionRegion} '{' (expressions+=ExpressionOrRegion ';')* '}';
	public ScenarioExpressionsGrammarAccess.ExpressionRegionElements getExpressionRegionAccess() {
		return gaScenarioExpressions.getExpressionRegionAccess();
	}
	
	public ParserRule getExpressionRegionRule() {
		return getExpressionRegionAccess().getRule();
	}

	//ExpressionOrRegion:
	//	ExpressionRegion | ExpressionAndVariables;
	public ScenarioExpressionsGrammarAccess.ExpressionOrRegionElements getExpressionOrRegionAccess() {
		return gaScenarioExpressions.getExpressionOrRegionAccess();
	}
	
	public ParserRule getExpressionOrRegionRule() {
		return getExpressionOrRegionAccess().getRule();
	}

	//ExpressionAndVariables:
	//	VariableExpression | Expression;
	public ScenarioExpressionsGrammarAccess.ExpressionAndVariablesElements getExpressionAndVariablesAccess() {
		return gaScenarioExpressions.getExpressionAndVariablesAccess();
	}
	
	public ParserRule getExpressionAndVariablesRule() {
		return getExpressionAndVariablesAccess().getRule();
	}

	//VariableExpression:
	//	TypedVariableDeclaration | VariableAssignment | ClockDeclaration | ClockAssignment;
	public ScenarioExpressionsGrammarAccess.VariableExpressionElements getVariableExpressionAccess() {
		return gaScenarioExpressions.getVariableExpressionAccess();
	}
	
	public ParserRule getVariableExpressionRule() {
		return getVariableExpressionAccess().getRule();
	}

	//VariableDeclaration:
	//	'var' name=ID '=' expression=Expression;
	public ScenarioExpressionsGrammarAccess.VariableDeclarationElements getVariableDeclarationAccess() {
		return gaScenarioExpressions.getVariableDeclarationAccess();
	}
	
	public ParserRule getVariableDeclarationRule() {
		return getVariableDeclarationAccess().getRule();
	}

	//VariableAssignment:
	//	variable=[VariableDeclaration] '=' expression=Expression;
	public ScenarioExpressionsGrammarAccess.VariableAssignmentElements getVariableAssignmentAccess() {
		return gaScenarioExpressions.getVariableAssignmentAccess();
	}
	
	public ParserRule getVariableAssignmentRule() {
		return getVariableAssignmentAccess().getRule();
	}

	//TypedVariableDeclaration:
	//	'var' type=[EClassifier] name=ID ('=' expression=Expression)?;
	public ScenarioExpressionsGrammarAccess.TypedVariableDeclarationElements getTypedVariableDeclarationAccess() {
		return gaScenarioExpressions.getTypedVariableDeclarationAccess();
	}
	
	public ParserRule getTypedVariableDeclarationRule() {
		return getTypedVariableDeclarationAccess().getRule();
	}

	//ClockDeclaration:
	//	{ClockDeclaration} 'clock' name=ID ('=' expression=IntegerValue)?;
	public ScenarioExpressionsGrammarAccess.ClockDeclarationElements getClockDeclarationAccess() {
		return gaScenarioExpressions.getClockDeclarationAccess();
	}
	
	public ParserRule getClockDeclarationRule() {
		return getClockDeclarationAccess().getRule();
	}

	//Clock:
	//	name=ID leftIncluded=BOOL rightIncluded=BOOL leftValue=INT rightValue=INT;
	public ScenarioExpressionsGrammarAccess.ClockElements getClockAccess() {
		return gaScenarioExpressions.getClockAccess();
	}
	
	public ParserRule getClockRule() {
		return getClockAccess().getRule();
	}

	//ClockAssignment:
	//	'reset clock' variable=[ClockDeclaration] ('=' expression=IntegerValue)?;
	public ScenarioExpressionsGrammarAccess.ClockAssignmentElements getClockAssignmentAccess() {
		return gaScenarioExpressions.getClockAssignmentAccess();
	}
	
	public ParserRule getClockAssignmentRule() {
		return getClockAssignmentAccess().getRule();
	}

	//Expression:
	//	ImplicationExpression;
	public ScenarioExpressionsGrammarAccess.ExpressionElements getExpressionAccess() {
		return gaScenarioExpressions.getExpressionAccess();
	}
	
	public ParserRule getExpressionRule() {
		return getExpressionAccess().getRule();
	}

	//ImplicationExpression Expression:
	//	DisjunctionExpression ({BinaryOperationExpression.left=current} operator="=>" right=ImplicationExpression)?;
	public ScenarioExpressionsGrammarAccess.ImplicationExpressionElements getImplicationExpressionAccess() {
		return gaScenarioExpressions.getImplicationExpressionAccess();
	}
	
	public ParserRule getImplicationExpressionRule() {
		return getImplicationExpressionAccess().getRule();
	}

	//DisjunctionExpression Expression:
	//	ConjunctionExpression ({BinaryOperationExpression.left=current} operator="||" right=DisjunctionExpression)?;
	public ScenarioExpressionsGrammarAccess.DisjunctionExpressionElements getDisjunctionExpressionAccess() {
		return gaScenarioExpressions.getDisjunctionExpressionAccess();
	}
	
	public ParserRule getDisjunctionExpressionRule() {
		return getDisjunctionExpressionAccess().getRule();
	}

	//ConjunctionExpression Expression:
	//	RelationExpression ({BinaryOperationExpression.left=current} operator="&&" right=ConjunctionExpression)?;
	public ScenarioExpressionsGrammarAccess.ConjunctionExpressionElements getConjunctionExpressionAccess() {
		return gaScenarioExpressions.getConjunctionExpressionAccess();
	}
	
	public ParserRule getConjunctionExpressionRule() {
		return getConjunctionExpressionAccess().getRule();
	}

	//RelationExpression Expression:
	//	AdditionExpression ({BinaryOperationExpression.left=current} operator=('==' | '!=' | '<' | '>' | '<=' | '>=')
	//	right=RelationExpression)?;
	public ScenarioExpressionsGrammarAccess.RelationExpressionElements getRelationExpressionAccess() {
		return gaScenarioExpressions.getRelationExpressionAccess();
	}
	
	public ParserRule getRelationExpressionRule() {
		return getRelationExpressionAccess().getRule();
	}

	//TimedExpression:
	//	clock=[ClockDeclaration] operator=('==' | '<' | '>' | '<=' | '>=') value=INT;
	public ScenarioExpressionsGrammarAccess.TimedExpressionElements getTimedExpressionAccess() {
		return gaScenarioExpressions.getTimedExpressionAccess();
	}
	
	public ParserRule getTimedExpressionRule() {
		return getTimedExpressionAccess().getRule();
	}

	//AdditionExpression Expression:
	//	MultiplicationExpression ({BinaryOperationExpression.left=current} operator=('+' | '-') right=AdditionExpression)?;
	public ScenarioExpressionsGrammarAccess.AdditionExpressionElements getAdditionExpressionAccess() {
		return gaScenarioExpressions.getAdditionExpressionAccess();
	}
	
	public ParserRule getAdditionExpressionRule() {
		return getAdditionExpressionAccess().getRule();
	}

	//MultiplicationExpression Expression:
	//	NegatedExpression ({BinaryOperationExpression.left=current} operator=('*' | '/') right=MultiplicationExpression)?;
	public ScenarioExpressionsGrammarAccess.MultiplicationExpressionElements getMultiplicationExpressionAccess() {
		return gaScenarioExpressions.getMultiplicationExpressionAccess();
	}
	
	public ParserRule getMultiplicationExpressionRule() {
		return getMultiplicationExpressionAccess().getRule();
	}

	//NegatedExpression Expression:
	//	{UnaryOperationExpression} => operator=('!' | '-') operand=BasicExpression | BasicExpression;
	public ScenarioExpressionsGrammarAccess.NegatedExpressionElements getNegatedExpressionAccess() {
		return gaScenarioExpressions.getNegatedExpressionAccess();
	}
	
	public ParserRule getNegatedExpressionRule() {
		return getNegatedExpressionAccess().getRule();
	}

	//BasicExpression Expression:
	//	Value | '(' Expression ')';
	public ScenarioExpressionsGrammarAccess.BasicExpressionElements getBasicExpressionAccess() {
		return gaScenarioExpressions.getBasicExpressionAccess();
	}
	
	public ParserRule getBasicExpressionRule() {
		return getBasicExpressionAccess().getRule();
	}

	//Value:
	//	IntegerValue | BooleanValue | StringValue | EnumValue | NullValue | VariableValue | FeatureAccess;
	public ScenarioExpressionsGrammarAccess.ValueElements getValueAccess() {
		return gaScenarioExpressions.getValueAccess();
	}
	
	public ParserRule getValueRule() {
		return getValueAccess().getRule();
	}

	//IntegerValue:
	//	value=(INT | SIGNEDINT);
	public ScenarioExpressionsGrammarAccess.IntegerValueElements getIntegerValueAccess() {
		return gaScenarioExpressions.getIntegerValueAccess();
	}
	
	public ParserRule getIntegerValueRule() {
		return getIntegerValueAccess().getRule();
	}

	//BooleanValue:
	//	value=BOOL;
	public ScenarioExpressionsGrammarAccess.BooleanValueElements getBooleanValueAccess() {
		return gaScenarioExpressions.getBooleanValueAccess();
	}
	
	public ParserRule getBooleanValueRule() {
		return getBooleanValueAccess().getRule();
	}

	//StringValue:
	//	value=STRING;
	public ScenarioExpressionsGrammarAccess.StringValueElements getStringValueAccess() {
		return gaScenarioExpressions.getStringValueAccess();
	}
	
	public ParserRule getStringValueRule() {
		return getStringValueAccess().getRule();
	}

	//EnumValue:
	//	type=[EEnum] ':' value=[EEnumLiteral];
	public ScenarioExpressionsGrammarAccess.EnumValueElements getEnumValueAccess() {
		return gaScenarioExpressions.getEnumValueAccess();
	}
	
	public ParserRule getEnumValueRule() {
		return getEnumValueAccess().getRule();
	}

	//NullValue:
	//	{NullValue} 'null';
	public ScenarioExpressionsGrammarAccess.NullValueElements getNullValueAccess() {
		return gaScenarioExpressions.getNullValueAccess();
	}
	
	public ParserRule getNullValueRule() {
		return getNullValueAccess().getRule();
	}

	//VariableValue:
	//	value=[Variable];
	public ScenarioExpressionsGrammarAccess.VariableValueElements getVariableValueAccess() {
		return gaScenarioExpressions.getVariableValueAccess();
	}
	
	public ParserRule getVariableValueRule() {
		return getVariableValueAccess().getRule();
	}

	//CollectionAccess:
	//	collectionOperation=CollectionOperation '(' parameter=Expression? ')';
	public ScenarioExpressionsGrammarAccess.CollectionAccessElements getCollectionAccessAccess() {
		return gaScenarioExpressions.getCollectionAccessAccess();
	}
	
	public ParserRule getCollectionAccessRule() {
		return getCollectionAccessAccess().getRule();
	}

	//enum CollectionOperation:
	//	any | contains | containsAll | first | get | isEmpty | last | size;
	public ScenarioExpressionsGrammarAccess.CollectionOperationElements getCollectionOperationAccess() {
		return gaScenarioExpressions.getCollectionOperationAccess();
	}
	
	public EnumRule getCollectionOperationRule() {
		return getCollectionOperationAccess().getRule();
	}

	//enum CollectionModification returns CollectionOperation:
	//	add | addToFront | clear | remove;
	public ScenarioExpressionsGrammarAccess.CollectionModificationElements getCollectionModificationAccess() {
		return gaScenarioExpressions.getCollectionModificationAccess();
	}
	
	public EnumRule getCollectionModificationRule() {
		return getCollectionModificationAccess().getRule();
	}

	//FeatureAccess:
	//	target=[EObject] '.' (value=StructuralFeatureValue ('.' collectionAccess=CollectionAccess)? | value=OperationValue
	//	'(' (parameters+=Expression (',' parameters+=Expression)*)? ')');
	public ScenarioExpressionsGrammarAccess.FeatureAccessElements getFeatureAccessAccess() {
		return gaScenarioExpressions.getFeatureAccessAccess();
	}
	
	public ParserRule getFeatureAccessRule() {
		return getFeatureAccessAccess().getRule();
	}

	//StructuralFeatureValue:
	//	value=[EStructuralFeature];
	public ScenarioExpressionsGrammarAccess.StructuralFeatureValueElements getStructuralFeatureValueAccess() {
		return gaScenarioExpressions.getStructuralFeatureValueAccess();
	}
	
	public ParserRule getStructuralFeatureValueRule() {
		return getStructuralFeatureValueAccess().getRule();
	}

	//OperationValue:
	//	value=[EOperation];
	public ScenarioExpressionsGrammarAccess.OperationValueElements getOperationValueAccess() {
		return gaScenarioExpressions.getOperationValueAccess();
	}
	
	public ParserRule getOperationValueRule() {
		return getOperationValueAccess().getRule();
	}

	//terminal BOOL returns EBoolean:
	//	'false' | 'true';
	public TerminalRule getBOOLRule() {
		return gaScenarioExpressions.getBOOLRule();
	} 

	//terminal SIGNEDINT returns EInt:
	//	'-' INT;
	public TerminalRule getSIGNEDINTRule() {
		return gaScenarioExpressions.getSIGNEDINTRule();
	} 

	//terminal DOUBLE returns EDouble:
	//	'-'? INT? '.' INT (('e' | 'E') '-'? INT)?;
	public TerminalRule getDOUBLERule() {
		return gaScenarioExpressions.getDOUBLERule();
	} 

	//terminal ID:
	//	'^'? ('a'..'z' | 'A'..'Z' | '_') ('a'..'z' | 'A'..'Z' | '_' | '0'..'9')*;
	public TerminalRule getIDRule() {
		return gaTerminals.getIDRule();
	} 

	//terminal INT returns ecore::EInt:
	//	'0'..'9'+;
	public TerminalRule getINTRule() {
		return gaTerminals.getINTRule();
	} 

	//terminal STRING:
	//	'"' ('\\' . | !('\\' | '"'))* '"' | "'" ('\\' . | !('\\' | "'"))* "'";
	public TerminalRule getSTRINGRule() {
		return gaTerminals.getSTRINGRule();
	} 

	//terminal ML_COMMENT:
	//	'/*'->'*/';
	public TerminalRule getML_COMMENTRule() {
		return gaTerminals.getML_COMMENTRule();
	} 

	//terminal SL_COMMENT:
	//	'//' !('\n' | '\r')* ('\r'? '\n')?;
	public TerminalRule getSL_COMMENTRule() {
		return gaTerminals.getSL_COMMENTRule();
	} 

	//terminal WS:
	//	' ' | '\t' | '\r' | '\n'+;
	public TerminalRule getWSRule() {
		return gaTerminals.getWSRule();
	} 

	//terminal ANY_OTHER:
	//	.;
	public TerminalRule getANY_OTHERRule() {
		return gaTerminals.getANY_OTHERRule();
	} 
}
