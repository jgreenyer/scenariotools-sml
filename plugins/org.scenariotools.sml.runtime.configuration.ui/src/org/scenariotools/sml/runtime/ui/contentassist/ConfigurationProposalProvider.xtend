/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.ui.contentassist

import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor
import org.scenariotools.sml.runtime.ui.util.ConfigurationUtil
import org.eclipse.xtext.Assignment
import org.scenariotools.sml.runtime.scoping.ConfigurationScopeProvider
import com.google.inject.Inject
import org.scenariotools.sml.runtime.configuration.ConfigurationPackage
import org.scenariotools.sml.runtime.configuration.Configuration

/**
 * see http://www.eclipse.org/Xtext/documentation.html#contentAssist on how to customize content assistant
 */
class ConfigurationProposalProvider extends AbstractConfigurationProposalProvider {

	override completeConfiguration_ImportedResources(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {

		val uris = ConfigurationUtil::platformURIsForExtension(context.resource, "collaboration", "sml", "genmodel")
		for (uri : uris) {
			acceptor.accept(
				createCompletionProposal("import \"" + uri.toString + "\"",
					uri.segment(uri.segmentCount - 2) + "/" + uri.segment(uri.segmentCount - 1), null, context))
		}
	}

	override complete_XMIImport(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		val uris = ConfigurationUtil::platformURIsForExtension(model.eResource, "collaboration", "xmi")
		for (uri : uris) {
			acceptor.accept(
				createCompletionProposal("use instancemodel \"" + uri.toString + "\"",
					uri.segment(uri.segmentCount - 2) + "/" + uri.segment(uri.segmentCount - 1), null, context))
		}
	}

	@Inject
	ConfigurationScopeProvider provider
	/**
	 * Use this for consistency, until FQNs are available.
	 */
	def collaborationProposals(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor ){
		for (description : provider.scopeForCollaborations(model as Configuration,
			ConfigurationPackage.Literals.CONFIGURATION__AUXILIARY_COLLABORATIONS).allElements) {
			if (description.name.segmentCount < 2)
				acceptor.accept(createCompletionProposal(description.name.toString, context))
		}	
	}
	override completeConfiguration_AuxiliaryCollaborations(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		collaborationProposals(model,assignment,context,acceptor)	
	}
	
	override completeConfiguration_IgnoredCollaborations(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		collaborationProposals(model,assignment,context,acceptor)	
	}
	
	override completeRoleBindings_Collaboration(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		for(c: ((model.eContainer as Configuration).consideredCollaborations)){
			acceptor.accept(createCompletionProposal(c.name, context))
		}
	}
	
}
