/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtext.ui.resource.IResourceSetProvider;
import org.eclipse.xtext.ui.resource.XtextLiveScopeResourceSetProvider;

/**
 * Use this class to register components to be used within the IDE.
 */
public class ConfigurationUiModule extends org.scenariotools.sml.runtime.ui.AbstractConfigurationUiModule {
	public ConfigurationUiModule(AbstractUIPlugin plugin) {
		super(plugin);
	}
	@Override
	public Class<? extends IResourceSetProvider> bindIResourceSetProvider() {
		// TODO Auto-generated method stub
		return XtextLiveScopeResourceSetProvider.class;
	}
}
