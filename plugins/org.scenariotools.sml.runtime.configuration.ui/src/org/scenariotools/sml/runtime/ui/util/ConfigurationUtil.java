/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.runtime.ui.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;

public class ConfigurationUtil {
	/**
	 * Returns a list of platform/resource URIs for files that have the given
	 * extension.
	 * 
	 * @param resource
	 * @param extensions
	 * @return
	 */
	public static List<URI> platformURIsForExtension(final Resource resource, final String... extensions) {
		String projectName = resource.getURI().segment(1);
		final List<URI> result = new ArrayList<>();
		if (extensions != null) {
			IResource project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
			try {
				project.accept(new IResourceVisitor() {

					@Override
					public boolean visit(IResource resource) throws CoreException {
						for (String x : extensions) {
							if (x.equals(resource.getFileExtension())) {
								result.add(URI.createURI("platform:/resource/" + projectName + "/"
										+ resource.getProjectRelativePath().toString()));
								return true;
							}
						}
						return true;
					}

				});
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;

	}
}
