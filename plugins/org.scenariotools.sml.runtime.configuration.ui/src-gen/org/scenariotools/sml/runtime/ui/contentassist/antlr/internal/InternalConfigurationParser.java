package org.scenariotools.sml.runtime.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.scenariotools.sml.runtime.services.ConfigurationGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalConfigurationParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'rolebindings'", "'configure'", "'specification'", "'ignored'", "'collaboration'", "','", "'auxiliary'", "'collaborations'", "'.'", "'for'", "'{'", "'}'", "'role'", "'bindings'", "'object'", "'plays'", "'use'", "'instancemodel'", "'import'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalConfigurationParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalConfigurationParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalConfigurationParser.tokenNames; }
    public String getGrammarFileName() { return "InternalConfiguration.g"; }


     
     	private ConfigurationGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(ConfigurationGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleConfiguration"
    // InternalConfiguration.g:60:1: entryRuleConfiguration : ruleConfiguration EOF ;
    public final void entryRuleConfiguration() throws RecognitionException {
        try {
            // InternalConfiguration.g:61:1: ( ruleConfiguration EOF )
            // InternalConfiguration.g:62:1: ruleConfiguration EOF
            {
             before(grammarAccess.getConfigurationRule()); 
            pushFollow(FOLLOW_1);
            ruleConfiguration();

            state._fsp--;

             after(grammarAccess.getConfigurationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConfiguration"


    // $ANTLR start "ruleConfiguration"
    // InternalConfiguration.g:69:1: ruleConfiguration : ( ( rule__Configuration__Group__0 ) ) ;
    public final void ruleConfiguration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:73:2: ( ( ( rule__Configuration__Group__0 ) ) )
            // InternalConfiguration.g:74:1: ( ( rule__Configuration__Group__0 ) )
            {
            // InternalConfiguration.g:74:1: ( ( rule__Configuration__Group__0 ) )
            // InternalConfiguration.g:75:1: ( rule__Configuration__Group__0 )
            {
             before(grammarAccess.getConfigurationAccess().getGroup()); 
            // InternalConfiguration.g:76:1: ( rule__Configuration__Group__0 )
            // InternalConfiguration.g:76:2: rule__Configuration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConfiguration"


    // $ANTLR start "entryRuleFQN"
    // InternalConfiguration.g:88:1: entryRuleFQN : ruleFQN EOF ;
    public final void entryRuleFQN() throws RecognitionException {
        try {
            // InternalConfiguration.g:89:1: ( ruleFQN EOF )
            // InternalConfiguration.g:90:1: ruleFQN EOF
            {
             before(grammarAccess.getFQNRule()); 
            pushFollow(FOLLOW_1);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getFQNRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFQN"


    // $ANTLR start "ruleFQN"
    // InternalConfiguration.g:97:1: ruleFQN : ( ( rule__FQN__Group__0 ) ) ;
    public final void ruleFQN() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:101:2: ( ( ( rule__FQN__Group__0 ) ) )
            // InternalConfiguration.g:102:1: ( ( rule__FQN__Group__0 ) )
            {
            // InternalConfiguration.g:102:1: ( ( rule__FQN__Group__0 ) )
            // InternalConfiguration.g:103:1: ( rule__FQN__Group__0 )
            {
             before(grammarAccess.getFQNAccess().getGroup()); 
            // InternalConfiguration.g:104:1: ( rule__FQN__Group__0 )
            // InternalConfiguration.g:104:2: rule__FQN__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__FQN__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFQNAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFQN"


    // $ANTLR start "entryRuleRoleBindings"
    // InternalConfiguration.g:116:1: entryRuleRoleBindings : ruleRoleBindings EOF ;
    public final void entryRuleRoleBindings() throws RecognitionException {
        try {
            // InternalConfiguration.g:117:1: ( ruleRoleBindings EOF )
            // InternalConfiguration.g:118:1: ruleRoleBindings EOF
            {
             before(grammarAccess.getRoleBindingsRule()); 
            pushFollow(FOLLOW_1);
            ruleRoleBindings();

            state._fsp--;

             after(grammarAccess.getRoleBindingsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRoleBindings"


    // $ANTLR start "ruleRoleBindings"
    // InternalConfiguration.g:125:1: ruleRoleBindings : ( ( rule__RoleBindings__Group__0 ) ) ;
    public final void ruleRoleBindings() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:129:2: ( ( ( rule__RoleBindings__Group__0 ) ) )
            // InternalConfiguration.g:130:1: ( ( rule__RoleBindings__Group__0 ) )
            {
            // InternalConfiguration.g:130:1: ( ( rule__RoleBindings__Group__0 ) )
            // InternalConfiguration.g:131:1: ( rule__RoleBindings__Group__0 )
            {
             before(grammarAccess.getRoleBindingsAccess().getGroup()); 
            // InternalConfiguration.g:132:1: ( rule__RoleBindings__Group__0 )
            // InternalConfiguration.g:132:2: rule__RoleBindings__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__RoleBindings__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRoleBindingsAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRoleBindings"


    // $ANTLR start "entryRuleRoleAssignment"
    // InternalConfiguration.g:144:1: entryRuleRoleAssignment : ruleRoleAssignment EOF ;
    public final void entryRuleRoleAssignment() throws RecognitionException {
        try {
            // InternalConfiguration.g:145:1: ( ruleRoleAssignment EOF )
            // InternalConfiguration.g:146:1: ruleRoleAssignment EOF
            {
             before(grammarAccess.getRoleAssignmentRule()); 
            pushFollow(FOLLOW_1);
            ruleRoleAssignment();

            state._fsp--;

             after(grammarAccess.getRoleAssignmentRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRoleAssignment"


    // $ANTLR start "ruleRoleAssignment"
    // InternalConfiguration.g:153:1: ruleRoleAssignment : ( ( rule__RoleAssignment__Group__0 ) ) ;
    public final void ruleRoleAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:157:2: ( ( ( rule__RoleAssignment__Group__0 ) ) )
            // InternalConfiguration.g:158:1: ( ( rule__RoleAssignment__Group__0 ) )
            {
            // InternalConfiguration.g:158:1: ( ( rule__RoleAssignment__Group__0 ) )
            // InternalConfiguration.g:159:1: ( rule__RoleAssignment__Group__0 )
            {
             before(grammarAccess.getRoleAssignmentAccess().getGroup()); 
            // InternalConfiguration.g:160:1: ( rule__RoleAssignment__Group__0 )
            // InternalConfiguration.g:160:2: rule__RoleAssignment__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__RoleAssignment__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRoleAssignmentAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRoleAssignment"


    // $ANTLR start "entryRuleXMIImport"
    // InternalConfiguration.g:172:1: entryRuleXMIImport : ruleXMIImport EOF ;
    public final void entryRuleXMIImport() throws RecognitionException {
        try {
            // InternalConfiguration.g:173:1: ( ruleXMIImport EOF )
            // InternalConfiguration.g:174:1: ruleXMIImport EOF
            {
             before(grammarAccess.getXMIImportRule()); 
            pushFollow(FOLLOW_1);
            ruleXMIImport();

            state._fsp--;

             after(grammarAccess.getXMIImportRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXMIImport"


    // $ANTLR start "ruleXMIImport"
    // InternalConfiguration.g:181:1: ruleXMIImport : ( ( rule__XMIImport__Group__0 ) ) ;
    public final void ruleXMIImport() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:185:2: ( ( ( rule__XMIImport__Group__0 ) ) )
            // InternalConfiguration.g:186:1: ( ( rule__XMIImport__Group__0 ) )
            {
            // InternalConfiguration.g:186:1: ( ( rule__XMIImport__Group__0 ) )
            // InternalConfiguration.g:187:1: ( rule__XMIImport__Group__0 )
            {
             before(grammarAccess.getXMIImportAccess().getGroup()); 
            // InternalConfiguration.g:188:1: ( rule__XMIImport__Group__0 )
            // InternalConfiguration.g:188:2: rule__XMIImport__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__XMIImport__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getXMIImportAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXMIImport"


    // $ANTLR start "entryRuleSMLImport"
    // InternalConfiguration.g:200:1: entryRuleSMLImport : ruleSMLImport EOF ;
    public final void entryRuleSMLImport() throws RecognitionException {
        try {
            // InternalConfiguration.g:201:1: ( ruleSMLImport EOF )
            // InternalConfiguration.g:202:1: ruleSMLImport EOF
            {
             before(grammarAccess.getSMLImportRule()); 
            pushFollow(FOLLOW_1);
            ruleSMLImport();

            state._fsp--;

             after(grammarAccess.getSMLImportRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSMLImport"


    // $ANTLR start "ruleSMLImport"
    // InternalConfiguration.g:209:1: ruleSMLImport : ( ( rule__SMLImport__Group__0 ) ) ;
    public final void ruleSMLImport() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:213:2: ( ( ( rule__SMLImport__Group__0 ) ) )
            // InternalConfiguration.g:214:1: ( ( rule__SMLImport__Group__0 ) )
            {
            // InternalConfiguration.g:214:1: ( ( rule__SMLImport__Group__0 ) )
            // InternalConfiguration.g:215:1: ( rule__SMLImport__Group__0 )
            {
             before(grammarAccess.getSMLImportAccess().getGroup()); 
            // InternalConfiguration.g:216:1: ( rule__SMLImport__Group__0 )
            // InternalConfiguration.g:216:2: rule__SMLImport__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SMLImport__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSMLImportAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSMLImport"


    // $ANTLR start "rule__RoleBindings__Alternatives_0"
    // InternalConfiguration.g:228:1: rule__RoleBindings__Alternatives_0 : ( ( 'rolebindings' ) | ( ( rule__RoleBindings__Group_0_1__0 ) ) );
    public final void rule__RoleBindings__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:232:1: ( ( 'rolebindings' ) | ( ( rule__RoleBindings__Group_0_1__0 ) ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==11) ) {
                alt1=1;
            }
            else if ( (LA1_0==23) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalConfiguration.g:233:1: ( 'rolebindings' )
                    {
                    // InternalConfiguration.g:233:1: ( 'rolebindings' )
                    // InternalConfiguration.g:234:1: 'rolebindings'
                    {
                     before(grammarAccess.getRoleBindingsAccess().getRolebindingsKeyword_0_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getRoleBindingsAccess().getRolebindingsKeyword_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalConfiguration.g:241:6: ( ( rule__RoleBindings__Group_0_1__0 ) )
                    {
                    // InternalConfiguration.g:241:6: ( ( rule__RoleBindings__Group_0_1__0 ) )
                    // InternalConfiguration.g:242:1: ( rule__RoleBindings__Group_0_1__0 )
                    {
                     before(grammarAccess.getRoleBindingsAccess().getGroup_0_1()); 
                    // InternalConfiguration.g:243:1: ( rule__RoleBindings__Group_0_1__0 )
                    // InternalConfiguration.g:243:2: rule__RoleBindings__Group_0_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__RoleBindings__Group_0_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getRoleBindingsAccess().getGroup_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Alternatives_0"


    // $ANTLR start "rule__Configuration__Group__0"
    // InternalConfiguration.g:254:1: rule__Configuration__Group__0 : rule__Configuration__Group__0__Impl rule__Configuration__Group__1 ;
    public final void rule__Configuration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:258:1: ( rule__Configuration__Group__0__Impl rule__Configuration__Group__1 )
            // InternalConfiguration.g:259:2: rule__Configuration__Group__0__Impl rule__Configuration__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Configuration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__0"


    // $ANTLR start "rule__Configuration__Group__0__Impl"
    // InternalConfiguration.g:266:1: rule__Configuration__Group__0__Impl : ( ( ( rule__Configuration__ImportedResourcesAssignment_0 ) ) ( ( rule__Configuration__ImportedResourcesAssignment_0 )* ) ) ;
    public final void rule__Configuration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:270:1: ( ( ( ( rule__Configuration__ImportedResourcesAssignment_0 ) ) ( ( rule__Configuration__ImportedResourcesAssignment_0 )* ) ) )
            // InternalConfiguration.g:271:1: ( ( ( rule__Configuration__ImportedResourcesAssignment_0 ) ) ( ( rule__Configuration__ImportedResourcesAssignment_0 )* ) )
            {
            // InternalConfiguration.g:271:1: ( ( ( rule__Configuration__ImportedResourcesAssignment_0 ) ) ( ( rule__Configuration__ImportedResourcesAssignment_0 )* ) )
            // InternalConfiguration.g:272:1: ( ( rule__Configuration__ImportedResourcesAssignment_0 ) ) ( ( rule__Configuration__ImportedResourcesAssignment_0 )* )
            {
            // InternalConfiguration.g:272:1: ( ( rule__Configuration__ImportedResourcesAssignment_0 ) )
            // InternalConfiguration.g:273:1: ( rule__Configuration__ImportedResourcesAssignment_0 )
            {
             before(grammarAccess.getConfigurationAccess().getImportedResourcesAssignment_0()); 
            // InternalConfiguration.g:274:1: ( rule__Configuration__ImportedResourcesAssignment_0 )
            // InternalConfiguration.g:274:2: rule__Configuration__ImportedResourcesAssignment_0
            {
            pushFollow(FOLLOW_4);
            rule__Configuration__ImportedResourcesAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getImportedResourcesAssignment_0()); 

            }

            // InternalConfiguration.g:277:1: ( ( rule__Configuration__ImportedResourcesAssignment_0 )* )
            // InternalConfiguration.g:278:1: ( rule__Configuration__ImportedResourcesAssignment_0 )*
            {
             before(grammarAccess.getConfigurationAccess().getImportedResourcesAssignment_0()); 
            // InternalConfiguration.g:279:1: ( rule__Configuration__ImportedResourcesAssignment_0 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==29) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalConfiguration.g:279:2: rule__Configuration__ImportedResourcesAssignment_0
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__Configuration__ImportedResourcesAssignment_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getConfigurationAccess().getImportedResourcesAssignment_0()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__0__Impl"


    // $ANTLR start "rule__Configuration__Group__1"
    // InternalConfiguration.g:290:1: rule__Configuration__Group__1 : rule__Configuration__Group__1__Impl rule__Configuration__Group__2 ;
    public final void rule__Configuration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:294:1: ( rule__Configuration__Group__1__Impl rule__Configuration__Group__2 )
            // InternalConfiguration.g:295:2: rule__Configuration__Group__1__Impl rule__Configuration__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Configuration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__1"


    // $ANTLR start "rule__Configuration__Group__1__Impl"
    // InternalConfiguration.g:302:1: rule__Configuration__Group__1__Impl : ( 'configure' ) ;
    public final void rule__Configuration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:306:1: ( ( 'configure' ) )
            // InternalConfiguration.g:307:1: ( 'configure' )
            {
            // InternalConfiguration.g:307:1: ( 'configure' )
            // InternalConfiguration.g:308:1: 'configure'
            {
             before(grammarAccess.getConfigurationAccess().getConfigureKeyword_1()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getConfigureKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__1__Impl"


    // $ANTLR start "rule__Configuration__Group__2"
    // InternalConfiguration.g:321:1: rule__Configuration__Group__2 : rule__Configuration__Group__2__Impl rule__Configuration__Group__3 ;
    public final void rule__Configuration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:325:1: ( rule__Configuration__Group__2__Impl rule__Configuration__Group__3 )
            // InternalConfiguration.g:326:2: rule__Configuration__Group__2__Impl rule__Configuration__Group__3
            {
            pushFollow(FOLLOW_6);
            rule__Configuration__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__2"


    // $ANTLR start "rule__Configuration__Group__2__Impl"
    // InternalConfiguration.g:333:1: rule__Configuration__Group__2__Impl : ( 'specification' ) ;
    public final void rule__Configuration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:337:1: ( ( 'specification' ) )
            // InternalConfiguration.g:338:1: ( 'specification' )
            {
            // InternalConfiguration.g:338:1: ( 'specification' )
            // InternalConfiguration.g:339:1: 'specification'
            {
             before(grammarAccess.getConfigurationAccess().getSpecificationKeyword_2()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getSpecificationKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__2__Impl"


    // $ANTLR start "rule__Configuration__Group__3"
    // InternalConfiguration.g:352:1: rule__Configuration__Group__3 : rule__Configuration__Group__3__Impl rule__Configuration__Group__4 ;
    public final void rule__Configuration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:356:1: ( rule__Configuration__Group__3__Impl rule__Configuration__Group__4 )
            // InternalConfiguration.g:357:2: rule__Configuration__Group__3__Impl rule__Configuration__Group__4
            {
            pushFollow(FOLLOW_7);
            rule__Configuration__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__3"


    // $ANTLR start "rule__Configuration__Group__3__Impl"
    // InternalConfiguration.g:364:1: rule__Configuration__Group__3__Impl : ( ( rule__Configuration__SpecificationAssignment_3 ) ) ;
    public final void rule__Configuration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:368:1: ( ( ( rule__Configuration__SpecificationAssignment_3 ) ) )
            // InternalConfiguration.g:369:1: ( ( rule__Configuration__SpecificationAssignment_3 ) )
            {
            // InternalConfiguration.g:369:1: ( ( rule__Configuration__SpecificationAssignment_3 ) )
            // InternalConfiguration.g:370:1: ( rule__Configuration__SpecificationAssignment_3 )
            {
             before(grammarAccess.getConfigurationAccess().getSpecificationAssignment_3()); 
            // InternalConfiguration.g:371:1: ( rule__Configuration__SpecificationAssignment_3 )
            // InternalConfiguration.g:371:2: rule__Configuration__SpecificationAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__SpecificationAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getSpecificationAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__3__Impl"


    // $ANTLR start "rule__Configuration__Group__4"
    // InternalConfiguration.g:381:1: rule__Configuration__Group__4 : rule__Configuration__Group__4__Impl rule__Configuration__Group__5 ;
    public final void rule__Configuration__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:385:1: ( rule__Configuration__Group__4__Impl rule__Configuration__Group__5 )
            // InternalConfiguration.g:386:2: rule__Configuration__Group__4__Impl rule__Configuration__Group__5
            {
            pushFollow(FOLLOW_7);
            rule__Configuration__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__4"


    // $ANTLR start "rule__Configuration__Group__4__Impl"
    // InternalConfiguration.g:393:1: rule__Configuration__Group__4__Impl : ( ( rule__Configuration__Group_4__0 )? ) ;
    public final void rule__Configuration__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:397:1: ( ( ( rule__Configuration__Group_4__0 )? ) )
            // InternalConfiguration.g:398:1: ( ( rule__Configuration__Group_4__0 )? )
            {
            // InternalConfiguration.g:398:1: ( ( rule__Configuration__Group_4__0 )? )
            // InternalConfiguration.g:399:1: ( rule__Configuration__Group_4__0 )?
            {
             before(grammarAccess.getConfigurationAccess().getGroup_4()); 
            // InternalConfiguration.g:400:1: ( rule__Configuration__Group_4__0 )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==14) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalConfiguration.g:400:2: rule__Configuration__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Configuration__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getConfigurationAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__4__Impl"


    // $ANTLR start "rule__Configuration__Group__5"
    // InternalConfiguration.g:410:1: rule__Configuration__Group__5 : rule__Configuration__Group__5__Impl rule__Configuration__Group__6 ;
    public final void rule__Configuration__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:414:1: ( rule__Configuration__Group__5__Impl rule__Configuration__Group__6 )
            // InternalConfiguration.g:415:2: rule__Configuration__Group__5__Impl rule__Configuration__Group__6
            {
            pushFollow(FOLLOW_7);
            rule__Configuration__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__5"


    // $ANTLR start "rule__Configuration__Group__5__Impl"
    // InternalConfiguration.g:422:1: rule__Configuration__Group__5__Impl : ( ( rule__Configuration__Group_5__0 )? ) ;
    public final void rule__Configuration__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:426:1: ( ( ( rule__Configuration__Group_5__0 )? ) )
            // InternalConfiguration.g:427:1: ( ( rule__Configuration__Group_5__0 )? )
            {
            // InternalConfiguration.g:427:1: ( ( rule__Configuration__Group_5__0 )? )
            // InternalConfiguration.g:428:1: ( rule__Configuration__Group_5__0 )?
            {
             before(grammarAccess.getConfigurationAccess().getGroup_5()); 
            // InternalConfiguration.g:429:1: ( rule__Configuration__Group_5__0 )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==17) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalConfiguration.g:429:2: rule__Configuration__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Configuration__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getConfigurationAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__5__Impl"


    // $ANTLR start "rule__Configuration__Group__6"
    // InternalConfiguration.g:439:1: rule__Configuration__Group__6 : rule__Configuration__Group__6__Impl rule__Configuration__Group__7 ;
    public final void rule__Configuration__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:443:1: ( rule__Configuration__Group__6__Impl rule__Configuration__Group__7 )
            // InternalConfiguration.g:444:2: rule__Configuration__Group__6__Impl rule__Configuration__Group__7
            {
            pushFollow(FOLLOW_8);
            rule__Configuration__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__6"


    // $ANTLR start "rule__Configuration__Group__6__Impl"
    // InternalConfiguration.g:451:1: rule__Configuration__Group__6__Impl : ( ( ( rule__Configuration__InstanceModelImportsAssignment_6 ) ) ( ( rule__Configuration__InstanceModelImportsAssignment_6 )* ) ) ;
    public final void rule__Configuration__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:455:1: ( ( ( ( rule__Configuration__InstanceModelImportsAssignment_6 ) ) ( ( rule__Configuration__InstanceModelImportsAssignment_6 )* ) ) )
            // InternalConfiguration.g:456:1: ( ( ( rule__Configuration__InstanceModelImportsAssignment_6 ) ) ( ( rule__Configuration__InstanceModelImportsAssignment_6 )* ) )
            {
            // InternalConfiguration.g:456:1: ( ( ( rule__Configuration__InstanceModelImportsAssignment_6 ) ) ( ( rule__Configuration__InstanceModelImportsAssignment_6 )* ) )
            // InternalConfiguration.g:457:1: ( ( rule__Configuration__InstanceModelImportsAssignment_6 ) ) ( ( rule__Configuration__InstanceModelImportsAssignment_6 )* )
            {
            // InternalConfiguration.g:457:1: ( ( rule__Configuration__InstanceModelImportsAssignment_6 ) )
            // InternalConfiguration.g:458:1: ( rule__Configuration__InstanceModelImportsAssignment_6 )
            {
             before(grammarAccess.getConfigurationAccess().getInstanceModelImportsAssignment_6()); 
            // InternalConfiguration.g:459:1: ( rule__Configuration__InstanceModelImportsAssignment_6 )
            // InternalConfiguration.g:459:2: rule__Configuration__InstanceModelImportsAssignment_6
            {
            pushFollow(FOLLOW_9);
            rule__Configuration__InstanceModelImportsAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getInstanceModelImportsAssignment_6()); 

            }

            // InternalConfiguration.g:462:1: ( ( rule__Configuration__InstanceModelImportsAssignment_6 )* )
            // InternalConfiguration.g:463:1: ( rule__Configuration__InstanceModelImportsAssignment_6 )*
            {
             before(grammarAccess.getConfigurationAccess().getInstanceModelImportsAssignment_6()); 
            // InternalConfiguration.g:464:1: ( rule__Configuration__InstanceModelImportsAssignment_6 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==27) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalConfiguration.g:464:2: rule__Configuration__InstanceModelImportsAssignment_6
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Configuration__InstanceModelImportsAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getConfigurationAccess().getInstanceModelImportsAssignment_6()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__6__Impl"


    // $ANTLR start "rule__Configuration__Group__7"
    // InternalConfiguration.g:475:1: rule__Configuration__Group__7 : rule__Configuration__Group__7__Impl ;
    public final void rule__Configuration__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:479:1: ( rule__Configuration__Group__7__Impl )
            // InternalConfiguration.g:480:2: rule__Configuration__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__7"


    // $ANTLR start "rule__Configuration__Group__7__Impl"
    // InternalConfiguration.g:486:1: rule__Configuration__Group__7__Impl : ( ( rule__Configuration__StaticRoleBindingsAssignment_7 )* ) ;
    public final void rule__Configuration__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:490:1: ( ( ( rule__Configuration__StaticRoleBindingsAssignment_7 )* ) )
            // InternalConfiguration.g:491:1: ( ( rule__Configuration__StaticRoleBindingsAssignment_7 )* )
            {
            // InternalConfiguration.g:491:1: ( ( rule__Configuration__StaticRoleBindingsAssignment_7 )* )
            // InternalConfiguration.g:492:1: ( rule__Configuration__StaticRoleBindingsAssignment_7 )*
            {
             before(grammarAccess.getConfigurationAccess().getStaticRoleBindingsAssignment_7()); 
            // InternalConfiguration.g:493:1: ( rule__Configuration__StaticRoleBindingsAssignment_7 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==11||LA6_0==23) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalConfiguration.g:493:2: rule__Configuration__StaticRoleBindingsAssignment_7
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__Configuration__StaticRoleBindingsAssignment_7();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getConfigurationAccess().getStaticRoleBindingsAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__7__Impl"


    // $ANTLR start "rule__Configuration__Group_4__0"
    // InternalConfiguration.g:519:1: rule__Configuration__Group_4__0 : rule__Configuration__Group_4__0__Impl rule__Configuration__Group_4__1 ;
    public final void rule__Configuration__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:523:1: ( rule__Configuration__Group_4__0__Impl rule__Configuration__Group_4__1 )
            // InternalConfiguration.g:524:2: rule__Configuration__Group_4__0__Impl rule__Configuration__Group_4__1
            {
            pushFollow(FOLLOW_11);
            rule__Configuration__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__0"


    // $ANTLR start "rule__Configuration__Group_4__0__Impl"
    // InternalConfiguration.g:531:1: rule__Configuration__Group_4__0__Impl : ( 'ignored' ) ;
    public final void rule__Configuration__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:535:1: ( ( 'ignored' ) )
            // InternalConfiguration.g:536:1: ( 'ignored' )
            {
            // InternalConfiguration.g:536:1: ( 'ignored' )
            // InternalConfiguration.g:537:1: 'ignored'
            {
             before(grammarAccess.getConfigurationAccess().getIgnoredKeyword_4_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getIgnoredKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__0__Impl"


    // $ANTLR start "rule__Configuration__Group_4__1"
    // InternalConfiguration.g:550:1: rule__Configuration__Group_4__1 : rule__Configuration__Group_4__1__Impl rule__Configuration__Group_4__2 ;
    public final void rule__Configuration__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:554:1: ( rule__Configuration__Group_4__1__Impl rule__Configuration__Group_4__2 )
            // InternalConfiguration.g:555:2: rule__Configuration__Group_4__1__Impl rule__Configuration__Group_4__2
            {
            pushFollow(FOLLOW_6);
            rule__Configuration__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__1"


    // $ANTLR start "rule__Configuration__Group_4__1__Impl"
    // InternalConfiguration.g:562:1: rule__Configuration__Group_4__1__Impl : ( 'collaboration' ) ;
    public final void rule__Configuration__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:566:1: ( ( 'collaboration' ) )
            // InternalConfiguration.g:567:1: ( 'collaboration' )
            {
            // InternalConfiguration.g:567:1: ( 'collaboration' )
            // InternalConfiguration.g:568:1: 'collaboration'
            {
             before(grammarAccess.getConfigurationAccess().getCollaborationKeyword_4_1()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getCollaborationKeyword_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__1__Impl"


    // $ANTLR start "rule__Configuration__Group_4__2"
    // InternalConfiguration.g:581:1: rule__Configuration__Group_4__2 : rule__Configuration__Group_4__2__Impl rule__Configuration__Group_4__3 ;
    public final void rule__Configuration__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:585:1: ( rule__Configuration__Group_4__2__Impl rule__Configuration__Group_4__3 )
            // InternalConfiguration.g:586:2: rule__Configuration__Group_4__2__Impl rule__Configuration__Group_4__3
            {
            pushFollow(FOLLOW_12);
            rule__Configuration__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__2"


    // $ANTLR start "rule__Configuration__Group_4__2__Impl"
    // InternalConfiguration.g:593:1: rule__Configuration__Group_4__2__Impl : ( ( rule__Configuration__IgnoredCollaborationsAssignment_4_2 ) ) ;
    public final void rule__Configuration__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:597:1: ( ( ( rule__Configuration__IgnoredCollaborationsAssignment_4_2 ) ) )
            // InternalConfiguration.g:598:1: ( ( rule__Configuration__IgnoredCollaborationsAssignment_4_2 ) )
            {
            // InternalConfiguration.g:598:1: ( ( rule__Configuration__IgnoredCollaborationsAssignment_4_2 ) )
            // InternalConfiguration.g:599:1: ( rule__Configuration__IgnoredCollaborationsAssignment_4_2 )
            {
             before(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsAssignment_4_2()); 
            // InternalConfiguration.g:600:1: ( rule__Configuration__IgnoredCollaborationsAssignment_4_2 )
            // InternalConfiguration.g:600:2: rule__Configuration__IgnoredCollaborationsAssignment_4_2
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__IgnoredCollaborationsAssignment_4_2();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsAssignment_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__2__Impl"


    // $ANTLR start "rule__Configuration__Group_4__3"
    // InternalConfiguration.g:610:1: rule__Configuration__Group_4__3 : rule__Configuration__Group_4__3__Impl ;
    public final void rule__Configuration__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:614:1: ( rule__Configuration__Group_4__3__Impl )
            // InternalConfiguration.g:615:2: rule__Configuration__Group_4__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__Group_4__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__3"


    // $ANTLR start "rule__Configuration__Group_4__3__Impl"
    // InternalConfiguration.g:621:1: rule__Configuration__Group_4__3__Impl : ( ( rule__Configuration__Group_4_3__0 )* ) ;
    public final void rule__Configuration__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:625:1: ( ( ( rule__Configuration__Group_4_3__0 )* ) )
            // InternalConfiguration.g:626:1: ( ( rule__Configuration__Group_4_3__0 )* )
            {
            // InternalConfiguration.g:626:1: ( ( rule__Configuration__Group_4_3__0 )* )
            // InternalConfiguration.g:627:1: ( rule__Configuration__Group_4_3__0 )*
            {
             before(grammarAccess.getConfigurationAccess().getGroup_4_3()); 
            // InternalConfiguration.g:628:1: ( rule__Configuration__Group_4_3__0 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==16) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalConfiguration.g:628:2: rule__Configuration__Group_4_3__0
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__Configuration__Group_4_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getConfigurationAccess().getGroup_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__3__Impl"


    // $ANTLR start "rule__Configuration__Group_4_3__0"
    // InternalConfiguration.g:646:1: rule__Configuration__Group_4_3__0 : rule__Configuration__Group_4_3__0__Impl rule__Configuration__Group_4_3__1 ;
    public final void rule__Configuration__Group_4_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:650:1: ( rule__Configuration__Group_4_3__0__Impl rule__Configuration__Group_4_3__1 )
            // InternalConfiguration.g:651:2: rule__Configuration__Group_4_3__0__Impl rule__Configuration__Group_4_3__1
            {
            pushFollow(FOLLOW_6);
            rule__Configuration__Group_4_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group_4_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4_3__0"


    // $ANTLR start "rule__Configuration__Group_4_3__0__Impl"
    // InternalConfiguration.g:658:1: rule__Configuration__Group_4_3__0__Impl : ( ',' ) ;
    public final void rule__Configuration__Group_4_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:662:1: ( ( ',' ) )
            // InternalConfiguration.g:663:1: ( ',' )
            {
            // InternalConfiguration.g:663:1: ( ',' )
            // InternalConfiguration.g:664:1: ','
            {
             before(grammarAccess.getConfigurationAccess().getCommaKeyword_4_3_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getCommaKeyword_4_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4_3__0__Impl"


    // $ANTLR start "rule__Configuration__Group_4_3__1"
    // InternalConfiguration.g:677:1: rule__Configuration__Group_4_3__1 : rule__Configuration__Group_4_3__1__Impl ;
    public final void rule__Configuration__Group_4_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:681:1: ( rule__Configuration__Group_4_3__1__Impl )
            // InternalConfiguration.g:682:2: rule__Configuration__Group_4_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__Group_4_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4_3__1"


    // $ANTLR start "rule__Configuration__Group_4_3__1__Impl"
    // InternalConfiguration.g:688:1: rule__Configuration__Group_4_3__1__Impl : ( ( rule__Configuration__IgnoredCollaborationsAssignment_4_3_1 ) ) ;
    public final void rule__Configuration__Group_4_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:692:1: ( ( ( rule__Configuration__IgnoredCollaborationsAssignment_4_3_1 ) ) )
            // InternalConfiguration.g:693:1: ( ( rule__Configuration__IgnoredCollaborationsAssignment_4_3_1 ) )
            {
            // InternalConfiguration.g:693:1: ( ( rule__Configuration__IgnoredCollaborationsAssignment_4_3_1 ) )
            // InternalConfiguration.g:694:1: ( rule__Configuration__IgnoredCollaborationsAssignment_4_3_1 )
            {
             before(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsAssignment_4_3_1()); 
            // InternalConfiguration.g:695:1: ( rule__Configuration__IgnoredCollaborationsAssignment_4_3_1 )
            // InternalConfiguration.g:695:2: rule__Configuration__IgnoredCollaborationsAssignment_4_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__IgnoredCollaborationsAssignment_4_3_1();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsAssignment_4_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4_3__1__Impl"


    // $ANTLR start "rule__Configuration__Group_5__0"
    // InternalConfiguration.g:709:1: rule__Configuration__Group_5__0 : rule__Configuration__Group_5__0__Impl rule__Configuration__Group_5__1 ;
    public final void rule__Configuration__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:713:1: ( rule__Configuration__Group_5__0__Impl rule__Configuration__Group_5__1 )
            // InternalConfiguration.g:714:2: rule__Configuration__Group_5__0__Impl rule__Configuration__Group_5__1
            {
            pushFollow(FOLLOW_14);
            rule__Configuration__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__0"


    // $ANTLR start "rule__Configuration__Group_5__0__Impl"
    // InternalConfiguration.g:721:1: rule__Configuration__Group_5__0__Impl : ( 'auxiliary' ) ;
    public final void rule__Configuration__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:725:1: ( ( 'auxiliary' ) )
            // InternalConfiguration.g:726:1: ( 'auxiliary' )
            {
            // InternalConfiguration.g:726:1: ( 'auxiliary' )
            // InternalConfiguration.g:727:1: 'auxiliary'
            {
             before(grammarAccess.getConfigurationAccess().getAuxiliaryKeyword_5_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getAuxiliaryKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__0__Impl"


    // $ANTLR start "rule__Configuration__Group_5__1"
    // InternalConfiguration.g:740:1: rule__Configuration__Group_5__1 : rule__Configuration__Group_5__1__Impl rule__Configuration__Group_5__2 ;
    public final void rule__Configuration__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:744:1: ( rule__Configuration__Group_5__1__Impl rule__Configuration__Group_5__2 )
            // InternalConfiguration.g:745:2: rule__Configuration__Group_5__1__Impl rule__Configuration__Group_5__2
            {
            pushFollow(FOLLOW_6);
            rule__Configuration__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__1"


    // $ANTLR start "rule__Configuration__Group_5__1__Impl"
    // InternalConfiguration.g:752:1: rule__Configuration__Group_5__1__Impl : ( 'collaborations' ) ;
    public final void rule__Configuration__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:756:1: ( ( 'collaborations' ) )
            // InternalConfiguration.g:757:1: ( 'collaborations' )
            {
            // InternalConfiguration.g:757:1: ( 'collaborations' )
            // InternalConfiguration.g:758:1: 'collaborations'
            {
             before(grammarAccess.getConfigurationAccess().getCollaborationsKeyword_5_1()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getCollaborationsKeyword_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__1__Impl"


    // $ANTLR start "rule__Configuration__Group_5__2"
    // InternalConfiguration.g:771:1: rule__Configuration__Group_5__2 : rule__Configuration__Group_5__2__Impl rule__Configuration__Group_5__3 ;
    public final void rule__Configuration__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:775:1: ( rule__Configuration__Group_5__2__Impl rule__Configuration__Group_5__3 )
            // InternalConfiguration.g:776:2: rule__Configuration__Group_5__2__Impl rule__Configuration__Group_5__3
            {
            pushFollow(FOLLOW_12);
            rule__Configuration__Group_5__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group_5__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__2"


    // $ANTLR start "rule__Configuration__Group_5__2__Impl"
    // InternalConfiguration.g:783:1: rule__Configuration__Group_5__2__Impl : ( ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_2 ) ) ;
    public final void rule__Configuration__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:787:1: ( ( ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_2 ) ) )
            // InternalConfiguration.g:788:1: ( ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_2 ) )
            {
            // InternalConfiguration.g:788:1: ( ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_2 ) )
            // InternalConfiguration.g:789:1: ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_2 )
            {
             before(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsAssignment_5_2()); 
            // InternalConfiguration.g:790:1: ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_2 )
            // InternalConfiguration.g:790:2: rule__Configuration__AuxiliaryCollaborationsAssignment_5_2
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__AuxiliaryCollaborationsAssignment_5_2();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsAssignment_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__2__Impl"


    // $ANTLR start "rule__Configuration__Group_5__3"
    // InternalConfiguration.g:800:1: rule__Configuration__Group_5__3 : rule__Configuration__Group_5__3__Impl ;
    public final void rule__Configuration__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:804:1: ( rule__Configuration__Group_5__3__Impl )
            // InternalConfiguration.g:805:2: rule__Configuration__Group_5__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__Group_5__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__3"


    // $ANTLR start "rule__Configuration__Group_5__3__Impl"
    // InternalConfiguration.g:811:1: rule__Configuration__Group_5__3__Impl : ( ( rule__Configuration__Group_5_3__0 )* ) ;
    public final void rule__Configuration__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:815:1: ( ( ( rule__Configuration__Group_5_3__0 )* ) )
            // InternalConfiguration.g:816:1: ( ( rule__Configuration__Group_5_3__0 )* )
            {
            // InternalConfiguration.g:816:1: ( ( rule__Configuration__Group_5_3__0 )* )
            // InternalConfiguration.g:817:1: ( rule__Configuration__Group_5_3__0 )*
            {
             before(grammarAccess.getConfigurationAccess().getGroup_5_3()); 
            // InternalConfiguration.g:818:1: ( rule__Configuration__Group_5_3__0 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==16) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalConfiguration.g:818:2: rule__Configuration__Group_5_3__0
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__Configuration__Group_5_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getConfigurationAccess().getGroup_5_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__3__Impl"


    // $ANTLR start "rule__Configuration__Group_5_3__0"
    // InternalConfiguration.g:836:1: rule__Configuration__Group_5_3__0 : rule__Configuration__Group_5_3__0__Impl rule__Configuration__Group_5_3__1 ;
    public final void rule__Configuration__Group_5_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:840:1: ( rule__Configuration__Group_5_3__0__Impl rule__Configuration__Group_5_3__1 )
            // InternalConfiguration.g:841:2: rule__Configuration__Group_5_3__0__Impl rule__Configuration__Group_5_3__1
            {
            pushFollow(FOLLOW_6);
            rule__Configuration__Group_5_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group_5_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5_3__0"


    // $ANTLR start "rule__Configuration__Group_5_3__0__Impl"
    // InternalConfiguration.g:848:1: rule__Configuration__Group_5_3__0__Impl : ( ',' ) ;
    public final void rule__Configuration__Group_5_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:852:1: ( ( ',' ) )
            // InternalConfiguration.g:853:1: ( ',' )
            {
            // InternalConfiguration.g:853:1: ( ',' )
            // InternalConfiguration.g:854:1: ','
            {
             before(grammarAccess.getConfigurationAccess().getCommaKeyword_5_3_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getCommaKeyword_5_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5_3__0__Impl"


    // $ANTLR start "rule__Configuration__Group_5_3__1"
    // InternalConfiguration.g:867:1: rule__Configuration__Group_5_3__1 : rule__Configuration__Group_5_3__1__Impl ;
    public final void rule__Configuration__Group_5_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:871:1: ( rule__Configuration__Group_5_3__1__Impl )
            // InternalConfiguration.g:872:2: rule__Configuration__Group_5_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__Group_5_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5_3__1"


    // $ANTLR start "rule__Configuration__Group_5_3__1__Impl"
    // InternalConfiguration.g:878:1: rule__Configuration__Group_5_3__1__Impl : ( ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_3_1 ) ) ;
    public final void rule__Configuration__Group_5_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:882:1: ( ( ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_3_1 ) ) )
            // InternalConfiguration.g:883:1: ( ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_3_1 ) )
            {
            // InternalConfiguration.g:883:1: ( ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_3_1 ) )
            // InternalConfiguration.g:884:1: ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_3_1 )
            {
             before(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsAssignment_5_3_1()); 
            // InternalConfiguration.g:885:1: ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_3_1 )
            // InternalConfiguration.g:885:2: rule__Configuration__AuxiliaryCollaborationsAssignment_5_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__AuxiliaryCollaborationsAssignment_5_3_1();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsAssignment_5_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5_3__1__Impl"


    // $ANTLR start "rule__FQN__Group__0"
    // InternalConfiguration.g:899:1: rule__FQN__Group__0 : rule__FQN__Group__0__Impl rule__FQN__Group__1 ;
    public final void rule__FQN__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:903:1: ( rule__FQN__Group__0__Impl rule__FQN__Group__1 )
            // InternalConfiguration.g:904:2: rule__FQN__Group__0__Impl rule__FQN__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__FQN__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FQN__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group__0"


    // $ANTLR start "rule__FQN__Group__0__Impl"
    // InternalConfiguration.g:911:1: rule__FQN__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__FQN__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:915:1: ( ( RULE_ID ) )
            // InternalConfiguration.g:916:1: ( RULE_ID )
            {
            // InternalConfiguration.g:916:1: ( RULE_ID )
            // InternalConfiguration.g:917:1: RULE_ID
            {
             before(grammarAccess.getFQNAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFQNAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group__0__Impl"


    // $ANTLR start "rule__FQN__Group__1"
    // InternalConfiguration.g:928:1: rule__FQN__Group__1 : rule__FQN__Group__1__Impl ;
    public final void rule__FQN__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:932:1: ( rule__FQN__Group__1__Impl )
            // InternalConfiguration.g:933:2: rule__FQN__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FQN__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group__1"


    // $ANTLR start "rule__FQN__Group__1__Impl"
    // InternalConfiguration.g:939:1: rule__FQN__Group__1__Impl : ( ( rule__FQN__Group_1__0 )* ) ;
    public final void rule__FQN__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:943:1: ( ( ( rule__FQN__Group_1__0 )* ) )
            // InternalConfiguration.g:944:1: ( ( rule__FQN__Group_1__0 )* )
            {
            // InternalConfiguration.g:944:1: ( ( rule__FQN__Group_1__0 )* )
            // InternalConfiguration.g:945:1: ( rule__FQN__Group_1__0 )*
            {
             before(grammarAccess.getFQNAccess().getGroup_1()); 
            // InternalConfiguration.g:946:1: ( rule__FQN__Group_1__0 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==19) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalConfiguration.g:946:2: rule__FQN__Group_1__0
            	    {
            	    pushFollow(FOLLOW_16);
            	    rule__FQN__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getFQNAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group__1__Impl"


    // $ANTLR start "rule__FQN__Group_1__0"
    // InternalConfiguration.g:960:1: rule__FQN__Group_1__0 : rule__FQN__Group_1__0__Impl rule__FQN__Group_1__1 ;
    public final void rule__FQN__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:964:1: ( rule__FQN__Group_1__0__Impl rule__FQN__Group_1__1 )
            // InternalConfiguration.g:965:2: rule__FQN__Group_1__0__Impl rule__FQN__Group_1__1
            {
            pushFollow(FOLLOW_6);
            rule__FQN__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FQN__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group_1__0"


    // $ANTLR start "rule__FQN__Group_1__0__Impl"
    // InternalConfiguration.g:972:1: rule__FQN__Group_1__0__Impl : ( '.' ) ;
    public final void rule__FQN__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:976:1: ( ( '.' ) )
            // InternalConfiguration.g:977:1: ( '.' )
            {
            // InternalConfiguration.g:977:1: ( '.' )
            // InternalConfiguration.g:978:1: '.'
            {
             before(grammarAccess.getFQNAccess().getFullStopKeyword_1_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getFQNAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group_1__0__Impl"


    // $ANTLR start "rule__FQN__Group_1__1"
    // InternalConfiguration.g:991:1: rule__FQN__Group_1__1 : rule__FQN__Group_1__1__Impl ;
    public final void rule__FQN__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:995:1: ( rule__FQN__Group_1__1__Impl )
            // InternalConfiguration.g:996:2: rule__FQN__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FQN__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group_1__1"


    // $ANTLR start "rule__FQN__Group_1__1__Impl"
    // InternalConfiguration.g:1002:1: rule__FQN__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__FQN__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1006:1: ( ( RULE_ID ) )
            // InternalConfiguration.g:1007:1: ( RULE_ID )
            {
            // InternalConfiguration.g:1007:1: ( RULE_ID )
            // InternalConfiguration.g:1008:1: RULE_ID
            {
             before(grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group_1__1__Impl"


    // $ANTLR start "rule__RoleBindings__Group__0"
    // InternalConfiguration.g:1023:1: rule__RoleBindings__Group__0 : rule__RoleBindings__Group__0__Impl rule__RoleBindings__Group__1 ;
    public final void rule__RoleBindings__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1027:1: ( rule__RoleBindings__Group__0__Impl rule__RoleBindings__Group__1 )
            // InternalConfiguration.g:1028:2: rule__RoleBindings__Group__0__Impl rule__RoleBindings__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__RoleBindings__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RoleBindings__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__0"


    // $ANTLR start "rule__RoleBindings__Group__0__Impl"
    // InternalConfiguration.g:1035:1: rule__RoleBindings__Group__0__Impl : ( ( rule__RoleBindings__Alternatives_0 ) ) ;
    public final void rule__RoleBindings__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1039:1: ( ( ( rule__RoleBindings__Alternatives_0 ) ) )
            // InternalConfiguration.g:1040:1: ( ( rule__RoleBindings__Alternatives_0 ) )
            {
            // InternalConfiguration.g:1040:1: ( ( rule__RoleBindings__Alternatives_0 ) )
            // InternalConfiguration.g:1041:1: ( rule__RoleBindings__Alternatives_0 )
            {
             before(grammarAccess.getRoleBindingsAccess().getAlternatives_0()); 
            // InternalConfiguration.g:1042:1: ( rule__RoleBindings__Alternatives_0 )
            // InternalConfiguration.g:1042:2: rule__RoleBindings__Alternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__RoleBindings__Alternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getRoleBindingsAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__0__Impl"


    // $ANTLR start "rule__RoleBindings__Group__1"
    // InternalConfiguration.g:1052:1: rule__RoleBindings__Group__1 : rule__RoleBindings__Group__1__Impl rule__RoleBindings__Group__2 ;
    public final void rule__RoleBindings__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1056:1: ( rule__RoleBindings__Group__1__Impl rule__RoleBindings__Group__2 )
            // InternalConfiguration.g:1057:2: rule__RoleBindings__Group__1__Impl rule__RoleBindings__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__RoleBindings__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RoleBindings__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__1"


    // $ANTLR start "rule__RoleBindings__Group__1__Impl"
    // InternalConfiguration.g:1064:1: rule__RoleBindings__Group__1__Impl : ( 'for' ) ;
    public final void rule__RoleBindings__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1068:1: ( ( 'for' ) )
            // InternalConfiguration.g:1069:1: ( 'for' )
            {
            // InternalConfiguration.g:1069:1: ( 'for' )
            // InternalConfiguration.g:1070:1: 'for'
            {
             before(grammarAccess.getRoleBindingsAccess().getForKeyword_1()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getRoleBindingsAccess().getForKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__1__Impl"


    // $ANTLR start "rule__RoleBindings__Group__2"
    // InternalConfiguration.g:1083:1: rule__RoleBindings__Group__2 : rule__RoleBindings__Group__2__Impl rule__RoleBindings__Group__3 ;
    public final void rule__RoleBindings__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1087:1: ( rule__RoleBindings__Group__2__Impl rule__RoleBindings__Group__3 )
            // InternalConfiguration.g:1088:2: rule__RoleBindings__Group__2__Impl rule__RoleBindings__Group__3
            {
            pushFollow(FOLLOW_6);
            rule__RoleBindings__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RoleBindings__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__2"


    // $ANTLR start "rule__RoleBindings__Group__2__Impl"
    // InternalConfiguration.g:1095:1: rule__RoleBindings__Group__2__Impl : ( 'collaboration' ) ;
    public final void rule__RoleBindings__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1099:1: ( ( 'collaboration' ) )
            // InternalConfiguration.g:1100:1: ( 'collaboration' )
            {
            // InternalConfiguration.g:1100:1: ( 'collaboration' )
            // InternalConfiguration.g:1101:1: 'collaboration'
            {
             before(grammarAccess.getRoleBindingsAccess().getCollaborationKeyword_2()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getRoleBindingsAccess().getCollaborationKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__2__Impl"


    // $ANTLR start "rule__RoleBindings__Group__3"
    // InternalConfiguration.g:1114:1: rule__RoleBindings__Group__3 : rule__RoleBindings__Group__3__Impl rule__RoleBindings__Group__4 ;
    public final void rule__RoleBindings__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1118:1: ( rule__RoleBindings__Group__3__Impl rule__RoleBindings__Group__4 )
            // InternalConfiguration.g:1119:2: rule__RoleBindings__Group__3__Impl rule__RoleBindings__Group__4
            {
            pushFollow(FOLLOW_18);
            rule__RoleBindings__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RoleBindings__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__3"


    // $ANTLR start "rule__RoleBindings__Group__3__Impl"
    // InternalConfiguration.g:1126:1: rule__RoleBindings__Group__3__Impl : ( ( rule__RoleBindings__CollaborationAssignment_3 ) ) ;
    public final void rule__RoleBindings__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1130:1: ( ( ( rule__RoleBindings__CollaborationAssignment_3 ) ) )
            // InternalConfiguration.g:1131:1: ( ( rule__RoleBindings__CollaborationAssignment_3 ) )
            {
            // InternalConfiguration.g:1131:1: ( ( rule__RoleBindings__CollaborationAssignment_3 ) )
            // InternalConfiguration.g:1132:1: ( rule__RoleBindings__CollaborationAssignment_3 )
            {
             before(grammarAccess.getRoleBindingsAccess().getCollaborationAssignment_3()); 
            // InternalConfiguration.g:1133:1: ( rule__RoleBindings__CollaborationAssignment_3 )
            // InternalConfiguration.g:1133:2: rule__RoleBindings__CollaborationAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__RoleBindings__CollaborationAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getRoleBindingsAccess().getCollaborationAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__3__Impl"


    // $ANTLR start "rule__RoleBindings__Group__4"
    // InternalConfiguration.g:1143:1: rule__RoleBindings__Group__4 : rule__RoleBindings__Group__4__Impl rule__RoleBindings__Group__5 ;
    public final void rule__RoleBindings__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1147:1: ( rule__RoleBindings__Group__4__Impl rule__RoleBindings__Group__5 )
            // InternalConfiguration.g:1148:2: rule__RoleBindings__Group__4__Impl rule__RoleBindings__Group__5
            {
            pushFollow(FOLLOW_19);
            rule__RoleBindings__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RoleBindings__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__4"


    // $ANTLR start "rule__RoleBindings__Group__4__Impl"
    // InternalConfiguration.g:1155:1: rule__RoleBindings__Group__4__Impl : ( '{' ) ;
    public final void rule__RoleBindings__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1159:1: ( ( '{' ) )
            // InternalConfiguration.g:1160:1: ( '{' )
            {
            // InternalConfiguration.g:1160:1: ( '{' )
            // InternalConfiguration.g:1161:1: '{'
            {
             before(grammarAccess.getRoleBindingsAccess().getLeftCurlyBracketKeyword_4()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getRoleBindingsAccess().getLeftCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__4__Impl"


    // $ANTLR start "rule__RoleBindings__Group__5"
    // InternalConfiguration.g:1174:1: rule__RoleBindings__Group__5 : rule__RoleBindings__Group__5__Impl rule__RoleBindings__Group__6 ;
    public final void rule__RoleBindings__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1178:1: ( rule__RoleBindings__Group__5__Impl rule__RoleBindings__Group__6 )
            // InternalConfiguration.g:1179:2: rule__RoleBindings__Group__5__Impl rule__RoleBindings__Group__6
            {
            pushFollow(FOLLOW_19);
            rule__RoleBindings__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RoleBindings__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__5"


    // $ANTLR start "rule__RoleBindings__Group__5__Impl"
    // InternalConfiguration.g:1186:1: rule__RoleBindings__Group__5__Impl : ( ( rule__RoleBindings__BindingsAssignment_5 )* ) ;
    public final void rule__RoleBindings__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1190:1: ( ( ( rule__RoleBindings__BindingsAssignment_5 )* ) )
            // InternalConfiguration.g:1191:1: ( ( rule__RoleBindings__BindingsAssignment_5 )* )
            {
            // InternalConfiguration.g:1191:1: ( ( rule__RoleBindings__BindingsAssignment_5 )* )
            // InternalConfiguration.g:1192:1: ( rule__RoleBindings__BindingsAssignment_5 )*
            {
             before(grammarAccess.getRoleBindingsAccess().getBindingsAssignment_5()); 
            // InternalConfiguration.g:1193:1: ( rule__RoleBindings__BindingsAssignment_5 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==25) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalConfiguration.g:1193:2: rule__RoleBindings__BindingsAssignment_5
            	    {
            	    pushFollow(FOLLOW_20);
            	    rule__RoleBindings__BindingsAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getRoleBindingsAccess().getBindingsAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__5__Impl"


    // $ANTLR start "rule__RoleBindings__Group__6"
    // InternalConfiguration.g:1203:1: rule__RoleBindings__Group__6 : rule__RoleBindings__Group__6__Impl ;
    public final void rule__RoleBindings__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1207:1: ( rule__RoleBindings__Group__6__Impl )
            // InternalConfiguration.g:1208:2: rule__RoleBindings__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__RoleBindings__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__6"


    // $ANTLR start "rule__RoleBindings__Group__6__Impl"
    // InternalConfiguration.g:1214:1: rule__RoleBindings__Group__6__Impl : ( '}' ) ;
    public final void rule__RoleBindings__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1218:1: ( ( '}' ) )
            // InternalConfiguration.g:1219:1: ( '}' )
            {
            // InternalConfiguration.g:1219:1: ( '}' )
            // InternalConfiguration.g:1220:1: '}'
            {
             before(grammarAccess.getRoleBindingsAccess().getRightCurlyBracketKeyword_6()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getRoleBindingsAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__6__Impl"


    // $ANTLR start "rule__RoleBindings__Group_0_1__0"
    // InternalConfiguration.g:1247:1: rule__RoleBindings__Group_0_1__0 : rule__RoleBindings__Group_0_1__0__Impl rule__RoleBindings__Group_0_1__1 ;
    public final void rule__RoleBindings__Group_0_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1251:1: ( rule__RoleBindings__Group_0_1__0__Impl rule__RoleBindings__Group_0_1__1 )
            // InternalConfiguration.g:1252:2: rule__RoleBindings__Group_0_1__0__Impl rule__RoleBindings__Group_0_1__1
            {
            pushFollow(FOLLOW_21);
            rule__RoleBindings__Group_0_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RoleBindings__Group_0_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group_0_1__0"


    // $ANTLR start "rule__RoleBindings__Group_0_1__0__Impl"
    // InternalConfiguration.g:1259:1: rule__RoleBindings__Group_0_1__0__Impl : ( 'role' ) ;
    public final void rule__RoleBindings__Group_0_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1263:1: ( ( 'role' ) )
            // InternalConfiguration.g:1264:1: ( 'role' )
            {
            // InternalConfiguration.g:1264:1: ( 'role' )
            // InternalConfiguration.g:1265:1: 'role'
            {
             before(grammarAccess.getRoleBindingsAccess().getRoleKeyword_0_1_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getRoleBindingsAccess().getRoleKeyword_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group_0_1__0__Impl"


    // $ANTLR start "rule__RoleBindings__Group_0_1__1"
    // InternalConfiguration.g:1278:1: rule__RoleBindings__Group_0_1__1 : rule__RoleBindings__Group_0_1__1__Impl ;
    public final void rule__RoleBindings__Group_0_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1282:1: ( rule__RoleBindings__Group_0_1__1__Impl )
            // InternalConfiguration.g:1283:2: rule__RoleBindings__Group_0_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__RoleBindings__Group_0_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group_0_1__1"


    // $ANTLR start "rule__RoleBindings__Group_0_1__1__Impl"
    // InternalConfiguration.g:1289:1: rule__RoleBindings__Group_0_1__1__Impl : ( 'bindings' ) ;
    public final void rule__RoleBindings__Group_0_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1293:1: ( ( 'bindings' ) )
            // InternalConfiguration.g:1294:1: ( 'bindings' )
            {
            // InternalConfiguration.g:1294:1: ( 'bindings' )
            // InternalConfiguration.g:1295:1: 'bindings'
            {
             before(grammarAccess.getRoleBindingsAccess().getBindingsKeyword_0_1_1()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getRoleBindingsAccess().getBindingsKeyword_0_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group_0_1__1__Impl"


    // $ANTLR start "rule__RoleAssignment__Group__0"
    // InternalConfiguration.g:1312:1: rule__RoleAssignment__Group__0 : rule__RoleAssignment__Group__0__Impl rule__RoleAssignment__Group__1 ;
    public final void rule__RoleAssignment__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1316:1: ( rule__RoleAssignment__Group__0__Impl rule__RoleAssignment__Group__1 )
            // InternalConfiguration.g:1317:2: rule__RoleAssignment__Group__0__Impl rule__RoleAssignment__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__RoleAssignment__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RoleAssignment__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__Group__0"


    // $ANTLR start "rule__RoleAssignment__Group__0__Impl"
    // InternalConfiguration.g:1324:1: rule__RoleAssignment__Group__0__Impl : ( 'object' ) ;
    public final void rule__RoleAssignment__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1328:1: ( ( 'object' ) )
            // InternalConfiguration.g:1329:1: ( 'object' )
            {
            // InternalConfiguration.g:1329:1: ( 'object' )
            // InternalConfiguration.g:1330:1: 'object'
            {
             before(grammarAccess.getRoleAssignmentAccess().getObjectKeyword_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getRoleAssignmentAccess().getObjectKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__Group__0__Impl"


    // $ANTLR start "rule__RoleAssignment__Group__1"
    // InternalConfiguration.g:1343:1: rule__RoleAssignment__Group__1 : rule__RoleAssignment__Group__1__Impl rule__RoleAssignment__Group__2 ;
    public final void rule__RoleAssignment__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1347:1: ( rule__RoleAssignment__Group__1__Impl rule__RoleAssignment__Group__2 )
            // InternalConfiguration.g:1348:2: rule__RoleAssignment__Group__1__Impl rule__RoleAssignment__Group__2
            {
            pushFollow(FOLLOW_22);
            rule__RoleAssignment__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RoleAssignment__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__Group__1"


    // $ANTLR start "rule__RoleAssignment__Group__1__Impl"
    // InternalConfiguration.g:1355:1: rule__RoleAssignment__Group__1__Impl : ( ( rule__RoleAssignment__ObjectAssignment_1 ) ) ;
    public final void rule__RoleAssignment__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1359:1: ( ( ( rule__RoleAssignment__ObjectAssignment_1 ) ) )
            // InternalConfiguration.g:1360:1: ( ( rule__RoleAssignment__ObjectAssignment_1 ) )
            {
            // InternalConfiguration.g:1360:1: ( ( rule__RoleAssignment__ObjectAssignment_1 ) )
            // InternalConfiguration.g:1361:1: ( rule__RoleAssignment__ObjectAssignment_1 )
            {
             before(grammarAccess.getRoleAssignmentAccess().getObjectAssignment_1()); 
            // InternalConfiguration.g:1362:1: ( rule__RoleAssignment__ObjectAssignment_1 )
            // InternalConfiguration.g:1362:2: rule__RoleAssignment__ObjectAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__RoleAssignment__ObjectAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getRoleAssignmentAccess().getObjectAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__Group__1__Impl"


    // $ANTLR start "rule__RoleAssignment__Group__2"
    // InternalConfiguration.g:1372:1: rule__RoleAssignment__Group__2 : rule__RoleAssignment__Group__2__Impl rule__RoleAssignment__Group__3 ;
    public final void rule__RoleAssignment__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1376:1: ( rule__RoleAssignment__Group__2__Impl rule__RoleAssignment__Group__3 )
            // InternalConfiguration.g:1377:2: rule__RoleAssignment__Group__2__Impl rule__RoleAssignment__Group__3
            {
            pushFollow(FOLLOW_23);
            rule__RoleAssignment__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RoleAssignment__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__Group__2"


    // $ANTLR start "rule__RoleAssignment__Group__2__Impl"
    // InternalConfiguration.g:1384:1: rule__RoleAssignment__Group__2__Impl : ( 'plays' ) ;
    public final void rule__RoleAssignment__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1388:1: ( ( 'plays' ) )
            // InternalConfiguration.g:1389:1: ( 'plays' )
            {
            // InternalConfiguration.g:1389:1: ( 'plays' )
            // InternalConfiguration.g:1390:1: 'plays'
            {
             before(grammarAccess.getRoleAssignmentAccess().getPlaysKeyword_2()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getRoleAssignmentAccess().getPlaysKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__Group__2__Impl"


    // $ANTLR start "rule__RoleAssignment__Group__3"
    // InternalConfiguration.g:1403:1: rule__RoleAssignment__Group__3 : rule__RoleAssignment__Group__3__Impl rule__RoleAssignment__Group__4 ;
    public final void rule__RoleAssignment__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1407:1: ( rule__RoleAssignment__Group__3__Impl rule__RoleAssignment__Group__4 )
            // InternalConfiguration.g:1408:2: rule__RoleAssignment__Group__3__Impl rule__RoleAssignment__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__RoleAssignment__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RoleAssignment__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__Group__3"


    // $ANTLR start "rule__RoleAssignment__Group__3__Impl"
    // InternalConfiguration.g:1415:1: rule__RoleAssignment__Group__3__Impl : ( 'role' ) ;
    public final void rule__RoleAssignment__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1419:1: ( ( 'role' ) )
            // InternalConfiguration.g:1420:1: ( 'role' )
            {
            // InternalConfiguration.g:1420:1: ( 'role' )
            // InternalConfiguration.g:1421:1: 'role'
            {
             before(grammarAccess.getRoleAssignmentAccess().getRoleKeyword_3()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getRoleAssignmentAccess().getRoleKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__Group__3__Impl"


    // $ANTLR start "rule__RoleAssignment__Group__4"
    // InternalConfiguration.g:1434:1: rule__RoleAssignment__Group__4 : rule__RoleAssignment__Group__4__Impl ;
    public final void rule__RoleAssignment__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1438:1: ( rule__RoleAssignment__Group__4__Impl )
            // InternalConfiguration.g:1439:2: rule__RoleAssignment__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__RoleAssignment__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__Group__4"


    // $ANTLR start "rule__RoleAssignment__Group__4__Impl"
    // InternalConfiguration.g:1445:1: rule__RoleAssignment__Group__4__Impl : ( ( rule__RoleAssignment__RoleAssignment_4 ) ) ;
    public final void rule__RoleAssignment__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1449:1: ( ( ( rule__RoleAssignment__RoleAssignment_4 ) ) )
            // InternalConfiguration.g:1450:1: ( ( rule__RoleAssignment__RoleAssignment_4 ) )
            {
            // InternalConfiguration.g:1450:1: ( ( rule__RoleAssignment__RoleAssignment_4 ) )
            // InternalConfiguration.g:1451:1: ( rule__RoleAssignment__RoleAssignment_4 )
            {
             before(grammarAccess.getRoleAssignmentAccess().getRoleAssignment_4()); 
            // InternalConfiguration.g:1452:1: ( rule__RoleAssignment__RoleAssignment_4 )
            // InternalConfiguration.g:1452:2: rule__RoleAssignment__RoleAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__RoleAssignment__RoleAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getRoleAssignmentAccess().getRoleAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__Group__4__Impl"


    // $ANTLR start "rule__XMIImport__Group__0"
    // InternalConfiguration.g:1472:1: rule__XMIImport__Group__0 : rule__XMIImport__Group__0__Impl rule__XMIImport__Group__1 ;
    public final void rule__XMIImport__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1476:1: ( rule__XMIImport__Group__0__Impl rule__XMIImport__Group__1 )
            // InternalConfiguration.g:1477:2: rule__XMIImport__Group__0__Impl rule__XMIImport__Group__1
            {
            pushFollow(FOLLOW_24);
            rule__XMIImport__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XMIImport__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XMIImport__Group__0"


    // $ANTLR start "rule__XMIImport__Group__0__Impl"
    // InternalConfiguration.g:1484:1: rule__XMIImport__Group__0__Impl : ( 'use' ) ;
    public final void rule__XMIImport__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1488:1: ( ( 'use' ) )
            // InternalConfiguration.g:1489:1: ( 'use' )
            {
            // InternalConfiguration.g:1489:1: ( 'use' )
            // InternalConfiguration.g:1490:1: 'use'
            {
             before(grammarAccess.getXMIImportAccess().getUseKeyword_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getXMIImportAccess().getUseKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XMIImport__Group__0__Impl"


    // $ANTLR start "rule__XMIImport__Group__1"
    // InternalConfiguration.g:1503:1: rule__XMIImport__Group__1 : rule__XMIImport__Group__1__Impl rule__XMIImport__Group__2 ;
    public final void rule__XMIImport__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1507:1: ( rule__XMIImport__Group__1__Impl rule__XMIImport__Group__2 )
            // InternalConfiguration.g:1508:2: rule__XMIImport__Group__1__Impl rule__XMIImport__Group__2
            {
            pushFollow(FOLLOW_25);
            rule__XMIImport__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XMIImport__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XMIImport__Group__1"


    // $ANTLR start "rule__XMIImport__Group__1__Impl"
    // InternalConfiguration.g:1515:1: rule__XMIImport__Group__1__Impl : ( 'instancemodel' ) ;
    public final void rule__XMIImport__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1519:1: ( ( 'instancemodel' ) )
            // InternalConfiguration.g:1520:1: ( 'instancemodel' )
            {
            // InternalConfiguration.g:1520:1: ( 'instancemodel' )
            // InternalConfiguration.g:1521:1: 'instancemodel'
            {
             before(grammarAccess.getXMIImportAccess().getInstancemodelKeyword_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getXMIImportAccess().getInstancemodelKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XMIImport__Group__1__Impl"


    // $ANTLR start "rule__XMIImport__Group__2"
    // InternalConfiguration.g:1534:1: rule__XMIImport__Group__2 : rule__XMIImport__Group__2__Impl ;
    public final void rule__XMIImport__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1538:1: ( rule__XMIImport__Group__2__Impl )
            // InternalConfiguration.g:1539:2: rule__XMIImport__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XMIImport__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XMIImport__Group__2"


    // $ANTLR start "rule__XMIImport__Group__2__Impl"
    // InternalConfiguration.g:1545:1: rule__XMIImport__Group__2__Impl : ( ( rule__XMIImport__ImportURIAssignment_2 ) ) ;
    public final void rule__XMIImport__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1549:1: ( ( ( rule__XMIImport__ImportURIAssignment_2 ) ) )
            // InternalConfiguration.g:1550:1: ( ( rule__XMIImport__ImportURIAssignment_2 ) )
            {
            // InternalConfiguration.g:1550:1: ( ( rule__XMIImport__ImportURIAssignment_2 ) )
            // InternalConfiguration.g:1551:1: ( rule__XMIImport__ImportURIAssignment_2 )
            {
             before(grammarAccess.getXMIImportAccess().getImportURIAssignment_2()); 
            // InternalConfiguration.g:1552:1: ( rule__XMIImport__ImportURIAssignment_2 )
            // InternalConfiguration.g:1552:2: rule__XMIImport__ImportURIAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__XMIImport__ImportURIAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getXMIImportAccess().getImportURIAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XMIImport__Group__2__Impl"


    // $ANTLR start "rule__SMLImport__Group__0"
    // InternalConfiguration.g:1568:1: rule__SMLImport__Group__0 : rule__SMLImport__Group__0__Impl rule__SMLImport__Group__1 ;
    public final void rule__SMLImport__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1572:1: ( rule__SMLImport__Group__0__Impl rule__SMLImport__Group__1 )
            // InternalConfiguration.g:1573:2: rule__SMLImport__Group__0__Impl rule__SMLImport__Group__1
            {
            pushFollow(FOLLOW_25);
            rule__SMLImport__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SMLImport__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SMLImport__Group__0"


    // $ANTLR start "rule__SMLImport__Group__0__Impl"
    // InternalConfiguration.g:1580:1: rule__SMLImport__Group__0__Impl : ( 'import' ) ;
    public final void rule__SMLImport__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1584:1: ( ( 'import' ) )
            // InternalConfiguration.g:1585:1: ( 'import' )
            {
            // InternalConfiguration.g:1585:1: ( 'import' )
            // InternalConfiguration.g:1586:1: 'import'
            {
             before(grammarAccess.getSMLImportAccess().getImportKeyword_0()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getSMLImportAccess().getImportKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SMLImport__Group__0__Impl"


    // $ANTLR start "rule__SMLImport__Group__1"
    // InternalConfiguration.g:1599:1: rule__SMLImport__Group__1 : rule__SMLImport__Group__1__Impl ;
    public final void rule__SMLImport__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1603:1: ( rule__SMLImport__Group__1__Impl )
            // InternalConfiguration.g:1604:2: rule__SMLImport__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SMLImport__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SMLImport__Group__1"


    // $ANTLR start "rule__SMLImport__Group__1__Impl"
    // InternalConfiguration.g:1610:1: rule__SMLImport__Group__1__Impl : ( ( rule__SMLImport__ImportURIAssignment_1 ) ) ;
    public final void rule__SMLImport__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1614:1: ( ( ( rule__SMLImport__ImportURIAssignment_1 ) ) )
            // InternalConfiguration.g:1615:1: ( ( rule__SMLImport__ImportURIAssignment_1 ) )
            {
            // InternalConfiguration.g:1615:1: ( ( rule__SMLImport__ImportURIAssignment_1 ) )
            // InternalConfiguration.g:1616:1: ( rule__SMLImport__ImportURIAssignment_1 )
            {
             before(grammarAccess.getSMLImportAccess().getImportURIAssignment_1()); 
            // InternalConfiguration.g:1617:1: ( rule__SMLImport__ImportURIAssignment_1 )
            // InternalConfiguration.g:1617:2: rule__SMLImport__ImportURIAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__SMLImport__ImportURIAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getSMLImportAccess().getImportURIAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SMLImport__Group__1__Impl"


    // $ANTLR start "rule__Configuration__ImportedResourcesAssignment_0"
    // InternalConfiguration.g:1632:1: rule__Configuration__ImportedResourcesAssignment_0 : ( ruleSMLImport ) ;
    public final void rule__Configuration__ImportedResourcesAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1636:1: ( ( ruleSMLImport ) )
            // InternalConfiguration.g:1637:1: ( ruleSMLImport )
            {
            // InternalConfiguration.g:1637:1: ( ruleSMLImport )
            // InternalConfiguration.g:1638:1: ruleSMLImport
            {
             before(grammarAccess.getConfigurationAccess().getImportedResourcesSMLImportParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleSMLImport();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getImportedResourcesSMLImportParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__ImportedResourcesAssignment_0"


    // $ANTLR start "rule__Configuration__SpecificationAssignment_3"
    // InternalConfiguration.g:1647:1: rule__Configuration__SpecificationAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__Configuration__SpecificationAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1651:1: ( ( ( RULE_ID ) ) )
            // InternalConfiguration.g:1652:1: ( ( RULE_ID ) )
            {
            // InternalConfiguration.g:1652:1: ( ( RULE_ID ) )
            // InternalConfiguration.g:1653:1: ( RULE_ID )
            {
             before(grammarAccess.getConfigurationAccess().getSpecificationSpecificationCrossReference_3_0()); 
            // InternalConfiguration.g:1654:1: ( RULE_ID )
            // InternalConfiguration.g:1655:1: RULE_ID
            {
             before(grammarAccess.getConfigurationAccess().getSpecificationSpecificationIDTerminalRuleCall_3_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getSpecificationSpecificationIDTerminalRuleCall_3_0_1()); 

            }

             after(grammarAccess.getConfigurationAccess().getSpecificationSpecificationCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__SpecificationAssignment_3"


    // $ANTLR start "rule__Configuration__IgnoredCollaborationsAssignment_4_2"
    // InternalConfiguration.g:1666:1: rule__Configuration__IgnoredCollaborationsAssignment_4_2 : ( ( ruleFQN ) ) ;
    public final void rule__Configuration__IgnoredCollaborationsAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1670:1: ( ( ( ruleFQN ) ) )
            // InternalConfiguration.g:1671:1: ( ( ruleFQN ) )
            {
            // InternalConfiguration.g:1671:1: ( ( ruleFQN ) )
            // InternalConfiguration.g:1672:1: ( ruleFQN )
            {
             before(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsCollaborationCrossReference_4_2_0()); 
            // InternalConfiguration.g:1673:1: ( ruleFQN )
            // InternalConfiguration.g:1674:1: ruleFQN
            {
             before(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsCollaborationFQNParserRuleCall_4_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsCollaborationFQNParserRuleCall_4_2_0_1()); 

            }

             after(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsCollaborationCrossReference_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__IgnoredCollaborationsAssignment_4_2"


    // $ANTLR start "rule__Configuration__IgnoredCollaborationsAssignment_4_3_1"
    // InternalConfiguration.g:1685:1: rule__Configuration__IgnoredCollaborationsAssignment_4_3_1 : ( ( ruleFQN ) ) ;
    public final void rule__Configuration__IgnoredCollaborationsAssignment_4_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1689:1: ( ( ( ruleFQN ) ) )
            // InternalConfiguration.g:1690:1: ( ( ruleFQN ) )
            {
            // InternalConfiguration.g:1690:1: ( ( ruleFQN ) )
            // InternalConfiguration.g:1691:1: ( ruleFQN )
            {
             before(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsCollaborationCrossReference_4_3_1_0()); 
            // InternalConfiguration.g:1692:1: ( ruleFQN )
            // InternalConfiguration.g:1693:1: ruleFQN
            {
             before(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsCollaborationFQNParserRuleCall_4_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsCollaborationFQNParserRuleCall_4_3_1_0_1()); 

            }

             after(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsCollaborationCrossReference_4_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__IgnoredCollaborationsAssignment_4_3_1"


    // $ANTLR start "rule__Configuration__AuxiliaryCollaborationsAssignment_5_2"
    // InternalConfiguration.g:1704:1: rule__Configuration__AuxiliaryCollaborationsAssignment_5_2 : ( ( ruleFQN ) ) ;
    public final void rule__Configuration__AuxiliaryCollaborationsAssignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1708:1: ( ( ( ruleFQN ) ) )
            // InternalConfiguration.g:1709:1: ( ( ruleFQN ) )
            {
            // InternalConfiguration.g:1709:1: ( ( ruleFQN ) )
            // InternalConfiguration.g:1710:1: ( ruleFQN )
            {
             before(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsCollaborationCrossReference_5_2_0()); 
            // InternalConfiguration.g:1711:1: ( ruleFQN )
            // InternalConfiguration.g:1712:1: ruleFQN
            {
             before(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsCollaborationFQNParserRuleCall_5_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsCollaborationFQNParserRuleCall_5_2_0_1()); 

            }

             after(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsCollaborationCrossReference_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__AuxiliaryCollaborationsAssignment_5_2"


    // $ANTLR start "rule__Configuration__AuxiliaryCollaborationsAssignment_5_3_1"
    // InternalConfiguration.g:1723:1: rule__Configuration__AuxiliaryCollaborationsAssignment_5_3_1 : ( ( ruleFQN ) ) ;
    public final void rule__Configuration__AuxiliaryCollaborationsAssignment_5_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1727:1: ( ( ( ruleFQN ) ) )
            // InternalConfiguration.g:1728:1: ( ( ruleFQN ) )
            {
            // InternalConfiguration.g:1728:1: ( ( ruleFQN ) )
            // InternalConfiguration.g:1729:1: ( ruleFQN )
            {
             before(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsCollaborationCrossReference_5_3_1_0()); 
            // InternalConfiguration.g:1730:1: ( ruleFQN )
            // InternalConfiguration.g:1731:1: ruleFQN
            {
             before(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsCollaborationFQNParserRuleCall_5_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsCollaborationFQNParserRuleCall_5_3_1_0_1()); 

            }

             after(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsCollaborationCrossReference_5_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__AuxiliaryCollaborationsAssignment_5_3_1"


    // $ANTLR start "rule__Configuration__InstanceModelImportsAssignment_6"
    // InternalConfiguration.g:1742:1: rule__Configuration__InstanceModelImportsAssignment_6 : ( ruleXMIImport ) ;
    public final void rule__Configuration__InstanceModelImportsAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1746:1: ( ( ruleXMIImport ) )
            // InternalConfiguration.g:1747:1: ( ruleXMIImport )
            {
            // InternalConfiguration.g:1747:1: ( ruleXMIImport )
            // InternalConfiguration.g:1748:1: ruleXMIImport
            {
             before(grammarAccess.getConfigurationAccess().getInstanceModelImportsXMIImportParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleXMIImport();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getInstanceModelImportsXMIImportParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__InstanceModelImportsAssignment_6"


    // $ANTLR start "rule__Configuration__StaticRoleBindingsAssignment_7"
    // InternalConfiguration.g:1757:1: rule__Configuration__StaticRoleBindingsAssignment_7 : ( ruleRoleBindings ) ;
    public final void rule__Configuration__StaticRoleBindingsAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1761:1: ( ( ruleRoleBindings ) )
            // InternalConfiguration.g:1762:1: ( ruleRoleBindings )
            {
            // InternalConfiguration.g:1762:1: ( ruleRoleBindings )
            // InternalConfiguration.g:1763:1: ruleRoleBindings
            {
             before(grammarAccess.getConfigurationAccess().getStaticRoleBindingsRoleBindingsParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleRoleBindings();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getStaticRoleBindingsRoleBindingsParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__StaticRoleBindingsAssignment_7"


    // $ANTLR start "rule__RoleBindings__CollaborationAssignment_3"
    // InternalConfiguration.g:1772:1: rule__RoleBindings__CollaborationAssignment_3 : ( ( ruleFQN ) ) ;
    public final void rule__RoleBindings__CollaborationAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1776:1: ( ( ( ruleFQN ) ) )
            // InternalConfiguration.g:1777:1: ( ( ruleFQN ) )
            {
            // InternalConfiguration.g:1777:1: ( ( ruleFQN ) )
            // InternalConfiguration.g:1778:1: ( ruleFQN )
            {
             before(grammarAccess.getRoleBindingsAccess().getCollaborationCollaborationCrossReference_3_0()); 
            // InternalConfiguration.g:1779:1: ( ruleFQN )
            // InternalConfiguration.g:1780:1: ruleFQN
            {
             before(grammarAccess.getRoleBindingsAccess().getCollaborationCollaborationFQNParserRuleCall_3_0_1()); 
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getRoleBindingsAccess().getCollaborationCollaborationFQNParserRuleCall_3_0_1()); 

            }

             after(grammarAccess.getRoleBindingsAccess().getCollaborationCollaborationCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__CollaborationAssignment_3"


    // $ANTLR start "rule__RoleBindings__BindingsAssignment_5"
    // InternalConfiguration.g:1791:1: rule__RoleBindings__BindingsAssignment_5 : ( ruleRoleAssignment ) ;
    public final void rule__RoleBindings__BindingsAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1795:1: ( ( ruleRoleAssignment ) )
            // InternalConfiguration.g:1796:1: ( ruleRoleAssignment )
            {
            // InternalConfiguration.g:1796:1: ( ruleRoleAssignment )
            // InternalConfiguration.g:1797:1: ruleRoleAssignment
            {
             before(grammarAccess.getRoleBindingsAccess().getBindingsRoleAssignmentParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleRoleAssignment();

            state._fsp--;

             after(grammarAccess.getRoleBindingsAccess().getBindingsRoleAssignmentParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__BindingsAssignment_5"


    // $ANTLR start "rule__RoleAssignment__ObjectAssignment_1"
    // InternalConfiguration.g:1806:1: rule__RoleAssignment__ObjectAssignment_1 : ( ( ruleFQN ) ) ;
    public final void rule__RoleAssignment__ObjectAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1810:1: ( ( ( ruleFQN ) ) )
            // InternalConfiguration.g:1811:1: ( ( ruleFQN ) )
            {
            // InternalConfiguration.g:1811:1: ( ( ruleFQN ) )
            // InternalConfiguration.g:1812:1: ( ruleFQN )
            {
             before(grammarAccess.getRoleAssignmentAccess().getObjectEObjectCrossReference_1_0()); 
            // InternalConfiguration.g:1813:1: ( ruleFQN )
            // InternalConfiguration.g:1814:1: ruleFQN
            {
             before(grammarAccess.getRoleAssignmentAccess().getObjectEObjectFQNParserRuleCall_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getRoleAssignmentAccess().getObjectEObjectFQNParserRuleCall_1_0_1()); 

            }

             after(grammarAccess.getRoleAssignmentAccess().getObjectEObjectCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__ObjectAssignment_1"


    // $ANTLR start "rule__RoleAssignment__RoleAssignment_4"
    // InternalConfiguration.g:1825:1: rule__RoleAssignment__RoleAssignment_4 : ( ( RULE_ID ) ) ;
    public final void rule__RoleAssignment__RoleAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1829:1: ( ( ( RULE_ID ) ) )
            // InternalConfiguration.g:1830:1: ( ( RULE_ID ) )
            {
            // InternalConfiguration.g:1830:1: ( ( RULE_ID ) )
            // InternalConfiguration.g:1831:1: ( RULE_ID )
            {
             before(grammarAccess.getRoleAssignmentAccess().getRoleRoleCrossReference_4_0()); 
            // InternalConfiguration.g:1832:1: ( RULE_ID )
            // InternalConfiguration.g:1833:1: RULE_ID
            {
             before(grammarAccess.getRoleAssignmentAccess().getRoleRoleIDTerminalRuleCall_4_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getRoleAssignmentAccess().getRoleRoleIDTerminalRuleCall_4_0_1()); 

            }

             after(grammarAccess.getRoleAssignmentAccess().getRoleRoleCrossReference_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__RoleAssignment_4"


    // $ANTLR start "rule__XMIImport__ImportURIAssignment_2"
    // InternalConfiguration.g:1844:1: rule__XMIImport__ImportURIAssignment_2 : ( RULE_STRING ) ;
    public final void rule__XMIImport__ImportURIAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1848:1: ( ( RULE_STRING ) )
            // InternalConfiguration.g:1849:1: ( RULE_STRING )
            {
            // InternalConfiguration.g:1849:1: ( RULE_STRING )
            // InternalConfiguration.g:1850:1: RULE_STRING
            {
             before(grammarAccess.getXMIImportAccess().getImportURISTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getXMIImportAccess().getImportURISTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XMIImport__ImportURIAssignment_2"


    // $ANTLR start "rule__SMLImport__ImportURIAssignment_1"
    // InternalConfiguration.g:1859:1: rule__SMLImport__ImportURIAssignment_1 : ( RULE_STRING ) ;
    public final void rule__SMLImport__ImportURIAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalConfiguration.g:1863:1: ( ( RULE_STRING ) )
            // InternalConfiguration.g:1864:1: ( RULE_STRING )
            {
            // InternalConfiguration.g:1864:1: ( RULE_STRING )
            // InternalConfiguration.g:1865:1: RULE_STRING
            {
             before(grammarAccess.getSMLImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getSMLImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SMLImport__ImportURIAssignment_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000008024000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000800800L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000008024002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000800802L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000002400000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000000000020L});

}