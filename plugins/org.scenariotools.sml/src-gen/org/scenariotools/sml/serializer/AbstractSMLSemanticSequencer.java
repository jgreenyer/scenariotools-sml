/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.serializer;

import com.google.inject.Inject;
import java.util.Set;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Parameter;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.serializer.ISerializationContext;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;
import org.scenariotools.sml.Alternative;
import org.scenariotools.sml.Case;
import org.scenariotools.sml.ChannelOptions;
import org.scenariotools.sml.Collaboration;
import org.scenariotools.sml.Condition;
import org.scenariotools.sml.ConditionExpression;
import org.scenariotools.sml.ConstraintBlock;
import org.scenariotools.sml.EnumRanges;
import org.scenariotools.sml.EventParameterRanges;
import org.scenariotools.sml.FeatureAccessBindingExpression;
import org.scenariotools.sml.IntegerRanges;
import org.scenariotools.sml.Interaction;
import org.scenariotools.sml.InterruptCondition;
import org.scenariotools.sml.Loop;
import org.scenariotools.sml.Message;
import org.scenariotools.sml.MessageChannel;
import org.scenariotools.sml.ModalMessage;
import org.scenariotools.sml.Parallel;
import org.scenariotools.sml.ParameterBinding;
import org.scenariotools.sml.RangesForParameter;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.RoleBindingConstraint;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.SmlPackage;
import org.scenariotools.sml.Specification;
import org.scenariotools.sml.StringRanges;
import org.scenariotools.sml.TimedInterruptCondition;
import org.scenariotools.sml.TimedViolationCondition;
import org.scenariotools.sml.TimedWaitCondition;
import org.scenariotools.sml.ValueParameterExpression;
import org.scenariotools.sml.VariableBindingParameterExpression;
import org.scenariotools.sml.VariableFragment;
import org.scenariotools.sml.ViolationCondition;
import org.scenariotools.sml.WaitCondition;
import org.scenariotools.sml.WildcardParameterExpression;
import org.scenariotools.sml.collaboration.serializer.CollaborationSemanticSequencer;
import org.scenariotools.sml.expressions.scenarioExpressions.BinaryOperationExpression;
import org.scenariotools.sml.expressions.scenarioExpressions.BooleanValue;
import org.scenariotools.sml.expressions.scenarioExpressions.Clock;
import org.scenariotools.sml.expressions.scenarioExpressions.ClockAssignment;
import org.scenariotools.sml.expressions.scenarioExpressions.ClockDeclaration;
import org.scenariotools.sml.expressions.scenarioExpressions.CollectionAccess;
import org.scenariotools.sml.expressions.scenarioExpressions.Document;
import org.scenariotools.sml.expressions.scenarioExpressions.EnumValue;
import org.scenariotools.sml.expressions.scenarioExpressions.ExpressionRegion;
import org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess;
import org.scenariotools.sml.expressions.scenarioExpressions.Import;
import org.scenariotools.sml.expressions.scenarioExpressions.IntegerValue;
import org.scenariotools.sml.expressions.scenarioExpressions.NullValue;
import org.scenariotools.sml.expressions.scenarioExpressions.OperationValue;
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage;
import org.scenariotools.sml.expressions.scenarioExpressions.StringValue;
import org.scenariotools.sml.expressions.scenarioExpressions.StructuralFeatureValue;
import org.scenariotools.sml.expressions.scenarioExpressions.TimedExpression;
import org.scenariotools.sml.expressions.scenarioExpressions.TypedVariableDeclaration;
import org.scenariotools.sml.expressions.scenarioExpressions.UnaryOperationExpression;
import org.scenariotools.sml.expressions.scenarioExpressions.VariableAssignment;
import org.scenariotools.sml.expressions.scenarioExpressions.VariableDeclaration;
import org.scenariotools.sml.expressions.scenarioExpressions.VariableValue;
import org.scenariotools.sml.services.SMLGrammarAccess;

@SuppressWarnings("all")
public abstract class AbstractSMLSemanticSequencer extends CollaborationSemanticSequencer {

	@Inject
	private SMLGrammarAccess grammarAccess;
	
	@Override
	public void sequence(ISerializationContext context, EObject semanticObject) {
		EPackage epackage = semanticObject.eClass().getEPackage();
		ParserRule rule = context.getParserRule();
		Action action = context.getAssignedAction();
		Set<Parameter> parameters = context.getEnabledBooleanParameters();
		if (epackage == ScenarioExpressionsPackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case ScenarioExpressionsPackage.BINARY_OPERATION_EXPRESSION:
				sequence_AdditionExpression_ConjunctionExpression_DisjunctionExpression_ImplicationExpression_MultiplicationExpression_RelationExpression(context, (BinaryOperationExpression) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.BOOLEAN_VALUE:
				sequence_BooleanValue(context, (BooleanValue) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.CLOCK:
				sequence_Clock(context, (Clock) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.CLOCK_ASSIGNMENT:
				sequence_ClockAssignment(context, (ClockAssignment) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.CLOCK_DECLARATION:
				sequence_ClockDeclaration(context, (ClockDeclaration) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.COLLECTION_ACCESS:
				sequence_CollectionAccess(context, (CollectionAccess) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.DOCUMENT:
				sequence_Document(context, (Document) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.ENUM_VALUE:
				sequence_EnumValue(context, (EnumValue) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.EXPRESSION_REGION:
				sequence_ExpressionRegion(context, (ExpressionRegion) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.FEATURE_ACCESS:
				sequence_FeatureAccess(context, (FeatureAccess) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.IMPORT:
				sequence_Import(context, (Import) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.INTEGER_VALUE:
				sequence_IntegerValue(context, (IntegerValue) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.NULL_VALUE:
				sequence_NullValue(context, (NullValue) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.OPERATION_VALUE:
				sequence_OperationValue(context, (OperationValue) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.STRING_VALUE:
				sequence_StringValue(context, (StringValue) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.STRUCTURAL_FEATURE_VALUE:
				sequence_StructuralFeatureValue(context, (StructuralFeatureValue) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.TIMED_EXPRESSION:
				sequence_TimedExpression(context, (TimedExpression) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.TYPED_VARIABLE_DECLARATION:
				sequence_TypedVariableDeclaration(context, (TypedVariableDeclaration) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.UNARY_OPERATION_EXPRESSION:
				sequence_NegatedExpression(context, (UnaryOperationExpression) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.VARIABLE_ASSIGNMENT:
				sequence_VariableAssignment(context, (VariableAssignment) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.VARIABLE_DECLARATION:
				sequence_VariableDeclaration(context, (VariableDeclaration) semanticObject); 
				return; 
			case ScenarioExpressionsPackage.VARIABLE_VALUE:
				sequence_VariableValue(context, (VariableValue) semanticObject); 
				return; 
			}
		else if (epackage == SmlPackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case SmlPackage.ALTERNATIVE:
				sequence_Alternative(context, (Alternative) semanticObject); 
				return; 
			case SmlPackage.CASE:
				sequence_Case(context, (Case) semanticObject); 
				return; 
			case SmlPackage.CHANNEL_OPTIONS:
				sequence_ChannelOptions(context, (ChannelOptions) semanticObject); 
				return; 
			case SmlPackage.COLLABORATION:
				if (rule == grammarAccess.getCollaborationRule()) {
					sequence_Collaboration(context, (Collaboration) semanticObject); 
					return; 
				}
				else if (rule == grammarAccess.getNestedCollaborationRule()) {
					sequence_NestedCollaboration(context, (Collaboration) semanticObject); 
					return; 
				}
				else break;
			case SmlPackage.CONDITION:
				sequence_Condition(context, (Condition) semanticObject); 
				return; 
			case SmlPackage.CONDITION_EXPRESSION:
				sequence_ConditionExpression(context, (ConditionExpression) semanticObject); 
				return; 
			case SmlPackage.CONSTRAINT_BLOCK:
				sequence_ConstraintBlock(context, (ConstraintBlock) semanticObject); 
				return; 
			case SmlPackage.ENUM_RANGES:
				sequence_EnumRanges(context, (EnumRanges) semanticObject); 
				return; 
			case SmlPackage.EVENT_PARAMETER_RANGES:
				sequence_EventParameterRanges(context, (EventParameterRanges) semanticObject); 
				return; 
			case SmlPackage.FEATURE_ACCESS_BINDING_EXPRESSION:
				sequence_FeatureAccessBindingExpression(context, (FeatureAccessBindingExpression) semanticObject); 
				return; 
			case SmlPackage.INTEGER_RANGES:
				sequence_IntegerRanges(context, (IntegerRanges) semanticObject); 
				return; 
			case SmlPackage.INTERACTION:
				sequence_Interaction(context, (Interaction) semanticObject); 
				return; 
			case SmlPackage.INTERRUPT_CONDITION:
				sequence_InterruptCondition(context, (InterruptCondition) semanticObject); 
				return; 
			case SmlPackage.LOOP:
				sequence_Loop(context, (Loop) semanticObject); 
				return; 
			case SmlPackage.MESSAGE:
				sequence_ConstraintMessage(context, (Message) semanticObject); 
				return; 
			case SmlPackage.MESSAGE_CHANNEL:
				sequence_MessageChannel(context, (MessageChannel) semanticObject); 
				return; 
			case SmlPackage.MODAL_MESSAGE:
				sequence_ModalMessage(context, (ModalMessage) semanticObject); 
				return; 
			case SmlPackage.PARALLEL:
				sequence_Parallel(context, (Parallel) semanticObject); 
				return; 
			case SmlPackage.PARAMETER_BINDING:
				sequence_ParameterBinding(context, (ParameterBinding) semanticObject); 
				return; 
			case SmlPackage.RANGES_FOR_PARAMETER:
				sequence_RangesForParameter(context, (RangesForParameter) semanticObject); 
				return; 
			case SmlPackage.ROLE:
				sequence_Role(context, (Role) semanticObject); 
				return; 
			case SmlPackage.ROLE_BINDING_CONSTRAINT:
				sequence_RoleBindingConstraint(context, (RoleBindingConstraint) semanticObject); 
				return; 
			case SmlPackage.SCENARIO:
				sequence_Scenario(context, (Scenario) semanticObject); 
				return; 
			case SmlPackage.SPECIFICATION:
				sequence_Specification(context, (Specification) semanticObject); 
				return; 
			case SmlPackage.STRING_RANGES:
				sequence_StringRanges(context, (StringRanges) semanticObject); 
				return; 
			case SmlPackage.TIMED_INTERRUPT_CONDITION:
				sequence_TimedInterruptCondition(context, (TimedInterruptCondition) semanticObject); 
				return; 
			case SmlPackage.TIMED_VIOLATION_CONDITION:
				sequence_TimedViolationCondition(context, (TimedViolationCondition) semanticObject); 
				return; 
			case SmlPackage.TIMED_WAIT_CONDITION:
				sequence_TimedWaitCondition(context, (TimedWaitCondition) semanticObject); 
				return; 
			case SmlPackage.VALUE_PARAMETER_EXPRESSION:
				sequence_ValueParameterExpression(context, (ValueParameterExpression) semanticObject); 
				return; 
			case SmlPackage.VARIABLE_BINDING_PARAMETER_EXPRESSION:
				sequence_VariableBindingParameterExpression(context, (VariableBindingParameterExpression) semanticObject); 
				return; 
			case SmlPackage.VARIABLE_FRAGMENT:
				sequence_VariableFragment(context, (VariableFragment) semanticObject); 
				return; 
			case SmlPackage.VIOLATION_CONDITION:
				sequence_ViolationCondition(context, (ViolationCondition) semanticObject); 
				return; 
			case SmlPackage.WAIT_CONDITION:
				sequence_WaitCondition(context, (WaitCondition) semanticObject); 
				return; 
			case SmlPackage.WILDCARD_PARAMETER_EXPRESSION:
				sequence_WildcardParameterExpression(context, (WildcardParameterExpression) semanticObject); 
				return; 
			}
		if (errorAcceptor != null)
			errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Contexts:
	 *     ChannelOptions returns ChannelOptions
	 *
	 * Constraint:
	 *     (allMessagesRequireLinks?='all messages require links'? messageChannels+=MessageChannel*)
	 */
	protected void sequence_ChannelOptions(ISerializationContext context, ChannelOptions semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AbstractRanges returns EnumRanges
	 *     EnumRanges returns EnumRanges
	 *
	 * Constraint:
	 *     (values+=[EEnumLiteral|FQNENUM] values+=[EEnumLiteral|FQNENUM]*)
	 */
	protected void sequence_EnumRanges(ISerializationContext context, EnumRanges semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     EventParameterRanges returns EventParameterRanges
	 *
	 * Constraint:
	 *     (event=[ETypedElement|FQN] rangesForParameter+=RangesForParameter rangesForParameter+=RangesForParameter*)
	 */
	protected void sequence_EventParameterRanges(ISerializationContext context, EventParameterRanges semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AbstractRanges returns IntegerRanges
	 *     IntegerRanges returns IntegerRanges
	 *
	 * Constraint:
	 *     (
	 *         ((min=INT | min=SIGNEDINT) (max=INT | max=SIGNEDINT) ((values+=INT | values+=SIGNEDINT) values+=SIGNEDINT? (values+=INT? values+=SIGNEDINT?)*)?) | 
	 *         ((values+=INT | values+=SIGNEDINT) values+=SIGNEDINT? (values+=INT? values+=SIGNEDINT?)*)
	 *     )
	 */
	protected void sequence_IntegerRanges(ISerializationContext context, IntegerRanges semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     MessageChannel returns MessageChannel
	 *
	 * Constraint:
	 *     (event=[ETypedElement|FQN] channelFeature=[EStructuralFeature|FQN])
	 */
	protected void sequence_MessageChannel(ISerializationContext context, MessageChannel semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, SmlPackage.Literals.MESSAGE_CHANNEL__EVENT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SmlPackage.Literals.MESSAGE_CHANNEL__EVENT));
			if (transientValues.isValueTransient(semanticObject, SmlPackage.Literals.MESSAGE_CHANNEL__CHANNEL_FEATURE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SmlPackage.Literals.MESSAGE_CHANNEL__CHANNEL_FEATURE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getMessageChannelAccess().getEventETypedElementFQNParserRuleCall_0_0_1(), semanticObject.eGet(SmlPackage.Literals.MESSAGE_CHANNEL__EVENT, false));
		feeder.accept(grammarAccess.getMessageChannelAccess().getChannelFeatureEStructuralFeatureFQNParserRuleCall_2_0_1(), semanticObject.eGet(SmlPackage.Literals.MESSAGE_CHANNEL__CHANNEL_FEATURE, false));
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     NestedCollaboration returns Collaboration
	 *
	 * Constraint:
	 *     (name=ID roles+=Role* scenarios+=Scenario*)
	 */
	protected void sequence_NestedCollaboration(ISerializationContext context, Collaboration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     RangesForParameter returns RangesForParameter
	 *
	 * Constraint:
	 *     (parameter=[ETypedElement|ID] ranges=AbstractRanges)
	 */
	protected void sequence_RangesForParameter(ISerializationContext context, RangesForParameter semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, SmlPackage.Literals.RANGES_FOR_PARAMETER__PARAMETER) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SmlPackage.Literals.RANGES_FOR_PARAMETER__PARAMETER));
			if (transientValues.isValueTransient(semanticObject, SmlPackage.Literals.RANGES_FOR_PARAMETER__RANGES) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SmlPackage.Literals.RANGES_FOR_PARAMETER__RANGES));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getRangesForParameterAccess().getParameterETypedElementIDTerminalRuleCall_0_0_1(), semanticObject.eGet(SmlPackage.Literals.RANGES_FOR_PARAMETER__PARAMETER, false));
		feeder.accept(grammarAccess.getRangesForParameterAccess().getRangesAbstractRangesParserRuleCall_2_0(), semanticObject.getRanges());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Specification returns Specification
	 *
	 * Constraint:
	 *     (
	 *         imports+=Import* 
	 *         name=ID 
	 *         domains+=[EPackage|FQN]* 
	 *         contexts+=[EPackage|FQN]* 
	 *         controllableEClasses+=[EClass|ID]* 
	 *         channelOptions=ChannelOptions 
	 *         (nonSpontaneousOperations+=[ETypedElement|FQN] nonSpontaneousOperations+=[ETypedElement|FQN]*)? 
	 *         (eventParameterRanges+=EventParameterRanges eventParameterRanges+=EventParameterRanges*)? 
	 *         (containedCollaborations+=NestedCollaboration | includedCollaborations+=[Collaboration|ID])*
	 *     )
	 */
	protected void sequence_Specification(ISerializationContext context, Specification semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AbstractRanges returns StringRanges
	 *     StringRanges returns StringRanges
	 *
	 * Constraint:
	 *     (values+=STRING values+=STRING*)
	 */
	protected void sequence_StringRanges(ISerializationContext context, StringRanges semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
}
