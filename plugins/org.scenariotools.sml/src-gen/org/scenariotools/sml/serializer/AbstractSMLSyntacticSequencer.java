/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.serializer;

import com.google.inject.Inject;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AbstractElementAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.GroupAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.TokenAlias;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynNavigable;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynTransition;
import org.eclipse.xtext.serializer.sequencer.AbstractSyntacticSequencer;
import org.scenariotools.sml.services.SMLGrammarAccess;

@SuppressWarnings("all")
public abstract class AbstractSMLSyntacticSequencer extends AbstractSyntacticSequencer {

	protected SMLGrammarAccess grammarAccess;
	protected AbstractElementAlias match_BasicExpression_LeftParenthesisKeyword_1_0_a;
	protected AbstractElementAlias match_BasicExpression_LeftParenthesisKeyword_1_0_p;
	protected AbstractElementAlias match_ChannelOptions___ChannelsKeyword_1_0_LeftCurlyBracketKeyword_1_1_RightCurlyBracketKeyword_1_4__q;
	protected AbstractElementAlias match_ModalMessage___LeftParenthesisKeyword_8_0_RightParenthesisKeyword_8_2__q;
	protected AbstractElementAlias match_Scenario___BindingsKeyword_6_0_LeftSquareBracketKeyword_6_1_RightSquareBracketKeyword_6_3__q;
	protected AbstractElementAlias match_Specification___ControllableKeyword_6_0_LeftCurlyBracketKeyword_6_1_RightCurlyBracketKeyword_6_3__q;
	protected AbstractElementAlias match_Specification___NonSpontaneousEventsKeyword_8_0_LeftCurlyBracketKeyword_8_1_RightCurlyBracketKeyword_8_3__q;
	protected AbstractElementAlias match_Specification___ParameterRangesKeyword_9_0_LeftCurlyBracketKeyword_9_1_RightCurlyBracketKeyword_9_3__q;
	
	@Inject
	protected void init(IGrammarAccess access) {
		grammarAccess = (SMLGrammarAccess) access;
		match_BasicExpression_LeftParenthesisKeyword_1_0_a = new TokenAlias(true, true, grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0());
		match_BasicExpression_LeftParenthesisKeyword_1_0_p = new TokenAlias(true, false, grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0());
		match_ChannelOptions___ChannelsKeyword_1_0_LeftCurlyBracketKeyword_1_1_RightCurlyBracketKeyword_1_4__q = new GroupAlias(false, true, new TokenAlias(false, false, grammarAccess.getChannelOptionsAccess().getChannelsKeyword_1_0()), new TokenAlias(false, false, grammarAccess.getChannelOptionsAccess().getLeftCurlyBracketKeyword_1_1()), new TokenAlias(false, false, grammarAccess.getChannelOptionsAccess().getRightCurlyBracketKeyword_1_4()));
		match_ModalMessage___LeftParenthesisKeyword_8_0_RightParenthesisKeyword_8_2__q = new GroupAlias(false, true, new TokenAlias(false, false, grammarAccess.getModalMessageAccess().getLeftParenthesisKeyword_8_0()), new TokenAlias(false, false, grammarAccess.getModalMessageAccess().getRightParenthesisKeyword_8_2()));
		match_Scenario___BindingsKeyword_6_0_LeftSquareBracketKeyword_6_1_RightSquareBracketKeyword_6_3__q = new GroupAlias(false, true, new TokenAlias(false, false, grammarAccess.getScenarioAccess().getBindingsKeyword_6_0()), new TokenAlias(false, false, grammarAccess.getScenarioAccess().getLeftSquareBracketKeyword_6_1()), new TokenAlias(false, false, grammarAccess.getScenarioAccess().getRightSquareBracketKeyword_6_3()));
		match_Specification___ControllableKeyword_6_0_LeftCurlyBracketKeyword_6_1_RightCurlyBracketKeyword_6_3__q = new GroupAlias(false, true, new TokenAlias(false, false, grammarAccess.getSpecificationAccess().getControllableKeyword_6_0()), new TokenAlias(false, false, grammarAccess.getSpecificationAccess().getLeftCurlyBracketKeyword_6_1()), new TokenAlias(false, false, grammarAccess.getSpecificationAccess().getRightCurlyBracketKeyword_6_3()));
		match_Specification___NonSpontaneousEventsKeyword_8_0_LeftCurlyBracketKeyword_8_1_RightCurlyBracketKeyword_8_3__q = new GroupAlias(false, true, new TokenAlias(false, false, grammarAccess.getSpecificationAccess().getNonSpontaneousEventsKeyword_8_0()), new TokenAlias(false, false, grammarAccess.getSpecificationAccess().getLeftCurlyBracketKeyword_8_1()), new TokenAlias(false, false, grammarAccess.getSpecificationAccess().getRightCurlyBracketKeyword_8_3()));
		match_Specification___ParameterRangesKeyword_9_0_LeftCurlyBracketKeyword_9_1_RightCurlyBracketKeyword_9_3__q = new GroupAlias(false, true, new TokenAlias(false, false, grammarAccess.getSpecificationAccess().getParameterRangesKeyword_9_0()), new TokenAlias(false, false, grammarAccess.getSpecificationAccess().getLeftCurlyBracketKeyword_9_1()), new TokenAlias(false, false, grammarAccess.getSpecificationAccess().getRightCurlyBracketKeyword_9_3()));
	}
	
	@Override
	protected String getUnassignedRuleCallToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		return "";
	}
	
	
	@Override
	protected void emitUnassignedTokens(EObject semanticObject, ISynTransition transition, INode fromNode, INode toNode) {
		if (transition.getAmbiguousSyntaxes().isEmpty()) return;
		List<INode> transitionNodes = collectNodes(fromNode, toNode);
		for (AbstractElementAlias syntax : transition.getAmbiguousSyntaxes()) {
			List<INode> syntaxNodes = getNodesFor(transitionNodes, syntax);
			if (match_BasicExpression_LeftParenthesisKeyword_1_0_a.equals(syntax))
				emit_BasicExpression_LeftParenthesisKeyword_1_0_a(semanticObject, getLastNavigableState(), syntaxNodes);
			else if (match_BasicExpression_LeftParenthesisKeyword_1_0_p.equals(syntax))
				emit_BasicExpression_LeftParenthesisKeyword_1_0_p(semanticObject, getLastNavigableState(), syntaxNodes);
			else if (match_ChannelOptions___ChannelsKeyword_1_0_LeftCurlyBracketKeyword_1_1_RightCurlyBracketKeyword_1_4__q.equals(syntax))
				emit_ChannelOptions___ChannelsKeyword_1_0_LeftCurlyBracketKeyword_1_1_RightCurlyBracketKeyword_1_4__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if (match_ModalMessage___LeftParenthesisKeyword_8_0_RightParenthesisKeyword_8_2__q.equals(syntax))
				emit_ModalMessage___LeftParenthesisKeyword_8_0_RightParenthesisKeyword_8_2__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if (match_Scenario___BindingsKeyword_6_0_LeftSquareBracketKeyword_6_1_RightSquareBracketKeyword_6_3__q.equals(syntax))
				emit_Scenario___BindingsKeyword_6_0_LeftSquareBracketKeyword_6_1_RightSquareBracketKeyword_6_3__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if (match_Specification___ControllableKeyword_6_0_LeftCurlyBracketKeyword_6_1_RightCurlyBracketKeyword_6_3__q.equals(syntax))
				emit_Specification___ControllableKeyword_6_0_LeftCurlyBracketKeyword_6_1_RightCurlyBracketKeyword_6_3__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if (match_Specification___NonSpontaneousEventsKeyword_8_0_LeftCurlyBracketKeyword_8_1_RightCurlyBracketKeyword_8_3__q.equals(syntax))
				emit_Specification___NonSpontaneousEventsKeyword_8_0_LeftCurlyBracketKeyword_8_1_RightCurlyBracketKeyword_8_3__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if (match_Specification___ParameterRangesKeyword_9_0_LeftCurlyBracketKeyword_9_1_RightCurlyBracketKeyword_9_3__q.equals(syntax))
				emit_Specification___ParameterRangesKeyword_9_0_LeftCurlyBracketKeyword_9_1_RightCurlyBracketKeyword_9_3__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else acceptNodes(getLastNavigableState(), syntaxNodes);
		}
	}

	/**
	 * Ambiguous syntax:
	 *     '('*
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) (ambiguity) 'null' (rule start)
	 *     (rule start) (ambiguity) operator='!'
	 *     (rule start) (ambiguity) operator='-'
	 *     (rule start) (ambiguity) target=[EObject|ID]
	 *     (rule start) (ambiguity) type=[EEnum|ID]
	 *     (rule start) (ambiguity) value=BOOL
	 *     (rule start) (ambiguity) value=INT
	 *     (rule start) (ambiguity) value=SIGNEDINT
	 *     (rule start) (ambiguity) value=STRING
	 *     (rule start) (ambiguity) value=[Variable|ID]
	 *     (rule start) (ambiguity) {BinaryOperationExpression.left=}
	 */
	protected void emit_BasicExpression_LeftParenthesisKeyword_1_0_a(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     '('+
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) (ambiguity) operator='!'
	 *     (rule start) (ambiguity) operator='-'
	 *     (rule start) (ambiguity) {BinaryOperationExpression.left=}
	 */
	protected void emit_BasicExpression_LeftParenthesisKeyword_1_0_p(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     ('channels' '{' '}')?
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) (ambiguity) (rule start)
	 */
	protected void emit_ChannelOptions___ChannelsKeyword_1_0_LeftCurlyBracketKeyword_1_1_RightCurlyBracketKeyword_1_4__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     ('(' ')')?
	 *
	 * This ambiguous syntax occurs at:
	 *     collectionModification=CollectionModification (ambiguity) (rule end)
	 *     modelElement=[ETypedElement|ID] (ambiguity) (rule end)
	 */
	protected void emit_ModalMessage___LeftParenthesisKeyword_8_0_RightParenthesisKeyword_8_2__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     ('bindings' '[' ']')?
	 *
	 * This ambiguous syntax occurs at:
	 *     contexts+=[EClass|ID] (ambiguity) ownedInteraction=Interaction
	 *     cost=DOUBLE ']' (ambiguity) ownedInteraction=Interaction
	 *     name=ID (ambiguity) ownedInteraction=Interaction
	 *     optimizeCost?='optimize' 'cost' (ambiguity) ownedInteraction=Interaction
	 */
	protected void emit_Scenario___BindingsKeyword_6_0_LeftSquareBracketKeyword_6_1_RightSquareBracketKeyword_6_3__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     ('controllable' '{' '}')?
	 *
	 * This ambiguous syntax occurs at:
	 *     contexts+=[EPackage|FQN] (ambiguity) channelOptions=ChannelOptions
	 *     domains+=[EPackage|FQN] (ambiguity) channelOptions=ChannelOptions
	 *     name=ID '{' (ambiguity) channelOptions=ChannelOptions
	 */
	protected void emit_Specification___ControllableKeyword_6_0_LeftCurlyBracketKeyword_6_1_RightCurlyBracketKeyword_6_3__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     ('non-spontaneous events' '{' '}')?
	 *
	 * This ambiguous syntax occurs at:
	 *     channelOptions=ChannelOptions (ambiguity) 'parameter ranges' '{' eventParameterRanges+=EventParameterRanges
	 *     channelOptions=ChannelOptions (ambiguity) ('parameter ranges' '{' '}')? 'include collaboration' includedCollaborations+=[Collaboration|ID]
	 *     channelOptions=ChannelOptions (ambiguity) ('parameter ranges' '{' '}')? '}' (rule end)
	 *     channelOptions=ChannelOptions (ambiguity) ('parameter ranges' '{' '}')? containedCollaborations+=NestedCollaboration
	 */
	protected void emit_Specification___NonSpontaneousEventsKeyword_8_0_LeftCurlyBracketKeyword_8_1_RightCurlyBracketKeyword_8_3__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     ('parameter ranges' '{' '}')?
	 *
	 * This ambiguous syntax occurs at:
	 *     channelOptions=ChannelOptions ('non-spontaneous events' '{' '}')? (ambiguity) 'include collaboration' includedCollaborations+=[Collaboration|ID]
	 *     channelOptions=ChannelOptions ('non-spontaneous events' '{' '}')? (ambiguity) '}' (rule end)
	 *     channelOptions=ChannelOptions ('non-spontaneous events' '{' '}')? (ambiguity) containedCollaborations+=NestedCollaboration
	 *     nonSpontaneousOperations+=[ETypedElement|FQN] '}' (ambiguity) 'include collaboration' includedCollaborations+=[Collaboration|ID]
	 *     nonSpontaneousOperations+=[ETypedElement|FQN] '}' (ambiguity) '}' (rule end)
	 *     nonSpontaneousOperations+=[ETypedElement|FQN] '}' (ambiguity) containedCollaborations+=NestedCollaboration
	 */
	protected void emit_Specification___ParameterRangesKeyword_9_0_LeftCurlyBracketKeyword_9_1_RightCurlyBracketKeyword_9_3__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
}
