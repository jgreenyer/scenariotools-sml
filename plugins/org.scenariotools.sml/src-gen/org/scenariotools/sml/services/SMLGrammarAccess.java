/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.services;

import com.google.inject.Singleton;
import com.google.inject.Inject;

import java.util.List;

import org.eclipse.xtext.*;
import org.eclipse.xtext.service.GrammarProvider;
import org.eclipse.xtext.service.AbstractElementFinder.*;

import org.scenariotools.sml.collaboration.services.CollaborationGrammarAccess;
import org.scenariotools.sml.expressions.services.ScenarioExpressionsGrammarAccess;
import org.eclipse.xtext.common.services.TerminalsGrammarAccess;

@Singleton
public class SMLGrammarAccess extends AbstractGrammarElementFinder {
	
	
	public class SpecificationElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.SML.Specification");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cImportsAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final RuleCall cImportsImportParserRuleCall_0_0 = (RuleCall)cImportsAssignment_0.eContents().get(0);
		private final Keyword cSpecificationKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cNameAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cNameIDTerminalRuleCall_2_0 = (RuleCall)cNameAssignment_2.eContents().get(0);
		private final Keyword cLeftCurlyBracketKeyword_3 = (Keyword)cGroup.eContents().get(3);
		private final Group cGroup_4 = (Group)cGroup.eContents().get(4);
		private final Group cGroup_4_0 = (Group)cGroup_4.eContents().get(0);
		private final Keyword cDomainKeyword_4_0_0 = (Keyword)cGroup_4_0.eContents().get(0);
		private final Assignment cDomainsAssignment_4_0_1 = (Assignment)cGroup_4_0.eContents().get(1);
		private final CrossReference cDomainsEPackageCrossReference_4_0_1_0 = (CrossReference)cDomainsAssignment_4_0_1.eContents().get(0);
		private final RuleCall cDomainsEPackageFQNParserRuleCall_4_0_1_0_1 = (RuleCall)cDomainsEPackageCrossReference_4_0_1_0.eContents().get(1);
		private final Group cGroup_5 = (Group)cGroup.eContents().get(5);
		private final Keyword cContextsKeyword_5_0 = (Keyword)cGroup_5.eContents().get(0);
		private final Assignment cContextsAssignment_5_1 = (Assignment)cGroup_5.eContents().get(1);
		private final CrossReference cContextsEPackageCrossReference_5_1_0 = (CrossReference)cContextsAssignment_5_1.eContents().get(0);
		private final RuleCall cContextsEPackageFQNParserRuleCall_5_1_0_1 = (RuleCall)cContextsEPackageCrossReference_5_1_0.eContents().get(1);
		private final Group cGroup_6 = (Group)cGroup.eContents().get(6);
		private final Keyword cControllableKeyword_6_0 = (Keyword)cGroup_6.eContents().get(0);
		private final Keyword cLeftCurlyBracketKeyword_6_1 = (Keyword)cGroup_6.eContents().get(1);
		private final Assignment cControllableEClassesAssignment_6_2 = (Assignment)cGroup_6.eContents().get(2);
		private final CrossReference cControllableEClassesEClassCrossReference_6_2_0 = (CrossReference)cControllableEClassesAssignment_6_2.eContents().get(0);
		private final RuleCall cControllableEClassesEClassIDTerminalRuleCall_6_2_0_1 = (RuleCall)cControllableEClassesEClassCrossReference_6_2_0.eContents().get(1);
		private final Keyword cRightCurlyBracketKeyword_6_3 = (Keyword)cGroup_6.eContents().get(3);
		private final Assignment cChannelOptionsAssignment_7 = (Assignment)cGroup.eContents().get(7);
		private final RuleCall cChannelOptionsChannelOptionsParserRuleCall_7_0 = (RuleCall)cChannelOptionsAssignment_7.eContents().get(0);
		private final Group cGroup_8 = (Group)cGroup.eContents().get(8);
		private final Keyword cNonSpontaneousEventsKeyword_8_0 = (Keyword)cGroup_8.eContents().get(0);
		private final Keyword cLeftCurlyBracketKeyword_8_1 = (Keyword)cGroup_8.eContents().get(1);
		private final Group cGroup_8_2 = (Group)cGroup_8.eContents().get(2);
		private final Assignment cNonSpontaneousOperationsAssignment_8_2_0 = (Assignment)cGroup_8_2.eContents().get(0);
		private final CrossReference cNonSpontaneousOperationsETypedElementCrossReference_8_2_0_0 = (CrossReference)cNonSpontaneousOperationsAssignment_8_2_0.eContents().get(0);
		private final RuleCall cNonSpontaneousOperationsETypedElementFQNParserRuleCall_8_2_0_0_1 = (RuleCall)cNonSpontaneousOperationsETypedElementCrossReference_8_2_0_0.eContents().get(1);
		private final Assignment cNonSpontaneousOperationsAssignment_8_2_1 = (Assignment)cGroup_8_2.eContents().get(1);
		private final CrossReference cNonSpontaneousOperationsETypedElementCrossReference_8_2_1_0 = (CrossReference)cNonSpontaneousOperationsAssignment_8_2_1.eContents().get(0);
		private final RuleCall cNonSpontaneousOperationsETypedElementFQNParserRuleCall_8_2_1_0_1 = (RuleCall)cNonSpontaneousOperationsETypedElementCrossReference_8_2_1_0.eContents().get(1);
		private final Keyword cRightCurlyBracketKeyword_8_3 = (Keyword)cGroup_8.eContents().get(3);
		private final Group cGroup_9 = (Group)cGroup.eContents().get(9);
		private final Keyword cParameterRangesKeyword_9_0 = (Keyword)cGroup_9.eContents().get(0);
		private final Keyword cLeftCurlyBracketKeyword_9_1 = (Keyword)cGroup_9.eContents().get(1);
		private final Group cGroup_9_2 = (Group)cGroup_9.eContents().get(2);
		private final Assignment cEventParameterRangesAssignment_9_2_0 = (Assignment)cGroup_9_2.eContents().get(0);
		private final RuleCall cEventParameterRangesEventParameterRangesParserRuleCall_9_2_0_0 = (RuleCall)cEventParameterRangesAssignment_9_2_0.eContents().get(0);
		private final Group cGroup_9_2_1 = (Group)cGroup_9_2.eContents().get(1);
		private final Keyword cCommaKeyword_9_2_1_0 = (Keyword)cGroup_9_2_1.eContents().get(0);
		private final Assignment cEventParameterRangesAssignment_9_2_1_1 = (Assignment)cGroup_9_2_1.eContents().get(1);
		private final RuleCall cEventParameterRangesEventParameterRangesParserRuleCall_9_2_1_1_0 = (RuleCall)cEventParameterRangesAssignment_9_2_1_1.eContents().get(0);
		private final Keyword cRightCurlyBracketKeyword_9_3 = (Keyword)cGroup_9.eContents().get(3);
		private final Alternatives cAlternatives_10 = (Alternatives)cGroup.eContents().get(10);
		private final Assignment cContainedCollaborationsAssignment_10_0 = (Assignment)cAlternatives_10.eContents().get(0);
		private final RuleCall cContainedCollaborationsNestedCollaborationParserRuleCall_10_0_0 = (RuleCall)cContainedCollaborationsAssignment_10_0.eContents().get(0);
		private final Group cGroup_10_1 = (Group)cAlternatives_10.eContents().get(1);
		private final Keyword cIncludeCollaborationKeyword_10_1_0 = (Keyword)cGroup_10_1.eContents().get(0);
		private final Assignment cIncludedCollaborationsAssignment_10_1_1 = (Assignment)cGroup_10_1.eContents().get(1);
		private final CrossReference cIncludedCollaborationsCollaborationCrossReference_10_1_1_0 = (CrossReference)cIncludedCollaborationsAssignment_10_1_1.eContents().get(0);
		private final RuleCall cIncludedCollaborationsCollaborationIDTerminalRuleCall_10_1_1_0_1 = (RuleCall)cIncludedCollaborationsCollaborationCrossReference_10_1_1_0.eContents().get(1);
		private final Keyword cRightCurlyBracketKeyword_11 = (Keyword)cGroup.eContents().get(11);
		
		//// Specification
		// // ------------------------------
		// Specification:
		//	imports+=Import* 'specification' name=ID '{' -> ('domain' domains+=[EPackage|FQN])* ('contexts'
		//	contexts+=[EPackage|FQN])* ('controllable' '{' controllableEClasses+=[EClass]* '}')? channelOptions=ChannelOptions
		//	('non-spontaneous events' '{' (nonSpontaneousOperations+=[ETypedElement|FQN]
		//	nonSpontaneousOperations+=[ETypedElement|FQN]*)? '}')? ('parameter ranges' '{'
		//	(eventParameterRanges+=EventParameterRanges (',' eventParameterRanges+=EventParameterRanges)*)? '}')?
		//	(containedCollaborations+=NestedCollaboration | 'include collaboration' includedCollaborations+=[Collaboration])*
		//	'}';
		@Override public ParserRule getRule() { return rule; }

		//imports+=Import* 'specification' name=ID '{' -> ('domain' domains+=[EPackage|FQN])* ('contexts'
		//contexts+=[EPackage|FQN])* ('controllable' '{' controllableEClasses+=[EClass]* '}')? channelOptions=ChannelOptions
		//('non-spontaneous events' '{' (nonSpontaneousOperations+=[ETypedElement|FQN]
		//nonSpontaneousOperations+=[ETypedElement|FQN]*)? '}')? ('parameter ranges' '{'
		//(eventParameterRanges+=EventParameterRanges (',' eventParameterRanges+=EventParameterRanges)*)? '}')?
		//(containedCollaborations+=NestedCollaboration | 'include collaboration' includedCollaborations+=[Collaboration])* '}'
		public Group getGroup() { return cGroup; }

		//imports+=Import*
		public Assignment getImportsAssignment_0() { return cImportsAssignment_0; }

		//Import
		public RuleCall getImportsImportParserRuleCall_0_0() { return cImportsImportParserRuleCall_0_0; }

		//'specification'
		public Keyword getSpecificationKeyword_1() { return cSpecificationKeyword_1; }

		//name=ID
		public Assignment getNameAssignment_2() { return cNameAssignment_2; }

		//ID
		public RuleCall getNameIDTerminalRuleCall_2_0() { return cNameIDTerminalRuleCall_2_0; }

		//'{'
		public Keyword getLeftCurlyBracketKeyword_3() { return cLeftCurlyBracketKeyword_3; }

		//-> ('domain' domains+=[EPackage|FQN])*
		public Group getGroup_4() { return cGroup_4; }

		//'domain' domains+=[EPackage|FQN]
		public Group getGroup_4_0() { return cGroup_4_0; }

		//'domain'
		public Keyword getDomainKeyword_4_0_0() { return cDomainKeyword_4_0_0; }

		//domains+=[EPackage|FQN]
		public Assignment getDomainsAssignment_4_0_1() { return cDomainsAssignment_4_0_1; }

		//[EPackage|FQN]
		public CrossReference getDomainsEPackageCrossReference_4_0_1_0() { return cDomainsEPackageCrossReference_4_0_1_0; }

		//FQN
		public RuleCall getDomainsEPackageFQNParserRuleCall_4_0_1_0_1() { return cDomainsEPackageFQNParserRuleCall_4_0_1_0_1; }

		//('contexts' contexts+=[EPackage|FQN])*
		public Group getGroup_5() { return cGroup_5; }

		//'contexts'
		public Keyword getContextsKeyword_5_0() { return cContextsKeyword_5_0; }

		//contexts+=[EPackage|FQN]
		public Assignment getContextsAssignment_5_1() { return cContextsAssignment_5_1; }

		//[EPackage|FQN]
		public CrossReference getContextsEPackageCrossReference_5_1_0() { return cContextsEPackageCrossReference_5_1_0; }

		//FQN
		public RuleCall getContextsEPackageFQNParserRuleCall_5_1_0_1() { return cContextsEPackageFQNParserRuleCall_5_1_0_1; }

		//('controllable' '{' controllableEClasses+=[EClass]* '}')?
		public Group getGroup_6() { return cGroup_6; }

		//'controllable'
		public Keyword getControllableKeyword_6_0() { return cControllableKeyword_6_0; }

		//'{'
		public Keyword getLeftCurlyBracketKeyword_6_1() { return cLeftCurlyBracketKeyword_6_1; }

		//controllableEClasses+=[EClass]*
		public Assignment getControllableEClassesAssignment_6_2() { return cControllableEClassesAssignment_6_2; }

		//[EClass]
		public CrossReference getControllableEClassesEClassCrossReference_6_2_0() { return cControllableEClassesEClassCrossReference_6_2_0; }

		//ID
		public RuleCall getControllableEClassesEClassIDTerminalRuleCall_6_2_0_1() { return cControllableEClassesEClassIDTerminalRuleCall_6_2_0_1; }

		//'}'
		public Keyword getRightCurlyBracketKeyword_6_3() { return cRightCurlyBracketKeyword_6_3; }

		//channelOptions=ChannelOptions
		public Assignment getChannelOptionsAssignment_7() { return cChannelOptionsAssignment_7; }

		//ChannelOptions
		public RuleCall getChannelOptionsChannelOptionsParserRuleCall_7_0() { return cChannelOptionsChannelOptionsParserRuleCall_7_0; }

		//('non-spontaneous events' '{' (nonSpontaneousOperations+=[ETypedElement|FQN]
		//nonSpontaneousOperations+=[ETypedElement|FQN]*)? '}')?
		public Group getGroup_8() { return cGroup_8; }

		//'non-spontaneous events'
		public Keyword getNonSpontaneousEventsKeyword_8_0() { return cNonSpontaneousEventsKeyword_8_0; }

		//'{'
		public Keyword getLeftCurlyBracketKeyword_8_1() { return cLeftCurlyBracketKeyword_8_1; }

		//(nonSpontaneousOperations+=[ETypedElement|FQN] nonSpontaneousOperations+=[ETypedElement|FQN]*)?
		public Group getGroup_8_2() { return cGroup_8_2; }

		//nonSpontaneousOperations+=[ETypedElement|FQN]
		public Assignment getNonSpontaneousOperationsAssignment_8_2_0() { return cNonSpontaneousOperationsAssignment_8_2_0; }

		//[ETypedElement|FQN]
		public CrossReference getNonSpontaneousOperationsETypedElementCrossReference_8_2_0_0() { return cNonSpontaneousOperationsETypedElementCrossReference_8_2_0_0; }

		//FQN
		public RuleCall getNonSpontaneousOperationsETypedElementFQNParserRuleCall_8_2_0_0_1() { return cNonSpontaneousOperationsETypedElementFQNParserRuleCall_8_2_0_0_1; }

		//nonSpontaneousOperations+=[ETypedElement|FQN]*
		public Assignment getNonSpontaneousOperationsAssignment_8_2_1() { return cNonSpontaneousOperationsAssignment_8_2_1; }

		//[ETypedElement|FQN]
		public CrossReference getNonSpontaneousOperationsETypedElementCrossReference_8_2_1_0() { return cNonSpontaneousOperationsETypedElementCrossReference_8_2_1_0; }

		//FQN
		public RuleCall getNonSpontaneousOperationsETypedElementFQNParserRuleCall_8_2_1_0_1() { return cNonSpontaneousOperationsETypedElementFQNParserRuleCall_8_2_1_0_1; }

		//'}'
		public Keyword getRightCurlyBracketKeyword_8_3() { return cRightCurlyBracketKeyword_8_3; }

		//('parameter ranges' '{' (eventParameterRanges+=EventParameterRanges (',' eventParameterRanges+=EventParameterRanges)*)?
		//'}')?
		public Group getGroup_9() { return cGroup_9; }

		//'parameter ranges'
		public Keyword getParameterRangesKeyword_9_0() { return cParameterRangesKeyword_9_0; }

		//'{'
		public Keyword getLeftCurlyBracketKeyword_9_1() { return cLeftCurlyBracketKeyword_9_1; }

		//(eventParameterRanges+=EventParameterRanges (',' eventParameterRanges+=EventParameterRanges)*)?
		public Group getGroup_9_2() { return cGroup_9_2; }

		//eventParameterRanges+=EventParameterRanges
		public Assignment getEventParameterRangesAssignment_9_2_0() { return cEventParameterRangesAssignment_9_2_0; }

		//EventParameterRanges
		public RuleCall getEventParameterRangesEventParameterRangesParserRuleCall_9_2_0_0() { return cEventParameterRangesEventParameterRangesParserRuleCall_9_2_0_0; }

		//(',' eventParameterRanges+=EventParameterRanges)*
		public Group getGroup_9_2_1() { return cGroup_9_2_1; }

		//','
		public Keyword getCommaKeyword_9_2_1_0() { return cCommaKeyword_9_2_1_0; }

		//eventParameterRanges+=EventParameterRanges
		public Assignment getEventParameterRangesAssignment_9_2_1_1() { return cEventParameterRangesAssignment_9_2_1_1; }

		//EventParameterRanges
		public RuleCall getEventParameterRangesEventParameterRangesParserRuleCall_9_2_1_1_0() { return cEventParameterRangesEventParameterRangesParserRuleCall_9_2_1_1_0; }

		//'}'
		public Keyword getRightCurlyBracketKeyword_9_3() { return cRightCurlyBracketKeyword_9_3; }

		//(containedCollaborations+=NestedCollaboration | 'include collaboration' includedCollaborations+=[Collaboration])*
		public Alternatives getAlternatives_10() { return cAlternatives_10; }

		//containedCollaborations+=NestedCollaboration
		public Assignment getContainedCollaborationsAssignment_10_0() { return cContainedCollaborationsAssignment_10_0; }

		//NestedCollaboration
		public RuleCall getContainedCollaborationsNestedCollaborationParserRuleCall_10_0_0() { return cContainedCollaborationsNestedCollaborationParserRuleCall_10_0_0; }

		//'include collaboration' includedCollaborations+=[Collaboration]
		public Group getGroup_10_1() { return cGroup_10_1; }

		//'include collaboration'
		public Keyword getIncludeCollaborationKeyword_10_1_0() { return cIncludeCollaborationKeyword_10_1_0; }

		//includedCollaborations+=[Collaboration]
		public Assignment getIncludedCollaborationsAssignment_10_1_1() { return cIncludedCollaborationsAssignment_10_1_1; }

		//[Collaboration]
		public CrossReference getIncludedCollaborationsCollaborationCrossReference_10_1_1_0() { return cIncludedCollaborationsCollaborationCrossReference_10_1_1_0; }

		//ID
		public RuleCall getIncludedCollaborationsCollaborationIDTerminalRuleCall_10_1_1_0_1() { return cIncludedCollaborationsCollaborationIDTerminalRuleCall_10_1_1_0_1; }

		//'}'
		public Keyword getRightCurlyBracketKeyword_11() { return cRightCurlyBracketKeyword_11; }
	}

	public class FQNENUMElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.SML.FQNENUM");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final RuleCall cIDTerminalRuleCall_0 = (RuleCall)cGroup.eContents().get(0);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Keyword cColonKeyword_1_0 = (Keyword)cGroup_1.eContents().get(0);
		private final RuleCall cIDTerminalRuleCall_1_1 = (RuleCall)cGroup_1.eContents().get(1);
		
		//FQNENUM:
		//	ID (':' ID)*;
		@Override public ParserRule getRule() { return rule; }

		//ID (':' ID)*
		public Group getGroup() { return cGroup; }

		//ID
		public RuleCall getIDTerminalRuleCall_0() { return cIDTerminalRuleCall_0; }

		//(':' ID)*
		public Group getGroup_1() { return cGroup_1; }

		//':'
		public Keyword getColonKeyword_1_0() { return cColonKeyword_1_0; }

		//ID
		public RuleCall getIDTerminalRuleCall_1_1() { return cIDTerminalRuleCall_1_1; }
	}

	public class ChannelOptionsElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.SML.ChannelOptions");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cChannelOptionsAction_0 = (Action)cGroup.eContents().get(0);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Keyword cChannelsKeyword_1_0 = (Keyword)cGroup_1.eContents().get(0);
		private final Keyword cLeftCurlyBracketKeyword_1_1 = (Keyword)cGroup_1.eContents().get(1);
		private final Assignment cAllMessagesRequireLinksAssignment_1_2 = (Assignment)cGroup_1.eContents().get(2);
		private final Keyword cAllMessagesRequireLinksAllMessagesRequireLinksKeyword_1_2_0 = (Keyword)cAllMessagesRequireLinksAssignment_1_2.eContents().get(0);
		private final Assignment cMessageChannelsAssignment_1_3 = (Assignment)cGroup_1.eContents().get(3);
		private final RuleCall cMessageChannelsMessageChannelParserRuleCall_1_3_0 = (RuleCall)cMessageChannelsAssignment_1_3.eContents().get(0);
		private final Keyword cRightCurlyBracketKeyword_1_4 = (Keyword)cGroup_1.eContents().get(4);
		
		//ChannelOptions:
		//	{ChannelOptions} ('channels' '{' allMessagesRequireLinks?='all messages require links'?
		//	messageChannels+=MessageChannel* '}')?;
		@Override public ParserRule getRule() { return rule; }

		//{ChannelOptions} ('channels' '{' allMessagesRequireLinks?='all messages require links'? messageChannels+=MessageChannel*
		//'}')?
		public Group getGroup() { return cGroup; }

		//{ChannelOptions}
		public Action getChannelOptionsAction_0() { return cChannelOptionsAction_0; }

		//('channels' '{' allMessagesRequireLinks?='all messages require links'? messageChannels+=MessageChannel* '}')?
		public Group getGroup_1() { return cGroup_1; }

		//'channels'
		public Keyword getChannelsKeyword_1_0() { return cChannelsKeyword_1_0; }

		//'{'
		public Keyword getLeftCurlyBracketKeyword_1_1() { return cLeftCurlyBracketKeyword_1_1; }

		//allMessagesRequireLinks?='all messages require links'?
		public Assignment getAllMessagesRequireLinksAssignment_1_2() { return cAllMessagesRequireLinksAssignment_1_2; }

		//'all messages require links'
		public Keyword getAllMessagesRequireLinksAllMessagesRequireLinksKeyword_1_2_0() { return cAllMessagesRequireLinksAllMessagesRequireLinksKeyword_1_2_0; }

		//messageChannels+=MessageChannel*
		public Assignment getMessageChannelsAssignment_1_3() { return cMessageChannelsAssignment_1_3; }

		//MessageChannel
		public RuleCall getMessageChannelsMessageChannelParserRuleCall_1_3_0() { return cMessageChannelsMessageChannelParserRuleCall_1_3_0; }

		//'}'
		public Keyword getRightCurlyBracketKeyword_1_4() { return cRightCurlyBracketKeyword_1_4; }
	}

	public class MessageChannelElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.SML.MessageChannel");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cEventAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final CrossReference cEventETypedElementCrossReference_0_0 = (CrossReference)cEventAssignment_0.eContents().get(0);
		private final RuleCall cEventETypedElementFQNParserRuleCall_0_0_1 = (RuleCall)cEventETypedElementCrossReference_0_0.eContents().get(1);
		private final Keyword cOverKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cChannelFeatureAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final CrossReference cChannelFeatureEStructuralFeatureCrossReference_2_0 = (CrossReference)cChannelFeatureAssignment_2.eContents().get(0);
		private final RuleCall cChannelFeatureEStructuralFeatureFQNParserRuleCall_2_0_1 = (RuleCall)cChannelFeatureEStructuralFeatureCrossReference_2_0.eContents().get(1);
		
		//MessageChannel:
		//	event=[ETypedElement|FQN] 'over' channelFeature=[EStructuralFeature|FQN];
		@Override public ParserRule getRule() { return rule; }

		//event=[ETypedElement|FQN] 'over' channelFeature=[EStructuralFeature|FQN]
		public Group getGroup() { return cGroup; }

		//event=[ETypedElement|FQN]
		public Assignment getEventAssignment_0() { return cEventAssignment_0; }

		//[ETypedElement|FQN]
		public CrossReference getEventETypedElementCrossReference_0_0() { return cEventETypedElementCrossReference_0_0; }

		//FQN
		public RuleCall getEventETypedElementFQNParserRuleCall_0_0_1() { return cEventETypedElementFQNParserRuleCall_0_0_1; }

		//'over'
		public Keyword getOverKeyword_1() { return cOverKeyword_1; }

		//channelFeature=[EStructuralFeature|FQN]
		public Assignment getChannelFeatureAssignment_2() { return cChannelFeatureAssignment_2; }

		//[EStructuralFeature|FQN]
		public CrossReference getChannelFeatureEStructuralFeatureCrossReference_2_0() { return cChannelFeatureEStructuralFeatureCrossReference_2_0; }

		//FQN
		public RuleCall getChannelFeatureEStructuralFeatureFQNParserRuleCall_2_0_1() { return cChannelFeatureEStructuralFeatureFQNParserRuleCall_2_0_1; }
	}

	public class NestedCollaborationElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.SML.NestedCollaboration");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cCollaborationKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cNameAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cNameIDTerminalRuleCall_1_0 = (RuleCall)cNameAssignment_1.eContents().get(0);
		private final Keyword cLeftCurlyBracketKeyword_2 = (Keyword)cGroup.eContents().get(2);
		private final Assignment cRolesAssignment_3 = (Assignment)cGroup.eContents().get(3);
		private final RuleCall cRolesRoleParserRuleCall_3_0 = (RuleCall)cRolesAssignment_3.eContents().get(0);
		private final Assignment cScenariosAssignment_4 = (Assignment)cGroup.eContents().get(4);
		private final RuleCall cScenariosScenarioParserRuleCall_4_0 = (RuleCall)cScenariosAssignment_4.eContents().get(0);
		private final Keyword cRightCurlyBracketKeyword_5 = (Keyword)cGroup.eContents().get(5);
		
		//NestedCollaboration Collaboration:
		//	'collaboration' name=ID '{' roles+=Role* scenarios+=Scenario* '}';
		@Override public ParserRule getRule() { return rule; }

		//'collaboration' name=ID '{' roles+=Role* scenarios+=Scenario* '}'
		public Group getGroup() { return cGroup; }

		//'collaboration'
		public Keyword getCollaborationKeyword_0() { return cCollaborationKeyword_0; }

		//name=ID
		public Assignment getNameAssignment_1() { return cNameAssignment_1; }

		//ID
		public RuleCall getNameIDTerminalRuleCall_1_0() { return cNameIDTerminalRuleCall_1_0; }

		//'{'
		public Keyword getLeftCurlyBracketKeyword_2() { return cLeftCurlyBracketKeyword_2; }

		//roles+=Role*
		public Assignment getRolesAssignment_3() { return cRolesAssignment_3; }

		//Role
		public RuleCall getRolesRoleParserRuleCall_3_0() { return cRolesRoleParserRuleCall_3_0; }

		//scenarios+=Scenario*
		public Assignment getScenariosAssignment_4() { return cScenariosAssignment_4; }

		//Scenario
		public RuleCall getScenariosScenarioParserRuleCall_4_0() { return cScenariosScenarioParserRuleCall_4_0; }

		//'}'
		public Keyword getRightCurlyBracketKeyword_5() { return cRightCurlyBracketKeyword_5; }
	}

	public class EventParameterRangesElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.SML.EventParameterRanges");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cEventAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final CrossReference cEventETypedElementCrossReference_0_0 = (CrossReference)cEventAssignment_0.eContents().get(0);
		private final RuleCall cEventETypedElementFQNParserRuleCall_0_0_1 = (RuleCall)cEventETypedElementCrossReference_0_0.eContents().get(1);
		private final Keyword cLeftParenthesisKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cRangesForParameterAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cRangesForParameterRangesForParameterParserRuleCall_2_0 = (RuleCall)cRangesForParameterAssignment_2.eContents().get(0);
		private final Group cGroup_3 = (Group)cGroup.eContents().get(3);
		private final Keyword cCommaKeyword_3_0 = (Keyword)cGroup_3.eContents().get(0);
		private final Assignment cRangesForParameterAssignment_3_1 = (Assignment)cGroup_3.eContents().get(1);
		private final RuleCall cRangesForParameterRangesForParameterParserRuleCall_3_1_0 = (RuleCall)cRangesForParameterAssignment_3_1.eContents().get(0);
		private final Keyword cRightParenthesisKeyword_4 = (Keyword)cGroup.eContents().get(4);
		
		//EventParameterRanges:
		//	event=[ETypedElement|FQN] '(' rangesForParameter+=RangesForParameter (',' rangesForParameter+=RangesForParameter)*
		//	')';
		@Override public ParserRule getRule() { return rule; }

		//event=[ETypedElement|FQN] '(' rangesForParameter+=RangesForParameter (',' rangesForParameter+=RangesForParameter)* ')'
		public Group getGroup() { return cGroup; }

		//event=[ETypedElement|FQN]
		public Assignment getEventAssignment_0() { return cEventAssignment_0; }

		//[ETypedElement|FQN]
		public CrossReference getEventETypedElementCrossReference_0_0() { return cEventETypedElementCrossReference_0_0; }

		//FQN
		public RuleCall getEventETypedElementFQNParserRuleCall_0_0_1() { return cEventETypedElementFQNParserRuleCall_0_0_1; }

		//'('
		public Keyword getLeftParenthesisKeyword_1() { return cLeftParenthesisKeyword_1; }

		//rangesForParameter+=RangesForParameter
		public Assignment getRangesForParameterAssignment_2() { return cRangesForParameterAssignment_2; }

		//RangesForParameter
		public RuleCall getRangesForParameterRangesForParameterParserRuleCall_2_0() { return cRangesForParameterRangesForParameterParserRuleCall_2_0; }

		//(',' rangesForParameter+=RangesForParameter)*
		public Group getGroup_3() { return cGroup_3; }

		//','
		public Keyword getCommaKeyword_3_0() { return cCommaKeyword_3_0; }

		//rangesForParameter+=RangesForParameter
		public Assignment getRangesForParameterAssignment_3_1() { return cRangesForParameterAssignment_3_1; }

		//RangesForParameter
		public RuleCall getRangesForParameterRangesForParameterParserRuleCall_3_1_0() { return cRangesForParameterRangesForParameterParserRuleCall_3_1_0; }

		//')'
		public Keyword getRightParenthesisKeyword_4() { return cRightParenthesisKeyword_4; }
	}

	public class RangesForParameterElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.SML.RangesForParameter");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cParameterAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final CrossReference cParameterETypedElementCrossReference_0_0 = (CrossReference)cParameterAssignment_0.eContents().get(0);
		private final RuleCall cParameterETypedElementIDTerminalRuleCall_0_0_1 = (RuleCall)cParameterETypedElementCrossReference_0_0.eContents().get(1);
		private final Keyword cEqualsSignKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cRangesAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cRangesAbstractRangesParserRuleCall_2_0 = (RuleCall)cRangesAssignment_2.eContents().get(0);
		
		//RangesForParameter:
		//	parameter=[ETypedElement] '=' ranges=AbstractRanges;
		@Override public ParserRule getRule() { return rule; }

		//parameter=[ETypedElement] '=' ranges=AbstractRanges
		public Group getGroup() { return cGroup; }

		//parameter=[ETypedElement]
		public Assignment getParameterAssignment_0() { return cParameterAssignment_0; }

		//[ETypedElement]
		public CrossReference getParameterETypedElementCrossReference_0_0() { return cParameterETypedElementCrossReference_0_0; }

		//ID
		public RuleCall getParameterETypedElementIDTerminalRuleCall_0_0_1() { return cParameterETypedElementIDTerminalRuleCall_0_0_1; }

		//'='
		public Keyword getEqualsSignKeyword_1() { return cEqualsSignKeyword_1; }

		//ranges=AbstractRanges
		public Assignment getRangesAssignment_2() { return cRangesAssignment_2; }

		//AbstractRanges
		public RuleCall getRangesAbstractRangesParserRuleCall_2_0() { return cRangesAbstractRangesParserRuleCall_2_0; }
	}

	public class AbstractRangesElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.SML.AbstractRanges");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cLeftSquareBracketKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Alternatives cAlternatives_1 = (Alternatives)cGroup.eContents().get(1);
		private final RuleCall cIntegerRangesParserRuleCall_1_0 = (RuleCall)cAlternatives_1.eContents().get(0);
		private final RuleCall cStringRangesParserRuleCall_1_1 = (RuleCall)cAlternatives_1.eContents().get(1);
		private final RuleCall cEnumRangesParserRuleCall_1_2 = (RuleCall)cAlternatives_1.eContents().get(2);
		private final Keyword cRightSquareBracketKeyword_2 = (Keyword)cGroup.eContents().get(2);
		
		//AbstractRanges:
		//	'[' (IntegerRanges | StringRanges | EnumRanges) ']';
		@Override public ParserRule getRule() { return rule; }

		//'[' (IntegerRanges | StringRanges | EnumRanges) ']'
		public Group getGroup() { return cGroup; }

		//'['
		public Keyword getLeftSquareBracketKeyword_0() { return cLeftSquareBracketKeyword_0; }

		//IntegerRanges | StringRanges | EnumRanges
		public Alternatives getAlternatives_1() { return cAlternatives_1; }

		//IntegerRanges
		public RuleCall getIntegerRangesParserRuleCall_1_0() { return cIntegerRangesParserRuleCall_1_0; }

		//StringRanges
		public RuleCall getStringRangesParserRuleCall_1_1() { return cStringRangesParserRuleCall_1_1; }

		//EnumRanges
		public RuleCall getEnumRangesParserRuleCall_1_2() { return cEnumRangesParserRuleCall_1_2; }

		//']'
		public Keyword getRightSquareBracketKeyword_2() { return cRightSquareBracketKeyword_2; }
	}

	public class IntegerRangesElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.SML.IntegerRanges");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final Group cGroup_0 = (Group)cAlternatives.eContents().get(0);
		private final Group cGroup_0_0 = (Group)cGroup_0.eContents().get(0);
		private final Assignment cMinAssignment_0_0_0 = (Assignment)cGroup_0_0.eContents().get(0);
		private final Alternatives cMinAlternatives_0_0_0_0 = (Alternatives)cMinAssignment_0_0_0.eContents().get(0);
		private final RuleCall cMinINTTerminalRuleCall_0_0_0_0_0 = (RuleCall)cMinAlternatives_0_0_0_0.eContents().get(0);
		private final RuleCall cMinSIGNEDINTTerminalRuleCall_0_0_0_0_1 = (RuleCall)cMinAlternatives_0_0_0_0.eContents().get(1);
		private final Keyword cFullStopFullStopKeyword_0_0_1 = (Keyword)cGroup_0_0.eContents().get(1);
		private final Assignment cMaxAssignment_0_0_2 = (Assignment)cGroup_0_0.eContents().get(2);
		private final Alternatives cMaxAlternatives_0_0_2_0 = (Alternatives)cMaxAssignment_0_0_2.eContents().get(0);
		private final RuleCall cMaxINTTerminalRuleCall_0_0_2_0_0 = (RuleCall)cMaxAlternatives_0_0_2_0.eContents().get(0);
		private final RuleCall cMaxSIGNEDINTTerminalRuleCall_0_0_2_0_1 = (RuleCall)cMaxAlternatives_0_0_2_0.eContents().get(1);
		private final Group cGroup_0_1 = (Group)cGroup_0.eContents().get(1);
		private final Keyword cCommaKeyword_0_1_0 = (Keyword)cGroup_0_1.eContents().get(0);
		private final Group cGroup_0_1_1 = (Group)cGroup_0_1.eContents().get(1);
		private final Assignment cValuesAssignment_0_1_1_0 = (Assignment)cGroup_0_1_1.eContents().get(0);
		private final Alternatives cValuesAlternatives_0_1_1_0_0 = (Alternatives)cValuesAssignment_0_1_1_0.eContents().get(0);
		private final RuleCall cValuesINTTerminalRuleCall_0_1_1_0_0_0 = (RuleCall)cValuesAlternatives_0_1_1_0_0.eContents().get(0);
		private final RuleCall cValuesSIGNEDINTTerminalRuleCall_0_1_1_0_0_1 = (RuleCall)cValuesAlternatives_0_1_1_0_0.eContents().get(1);
		private final Group cGroup_0_1_1_1 = (Group)cGroup_0_1_1.eContents().get(1);
		private final Keyword cCommaKeyword_0_1_1_1_0 = (Keyword)cGroup_0_1_1_1.eContents().get(0);
		private final Assignment cValuesAssignment_0_1_1_1_1 = (Assignment)cGroup_0_1_1_1.eContents().get(1);
		private final Alternatives cValuesAlternatives_0_1_1_1_1_0 = (Alternatives)cValuesAssignment_0_1_1_1_1.eContents().get(0);
		private final RuleCall cValuesINTTerminalRuleCall_0_1_1_1_1_0_0 = (RuleCall)cValuesAlternatives_0_1_1_1_1_0.eContents().get(0);
		private final RuleCall cValuesSIGNEDINTTerminalRuleCall_0_1_1_1_1_0_1 = (RuleCall)cValuesAlternatives_0_1_1_1_1_0.eContents().get(1);
		private final Group cGroup_1 = (Group)cAlternatives.eContents().get(1);
		private final Assignment cValuesAssignment_1_0 = (Assignment)cGroup_1.eContents().get(0);
		private final Alternatives cValuesAlternatives_1_0_0 = (Alternatives)cValuesAssignment_1_0.eContents().get(0);
		private final RuleCall cValuesINTTerminalRuleCall_1_0_0_0 = (RuleCall)cValuesAlternatives_1_0_0.eContents().get(0);
		private final RuleCall cValuesSIGNEDINTTerminalRuleCall_1_0_0_1 = (RuleCall)cValuesAlternatives_1_0_0.eContents().get(1);
		private final Group cGroup_1_1 = (Group)cGroup_1.eContents().get(1);
		private final Keyword cCommaKeyword_1_1_0 = (Keyword)cGroup_1_1.eContents().get(0);
		private final Assignment cValuesAssignment_1_1_1 = (Assignment)cGroup_1_1.eContents().get(1);
		private final Alternatives cValuesAlternatives_1_1_1_0 = (Alternatives)cValuesAssignment_1_1_1.eContents().get(0);
		private final RuleCall cValuesINTTerminalRuleCall_1_1_1_0_0 = (RuleCall)cValuesAlternatives_1_1_1_0.eContents().get(0);
		private final RuleCall cValuesSIGNEDINTTerminalRuleCall_1_1_1_0_1 = (RuleCall)cValuesAlternatives_1_1_1_0.eContents().get(1);
		
		//IntegerRanges:
		//	(min=(INT | SIGNEDINT) '..' max=(INT | SIGNEDINT)) (',' (values+=(INT | SIGNEDINT) (',' values+=(INT |
		//	SIGNEDINT))*))? | values+=(INT | SIGNEDINT) (',' values+=(INT | SIGNEDINT))*;
		@Override public ParserRule getRule() { return rule; }

		//(min=(INT | SIGNEDINT) '..' max=(INT | SIGNEDINT)) (',' (values+=(INT | SIGNEDINT) (',' values+=(INT | SIGNEDINT))*))? |
		//values+=(INT | SIGNEDINT) (',' values+=(INT | SIGNEDINT))*
		public Alternatives getAlternatives() { return cAlternatives; }

		//(min=(INT | SIGNEDINT) '..' max=(INT | SIGNEDINT)) (',' (values+=(INT | SIGNEDINT) (',' values+=(INT | SIGNEDINT))*))?
		public Group getGroup_0() { return cGroup_0; }

		//min=(INT | SIGNEDINT) '..' max=(INT | SIGNEDINT)
		public Group getGroup_0_0() { return cGroup_0_0; }

		//min=(INT | SIGNEDINT)
		public Assignment getMinAssignment_0_0_0() { return cMinAssignment_0_0_0; }

		//(INT | SIGNEDINT)
		public Alternatives getMinAlternatives_0_0_0_0() { return cMinAlternatives_0_0_0_0; }

		//INT
		public RuleCall getMinINTTerminalRuleCall_0_0_0_0_0() { return cMinINTTerminalRuleCall_0_0_0_0_0; }

		//SIGNEDINT
		public RuleCall getMinSIGNEDINTTerminalRuleCall_0_0_0_0_1() { return cMinSIGNEDINTTerminalRuleCall_0_0_0_0_1; }

		//'..'
		public Keyword getFullStopFullStopKeyword_0_0_1() { return cFullStopFullStopKeyword_0_0_1; }

		//max=(INT | SIGNEDINT)
		public Assignment getMaxAssignment_0_0_2() { return cMaxAssignment_0_0_2; }

		//(INT | SIGNEDINT)
		public Alternatives getMaxAlternatives_0_0_2_0() { return cMaxAlternatives_0_0_2_0; }

		//INT
		public RuleCall getMaxINTTerminalRuleCall_0_0_2_0_0() { return cMaxINTTerminalRuleCall_0_0_2_0_0; }

		//SIGNEDINT
		public RuleCall getMaxSIGNEDINTTerminalRuleCall_0_0_2_0_1() { return cMaxSIGNEDINTTerminalRuleCall_0_0_2_0_1; }

		//(',' (values+=(INT | SIGNEDINT) (',' values+=(INT | SIGNEDINT))*))?
		public Group getGroup_0_1() { return cGroup_0_1; }

		//','
		public Keyword getCommaKeyword_0_1_0() { return cCommaKeyword_0_1_0; }

		//values+=(INT | SIGNEDINT) (',' values+=(INT | SIGNEDINT))*
		public Group getGroup_0_1_1() { return cGroup_0_1_1; }

		//values+=(INT | SIGNEDINT)
		public Assignment getValuesAssignment_0_1_1_0() { return cValuesAssignment_0_1_1_0; }

		//(INT | SIGNEDINT)
		public Alternatives getValuesAlternatives_0_1_1_0_0() { return cValuesAlternatives_0_1_1_0_0; }

		//INT
		public RuleCall getValuesINTTerminalRuleCall_0_1_1_0_0_0() { return cValuesINTTerminalRuleCall_0_1_1_0_0_0; }

		//SIGNEDINT
		public RuleCall getValuesSIGNEDINTTerminalRuleCall_0_1_1_0_0_1() { return cValuesSIGNEDINTTerminalRuleCall_0_1_1_0_0_1; }

		//(',' values+=(INT | SIGNEDINT))*
		public Group getGroup_0_1_1_1() { return cGroup_0_1_1_1; }

		//','
		public Keyword getCommaKeyword_0_1_1_1_0() { return cCommaKeyword_0_1_1_1_0; }

		//values+=(INT | SIGNEDINT)
		public Assignment getValuesAssignment_0_1_1_1_1() { return cValuesAssignment_0_1_1_1_1; }

		//(INT | SIGNEDINT)
		public Alternatives getValuesAlternatives_0_1_1_1_1_0() { return cValuesAlternatives_0_1_1_1_1_0; }

		//INT
		public RuleCall getValuesINTTerminalRuleCall_0_1_1_1_1_0_0() { return cValuesINTTerminalRuleCall_0_1_1_1_1_0_0; }

		//SIGNEDINT
		public RuleCall getValuesSIGNEDINTTerminalRuleCall_0_1_1_1_1_0_1() { return cValuesSIGNEDINTTerminalRuleCall_0_1_1_1_1_0_1; }

		//values+=(INT | SIGNEDINT) (',' values+=(INT | SIGNEDINT))*
		public Group getGroup_1() { return cGroup_1; }

		//values+=(INT | SIGNEDINT)
		public Assignment getValuesAssignment_1_0() { return cValuesAssignment_1_0; }

		//(INT | SIGNEDINT)
		public Alternatives getValuesAlternatives_1_0_0() { return cValuesAlternatives_1_0_0; }

		//INT
		public RuleCall getValuesINTTerminalRuleCall_1_0_0_0() { return cValuesINTTerminalRuleCall_1_0_0_0; }

		//SIGNEDINT
		public RuleCall getValuesSIGNEDINTTerminalRuleCall_1_0_0_1() { return cValuesSIGNEDINTTerminalRuleCall_1_0_0_1; }

		//(',' values+=(INT | SIGNEDINT))*
		public Group getGroup_1_1() { return cGroup_1_1; }

		//','
		public Keyword getCommaKeyword_1_1_0() { return cCommaKeyword_1_1_0; }

		//values+=(INT | SIGNEDINT)
		public Assignment getValuesAssignment_1_1_1() { return cValuesAssignment_1_1_1; }

		//(INT | SIGNEDINT)
		public Alternatives getValuesAlternatives_1_1_1_0() { return cValuesAlternatives_1_1_1_0; }

		//INT
		public RuleCall getValuesINTTerminalRuleCall_1_1_1_0_0() { return cValuesINTTerminalRuleCall_1_1_1_0_0; }

		//SIGNEDINT
		public RuleCall getValuesSIGNEDINTTerminalRuleCall_1_1_1_0_1() { return cValuesSIGNEDINTTerminalRuleCall_1_1_1_0_1; }
	}

	public class StringRangesElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.SML.StringRanges");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cValuesAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final RuleCall cValuesSTRINGTerminalRuleCall_0_0 = (RuleCall)cValuesAssignment_0.eContents().get(0);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Keyword cCommaKeyword_1_0 = (Keyword)cGroup_1.eContents().get(0);
		private final Assignment cValuesAssignment_1_1 = (Assignment)cGroup_1.eContents().get(1);
		private final RuleCall cValuesSTRINGTerminalRuleCall_1_1_0 = (RuleCall)cValuesAssignment_1_1.eContents().get(0);
		
		//StringRanges:
		//	values+=STRING (',' values+=STRING)*;
		@Override public ParserRule getRule() { return rule; }

		//values+=STRING (',' values+=STRING)*
		public Group getGroup() { return cGroup; }

		//values+=STRING
		public Assignment getValuesAssignment_0() { return cValuesAssignment_0; }

		//STRING
		public RuleCall getValuesSTRINGTerminalRuleCall_0_0() { return cValuesSTRINGTerminalRuleCall_0_0; }

		//(',' values+=STRING)*
		public Group getGroup_1() { return cGroup_1; }

		//','
		public Keyword getCommaKeyword_1_0() { return cCommaKeyword_1_0; }

		//values+=STRING
		public Assignment getValuesAssignment_1_1() { return cValuesAssignment_1_1; }

		//STRING
		public RuleCall getValuesSTRINGTerminalRuleCall_1_1_0() { return cValuesSTRINGTerminalRuleCall_1_1_0; }
	}

	public class EnumRangesElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "org.scenariotools.sml.SML.EnumRanges");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cValuesAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final CrossReference cValuesEEnumLiteralCrossReference_0_0 = (CrossReference)cValuesAssignment_0.eContents().get(0);
		private final RuleCall cValuesEEnumLiteralFQNENUMParserRuleCall_0_0_1 = (RuleCall)cValuesEEnumLiteralCrossReference_0_0.eContents().get(1);
		private final Group cGroup_1 = (Group)cGroup.eContents().get(1);
		private final Keyword cCommaKeyword_1_0 = (Keyword)cGroup_1.eContents().get(0);
		private final Assignment cValuesAssignment_1_1 = (Assignment)cGroup_1.eContents().get(1);
		private final CrossReference cValuesEEnumLiteralCrossReference_1_1_0 = (CrossReference)cValuesAssignment_1_1.eContents().get(0);
		private final RuleCall cValuesEEnumLiteralFQNENUMParserRuleCall_1_1_0_1 = (RuleCall)cValuesEEnumLiteralCrossReference_1_1_0.eContents().get(1);
		
		//EnumRanges:
		//	values+=[EEnumLiteral|FQNENUM] (',' values+=[EEnumLiteral|FQNENUM])*;
		@Override public ParserRule getRule() { return rule; }

		//values+=[EEnumLiteral|FQNENUM] (',' values+=[EEnumLiteral|FQNENUM])*
		public Group getGroup() { return cGroup; }

		//values+=[EEnumLiteral|FQNENUM]
		public Assignment getValuesAssignment_0() { return cValuesAssignment_0; }

		//[EEnumLiteral|FQNENUM]
		public CrossReference getValuesEEnumLiteralCrossReference_0_0() { return cValuesEEnumLiteralCrossReference_0_0; }

		//FQNENUM
		public RuleCall getValuesEEnumLiteralFQNENUMParserRuleCall_0_0_1() { return cValuesEEnumLiteralFQNENUMParserRuleCall_0_0_1; }

		//(',' values+=[EEnumLiteral|FQNENUM])*
		public Group getGroup_1() { return cGroup_1; }

		//','
		public Keyword getCommaKeyword_1_0() { return cCommaKeyword_1_0; }

		//values+=[EEnumLiteral|FQNENUM]
		public Assignment getValuesAssignment_1_1() { return cValuesAssignment_1_1; }

		//[EEnumLiteral|FQNENUM]
		public CrossReference getValuesEEnumLiteralCrossReference_1_1_0() { return cValuesEEnumLiteralCrossReference_1_1_0; }

		//FQNENUM
		public RuleCall getValuesEEnumLiteralFQNENUMParserRuleCall_1_1_0_1() { return cValuesEEnumLiteralFQNENUMParserRuleCall_1_1_0_1; }
	}
	
	
	private final SpecificationElements pSpecification;
	private final FQNENUMElements pFQNENUM;
	private final ChannelOptionsElements pChannelOptions;
	private final MessageChannelElements pMessageChannel;
	private final NestedCollaborationElements pNestedCollaboration;
	private final EventParameterRangesElements pEventParameterRanges;
	private final RangesForParameterElements pRangesForParameter;
	private final AbstractRangesElements pAbstractRanges;
	private final IntegerRangesElements pIntegerRanges;
	private final StringRangesElements pStringRanges;
	private final EnumRangesElements pEnumRanges;
	
	private final Grammar grammar;

	private final CollaborationGrammarAccess gaCollaboration;

	private final ScenarioExpressionsGrammarAccess gaScenarioExpressions;

	private final TerminalsGrammarAccess gaTerminals;

	@Inject
	public SMLGrammarAccess(GrammarProvider grammarProvider,
		CollaborationGrammarAccess gaCollaboration,
		ScenarioExpressionsGrammarAccess gaScenarioExpressions,
		TerminalsGrammarAccess gaTerminals) {
		this.grammar = internalFindGrammar(grammarProvider);
		this.gaCollaboration = gaCollaboration;
		this.gaScenarioExpressions = gaScenarioExpressions;
		this.gaTerminals = gaTerminals;
		this.pSpecification = new SpecificationElements();
		this.pFQNENUM = new FQNENUMElements();
		this.pChannelOptions = new ChannelOptionsElements();
		this.pMessageChannel = new MessageChannelElements();
		this.pNestedCollaboration = new NestedCollaborationElements();
		this.pEventParameterRanges = new EventParameterRangesElements();
		this.pRangesForParameter = new RangesForParameterElements();
		this.pAbstractRanges = new AbstractRangesElements();
		this.pIntegerRanges = new IntegerRangesElements();
		this.pStringRanges = new StringRangesElements();
		this.pEnumRanges = new EnumRangesElements();
	}
	
	protected Grammar internalFindGrammar(GrammarProvider grammarProvider) {
		Grammar grammar = grammarProvider.getGrammar(this);
		while (grammar != null) {
			if ("org.scenariotools.sml.SML".equals(grammar.getName())) {
				return grammar;
			}
			List<Grammar> grammars = grammar.getUsedGrammars();
			if (!grammars.isEmpty()) {
				grammar = grammars.iterator().next();
			} else {
				return null;
			}
		}
		return grammar;
	}
	
	@Override
	public Grammar getGrammar() {
		return grammar;
	}
	

	public CollaborationGrammarAccess getCollaborationGrammarAccess() {
		return gaCollaboration;
	}

	public ScenarioExpressionsGrammarAccess getScenarioExpressionsGrammarAccess() {
		return gaScenarioExpressions;
	}

	public TerminalsGrammarAccess getTerminalsGrammarAccess() {
		return gaTerminals;
	}

	
	//// Specification
	// // ------------------------------
	// Specification:
	//	imports+=Import* 'specification' name=ID '{' -> ('domain' domains+=[EPackage|FQN])* ('contexts'
	//	contexts+=[EPackage|FQN])* ('controllable' '{' controllableEClasses+=[EClass]* '}')? channelOptions=ChannelOptions
	//	('non-spontaneous events' '{' (nonSpontaneousOperations+=[ETypedElement|FQN]
	//	nonSpontaneousOperations+=[ETypedElement|FQN]*)? '}')? ('parameter ranges' '{'
	//	(eventParameterRanges+=EventParameterRanges (',' eventParameterRanges+=EventParameterRanges)*)? '}')?
	//	(containedCollaborations+=NestedCollaboration | 'include collaboration' includedCollaborations+=[Collaboration])*
	//	'}';
	public SpecificationElements getSpecificationAccess() {
		return pSpecification;
	}
	
	public ParserRule getSpecificationRule() {
		return getSpecificationAccess().getRule();
	}

	//FQNENUM:
	//	ID (':' ID)*;
	public FQNENUMElements getFQNENUMAccess() {
		return pFQNENUM;
	}
	
	public ParserRule getFQNENUMRule() {
		return getFQNENUMAccess().getRule();
	}

	//ChannelOptions:
	//	{ChannelOptions} ('channels' '{' allMessagesRequireLinks?='all messages require links'?
	//	messageChannels+=MessageChannel* '}')?;
	public ChannelOptionsElements getChannelOptionsAccess() {
		return pChannelOptions;
	}
	
	public ParserRule getChannelOptionsRule() {
		return getChannelOptionsAccess().getRule();
	}

	//MessageChannel:
	//	event=[ETypedElement|FQN] 'over' channelFeature=[EStructuralFeature|FQN];
	public MessageChannelElements getMessageChannelAccess() {
		return pMessageChannel;
	}
	
	public ParserRule getMessageChannelRule() {
		return getMessageChannelAccess().getRule();
	}

	//NestedCollaboration Collaboration:
	//	'collaboration' name=ID '{' roles+=Role* scenarios+=Scenario* '}';
	public NestedCollaborationElements getNestedCollaborationAccess() {
		return pNestedCollaboration;
	}
	
	public ParserRule getNestedCollaborationRule() {
		return getNestedCollaborationAccess().getRule();
	}

	//EventParameterRanges:
	//	event=[ETypedElement|FQN] '(' rangesForParameter+=RangesForParameter (',' rangesForParameter+=RangesForParameter)*
	//	')';
	public EventParameterRangesElements getEventParameterRangesAccess() {
		return pEventParameterRanges;
	}
	
	public ParserRule getEventParameterRangesRule() {
		return getEventParameterRangesAccess().getRule();
	}

	//RangesForParameter:
	//	parameter=[ETypedElement] '=' ranges=AbstractRanges;
	public RangesForParameterElements getRangesForParameterAccess() {
		return pRangesForParameter;
	}
	
	public ParserRule getRangesForParameterRule() {
		return getRangesForParameterAccess().getRule();
	}

	//AbstractRanges:
	//	'[' (IntegerRanges | StringRanges | EnumRanges) ']';
	public AbstractRangesElements getAbstractRangesAccess() {
		return pAbstractRanges;
	}
	
	public ParserRule getAbstractRangesRule() {
		return getAbstractRangesAccess().getRule();
	}

	//IntegerRanges:
	//	(min=(INT | SIGNEDINT) '..' max=(INT | SIGNEDINT)) (',' (values+=(INT | SIGNEDINT) (',' values+=(INT |
	//	SIGNEDINT))*))? | values+=(INT | SIGNEDINT) (',' values+=(INT | SIGNEDINT))*;
	public IntegerRangesElements getIntegerRangesAccess() {
		return pIntegerRanges;
	}
	
	public ParserRule getIntegerRangesRule() {
		return getIntegerRangesAccess().getRule();
	}

	//StringRanges:
	//	values+=STRING (',' values+=STRING)*;
	public StringRangesElements getStringRangesAccess() {
		return pStringRanges;
	}
	
	public ParserRule getStringRangesRule() {
		return getStringRangesAccess().getRule();
	}

	//EnumRanges:
	//	values+=[EEnumLiteral|FQNENUM] (',' values+=[EEnumLiteral|FQNENUM])*;
	public EnumRangesElements getEnumRangesAccess() {
		return pEnumRanges;
	}
	
	public ParserRule getEnumRangesRule() {
		return getEnumRangesAccess().getRule();
	}

	//Collaboration:
	//	imports+=Import* ('domain' domains+=[EPackage|FQN])* ('contexts' contexts+=[EPackage|FQN])* 'collaboration' name=ID
	//	'{' roles+=Role* scenarios+=Scenario* '}';
	public CollaborationGrammarAccess.CollaborationElements getCollaborationAccess() {
		return gaCollaboration.getCollaborationAccess();
	}
	
	public ParserRule getCollaborationRule() {
		return getCollaborationAccess().getRule();
	}

	//// Terminal-Definitions
	// // ------------------------------
	// FQN:
	//	ID ('.' ID)*;
	public CollaborationGrammarAccess.FQNElements getFQNAccess() {
		return gaCollaboration.getFQNAccess();
	}
	
	public ParserRule getFQNRule() {
		return getFQNAccess().getRule();
	}

	//Role:
	//	(static?='static' | 'dynamic' multiRole?='multi'?) 'role' type=[EClass] name=ID;
	public CollaborationGrammarAccess.RoleElements getRoleAccess() {
		return gaCollaboration.getRoleAccess();
	}
	
	public ParserRule getRoleRule() {
		return getRoleAccess().getRule();
	}

	//// Scenario
	// // ------------------------------
	// Scenario:
	//	singular?='singular'? kind=ScenarioKind 'scenario' name=ID (optimizeCost?='optimize' 'cost' | 'cost' '[' cost=DOUBLE
	//	']')? ('context' contexts+=[EClass] (',' contexts+=[EClass])*)? ('bindings' '[' roleBindings+=RoleBindingConstraint*
	//	']')? ownedInteraction=Interaction;
	public CollaborationGrammarAccess.ScenarioElements getScenarioAccess() {
		return gaCollaboration.getScenarioAccess();
	}
	
	public ParserRule getScenarioRule() {
		return getScenarioAccess().getRule();
	}

	//enum ScenarioKind:
	//	assumption | guarantee | existential;
	public CollaborationGrammarAccess.ScenarioKindElements getScenarioKindAccess() {
		return gaCollaboration.getScenarioKindAccess();
	}
	
	public EnumRule getScenarioKindRule() {
		return getScenarioKindAccess().getRule();
	}

	//RoleBindingConstraint:
	//	role=[Role] '=' bindingExpression=BindingExpression;
	public CollaborationGrammarAccess.RoleBindingConstraintElements getRoleBindingConstraintAccess() {
		return gaCollaboration.getRoleBindingConstraintAccess();
	}
	
	public ParserRule getRoleBindingConstraintRule() {
		return getRoleBindingConstraintAccess().getRule();
	}

	//// Binding-Expressions
	// // ------------------------------
	// BindingExpression:
	//	FeatureAccessBindingExpression;
	public CollaborationGrammarAccess.BindingExpressionElements getBindingExpressionAccess() {
		return gaCollaboration.getBindingExpressionAccess();
	}
	
	public ParserRule getBindingExpressionRule() {
		return getBindingExpressionAccess().getRule();
	}

	//FeatureAccessBindingExpression:
	//	featureaccess=FeatureAccess;
	public CollaborationGrammarAccess.FeatureAccessBindingExpressionElements getFeatureAccessBindingExpressionAccess() {
		return gaCollaboration.getFeatureAccessBindingExpressionAccess();
	}
	
	public ParserRule getFeatureAccessBindingExpressionRule() {
		return getFeatureAccessBindingExpressionAccess().getRule();
	}

	//// Interaction
	// // ------------------------------
	// InteractionFragment:
	//	Interaction | ModalMessage | Alternative | Loop | Parallel | ConditionFragment | TimedConditionFragment |
	//	VariableFragment;
	public CollaborationGrammarAccess.InteractionFragmentElements getInteractionFragmentAccess() {
		return gaCollaboration.getInteractionFragmentAccess();
	}
	
	public ParserRule getInteractionFragmentRule() {
		return getInteractionFragmentAccess().getRule();
	}

	//VariableFragment:
	//	expression=(TypedVariableDeclaration | VariableAssignment | ClockDeclaration | ClockAssignment);
	public CollaborationGrammarAccess.VariableFragmentElements getVariableFragmentAccess() {
		return gaCollaboration.getVariableFragmentAccess();
	}
	
	public ParserRule getVariableFragmentRule() {
		return getVariableFragmentAccess().getRule();
	}

	//Interaction:
	//	{Interaction} '{' fragments+=InteractionFragment* '}' constraints=ConstraintBlock?;
	public CollaborationGrammarAccess.InteractionElements getInteractionAccess() {
		return gaCollaboration.getInteractionAccess();
	}
	
	public ParserRule getInteractionRule() {
		return getInteractionAccess().getRule();
	}

	//enum ExpectationKind:
	//	eventually | requested | urgent | committed;
	public CollaborationGrammarAccess.ExpectationKindElements getExpectationKindAccess() {
		return gaCollaboration.getExpectationKindAccess();
	}
	
	public EnumRule getExpectationKindRule() {
		return getExpectationKindAccess().getRule();
	}

	//ModalMessage:
	//	strict?='strict'? (monitored?='monitored'? expectationKind=ExpectationKind)? sender=[Role] '->' receiver=[Role] '.'
	//	modelElement=[ETypedElement] ('.' collectionModification=CollectionModification)? ('(' (parameters+=ParameterBinding
	//	(',' parameters+=ParameterBinding)*)? ')')?;
	public CollaborationGrammarAccess.ModalMessageElements getModalMessageAccess() {
		return gaCollaboration.getModalMessageAccess();
	}
	
	public ParserRule getModalMessageRule() {
		return getModalMessageAccess().getRule();
	}

	//ParameterBinding:
	//	bindingExpression=ParameterExpression;
	public CollaborationGrammarAccess.ParameterBindingElements getParameterBindingAccess() {
		return gaCollaboration.getParameterBindingAccess();
	}
	
	public ParserRule getParameterBindingRule() {
		return getParameterBindingAccess().getRule();
	}

	//ParameterExpression:
	//	WildcardParameterExpression | ValueParameterExpression | VariableBindingParameterExpression;
	public CollaborationGrammarAccess.ParameterExpressionElements getParameterExpressionAccess() {
		return gaCollaboration.getParameterExpressionAccess();
	}
	
	public ParserRule getParameterExpressionRule() {
		return getParameterExpressionAccess().getRule();
	}

	//WildcardParameterExpression:
	//	{WildcardParameterExpression} '*';
	public CollaborationGrammarAccess.WildcardParameterExpressionElements getWildcardParameterExpressionAccess() {
		return gaCollaboration.getWildcardParameterExpressionAccess();
	}
	
	public ParserRule getWildcardParameterExpressionRule() {
		return getWildcardParameterExpressionAccess().getRule();
	}

	//ValueParameterExpression:
	//	value=Expression;
	public CollaborationGrammarAccess.ValueParameterExpressionElements getValueParameterExpressionAccess() {
		return gaCollaboration.getValueParameterExpressionAccess();
	}
	
	public ParserRule getValueParameterExpressionRule() {
		return getValueParameterExpressionAccess().getRule();
	}

	//VariableBindingParameterExpression:
	//	'bind' variable=VariableValue;
	public CollaborationGrammarAccess.VariableBindingParameterExpressionElements getVariableBindingParameterExpressionAccess() {
		return gaCollaboration.getVariableBindingParameterExpressionAccess();
	}
	
	public ParserRule getVariableBindingParameterExpressionRule() {
		return getVariableBindingParameterExpressionAccess().getRule();
	}

	//Alternative:
	//	{Alternative} 'alternative' cases+=Case ('or' cases+=Case)*;
	public CollaborationGrammarAccess.AlternativeElements getAlternativeAccess() {
		return gaCollaboration.getAlternativeAccess();
	}
	
	public ParserRule getAlternativeRule() {
		return getAlternativeAccess().getRule();
	}

	//Case:
	//	{Case} caseCondition=Condition? caseInteraction=Interaction;
	public CollaborationGrammarAccess.CaseElements getCaseAccess() {
		return gaCollaboration.getCaseAccess();
	}
	
	public ParserRule getCaseRule() {
		return getCaseAccess().getRule();
	}

	//Loop:
	//	'while' loopCondition=Condition? bodyInteraction=Interaction;
	public CollaborationGrammarAccess.LoopElements getLoopAccess() {
		return gaCollaboration.getLoopAccess();
	}
	
	public ParserRule getLoopRule() {
		return getLoopAccess().getRule();
	}

	//Parallel:
	//	{Parallel} 'parallel' parallelInteraction+=Interaction ('and' parallelInteraction+=Interaction)*;
	public CollaborationGrammarAccess.ParallelElements getParallelAccess() {
		return gaCollaboration.getParallelAccess();
	}
	
	public ParserRule getParallelRule() {
		return getParallelAccess().getRule();
	}

	//// Condition
	// // ------------------------------
	// TimedConditionFragment:
	//	TimedWaitCondition | TimedInterruptCondition | TimedViolationCondition;
	public CollaborationGrammarAccess.TimedConditionFragmentElements getTimedConditionFragmentAccess() {
		return gaCollaboration.getTimedConditionFragmentAccess();
	}
	
	public ParserRule getTimedConditionFragmentRule() {
		return getTimedConditionFragmentAccess().getRule();
	}

	//ConditionFragment:
	//	WaitCondition | InterruptCondition | ViolationCondition;
	public CollaborationGrammarAccess.ConditionFragmentElements getConditionFragmentAccess() {
		return gaCollaboration.getConditionFragmentAccess();
	}
	
	public ParserRule getConditionFragmentRule() {
		return getConditionFragmentAccess().getRule();
	}

	//WaitCondition:
	//	'wait' strict?='strict'? requested?='eventually'? '[' conditionExpression=ConditionExpression ']';
	public CollaborationGrammarAccess.WaitConditionElements getWaitConditionAccess() {
		return gaCollaboration.getWaitConditionAccess();
	}
	
	public ParserRule getWaitConditionRule() {
		return getWaitConditionAccess().getRule();
	}

	//InterruptCondition:
	//	'interrupt' '[' conditionExpression=ConditionExpression ']';
	public CollaborationGrammarAccess.InterruptConditionElements getInterruptConditionAccess() {
		return gaCollaboration.getInterruptConditionAccess();
	}
	
	public ParserRule getInterruptConditionRule() {
		return getInterruptConditionAccess().getRule();
	}

	//ViolationCondition:
	//	'violation' '[' conditionExpression=ConditionExpression ']';
	public CollaborationGrammarAccess.ViolationConditionElements getViolationConditionAccess() {
		return gaCollaboration.getViolationConditionAccess();
	}
	
	public ParserRule getViolationConditionRule() {
		return getViolationConditionAccess().getRule();
	}

	//TimedWaitCondition:
	//	'timed' 'wait' strict?='strict'? requested?='eventually'? '[' timedConditionExpression=TimedExpression ']';
	public CollaborationGrammarAccess.TimedWaitConditionElements getTimedWaitConditionAccess() {
		return gaCollaboration.getTimedWaitConditionAccess();
	}
	
	public ParserRule getTimedWaitConditionRule() {
		return getTimedWaitConditionAccess().getRule();
	}

	//TimedViolationCondition:
	//	'timed' 'violation' '[' timedConditionExpression=TimedExpression ']';
	public CollaborationGrammarAccess.TimedViolationConditionElements getTimedViolationConditionAccess() {
		return gaCollaboration.getTimedViolationConditionAccess();
	}
	
	public ParserRule getTimedViolationConditionRule() {
		return getTimedViolationConditionAccess().getRule();
	}

	//TimedInterruptCondition:
	//	'timed' 'interrupt' '[' timedConditionExpression=TimedExpression ']';
	public CollaborationGrammarAccess.TimedInterruptConditionElements getTimedInterruptConditionAccess() {
		return gaCollaboration.getTimedInterruptConditionAccess();
	}
	
	public ParserRule getTimedInterruptConditionRule() {
		return getTimedInterruptConditionAccess().getRule();
	}

	//Condition:
	//	'[' conditionExpression=ConditionExpression ']';
	public CollaborationGrammarAccess.ConditionElements getConditionAccess() {
		return gaCollaboration.getConditionAccess();
	}
	
	public ParserRule getConditionRule() {
		return getConditionAccess().getRule();
	}

	//ConditionExpression:
	//	expression=Expression;
	public CollaborationGrammarAccess.ConditionExpressionElements getConditionExpressionAccess() {
		return gaCollaboration.getConditionExpressionAccess();
	}
	
	public ParserRule getConditionExpressionRule() {
		return getConditionExpressionAccess().getRule();
	}

	//// Constraints
	// // ------------------------------
	// ConstraintBlock:
	//	{ConstraintBlock} 'constraints' '[' ('consider' consider+=ConstraintMessage | 'ignore' ignore+=ConstraintMessage |
	//	'forbidden' forbidden+=ConstraintMessage | 'interrupt' interrupt+=ConstraintMessage)* ']';
	public CollaborationGrammarAccess.ConstraintBlockElements getConstraintBlockAccess() {
		return gaCollaboration.getConstraintBlockAccess();
	}
	
	public ParserRule getConstraintBlockRule() {
		return getConstraintBlockAccess().getRule();
	}

	//ConstraintMessage Message:
	//	sender=[Role] '->' receiver=[Role] '.' modelElement=[ETypedElement] ('.'
	//	collectionModification=CollectionModification)? '(' (parameters+=ParameterBinding (','
	//	parameters+=ParameterBinding)*)? ')';
	public CollaborationGrammarAccess.ConstraintMessageElements getConstraintMessageAccess() {
		return gaCollaboration.getConstraintMessageAccess();
	}
	
	public ParserRule getConstraintMessageRule() {
		return getConstraintMessageAccess().getRule();
	}

	//Document:
	//	imports+=Import* ('domain' domains+=[EPackage])* expressions+=ExpressionRegion*;
	public ScenarioExpressionsGrammarAccess.DocumentElements getDocumentAccess() {
		return gaScenarioExpressions.getDocumentAccess();
	}
	
	public ParserRule getDocumentRule() {
		return getDocumentAccess().getRule();
	}

	//Import:
	//	'import' importURI=STRING;
	public ScenarioExpressionsGrammarAccess.ImportElements getImportAccess() {
		return gaScenarioExpressions.getImportAccess();
	}
	
	public ParserRule getImportRule() {
		return getImportAccess().getRule();
	}

	//ExpressionRegion:
	//	{ExpressionRegion} '{' (expressions+=ExpressionOrRegion ';')* '}';
	public ScenarioExpressionsGrammarAccess.ExpressionRegionElements getExpressionRegionAccess() {
		return gaScenarioExpressions.getExpressionRegionAccess();
	}
	
	public ParserRule getExpressionRegionRule() {
		return getExpressionRegionAccess().getRule();
	}

	//ExpressionOrRegion:
	//	ExpressionRegion | ExpressionAndVariables;
	public ScenarioExpressionsGrammarAccess.ExpressionOrRegionElements getExpressionOrRegionAccess() {
		return gaScenarioExpressions.getExpressionOrRegionAccess();
	}
	
	public ParserRule getExpressionOrRegionRule() {
		return getExpressionOrRegionAccess().getRule();
	}

	//ExpressionAndVariables:
	//	VariableExpression | Expression;
	public ScenarioExpressionsGrammarAccess.ExpressionAndVariablesElements getExpressionAndVariablesAccess() {
		return gaScenarioExpressions.getExpressionAndVariablesAccess();
	}
	
	public ParserRule getExpressionAndVariablesRule() {
		return getExpressionAndVariablesAccess().getRule();
	}

	//VariableExpression:
	//	TypedVariableDeclaration | VariableAssignment | ClockDeclaration | ClockAssignment;
	public ScenarioExpressionsGrammarAccess.VariableExpressionElements getVariableExpressionAccess() {
		return gaScenarioExpressions.getVariableExpressionAccess();
	}
	
	public ParserRule getVariableExpressionRule() {
		return getVariableExpressionAccess().getRule();
	}

	//VariableDeclaration:
	//	'var' name=ID '=' expression=Expression;
	public ScenarioExpressionsGrammarAccess.VariableDeclarationElements getVariableDeclarationAccess() {
		return gaScenarioExpressions.getVariableDeclarationAccess();
	}
	
	public ParserRule getVariableDeclarationRule() {
		return getVariableDeclarationAccess().getRule();
	}

	//VariableAssignment:
	//	variable=[VariableDeclaration] '=' expression=Expression;
	public ScenarioExpressionsGrammarAccess.VariableAssignmentElements getVariableAssignmentAccess() {
		return gaScenarioExpressions.getVariableAssignmentAccess();
	}
	
	public ParserRule getVariableAssignmentRule() {
		return getVariableAssignmentAccess().getRule();
	}

	//TypedVariableDeclaration:
	//	'var' type=[EClassifier] name=ID ('=' expression=Expression)?;
	public ScenarioExpressionsGrammarAccess.TypedVariableDeclarationElements getTypedVariableDeclarationAccess() {
		return gaScenarioExpressions.getTypedVariableDeclarationAccess();
	}
	
	public ParserRule getTypedVariableDeclarationRule() {
		return getTypedVariableDeclarationAccess().getRule();
	}

	//ClockDeclaration:
	//	{ClockDeclaration} 'clock' name=ID ('=' expression=IntegerValue)?;
	public ScenarioExpressionsGrammarAccess.ClockDeclarationElements getClockDeclarationAccess() {
		return gaScenarioExpressions.getClockDeclarationAccess();
	}
	
	public ParserRule getClockDeclarationRule() {
		return getClockDeclarationAccess().getRule();
	}

	//Clock:
	//	name=ID leftIncluded=BOOL rightIncluded=BOOL leftValue=INT rightValue=INT;
	public ScenarioExpressionsGrammarAccess.ClockElements getClockAccess() {
		return gaScenarioExpressions.getClockAccess();
	}
	
	public ParserRule getClockRule() {
		return getClockAccess().getRule();
	}

	//ClockAssignment:
	//	'reset clock' variable=[ClockDeclaration] ('=' expression=IntegerValue)?;
	public ScenarioExpressionsGrammarAccess.ClockAssignmentElements getClockAssignmentAccess() {
		return gaScenarioExpressions.getClockAssignmentAccess();
	}
	
	public ParserRule getClockAssignmentRule() {
		return getClockAssignmentAccess().getRule();
	}

	//Expression:
	//	ImplicationExpression;
	public ScenarioExpressionsGrammarAccess.ExpressionElements getExpressionAccess() {
		return gaScenarioExpressions.getExpressionAccess();
	}
	
	public ParserRule getExpressionRule() {
		return getExpressionAccess().getRule();
	}

	//ImplicationExpression Expression:
	//	DisjunctionExpression ({BinaryOperationExpression.left=current} operator="=>" right=ImplicationExpression)?;
	public ScenarioExpressionsGrammarAccess.ImplicationExpressionElements getImplicationExpressionAccess() {
		return gaScenarioExpressions.getImplicationExpressionAccess();
	}
	
	public ParserRule getImplicationExpressionRule() {
		return getImplicationExpressionAccess().getRule();
	}

	//DisjunctionExpression Expression:
	//	ConjunctionExpression ({BinaryOperationExpression.left=current} operator="||" right=DisjunctionExpression)?;
	public ScenarioExpressionsGrammarAccess.DisjunctionExpressionElements getDisjunctionExpressionAccess() {
		return gaScenarioExpressions.getDisjunctionExpressionAccess();
	}
	
	public ParserRule getDisjunctionExpressionRule() {
		return getDisjunctionExpressionAccess().getRule();
	}

	//ConjunctionExpression Expression:
	//	RelationExpression ({BinaryOperationExpression.left=current} operator="&&" right=ConjunctionExpression)?;
	public ScenarioExpressionsGrammarAccess.ConjunctionExpressionElements getConjunctionExpressionAccess() {
		return gaScenarioExpressions.getConjunctionExpressionAccess();
	}
	
	public ParserRule getConjunctionExpressionRule() {
		return getConjunctionExpressionAccess().getRule();
	}

	//RelationExpression Expression:
	//	AdditionExpression ({BinaryOperationExpression.left=current} operator=('==' | '!=' | '<' | '>' | '<=' | '>=')
	//	right=RelationExpression)?;
	public ScenarioExpressionsGrammarAccess.RelationExpressionElements getRelationExpressionAccess() {
		return gaScenarioExpressions.getRelationExpressionAccess();
	}
	
	public ParserRule getRelationExpressionRule() {
		return getRelationExpressionAccess().getRule();
	}

	//TimedExpression:
	//	clock=[ClockDeclaration] operator=('==' | '<' | '>' | '<=' | '>=') value=INT;
	public ScenarioExpressionsGrammarAccess.TimedExpressionElements getTimedExpressionAccess() {
		return gaScenarioExpressions.getTimedExpressionAccess();
	}
	
	public ParserRule getTimedExpressionRule() {
		return getTimedExpressionAccess().getRule();
	}

	//AdditionExpression Expression:
	//	MultiplicationExpression ({BinaryOperationExpression.left=current} operator=('+' | '-') right=AdditionExpression)?;
	public ScenarioExpressionsGrammarAccess.AdditionExpressionElements getAdditionExpressionAccess() {
		return gaScenarioExpressions.getAdditionExpressionAccess();
	}
	
	public ParserRule getAdditionExpressionRule() {
		return getAdditionExpressionAccess().getRule();
	}

	//MultiplicationExpression Expression:
	//	NegatedExpression ({BinaryOperationExpression.left=current} operator=('*' | '/') right=MultiplicationExpression)?;
	public ScenarioExpressionsGrammarAccess.MultiplicationExpressionElements getMultiplicationExpressionAccess() {
		return gaScenarioExpressions.getMultiplicationExpressionAccess();
	}
	
	public ParserRule getMultiplicationExpressionRule() {
		return getMultiplicationExpressionAccess().getRule();
	}

	//NegatedExpression Expression:
	//	{UnaryOperationExpression} => operator=('!' | '-') operand=BasicExpression | BasicExpression;
	public ScenarioExpressionsGrammarAccess.NegatedExpressionElements getNegatedExpressionAccess() {
		return gaScenarioExpressions.getNegatedExpressionAccess();
	}
	
	public ParserRule getNegatedExpressionRule() {
		return getNegatedExpressionAccess().getRule();
	}

	//BasicExpression Expression:
	//	Value | '(' Expression ')';
	public ScenarioExpressionsGrammarAccess.BasicExpressionElements getBasicExpressionAccess() {
		return gaScenarioExpressions.getBasicExpressionAccess();
	}
	
	public ParserRule getBasicExpressionRule() {
		return getBasicExpressionAccess().getRule();
	}

	//Value:
	//	IntegerValue | BooleanValue | StringValue | EnumValue | NullValue | VariableValue | FeatureAccess;
	public ScenarioExpressionsGrammarAccess.ValueElements getValueAccess() {
		return gaScenarioExpressions.getValueAccess();
	}
	
	public ParserRule getValueRule() {
		return getValueAccess().getRule();
	}

	//IntegerValue:
	//	value=(INT | SIGNEDINT);
	public ScenarioExpressionsGrammarAccess.IntegerValueElements getIntegerValueAccess() {
		return gaScenarioExpressions.getIntegerValueAccess();
	}
	
	public ParserRule getIntegerValueRule() {
		return getIntegerValueAccess().getRule();
	}

	//BooleanValue:
	//	value=BOOL;
	public ScenarioExpressionsGrammarAccess.BooleanValueElements getBooleanValueAccess() {
		return gaScenarioExpressions.getBooleanValueAccess();
	}
	
	public ParserRule getBooleanValueRule() {
		return getBooleanValueAccess().getRule();
	}

	//StringValue:
	//	value=STRING;
	public ScenarioExpressionsGrammarAccess.StringValueElements getStringValueAccess() {
		return gaScenarioExpressions.getStringValueAccess();
	}
	
	public ParserRule getStringValueRule() {
		return getStringValueAccess().getRule();
	}

	//EnumValue:
	//	type=[EEnum] ':' value=[EEnumLiteral];
	public ScenarioExpressionsGrammarAccess.EnumValueElements getEnumValueAccess() {
		return gaScenarioExpressions.getEnumValueAccess();
	}
	
	public ParserRule getEnumValueRule() {
		return getEnumValueAccess().getRule();
	}

	//NullValue:
	//	{NullValue} 'null';
	public ScenarioExpressionsGrammarAccess.NullValueElements getNullValueAccess() {
		return gaScenarioExpressions.getNullValueAccess();
	}
	
	public ParserRule getNullValueRule() {
		return getNullValueAccess().getRule();
	}

	//VariableValue:
	//	value=[Variable];
	public ScenarioExpressionsGrammarAccess.VariableValueElements getVariableValueAccess() {
		return gaScenarioExpressions.getVariableValueAccess();
	}
	
	public ParserRule getVariableValueRule() {
		return getVariableValueAccess().getRule();
	}

	//CollectionAccess:
	//	collectionOperation=CollectionOperation '(' parameter=Expression? ')';
	public ScenarioExpressionsGrammarAccess.CollectionAccessElements getCollectionAccessAccess() {
		return gaScenarioExpressions.getCollectionAccessAccess();
	}
	
	public ParserRule getCollectionAccessRule() {
		return getCollectionAccessAccess().getRule();
	}

	//enum CollectionOperation:
	//	any | contains | containsAll | first | get | isEmpty | last | size;
	public ScenarioExpressionsGrammarAccess.CollectionOperationElements getCollectionOperationAccess() {
		return gaScenarioExpressions.getCollectionOperationAccess();
	}
	
	public EnumRule getCollectionOperationRule() {
		return getCollectionOperationAccess().getRule();
	}

	//enum CollectionModification returns CollectionOperation:
	//	add | addToFront | clear | remove;
	public ScenarioExpressionsGrammarAccess.CollectionModificationElements getCollectionModificationAccess() {
		return gaScenarioExpressions.getCollectionModificationAccess();
	}
	
	public EnumRule getCollectionModificationRule() {
		return getCollectionModificationAccess().getRule();
	}

	//FeatureAccess:
	//	target=[EObject] '.' (value=StructuralFeatureValue ('.' collectionAccess=CollectionAccess)? | value=OperationValue
	//	'(' (parameters+=Expression (',' parameters+=Expression)*)? ')');
	public ScenarioExpressionsGrammarAccess.FeatureAccessElements getFeatureAccessAccess() {
		return gaScenarioExpressions.getFeatureAccessAccess();
	}
	
	public ParserRule getFeatureAccessRule() {
		return getFeatureAccessAccess().getRule();
	}

	//StructuralFeatureValue:
	//	value=[EStructuralFeature];
	public ScenarioExpressionsGrammarAccess.StructuralFeatureValueElements getStructuralFeatureValueAccess() {
		return gaScenarioExpressions.getStructuralFeatureValueAccess();
	}
	
	public ParserRule getStructuralFeatureValueRule() {
		return getStructuralFeatureValueAccess().getRule();
	}

	//OperationValue:
	//	value=[EOperation];
	public ScenarioExpressionsGrammarAccess.OperationValueElements getOperationValueAccess() {
		return gaScenarioExpressions.getOperationValueAccess();
	}
	
	public ParserRule getOperationValueRule() {
		return getOperationValueAccess().getRule();
	}

	//terminal BOOL returns EBoolean:
	//	'false' | 'true';
	public TerminalRule getBOOLRule() {
		return gaScenarioExpressions.getBOOLRule();
	} 

	//terminal SIGNEDINT returns EInt:
	//	'-' INT;
	public TerminalRule getSIGNEDINTRule() {
		return gaScenarioExpressions.getSIGNEDINTRule();
	} 

	//terminal DOUBLE returns EDouble:
	//	'-'? INT? '.' INT (('e' | 'E') '-'? INT)?;
	public TerminalRule getDOUBLERule() {
		return gaScenarioExpressions.getDOUBLERule();
	} 

	//terminal ID:
	//	'^'? ('a'..'z' | 'A'..'Z' | '_') ('a'..'z' | 'A'..'Z' | '_' | '0'..'9')*;
	public TerminalRule getIDRule() {
		return gaTerminals.getIDRule();
	} 

	//terminal INT returns ecore::EInt:
	//	'0'..'9'+;
	public TerminalRule getINTRule() {
		return gaTerminals.getINTRule();
	} 

	//terminal STRING:
	//	'"' ('\\' . | !('\\' | '"'))* '"' | "'" ('\\' . | !('\\' | "'"))* "'";
	public TerminalRule getSTRINGRule() {
		return gaTerminals.getSTRINGRule();
	} 

	//terminal ML_COMMENT:
	//	'/*'->'*/';
	public TerminalRule getML_COMMENTRule() {
		return gaTerminals.getML_COMMENTRule();
	} 

	//terminal SL_COMMENT:
	//	'//' !('\n' | '\r')* ('\r'? '\n')?;
	public TerminalRule getSL_COMMENTRule() {
		return gaTerminals.getSL_COMMENTRule();
	} 

	//terminal WS:
	//	' ' | '\t' | '\r' | '\n'+;
	public TerminalRule getWSRule() {
		return gaTerminals.getWSRule();
	} 

	//terminal ANY_OTHER:
	//	.;
	public TerminalRule getANY_OTHERRule() {
		return gaTerminals.getANY_OTHERRule();
	} 
}
