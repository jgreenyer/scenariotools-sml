package org.scenariotools.sml.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.scenariotools.sml.services.SMLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
@SuppressWarnings("all")
public class InternalSMLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_SIGNEDINT", "RULE_STRING", "RULE_DOUBLE", "RULE_BOOL", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'specification'", "'{'", "'domain'", "'contexts'", "'controllable'", "'}'", "'non-spontaneous events'", "'parameter ranges'", "','", "'include collaboration'", "':'", "'channels'", "'all messages require links'", "'over'", "'collaboration'", "'('", "')'", "'='", "'['", "']'", "'..'", "'.'", "'static'", "'dynamic'", "'multi'", "'role'", "'singular'", "'scenario'", "'optimize'", "'cost'", "'context'", "'bindings'", "'strict'", "'monitored'", "'->'", "'*'", "'bind'", "'alternative'", "'or'", "'while'", "'parallel'", "'and'", "'wait'", "'eventually'", "'interrupt'", "'violation'", "'timed'", "'constraints'", "'consider'", "'ignore'", "'forbidden'", "'import'", "';'", "'var'", "'clock'", "'reset clock'", "'=>'", "'||'", "'&&'", "'=='", "'!='", "'<'", "'>'", "'<='", "'>='", "'+'", "'-'", "'/'", "'!'", "'null'", "'assumption'", "'guarantee'", "'existential'", "'requested'", "'urgent'", "'committed'", "'any'", "'contains'", "'containsAll'", "'first'", "'get'", "'isEmpty'", "'last'", "'size'", "'add'", "'addToFront'", "'clear'", "'remove'"
    };
    public static final int T__50=50;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=4;
    public static final int RULE_INT=5;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=10;
    public static final int T__67=67;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__64=64;
    public static final int T__65=65;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_BOOL=9;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__91=91;
    public static final int T__100=100;
    public static final int T__92=92;
    public static final int T__93=93;
    public static final int T__94=94;
    public static final int T__101=101;
    public static final int T__90=90;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__99=99;
    public static final int T__14=14;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__97=97;
    public static final int T__98=98;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_SIGNEDINT=6;
    public static final int RULE_STRING=7;
    public static final int RULE_SL_COMMENT=11;
    public static final int RULE_DOUBLE=8;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__74=74;
    public static final int T__75=75;
    public static final int T__76=76;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int RULE_WS=12;
    public static final int RULE_ANY_OTHER=13;
    public static final int T__88=88;
    public static final int T__89=89;
    public static final int T__84=84;
    public static final int T__85=85;
    public static final int T__86=86;
    public static final int T__87=87;

    // delegates
    // delegators


        public InternalSMLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSMLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSMLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalSML.g"; }



     	private SMLGrammarAccess grammarAccess;
     	
        public InternalSMLParser(TokenStream input, SMLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Specification";	
       	}
       	
       	@Override
       	protected SMLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleSpecification"
    // InternalSML.g:77:1: entryRuleSpecification returns [EObject current=null] : iv_ruleSpecification= ruleSpecification EOF ;
    public final EObject entryRuleSpecification() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSpecification = null;


        try {
            // InternalSML.g:78:2: (iv_ruleSpecification= ruleSpecification EOF )
            // InternalSML.g:79:2: iv_ruleSpecification= ruleSpecification EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSpecificationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSpecification=ruleSpecification();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSpecification; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSpecification"


    // $ANTLR start "ruleSpecification"
    // InternalSML.g:86:1: ruleSpecification returns [EObject current=null] : ( ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'specification' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( 'domain' )=> (otherlv_4= 'domain' ( ( ruleFQN ) ) ) )* (otherlv_6= 'contexts' ( ( ruleFQN ) ) )* (otherlv_8= 'controllable' otherlv_9= '{' ( (otherlv_10= RULE_ID ) )* otherlv_11= '}' )? ( (lv_channelOptions_12_0= ruleChannelOptions ) ) (otherlv_13= 'non-spontaneous events' otherlv_14= '{' ( ( ( ruleFQN ) ) ( ( ruleFQN ) )* )? otherlv_17= '}' )? (otherlv_18= 'parameter ranges' otherlv_19= '{' ( ( (lv_eventParameterRanges_20_0= ruleEventParameterRanges ) ) (otherlv_21= ',' ( (lv_eventParameterRanges_22_0= ruleEventParameterRanges ) ) )* )? otherlv_23= '}' )? ( ( (lv_containedCollaborations_24_0= ruleNestedCollaboration ) ) | (otherlv_25= 'include collaboration' ( (otherlv_26= RULE_ID ) ) ) )* otherlv_27= '}' ) ;
    public final EObject ruleSpecification() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_23=null;
        Token otherlv_25=null;
        Token otherlv_26=null;
        Token otherlv_27=null;
        EObject lv_imports_0_0 = null;

        EObject lv_channelOptions_12_0 = null;

        EObject lv_eventParameterRanges_20_0 = null;

        EObject lv_eventParameterRanges_22_0 = null;

        EObject lv_containedCollaborations_24_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:89:28: ( ( ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'specification' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( 'domain' )=> (otherlv_4= 'domain' ( ( ruleFQN ) ) ) )* (otherlv_6= 'contexts' ( ( ruleFQN ) ) )* (otherlv_8= 'controllable' otherlv_9= '{' ( (otherlv_10= RULE_ID ) )* otherlv_11= '}' )? ( (lv_channelOptions_12_0= ruleChannelOptions ) ) (otherlv_13= 'non-spontaneous events' otherlv_14= '{' ( ( ( ruleFQN ) ) ( ( ruleFQN ) )* )? otherlv_17= '}' )? (otherlv_18= 'parameter ranges' otherlv_19= '{' ( ( (lv_eventParameterRanges_20_0= ruleEventParameterRanges ) ) (otherlv_21= ',' ( (lv_eventParameterRanges_22_0= ruleEventParameterRanges ) ) )* )? otherlv_23= '}' )? ( ( (lv_containedCollaborations_24_0= ruleNestedCollaboration ) ) | (otherlv_25= 'include collaboration' ( (otherlv_26= RULE_ID ) ) ) )* otherlv_27= '}' ) )
            // InternalSML.g:90:1: ( ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'specification' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( 'domain' )=> (otherlv_4= 'domain' ( ( ruleFQN ) ) ) )* (otherlv_6= 'contexts' ( ( ruleFQN ) ) )* (otherlv_8= 'controllable' otherlv_9= '{' ( (otherlv_10= RULE_ID ) )* otherlv_11= '}' )? ( (lv_channelOptions_12_0= ruleChannelOptions ) ) (otherlv_13= 'non-spontaneous events' otherlv_14= '{' ( ( ( ruleFQN ) ) ( ( ruleFQN ) )* )? otherlv_17= '}' )? (otherlv_18= 'parameter ranges' otherlv_19= '{' ( ( (lv_eventParameterRanges_20_0= ruleEventParameterRanges ) ) (otherlv_21= ',' ( (lv_eventParameterRanges_22_0= ruleEventParameterRanges ) ) )* )? otherlv_23= '}' )? ( ( (lv_containedCollaborations_24_0= ruleNestedCollaboration ) ) | (otherlv_25= 'include collaboration' ( (otherlv_26= RULE_ID ) ) ) )* otherlv_27= '}' )
            {
            // InternalSML.g:90:1: ( ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'specification' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( 'domain' )=> (otherlv_4= 'domain' ( ( ruleFQN ) ) ) )* (otherlv_6= 'contexts' ( ( ruleFQN ) ) )* (otherlv_8= 'controllable' otherlv_9= '{' ( (otherlv_10= RULE_ID ) )* otherlv_11= '}' )? ( (lv_channelOptions_12_0= ruleChannelOptions ) ) (otherlv_13= 'non-spontaneous events' otherlv_14= '{' ( ( ( ruleFQN ) ) ( ( ruleFQN ) )* )? otherlv_17= '}' )? (otherlv_18= 'parameter ranges' otherlv_19= '{' ( ( (lv_eventParameterRanges_20_0= ruleEventParameterRanges ) ) (otherlv_21= ',' ( (lv_eventParameterRanges_22_0= ruleEventParameterRanges ) ) )* )? otherlv_23= '}' )? ( ( (lv_containedCollaborations_24_0= ruleNestedCollaboration ) ) | (otherlv_25= 'include collaboration' ( (otherlv_26= RULE_ID ) ) ) )* otherlv_27= '}' )
            // InternalSML.g:90:2: ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'specification' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( 'domain' )=> (otherlv_4= 'domain' ( ( ruleFQN ) ) ) )* (otherlv_6= 'contexts' ( ( ruleFQN ) ) )* (otherlv_8= 'controllable' otherlv_9= '{' ( (otherlv_10= RULE_ID ) )* otherlv_11= '}' )? ( (lv_channelOptions_12_0= ruleChannelOptions ) ) (otherlv_13= 'non-spontaneous events' otherlv_14= '{' ( ( ( ruleFQN ) ) ( ( ruleFQN ) )* )? otherlv_17= '}' )? (otherlv_18= 'parameter ranges' otherlv_19= '{' ( ( (lv_eventParameterRanges_20_0= ruleEventParameterRanges ) ) (otherlv_21= ',' ( (lv_eventParameterRanges_22_0= ruleEventParameterRanges ) ) )* )? otherlv_23= '}' )? ( ( (lv_containedCollaborations_24_0= ruleNestedCollaboration ) ) | (otherlv_25= 'include collaboration' ( (otherlv_26= RULE_ID ) ) ) )* otherlv_27= '}'
            {
            // InternalSML.g:90:2: ( (lv_imports_0_0= ruleImport ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==65) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalSML.g:91:1: (lv_imports_0_0= ruleImport )
            	    {
            	    // InternalSML.g:91:1: (lv_imports_0_0= ruleImport )
            	    // InternalSML.g:92:3: lv_imports_0_0= ruleImport
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getSpecificationAccess().getImportsImportParserRuleCall_0_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_3);
            	    lv_imports_0_0=ruleImport();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getSpecificationRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"imports",
            	              		lv_imports_0_0, 
            	              		"org.scenariotools.sml.expressions.ScenarioExpressions.Import");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_1=(Token)match(input,14,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getSpecificationAccess().getSpecificationKeyword_1());
                  
            }
            // InternalSML.g:112:1: ( (lv_name_2_0= RULE_ID ) )
            // InternalSML.g:113:1: (lv_name_2_0= RULE_ID )
            {
            // InternalSML.g:113:1: (lv_name_2_0= RULE_ID )
            // InternalSML.g:114:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getSpecificationAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getSpecificationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,15,FollowSets000.FOLLOW_6); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getSpecificationAccess().getLeftCurlyBracketKeyword_3());
                  
            }
            // InternalSML.g:134:1: ( ( 'domain' )=> (otherlv_4= 'domain' ( ( ruleFQN ) ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==16) && (synpred1_InternalSML())) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalSML.g:134:2: ( 'domain' )=> (otherlv_4= 'domain' ( ( ruleFQN ) ) )
            	    {
            	    // InternalSML.g:135:4: (otherlv_4= 'domain' ( ( ruleFQN ) ) )
            	    // InternalSML.g:135:6: otherlv_4= 'domain' ( ( ruleFQN ) )
            	    {
            	    otherlv_4=(Token)match(input,16,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_4, grammarAccess.getSpecificationAccess().getDomainKeyword_4_0_0());
            	          
            	    }
            	    // InternalSML.g:139:1: ( ( ruleFQN ) )
            	    // InternalSML.g:140:1: ( ruleFQN )
            	    {
            	    // InternalSML.g:140:1: ( ruleFQN )
            	    // InternalSML.g:141:3: ruleFQN
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getSpecificationRule());
            	      	        }
            	              
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getSpecificationAccess().getDomainsEPackageCrossReference_4_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_6);
            	    ruleFQN();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {
            	       
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // InternalSML.g:154:5: (otherlv_6= 'contexts' ( ( ruleFQN ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==17) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalSML.g:154:7: otherlv_6= 'contexts' ( ( ruleFQN ) )
            	    {
            	    otherlv_6=(Token)match(input,17,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_6, grammarAccess.getSpecificationAccess().getContextsKeyword_5_0());
            	          
            	    }
            	    // InternalSML.g:158:1: ( ( ruleFQN ) )
            	    // InternalSML.g:159:1: ( ruleFQN )
            	    {
            	    // InternalSML.g:159:1: ( ruleFQN )
            	    // InternalSML.g:160:3: ruleFQN
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getSpecificationRule());
            	      	        }
            	              
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getSpecificationAccess().getContextsEPackageCrossReference_5_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_7);
            	    ruleFQN();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {
            	       
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            // InternalSML.g:173:4: (otherlv_8= 'controllable' otherlv_9= '{' ( (otherlv_10= RULE_ID ) )* otherlv_11= '}' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==18) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalSML.g:173:6: otherlv_8= 'controllable' otherlv_9= '{' ( (otherlv_10= RULE_ID ) )* otherlv_11= '}'
                    {
                    otherlv_8=(Token)match(input,18,FollowSets000.FOLLOW_5); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_8, grammarAccess.getSpecificationAccess().getControllableKeyword_6_0());
                          
                    }
                    otherlv_9=(Token)match(input,15,FollowSets000.FOLLOW_8); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_9, grammarAccess.getSpecificationAccess().getLeftCurlyBracketKeyword_6_1());
                          
                    }
                    // InternalSML.g:181:1: ( (otherlv_10= RULE_ID ) )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==RULE_ID) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // InternalSML.g:182:1: (otherlv_10= RULE_ID )
                    	    {
                    	    // InternalSML.g:182:1: (otherlv_10= RULE_ID )
                    	    // InternalSML.g:183:3: otherlv_10= RULE_ID
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      			if (current==null) {
                    	      	            current = createModelElement(grammarAccess.getSpecificationRule());
                    	      	        }
                    	              
                    	    }
                    	    otherlv_10=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_8); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      		newLeafNode(otherlv_10, grammarAccess.getSpecificationAccess().getControllableEClassesEClassCrossReference_6_2_0()); 
                    	      	
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);

                    otherlv_11=(Token)match(input,19,FollowSets000.FOLLOW_9); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_11, grammarAccess.getSpecificationAccess().getRightCurlyBracketKeyword_6_3());
                          
                    }

                    }
                    break;

            }

            // InternalSML.g:198:3: ( (lv_channelOptions_12_0= ruleChannelOptions ) )
            // InternalSML.g:199:1: (lv_channelOptions_12_0= ruleChannelOptions )
            {
            // InternalSML.g:199:1: (lv_channelOptions_12_0= ruleChannelOptions )
            // InternalSML.g:200:3: lv_channelOptions_12_0= ruleChannelOptions
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getSpecificationAccess().getChannelOptionsChannelOptionsParserRuleCall_7_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_10);
            lv_channelOptions_12_0=ruleChannelOptions();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getSpecificationRule());
              	        }
                     		set(
                     			current, 
                     			"channelOptions",
                      		lv_channelOptions_12_0, 
                      		"org.scenariotools.sml.SML.ChannelOptions");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalSML.g:216:2: (otherlv_13= 'non-spontaneous events' otherlv_14= '{' ( ( ( ruleFQN ) ) ( ( ruleFQN ) )* )? otherlv_17= '}' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==20) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalSML.g:216:4: otherlv_13= 'non-spontaneous events' otherlv_14= '{' ( ( ( ruleFQN ) ) ( ( ruleFQN ) )* )? otherlv_17= '}'
                    {
                    otherlv_13=(Token)match(input,20,FollowSets000.FOLLOW_5); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_13, grammarAccess.getSpecificationAccess().getNonSpontaneousEventsKeyword_8_0());
                          
                    }
                    otherlv_14=(Token)match(input,15,FollowSets000.FOLLOW_8); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_14, grammarAccess.getSpecificationAccess().getLeftCurlyBracketKeyword_8_1());
                          
                    }
                    // InternalSML.g:224:1: ( ( ( ruleFQN ) ) ( ( ruleFQN ) )* )?
                    int alt7=2;
                    int LA7_0 = input.LA(1);

                    if ( (LA7_0==RULE_ID) ) {
                        alt7=1;
                    }
                    switch (alt7) {
                        case 1 :
                            // InternalSML.g:224:2: ( ( ruleFQN ) ) ( ( ruleFQN ) )*
                            {
                            // InternalSML.g:224:2: ( ( ruleFQN ) )
                            // InternalSML.g:225:1: ( ruleFQN )
                            {
                            // InternalSML.g:225:1: ( ruleFQN )
                            // InternalSML.g:226:3: ruleFQN
                            {
                            if ( state.backtracking==0 ) {

                              			if (current==null) {
                              	            current = createModelElement(grammarAccess.getSpecificationRule());
                              	        }
                                      
                            }
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getSpecificationAccess().getNonSpontaneousOperationsETypedElementCrossReference_8_2_0_0()); 
                              	    
                            }
                            pushFollow(FollowSets000.FOLLOW_8);
                            ruleFQN();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {
                               
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }

                            // InternalSML.g:239:2: ( ( ruleFQN ) )*
                            loop6:
                            do {
                                int alt6=2;
                                int LA6_0 = input.LA(1);

                                if ( (LA6_0==RULE_ID) ) {
                                    alt6=1;
                                }


                                switch (alt6) {
                            	case 1 :
                            	    // InternalSML.g:240:1: ( ruleFQN )
                            	    {
                            	    // InternalSML.g:240:1: ( ruleFQN )
                            	    // InternalSML.g:241:3: ruleFQN
                            	    {
                            	    if ( state.backtracking==0 ) {

                            	      			if (current==null) {
                            	      	            current = createModelElement(grammarAccess.getSpecificationRule());
                            	      	        }
                            	              
                            	    }
                            	    if ( state.backtracking==0 ) {
                            	       
                            	      	        newCompositeNode(grammarAccess.getSpecificationAccess().getNonSpontaneousOperationsETypedElementCrossReference_8_2_1_0()); 
                            	      	    
                            	    }
                            	    pushFollow(FollowSets000.FOLLOW_8);
                            	    ruleFQN();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {
                            	       
                            	      	        afterParserOrEnumRuleCall();
                            	      	    
                            	    }

                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop6;
                                }
                            } while (true);


                            }
                            break;

                    }

                    otherlv_17=(Token)match(input,19,FollowSets000.FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_17, grammarAccess.getSpecificationAccess().getRightCurlyBracketKeyword_8_3());
                          
                    }

                    }
                    break;

            }

            // InternalSML.g:258:3: (otherlv_18= 'parameter ranges' otherlv_19= '{' ( ( (lv_eventParameterRanges_20_0= ruleEventParameterRanges ) ) (otherlv_21= ',' ( (lv_eventParameterRanges_22_0= ruleEventParameterRanges ) ) )* )? otherlv_23= '}' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==21) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalSML.g:258:5: otherlv_18= 'parameter ranges' otherlv_19= '{' ( ( (lv_eventParameterRanges_20_0= ruleEventParameterRanges ) ) (otherlv_21= ',' ( (lv_eventParameterRanges_22_0= ruleEventParameterRanges ) ) )* )? otherlv_23= '}'
                    {
                    otherlv_18=(Token)match(input,21,FollowSets000.FOLLOW_5); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_18, grammarAccess.getSpecificationAccess().getParameterRangesKeyword_9_0());
                          
                    }
                    otherlv_19=(Token)match(input,15,FollowSets000.FOLLOW_8); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_19, grammarAccess.getSpecificationAccess().getLeftCurlyBracketKeyword_9_1());
                          
                    }
                    // InternalSML.g:266:1: ( ( (lv_eventParameterRanges_20_0= ruleEventParameterRanges ) ) (otherlv_21= ',' ( (lv_eventParameterRanges_22_0= ruleEventParameterRanges ) ) )* )?
                    int alt10=2;
                    int LA10_0 = input.LA(1);

                    if ( (LA10_0==RULE_ID) ) {
                        alt10=1;
                    }
                    switch (alt10) {
                        case 1 :
                            // InternalSML.g:266:2: ( (lv_eventParameterRanges_20_0= ruleEventParameterRanges ) ) (otherlv_21= ',' ( (lv_eventParameterRanges_22_0= ruleEventParameterRanges ) ) )*
                            {
                            // InternalSML.g:266:2: ( (lv_eventParameterRanges_20_0= ruleEventParameterRanges ) )
                            // InternalSML.g:267:1: (lv_eventParameterRanges_20_0= ruleEventParameterRanges )
                            {
                            // InternalSML.g:267:1: (lv_eventParameterRanges_20_0= ruleEventParameterRanges )
                            // InternalSML.g:268:3: lv_eventParameterRanges_20_0= ruleEventParameterRanges
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getSpecificationAccess().getEventParameterRangesEventParameterRangesParserRuleCall_9_2_0_0()); 
                              	    
                            }
                            pushFollow(FollowSets000.FOLLOW_12);
                            lv_eventParameterRanges_20_0=ruleEventParameterRanges();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getSpecificationRule());
                              	        }
                                     		add(
                                     			current, 
                                     			"eventParameterRanges",
                                      		lv_eventParameterRanges_20_0, 
                                      		"org.scenariotools.sml.SML.EventParameterRanges");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }

                            // InternalSML.g:284:2: (otherlv_21= ',' ( (lv_eventParameterRanges_22_0= ruleEventParameterRanges ) ) )*
                            loop9:
                            do {
                                int alt9=2;
                                int LA9_0 = input.LA(1);

                                if ( (LA9_0==22) ) {
                                    alt9=1;
                                }


                                switch (alt9) {
                            	case 1 :
                            	    // InternalSML.g:284:4: otherlv_21= ',' ( (lv_eventParameterRanges_22_0= ruleEventParameterRanges ) )
                            	    {
                            	    otherlv_21=(Token)match(input,22,FollowSets000.FOLLOW_4); if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	          	newLeafNode(otherlv_21, grammarAccess.getSpecificationAccess().getCommaKeyword_9_2_1_0());
                            	          
                            	    }
                            	    // InternalSML.g:288:1: ( (lv_eventParameterRanges_22_0= ruleEventParameterRanges ) )
                            	    // InternalSML.g:289:1: (lv_eventParameterRanges_22_0= ruleEventParameterRanges )
                            	    {
                            	    // InternalSML.g:289:1: (lv_eventParameterRanges_22_0= ruleEventParameterRanges )
                            	    // InternalSML.g:290:3: lv_eventParameterRanges_22_0= ruleEventParameterRanges
                            	    {
                            	    if ( state.backtracking==0 ) {
                            	       
                            	      	        newCompositeNode(grammarAccess.getSpecificationAccess().getEventParameterRangesEventParameterRangesParserRuleCall_9_2_1_1_0()); 
                            	      	    
                            	    }
                            	    pushFollow(FollowSets000.FOLLOW_12);
                            	    lv_eventParameterRanges_22_0=ruleEventParameterRanges();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      	        if (current==null) {
                            	      	            current = createModelElementForParent(grammarAccess.getSpecificationRule());
                            	      	        }
                            	             		add(
                            	             			current, 
                            	             			"eventParameterRanges",
                            	              		lv_eventParameterRanges_22_0, 
                            	              		"org.scenariotools.sml.SML.EventParameterRanges");
                            	      	        afterParserOrEnumRuleCall();
                            	      	    
                            	    }

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop9;
                                }
                            } while (true);


                            }
                            break;

                    }

                    otherlv_23=(Token)match(input,19,FollowSets000.FOLLOW_13); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_23, grammarAccess.getSpecificationAccess().getRightCurlyBracketKeyword_9_3());
                          
                    }

                    }
                    break;

            }

            // InternalSML.g:310:3: ( ( (lv_containedCollaborations_24_0= ruleNestedCollaboration ) ) | (otherlv_25= 'include collaboration' ( (otherlv_26= RULE_ID ) ) ) )*
            loop12:
            do {
                int alt12=3;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==28) ) {
                    alt12=1;
                }
                else if ( (LA12_0==23) ) {
                    alt12=2;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalSML.g:310:4: ( (lv_containedCollaborations_24_0= ruleNestedCollaboration ) )
            	    {
            	    // InternalSML.g:310:4: ( (lv_containedCollaborations_24_0= ruleNestedCollaboration ) )
            	    // InternalSML.g:311:1: (lv_containedCollaborations_24_0= ruleNestedCollaboration )
            	    {
            	    // InternalSML.g:311:1: (lv_containedCollaborations_24_0= ruleNestedCollaboration )
            	    // InternalSML.g:312:3: lv_containedCollaborations_24_0= ruleNestedCollaboration
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getSpecificationAccess().getContainedCollaborationsNestedCollaborationParserRuleCall_10_0_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_13);
            	    lv_containedCollaborations_24_0=ruleNestedCollaboration();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getSpecificationRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"containedCollaborations",
            	              		lv_containedCollaborations_24_0, 
            	              		"org.scenariotools.sml.SML.NestedCollaboration");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalSML.g:329:6: (otherlv_25= 'include collaboration' ( (otherlv_26= RULE_ID ) ) )
            	    {
            	    // InternalSML.g:329:6: (otherlv_25= 'include collaboration' ( (otherlv_26= RULE_ID ) ) )
            	    // InternalSML.g:329:8: otherlv_25= 'include collaboration' ( (otherlv_26= RULE_ID ) )
            	    {
            	    otherlv_25=(Token)match(input,23,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_25, grammarAccess.getSpecificationAccess().getIncludeCollaborationKeyword_10_1_0());
            	          
            	    }
            	    // InternalSML.g:333:1: ( (otherlv_26= RULE_ID ) )
            	    // InternalSML.g:334:1: (otherlv_26= RULE_ID )
            	    {
            	    // InternalSML.g:334:1: (otherlv_26= RULE_ID )
            	    // InternalSML.g:335:3: otherlv_26= RULE_ID
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getSpecificationRule());
            	      	        }
            	              
            	    }
            	    otherlv_26=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_13); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		newLeafNode(otherlv_26, grammarAccess.getSpecificationAccess().getIncludedCollaborationsCollaborationCrossReference_10_1_1_0()); 
            	      	
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            otherlv_27=(Token)match(input,19,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_27, grammarAccess.getSpecificationAccess().getRightCurlyBracketKeyword_11());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSpecification"


    // $ANTLR start "entryRuleFQNENUM"
    // InternalSML.g:358:1: entryRuleFQNENUM returns [String current=null] : iv_ruleFQNENUM= ruleFQNENUM EOF ;
    public final String entryRuleFQNENUM() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFQNENUM = null;


        try {
            // InternalSML.g:359:2: (iv_ruleFQNENUM= ruleFQNENUM EOF )
            // InternalSML.g:360:2: iv_ruleFQNENUM= ruleFQNENUM EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFQNENUMRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleFQNENUM=ruleFQNENUM();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFQNENUM.getText(); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFQNENUM"


    // $ANTLR start "ruleFQNENUM"
    // InternalSML.g:367:1: ruleFQNENUM returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= ':' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleFQNENUM() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;

         enterRule(); 
            
        try {
            // InternalSML.g:370:28: ( (this_ID_0= RULE_ID (kw= ':' this_ID_2= RULE_ID )* ) )
            // InternalSML.g:371:1: (this_ID_0= RULE_ID (kw= ':' this_ID_2= RULE_ID )* )
            {
            // InternalSML.g:371:1: (this_ID_0= RULE_ID (kw= ':' this_ID_2= RULE_ID )* )
            // InternalSML.g:371:6: this_ID_0= RULE_ID (kw= ':' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_14); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current.merge(this_ID_0);
                  
            }
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_ID_0, grammarAccess.getFQNENUMAccess().getIDTerminalRuleCall_0()); 
                  
            }
            // InternalSML.g:378:1: (kw= ':' this_ID_2= RULE_ID )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==24) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalSML.g:379:2: kw= ':' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,24,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	              current.merge(kw);
            	              newLeafNode(kw, grammarAccess.getFQNENUMAccess().getColonKeyword_1_0()); 
            	          
            	    }
            	    this_ID_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_14); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		current.merge(this_ID_2);
            	          
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	          newLeafNode(this_ID_2, grammarAccess.getFQNENUMAccess().getIDTerminalRuleCall_1_1()); 
            	          
            	    }

            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFQNENUM"


    // $ANTLR start "entryRuleChannelOptions"
    // InternalSML.g:399:1: entryRuleChannelOptions returns [EObject current=null] : iv_ruleChannelOptions= ruleChannelOptions EOF ;
    public final EObject entryRuleChannelOptions() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleChannelOptions = null;


        try {
            // InternalSML.g:400:2: (iv_ruleChannelOptions= ruleChannelOptions EOF )
            // InternalSML.g:401:2: iv_ruleChannelOptions= ruleChannelOptions EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getChannelOptionsRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleChannelOptions=ruleChannelOptions();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleChannelOptions; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleChannelOptions"


    // $ANTLR start "ruleChannelOptions"
    // InternalSML.g:408:1: ruleChannelOptions returns [EObject current=null] : ( () (otherlv_1= 'channels' otherlv_2= '{' ( (lv_allMessagesRequireLinks_3_0= 'all messages require links' ) )? ( (lv_messageChannels_4_0= ruleMessageChannel ) )* otherlv_5= '}' )? ) ;
    public final EObject ruleChannelOptions() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_allMessagesRequireLinks_3_0=null;
        Token otherlv_5=null;
        EObject lv_messageChannels_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:411:28: ( ( () (otherlv_1= 'channels' otherlv_2= '{' ( (lv_allMessagesRequireLinks_3_0= 'all messages require links' ) )? ( (lv_messageChannels_4_0= ruleMessageChannel ) )* otherlv_5= '}' )? ) )
            // InternalSML.g:412:1: ( () (otherlv_1= 'channels' otherlv_2= '{' ( (lv_allMessagesRequireLinks_3_0= 'all messages require links' ) )? ( (lv_messageChannels_4_0= ruleMessageChannel ) )* otherlv_5= '}' )? )
            {
            // InternalSML.g:412:1: ( () (otherlv_1= 'channels' otherlv_2= '{' ( (lv_allMessagesRequireLinks_3_0= 'all messages require links' ) )? ( (lv_messageChannels_4_0= ruleMessageChannel ) )* otherlv_5= '}' )? )
            // InternalSML.g:412:2: () (otherlv_1= 'channels' otherlv_2= '{' ( (lv_allMessagesRequireLinks_3_0= 'all messages require links' ) )? ( (lv_messageChannels_4_0= ruleMessageChannel ) )* otherlv_5= '}' )?
            {
            // InternalSML.g:412:2: ()
            // InternalSML.g:413:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getChannelOptionsAccess().getChannelOptionsAction_0(),
                          current);
                  
            }

            }

            // InternalSML.g:418:2: (otherlv_1= 'channels' otherlv_2= '{' ( (lv_allMessagesRequireLinks_3_0= 'all messages require links' ) )? ( (lv_messageChannels_4_0= ruleMessageChannel ) )* otherlv_5= '}' )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==25) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalSML.g:418:4: otherlv_1= 'channels' otherlv_2= '{' ( (lv_allMessagesRequireLinks_3_0= 'all messages require links' ) )? ( (lv_messageChannels_4_0= ruleMessageChannel ) )* otherlv_5= '}'
                    {
                    otherlv_1=(Token)match(input,25,FollowSets000.FOLLOW_5); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getChannelOptionsAccess().getChannelsKeyword_1_0());
                          
                    }
                    otherlv_2=(Token)match(input,15,FollowSets000.FOLLOW_15); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getChannelOptionsAccess().getLeftCurlyBracketKeyword_1_1());
                          
                    }
                    // InternalSML.g:426:1: ( (lv_allMessagesRequireLinks_3_0= 'all messages require links' ) )?
                    int alt14=2;
                    int LA14_0 = input.LA(1);

                    if ( (LA14_0==26) ) {
                        alt14=1;
                    }
                    switch (alt14) {
                        case 1 :
                            // InternalSML.g:427:1: (lv_allMessagesRequireLinks_3_0= 'all messages require links' )
                            {
                            // InternalSML.g:427:1: (lv_allMessagesRequireLinks_3_0= 'all messages require links' )
                            // InternalSML.g:428:3: lv_allMessagesRequireLinks_3_0= 'all messages require links'
                            {
                            lv_allMessagesRequireLinks_3_0=(Token)match(input,26,FollowSets000.FOLLOW_8); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_allMessagesRequireLinks_3_0, grammarAccess.getChannelOptionsAccess().getAllMessagesRequireLinksAllMessagesRequireLinksKeyword_1_2_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getChannelOptionsRule());
                              	        }
                                     		setWithLastConsumed(current, "allMessagesRequireLinks", true, "all messages require links");
                              	    
                            }

                            }


                            }
                            break;

                    }

                    // InternalSML.g:441:3: ( (lv_messageChannels_4_0= ruleMessageChannel ) )*
                    loop15:
                    do {
                        int alt15=2;
                        int LA15_0 = input.LA(1);

                        if ( (LA15_0==RULE_ID) ) {
                            alt15=1;
                        }


                        switch (alt15) {
                    	case 1 :
                    	    // InternalSML.g:442:1: (lv_messageChannels_4_0= ruleMessageChannel )
                    	    {
                    	    // InternalSML.g:442:1: (lv_messageChannels_4_0= ruleMessageChannel )
                    	    // InternalSML.g:443:3: lv_messageChannels_4_0= ruleMessageChannel
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getChannelOptionsAccess().getMessageChannelsMessageChannelParserRuleCall_1_3_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_8);
                    	    lv_messageChannels_4_0=ruleMessageChannel();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getChannelOptionsRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"messageChannels",
                    	              		lv_messageChannels_4_0, 
                    	              		"org.scenariotools.sml.SML.MessageChannel");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop15;
                        }
                    } while (true);

                    otherlv_5=(Token)match(input,19,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_5, grammarAccess.getChannelOptionsAccess().getRightCurlyBracketKeyword_1_4());
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleChannelOptions"


    // $ANTLR start "entryRuleMessageChannel"
    // InternalSML.g:471:1: entryRuleMessageChannel returns [EObject current=null] : iv_ruleMessageChannel= ruleMessageChannel EOF ;
    public final EObject entryRuleMessageChannel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMessageChannel = null;


        try {
            // InternalSML.g:472:2: (iv_ruleMessageChannel= ruleMessageChannel EOF )
            // InternalSML.g:473:2: iv_ruleMessageChannel= ruleMessageChannel EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMessageChannelRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleMessageChannel=ruleMessageChannel();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMessageChannel; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMessageChannel"


    // $ANTLR start "ruleMessageChannel"
    // InternalSML.g:480:1: ruleMessageChannel returns [EObject current=null] : ( ( ( ruleFQN ) ) otherlv_1= 'over' ( ( ruleFQN ) ) ) ;
    public final EObject ruleMessageChannel() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalSML.g:483:28: ( ( ( ( ruleFQN ) ) otherlv_1= 'over' ( ( ruleFQN ) ) ) )
            // InternalSML.g:484:1: ( ( ( ruleFQN ) ) otherlv_1= 'over' ( ( ruleFQN ) ) )
            {
            // InternalSML.g:484:1: ( ( ( ruleFQN ) ) otherlv_1= 'over' ( ( ruleFQN ) ) )
            // InternalSML.g:484:2: ( ( ruleFQN ) ) otherlv_1= 'over' ( ( ruleFQN ) )
            {
            // InternalSML.g:484:2: ( ( ruleFQN ) )
            // InternalSML.g:485:1: ( ruleFQN )
            {
            // InternalSML.g:485:1: ( ruleFQN )
            // InternalSML.g:486:3: ruleFQN
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getMessageChannelRule());
              	        }
                      
            }
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getMessageChannelAccess().getEventETypedElementCrossReference_0_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_16);
            ruleFQN();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,27,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getMessageChannelAccess().getOverKeyword_1());
                  
            }
            // InternalSML.g:503:1: ( ( ruleFQN ) )
            // InternalSML.g:504:1: ( ruleFQN )
            {
            // InternalSML.g:504:1: ( ruleFQN )
            // InternalSML.g:505:3: ruleFQN
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getMessageChannelRule());
              	        }
                      
            }
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getMessageChannelAccess().getChannelFeatureEStructuralFeatureCrossReference_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleFQN();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMessageChannel"


    // $ANTLR start "entryRuleNestedCollaboration"
    // InternalSML.g:526:1: entryRuleNestedCollaboration returns [EObject current=null] : iv_ruleNestedCollaboration= ruleNestedCollaboration EOF ;
    public final EObject entryRuleNestedCollaboration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNestedCollaboration = null;


        try {
            // InternalSML.g:527:2: (iv_ruleNestedCollaboration= ruleNestedCollaboration EOF )
            // InternalSML.g:528:2: iv_ruleNestedCollaboration= ruleNestedCollaboration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNestedCollaborationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNestedCollaboration=ruleNestedCollaboration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNestedCollaboration; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNestedCollaboration"


    // $ANTLR start "ruleNestedCollaboration"
    // InternalSML.g:535:1: ruleNestedCollaboration returns [EObject current=null] : (otherlv_0= 'collaboration' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_roles_3_0= ruleRole ) )* ( (lv_scenarios_4_0= ruleScenario ) )* otherlv_5= '}' ) ;
    public final EObject ruleNestedCollaboration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        EObject lv_roles_3_0 = null;

        EObject lv_scenarios_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:538:28: ( (otherlv_0= 'collaboration' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_roles_3_0= ruleRole ) )* ( (lv_scenarios_4_0= ruleScenario ) )* otherlv_5= '}' ) )
            // InternalSML.g:539:1: (otherlv_0= 'collaboration' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_roles_3_0= ruleRole ) )* ( (lv_scenarios_4_0= ruleScenario ) )* otherlv_5= '}' )
            {
            // InternalSML.g:539:1: (otherlv_0= 'collaboration' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_roles_3_0= ruleRole ) )* ( (lv_scenarios_4_0= ruleScenario ) )* otherlv_5= '}' )
            // InternalSML.g:539:3: otherlv_0= 'collaboration' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_roles_3_0= ruleRole ) )* ( (lv_scenarios_4_0= ruleScenario ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,28,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getNestedCollaborationAccess().getCollaborationKeyword_0());
                  
            }
            // InternalSML.g:543:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalSML.g:544:1: (lv_name_1_0= RULE_ID )
            {
            // InternalSML.g:544:1: (lv_name_1_0= RULE_ID )
            // InternalSML.g:545:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getNestedCollaborationAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getNestedCollaborationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,15,FollowSets000.FOLLOW_17); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getNestedCollaborationAccess().getLeftCurlyBracketKeyword_2());
                  
            }
            // InternalSML.g:565:1: ( (lv_roles_3_0= ruleRole ) )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( ((LA17_0>=36 && LA17_0<=37)) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalSML.g:566:1: (lv_roles_3_0= ruleRole )
            	    {
            	    // InternalSML.g:566:1: (lv_roles_3_0= ruleRole )
            	    // InternalSML.g:567:3: lv_roles_3_0= ruleRole
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getNestedCollaborationAccess().getRolesRoleParserRuleCall_3_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_17);
            	    lv_roles_3_0=ruleRole();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getNestedCollaborationRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"roles",
            	              		lv_roles_3_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.Role");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

            // InternalSML.g:583:3: ( (lv_scenarios_4_0= ruleScenario ) )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==40||(LA18_0>=84 && LA18_0<=86)) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalSML.g:584:1: (lv_scenarios_4_0= ruleScenario )
            	    {
            	    // InternalSML.g:584:1: (lv_scenarios_4_0= ruleScenario )
            	    // InternalSML.g:585:3: lv_scenarios_4_0= ruleScenario
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getNestedCollaborationAccess().getScenariosScenarioParserRuleCall_4_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_18);
            	    lv_scenarios_4_0=ruleScenario();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getNestedCollaborationRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"scenarios",
            	              		lv_scenarios_4_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.Scenario");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

            otherlv_5=(Token)match(input,19,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getNestedCollaborationAccess().getRightCurlyBracketKeyword_5());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNestedCollaboration"


    // $ANTLR start "entryRuleEventParameterRanges"
    // InternalSML.g:613:1: entryRuleEventParameterRanges returns [EObject current=null] : iv_ruleEventParameterRanges= ruleEventParameterRanges EOF ;
    public final EObject entryRuleEventParameterRanges() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEventParameterRanges = null;


        try {
            // InternalSML.g:614:2: (iv_ruleEventParameterRanges= ruleEventParameterRanges EOF )
            // InternalSML.g:615:2: iv_ruleEventParameterRanges= ruleEventParameterRanges EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEventParameterRangesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleEventParameterRanges=ruleEventParameterRanges();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEventParameterRanges; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEventParameterRanges"


    // $ANTLR start "ruleEventParameterRanges"
    // InternalSML.g:622:1: ruleEventParameterRanges returns [EObject current=null] : ( ( ( ruleFQN ) ) otherlv_1= '(' ( (lv_rangesForParameter_2_0= ruleRangesForParameter ) ) (otherlv_3= ',' ( (lv_rangesForParameter_4_0= ruleRangesForParameter ) ) )* otherlv_5= ')' ) ;
    public final EObject ruleEventParameterRanges() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_rangesForParameter_2_0 = null;

        EObject lv_rangesForParameter_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:625:28: ( ( ( ( ruleFQN ) ) otherlv_1= '(' ( (lv_rangesForParameter_2_0= ruleRangesForParameter ) ) (otherlv_3= ',' ( (lv_rangesForParameter_4_0= ruleRangesForParameter ) ) )* otherlv_5= ')' ) )
            // InternalSML.g:626:1: ( ( ( ruleFQN ) ) otherlv_1= '(' ( (lv_rangesForParameter_2_0= ruleRangesForParameter ) ) (otherlv_3= ',' ( (lv_rangesForParameter_4_0= ruleRangesForParameter ) ) )* otherlv_5= ')' )
            {
            // InternalSML.g:626:1: ( ( ( ruleFQN ) ) otherlv_1= '(' ( (lv_rangesForParameter_2_0= ruleRangesForParameter ) ) (otherlv_3= ',' ( (lv_rangesForParameter_4_0= ruleRangesForParameter ) ) )* otherlv_5= ')' )
            // InternalSML.g:626:2: ( ( ruleFQN ) ) otherlv_1= '(' ( (lv_rangesForParameter_2_0= ruleRangesForParameter ) ) (otherlv_3= ',' ( (lv_rangesForParameter_4_0= ruleRangesForParameter ) ) )* otherlv_5= ')'
            {
            // InternalSML.g:626:2: ( ( ruleFQN ) )
            // InternalSML.g:627:1: ( ruleFQN )
            {
            // InternalSML.g:627:1: ( ruleFQN )
            // InternalSML.g:628:3: ruleFQN
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getEventParameterRangesRule());
              	        }
                      
            }
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getEventParameterRangesAccess().getEventETypedElementCrossReference_0_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_19);
            ruleFQN();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,29,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getEventParameterRangesAccess().getLeftParenthesisKeyword_1());
                  
            }
            // InternalSML.g:645:1: ( (lv_rangesForParameter_2_0= ruleRangesForParameter ) )
            // InternalSML.g:646:1: (lv_rangesForParameter_2_0= ruleRangesForParameter )
            {
            // InternalSML.g:646:1: (lv_rangesForParameter_2_0= ruleRangesForParameter )
            // InternalSML.g:647:3: lv_rangesForParameter_2_0= ruleRangesForParameter
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getEventParameterRangesAccess().getRangesForParameterRangesForParameterParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_20);
            lv_rangesForParameter_2_0=ruleRangesForParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getEventParameterRangesRule());
              	        }
                     		add(
                     			current, 
                     			"rangesForParameter",
                      		lv_rangesForParameter_2_0, 
                      		"org.scenariotools.sml.SML.RangesForParameter");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalSML.g:663:2: (otherlv_3= ',' ( (lv_rangesForParameter_4_0= ruleRangesForParameter ) ) )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==22) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalSML.g:663:4: otherlv_3= ',' ( (lv_rangesForParameter_4_0= ruleRangesForParameter ) )
            	    {
            	    otherlv_3=(Token)match(input,22,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getEventParameterRangesAccess().getCommaKeyword_3_0());
            	          
            	    }
            	    // InternalSML.g:667:1: ( (lv_rangesForParameter_4_0= ruleRangesForParameter ) )
            	    // InternalSML.g:668:1: (lv_rangesForParameter_4_0= ruleRangesForParameter )
            	    {
            	    // InternalSML.g:668:1: (lv_rangesForParameter_4_0= ruleRangesForParameter )
            	    // InternalSML.g:669:3: lv_rangesForParameter_4_0= ruleRangesForParameter
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getEventParameterRangesAccess().getRangesForParameterRangesForParameterParserRuleCall_3_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_20);
            	    lv_rangesForParameter_4_0=ruleRangesForParameter();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getEventParameterRangesRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"rangesForParameter",
            	              		lv_rangesForParameter_4_0, 
            	              		"org.scenariotools.sml.SML.RangesForParameter");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

            otherlv_5=(Token)match(input,30,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getEventParameterRangesAccess().getRightParenthesisKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEventParameterRanges"


    // $ANTLR start "entryRuleRangesForParameter"
    // InternalSML.g:697:1: entryRuleRangesForParameter returns [EObject current=null] : iv_ruleRangesForParameter= ruleRangesForParameter EOF ;
    public final EObject entryRuleRangesForParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRangesForParameter = null;


        try {
            // InternalSML.g:698:2: (iv_ruleRangesForParameter= ruleRangesForParameter EOF )
            // InternalSML.g:699:2: iv_ruleRangesForParameter= ruleRangesForParameter EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRangesForParameterRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRangesForParameter=ruleRangesForParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRangesForParameter; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRangesForParameter"


    // $ANTLR start "ruleRangesForParameter"
    // InternalSML.g:706:1: ruleRangesForParameter returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_ranges_2_0= ruleAbstractRanges ) ) ) ;
    public final EObject ruleRangesForParameter() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_ranges_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:709:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_ranges_2_0= ruleAbstractRanges ) ) ) )
            // InternalSML.g:710:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_ranges_2_0= ruleAbstractRanges ) ) )
            {
            // InternalSML.g:710:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_ranges_2_0= ruleAbstractRanges ) ) )
            // InternalSML.g:710:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_ranges_2_0= ruleAbstractRanges ) )
            {
            // InternalSML.g:710:2: ( (otherlv_0= RULE_ID ) )
            // InternalSML.g:711:1: (otherlv_0= RULE_ID )
            {
            // InternalSML.g:711:1: (otherlv_0= RULE_ID )
            // InternalSML.g:712:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getRangesForParameterRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_21); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getRangesForParameterAccess().getParameterETypedElementCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,31,FollowSets000.FOLLOW_22); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getRangesForParameterAccess().getEqualsSignKeyword_1());
                  
            }
            // InternalSML.g:727:1: ( (lv_ranges_2_0= ruleAbstractRanges ) )
            // InternalSML.g:728:1: (lv_ranges_2_0= ruleAbstractRanges )
            {
            // InternalSML.g:728:1: (lv_ranges_2_0= ruleAbstractRanges )
            // InternalSML.g:729:3: lv_ranges_2_0= ruleAbstractRanges
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getRangesForParameterAccess().getRangesAbstractRangesParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_ranges_2_0=ruleAbstractRanges();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getRangesForParameterRule());
              	        }
                     		set(
                     			current, 
                     			"ranges",
                      		lv_ranges_2_0, 
                      		"org.scenariotools.sml.SML.AbstractRanges");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRangesForParameter"


    // $ANTLR start "entryRuleAbstractRanges"
    // InternalSML.g:753:1: entryRuleAbstractRanges returns [EObject current=null] : iv_ruleAbstractRanges= ruleAbstractRanges EOF ;
    public final EObject entryRuleAbstractRanges() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractRanges = null;


        try {
            // InternalSML.g:754:2: (iv_ruleAbstractRanges= ruleAbstractRanges EOF )
            // InternalSML.g:755:2: iv_ruleAbstractRanges= ruleAbstractRanges EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAbstractRangesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAbstractRanges=ruleAbstractRanges();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAbstractRanges; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractRanges"


    // $ANTLR start "ruleAbstractRanges"
    // InternalSML.g:762:1: ruleAbstractRanges returns [EObject current=null] : (otherlv_0= '[' (this_IntegerRanges_1= ruleIntegerRanges | this_StringRanges_2= ruleStringRanges | this_EnumRanges_3= ruleEnumRanges ) otherlv_4= ']' ) ;
    public final EObject ruleAbstractRanges() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_4=null;
        EObject this_IntegerRanges_1 = null;

        EObject this_StringRanges_2 = null;

        EObject this_EnumRanges_3 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:765:28: ( (otherlv_0= '[' (this_IntegerRanges_1= ruleIntegerRanges | this_StringRanges_2= ruleStringRanges | this_EnumRanges_3= ruleEnumRanges ) otherlv_4= ']' ) )
            // InternalSML.g:766:1: (otherlv_0= '[' (this_IntegerRanges_1= ruleIntegerRanges | this_StringRanges_2= ruleStringRanges | this_EnumRanges_3= ruleEnumRanges ) otherlv_4= ']' )
            {
            // InternalSML.g:766:1: (otherlv_0= '[' (this_IntegerRanges_1= ruleIntegerRanges | this_StringRanges_2= ruleStringRanges | this_EnumRanges_3= ruleEnumRanges ) otherlv_4= ']' )
            // InternalSML.g:766:3: otherlv_0= '[' (this_IntegerRanges_1= ruleIntegerRanges | this_StringRanges_2= ruleStringRanges | this_EnumRanges_3= ruleEnumRanges ) otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,32,FollowSets000.FOLLOW_23); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getAbstractRangesAccess().getLeftSquareBracketKeyword_0());
                  
            }
            // InternalSML.g:770:1: (this_IntegerRanges_1= ruleIntegerRanges | this_StringRanges_2= ruleStringRanges | this_EnumRanges_3= ruleEnumRanges )
            int alt20=3;
            switch ( input.LA(1) ) {
            case RULE_INT:
            case RULE_SIGNEDINT:
                {
                alt20=1;
                }
                break;
            case RULE_STRING:
                {
                alt20=2;
                }
                break;
            case RULE_ID:
                {
                alt20=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }

            switch (alt20) {
                case 1 :
                    // InternalSML.g:771:5: this_IntegerRanges_1= ruleIntegerRanges
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getAbstractRangesAccess().getIntegerRangesParserRuleCall_1_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_24);
                    this_IntegerRanges_1=ruleIntegerRanges();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IntegerRanges_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:781:5: this_StringRanges_2= ruleStringRanges
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getAbstractRangesAccess().getStringRangesParserRuleCall_1_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_24);
                    this_StringRanges_2=ruleStringRanges();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_StringRanges_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalSML.g:791:5: this_EnumRanges_3= ruleEnumRanges
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getAbstractRangesAccess().getEnumRangesParserRuleCall_1_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_24);
                    this_EnumRanges_3=ruleEnumRanges();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_EnumRanges_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }

            otherlv_4=(Token)match(input,33,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getAbstractRangesAccess().getRightSquareBracketKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractRanges"


    // $ANTLR start "entryRuleIntegerRanges"
    // InternalSML.g:811:1: entryRuleIntegerRanges returns [EObject current=null] : iv_ruleIntegerRanges= ruleIntegerRanges EOF ;
    public final EObject entryRuleIntegerRanges() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerRanges = null;


        try {
            // InternalSML.g:812:2: (iv_ruleIntegerRanges= ruleIntegerRanges EOF )
            // InternalSML.g:813:2: iv_ruleIntegerRanges= ruleIntegerRanges EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerRangesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntegerRanges=ruleIntegerRanges();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerRanges; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerRanges"


    // $ANTLR start "ruleIntegerRanges"
    // InternalSML.g:820:1: ruleIntegerRanges returns [EObject current=null] : ( ( ( ( ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) ) ) otherlv_1= '..' ( ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) ) ) ) (otherlv_3= ',' ( ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )* ) )? ) | ( ( ( (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT ) ) ) (otherlv_8= ',' ( ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) ) ) )* ) ) ;
    public final EObject ruleIntegerRanges() throws RecognitionException {
        EObject current = null;

        Token lv_min_0_1=null;
        Token lv_min_0_2=null;
        Token otherlv_1=null;
        Token lv_max_2_1=null;
        Token lv_max_2_2=null;
        Token otherlv_3=null;
        Token lv_values_4_1=null;
        Token lv_values_4_2=null;
        Token otherlv_5=null;
        Token lv_values_6_1=null;
        Token lv_values_6_2=null;
        Token lv_values_7_1=null;
        Token lv_values_7_2=null;
        Token otherlv_8=null;
        Token lv_values_9_1=null;
        Token lv_values_9_2=null;

         enterRule(); 
            
        try {
            // InternalSML.g:823:28: ( ( ( ( ( ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) ) ) otherlv_1= '..' ( ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) ) ) ) (otherlv_3= ',' ( ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )* ) )? ) | ( ( ( (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT ) ) ) (otherlv_8= ',' ( ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) ) ) )* ) ) )
            // InternalSML.g:824:1: ( ( ( ( ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) ) ) otherlv_1= '..' ( ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) ) ) ) (otherlv_3= ',' ( ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )* ) )? ) | ( ( ( (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT ) ) ) (otherlv_8= ',' ( ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) ) ) )* ) )
            {
            // InternalSML.g:824:1: ( ( ( ( ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) ) ) otherlv_1= '..' ( ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) ) ) ) (otherlv_3= ',' ( ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )* ) )? ) | ( ( ( (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT ) ) ) (otherlv_8= ',' ( ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) ) ) )* ) )
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==RULE_INT) ) {
                int LA30_1 = input.LA(2);

                if ( (LA30_1==EOF||LA30_1==22||LA30_1==33) ) {
                    alt30=2;
                }
                else if ( (LA30_1==34) ) {
                    alt30=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 30, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA30_0==RULE_SIGNEDINT) ) {
                int LA30_2 = input.LA(2);

                if ( (LA30_2==34) ) {
                    alt30=1;
                }
                else if ( (LA30_2==EOF||LA30_2==22||LA30_2==33) ) {
                    alt30=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 30, 2, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }
            switch (alt30) {
                case 1 :
                    // InternalSML.g:824:2: ( ( ( ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) ) ) otherlv_1= '..' ( ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) ) ) ) (otherlv_3= ',' ( ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )* ) )? )
                    {
                    // InternalSML.g:824:2: ( ( ( ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) ) ) otherlv_1= '..' ( ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) ) ) ) (otherlv_3= ',' ( ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )* ) )? )
                    // InternalSML.g:824:3: ( ( ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) ) ) otherlv_1= '..' ( ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) ) ) ) (otherlv_3= ',' ( ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )* ) )?
                    {
                    // InternalSML.g:824:3: ( ( ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) ) ) otherlv_1= '..' ( ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) ) ) )
                    // InternalSML.g:824:4: ( ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) ) ) otherlv_1= '..' ( ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) ) )
                    {
                    // InternalSML.g:824:4: ( ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) ) )
                    // InternalSML.g:825:1: ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) )
                    {
                    // InternalSML.g:825:1: ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) )
                    // InternalSML.g:826:1: (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT )
                    {
                    // InternalSML.g:826:1: (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT )
                    int alt21=2;
                    int LA21_0 = input.LA(1);

                    if ( (LA21_0==RULE_INT) ) {
                        alt21=1;
                    }
                    else if ( (LA21_0==RULE_SIGNEDINT) ) {
                        alt21=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 21, 0, input);

                        throw nvae;
                    }
                    switch (alt21) {
                        case 1 :
                            // InternalSML.g:827:3: lv_min_0_1= RULE_INT
                            {
                            lv_min_0_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_25); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              			newLeafNode(lv_min_0_1, grammarAccess.getIntegerRangesAccess().getMinINTTerminalRuleCall_0_0_0_0_0()); 
                              		
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                              	        }
                                     		setWithLastConsumed(
                                     			current, 
                                     			"min",
                                      		lv_min_0_1, 
                                      		"org.eclipse.xtext.common.Terminals.INT");
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalSML.g:842:8: lv_min_0_2= RULE_SIGNEDINT
                            {
                            lv_min_0_2=(Token)match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_25); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              			newLeafNode(lv_min_0_2, grammarAccess.getIntegerRangesAccess().getMinSIGNEDINTTerminalRuleCall_0_0_0_0_1()); 
                              		
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                              	        }
                                     		setWithLastConsumed(
                                     			current, 
                                     			"min",
                                      		lv_min_0_2, 
                                      		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    otherlv_1=(Token)match(input,34,FollowSets000.FOLLOW_26); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getIntegerRangesAccess().getFullStopFullStopKeyword_0_0_1());
                          
                    }
                    // InternalSML.g:864:1: ( ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) ) )
                    // InternalSML.g:865:1: ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) )
                    {
                    // InternalSML.g:865:1: ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) )
                    // InternalSML.g:866:1: (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT )
                    {
                    // InternalSML.g:866:1: (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT )
                    int alt22=2;
                    int LA22_0 = input.LA(1);

                    if ( (LA22_0==RULE_INT) ) {
                        alt22=1;
                    }
                    else if ( (LA22_0==RULE_SIGNEDINT) ) {
                        alt22=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 22, 0, input);

                        throw nvae;
                    }
                    switch (alt22) {
                        case 1 :
                            // InternalSML.g:867:3: lv_max_2_1= RULE_INT
                            {
                            lv_max_2_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_27); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              			newLeafNode(lv_max_2_1, grammarAccess.getIntegerRangesAccess().getMaxINTTerminalRuleCall_0_0_2_0_0()); 
                              		
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                              	        }
                                     		setWithLastConsumed(
                                     			current, 
                                     			"max",
                                      		lv_max_2_1, 
                                      		"org.eclipse.xtext.common.Terminals.INT");
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalSML.g:882:8: lv_max_2_2= RULE_SIGNEDINT
                            {
                            lv_max_2_2=(Token)match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_27); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              			newLeafNode(lv_max_2_2, grammarAccess.getIntegerRangesAccess().getMaxSIGNEDINTTerminalRuleCall_0_0_2_0_1()); 
                              		
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                              	        }
                                     		setWithLastConsumed(
                                     			current, 
                                     			"max",
                                      		lv_max_2_2, 
                                      		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }


                    }

                    // InternalSML.g:900:3: (otherlv_3= ',' ( ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )* ) )?
                    int alt26=2;
                    int LA26_0 = input.LA(1);

                    if ( (LA26_0==22) ) {
                        alt26=1;
                    }
                    switch (alt26) {
                        case 1 :
                            // InternalSML.g:900:5: otherlv_3= ',' ( ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )* )
                            {
                            otherlv_3=(Token)match(input,22,FollowSets000.FOLLOW_26); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                  	newLeafNode(otherlv_3, grammarAccess.getIntegerRangesAccess().getCommaKeyword_0_1_0());
                                  
                            }
                            // InternalSML.g:904:1: ( ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )* )
                            // InternalSML.g:904:2: ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )*
                            {
                            // InternalSML.g:904:2: ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) )
                            // InternalSML.g:905:1: ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) )
                            {
                            // InternalSML.g:905:1: ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) )
                            // InternalSML.g:906:1: (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT )
                            {
                            // InternalSML.g:906:1: (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT )
                            int alt23=2;
                            int LA23_0 = input.LA(1);

                            if ( (LA23_0==RULE_INT) ) {
                                alt23=1;
                            }
                            else if ( (LA23_0==RULE_SIGNEDINT) ) {
                                alt23=2;
                            }
                            else {
                                if (state.backtracking>0) {state.failed=true; return current;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 23, 0, input);

                                throw nvae;
                            }
                            switch (alt23) {
                                case 1 :
                                    // InternalSML.g:907:3: lv_values_4_1= RULE_INT
                                    {
                                    lv_values_4_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_27); if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      			newLeafNode(lv_values_4_1, grammarAccess.getIntegerRangesAccess().getValuesINTTerminalRuleCall_0_1_1_0_0_0()); 
                                      		
                                    }
                                    if ( state.backtracking==0 ) {

                                      	        if (current==null) {
                                      	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                                      	        }
                                             		addWithLastConsumed(
                                             			current, 
                                             			"values",
                                              		lv_values_4_1, 
                                              		"org.eclipse.xtext.common.Terminals.INT");
                                      	    
                                    }

                                    }
                                    break;
                                case 2 :
                                    // InternalSML.g:922:8: lv_values_4_2= RULE_SIGNEDINT
                                    {
                                    lv_values_4_2=(Token)match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_27); if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      			newLeafNode(lv_values_4_2, grammarAccess.getIntegerRangesAccess().getValuesSIGNEDINTTerminalRuleCall_0_1_1_0_0_1()); 
                                      		
                                    }
                                    if ( state.backtracking==0 ) {

                                      	        if (current==null) {
                                      	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                                      	        }
                                             		addWithLastConsumed(
                                             			current, 
                                             			"values",
                                              		lv_values_4_2, 
                                              		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
                                      	    
                                    }

                                    }
                                    break;

                            }


                            }


                            }

                            // InternalSML.g:940:2: (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )*
                            loop25:
                            do {
                                int alt25=2;
                                int LA25_0 = input.LA(1);

                                if ( (LA25_0==22) ) {
                                    alt25=1;
                                }


                                switch (alt25) {
                            	case 1 :
                            	    // InternalSML.g:940:4: otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) )
                            	    {
                            	    otherlv_5=(Token)match(input,22,FollowSets000.FOLLOW_26); if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	          	newLeafNode(otherlv_5, grammarAccess.getIntegerRangesAccess().getCommaKeyword_0_1_1_1_0());
                            	          
                            	    }
                            	    // InternalSML.g:944:1: ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) )
                            	    // InternalSML.g:945:1: ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) )
                            	    {
                            	    // InternalSML.g:945:1: ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) )
                            	    // InternalSML.g:946:1: (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT )
                            	    {
                            	    // InternalSML.g:946:1: (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT )
                            	    int alt24=2;
                            	    int LA24_0 = input.LA(1);

                            	    if ( (LA24_0==RULE_INT) ) {
                            	        alt24=1;
                            	    }
                            	    else if ( (LA24_0==RULE_SIGNEDINT) ) {
                            	        alt24=2;
                            	    }
                            	    else {
                            	        if (state.backtracking>0) {state.failed=true; return current;}
                            	        NoViableAltException nvae =
                            	            new NoViableAltException("", 24, 0, input);

                            	        throw nvae;
                            	    }
                            	    switch (alt24) {
                            	        case 1 :
                            	            // InternalSML.g:947:3: lv_values_6_1= RULE_INT
                            	            {
                            	            lv_values_6_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_27); if (state.failed) return current;
                            	            if ( state.backtracking==0 ) {

                            	              			newLeafNode(lv_values_6_1, grammarAccess.getIntegerRangesAccess().getValuesINTTerminalRuleCall_0_1_1_1_1_0_0()); 
                            	              		
                            	            }
                            	            if ( state.backtracking==0 ) {

                            	              	        if (current==null) {
                            	              	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                            	              	        }
                            	                     		addWithLastConsumed(
                            	                     			current, 
                            	                     			"values",
                            	                      		lv_values_6_1, 
                            	                      		"org.eclipse.xtext.common.Terminals.INT");
                            	              	    
                            	            }

                            	            }
                            	            break;
                            	        case 2 :
                            	            // InternalSML.g:962:8: lv_values_6_2= RULE_SIGNEDINT
                            	            {
                            	            lv_values_6_2=(Token)match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_27); if (state.failed) return current;
                            	            if ( state.backtracking==0 ) {

                            	              			newLeafNode(lv_values_6_2, grammarAccess.getIntegerRangesAccess().getValuesSIGNEDINTTerminalRuleCall_0_1_1_1_1_0_1()); 
                            	              		
                            	            }
                            	            if ( state.backtracking==0 ) {

                            	              	        if (current==null) {
                            	              	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                            	              	        }
                            	                     		addWithLastConsumed(
                            	                     			current, 
                            	                     			"values",
                            	                      		lv_values_6_2, 
                            	                      		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
                            	              	    
                            	            }

                            	            }
                            	            break;

                            	    }


                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop25;
                                }
                            } while (true);


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSML.g:981:6: ( ( ( (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT ) ) ) (otherlv_8= ',' ( ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) ) ) )* )
                    {
                    // InternalSML.g:981:6: ( ( ( (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT ) ) ) (otherlv_8= ',' ( ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) ) ) )* )
                    // InternalSML.g:981:7: ( ( (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT ) ) ) (otherlv_8= ',' ( ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) ) ) )*
                    {
                    // InternalSML.g:981:7: ( ( (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT ) ) )
                    // InternalSML.g:982:1: ( (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT ) )
                    {
                    // InternalSML.g:982:1: ( (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT ) )
                    // InternalSML.g:983:1: (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT )
                    {
                    // InternalSML.g:983:1: (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT )
                    int alt27=2;
                    int LA27_0 = input.LA(1);

                    if ( (LA27_0==RULE_INT) ) {
                        alt27=1;
                    }
                    else if ( (LA27_0==RULE_SIGNEDINT) ) {
                        alt27=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 27, 0, input);

                        throw nvae;
                    }
                    switch (alt27) {
                        case 1 :
                            // InternalSML.g:984:3: lv_values_7_1= RULE_INT
                            {
                            lv_values_7_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_27); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              			newLeafNode(lv_values_7_1, grammarAccess.getIntegerRangesAccess().getValuesINTTerminalRuleCall_1_0_0_0()); 
                              		
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                              	        }
                                     		addWithLastConsumed(
                                     			current, 
                                     			"values",
                                      		lv_values_7_1, 
                                      		"org.eclipse.xtext.common.Terminals.INT");
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalSML.g:999:8: lv_values_7_2= RULE_SIGNEDINT
                            {
                            lv_values_7_2=(Token)match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_27); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              			newLeafNode(lv_values_7_2, grammarAccess.getIntegerRangesAccess().getValuesSIGNEDINTTerminalRuleCall_1_0_0_1()); 
                              		
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                              	        }
                                     		addWithLastConsumed(
                                     			current, 
                                     			"values",
                                      		lv_values_7_2, 
                                      		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalSML.g:1017:2: (otherlv_8= ',' ( ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) ) ) )*
                    loop29:
                    do {
                        int alt29=2;
                        int LA29_0 = input.LA(1);

                        if ( (LA29_0==22) ) {
                            alt29=1;
                        }


                        switch (alt29) {
                    	case 1 :
                    	    // InternalSML.g:1017:4: otherlv_8= ',' ( ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) ) )
                    	    {
                    	    otherlv_8=(Token)match(input,22,FollowSets000.FOLLOW_26); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_8, grammarAccess.getIntegerRangesAccess().getCommaKeyword_1_1_0());
                    	          
                    	    }
                    	    // InternalSML.g:1021:1: ( ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) ) )
                    	    // InternalSML.g:1022:1: ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) )
                    	    {
                    	    // InternalSML.g:1022:1: ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) )
                    	    // InternalSML.g:1023:1: (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT )
                    	    {
                    	    // InternalSML.g:1023:1: (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT )
                    	    int alt28=2;
                    	    int LA28_0 = input.LA(1);

                    	    if ( (LA28_0==RULE_INT) ) {
                    	        alt28=1;
                    	    }
                    	    else if ( (LA28_0==RULE_SIGNEDINT) ) {
                    	        alt28=2;
                    	    }
                    	    else {
                    	        if (state.backtracking>0) {state.failed=true; return current;}
                    	        NoViableAltException nvae =
                    	            new NoViableAltException("", 28, 0, input);

                    	        throw nvae;
                    	    }
                    	    switch (alt28) {
                    	        case 1 :
                    	            // InternalSML.g:1024:3: lv_values_9_1= RULE_INT
                    	            {
                    	            lv_values_9_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_27); if (state.failed) return current;
                    	            if ( state.backtracking==0 ) {

                    	              			newLeafNode(lv_values_9_1, grammarAccess.getIntegerRangesAccess().getValuesINTTerminalRuleCall_1_1_1_0_0()); 
                    	              		
                    	            }
                    	            if ( state.backtracking==0 ) {

                    	              	        if (current==null) {
                    	              	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                    	              	        }
                    	                     		addWithLastConsumed(
                    	                     			current, 
                    	                     			"values",
                    	                      		lv_values_9_1, 
                    	                      		"org.eclipse.xtext.common.Terminals.INT");
                    	              	    
                    	            }

                    	            }
                    	            break;
                    	        case 2 :
                    	            // InternalSML.g:1039:8: lv_values_9_2= RULE_SIGNEDINT
                    	            {
                    	            lv_values_9_2=(Token)match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_27); if (state.failed) return current;
                    	            if ( state.backtracking==0 ) {

                    	              			newLeafNode(lv_values_9_2, grammarAccess.getIntegerRangesAccess().getValuesSIGNEDINTTerminalRuleCall_1_1_1_0_1()); 
                    	              		
                    	            }
                    	            if ( state.backtracking==0 ) {

                    	              	        if (current==null) {
                    	              	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                    	              	        }
                    	                     		addWithLastConsumed(
                    	                     			current, 
                    	                     			"values",
                    	                      		lv_values_9_2, 
                    	                      		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
                    	              	    
                    	            }

                    	            }
                    	            break;

                    	    }


                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop29;
                        }
                    } while (true);


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerRanges"


    // $ANTLR start "entryRuleStringRanges"
    // InternalSML.g:1065:1: entryRuleStringRanges returns [EObject current=null] : iv_ruleStringRanges= ruleStringRanges EOF ;
    public final EObject entryRuleStringRanges() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringRanges = null;


        try {
            // InternalSML.g:1066:2: (iv_ruleStringRanges= ruleStringRanges EOF )
            // InternalSML.g:1067:2: iv_ruleStringRanges= ruleStringRanges EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStringRangesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStringRanges=ruleStringRanges();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStringRanges; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringRanges"


    // $ANTLR start "ruleStringRanges"
    // InternalSML.g:1074:1: ruleStringRanges returns [EObject current=null] : ( ( (lv_values_0_0= RULE_STRING ) ) (otherlv_1= ',' ( (lv_values_2_0= RULE_STRING ) ) )* ) ;
    public final EObject ruleStringRanges() throws RecognitionException {
        EObject current = null;

        Token lv_values_0_0=null;
        Token otherlv_1=null;
        Token lv_values_2_0=null;

         enterRule(); 
            
        try {
            // InternalSML.g:1077:28: ( ( ( (lv_values_0_0= RULE_STRING ) ) (otherlv_1= ',' ( (lv_values_2_0= RULE_STRING ) ) )* ) )
            // InternalSML.g:1078:1: ( ( (lv_values_0_0= RULE_STRING ) ) (otherlv_1= ',' ( (lv_values_2_0= RULE_STRING ) ) )* )
            {
            // InternalSML.g:1078:1: ( ( (lv_values_0_0= RULE_STRING ) ) (otherlv_1= ',' ( (lv_values_2_0= RULE_STRING ) ) )* )
            // InternalSML.g:1078:2: ( (lv_values_0_0= RULE_STRING ) ) (otherlv_1= ',' ( (lv_values_2_0= RULE_STRING ) ) )*
            {
            // InternalSML.g:1078:2: ( (lv_values_0_0= RULE_STRING ) )
            // InternalSML.g:1079:1: (lv_values_0_0= RULE_STRING )
            {
            // InternalSML.g:1079:1: (lv_values_0_0= RULE_STRING )
            // InternalSML.g:1080:3: lv_values_0_0= RULE_STRING
            {
            lv_values_0_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_values_0_0, grammarAccess.getStringRangesAccess().getValuesSTRINGTerminalRuleCall_0_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getStringRangesRule());
              	        }
                     		addWithLastConsumed(
                     			current, 
                     			"values",
                      		lv_values_0_0, 
                      		"org.eclipse.xtext.common.Terminals.STRING");
              	    
            }

            }


            }

            // InternalSML.g:1096:2: (otherlv_1= ',' ( (lv_values_2_0= RULE_STRING ) ) )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==22) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // InternalSML.g:1096:4: otherlv_1= ',' ( (lv_values_2_0= RULE_STRING ) )
            	    {
            	    otherlv_1=(Token)match(input,22,FollowSets000.FOLLOW_28); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_1, grammarAccess.getStringRangesAccess().getCommaKeyword_1_0());
            	          
            	    }
            	    // InternalSML.g:1100:1: ( (lv_values_2_0= RULE_STRING ) )
            	    // InternalSML.g:1101:1: (lv_values_2_0= RULE_STRING )
            	    {
            	    // InternalSML.g:1101:1: (lv_values_2_0= RULE_STRING )
            	    // InternalSML.g:1102:3: lv_values_2_0= RULE_STRING
            	    {
            	    lv_values_2_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_27); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      			newLeafNode(lv_values_2_0, grammarAccess.getStringRangesAccess().getValuesSTRINGTerminalRuleCall_1_1_0()); 
            	      		
            	    }
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElement(grammarAccess.getStringRangesRule());
            	      	        }
            	             		addWithLastConsumed(
            	             			current, 
            	             			"values",
            	              		lv_values_2_0, 
            	              		"org.eclipse.xtext.common.Terminals.STRING");
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringRanges"


    // $ANTLR start "entryRuleEnumRanges"
    // InternalSML.g:1126:1: entryRuleEnumRanges returns [EObject current=null] : iv_ruleEnumRanges= ruleEnumRanges EOF ;
    public final EObject entryRuleEnumRanges() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumRanges = null;


        try {
            // InternalSML.g:1127:2: (iv_ruleEnumRanges= ruleEnumRanges EOF )
            // InternalSML.g:1128:2: iv_ruleEnumRanges= ruleEnumRanges EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEnumRangesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleEnumRanges=ruleEnumRanges();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEnumRanges; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumRanges"


    // $ANTLR start "ruleEnumRanges"
    // InternalSML.g:1135:1: ruleEnumRanges returns [EObject current=null] : ( ( ( ruleFQNENUM ) ) (otherlv_1= ',' ( ( ruleFQNENUM ) ) )* ) ;
    public final EObject ruleEnumRanges() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalSML.g:1138:28: ( ( ( ( ruleFQNENUM ) ) (otherlv_1= ',' ( ( ruleFQNENUM ) ) )* ) )
            // InternalSML.g:1139:1: ( ( ( ruleFQNENUM ) ) (otherlv_1= ',' ( ( ruleFQNENUM ) ) )* )
            {
            // InternalSML.g:1139:1: ( ( ( ruleFQNENUM ) ) (otherlv_1= ',' ( ( ruleFQNENUM ) ) )* )
            // InternalSML.g:1139:2: ( ( ruleFQNENUM ) ) (otherlv_1= ',' ( ( ruleFQNENUM ) ) )*
            {
            // InternalSML.g:1139:2: ( ( ruleFQNENUM ) )
            // InternalSML.g:1140:1: ( ruleFQNENUM )
            {
            // InternalSML.g:1140:1: ( ruleFQNENUM )
            // InternalSML.g:1141:3: ruleFQNENUM
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getEnumRangesRule());
              	        }
                      
            }
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getEnumRangesAccess().getValuesEEnumLiteralCrossReference_0_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_27);
            ruleFQNENUM();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalSML.g:1154:2: (otherlv_1= ',' ( ( ruleFQNENUM ) ) )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( (LA32_0==22) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // InternalSML.g:1154:4: otherlv_1= ',' ( ( ruleFQNENUM ) )
            	    {
            	    otherlv_1=(Token)match(input,22,FollowSets000.FOLLOW_23); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_1, grammarAccess.getEnumRangesAccess().getCommaKeyword_1_0());
            	          
            	    }
            	    // InternalSML.g:1158:1: ( ( ruleFQNENUM ) )
            	    // InternalSML.g:1159:1: ( ruleFQNENUM )
            	    {
            	    // InternalSML.g:1159:1: ( ruleFQNENUM )
            	    // InternalSML.g:1160:3: ruleFQNENUM
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getEnumRangesRule());
            	      	        }
            	              
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getEnumRangesAccess().getValuesEEnumLiteralCrossReference_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_27);
            	    ruleFQNENUM();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {
            	       
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumRanges"


    // $ANTLR start "entryRuleFQN"
    // InternalSML.g:1183:1: entryRuleFQN returns [String current=null] : iv_ruleFQN= ruleFQN EOF ;
    public final String entryRuleFQN() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFQN = null;


        try {
            // InternalSML.g:1184:2: (iv_ruleFQN= ruleFQN EOF )
            // InternalSML.g:1185:2: iv_ruleFQN= ruleFQN EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFQNRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleFQN=ruleFQN();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFQN.getText(); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFQN"


    // $ANTLR start "ruleFQN"
    // InternalSML.g:1192:1: ruleFQN returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleFQN() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;

         enterRule(); 
            
        try {
            // InternalSML.g:1195:28: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalSML.g:1196:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalSML.g:1196:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalSML.g:1196:6: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_29); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current.merge(this_ID_0);
                  
            }
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_ID_0, grammarAccess.getFQNAccess().getIDTerminalRuleCall_0()); 
                  
            }
            // InternalSML.g:1203:1: (kw= '.' this_ID_2= RULE_ID )*
            loop33:
            do {
                int alt33=2;
                int LA33_0 = input.LA(1);

                if ( (LA33_0==35) ) {
                    alt33=1;
                }


                switch (alt33) {
            	case 1 :
            	    // InternalSML.g:1204:2: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,35,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	              current.merge(kw);
            	              newLeafNode(kw, grammarAccess.getFQNAccess().getFullStopKeyword_1_0()); 
            	          
            	    }
            	    this_ID_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_29); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		current.merge(this_ID_2);
            	          
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	          newLeafNode(this_ID_2, grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1()); 
            	          
            	    }

            	    }
            	    break;

            	default :
            	    break loop33;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFQN"


    // $ANTLR start "entryRuleRole"
    // InternalSML.g:1224:1: entryRuleRole returns [EObject current=null] : iv_ruleRole= ruleRole EOF ;
    public final EObject entryRuleRole() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRole = null;


        try {
            // InternalSML.g:1225:2: (iv_ruleRole= ruleRole EOF )
            // InternalSML.g:1226:2: iv_ruleRole= ruleRole EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRoleRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRole=ruleRole();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRole; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRole"


    // $ANTLR start "ruleRole"
    // InternalSML.g:1233:1: ruleRole returns [EObject current=null] : ( ( ( (lv_static_0_0= 'static' ) ) | (otherlv_1= 'dynamic' ( (lv_multiRole_2_0= 'multi' ) )? ) ) otherlv_3= 'role' ( (otherlv_4= RULE_ID ) ) ( (lv_name_5_0= RULE_ID ) ) ) ;
    public final EObject ruleRole() throws RecognitionException {
        EObject current = null;

        Token lv_static_0_0=null;
        Token otherlv_1=null;
        Token lv_multiRole_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_name_5_0=null;

         enterRule(); 
            
        try {
            // InternalSML.g:1236:28: ( ( ( ( (lv_static_0_0= 'static' ) ) | (otherlv_1= 'dynamic' ( (lv_multiRole_2_0= 'multi' ) )? ) ) otherlv_3= 'role' ( (otherlv_4= RULE_ID ) ) ( (lv_name_5_0= RULE_ID ) ) ) )
            // InternalSML.g:1237:1: ( ( ( (lv_static_0_0= 'static' ) ) | (otherlv_1= 'dynamic' ( (lv_multiRole_2_0= 'multi' ) )? ) ) otherlv_3= 'role' ( (otherlv_4= RULE_ID ) ) ( (lv_name_5_0= RULE_ID ) ) )
            {
            // InternalSML.g:1237:1: ( ( ( (lv_static_0_0= 'static' ) ) | (otherlv_1= 'dynamic' ( (lv_multiRole_2_0= 'multi' ) )? ) ) otherlv_3= 'role' ( (otherlv_4= RULE_ID ) ) ( (lv_name_5_0= RULE_ID ) ) )
            // InternalSML.g:1237:2: ( ( (lv_static_0_0= 'static' ) ) | (otherlv_1= 'dynamic' ( (lv_multiRole_2_0= 'multi' ) )? ) ) otherlv_3= 'role' ( (otherlv_4= RULE_ID ) ) ( (lv_name_5_0= RULE_ID ) )
            {
            // InternalSML.g:1237:2: ( ( (lv_static_0_0= 'static' ) ) | (otherlv_1= 'dynamic' ( (lv_multiRole_2_0= 'multi' ) )? ) )
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==36) ) {
                alt35=1;
            }
            else if ( (LA35_0==37) ) {
                alt35=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 35, 0, input);

                throw nvae;
            }
            switch (alt35) {
                case 1 :
                    // InternalSML.g:1237:3: ( (lv_static_0_0= 'static' ) )
                    {
                    // InternalSML.g:1237:3: ( (lv_static_0_0= 'static' ) )
                    // InternalSML.g:1238:1: (lv_static_0_0= 'static' )
                    {
                    // InternalSML.g:1238:1: (lv_static_0_0= 'static' )
                    // InternalSML.g:1239:3: lv_static_0_0= 'static'
                    {
                    lv_static_0_0=(Token)match(input,36,FollowSets000.FOLLOW_30); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_static_0_0, grammarAccess.getRoleAccess().getStaticStaticKeyword_0_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getRoleRule());
                      	        }
                             		setWithLastConsumed(current, "static", true, "static");
                      	    
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSML.g:1253:6: (otherlv_1= 'dynamic' ( (lv_multiRole_2_0= 'multi' ) )? )
                    {
                    // InternalSML.g:1253:6: (otherlv_1= 'dynamic' ( (lv_multiRole_2_0= 'multi' ) )? )
                    // InternalSML.g:1253:8: otherlv_1= 'dynamic' ( (lv_multiRole_2_0= 'multi' ) )?
                    {
                    otherlv_1=(Token)match(input,37,FollowSets000.FOLLOW_31); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getRoleAccess().getDynamicKeyword_0_1_0());
                          
                    }
                    // InternalSML.g:1257:1: ( (lv_multiRole_2_0= 'multi' ) )?
                    int alt34=2;
                    int LA34_0 = input.LA(1);

                    if ( (LA34_0==38) ) {
                        alt34=1;
                    }
                    switch (alt34) {
                        case 1 :
                            // InternalSML.g:1258:1: (lv_multiRole_2_0= 'multi' )
                            {
                            // InternalSML.g:1258:1: (lv_multiRole_2_0= 'multi' )
                            // InternalSML.g:1259:3: lv_multiRole_2_0= 'multi'
                            {
                            lv_multiRole_2_0=(Token)match(input,38,FollowSets000.FOLLOW_30); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_multiRole_2_0, grammarAccess.getRoleAccess().getMultiRoleMultiKeyword_0_1_1_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRoleRule());
                              	        }
                                     		setWithLastConsumed(current, "multiRole", true, "multi");
                              	    
                            }

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,39,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getRoleAccess().getRoleKeyword_1());
                  
            }
            // InternalSML.g:1276:1: ( (otherlv_4= RULE_ID ) )
            // InternalSML.g:1277:1: (otherlv_4= RULE_ID )
            {
            // InternalSML.g:1277:1: (otherlv_4= RULE_ID )
            // InternalSML.g:1278:3: otherlv_4= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getRoleRule());
              	        }
                      
            }
            otherlv_4=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_4, grammarAccess.getRoleAccess().getTypeEClassCrossReference_2_0()); 
              	
            }

            }


            }

            // InternalSML.g:1289:2: ( (lv_name_5_0= RULE_ID ) )
            // InternalSML.g:1290:1: (lv_name_5_0= RULE_ID )
            {
            // InternalSML.g:1290:1: (lv_name_5_0= RULE_ID )
            // InternalSML.g:1291:3: lv_name_5_0= RULE_ID
            {
            lv_name_5_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_5_0, grammarAccess.getRoleAccess().getNameIDTerminalRuleCall_3_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getRoleRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_5_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRole"


    // $ANTLR start "entryRuleScenario"
    // InternalSML.g:1315:1: entryRuleScenario returns [EObject current=null] : iv_ruleScenario= ruleScenario EOF ;
    public final EObject entryRuleScenario() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleScenario = null;


        try {
            // InternalSML.g:1316:2: (iv_ruleScenario= ruleScenario EOF )
            // InternalSML.g:1317:2: iv_ruleScenario= ruleScenario EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getScenarioRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleScenario=ruleScenario();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleScenario; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleScenario"


    // $ANTLR start "ruleScenario"
    // InternalSML.g:1324:1: ruleScenario returns [EObject current=null] : ( ( (lv_singular_0_0= 'singular' ) )? ( (lv_kind_1_0= ruleScenarioKind ) ) otherlv_2= 'scenario' ( (lv_name_3_0= RULE_ID ) ) ( ( ( (lv_optimizeCost_4_0= 'optimize' ) ) otherlv_5= 'cost' ) | (otherlv_6= 'cost' otherlv_7= '[' ( (lv_cost_8_0= RULE_DOUBLE ) ) otherlv_9= ']' ) )? (otherlv_10= 'context' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* )? (otherlv_14= 'bindings' otherlv_15= '[' ( (lv_roleBindings_16_0= ruleRoleBindingConstraint ) )* otherlv_17= ']' )? ( (lv_ownedInteraction_18_0= ruleInteraction ) ) ) ;
    public final EObject ruleScenario() throws RecognitionException {
        EObject current = null;

        Token lv_singular_0_0=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token lv_optimizeCost_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token lv_cost_8_0=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Enumerator lv_kind_1_0 = null;

        EObject lv_roleBindings_16_0 = null;

        EObject lv_ownedInteraction_18_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:1327:28: ( ( ( (lv_singular_0_0= 'singular' ) )? ( (lv_kind_1_0= ruleScenarioKind ) ) otherlv_2= 'scenario' ( (lv_name_3_0= RULE_ID ) ) ( ( ( (lv_optimizeCost_4_0= 'optimize' ) ) otherlv_5= 'cost' ) | (otherlv_6= 'cost' otherlv_7= '[' ( (lv_cost_8_0= RULE_DOUBLE ) ) otherlv_9= ']' ) )? (otherlv_10= 'context' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* )? (otherlv_14= 'bindings' otherlv_15= '[' ( (lv_roleBindings_16_0= ruleRoleBindingConstraint ) )* otherlv_17= ']' )? ( (lv_ownedInteraction_18_0= ruleInteraction ) ) ) )
            // InternalSML.g:1328:1: ( ( (lv_singular_0_0= 'singular' ) )? ( (lv_kind_1_0= ruleScenarioKind ) ) otherlv_2= 'scenario' ( (lv_name_3_0= RULE_ID ) ) ( ( ( (lv_optimizeCost_4_0= 'optimize' ) ) otherlv_5= 'cost' ) | (otherlv_6= 'cost' otherlv_7= '[' ( (lv_cost_8_0= RULE_DOUBLE ) ) otherlv_9= ']' ) )? (otherlv_10= 'context' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* )? (otherlv_14= 'bindings' otherlv_15= '[' ( (lv_roleBindings_16_0= ruleRoleBindingConstraint ) )* otherlv_17= ']' )? ( (lv_ownedInteraction_18_0= ruleInteraction ) ) )
            {
            // InternalSML.g:1328:1: ( ( (lv_singular_0_0= 'singular' ) )? ( (lv_kind_1_0= ruleScenarioKind ) ) otherlv_2= 'scenario' ( (lv_name_3_0= RULE_ID ) ) ( ( ( (lv_optimizeCost_4_0= 'optimize' ) ) otherlv_5= 'cost' ) | (otherlv_6= 'cost' otherlv_7= '[' ( (lv_cost_8_0= RULE_DOUBLE ) ) otherlv_9= ']' ) )? (otherlv_10= 'context' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* )? (otherlv_14= 'bindings' otherlv_15= '[' ( (lv_roleBindings_16_0= ruleRoleBindingConstraint ) )* otherlv_17= ']' )? ( (lv_ownedInteraction_18_0= ruleInteraction ) ) )
            // InternalSML.g:1328:2: ( (lv_singular_0_0= 'singular' ) )? ( (lv_kind_1_0= ruleScenarioKind ) ) otherlv_2= 'scenario' ( (lv_name_3_0= RULE_ID ) ) ( ( ( (lv_optimizeCost_4_0= 'optimize' ) ) otherlv_5= 'cost' ) | (otherlv_6= 'cost' otherlv_7= '[' ( (lv_cost_8_0= RULE_DOUBLE ) ) otherlv_9= ']' ) )? (otherlv_10= 'context' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* )? (otherlv_14= 'bindings' otherlv_15= '[' ( (lv_roleBindings_16_0= ruleRoleBindingConstraint ) )* otherlv_17= ']' )? ( (lv_ownedInteraction_18_0= ruleInteraction ) )
            {
            // InternalSML.g:1328:2: ( (lv_singular_0_0= 'singular' ) )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==40) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalSML.g:1329:1: (lv_singular_0_0= 'singular' )
                    {
                    // InternalSML.g:1329:1: (lv_singular_0_0= 'singular' )
                    // InternalSML.g:1330:3: lv_singular_0_0= 'singular'
                    {
                    lv_singular_0_0=(Token)match(input,40,FollowSets000.FOLLOW_32); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_singular_0_0, grammarAccess.getScenarioAccess().getSingularSingularKeyword_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getScenarioRule());
                      	        }
                             		setWithLastConsumed(current, "singular", true, "singular");
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalSML.g:1343:3: ( (lv_kind_1_0= ruleScenarioKind ) )
            // InternalSML.g:1344:1: (lv_kind_1_0= ruleScenarioKind )
            {
            // InternalSML.g:1344:1: (lv_kind_1_0= ruleScenarioKind )
            // InternalSML.g:1345:3: lv_kind_1_0= ruleScenarioKind
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getScenarioAccess().getKindScenarioKindEnumRuleCall_1_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_33);
            lv_kind_1_0=ruleScenarioKind();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getScenarioRule());
              	        }
                     		set(
                     			current, 
                     			"kind",
                      		lv_kind_1_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ScenarioKind");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,41,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getScenarioAccess().getScenarioKeyword_2());
                  
            }
            // InternalSML.g:1365:1: ( (lv_name_3_0= RULE_ID ) )
            // InternalSML.g:1366:1: (lv_name_3_0= RULE_ID )
            {
            // InternalSML.g:1366:1: (lv_name_3_0= RULE_ID )
            // InternalSML.g:1367:3: lv_name_3_0= RULE_ID
            {
            lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_34); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_3_0, grammarAccess.getScenarioAccess().getNameIDTerminalRuleCall_3_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getScenarioRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_3_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalSML.g:1383:2: ( ( ( (lv_optimizeCost_4_0= 'optimize' ) ) otherlv_5= 'cost' ) | (otherlv_6= 'cost' otherlv_7= '[' ( (lv_cost_8_0= RULE_DOUBLE ) ) otherlv_9= ']' ) )?
            int alt37=3;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==42) ) {
                alt37=1;
            }
            else if ( (LA37_0==43) ) {
                alt37=2;
            }
            switch (alt37) {
                case 1 :
                    // InternalSML.g:1383:3: ( ( (lv_optimizeCost_4_0= 'optimize' ) ) otherlv_5= 'cost' )
                    {
                    // InternalSML.g:1383:3: ( ( (lv_optimizeCost_4_0= 'optimize' ) ) otherlv_5= 'cost' )
                    // InternalSML.g:1383:4: ( (lv_optimizeCost_4_0= 'optimize' ) ) otherlv_5= 'cost'
                    {
                    // InternalSML.g:1383:4: ( (lv_optimizeCost_4_0= 'optimize' ) )
                    // InternalSML.g:1384:1: (lv_optimizeCost_4_0= 'optimize' )
                    {
                    // InternalSML.g:1384:1: (lv_optimizeCost_4_0= 'optimize' )
                    // InternalSML.g:1385:3: lv_optimizeCost_4_0= 'optimize'
                    {
                    lv_optimizeCost_4_0=(Token)match(input,42,FollowSets000.FOLLOW_35); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_optimizeCost_4_0, grammarAccess.getScenarioAccess().getOptimizeCostOptimizeKeyword_4_0_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getScenarioRule());
                      	        }
                             		setWithLastConsumed(current, "optimizeCost", true, "optimize");
                      	    
                    }

                    }


                    }

                    otherlv_5=(Token)match(input,43,FollowSets000.FOLLOW_34); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_5, grammarAccess.getScenarioAccess().getCostKeyword_4_0_1());
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalSML.g:1403:6: (otherlv_6= 'cost' otherlv_7= '[' ( (lv_cost_8_0= RULE_DOUBLE ) ) otherlv_9= ']' )
                    {
                    // InternalSML.g:1403:6: (otherlv_6= 'cost' otherlv_7= '[' ( (lv_cost_8_0= RULE_DOUBLE ) ) otherlv_9= ']' )
                    // InternalSML.g:1403:8: otherlv_6= 'cost' otherlv_7= '[' ( (lv_cost_8_0= RULE_DOUBLE ) ) otherlv_9= ']'
                    {
                    otherlv_6=(Token)match(input,43,FollowSets000.FOLLOW_22); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_6, grammarAccess.getScenarioAccess().getCostKeyword_4_1_0());
                          
                    }
                    otherlv_7=(Token)match(input,32,FollowSets000.FOLLOW_36); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_7, grammarAccess.getScenarioAccess().getLeftSquareBracketKeyword_4_1_1());
                          
                    }
                    // InternalSML.g:1411:1: ( (lv_cost_8_0= RULE_DOUBLE ) )
                    // InternalSML.g:1412:1: (lv_cost_8_0= RULE_DOUBLE )
                    {
                    // InternalSML.g:1412:1: (lv_cost_8_0= RULE_DOUBLE )
                    // InternalSML.g:1413:3: lv_cost_8_0= RULE_DOUBLE
                    {
                    lv_cost_8_0=(Token)match(input,RULE_DOUBLE,FollowSets000.FOLLOW_24); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_cost_8_0, grammarAccess.getScenarioAccess().getCostDOUBLETerminalRuleCall_4_1_2_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getScenarioRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"cost",
                              		lv_cost_8_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.DOUBLE");
                      	    
                    }

                    }


                    }

                    otherlv_9=(Token)match(input,33,FollowSets000.FOLLOW_34); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_9, grammarAccess.getScenarioAccess().getRightSquareBracketKeyword_4_1_3());
                          
                    }

                    }


                    }
                    break;

            }

            // InternalSML.g:1433:4: (otherlv_10= 'context' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )* )?
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==44) ) {
                alt39=1;
            }
            switch (alt39) {
                case 1 :
                    // InternalSML.g:1433:6: otherlv_10= 'context' ( (otherlv_11= RULE_ID ) ) (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )*
                    {
                    otherlv_10=(Token)match(input,44,FollowSets000.FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_10, grammarAccess.getScenarioAccess().getContextKeyword_5_0());
                          
                    }
                    // InternalSML.g:1437:1: ( (otherlv_11= RULE_ID ) )
                    // InternalSML.g:1438:1: (otherlv_11= RULE_ID )
                    {
                    // InternalSML.g:1438:1: (otherlv_11= RULE_ID )
                    // InternalSML.g:1439:3: otherlv_11= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      			if (current==null) {
                      	            current = createModelElement(grammarAccess.getScenarioRule());
                      	        }
                              
                    }
                    otherlv_11=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_37); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		newLeafNode(otherlv_11, grammarAccess.getScenarioAccess().getContextsEClassCrossReference_5_1_0()); 
                      	
                    }

                    }


                    }

                    // InternalSML.g:1450:2: (otherlv_12= ',' ( (otherlv_13= RULE_ID ) ) )*
                    loop38:
                    do {
                        int alt38=2;
                        int LA38_0 = input.LA(1);

                        if ( (LA38_0==22) ) {
                            alt38=1;
                        }


                        switch (alt38) {
                    	case 1 :
                    	    // InternalSML.g:1450:4: otherlv_12= ',' ( (otherlv_13= RULE_ID ) )
                    	    {
                    	    otherlv_12=(Token)match(input,22,FollowSets000.FOLLOW_4); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_12, grammarAccess.getScenarioAccess().getCommaKeyword_5_2_0());
                    	          
                    	    }
                    	    // InternalSML.g:1454:1: ( (otherlv_13= RULE_ID ) )
                    	    // InternalSML.g:1455:1: (otherlv_13= RULE_ID )
                    	    {
                    	    // InternalSML.g:1455:1: (otherlv_13= RULE_ID )
                    	    // InternalSML.g:1456:3: otherlv_13= RULE_ID
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      			if (current==null) {
                    	      	            current = createModelElement(grammarAccess.getScenarioRule());
                    	      	        }
                    	              
                    	    }
                    	    otherlv_13=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_37); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      		newLeafNode(otherlv_13, grammarAccess.getScenarioAccess().getContextsEClassCrossReference_5_2_1_0()); 
                    	      	
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop38;
                        }
                    } while (true);


                    }
                    break;

            }

            // InternalSML.g:1467:6: (otherlv_14= 'bindings' otherlv_15= '[' ( (lv_roleBindings_16_0= ruleRoleBindingConstraint ) )* otherlv_17= ']' )?
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( (LA41_0==45) ) {
                alt41=1;
            }
            switch (alt41) {
                case 1 :
                    // InternalSML.g:1467:8: otherlv_14= 'bindings' otherlv_15= '[' ( (lv_roleBindings_16_0= ruleRoleBindingConstraint ) )* otherlv_17= ']'
                    {
                    otherlv_14=(Token)match(input,45,FollowSets000.FOLLOW_22); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_14, grammarAccess.getScenarioAccess().getBindingsKeyword_6_0());
                          
                    }
                    otherlv_15=(Token)match(input,32,FollowSets000.FOLLOW_38); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_15, grammarAccess.getScenarioAccess().getLeftSquareBracketKeyword_6_1());
                          
                    }
                    // InternalSML.g:1475:1: ( (lv_roleBindings_16_0= ruleRoleBindingConstraint ) )*
                    loop40:
                    do {
                        int alt40=2;
                        int LA40_0 = input.LA(1);

                        if ( (LA40_0==RULE_ID) ) {
                            alt40=1;
                        }


                        switch (alt40) {
                    	case 1 :
                    	    // InternalSML.g:1476:1: (lv_roleBindings_16_0= ruleRoleBindingConstraint )
                    	    {
                    	    // InternalSML.g:1476:1: (lv_roleBindings_16_0= ruleRoleBindingConstraint )
                    	    // InternalSML.g:1477:3: lv_roleBindings_16_0= ruleRoleBindingConstraint
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getScenarioAccess().getRoleBindingsRoleBindingConstraintParserRuleCall_6_2_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_38);
                    	    lv_roleBindings_16_0=ruleRoleBindingConstraint();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getScenarioRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"roleBindings",
                    	              		lv_roleBindings_16_0, 
                    	              		"org.scenariotools.sml.collaboration.Collaboration.RoleBindingConstraint");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop40;
                        }
                    } while (true);

                    otherlv_17=(Token)match(input,33,FollowSets000.FOLLOW_34); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_17, grammarAccess.getScenarioAccess().getRightSquareBracketKeyword_6_3());
                          
                    }

                    }
                    break;

            }

            // InternalSML.g:1497:3: ( (lv_ownedInteraction_18_0= ruleInteraction ) )
            // InternalSML.g:1498:1: (lv_ownedInteraction_18_0= ruleInteraction )
            {
            // InternalSML.g:1498:1: (lv_ownedInteraction_18_0= ruleInteraction )
            // InternalSML.g:1499:3: lv_ownedInteraction_18_0= ruleInteraction
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getScenarioAccess().getOwnedInteractionInteractionParserRuleCall_7_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_ownedInteraction_18_0=ruleInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getScenarioRule());
              	        }
                     		set(
                     			current, 
                     			"ownedInteraction",
                      		lv_ownedInteraction_18_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleScenario"


    // $ANTLR start "entryRuleRoleBindingConstraint"
    // InternalSML.g:1523:1: entryRuleRoleBindingConstraint returns [EObject current=null] : iv_ruleRoleBindingConstraint= ruleRoleBindingConstraint EOF ;
    public final EObject entryRuleRoleBindingConstraint() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRoleBindingConstraint = null;


        try {
            // InternalSML.g:1524:2: (iv_ruleRoleBindingConstraint= ruleRoleBindingConstraint EOF )
            // InternalSML.g:1525:2: iv_ruleRoleBindingConstraint= ruleRoleBindingConstraint EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRoleBindingConstraintRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRoleBindingConstraint=ruleRoleBindingConstraint();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRoleBindingConstraint; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoleBindingConstraint"


    // $ANTLR start "ruleRoleBindingConstraint"
    // InternalSML.g:1532:1: ruleRoleBindingConstraint returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_bindingExpression_2_0= ruleBindingExpression ) ) ) ;
    public final EObject ruleRoleBindingConstraint() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_bindingExpression_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:1535:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_bindingExpression_2_0= ruleBindingExpression ) ) ) )
            // InternalSML.g:1536:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_bindingExpression_2_0= ruleBindingExpression ) ) )
            {
            // InternalSML.g:1536:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_bindingExpression_2_0= ruleBindingExpression ) ) )
            // InternalSML.g:1536:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_bindingExpression_2_0= ruleBindingExpression ) )
            {
            // InternalSML.g:1536:2: ( (otherlv_0= RULE_ID ) )
            // InternalSML.g:1537:1: (otherlv_0= RULE_ID )
            {
            // InternalSML.g:1537:1: (otherlv_0= RULE_ID )
            // InternalSML.g:1538:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getRoleBindingConstraintRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_21); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getRoleBindingConstraintAccess().getRoleRoleCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,31,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getRoleBindingConstraintAccess().getEqualsSignKeyword_1());
                  
            }
            // InternalSML.g:1553:1: ( (lv_bindingExpression_2_0= ruleBindingExpression ) )
            // InternalSML.g:1554:1: (lv_bindingExpression_2_0= ruleBindingExpression )
            {
            // InternalSML.g:1554:1: (lv_bindingExpression_2_0= ruleBindingExpression )
            // InternalSML.g:1555:3: lv_bindingExpression_2_0= ruleBindingExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getRoleBindingConstraintAccess().getBindingExpressionBindingExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_bindingExpression_2_0=ruleBindingExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getRoleBindingConstraintRule());
              	        }
                     		set(
                     			current, 
                     			"bindingExpression",
                      		lv_bindingExpression_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.BindingExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoleBindingConstraint"


    // $ANTLR start "entryRuleBindingExpression"
    // InternalSML.g:1579:1: entryRuleBindingExpression returns [EObject current=null] : iv_ruleBindingExpression= ruleBindingExpression EOF ;
    public final EObject entryRuleBindingExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBindingExpression = null;


        try {
            // InternalSML.g:1580:2: (iv_ruleBindingExpression= ruleBindingExpression EOF )
            // InternalSML.g:1581:2: iv_ruleBindingExpression= ruleBindingExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBindingExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBindingExpression=ruleBindingExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBindingExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBindingExpression"


    // $ANTLR start "ruleBindingExpression"
    // InternalSML.g:1588:1: ruleBindingExpression returns [EObject current=null] : this_FeatureAccessBindingExpression_0= ruleFeatureAccessBindingExpression ;
    public final EObject ruleBindingExpression() throws RecognitionException {
        EObject current = null;

        EObject this_FeatureAccessBindingExpression_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:1591:28: (this_FeatureAccessBindingExpression_0= ruleFeatureAccessBindingExpression )
            // InternalSML.g:1593:5: this_FeatureAccessBindingExpression_0= ruleFeatureAccessBindingExpression
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getBindingExpressionAccess().getFeatureAccessBindingExpressionParserRuleCall()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_2);
            this_FeatureAccessBindingExpression_0=ruleFeatureAccessBindingExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_FeatureAccessBindingExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBindingExpression"


    // $ANTLR start "entryRuleFeatureAccessBindingExpression"
    // InternalSML.g:1609:1: entryRuleFeatureAccessBindingExpression returns [EObject current=null] : iv_ruleFeatureAccessBindingExpression= ruleFeatureAccessBindingExpression EOF ;
    public final EObject entryRuleFeatureAccessBindingExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureAccessBindingExpression = null;


        try {
            // InternalSML.g:1610:2: (iv_ruleFeatureAccessBindingExpression= ruleFeatureAccessBindingExpression EOF )
            // InternalSML.g:1611:2: iv_ruleFeatureAccessBindingExpression= ruleFeatureAccessBindingExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFeatureAccessBindingExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleFeatureAccessBindingExpression=ruleFeatureAccessBindingExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFeatureAccessBindingExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureAccessBindingExpression"


    // $ANTLR start "ruleFeatureAccessBindingExpression"
    // InternalSML.g:1618:1: ruleFeatureAccessBindingExpression returns [EObject current=null] : ( (lv_featureaccess_0_0= ruleFeatureAccess ) ) ;
    public final EObject ruleFeatureAccessBindingExpression() throws RecognitionException {
        EObject current = null;

        EObject lv_featureaccess_0_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:1621:28: ( ( (lv_featureaccess_0_0= ruleFeatureAccess ) ) )
            // InternalSML.g:1622:1: ( (lv_featureaccess_0_0= ruleFeatureAccess ) )
            {
            // InternalSML.g:1622:1: ( (lv_featureaccess_0_0= ruleFeatureAccess ) )
            // InternalSML.g:1623:1: (lv_featureaccess_0_0= ruleFeatureAccess )
            {
            // InternalSML.g:1623:1: (lv_featureaccess_0_0= ruleFeatureAccess )
            // InternalSML.g:1624:3: lv_featureaccess_0_0= ruleFeatureAccess
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getFeatureAccessBindingExpressionAccess().getFeatureaccessFeatureAccessParserRuleCall_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_featureaccess_0_0=ruleFeatureAccess();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getFeatureAccessBindingExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"featureaccess",
                      		lv_featureaccess_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.FeatureAccess");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureAccessBindingExpression"


    // $ANTLR start "entryRuleInteractionFragment"
    // InternalSML.g:1648:1: entryRuleInteractionFragment returns [EObject current=null] : iv_ruleInteractionFragment= ruleInteractionFragment EOF ;
    public final EObject entryRuleInteractionFragment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInteractionFragment = null;


        try {
            // InternalSML.g:1649:2: (iv_ruleInteractionFragment= ruleInteractionFragment EOF )
            // InternalSML.g:1650:2: iv_ruleInteractionFragment= ruleInteractionFragment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInteractionFragmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleInteractionFragment=ruleInteractionFragment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInteractionFragment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInteractionFragment"


    // $ANTLR start "ruleInteractionFragment"
    // InternalSML.g:1657:1: ruleInteractionFragment returns [EObject current=null] : (this_Interaction_0= ruleInteraction | this_ModalMessage_1= ruleModalMessage | this_Alternative_2= ruleAlternative | this_Loop_3= ruleLoop | this_Parallel_4= ruleParallel | this_ConditionFragment_5= ruleConditionFragment | this_TimedConditionFragment_6= ruleTimedConditionFragment | this_VariableFragment_7= ruleVariableFragment ) ;
    public final EObject ruleInteractionFragment() throws RecognitionException {
        EObject current = null;

        EObject this_Interaction_0 = null;

        EObject this_ModalMessage_1 = null;

        EObject this_Alternative_2 = null;

        EObject this_Loop_3 = null;

        EObject this_Parallel_4 = null;

        EObject this_ConditionFragment_5 = null;

        EObject this_TimedConditionFragment_6 = null;

        EObject this_VariableFragment_7 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:1660:28: ( (this_Interaction_0= ruleInteraction | this_ModalMessage_1= ruleModalMessage | this_Alternative_2= ruleAlternative | this_Loop_3= ruleLoop | this_Parallel_4= ruleParallel | this_ConditionFragment_5= ruleConditionFragment | this_TimedConditionFragment_6= ruleTimedConditionFragment | this_VariableFragment_7= ruleVariableFragment ) )
            // InternalSML.g:1661:1: (this_Interaction_0= ruleInteraction | this_ModalMessage_1= ruleModalMessage | this_Alternative_2= ruleAlternative | this_Loop_3= ruleLoop | this_Parallel_4= ruleParallel | this_ConditionFragment_5= ruleConditionFragment | this_TimedConditionFragment_6= ruleTimedConditionFragment | this_VariableFragment_7= ruleVariableFragment )
            {
            // InternalSML.g:1661:1: (this_Interaction_0= ruleInteraction | this_ModalMessage_1= ruleModalMessage | this_Alternative_2= ruleAlternative | this_Loop_3= ruleLoop | this_Parallel_4= ruleParallel | this_ConditionFragment_5= ruleConditionFragment | this_TimedConditionFragment_6= ruleTimedConditionFragment | this_VariableFragment_7= ruleVariableFragment )
            int alt42=8;
            alt42 = dfa42.predict(input);
            switch (alt42) {
                case 1 :
                    // InternalSML.g:1662:5: this_Interaction_0= ruleInteraction
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getInteractionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Interaction_0=ruleInteraction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Interaction_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:1672:5: this_ModalMessage_1= ruleModalMessage
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getModalMessageParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ModalMessage_1=ruleModalMessage();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ModalMessage_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalSML.g:1682:5: this_Alternative_2= ruleAlternative
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getAlternativeParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Alternative_2=ruleAlternative();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Alternative_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // InternalSML.g:1692:5: this_Loop_3= ruleLoop
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getLoopParserRuleCall_3()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Loop_3=ruleLoop();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Loop_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // InternalSML.g:1702:5: this_Parallel_4= ruleParallel
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getParallelParserRuleCall_4()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Parallel_4=ruleParallel();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Parallel_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 6 :
                    // InternalSML.g:1712:5: this_ConditionFragment_5= ruleConditionFragment
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getConditionFragmentParserRuleCall_5()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ConditionFragment_5=ruleConditionFragment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ConditionFragment_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 7 :
                    // InternalSML.g:1722:5: this_TimedConditionFragment_6= ruleTimedConditionFragment
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getTimedConditionFragmentParserRuleCall_6()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_TimedConditionFragment_6=ruleTimedConditionFragment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TimedConditionFragment_6; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 8 :
                    // InternalSML.g:1732:5: this_VariableFragment_7= ruleVariableFragment
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getVariableFragmentParserRuleCall_7()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableFragment_7=ruleVariableFragment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableFragment_7; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInteractionFragment"


    // $ANTLR start "entryRuleVariableFragment"
    // InternalSML.g:1748:1: entryRuleVariableFragment returns [EObject current=null] : iv_ruleVariableFragment= ruleVariableFragment EOF ;
    public final EObject entryRuleVariableFragment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableFragment = null;


        try {
            // InternalSML.g:1749:2: (iv_ruleVariableFragment= ruleVariableFragment EOF )
            // InternalSML.g:1750:2: iv_ruleVariableFragment= ruleVariableFragment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableFragmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableFragment=ruleVariableFragment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableFragment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableFragment"


    // $ANTLR start "ruleVariableFragment"
    // InternalSML.g:1757:1: ruleVariableFragment returns [EObject current=null] : ( ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment | lv_expression_0_3= ruleClockDeclaration | lv_expression_0_4= ruleClockAssignment ) ) ) ;
    public final EObject ruleVariableFragment() throws RecognitionException {
        EObject current = null;

        EObject lv_expression_0_1 = null;

        EObject lv_expression_0_2 = null;

        EObject lv_expression_0_3 = null;

        EObject lv_expression_0_4 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:1760:28: ( ( ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment | lv_expression_0_3= ruleClockDeclaration | lv_expression_0_4= ruleClockAssignment ) ) ) )
            // InternalSML.g:1761:1: ( ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment | lv_expression_0_3= ruleClockDeclaration | lv_expression_0_4= ruleClockAssignment ) ) )
            {
            // InternalSML.g:1761:1: ( ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment | lv_expression_0_3= ruleClockDeclaration | lv_expression_0_4= ruleClockAssignment ) ) )
            // InternalSML.g:1762:1: ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment | lv_expression_0_3= ruleClockDeclaration | lv_expression_0_4= ruleClockAssignment ) )
            {
            // InternalSML.g:1762:1: ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment | lv_expression_0_3= ruleClockDeclaration | lv_expression_0_4= ruleClockAssignment ) )
            // InternalSML.g:1763:1: (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment | lv_expression_0_3= ruleClockDeclaration | lv_expression_0_4= ruleClockAssignment )
            {
            // InternalSML.g:1763:1: (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment | lv_expression_0_3= ruleClockDeclaration | lv_expression_0_4= ruleClockAssignment )
            int alt43=4;
            switch ( input.LA(1) ) {
            case 67:
                {
                alt43=1;
                }
                break;
            case RULE_ID:
                {
                alt43=2;
                }
                break;
            case 68:
                {
                alt43=3;
                }
                break;
            case 69:
                {
                alt43=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 43, 0, input);

                throw nvae;
            }

            switch (alt43) {
                case 1 :
                    // InternalSML.g:1764:3: lv_expression_0_1= ruleTypedVariableDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getVariableFragmentAccess().getExpressionTypedVariableDeclarationParserRuleCall_0_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_0_1=ruleTypedVariableDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getVariableFragmentRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_0_1, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.TypedVariableDeclaration");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:1779:8: lv_expression_0_2= ruleVariableAssignment
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getVariableFragmentAccess().getExpressionVariableAssignmentParserRuleCall_0_1()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_0_2=ruleVariableAssignment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getVariableFragmentRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_0_2, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.VariableAssignment");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }
                    break;
                case 3 :
                    // InternalSML.g:1794:8: lv_expression_0_3= ruleClockDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getVariableFragmentAccess().getExpressionClockDeclarationParserRuleCall_0_2()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_0_3=ruleClockDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getVariableFragmentRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_0_3, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.ClockDeclaration");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }
                    break;
                case 4 :
                    // InternalSML.g:1809:8: lv_expression_0_4= ruleClockAssignment
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getVariableFragmentAccess().getExpressionClockAssignmentParserRuleCall_0_3()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_0_4=ruleClockAssignment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getVariableFragmentRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_0_4, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.ClockAssignment");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableFragment"


    // $ANTLR start "entryRuleInteraction"
    // InternalSML.g:1835:1: entryRuleInteraction returns [EObject current=null] : iv_ruleInteraction= ruleInteraction EOF ;
    public final EObject entryRuleInteraction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInteraction = null;


        try {
            // InternalSML.g:1836:2: (iv_ruleInteraction= ruleInteraction EOF )
            // InternalSML.g:1837:2: iv_ruleInteraction= ruleInteraction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInteractionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleInteraction=ruleInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInteraction; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInteraction"


    // $ANTLR start "ruleInteraction"
    // InternalSML.g:1844:1: ruleInteraction returns [EObject current=null] : ( () otherlv_1= '{' ( (lv_fragments_2_0= ruleInteractionFragment ) )* otherlv_3= '}' ( (lv_constraints_4_0= ruleConstraintBlock ) )? ) ;
    public final EObject ruleInteraction() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_fragments_2_0 = null;

        EObject lv_constraints_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:1847:28: ( ( () otherlv_1= '{' ( (lv_fragments_2_0= ruleInteractionFragment ) )* otherlv_3= '}' ( (lv_constraints_4_0= ruleConstraintBlock ) )? ) )
            // InternalSML.g:1848:1: ( () otherlv_1= '{' ( (lv_fragments_2_0= ruleInteractionFragment ) )* otherlv_3= '}' ( (lv_constraints_4_0= ruleConstraintBlock ) )? )
            {
            // InternalSML.g:1848:1: ( () otherlv_1= '{' ( (lv_fragments_2_0= ruleInteractionFragment ) )* otherlv_3= '}' ( (lv_constraints_4_0= ruleConstraintBlock ) )? )
            // InternalSML.g:1848:2: () otherlv_1= '{' ( (lv_fragments_2_0= ruleInteractionFragment ) )* otherlv_3= '}' ( (lv_constraints_4_0= ruleConstraintBlock ) )?
            {
            // InternalSML.g:1848:2: ()
            // InternalSML.g:1849:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getInteractionAccess().getInteractionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,15,FollowSets000.FOLLOW_39); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getInteractionAccess().getLeftCurlyBracketKeyword_1());
                  
            }
            // InternalSML.g:1858:1: ( (lv_fragments_2_0= ruleInteractionFragment ) )*
            loop44:
            do {
                int alt44=2;
                int LA44_0 = input.LA(1);

                if ( (LA44_0==RULE_ID||LA44_0==15||(LA44_0>=46 && LA44_0<=47)||LA44_0==51||(LA44_0>=53 && LA44_0<=54)||(LA44_0>=56 && LA44_0<=60)||(LA44_0>=67 && LA44_0<=69)||(LA44_0>=87 && LA44_0<=89)) ) {
                    alt44=1;
                }


                switch (alt44) {
            	case 1 :
            	    // InternalSML.g:1859:1: (lv_fragments_2_0= ruleInteractionFragment )
            	    {
            	    // InternalSML.g:1859:1: (lv_fragments_2_0= ruleInteractionFragment )
            	    // InternalSML.g:1860:3: lv_fragments_2_0= ruleInteractionFragment
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getInteractionAccess().getFragmentsInteractionFragmentParserRuleCall_2_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_39);
            	    lv_fragments_2_0=ruleInteractionFragment();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getInteractionRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"fragments",
            	              		lv_fragments_2_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.InteractionFragment");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop44;
                }
            } while (true);

            otherlv_3=(Token)match(input,19,FollowSets000.FOLLOW_40); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getInteractionAccess().getRightCurlyBracketKeyword_3());
                  
            }
            // InternalSML.g:1880:1: ( (lv_constraints_4_0= ruleConstraintBlock ) )?
            int alt45=2;
            int LA45_0 = input.LA(1);

            if ( (LA45_0==61) ) {
                alt45=1;
            }
            switch (alt45) {
                case 1 :
                    // InternalSML.g:1881:1: (lv_constraints_4_0= ruleConstraintBlock )
                    {
                    // InternalSML.g:1881:1: (lv_constraints_4_0= ruleConstraintBlock )
                    // InternalSML.g:1882:3: lv_constraints_4_0= ruleConstraintBlock
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getInteractionAccess().getConstraintsConstraintBlockParserRuleCall_4_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_constraints_4_0=ruleConstraintBlock();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getInteractionRule());
                      	        }
                             		set(
                             			current, 
                             			"constraints",
                              		lv_constraints_4_0, 
                              		"org.scenariotools.sml.collaboration.Collaboration.ConstraintBlock");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInteraction"


    // $ANTLR start "entryRuleModalMessage"
    // InternalSML.g:1906:1: entryRuleModalMessage returns [EObject current=null] : iv_ruleModalMessage= ruleModalMessage EOF ;
    public final EObject entryRuleModalMessage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModalMessage = null;


        try {
            // InternalSML.g:1907:2: (iv_ruleModalMessage= ruleModalMessage EOF )
            // InternalSML.g:1908:2: iv_ruleModalMessage= ruleModalMessage EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getModalMessageRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleModalMessage=ruleModalMessage();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleModalMessage; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModalMessage"


    // $ANTLR start "ruleModalMessage"
    // InternalSML.g:1915:1: ruleModalMessage returns [EObject current=null] : ( ( (lv_strict_0_0= 'strict' ) )? ( ( (lv_monitored_1_0= 'monitored' ) )? ( (lv_expectationKind_2_0= ruleExpectationKind ) ) )? ( (otherlv_3= RULE_ID ) ) otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) otherlv_6= '.' ( (otherlv_7= RULE_ID ) ) (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )? (otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')' )? ) ;
    public final EObject ruleModalMessage() throws RecognitionException {
        EObject current = null;

        Token lv_strict_0_0=null;
        Token lv_monitored_1_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Enumerator lv_expectationKind_2_0 = null;

        Enumerator lv_collectionModification_9_0 = null;

        EObject lv_parameters_11_0 = null;

        EObject lv_parameters_13_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:1918:28: ( ( ( (lv_strict_0_0= 'strict' ) )? ( ( (lv_monitored_1_0= 'monitored' ) )? ( (lv_expectationKind_2_0= ruleExpectationKind ) ) )? ( (otherlv_3= RULE_ID ) ) otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) otherlv_6= '.' ( (otherlv_7= RULE_ID ) ) (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )? (otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')' )? ) )
            // InternalSML.g:1919:1: ( ( (lv_strict_0_0= 'strict' ) )? ( ( (lv_monitored_1_0= 'monitored' ) )? ( (lv_expectationKind_2_0= ruleExpectationKind ) ) )? ( (otherlv_3= RULE_ID ) ) otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) otherlv_6= '.' ( (otherlv_7= RULE_ID ) ) (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )? (otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')' )? )
            {
            // InternalSML.g:1919:1: ( ( (lv_strict_0_0= 'strict' ) )? ( ( (lv_monitored_1_0= 'monitored' ) )? ( (lv_expectationKind_2_0= ruleExpectationKind ) ) )? ( (otherlv_3= RULE_ID ) ) otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) otherlv_6= '.' ( (otherlv_7= RULE_ID ) ) (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )? (otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')' )? )
            // InternalSML.g:1919:2: ( (lv_strict_0_0= 'strict' ) )? ( ( (lv_monitored_1_0= 'monitored' ) )? ( (lv_expectationKind_2_0= ruleExpectationKind ) ) )? ( (otherlv_3= RULE_ID ) ) otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) otherlv_6= '.' ( (otherlv_7= RULE_ID ) ) (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )? (otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')' )?
            {
            // InternalSML.g:1919:2: ( (lv_strict_0_0= 'strict' ) )?
            int alt46=2;
            int LA46_0 = input.LA(1);

            if ( (LA46_0==46) ) {
                alt46=1;
            }
            switch (alt46) {
                case 1 :
                    // InternalSML.g:1920:1: (lv_strict_0_0= 'strict' )
                    {
                    // InternalSML.g:1920:1: (lv_strict_0_0= 'strict' )
                    // InternalSML.g:1921:3: lv_strict_0_0= 'strict'
                    {
                    lv_strict_0_0=(Token)match(input,46,FollowSets000.FOLLOW_41); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_strict_0_0, grammarAccess.getModalMessageAccess().getStrictStrictKeyword_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getModalMessageRule());
                      	        }
                             		setWithLastConsumed(current, "strict", true, "strict");
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalSML.g:1934:3: ( ( (lv_monitored_1_0= 'monitored' ) )? ( (lv_expectationKind_2_0= ruleExpectationKind ) ) )?
            int alt48=2;
            int LA48_0 = input.LA(1);

            if ( (LA48_0==47||LA48_0==57||(LA48_0>=87 && LA48_0<=89)) ) {
                alt48=1;
            }
            switch (alt48) {
                case 1 :
                    // InternalSML.g:1934:4: ( (lv_monitored_1_0= 'monitored' ) )? ( (lv_expectationKind_2_0= ruleExpectationKind ) )
                    {
                    // InternalSML.g:1934:4: ( (lv_monitored_1_0= 'monitored' ) )?
                    int alt47=2;
                    int LA47_0 = input.LA(1);

                    if ( (LA47_0==47) ) {
                        alt47=1;
                    }
                    switch (alt47) {
                        case 1 :
                            // InternalSML.g:1935:1: (lv_monitored_1_0= 'monitored' )
                            {
                            // InternalSML.g:1935:1: (lv_monitored_1_0= 'monitored' )
                            // InternalSML.g:1936:3: lv_monitored_1_0= 'monitored'
                            {
                            lv_monitored_1_0=(Token)match(input,47,FollowSets000.FOLLOW_42); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_monitored_1_0, grammarAccess.getModalMessageAccess().getMonitoredMonitoredKeyword_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getModalMessageRule());
                              	        }
                                     		setWithLastConsumed(current, "monitored", true, "monitored");
                              	    
                            }

                            }


                            }
                            break;

                    }

                    // InternalSML.g:1949:3: ( (lv_expectationKind_2_0= ruleExpectationKind ) )
                    // InternalSML.g:1950:1: (lv_expectationKind_2_0= ruleExpectationKind )
                    {
                    // InternalSML.g:1950:1: (lv_expectationKind_2_0= ruleExpectationKind )
                    // InternalSML.g:1951:3: lv_expectationKind_2_0= ruleExpectationKind
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getModalMessageAccess().getExpectationKindExpectationKindEnumRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_4);
                    lv_expectationKind_2_0=ruleExpectationKind();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getModalMessageRule());
                      	        }
                             		set(
                             			current, 
                             			"expectationKind",
                              		lv_expectationKind_2_0, 
                              		"org.scenariotools.sml.collaboration.Collaboration.ExpectationKind");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalSML.g:1967:4: ( (otherlv_3= RULE_ID ) )
            // InternalSML.g:1968:1: (otherlv_3= RULE_ID )
            {
            // InternalSML.g:1968:1: (otherlv_3= RULE_ID )
            // InternalSML.g:1969:3: otherlv_3= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getModalMessageRule());
              	        }
                      
            }
            otherlv_3=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_43); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_3, grammarAccess.getModalMessageAccess().getSenderRoleCrossReference_2_0()); 
              	
            }

            }


            }

            otherlv_4=(Token)match(input,48,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getModalMessageAccess().getHyphenMinusGreaterThanSignKeyword_3());
                  
            }
            // InternalSML.g:1984:1: ( (otherlv_5= RULE_ID ) )
            // InternalSML.g:1985:1: (otherlv_5= RULE_ID )
            {
            // InternalSML.g:1985:1: (otherlv_5= RULE_ID )
            // InternalSML.g:1986:3: otherlv_5= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getModalMessageRule());
              	        }
                      
            }
            otherlv_5=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_44); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_5, grammarAccess.getModalMessageAccess().getReceiverRoleCrossReference_4_0()); 
              	
            }

            }


            }

            otherlv_6=(Token)match(input,35,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getModalMessageAccess().getFullStopKeyword_5());
                  
            }
            // InternalSML.g:2001:1: ( (otherlv_7= RULE_ID ) )
            // InternalSML.g:2002:1: (otherlv_7= RULE_ID )
            {
            // InternalSML.g:2002:1: (otherlv_7= RULE_ID )
            // InternalSML.g:2003:3: otherlv_7= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getModalMessageRule());
              	        }
                      
            }
            otherlv_7=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_45); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_7, grammarAccess.getModalMessageAccess().getModelElementETypedElementCrossReference_6_0()); 
              	
            }

            }


            }

            // InternalSML.g:2014:2: (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )?
            int alt49=2;
            int LA49_0 = input.LA(1);

            if ( (LA49_0==35) ) {
                alt49=1;
            }
            switch (alt49) {
                case 1 :
                    // InternalSML.g:2014:4: otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) )
                    {
                    otherlv_8=(Token)match(input,35,FollowSets000.FOLLOW_46); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_8, grammarAccess.getModalMessageAccess().getFullStopKeyword_7_0());
                          
                    }
                    // InternalSML.g:2018:1: ( (lv_collectionModification_9_0= ruleCollectionModification ) )
                    // InternalSML.g:2019:1: (lv_collectionModification_9_0= ruleCollectionModification )
                    {
                    // InternalSML.g:2019:1: (lv_collectionModification_9_0= ruleCollectionModification )
                    // InternalSML.g:2020:3: lv_collectionModification_9_0= ruleCollectionModification
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getModalMessageAccess().getCollectionModificationCollectionModificationEnumRuleCall_7_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_47);
                    lv_collectionModification_9_0=ruleCollectionModification();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getModalMessageRule());
                      	        }
                             		set(
                             			current, 
                             			"collectionModification",
                              		lv_collectionModification_9_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionModification");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalSML.g:2036:4: (otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')' )?
            int alt52=2;
            int LA52_0 = input.LA(1);

            if ( (LA52_0==29) ) {
                alt52=1;
            }
            switch (alt52) {
                case 1 :
                    // InternalSML.g:2036:6: otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')'
                    {
                    otherlv_10=(Token)match(input,29,FollowSets000.FOLLOW_48); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_10, grammarAccess.getModalMessageAccess().getLeftParenthesisKeyword_8_0());
                          
                    }
                    // InternalSML.g:2040:1: ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )?
                    int alt51=2;
                    int LA51_0 = input.LA(1);

                    if ( ((LA51_0>=RULE_ID && LA51_0<=RULE_STRING)||LA51_0==RULE_BOOL||LA51_0==29||(LA51_0>=49 && LA51_0<=50)||LA51_0==80||(LA51_0>=82 && LA51_0<=83)) ) {
                        alt51=1;
                    }
                    switch (alt51) {
                        case 1 :
                            // InternalSML.g:2040:2: ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )*
                            {
                            // InternalSML.g:2040:2: ( (lv_parameters_11_0= ruleParameterBinding ) )
                            // InternalSML.g:2041:1: (lv_parameters_11_0= ruleParameterBinding )
                            {
                            // InternalSML.g:2041:1: (lv_parameters_11_0= ruleParameterBinding )
                            // InternalSML.g:2042:3: lv_parameters_11_0= ruleParameterBinding
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getModalMessageAccess().getParametersParameterBindingParserRuleCall_8_1_0_0()); 
                              	    
                            }
                            pushFollow(FollowSets000.FOLLOW_20);
                            lv_parameters_11_0=ruleParameterBinding();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getModalMessageRule());
                              	        }
                                     		add(
                                     			current, 
                                     			"parameters",
                                      		lv_parameters_11_0, 
                                      		"org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }

                            // InternalSML.g:2058:2: (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )*
                            loop50:
                            do {
                                int alt50=2;
                                int LA50_0 = input.LA(1);

                                if ( (LA50_0==22) ) {
                                    alt50=1;
                                }


                                switch (alt50) {
                            	case 1 :
                            	    // InternalSML.g:2058:4: otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) )
                            	    {
                            	    otherlv_12=(Token)match(input,22,FollowSets000.FOLLOW_49); if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	          	newLeafNode(otherlv_12, grammarAccess.getModalMessageAccess().getCommaKeyword_8_1_1_0());
                            	          
                            	    }
                            	    // InternalSML.g:2062:1: ( (lv_parameters_13_0= ruleParameterBinding ) )
                            	    // InternalSML.g:2063:1: (lv_parameters_13_0= ruleParameterBinding )
                            	    {
                            	    // InternalSML.g:2063:1: (lv_parameters_13_0= ruleParameterBinding )
                            	    // InternalSML.g:2064:3: lv_parameters_13_0= ruleParameterBinding
                            	    {
                            	    if ( state.backtracking==0 ) {
                            	       
                            	      	        newCompositeNode(grammarAccess.getModalMessageAccess().getParametersParameterBindingParserRuleCall_8_1_1_1_0()); 
                            	      	    
                            	    }
                            	    pushFollow(FollowSets000.FOLLOW_20);
                            	    lv_parameters_13_0=ruleParameterBinding();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      	        if (current==null) {
                            	      	            current = createModelElementForParent(grammarAccess.getModalMessageRule());
                            	      	        }
                            	             		add(
                            	             			current, 
                            	             			"parameters",
                            	              		lv_parameters_13_0, 
                            	              		"org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
                            	      	        afterParserOrEnumRuleCall();
                            	      	    
                            	    }

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop50;
                                }
                            } while (true);


                            }
                            break;

                    }

                    otherlv_14=(Token)match(input,30,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_14, grammarAccess.getModalMessageAccess().getRightParenthesisKeyword_8_2());
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModalMessage"


    // $ANTLR start "entryRuleParameterBinding"
    // InternalSML.g:2092:1: entryRuleParameterBinding returns [EObject current=null] : iv_ruleParameterBinding= ruleParameterBinding EOF ;
    public final EObject entryRuleParameterBinding() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameterBinding = null;


        try {
            // InternalSML.g:2093:2: (iv_ruleParameterBinding= ruleParameterBinding EOF )
            // InternalSML.g:2094:2: iv_ruleParameterBinding= ruleParameterBinding EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParameterBindingRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleParameterBinding=ruleParameterBinding();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParameterBinding; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameterBinding"


    // $ANTLR start "ruleParameterBinding"
    // InternalSML.g:2101:1: ruleParameterBinding returns [EObject current=null] : ( (lv_bindingExpression_0_0= ruleParameterExpression ) ) ;
    public final EObject ruleParameterBinding() throws RecognitionException {
        EObject current = null;

        EObject lv_bindingExpression_0_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2104:28: ( ( (lv_bindingExpression_0_0= ruleParameterExpression ) ) )
            // InternalSML.g:2105:1: ( (lv_bindingExpression_0_0= ruleParameterExpression ) )
            {
            // InternalSML.g:2105:1: ( (lv_bindingExpression_0_0= ruleParameterExpression ) )
            // InternalSML.g:2106:1: (lv_bindingExpression_0_0= ruleParameterExpression )
            {
            // InternalSML.g:2106:1: (lv_bindingExpression_0_0= ruleParameterExpression )
            // InternalSML.g:2107:3: lv_bindingExpression_0_0= ruleParameterExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getParameterBindingAccess().getBindingExpressionParameterExpressionParserRuleCall_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_bindingExpression_0_0=ruleParameterExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getParameterBindingRule());
              	        }
                     		set(
                     			current, 
                     			"bindingExpression",
                      		lv_bindingExpression_0_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ParameterExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameterBinding"


    // $ANTLR start "entryRuleParameterExpression"
    // InternalSML.g:2131:1: entryRuleParameterExpression returns [EObject current=null] : iv_ruleParameterExpression= ruleParameterExpression EOF ;
    public final EObject entryRuleParameterExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameterExpression = null;


        try {
            // InternalSML.g:2132:2: (iv_ruleParameterExpression= ruleParameterExpression EOF )
            // InternalSML.g:2133:2: iv_ruleParameterExpression= ruleParameterExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParameterExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleParameterExpression=ruleParameterExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParameterExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameterExpression"


    // $ANTLR start "ruleParameterExpression"
    // InternalSML.g:2140:1: ruleParameterExpression returns [EObject current=null] : (this_WildcardParameterExpression_0= ruleWildcardParameterExpression | this_ValueParameterExpression_1= ruleValueParameterExpression | this_VariableBindingParameterExpression_2= ruleVariableBindingParameterExpression ) ;
    public final EObject ruleParameterExpression() throws RecognitionException {
        EObject current = null;

        EObject this_WildcardParameterExpression_0 = null;

        EObject this_ValueParameterExpression_1 = null;

        EObject this_VariableBindingParameterExpression_2 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2143:28: ( (this_WildcardParameterExpression_0= ruleWildcardParameterExpression | this_ValueParameterExpression_1= ruleValueParameterExpression | this_VariableBindingParameterExpression_2= ruleVariableBindingParameterExpression ) )
            // InternalSML.g:2144:1: (this_WildcardParameterExpression_0= ruleWildcardParameterExpression | this_ValueParameterExpression_1= ruleValueParameterExpression | this_VariableBindingParameterExpression_2= ruleVariableBindingParameterExpression )
            {
            // InternalSML.g:2144:1: (this_WildcardParameterExpression_0= ruleWildcardParameterExpression | this_ValueParameterExpression_1= ruleValueParameterExpression | this_VariableBindingParameterExpression_2= ruleVariableBindingParameterExpression )
            int alt53=3;
            switch ( input.LA(1) ) {
            case 49:
                {
                alt53=1;
                }
                break;
            case RULE_ID:
            case RULE_INT:
            case RULE_SIGNEDINT:
            case RULE_STRING:
            case RULE_BOOL:
            case 29:
            case 80:
            case 82:
            case 83:
                {
                alt53=2;
                }
                break;
            case 50:
                {
                alt53=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 53, 0, input);

                throw nvae;
            }

            switch (alt53) {
                case 1 :
                    // InternalSML.g:2145:5: this_WildcardParameterExpression_0= ruleWildcardParameterExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getParameterExpressionAccess().getWildcardParameterExpressionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_WildcardParameterExpression_0=ruleWildcardParameterExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_WildcardParameterExpression_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:2155:5: this_ValueParameterExpression_1= ruleValueParameterExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getParameterExpressionAccess().getValueParameterExpressionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ValueParameterExpression_1=ruleValueParameterExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ValueParameterExpression_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalSML.g:2165:5: this_VariableBindingParameterExpression_2= ruleVariableBindingParameterExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getParameterExpressionAccess().getVariableBindingParameterExpressionParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableBindingParameterExpression_2=ruleVariableBindingParameterExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableBindingParameterExpression_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameterExpression"


    // $ANTLR start "entryRuleWildcardParameterExpression"
    // InternalSML.g:2181:1: entryRuleWildcardParameterExpression returns [EObject current=null] : iv_ruleWildcardParameterExpression= ruleWildcardParameterExpression EOF ;
    public final EObject entryRuleWildcardParameterExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWildcardParameterExpression = null;


        try {
            // InternalSML.g:2182:2: (iv_ruleWildcardParameterExpression= ruleWildcardParameterExpression EOF )
            // InternalSML.g:2183:2: iv_ruleWildcardParameterExpression= ruleWildcardParameterExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getWildcardParameterExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleWildcardParameterExpression=ruleWildcardParameterExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleWildcardParameterExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWildcardParameterExpression"


    // $ANTLR start "ruleWildcardParameterExpression"
    // InternalSML.g:2190:1: ruleWildcardParameterExpression returns [EObject current=null] : ( () otherlv_1= '*' ) ;
    public final EObject ruleWildcardParameterExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalSML.g:2193:28: ( ( () otherlv_1= '*' ) )
            // InternalSML.g:2194:1: ( () otherlv_1= '*' )
            {
            // InternalSML.g:2194:1: ( () otherlv_1= '*' )
            // InternalSML.g:2194:2: () otherlv_1= '*'
            {
            // InternalSML.g:2194:2: ()
            // InternalSML.g:2195:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getWildcardParameterExpressionAccess().getWildcardParameterExpressionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,49,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getWildcardParameterExpressionAccess().getAsteriskKeyword_1());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWildcardParameterExpression"


    // $ANTLR start "entryRuleValueParameterExpression"
    // InternalSML.g:2212:1: entryRuleValueParameterExpression returns [EObject current=null] : iv_ruleValueParameterExpression= ruleValueParameterExpression EOF ;
    public final EObject entryRuleValueParameterExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleValueParameterExpression = null;


        try {
            // InternalSML.g:2213:2: (iv_ruleValueParameterExpression= ruleValueParameterExpression EOF )
            // InternalSML.g:2214:2: iv_ruleValueParameterExpression= ruleValueParameterExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getValueParameterExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleValueParameterExpression=ruleValueParameterExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleValueParameterExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValueParameterExpression"


    // $ANTLR start "ruleValueParameterExpression"
    // InternalSML.g:2221:1: ruleValueParameterExpression returns [EObject current=null] : ( (lv_value_0_0= ruleExpression ) ) ;
    public final EObject ruleValueParameterExpression() throws RecognitionException {
        EObject current = null;

        EObject lv_value_0_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2224:28: ( ( (lv_value_0_0= ruleExpression ) ) )
            // InternalSML.g:2225:1: ( (lv_value_0_0= ruleExpression ) )
            {
            // InternalSML.g:2225:1: ( (lv_value_0_0= ruleExpression ) )
            // InternalSML.g:2226:1: (lv_value_0_0= ruleExpression )
            {
            // InternalSML.g:2226:1: (lv_value_0_0= ruleExpression )
            // InternalSML.g:2227:3: lv_value_0_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getValueParameterExpressionAccess().getValueExpressionParserRuleCall_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_value_0_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getValueParameterExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValueParameterExpression"


    // $ANTLR start "entryRuleVariableBindingParameterExpression"
    // InternalSML.g:2251:1: entryRuleVariableBindingParameterExpression returns [EObject current=null] : iv_ruleVariableBindingParameterExpression= ruleVariableBindingParameterExpression EOF ;
    public final EObject entryRuleVariableBindingParameterExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableBindingParameterExpression = null;


        try {
            // InternalSML.g:2252:2: (iv_ruleVariableBindingParameterExpression= ruleVariableBindingParameterExpression EOF )
            // InternalSML.g:2253:2: iv_ruleVariableBindingParameterExpression= ruleVariableBindingParameterExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableBindingParameterExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableBindingParameterExpression=ruleVariableBindingParameterExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableBindingParameterExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableBindingParameterExpression"


    // $ANTLR start "ruleVariableBindingParameterExpression"
    // InternalSML.g:2260:1: ruleVariableBindingParameterExpression returns [EObject current=null] : (otherlv_0= 'bind' ( (lv_variable_1_0= ruleVariableValue ) ) ) ;
    public final EObject ruleVariableBindingParameterExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_variable_1_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2263:28: ( (otherlv_0= 'bind' ( (lv_variable_1_0= ruleVariableValue ) ) ) )
            // InternalSML.g:2264:1: (otherlv_0= 'bind' ( (lv_variable_1_0= ruleVariableValue ) ) )
            {
            // InternalSML.g:2264:1: (otherlv_0= 'bind' ( (lv_variable_1_0= ruleVariableValue ) ) )
            // InternalSML.g:2264:3: otherlv_0= 'bind' ( (lv_variable_1_0= ruleVariableValue ) )
            {
            otherlv_0=(Token)match(input,50,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getVariableBindingParameterExpressionAccess().getBindKeyword_0());
                  
            }
            // InternalSML.g:2268:1: ( (lv_variable_1_0= ruleVariableValue ) )
            // InternalSML.g:2269:1: (lv_variable_1_0= ruleVariableValue )
            {
            // InternalSML.g:2269:1: (lv_variable_1_0= ruleVariableValue )
            // InternalSML.g:2270:3: lv_variable_1_0= ruleVariableValue
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getVariableBindingParameterExpressionAccess().getVariableVariableValueParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_variable_1_0=ruleVariableValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getVariableBindingParameterExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"variable",
                      		lv_variable_1_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.VariableValue");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableBindingParameterExpression"


    // $ANTLR start "entryRuleAlternative"
    // InternalSML.g:2294:1: entryRuleAlternative returns [EObject current=null] : iv_ruleAlternative= ruleAlternative EOF ;
    public final EObject entryRuleAlternative() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAlternative = null;


        try {
            // InternalSML.g:2295:2: (iv_ruleAlternative= ruleAlternative EOF )
            // InternalSML.g:2296:2: iv_ruleAlternative= ruleAlternative EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAlternativeRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAlternative=ruleAlternative();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAlternative; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAlternative"


    // $ANTLR start "ruleAlternative"
    // InternalSML.g:2303:1: ruleAlternative returns [EObject current=null] : ( () otherlv_1= 'alternative' ( (lv_cases_2_0= ruleCase ) ) (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )* ) ;
    public final EObject ruleAlternative() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_cases_2_0 = null;

        EObject lv_cases_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2306:28: ( ( () otherlv_1= 'alternative' ( (lv_cases_2_0= ruleCase ) ) (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )* ) )
            // InternalSML.g:2307:1: ( () otherlv_1= 'alternative' ( (lv_cases_2_0= ruleCase ) ) (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )* )
            {
            // InternalSML.g:2307:1: ( () otherlv_1= 'alternative' ( (lv_cases_2_0= ruleCase ) ) (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )* )
            // InternalSML.g:2307:2: () otherlv_1= 'alternative' ( (lv_cases_2_0= ruleCase ) ) (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )*
            {
            // InternalSML.g:2307:2: ()
            // InternalSML.g:2308:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getAlternativeAccess().getAlternativeAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,51,FollowSets000.FOLLOW_50); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getAlternativeAccess().getAlternativeKeyword_1());
                  
            }
            // InternalSML.g:2317:1: ( (lv_cases_2_0= ruleCase ) )
            // InternalSML.g:2318:1: (lv_cases_2_0= ruleCase )
            {
            // InternalSML.g:2318:1: (lv_cases_2_0= ruleCase )
            // InternalSML.g:2319:3: lv_cases_2_0= ruleCase
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getAlternativeAccess().getCasesCaseParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_cases_2_0=ruleCase();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getAlternativeRule());
              	        }
                     		add(
                     			current, 
                     			"cases",
                      		lv_cases_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.Case");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalSML.g:2335:2: (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )*
            loop54:
            do {
                int alt54=2;
                int LA54_0 = input.LA(1);

                if ( (LA54_0==52) ) {
                    alt54=1;
                }


                switch (alt54) {
            	case 1 :
            	    // InternalSML.g:2335:4: otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) )
            	    {
            	    otherlv_3=(Token)match(input,52,FollowSets000.FOLLOW_50); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getAlternativeAccess().getOrKeyword_3_0());
            	          
            	    }
            	    // InternalSML.g:2339:1: ( (lv_cases_4_0= ruleCase ) )
            	    // InternalSML.g:2340:1: (lv_cases_4_0= ruleCase )
            	    {
            	    // InternalSML.g:2340:1: (lv_cases_4_0= ruleCase )
            	    // InternalSML.g:2341:3: lv_cases_4_0= ruleCase
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getAlternativeAccess().getCasesCaseParserRuleCall_3_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_51);
            	    lv_cases_4_0=ruleCase();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getAlternativeRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"cases",
            	              		lv_cases_4_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.Case");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop54;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAlternative"


    // $ANTLR start "entryRuleCase"
    // InternalSML.g:2365:1: entryRuleCase returns [EObject current=null] : iv_ruleCase= ruleCase EOF ;
    public final EObject entryRuleCase() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCase = null;


        try {
            // InternalSML.g:2366:2: (iv_ruleCase= ruleCase EOF )
            // InternalSML.g:2367:2: iv_ruleCase= ruleCase EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCaseRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCase=ruleCase();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCase; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCase"


    // $ANTLR start "ruleCase"
    // InternalSML.g:2374:1: ruleCase returns [EObject current=null] : ( () ( (lv_caseCondition_1_0= ruleCondition ) )? ( (lv_caseInteraction_2_0= ruleInteraction ) ) ) ;
    public final EObject ruleCase() throws RecognitionException {
        EObject current = null;

        EObject lv_caseCondition_1_0 = null;

        EObject lv_caseInteraction_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2377:28: ( ( () ( (lv_caseCondition_1_0= ruleCondition ) )? ( (lv_caseInteraction_2_0= ruleInteraction ) ) ) )
            // InternalSML.g:2378:1: ( () ( (lv_caseCondition_1_0= ruleCondition ) )? ( (lv_caseInteraction_2_0= ruleInteraction ) ) )
            {
            // InternalSML.g:2378:1: ( () ( (lv_caseCondition_1_0= ruleCondition ) )? ( (lv_caseInteraction_2_0= ruleInteraction ) ) )
            // InternalSML.g:2378:2: () ( (lv_caseCondition_1_0= ruleCondition ) )? ( (lv_caseInteraction_2_0= ruleInteraction ) )
            {
            // InternalSML.g:2378:2: ()
            // InternalSML.g:2379:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getCaseAccess().getCaseAction_0(),
                          current);
                  
            }

            }

            // InternalSML.g:2384:2: ( (lv_caseCondition_1_0= ruleCondition ) )?
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==32) ) {
                alt55=1;
            }
            switch (alt55) {
                case 1 :
                    // InternalSML.g:2385:1: (lv_caseCondition_1_0= ruleCondition )
                    {
                    // InternalSML.g:2385:1: (lv_caseCondition_1_0= ruleCondition )
                    // InternalSML.g:2386:3: lv_caseCondition_1_0= ruleCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getCaseAccess().getCaseConditionConditionParserRuleCall_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_34);
                    lv_caseCondition_1_0=ruleCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getCaseRule());
                      	        }
                             		set(
                             			current, 
                             			"caseCondition",
                              		lv_caseCondition_1_0, 
                              		"org.scenariotools.sml.collaboration.Collaboration.Condition");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalSML.g:2402:3: ( (lv_caseInteraction_2_0= ruleInteraction ) )
            // InternalSML.g:2403:1: (lv_caseInteraction_2_0= ruleInteraction )
            {
            // InternalSML.g:2403:1: (lv_caseInteraction_2_0= ruleInteraction )
            // InternalSML.g:2404:3: lv_caseInteraction_2_0= ruleInteraction
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getCaseAccess().getCaseInteractionInteractionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_caseInteraction_2_0=ruleInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getCaseRule());
              	        }
                     		set(
                     			current, 
                     			"caseInteraction",
                      		lv_caseInteraction_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCase"


    // $ANTLR start "entryRuleLoop"
    // InternalSML.g:2428:1: entryRuleLoop returns [EObject current=null] : iv_ruleLoop= ruleLoop EOF ;
    public final EObject entryRuleLoop() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLoop = null;


        try {
            // InternalSML.g:2429:2: (iv_ruleLoop= ruleLoop EOF )
            // InternalSML.g:2430:2: iv_ruleLoop= ruleLoop EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLoopRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleLoop=ruleLoop();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLoop; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLoop"


    // $ANTLR start "ruleLoop"
    // InternalSML.g:2437:1: ruleLoop returns [EObject current=null] : (otherlv_0= 'while' ( (lv_loopCondition_1_0= ruleCondition ) )? ( (lv_bodyInteraction_2_0= ruleInteraction ) ) ) ;
    public final EObject ruleLoop() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_loopCondition_1_0 = null;

        EObject lv_bodyInteraction_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2440:28: ( (otherlv_0= 'while' ( (lv_loopCondition_1_0= ruleCondition ) )? ( (lv_bodyInteraction_2_0= ruleInteraction ) ) ) )
            // InternalSML.g:2441:1: (otherlv_0= 'while' ( (lv_loopCondition_1_0= ruleCondition ) )? ( (lv_bodyInteraction_2_0= ruleInteraction ) ) )
            {
            // InternalSML.g:2441:1: (otherlv_0= 'while' ( (lv_loopCondition_1_0= ruleCondition ) )? ( (lv_bodyInteraction_2_0= ruleInteraction ) ) )
            // InternalSML.g:2441:3: otherlv_0= 'while' ( (lv_loopCondition_1_0= ruleCondition ) )? ( (lv_bodyInteraction_2_0= ruleInteraction ) )
            {
            otherlv_0=(Token)match(input,53,FollowSets000.FOLLOW_50); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getLoopAccess().getWhileKeyword_0());
                  
            }
            // InternalSML.g:2445:1: ( (lv_loopCondition_1_0= ruleCondition ) )?
            int alt56=2;
            int LA56_0 = input.LA(1);

            if ( (LA56_0==32) ) {
                alt56=1;
            }
            switch (alt56) {
                case 1 :
                    // InternalSML.g:2446:1: (lv_loopCondition_1_0= ruleCondition )
                    {
                    // InternalSML.g:2446:1: (lv_loopCondition_1_0= ruleCondition )
                    // InternalSML.g:2447:3: lv_loopCondition_1_0= ruleCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getLoopAccess().getLoopConditionConditionParserRuleCall_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_34);
                    lv_loopCondition_1_0=ruleCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getLoopRule());
                      	        }
                             		set(
                             			current, 
                             			"loopCondition",
                              		lv_loopCondition_1_0, 
                              		"org.scenariotools.sml.collaboration.Collaboration.Condition");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalSML.g:2463:3: ( (lv_bodyInteraction_2_0= ruleInteraction ) )
            // InternalSML.g:2464:1: (lv_bodyInteraction_2_0= ruleInteraction )
            {
            // InternalSML.g:2464:1: (lv_bodyInteraction_2_0= ruleInteraction )
            // InternalSML.g:2465:3: lv_bodyInteraction_2_0= ruleInteraction
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLoopAccess().getBodyInteractionInteractionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_bodyInteraction_2_0=ruleInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLoopRule());
              	        }
                     		set(
                     			current, 
                     			"bodyInteraction",
                      		lv_bodyInteraction_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLoop"


    // $ANTLR start "entryRuleParallel"
    // InternalSML.g:2489:1: entryRuleParallel returns [EObject current=null] : iv_ruleParallel= ruleParallel EOF ;
    public final EObject entryRuleParallel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParallel = null;


        try {
            // InternalSML.g:2490:2: (iv_ruleParallel= ruleParallel EOF )
            // InternalSML.g:2491:2: iv_ruleParallel= ruleParallel EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParallelRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleParallel=ruleParallel();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParallel; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParallel"


    // $ANTLR start "ruleParallel"
    // InternalSML.g:2498:1: ruleParallel returns [EObject current=null] : ( () otherlv_1= 'parallel' ( (lv_parallelInteraction_2_0= ruleInteraction ) ) (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )* ) ;
    public final EObject ruleParallel() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_parallelInteraction_2_0 = null;

        EObject lv_parallelInteraction_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2501:28: ( ( () otherlv_1= 'parallel' ( (lv_parallelInteraction_2_0= ruleInteraction ) ) (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )* ) )
            // InternalSML.g:2502:1: ( () otherlv_1= 'parallel' ( (lv_parallelInteraction_2_0= ruleInteraction ) ) (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )* )
            {
            // InternalSML.g:2502:1: ( () otherlv_1= 'parallel' ( (lv_parallelInteraction_2_0= ruleInteraction ) ) (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )* )
            // InternalSML.g:2502:2: () otherlv_1= 'parallel' ( (lv_parallelInteraction_2_0= ruleInteraction ) ) (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )*
            {
            // InternalSML.g:2502:2: ()
            // InternalSML.g:2503:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getParallelAccess().getParallelAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,54,FollowSets000.FOLLOW_34); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getParallelAccess().getParallelKeyword_1());
                  
            }
            // InternalSML.g:2512:1: ( (lv_parallelInteraction_2_0= ruleInteraction ) )
            // InternalSML.g:2513:1: (lv_parallelInteraction_2_0= ruleInteraction )
            {
            // InternalSML.g:2513:1: (lv_parallelInteraction_2_0= ruleInteraction )
            // InternalSML.g:2514:3: lv_parallelInteraction_2_0= ruleInteraction
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getParallelAccess().getParallelInteractionInteractionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_52);
            lv_parallelInteraction_2_0=ruleInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getParallelRule());
              	        }
                     		add(
                     			current, 
                     			"parallelInteraction",
                      		lv_parallelInteraction_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalSML.g:2530:2: (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )*
            loop57:
            do {
                int alt57=2;
                int LA57_0 = input.LA(1);

                if ( (LA57_0==55) ) {
                    alt57=1;
                }


                switch (alt57) {
            	case 1 :
            	    // InternalSML.g:2530:4: otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) )
            	    {
            	    otherlv_3=(Token)match(input,55,FollowSets000.FOLLOW_34); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getParallelAccess().getAndKeyword_3_0());
            	          
            	    }
            	    // InternalSML.g:2534:1: ( (lv_parallelInteraction_4_0= ruleInteraction ) )
            	    // InternalSML.g:2535:1: (lv_parallelInteraction_4_0= ruleInteraction )
            	    {
            	    // InternalSML.g:2535:1: (lv_parallelInteraction_4_0= ruleInteraction )
            	    // InternalSML.g:2536:3: lv_parallelInteraction_4_0= ruleInteraction
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getParallelAccess().getParallelInteractionInteractionParserRuleCall_3_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_52);
            	    lv_parallelInteraction_4_0=ruleInteraction();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getParallelRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"parallelInteraction",
            	              		lv_parallelInteraction_4_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop57;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParallel"


    // $ANTLR start "entryRuleTimedConditionFragment"
    // InternalSML.g:2560:1: entryRuleTimedConditionFragment returns [EObject current=null] : iv_ruleTimedConditionFragment= ruleTimedConditionFragment EOF ;
    public final EObject entryRuleTimedConditionFragment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimedConditionFragment = null;


        try {
            // InternalSML.g:2561:2: (iv_ruleTimedConditionFragment= ruleTimedConditionFragment EOF )
            // InternalSML.g:2562:2: iv_ruleTimedConditionFragment= ruleTimedConditionFragment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimedConditionFragmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleTimedConditionFragment=ruleTimedConditionFragment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTimedConditionFragment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimedConditionFragment"


    // $ANTLR start "ruleTimedConditionFragment"
    // InternalSML.g:2569:1: ruleTimedConditionFragment returns [EObject current=null] : (this_TimedWaitCondition_0= ruleTimedWaitCondition | this_TimedInterruptCondition_1= ruleTimedInterruptCondition | this_TimedViolationCondition_2= ruleTimedViolationCondition ) ;
    public final EObject ruleTimedConditionFragment() throws RecognitionException {
        EObject current = null;

        EObject this_TimedWaitCondition_0 = null;

        EObject this_TimedInterruptCondition_1 = null;

        EObject this_TimedViolationCondition_2 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2572:28: ( (this_TimedWaitCondition_0= ruleTimedWaitCondition | this_TimedInterruptCondition_1= ruleTimedInterruptCondition | this_TimedViolationCondition_2= ruleTimedViolationCondition ) )
            // InternalSML.g:2573:1: (this_TimedWaitCondition_0= ruleTimedWaitCondition | this_TimedInterruptCondition_1= ruleTimedInterruptCondition | this_TimedViolationCondition_2= ruleTimedViolationCondition )
            {
            // InternalSML.g:2573:1: (this_TimedWaitCondition_0= ruleTimedWaitCondition | this_TimedInterruptCondition_1= ruleTimedInterruptCondition | this_TimedViolationCondition_2= ruleTimedViolationCondition )
            int alt58=3;
            int LA58_0 = input.LA(1);

            if ( (LA58_0==60) ) {
                switch ( input.LA(2) ) {
                case 59:
                    {
                    alt58=3;
                    }
                    break;
                case 56:
                    {
                    alt58=1;
                    }
                    break;
                case 58:
                    {
                    alt58=2;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 58, 1, input);

                    throw nvae;
                }

            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 58, 0, input);

                throw nvae;
            }
            switch (alt58) {
                case 1 :
                    // InternalSML.g:2574:5: this_TimedWaitCondition_0= ruleTimedWaitCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTimedConditionFragmentAccess().getTimedWaitConditionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_TimedWaitCondition_0=ruleTimedWaitCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TimedWaitCondition_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:2584:5: this_TimedInterruptCondition_1= ruleTimedInterruptCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTimedConditionFragmentAccess().getTimedInterruptConditionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_TimedInterruptCondition_1=ruleTimedInterruptCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TimedInterruptCondition_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalSML.g:2594:5: this_TimedViolationCondition_2= ruleTimedViolationCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTimedConditionFragmentAccess().getTimedViolationConditionParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_TimedViolationCondition_2=ruleTimedViolationCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TimedViolationCondition_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimedConditionFragment"


    // $ANTLR start "entryRuleConditionFragment"
    // InternalSML.g:2610:1: entryRuleConditionFragment returns [EObject current=null] : iv_ruleConditionFragment= ruleConditionFragment EOF ;
    public final EObject entryRuleConditionFragment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConditionFragment = null;


        try {
            // InternalSML.g:2611:2: (iv_ruleConditionFragment= ruleConditionFragment EOF )
            // InternalSML.g:2612:2: iv_ruleConditionFragment= ruleConditionFragment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConditionFragmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConditionFragment=ruleConditionFragment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConditionFragment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConditionFragment"


    // $ANTLR start "ruleConditionFragment"
    // InternalSML.g:2619:1: ruleConditionFragment returns [EObject current=null] : (this_WaitCondition_0= ruleWaitCondition | this_InterruptCondition_1= ruleInterruptCondition | this_ViolationCondition_2= ruleViolationCondition ) ;
    public final EObject ruleConditionFragment() throws RecognitionException {
        EObject current = null;

        EObject this_WaitCondition_0 = null;

        EObject this_InterruptCondition_1 = null;

        EObject this_ViolationCondition_2 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2622:28: ( (this_WaitCondition_0= ruleWaitCondition | this_InterruptCondition_1= ruleInterruptCondition | this_ViolationCondition_2= ruleViolationCondition ) )
            // InternalSML.g:2623:1: (this_WaitCondition_0= ruleWaitCondition | this_InterruptCondition_1= ruleInterruptCondition | this_ViolationCondition_2= ruleViolationCondition )
            {
            // InternalSML.g:2623:1: (this_WaitCondition_0= ruleWaitCondition | this_InterruptCondition_1= ruleInterruptCondition | this_ViolationCondition_2= ruleViolationCondition )
            int alt59=3;
            switch ( input.LA(1) ) {
            case 56:
                {
                alt59=1;
                }
                break;
            case 58:
                {
                alt59=2;
                }
                break;
            case 59:
                {
                alt59=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 59, 0, input);

                throw nvae;
            }

            switch (alt59) {
                case 1 :
                    // InternalSML.g:2624:5: this_WaitCondition_0= ruleWaitCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getConditionFragmentAccess().getWaitConditionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_WaitCondition_0=ruleWaitCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_WaitCondition_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:2634:5: this_InterruptCondition_1= ruleInterruptCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getConditionFragmentAccess().getInterruptConditionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_InterruptCondition_1=ruleInterruptCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_InterruptCondition_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalSML.g:2644:5: this_ViolationCondition_2= ruleViolationCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getConditionFragmentAccess().getViolationConditionParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ViolationCondition_2=ruleViolationCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ViolationCondition_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConditionFragment"


    // $ANTLR start "entryRuleWaitCondition"
    // InternalSML.g:2660:1: entryRuleWaitCondition returns [EObject current=null] : iv_ruleWaitCondition= ruleWaitCondition EOF ;
    public final EObject entryRuleWaitCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWaitCondition = null;


        try {
            // InternalSML.g:2661:2: (iv_ruleWaitCondition= ruleWaitCondition EOF )
            // InternalSML.g:2662:2: iv_ruleWaitCondition= ruleWaitCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getWaitConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleWaitCondition=ruleWaitCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleWaitCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWaitCondition"


    // $ANTLR start "ruleWaitCondition"
    // InternalSML.g:2669:1: ruleWaitCondition returns [EObject current=null] : (otherlv_0= 'wait' ( (lv_strict_1_0= 'strict' ) )? ( (lv_requested_2_0= 'eventually' ) )? otherlv_3= '[' ( (lv_conditionExpression_4_0= ruleConditionExpression ) ) otherlv_5= ']' ) ;
    public final EObject ruleWaitCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_strict_1_0=null;
        Token lv_requested_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_conditionExpression_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2672:28: ( (otherlv_0= 'wait' ( (lv_strict_1_0= 'strict' ) )? ( (lv_requested_2_0= 'eventually' ) )? otherlv_3= '[' ( (lv_conditionExpression_4_0= ruleConditionExpression ) ) otherlv_5= ']' ) )
            // InternalSML.g:2673:1: (otherlv_0= 'wait' ( (lv_strict_1_0= 'strict' ) )? ( (lv_requested_2_0= 'eventually' ) )? otherlv_3= '[' ( (lv_conditionExpression_4_0= ruleConditionExpression ) ) otherlv_5= ']' )
            {
            // InternalSML.g:2673:1: (otherlv_0= 'wait' ( (lv_strict_1_0= 'strict' ) )? ( (lv_requested_2_0= 'eventually' ) )? otherlv_3= '[' ( (lv_conditionExpression_4_0= ruleConditionExpression ) ) otherlv_5= ']' )
            // InternalSML.g:2673:3: otherlv_0= 'wait' ( (lv_strict_1_0= 'strict' ) )? ( (lv_requested_2_0= 'eventually' ) )? otherlv_3= '[' ( (lv_conditionExpression_4_0= ruleConditionExpression ) ) otherlv_5= ']'
            {
            otherlv_0=(Token)match(input,56,FollowSets000.FOLLOW_53); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getWaitConditionAccess().getWaitKeyword_0());
                  
            }
            // InternalSML.g:2677:1: ( (lv_strict_1_0= 'strict' ) )?
            int alt60=2;
            int LA60_0 = input.LA(1);

            if ( (LA60_0==46) ) {
                alt60=1;
            }
            switch (alt60) {
                case 1 :
                    // InternalSML.g:2678:1: (lv_strict_1_0= 'strict' )
                    {
                    // InternalSML.g:2678:1: (lv_strict_1_0= 'strict' )
                    // InternalSML.g:2679:3: lv_strict_1_0= 'strict'
                    {
                    lv_strict_1_0=(Token)match(input,46,FollowSets000.FOLLOW_54); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_strict_1_0, grammarAccess.getWaitConditionAccess().getStrictStrictKeyword_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getWaitConditionRule());
                      	        }
                             		setWithLastConsumed(current, "strict", true, "strict");
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalSML.g:2692:3: ( (lv_requested_2_0= 'eventually' ) )?
            int alt61=2;
            int LA61_0 = input.LA(1);

            if ( (LA61_0==57) ) {
                alt61=1;
            }
            switch (alt61) {
                case 1 :
                    // InternalSML.g:2693:1: (lv_requested_2_0= 'eventually' )
                    {
                    // InternalSML.g:2693:1: (lv_requested_2_0= 'eventually' )
                    // InternalSML.g:2694:3: lv_requested_2_0= 'eventually'
                    {
                    lv_requested_2_0=(Token)match(input,57,FollowSets000.FOLLOW_22); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_requested_2_0, grammarAccess.getWaitConditionAccess().getRequestedEventuallyKeyword_2_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getWaitConditionRule());
                      	        }
                             		setWithLastConsumed(current, "requested", true, "eventually");
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,32,FollowSets000.FOLLOW_55); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getWaitConditionAccess().getLeftSquareBracketKeyword_3());
                  
            }
            // InternalSML.g:2711:1: ( (lv_conditionExpression_4_0= ruleConditionExpression ) )
            // InternalSML.g:2712:1: (lv_conditionExpression_4_0= ruleConditionExpression )
            {
            // InternalSML.g:2712:1: (lv_conditionExpression_4_0= ruleConditionExpression )
            // InternalSML.g:2713:3: lv_conditionExpression_4_0= ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getWaitConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_4_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_24);
            lv_conditionExpression_4_0=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getWaitConditionRule());
              	        }
                     		set(
                     			current, 
                     			"conditionExpression",
                      		lv_conditionExpression_4_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_5=(Token)match(input,33,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getWaitConditionAccess().getRightSquareBracketKeyword_5());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWaitCondition"


    // $ANTLR start "entryRuleInterruptCondition"
    // InternalSML.g:2741:1: entryRuleInterruptCondition returns [EObject current=null] : iv_ruleInterruptCondition= ruleInterruptCondition EOF ;
    public final EObject entryRuleInterruptCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInterruptCondition = null;


        try {
            // InternalSML.g:2742:2: (iv_ruleInterruptCondition= ruleInterruptCondition EOF )
            // InternalSML.g:2743:2: iv_ruleInterruptCondition= ruleInterruptCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInterruptConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleInterruptCondition=ruleInterruptCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInterruptCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInterruptCondition"


    // $ANTLR start "ruleInterruptCondition"
    // InternalSML.g:2750:1: ruleInterruptCondition returns [EObject current=null] : (otherlv_0= 'interrupt' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' ) ;
    public final EObject ruleInterruptCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_conditionExpression_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2753:28: ( (otherlv_0= 'interrupt' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' ) )
            // InternalSML.g:2754:1: (otherlv_0= 'interrupt' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' )
            {
            // InternalSML.g:2754:1: (otherlv_0= 'interrupt' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' )
            // InternalSML.g:2754:3: otherlv_0= 'interrupt' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']'
            {
            otherlv_0=(Token)match(input,58,FollowSets000.FOLLOW_22); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getInterruptConditionAccess().getInterruptKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,32,FollowSets000.FOLLOW_55); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getInterruptConditionAccess().getLeftSquareBracketKeyword_1());
                  
            }
            // InternalSML.g:2762:1: ( (lv_conditionExpression_2_0= ruleConditionExpression ) )
            // InternalSML.g:2763:1: (lv_conditionExpression_2_0= ruleConditionExpression )
            {
            // InternalSML.g:2763:1: (lv_conditionExpression_2_0= ruleConditionExpression )
            // InternalSML.g:2764:3: lv_conditionExpression_2_0= ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getInterruptConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_24);
            lv_conditionExpression_2_0=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getInterruptConditionRule());
              	        }
                     		set(
                     			current, 
                     			"conditionExpression",
                      		lv_conditionExpression_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,33,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getInterruptConditionAccess().getRightSquareBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInterruptCondition"


    // $ANTLR start "entryRuleViolationCondition"
    // InternalSML.g:2792:1: entryRuleViolationCondition returns [EObject current=null] : iv_ruleViolationCondition= ruleViolationCondition EOF ;
    public final EObject entryRuleViolationCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleViolationCondition = null;


        try {
            // InternalSML.g:2793:2: (iv_ruleViolationCondition= ruleViolationCondition EOF )
            // InternalSML.g:2794:2: iv_ruleViolationCondition= ruleViolationCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getViolationConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleViolationCondition=ruleViolationCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleViolationCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleViolationCondition"


    // $ANTLR start "ruleViolationCondition"
    // InternalSML.g:2801:1: ruleViolationCondition returns [EObject current=null] : (otherlv_0= 'violation' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' ) ;
    public final EObject ruleViolationCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_conditionExpression_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2804:28: ( (otherlv_0= 'violation' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' ) )
            // InternalSML.g:2805:1: (otherlv_0= 'violation' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' )
            {
            // InternalSML.g:2805:1: (otherlv_0= 'violation' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' )
            // InternalSML.g:2805:3: otherlv_0= 'violation' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']'
            {
            otherlv_0=(Token)match(input,59,FollowSets000.FOLLOW_22); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getViolationConditionAccess().getViolationKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,32,FollowSets000.FOLLOW_55); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getViolationConditionAccess().getLeftSquareBracketKeyword_1());
                  
            }
            // InternalSML.g:2813:1: ( (lv_conditionExpression_2_0= ruleConditionExpression ) )
            // InternalSML.g:2814:1: (lv_conditionExpression_2_0= ruleConditionExpression )
            {
            // InternalSML.g:2814:1: (lv_conditionExpression_2_0= ruleConditionExpression )
            // InternalSML.g:2815:3: lv_conditionExpression_2_0= ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getViolationConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_24);
            lv_conditionExpression_2_0=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getViolationConditionRule());
              	        }
                     		set(
                     			current, 
                     			"conditionExpression",
                      		lv_conditionExpression_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,33,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getViolationConditionAccess().getRightSquareBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleViolationCondition"


    // $ANTLR start "entryRuleTimedWaitCondition"
    // InternalSML.g:2843:1: entryRuleTimedWaitCondition returns [EObject current=null] : iv_ruleTimedWaitCondition= ruleTimedWaitCondition EOF ;
    public final EObject entryRuleTimedWaitCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimedWaitCondition = null;


        try {
            // InternalSML.g:2844:2: (iv_ruleTimedWaitCondition= ruleTimedWaitCondition EOF )
            // InternalSML.g:2845:2: iv_ruleTimedWaitCondition= ruleTimedWaitCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimedWaitConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleTimedWaitCondition=ruleTimedWaitCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTimedWaitCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimedWaitCondition"


    // $ANTLR start "ruleTimedWaitCondition"
    // InternalSML.g:2852:1: ruleTimedWaitCondition returns [EObject current=null] : (otherlv_0= 'timed' otherlv_1= 'wait' ( (lv_strict_2_0= 'strict' ) )? ( (lv_requested_3_0= 'eventually' ) )? otherlv_4= '[' ( (lv_timedConditionExpression_5_0= ruleTimedExpression ) ) otherlv_6= ']' ) ;
    public final EObject ruleTimedWaitCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_strict_2_0=null;
        Token lv_requested_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_timedConditionExpression_5_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2855:28: ( (otherlv_0= 'timed' otherlv_1= 'wait' ( (lv_strict_2_0= 'strict' ) )? ( (lv_requested_3_0= 'eventually' ) )? otherlv_4= '[' ( (lv_timedConditionExpression_5_0= ruleTimedExpression ) ) otherlv_6= ']' ) )
            // InternalSML.g:2856:1: (otherlv_0= 'timed' otherlv_1= 'wait' ( (lv_strict_2_0= 'strict' ) )? ( (lv_requested_3_0= 'eventually' ) )? otherlv_4= '[' ( (lv_timedConditionExpression_5_0= ruleTimedExpression ) ) otherlv_6= ']' )
            {
            // InternalSML.g:2856:1: (otherlv_0= 'timed' otherlv_1= 'wait' ( (lv_strict_2_0= 'strict' ) )? ( (lv_requested_3_0= 'eventually' ) )? otherlv_4= '[' ( (lv_timedConditionExpression_5_0= ruleTimedExpression ) ) otherlv_6= ']' )
            // InternalSML.g:2856:3: otherlv_0= 'timed' otherlv_1= 'wait' ( (lv_strict_2_0= 'strict' ) )? ( (lv_requested_3_0= 'eventually' ) )? otherlv_4= '[' ( (lv_timedConditionExpression_5_0= ruleTimedExpression ) ) otherlv_6= ']'
            {
            otherlv_0=(Token)match(input,60,FollowSets000.FOLLOW_56); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getTimedWaitConditionAccess().getTimedKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,56,FollowSets000.FOLLOW_53); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getTimedWaitConditionAccess().getWaitKeyword_1());
                  
            }
            // InternalSML.g:2864:1: ( (lv_strict_2_0= 'strict' ) )?
            int alt62=2;
            int LA62_0 = input.LA(1);

            if ( (LA62_0==46) ) {
                alt62=1;
            }
            switch (alt62) {
                case 1 :
                    // InternalSML.g:2865:1: (lv_strict_2_0= 'strict' )
                    {
                    // InternalSML.g:2865:1: (lv_strict_2_0= 'strict' )
                    // InternalSML.g:2866:3: lv_strict_2_0= 'strict'
                    {
                    lv_strict_2_0=(Token)match(input,46,FollowSets000.FOLLOW_54); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_strict_2_0, grammarAccess.getTimedWaitConditionAccess().getStrictStrictKeyword_2_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getTimedWaitConditionRule());
                      	        }
                             		setWithLastConsumed(current, "strict", true, "strict");
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalSML.g:2879:3: ( (lv_requested_3_0= 'eventually' ) )?
            int alt63=2;
            int LA63_0 = input.LA(1);

            if ( (LA63_0==57) ) {
                alt63=1;
            }
            switch (alt63) {
                case 1 :
                    // InternalSML.g:2880:1: (lv_requested_3_0= 'eventually' )
                    {
                    // InternalSML.g:2880:1: (lv_requested_3_0= 'eventually' )
                    // InternalSML.g:2881:3: lv_requested_3_0= 'eventually'
                    {
                    lv_requested_3_0=(Token)match(input,57,FollowSets000.FOLLOW_22); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_requested_3_0, grammarAccess.getTimedWaitConditionAccess().getRequestedEventuallyKeyword_3_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getTimedWaitConditionRule());
                      	        }
                             		setWithLastConsumed(current, "requested", true, "eventually");
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,32,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getTimedWaitConditionAccess().getLeftSquareBracketKeyword_4());
                  
            }
            // InternalSML.g:2898:1: ( (lv_timedConditionExpression_5_0= ruleTimedExpression ) )
            // InternalSML.g:2899:1: (lv_timedConditionExpression_5_0= ruleTimedExpression )
            {
            // InternalSML.g:2899:1: (lv_timedConditionExpression_5_0= ruleTimedExpression )
            // InternalSML.g:2900:3: lv_timedConditionExpression_5_0= ruleTimedExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getTimedWaitConditionAccess().getTimedConditionExpressionTimedExpressionParserRuleCall_5_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_24);
            lv_timedConditionExpression_5_0=ruleTimedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getTimedWaitConditionRule());
              	        }
                     		set(
                     			current, 
                     			"timedConditionExpression",
                      		lv_timedConditionExpression_5_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.TimedExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_6=(Token)match(input,33,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getTimedWaitConditionAccess().getRightSquareBracketKeyword_6());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimedWaitCondition"


    // $ANTLR start "entryRuleTimedViolationCondition"
    // InternalSML.g:2928:1: entryRuleTimedViolationCondition returns [EObject current=null] : iv_ruleTimedViolationCondition= ruleTimedViolationCondition EOF ;
    public final EObject entryRuleTimedViolationCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimedViolationCondition = null;


        try {
            // InternalSML.g:2929:2: (iv_ruleTimedViolationCondition= ruleTimedViolationCondition EOF )
            // InternalSML.g:2930:2: iv_ruleTimedViolationCondition= ruleTimedViolationCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimedViolationConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleTimedViolationCondition=ruleTimedViolationCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTimedViolationCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimedViolationCondition"


    // $ANTLR start "ruleTimedViolationCondition"
    // InternalSML.g:2937:1: ruleTimedViolationCondition returns [EObject current=null] : (otherlv_0= 'timed' otherlv_1= 'violation' otherlv_2= '[' ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) ) otherlv_4= ']' ) ;
    public final EObject ruleTimedViolationCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_timedConditionExpression_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2940:28: ( (otherlv_0= 'timed' otherlv_1= 'violation' otherlv_2= '[' ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) ) otherlv_4= ']' ) )
            // InternalSML.g:2941:1: (otherlv_0= 'timed' otherlv_1= 'violation' otherlv_2= '[' ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) ) otherlv_4= ']' )
            {
            // InternalSML.g:2941:1: (otherlv_0= 'timed' otherlv_1= 'violation' otherlv_2= '[' ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) ) otherlv_4= ']' )
            // InternalSML.g:2941:3: otherlv_0= 'timed' otherlv_1= 'violation' otherlv_2= '[' ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) ) otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,60,FollowSets000.FOLLOW_57); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getTimedViolationConditionAccess().getTimedKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,59,FollowSets000.FOLLOW_22); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getTimedViolationConditionAccess().getViolationKeyword_1());
                  
            }
            otherlv_2=(Token)match(input,32,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getTimedViolationConditionAccess().getLeftSquareBracketKeyword_2());
                  
            }
            // InternalSML.g:2953:1: ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) )
            // InternalSML.g:2954:1: (lv_timedConditionExpression_3_0= ruleTimedExpression )
            {
            // InternalSML.g:2954:1: (lv_timedConditionExpression_3_0= ruleTimedExpression )
            // InternalSML.g:2955:3: lv_timedConditionExpression_3_0= ruleTimedExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getTimedViolationConditionAccess().getTimedConditionExpressionTimedExpressionParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_24);
            lv_timedConditionExpression_3_0=ruleTimedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getTimedViolationConditionRule());
              	        }
                     		set(
                     			current, 
                     			"timedConditionExpression",
                      		lv_timedConditionExpression_3_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.TimedExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,33,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getTimedViolationConditionAccess().getRightSquareBracketKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimedViolationCondition"


    // $ANTLR start "entryRuleTimedInterruptCondition"
    // InternalSML.g:2983:1: entryRuleTimedInterruptCondition returns [EObject current=null] : iv_ruleTimedInterruptCondition= ruleTimedInterruptCondition EOF ;
    public final EObject entryRuleTimedInterruptCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimedInterruptCondition = null;


        try {
            // InternalSML.g:2984:2: (iv_ruleTimedInterruptCondition= ruleTimedInterruptCondition EOF )
            // InternalSML.g:2985:2: iv_ruleTimedInterruptCondition= ruleTimedInterruptCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimedInterruptConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleTimedInterruptCondition=ruleTimedInterruptCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTimedInterruptCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimedInterruptCondition"


    // $ANTLR start "ruleTimedInterruptCondition"
    // InternalSML.g:2992:1: ruleTimedInterruptCondition returns [EObject current=null] : (otherlv_0= 'timed' otherlv_1= 'interrupt' otherlv_2= '[' ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) ) otherlv_4= ']' ) ;
    public final EObject ruleTimedInterruptCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_timedConditionExpression_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2995:28: ( (otherlv_0= 'timed' otherlv_1= 'interrupt' otherlv_2= '[' ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) ) otherlv_4= ']' ) )
            // InternalSML.g:2996:1: (otherlv_0= 'timed' otherlv_1= 'interrupt' otherlv_2= '[' ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) ) otherlv_4= ']' )
            {
            // InternalSML.g:2996:1: (otherlv_0= 'timed' otherlv_1= 'interrupt' otherlv_2= '[' ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) ) otherlv_4= ']' )
            // InternalSML.g:2996:3: otherlv_0= 'timed' otherlv_1= 'interrupt' otherlv_2= '[' ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) ) otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,60,FollowSets000.FOLLOW_58); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getTimedInterruptConditionAccess().getTimedKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,58,FollowSets000.FOLLOW_22); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getTimedInterruptConditionAccess().getInterruptKeyword_1());
                  
            }
            otherlv_2=(Token)match(input,32,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getTimedInterruptConditionAccess().getLeftSquareBracketKeyword_2());
                  
            }
            // InternalSML.g:3008:1: ( (lv_timedConditionExpression_3_0= ruleTimedExpression ) )
            // InternalSML.g:3009:1: (lv_timedConditionExpression_3_0= ruleTimedExpression )
            {
            // InternalSML.g:3009:1: (lv_timedConditionExpression_3_0= ruleTimedExpression )
            // InternalSML.g:3010:3: lv_timedConditionExpression_3_0= ruleTimedExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getTimedInterruptConditionAccess().getTimedConditionExpressionTimedExpressionParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_24);
            lv_timedConditionExpression_3_0=ruleTimedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getTimedInterruptConditionRule());
              	        }
                     		set(
                     			current, 
                     			"timedConditionExpression",
                      		lv_timedConditionExpression_3_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.TimedExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,33,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getTimedInterruptConditionAccess().getRightSquareBracketKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimedInterruptCondition"


    // $ANTLR start "entryRuleCondition"
    // InternalSML.g:3038:1: entryRuleCondition returns [EObject current=null] : iv_ruleCondition= ruleCondition EOF ;
    public final EObject entryRuleCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCondition = null;


        try {
            // InternalSML.g:3039:2: (iv_ruleCondition= ruleCondition EOF )
            // InternalSML.g:3040:2: iv_ruleCondition= ruleCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCondition=ruleCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCondition"


    // $ANTLR start "ruleCondition"
    // InternalSML.g:3047:1: ruleCondition returns [EObject current=null] : (otherlv_0= '[' ( (lv_conditionExpression_1_0= ruleConditionExpression ) ) otherlv_2= ']' ) ;
    public final EObject ruleCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_conditionExpression_1_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3050:28: ( (otherlv_0= '[' ( (lv_conditionExpression_1_0= ruleConditionExpression ) ) otherlv_2= ']' ) )
            // InternalSML.g:3051:1: (otherlv_0= '[' ( (lv_conditionExpression_1_0= ruleConditionExpression ) ) otherlv_2= ']' )
            {
            // InternalSML.g:3051:1: (otherlv_0= '[' ( (lv_conditionExpression_1_0= ruleConditionExpression ) ) otherlv_2= ']' )
            // InternalSML.g:3051:3: otherlv_0= '[' ( (lv_conditionExpression_1_0= ruleConditionExpression ) ) otherlv_2= ']'
            {
            otherlv_0=(Token)match(input,32,FollowSets000.FOLLOW_55); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getConditionAccess().getLeftSquareBracketKeyword_0());
                  
            }
            // InternalSML.g:3055:1: ( (lv_conditionExpression_1_0= ruleConditionExpression ) )
            // InternalSML.g:3056:1: (lv_conditionExpression_1_0= ruleConditionExpression )
            {
            // InternalSML.g:3056:1: (lv_conditionExpression_1_0= ruleConditionExpression )
            // InternalSML.g:3057:3: lv_conditionExpression_1_0= ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_24);
            lv_conditionExpression_1_0=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getConditionRule());
              	        }
                     		set(
                     			current, 
                     			"conditionExpression",
                      		lv_conditionExpression_1_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,33,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getConditionAccess().getRightSquareBracketKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCondition"


    // $ANTLR start "entryRuleConditionExpression"
    // InternalSML.g:3085:1: entryRuleConditionExpression returns [EObject current=null] : iv_ruleConditionExpression= ruleConditionExpression EOF ;
    public final EObject entryRuleConditionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConditionExpression = null;


        try {
            // InternalSML.g:3086:2: (iv_ruleConditionExpression= ruleConditionExpression EOF )
            // InternalSML.g:3087:2: iv_ruleConditionExpression= ruleConditionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConditionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConditionExpression=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConditionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConditionExpression"


    // $ANTLR start "ruleConditionExpression"
    // InternalSML.g:3094:1: ruleConditionExpression returns [EObject current=null] : ( (lv_expression_0_0= ruleExpression ) ) ;
    public final EObject ruleConditionExpression() throws RecognitionException {
        EObject current = null;

        EObject lv_expression_0_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3097:28: ( ( (lv_expression_0_0= ruleExpression ) ) )
            // InternalSML.g:3098:1: ( (lv_expression_0_0= ruleExpression ) )
            {
            // InternalSML.g:3098:1: ( (lv_expression_0_0= ruleExpression ) )
            // InternalSML.g:3099:1: (lv_expression_0_0= ruleExpression )
            {
            // InternalSML.g:3099:1: (lv_expression_0_0= ruleExpression )
            // InternalSML.g:3100:3: lv_expression_0_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getConditionExpressionAccess().getExpressionExpressionParserRuleCall_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_expression_0_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getConditionExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConditionExpression"


    // $ANTLR start "entryRuleConstraintBlock"
    // InternalSML.g:3124:1: entryRuleConstraintBlock returns [EObject current=null] : iv_ruleConstraintBlock= ruleConstraintBlock EOF ;
    public final EObject entryRuleConstraintBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstraintBlock = null;


        try {
            // InternalSML.g:3125:2: (iv_ruleConstraintBlock= ruleConstraintBlock EOF )
            // InternalSML.g:3126:2: iv_ruleConstraintBlock= ruleConstraintBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConstraintBlockRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConstraintBlock=ruleConstraintBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConstraintBlock; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstraintBlock"


    // $ANTLR start "ruleConstraintBlock"
    // InternalSML.g:3133:1: ruleConstraintBlock returns [EObject current=null] : ( () otherlv_1= 'constraints' otherlv_2= '[' ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )* otherlv_11= ']' ) ;
    public final EObject ruleConstraintBlock() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        EObject lv_consider_4_0 = null;

        EObject lv_ignore_6_0 = null;

        EObject lv_forbidden_8_0 = null;

        EObject lv_interrupt_10_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3136:28: ( ( () otherlv_1= 'constraints' otherlv_2= '[' ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )* otherlv_11= ']' ) )
            // InternalSML.g:3137:1: ( () otherlv_1= 'constraints' otherlv_2= '[' ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )* otherlv_11= ']' )
            {
            // InternalSML.g:3137:1: ( () otherlv_1= 'constraints' otherlv_2= '[' ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )* otherlv_11= ']' )
            // InternalSML.g:3137:2: () otherlv_1= 'constraints' otherlv_2= '[' ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )* otherlv_11= ']'
            {
            // InternalSML.g:3137:2: ()
            // InternalSML.g:3138:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getConstraintBlockAccess().getConstraintBlockAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,61,FollowSets000.FOLLOW_22); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getConstraintBlockAccess().getConstraintsKeyword_1());
                  
            }
            otherlv_2=(Token)match(input,32,FollowSets000.FOLLOW_59); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getConstraintBlockAccess().getLeftSquareBracketKeyword_2());
                  
            }
            // InternalSML.g:3151:1: ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )*
            loop64:
            do {
                int alt64=5;
                switch ( input.LA(1) ) {
                case 62:
                    {
                    alt64=1;
                    }
                    break;
                case 63:
                    {
                    alt64=2;
                    }
                    break;
                case 64:
                    {
                    alt64=3;
                    }
                    break;
                case 58:
                    {
                    alt64=4;
                    }
                    break;

                }

                switch (alt64) {
            	case 1 :
            	    // InternalSML.g:3151:2: (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) )
            	    {
            	    // InternalSML.g:3151:2: (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) )
            	    // InternalSML.g:3151:4: otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) )
            	    {
            	    otherlv_3=(Token)match(input,62,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getConstraintBlockAccess().getConsiderKeyword_3_0_0());
            	          
            	    }
            	    // InternalSML.g:3155:1: ( (lv_consider_4_0= ruleConstraintMessage ) )
            	    // InternalSML.g:3156:1: (lv_consider_4_0= ruleConstraintMessage )
            	    {
            	    // InternalSML.g:3156:1: (lv_consider_4_0= ruleConstraintMessage )
            	    // InternalSML.g:3157:3: lv_consider_4_0= ruleConstraintMessage
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getConstraintBlockAccess().getConsiderConstraintMessageParserRuleCall_3_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_59);
            	    lv_consider_4_0=ruleConstraintMessage();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getConstraintBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"consider",
            	              		lv_consider_4_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalSML.g:3174:6: (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) )
            	    {
            	    // InternalSML.g:3174:6: (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) )
            	    // InternalSML.g:3174:8: otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) )
            	    {
            	    otherlv_5=(Token)match(input,63,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_5, grammarAccess.getConstraintBlockAccess().getIgnoreKeyword_3_1_0());
            	          
            	    }
            	    // InternalSML.g:3178:1: ( (lv_ignore_6_0= ruleConstraintMessage ) )
            	    // InternalSML.g:3179:1: (lv_ignore_6_0= ruleConstraintMessage )
            	    {
            	    // InternalSML.g:3179:1: (lv_ignore_6_0= ruleConstraintMessage )
            	    // InternalSML.g:3180:3: lv_ignore_6_0= ruleConstraintMessage
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getConstraintBlockAccess().getIgnoreConstraintMessageParserRuleCall_3_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_59);
            	    lv_ignore_6_0=ruleConstraintMessage();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getConstraintBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"ignore",
            	              		lv_ignore_6_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalSML.g:3197:6: (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) )
            	    {
            	    // InternalSML.g:3197:6: (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) )
            	    // InternalSML.g:3197:8: otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) )
            	    {
            	    otherlv_7=(Token)match(input,64,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_7, grammarAccess.getConstraintBlockAccess().getForbiddenKeyword_3_2_0());
            	          
            	    }
            	    // InternalSML.g:3201:1: ( (lv_forbidden_8_0= ruleConstraintMessage ) )
            	    // InternalSML.g:3202:1: (lv_forbidden_8_0= ruleConstraintMessage )
            	    {
            	    // InternalSML.g:3202:1: (lv_forbidden_8_0= ruleConstraintMessage )
            	    // InternalSML.g:3203:3: lv_forbidden_8_0= ruleConstraintMessage
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getConstraintBlockAccess().getForbiddenConstraintMessageParserRuleCall_3_2_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_59);
            	    lv_forbidden_8_0=ruleConstraintMessage();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getConstraintBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"forbidden",
            	              		lv_forbidden_8_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalSML.g:3220:6: (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) )
            	    {
            	    // InternalSML.g:3220:6: (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) )
            	    // InternalSML.g:3220:8: otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) )
            	    {
            	    otherlv_9=(Token)match(input,58,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_9, grammarAccess.getConstraintBlockAccess().getInterruptKeyword_3_3_0());
            	          
            	    }
            	    // InternalSML.g:3224:1: ( (lv_interrupt_10_0= ruleConstraintMessage ) )
            	    // InternalSML.g:3225:1: (lv_interrupt_10_0= ruleConstraintMessage )
            	    {
            	    // InternalSML.g:3225:1: (lv_interrupt_10_0= ruleConstraintMessage )
            	    // InternalSML.g:3226:3: lv_interrupt_10_0= ruleConstraintMessage
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getConstraintBlockAccess().getInterruptConstraintMessageParserRuleCall_3_3_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_59);
            	    lv_interrupt_10_0=ruleConstraintMessage();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getConstraintBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"interrupt",
            	              		lv_interrupt_10_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop64;
                }
            } while (true);

            otherlv_11=(Token)match(input,33,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_11, grammarAccess.getConstraintBlockAccess().getRightSquareBracketKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstraintBlock"


    // $ANTLR start "entryRuleConstraintMessage"
    // InternalSML.g:3254:1: entryRuleConstraintMessage returns [EObject current=null] : iv_ruleConstraintMessage= ruleConstraintMessage EOF ;
    public final EObject entryRuleConstraintMessage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstraintMessage = null;


        try {
            // InternalSML.g:3255:2: (iv_ruleConstraintMessage= ruleConstraintMessage EOF )
            // InternalSML.g:3256:2: iv_ruleConstraintMessage= ruleConstraintMessage EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConstraintMessageRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConstraintMessage=ruleConstraintMessage();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConstraintMessage; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstraintMessage"


    // $ANTLR start "ruleConstraintMessage"
    // InternalSML.g:3263:1: ruleConstraintMessage returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) otherlv_3= '.' ( (otherlv_4= RULE_ID ) ) (otherlv_5= '.' ( (lv_collectionModification_6_0= ruleCollectionModification ) ) )? otherlv_7= '(' ( ( (lv_parameters_8_0= ruleParameterBinding ) ) (otherlv_9= ',' ( (lv_parameters_10_0= ruleParameterBinding ) ) )* )? otherlv_11= ')' ) ;
    public final EObject ruleConstraintMessage() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Enumerator lv_collectionModification_6_0 = null;

        EObject lv_parameters_8_0 = null;

        EObject lv_parameters_10_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3266:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) otherlv_3= '.' ( (otherlv_4= RULE_ID ) ) (otherlv_5= '.' ( (lv_collectionModification_6_0= ruleCollectionModification ) ) )? otherlv_7= '(' ( ( (lv_parameters_8_0= ruleParameterBinding ) ) (otherlv_9= ',' ( (lv_parameters_10_0= ruleParameterBinding ) ) )* )? otherlv_11= ')' ) )
            // InternalSML.g:3267:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) otherlv_3= '.' ( (otherlv_4= RULE_ID ) ) (otherlv_5= '.' ( (lv_collectionModification_6_0= ruleCollectionModification ) ) )? otherlv_7= '(' ( ( (lv_parameters_8_0= ruleParameterBinding ) ) (otherlv_9= ',' ( (lv_parameters_10_0= ruleParameterBinding ) ) )* )? otherlv_11= ')' )
            {
            // InternalSML.g:3267:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) otherlv_3= '.' ( (otherlv_4= RULE_ID ) ) (otherlv_5= '.' ( (lv_collectionModification_6_0= ruleCollectionModification ) ) )? otherlv_7= '(' ( ( (lv_parameters_8_0= ruleParameterBinding ) ) (otherlv_9= ',' ( (lv_parameters_10_0= ruleParameterBinding ) ) )* )? otherlv_11= ')' )
            // InternalSML.g:3267:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) otherlv_3= '.' ( (otherlv_4= RULE_ID ) ) (otherlv_5= '.' ( (lv_collectionModification_6_0= ruleCollectionModification ) ) )? otherlv_7= '(' ( ( (lv_parameters_8_0= ruleParameterBinding ) ) (otherlv_9= ',' ( (lv_parameters_10_0= ruleParameterBinding ) ) )* )? otherlv_11= ')'
            {
            // InternalSML.g:3267:2: ( (otherlv_0= RULE_ID ) )
            // InternalSML.g:3268:1: (otherlv_0= RULE_ID )
            {
            // InternalSML.g:3268:1: (otherlv_0= RULE_ID )
            // InternalSML.g:3269:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getConstraintMessageRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_43); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getConstraintMessageAccess().getSenderRoleCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,48,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getConstraintMessageAccess().getHyphenMinusGreaterThanSignKeyword_1());
                  
            }
            // InternalSML.g:3284:1: ( (otherlv_2= RULE_ID ) )
            // InternalSML.g:3285:1: (otherlv_2= RULE_ID )
            {
            // InternalSML.g:3285:1: (otherlv_2= RULE_ID )
            // InternalSML.g:3286:3: otherlv_2= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getConstraintMessageRule());
              	        }
                      
            }
            otherlv_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_44); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_2, grammarAccess.getConstraintMessageAccess().getReceiverRoleCrossReference_2_0()); 
              	
            }

            }


            }

            otherlv_3=(Token)match(input,35,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getConstraintMessageAccess().getFullStopKeyword_3());
                  
            }
            // InternalSML.g:3301:1: ( (otherlv_4= RULE_ID ) )
            // InternalSML.g:3302:1: (otherlv_4= RULE_ID )
            {
            // InternalSML.g:3302:1: (otherlv_4= RULE_ID )
            // InternalSML.g:3303:3: otherlv_4= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getConstraintMessageRule());
              	        }
                      
            }
            otherlv_4=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_60); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_4, grammarAccess.getConstraintMessageAccess().getModelElementETypedElementCrossReference_4_0()); 
              	
            }

            }


            }

            // InternalSML.g:3314:2: (otherlv_5= '.' ( (lv_collectionModification_6_0= ruleCollectionModification ) ) )?
            int alt65=2;
            int LA65_0 = input.LA(1);

            if ( (LA65_0==35) ) {
                alt65=1;
            }
            switch (alt65) {
                case 1 :
                    // InternalSML.g:3314:4: otherlv_5= '.' ( (lv_collectionModification_6_0= ruleCollectionModification ) )
                    {
                    otherlv_5=(Token)match(input,35,FollowSets000.FOLLOW_46); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_5, grammarAccess.getConstraintMessageAccess().getFullStopKeyword_5_0());
                          
                    }
                    // InternalSML.g:3318:1: ( (lv_collectionModification_6_0= ruleCollectionModification ) )
                    // InternalSML.g:3319:1: (lv_collectionModification_6_0= ruleCollectionModification )
                    {
                    // InternalSML.g:3319:1: (lv_collectionModification_6_0= ruleCollectionModification )
                    // InternalSML.g:3320:3: lv_collectionModification_6_0= ruleCollectionModification
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getConstraintMessageAccess().getCollectionModificationCollectionModificationEnumRuleCall_5_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_19);
                    lv_collectionModification_6_0=ruleCollectionModification();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getConstraintMessageRule());
                      	        }
                             		set(
                             			current, 
                             			"collectionModification",
                              		lv_collectionModification_6_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionModification");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_7=(Token)match(input,29,FollowSets000.FOLLOW_48); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_7, grammarAccess.getConstraintMessageAccess().getLeftParenthesisKeyword_6());
                  
            }
            // InternalSML.g:3340:1: ( ( (lv_parameters_8_0= ruleParameterBinding ) ) (otherlv_9= ',' ( (lv_parameters_10_0= ruleParameterBinding ) ) )* )?
            int alt67=2;
            int LA67_0 = input.LA(1);

            if ( ((LA67_0>=RULE_ID && LA67_0<=RULE_STRING)||LA67_0==RULE_BOOL||LA67_0==29||(LA67_0>=49 && LA67_0<=50)||LA67_0==80||(LA67_0>=82 && LA67_0<=83)) ) {
                alt67=1;
            }
            switch (alt67) {
                case 1 :
                    // InternalSML.g:3340:2: ( (lv_parameters_8_0= ruleParameterBinding ) ) (otherlv_9= ',' ( (lv_parameters_10_0= ruleParameterBinding ) ) )*
                    {
                    // InternalSML.g:3340:2: ( (lv_parameters_8_0= ruleParameterBinding ) )
                    // InternalSML.g:3341:1: (lv_parameters_8_0= ruleParameterBinding )
                    {
                    // InternalSML.g:3341:1: (lv_parameters_8_0= ruleParameterBinding )
                    // InternalSML.g:3342:3: lv_parameters_8_0= ruleParameterBinding
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getConstraintMessageAccess().getParametersParameterBindingParserRuleCall_7_0_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_20);
                    lv_parameters_8_0=ruleParameterBinding();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getConstraintMessageRule());
                      	        }
                             		add(
                             			current, 
                             			"parameters",
                              		lv_parameters_8_0, 
                              		"org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // InternalSML.g:3358:2: (otherlv_9= ',' ( (lv_parameters_10_0= ruleParameterBinding ) ) )*
                    loop66:
                    do {
                        int alt66=2;
                        int LA66_0 = input.LA(1);

                        if ( (LA66_0==22) ) {
                            alt66=1;
                        }


                        switch (alt66) {
                    	case 1 :
                    	    // InternalSML.g:3358:4: otherlv_9= ',' ( (lv_parameters_10_0= ruleParameterBinding ) )
                    	    {
                    	    otherlv_9=(Token)match(input,22,FollowSets000.FOLLOW_49); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_9, grammarAccess.getConstraintMessageAccess().getCommaKeyword_7_1_0());
                    	          
                    	    }
                    	    // InternalSML.g:3362:1: ( (lv_parameters_10_0= ruleParameterBinding ) )
                    	    // InternalSML.g:3363:1: (lv_parameters_10_0= ruleParameterBinding )
                    	    {
                    	    // InternalSML.g:3363:1: (lv_parameters_10_0= ruleParameterBinding )
                    	    // InternalSML.g:3364:3: lv_parameters_10_0= ruleParameterBinding
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getConstraintMessageAccess().getParametersParameterBindingParserRuleCall_7_1_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_20);
                    	    lv_parameters_10_0=ruleParameterBinding();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getConstraintMessageRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"parameters",
                    	              		lv_parameters_10_0, 
                    	              		"org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop66;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_11=(Token)match(input,30,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_11, grammarAccess.getConstraintMessageAccess().getRightParenthesisKeyword_8());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstraintMessage"


    // $ANTLR start "entryRuleImport"
    // InternalSML.g:3394:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalSML.g:3395:2: (iv_ruleImport= ruleImport EOF )
            // InternalSML.g:3396:2: iv_ruleImport= ruleImport EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getImportRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleImport; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalSML.g:3403:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_importURI_1_0=null;

         enterRule(); 
            
        try {
            // InternalSML.g:3406:28: ( (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) )
            // InternalSML.g:3407:1: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            {
            // InternalSML.g:3407:1: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            // InternalSML.g:3407:3: otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,65,FollowSets000.FOLLOW_28); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
                  
            }
            // InternalSML.g:3411:1: ( (lv_importURI_1_0= RULE_STRING ) )
            // InternalSML.g:3412:1: (lv_importURI_1_0= RULE_STRING )
            {
            // InternalSML.g:3412:1: (lv_importURI_1_0= RULE_STRING )
            // InternalSML.g:3413:3: lv_importURI_1_0= RULE_STRING
            {
            lv_importURI_1_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_importURI_1_0, grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getImportRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"importURI",
                      		lv_importURI_1_0, 
                      		"org.eclipse.xtext.common.Terminals.STRING");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleExpressionRegion"
    // InternalSML.g:3437:1: entryRuleExpressionRegion returns [EObject current=null] : iv_ruleExpressionRegion= ruleExpressionRegion EOF ;
    public final EObject entryRuleExpressionRegion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionRegion = null;


        try {
            // InternalSML.g:3438:2: (iv_ruleExpressionRegion= ruleExpressionRegion EOF )
            // InternalSML.g:3439:2: iv_ruleExpressionRegion= ruleExpressionRegion EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionRegionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpressionRegion=ruleExpressionRegion();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionRegion; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionRegion"


    // $ANTLR start "ruleExpressionRegion"
    // InternalSML.g:3446:1: ruleExpressionRegion returns [EObject current=null] : ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' ) ;
    public final EObject ruleExpressionRegion() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject lv_expressions_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3449:28: ( ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' ) )
            // InternalSML.g:3450:1: ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' )
            {
            // InternalSML.g:3450:1: ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' )
            // InternalSML.g:3450:2: () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}'
            {
            // InternalSML.g:3450:2: ()
            // InternalSML.g:3451:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getExpressionRegionAccess().getExpressionRegionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,15,FollowSets000.FOLLOW_61); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getExpressionRegionAccess().getLeftCurlyBracketKeyword_1());
                  
            }
            // InternalSML.g:3460:1: ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )*
            loop68:
            do {
                int alt68=2;
                int LA68_0 = input.LA(1);

                if ( ((LA68_0>=RULE_ID && LA68_0<=RULE_STRING)||LA68_0==RULE_BOOL||LA68_0==15||LA68_0==29||(LA68_0>=67 && LA68_0<=69)||LA68_0==80||(LA68_0>=82 && LA68_0<=83)) ) {
                    alt68=1;
                }


                switch (alt68) {
            	case 1 :
            	    // InternalSML.g:3460:2: ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';'
            	    {
            	    // InternalSML.g:3460:2: ( (lv_expressions_2_0= ruleExpressionOrRegion ) )
            	    // InternalSML.g:3461:1: (lv_expressions_2_0= ruleExpressionOrRegion )
            	    {
            	    // InternalSML.g:3461:1: (lv_expressions_2_0= ruleExpressionOrRegion )
            	    // InternalSML.g:3462:3: lv_expressions_2_0= ruleExpressionOrRegion
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getExpressionRegionAccess().getExpressionsExpressionOrRegionParserRuleCall_2_0_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_62);
            	    lv_expressions_2_0=ruleExpressionOrRegion();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getExpressionRegionRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"expressions",
            	              		lv_expressions_2_0, 
            	              		"org.scenariotools.sml.expressions.ScenarioExpressions.ExpressionOrRegion");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }

            	    otherlv_3=(Token)match(input,66,FollowSets000.FOLLOW_61); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getExpressionRegionAccess().getSemicolonKeyword_2_1());
            	          
            	    }

            	    }
            	    break;

            	default :
            	    break loop68;
                }
            } while (true);

            otherlv_4=(Token)match(input,19,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getExpressionRegionAccess().getRightCurlyBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionRegion"


    // $ANTLR start "entryRuleExpressionOrRegion"
    // InternalSML.g:3494:1: entryRuleExpressionOrRegion returns [EObject current=null] : iv_ruleExpressionOrRegion= ruleExpressionOrRegion EOF ;
    public final EObject entryRuleExpressionOrRegion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionOrRegion = null;


        try {
            // InternalSML.g:3495:2: (iv_ruleExpressionOrRegion= ruleExpressionOrRegion EOF )
            // InternalSML.g:3496:2: iv_ruleExpressionOrRegion= ruleExpressionOrRegion EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionOrRegionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpressionOrRegion=ruleExpressionOrRegion();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionOrRegion; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionOrRegion"


    // $ANTLR start "ruleExpressionOrRegion"
    // InternalSML.g:3503:1: ruleExpressionOrRegion returns [EObject current=null] : (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables ) ;
    public final EObject ruleExpressionOrRegion() throws RecognitionException {
        EObject current = null;

        EObject this_ExpressionRegion_0 = null;

        EObject this_ExpressionAndVariables_1 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3506:28: ( (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables ) )
            // InternalSML.g:3507:1: (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables )
            {
            // InternalSML.g:3507:1: (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables )
            int alt69=2;
            int LA69_0 = input.LA(1);

            if ( (LA69_0==15) ) {
                alt69=1;
            }
            else if ( ((LA69_0>=RULE_ID && LA69_0<=RULE_STRING)||LA69_0==RULE_BOOL||LA69_0==29||(LA69_0>=67 && LA69_0<=69)||LA69_0==80||(LA69_0>=82 && LA69_0<=83)) ) {
                alt69=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 69, 0, input);

                throw nvae;
            }
            switch (alt69) {
                case 1 :
                    // InternalSML.g:3508:5: this_ExpressionRegion_0= ruleExpressionRegion
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionOrRegionAccess().getExpressionRegionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ExpressionRegion_0=ruleExpressionRegion();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ExpressionRegion_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:3518:5: this_ExpressionAndVariables_1= ruleExpressionAndVariables
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionOrRegionAccess().getExpressionAndVariablesParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ExpressionAndVariables_1=ruleExpressionAndVariables();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ExpressionAndVariables_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionOrRegion"


    // $ANTLR start "entryRuleExpressionAndVariables"
    // InternalSML.g:3534:1: entryRuleExpressionAndVariables returns [EObject current=null] : iv_ruleExpressionAndVariables= ruleExpressionAndVariables EOF ;
    public final EObject entryRuleExpressionAndVariables() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionAndVariables = null;


        try {
            // InternalSML.g:3535:2: (iv_ruleExpressionAndVariables= ruleExpressionAndVariables EOF )
            // InternalSML.g:3536:2: iv_ruleExpressionAndVariables= ruleExpressionAndVariables EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionAndVariablesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpressionAndVariables=ruleExpressionAndVariables();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionAndVariables; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionAndVariables"


    // $ANTLR start "ruleExpressionAndVariables"
    // InternalSML.g:3543:1: ruleExpressionAndVariables returns [EObject current=null] : (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression ) ;
    public final EObject ruleExpressionAndVariables() throws RecognitionException {
        EObject current = null;

        EObject this_VariableExpression_0 = null;

        EObject this_Expression_1 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3546:28: ( (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression ) )
            // InternalSML.g:3547:1: (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression )
            {
            // InternalSML.g:3547:1: (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression )
            int alt70=2;
            switch ( input.LA(1) ) {
            case 67:
            case 68:
            case 69:
                {
                alt70=1;
                }
                break;
            case RULE_ID:
                {
                int LA70_2 = input.LA(2);

                if ( (LA70_2==EOF||LA70_2==24||LA70_2==35||LA70_2==49||LA70_2==66||(LA70_2>=70 && LA70_2<=81)) ) {
                    alt70=2;
                }
                else if ( (LA70_2==31) ) {
                    alt70=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 70, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_INT:
            case RULE_SIGNEDINT:
            case RULE_STRING:
            case RULE_BOOL:
            case 29:
            case 80:
            case 82:
            case 83:
                {
                alt70=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 70, 0, input);

                throw nvae;
            }

            switch (alt70) {
                case 1 :
                    // InternalSML.g:3548:5: this_VariableExpression_0= ruleVariableExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionAndVariablesAccess().getVariableExpressionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableExpression_0=ruleVariableExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableExpression_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:3558:5: this_Expression_1= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionAndVariablesAccess().getExpressionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Expression_1=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Expression_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionAndVariables"


    // $ANTLR start "entryRuleVariableExpression"
    // InternalSML.g:3574:1: entryRuleVariableExpression returns [EObject current=null] : iv_ruleVariableExpression= ruleVariableExpression EOF ;
    public final EObject entryRuleVariableExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableExpression = null;


        try {
            // InternalSML.g:3575:2: (iv_ruleVariableExpression= ruleVariableExpression EOF )
            // InternalSML.g:3576:2: iv_ruleVariableExpression= ruleVariableExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableExpression=ruleVariableExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableExpression"


    // $ANTLR start "ruleVariableExpression"
    // InternalSML.g:3583:1: ruleVariableExpression returns [EObject current=null] : (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment | this_ClockDeclaration_2= ruleClockDeclaration | this_ClockAssignment_3= ruleClockAssignment ) ;
    public final EObject ruleVariableExpression() throws RecognitionException {
        EObject current = null;

        EObject this_TypedVariableDeclaration_0 = null;

        EObject this_VariableAssignment_1 = null;

        EObject this_ClockDeclaration_2 = null;

        EObject this_ClockAssignment_3 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3586:28: ( (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment | this_ClockDeclaration_2= ruleClockDeclaration | this_ClockAssignment_3= ruleClockAssignment ) )
            // InternalSML.g:3587:1: (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment | this_ClockDeclaration_2= ruleClockDeclaration | this_ClockAssignment_3= ruleClockAssignment )
            {
            // InternalSML.g:3587:1: (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment | this_ClockDeclaration_2= ruleClockDeclaration | this_ClockAssignment_3= ruleClockAssignment )
            int alt71=4;
            switch ( input.LA(1) ) {
            case 67:
                {
                alt71=1;
                }
                break;
            case RULE_ID:
                {
                alt71=2;
                }
                break;
            case 68:
                {
                alt71=3;
                }
                break;
            case 69:
                {
                alt71=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 71, 0, input);

                throw nvae;
            }

            switch (alt71) {
                case 1 :
                    // InternalSML.g:3588:5: this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getVariableExpressionAccess().getTypedVariableDeclarationParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_TypedVariableDeclaration_0=ruleTypedVariableDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TypedVariableDeclaration_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:3598:5: this_VariableAssignment_1= ruleVariableAssignment
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getVariableExpressionAccess().getVariableAssignmentParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableAssignment_1=ruleVariableAssignment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableAssignment_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalSML.g:3608:5: this_ClockDeclaration_2= ruleClockDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getVariableExpressionAccess().getClockDeclarationParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ClockDeclaration_2=ruleClockDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ClockDeclaration_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // InternalSML.g:3618:5: this_ClockAssignment_3= ruleClockAssignment
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getVariableExpressionAccess().getClockAssignmentParserRuleCall_3()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ClockAssignment_3=ruleClockAssignment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ClockAssignment_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableExpression"


    // $ANTLR start "entryRuleVariableAssignment"
    // InternalSML.g:3636:1: entryRuleVariableAssignment returns [EObject current=null] : iv_ruleVariableAssignment= ruleVariableAssignment EOF ;
    public final EObject entryRuleVariableAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableAssignment = null;


        try {
            // InternalSML.g:3637:2: (iv_ruleVariableAssignment= ruleVariableAssignment EOF )
            // InternalSML.g:3638:2: iv_ruleVariableAssignment= ruleVariableAssignment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableAssignmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableAssignment=ruleVariableAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableAssignment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableAssignment"


    // $ANTLR start "ruleVariableAssignment"
    // InternalSML.g:3645:1: ruleVariableAssignment returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) ) ;
    public final EObject ruleVariableAssignment() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_expression_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3648:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) ) )
            // InternalSML.g:3649:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) )
            {
            // InternalSML.g:3649:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) )
            // InternalSML.g:3649:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) )
            {
            // InternalSML.g:3649:2: ( (otherlv_0= RULE_ID ) )
            // InternalSML.g:3650:1: (otherlv_0= RULE_ID )
            {
            // InternalSML.g:3650:1: (otherlv_0= RULE_ID )
            // InternalSML.g:3651:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getVariableAssignmentRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_21); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,31,FollowSets000.FOLLOW_55); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getVariableAssignmentAccess().getEqualsSignKeyword_1());
                  
            }
            // InternalSML.g:3666:1: ( (lv_expression_2_0= ruleExpression ) )
            // InternalSML.g:3667:1: (lv_expression_2_0= ruleExpression )
            {
            // InternalSML.g:3667:1: (lv_expression_2_0= ruleExpression )
            // InternalSML.g:3668:3: lv_expression_2_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getVariableAssignmentAccess().getExpressionExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_expression_2_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getVariableAssignmentRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_2_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableAssignment"


    // $ANTLR start "entryRuleTypedVariableDeclaration"
    // InternalSML.g:3692:1: entryRuleTypedVariableDeclaration returns [EObject current=null] : iv_ruleTypedVariableDeclaration= ruleTypedVariableDeclaration EOF ;
    public final EObject entryRuleTypedVariableDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypedVariableDeclaration = null;


        try {
            // InternalSML.g:3693:2: (iv_ruleTypedVariableDeclaration= ruleTypedVariableDeclaration EOF )
            // InternalSML.g:3694:2: iv_ruleTypedVariableDeclaration= ruleTypedVariableDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypedVariableDeclarationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleTypedVariableDeclaration=ruleTypedVariableDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTypedVariableDeclaration; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypedVariableDeclaration"


    // $ANTLR start "ruleTypedVariableDeclaration"
    // InternalSML.g:3701:1: ruleTypedVariableDeclaration returns [EObject current=null] : (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? ) ;
    public final EObject ruleTypedVariableDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        EObject lv_expression_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3704:28: ( (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? ) )
            // InternalSML.g:3705:1: (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? )
            {
            // InternalSML.g:3705:1: (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? )
            // InternalSML.g:3705:3: otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )?
            {
            otherlv_0=(Token)match(input,67,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getTypedVariableDeclarationAccess().getVarKeyword_0());
                  
            }
            // InternalSML.g:3709:1: ( (otherlv_1= RULE_ID ) )
            // InternalSML.g:3710:1: (otherlv_1= RULE_ID )
            {
            // InternalSML.g:3710:1: (otherlv_1= RULE_ID )
            // InternalSML.g:3711:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getTypedVariableDeclarationRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierCrossReference_1_0()); 
              	
            }

            }


            }

            // InternalSML.g:3722:2: ( (lv_name_2_0= RULE_ID ) )
            // InternalSML.g:3723:1: (lv_name_2_0= RULE_ID )
            {
            // InternalSML.g:3723:1: (lv_name_2_0= RULE_ID )
            // InternalSML.g:3724:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_63); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getTypedVariableDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getTypedVariableDeclarationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalSML.g:3740:2: (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )?
            int alt72=2;
            int LA72_0 = input.LA(1);

            if ( (LA72_0==31) ) {
                alt72=1;
            }
            switch (alt72) {
                case 1 :
                    // InternalSML.g:3740:4: otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) )
                    {
                    otherlv_3=(Token)match(input,31,FollowSets000.FOLLOW_55); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getTypedVariableDeclarationAccess().getEqualsSignKeyword_3_0());
                          
                    }
                    // InternalSML.g:3744:1: ( (lv_expression_4_0= ruleExpression ) )
                    // InternalSML.g:3745:1: (lv_expression_4_0= ruleExpression )
                    {
                    // InternalSML.g:3745:1: (lv_expression_4_0= ruleExpression )
                    // InternalSML.g:3746:3: lv_expression_4_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getTypedVariableDeclarationAccess().getExpressionExpressionParserRuleCall_3_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_4_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getTypedVariableDeclarationRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_4_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypedVariableDeclaration"


    // $ANTLR start "entryRuleClockDeclaration"
    // InternalSML.g:3770:1: entryRuleClockDeclaration returns [EObject current=null] : iv_ruleClockDeclaration= ruleClockDeclaration EOF ;
    public final EObject entryRuleClockDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClockDeclaration = null;


        try {
            // InternalSML.g:3771:2: (iv_ruleClockDeclaration= ruleClockDeclaration EOF )
            // InternalSML.g:3772:2: iv_ruleClockDeclaration= ruleClockDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClockDeclarationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleClockDeclaration=ruleClockDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClockDeclaration; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClockDeclaration"


    // $ANTLR start "ruleClockDeclaration"
    // InternalSML.g:3779:1: ruleClockDeclaration returns [EObject current=null] : ( () otherlv_1= 'clock' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) ) )? ) ;
    public final EObject ruleClockDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        EObject lv_expression_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3782:28: ( ( () otherlv_1= 'clock' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) ) )? ) )
            // InternalSML.g:3783:1: ( () otherlv_1= 'clock' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) ) )? )
            {
            // InternalSML.g:3783:1: ( () otherlv_1= 'clock' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) ) )? )
            // InternalSML.g:3783:2: () otherlv_1= 'clock' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) ) )?
            {
            // InternalSML.g:3783:2: ()
            // InternalSML.g:3784:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getClockDeclarationAccess().getClockDeclarationAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,68,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getClockDeclarationAccess().getClockKeyword_1());
                  
            }
            // InternalSML.g:3793:1: ( (lv_name_2_0= RULE_ID ) )
            // InternalSML.g:3794:1: (lv_name_2_0= RULE_ID )
            {
            // InternalSML.g:3794:1: (lv_name_2_0= RULE_ID )
            // InternalSML.g:3795:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_63); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getClockDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getClockDeclarationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalSML.g:3811:2: (otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) ) )?
            int alt73=2;
            int LA73_0 = input.LA(1);

            if ( (LA73_0==31) ) {
                alt73=1;
            }
            switch (alt73) {
                case 1 :
                    // InternalSML.g:3811:4: otherlv_3= '=' ( (lv_expression_4_0= ruleIntegerValue ) )
                    {
                    otherlv_3=(Token)match(input,31,FollowSets000.FOLLOW_26); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getClockDeclarationAccess().getEqualsSignKeyword_3_0());
                          
                    }
                    // InternalSML.g:3815:1: ( (lv_expression_4_0= ruleIntegerValue ) )
                    // InternalSML.g:3816:1: (lv_expression_4_0= ruleIntegerValue )
                    {
                    // InternalSML.g:3816:1: (lv_expression_4_0= ruleIntegerValue )
                    // InternalSML.g:3817:3: lv_expression_4_0= ruleIntegerValue
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getClockDeclarationAccess().getExpressionIntegerValueParserRuleCall_3_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_4_0=ruleIntegerValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getClockDeclarationRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_4_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.IntegerValue");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClockDeclaration"


    // $ANTLR start "entryRuleClockAssignment"
    // InternalSML.g:3843:1: entryRuleClockAssignment returns [EObject current=null] : iv_ruleClockAssignment= ruleClockAssignment EOF ;
    public final EObject entryRuleClockAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClockAssignment = null;


        try {
            // InternalSML.g:3844:2: (iv_ruleClockAssignment= ruleClockAssignment EOF )
            // InternalSML.g:3845:2: iv_ruleClockAssignment= ruleClockAssignment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClockAssignmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleClockAssignment=ruleClockAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClockAssignment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClockAssignment"


    // $ANTLR start "ruleClockAssignment"
    // InternalSML.g:3852:1: ruleClockAssignment returns [EObject current=null] : (otherlv_0= 'reset clock' ( (otherlv_1= RULE_ID ) ) (otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) ) )? ) ;
    public final EObject ruleClockAssignment() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_expression_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3855:28: ( (otherlv_0= 'reset clock' ( (otherlv_1= RULE_ID ) ) (otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) ) )? ) )
            // InternalSML.g:3856:1: (otherlv_0= 'reset clock' ( (otherlv_1= RULE_ID ) ) (otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) ) )? )
            {
            // InternalSML.g:3856:1: (otherlv_0= 'reset clock' ( (otherlv_1= RULE_ID ) ) (otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) ) )? )
            // InternalSML.g:3856:3: otherlv_0= 'reset clock' ( (otherlv_1= RULE_ID ) ) (otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) ) )?
            {
            otherlv_0=(Token)match(input,69,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getClockAssignmentAccess().getResetClockKeyword_0());
                  
            }
            // InternalSML.g:3860:1: ( (otherlv_1= RULE_ID ) )
            // InternalSML.g:3861:1: (otherlv_1= RULE_ID )
            {
            // InternalSML.g:3861:1: (otherlv_1= RULE_ID )
            // InternalSML.g:3862:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getClockAssignmentRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_63); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getClockAssignmentAccess().getVariableClockDeclarationCrossReference_1_0()); 
              	
            }

            }


            }

            // InternalSML.g:3873:2: (otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) ) )?
            int alt74=2;
            int LA74_0 = input.LA(1);

            if ( (LA74_0==31) ) {
                alt74=1;
            }
            switch (alt74) {
                case 1 :
                    // InternalSML.g:3873:4: otherlv_2= '=' ( (lv_expression_3_0= ruleIntegerValue ) )
                    {
                    otherlv_2=(Token)match(input,31,FollowSets000.FOLLOW_26); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getClockAssignmentAccess().getEqualsSignKeyword_2_0());
                          
                    }
                    // InternalSML.g:3877:1: ( (lv_expression_3_0= ruleIntegerValue ) )
                    // InternalSML.g:3878:1: (lv_expression_3_0= ruleIntegerValue )
                    {
                    // InternalSML.g:3878:1: (lv_expression_3_0= ruleIntegerValue )
                    // InternalSML.g:3879:3: lv_expression_3_0= ruleIntegerValue
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getClockAssignmentAccess().getExpressionIntegerValueParserRuleCall_2_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_3_0=ruleIntegerValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getClockAssignmentRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.IntegerValue");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClockAssignment"


    // $ANTLR start "entryRuleExpression"
    // InternalSML.g:3903:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // InternalSML.g:3904:2: (iv_ruleExpression= ruleExpression EOF )
            // InternalSML.g:3905:2: iv_ruleExpression= ruleExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpression=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalSML.g:3912:1: ruleExpression returns [EObject current=null] : this_ImplicationExpression_0= ruleImplicationExpression ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_ImplicationExpression_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3915:28: (this_ImplicationExpression_0= ruleImplicationExpression )
            // InternalSML.g:3917:5: this_ImplicationExpression_0= ruleImplicationExpression
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getExpressionAccess().getImplicationExpressionParserRuleCall()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_2);
            this_ImplicationExpression_0=ruleImplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_ImplicationExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleImplicationExpression"
    // InternalSML.g:3933:1: entryRuleImplicationExpression returns [EObject current=null] : iv_ruleImplicationExpression= ruleImplicationExpression EOF ;
    public final EObject entryRuleImplicationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImplicationExpression = null;


        try {
            // InternalSML.g:3934:2: (iv_ruleImplicationExpression= ruleImplicationExpression EOF )
            // InternalSML.g:3935:2: iv_ruleImplicationExpression= ruleImplicationExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getImplicationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleImplicationExpression=ruleImplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleImplicationExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImplicationExpression"


    // $ANTLR start "ruleImplicationExpression"
    // InternalSML.g:3942:1: ruleImplicationExpression returns [EObject current=null] : (this_DisjunctionExpression_0= ruleDisjunctionExpression ( () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) ) )? ) ;
    public final EObject ruleImplicationExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_0=null;
        EObject this_DisjunctionExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3945:28: ( (this_DisjunctionExpression_0= ruleDisjunctionExpression ( () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) ) )? ) )
            // InternalSML.g:3946:1: (this_DisjunctionExpression_0= ruleDisjunctionExpression ( () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) ) )? )
            {
            // InternalSML.g:3946:1: (this_DisjunctionExpression_0= ruleDisjunctionExpression ( () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) ) )? )
            // InternalSML.g:3947:5: this_DisjunctionExpression_0= ruleDisjunctionExpression ( () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getImplicationExpressionAccess().getDisjunctionExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_64);
            this_DisjunctionExpression_0=ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_DisjunctionExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalSML.g:3955:1: ( () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) ) )?
            int alt75=2;
            int LA75_0 = input.LA(1);

            if ( (LA75_0==70) ) {
                alt75=1;
            }
            switch (alt75) {
                case 1 :
                    // InternalSML.g:3955:2: () ( (lv_operator_2_0= '=>' ) ) ( (lv_right_3_0= ruleImplicationExpression ) )
                    {
                    // InternalSML.g:3955:2: ()
                    // InternalSML.g:3956:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getImplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalSML.g:3961:2: ( (lv_operator_2_0= '=>' ) )
                    // InternalSML.g:3962:1: (lv_operator_2_0= '=>' )
                    {
                    // InternalSML.g:3962:1: (lv_operator_2_0= '=>' )
                    // InternalSML.g:3963:3: lv_operator_2_0= '=>'
                    {
                    lv_operator_2_0=(Token)match(input,70,FollowSets000.FOLLOW_55); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_2_0, grammarAccess.getImplicationExpressionAccess().getOperatorEqualsSignGreaterThanSignKeyword_1_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getImplicationExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_2_0, "=>");
                      	    
                    }

                    }


                    }

                    // InternalSML.g:3976:2: ( (lv_right_3_0= ruleImplicationExpression ) )
                    // InternalSML.g:3977:1: (lv_right_3_0= ruleImplicationExpression )
                    {
                    // InternalSML.g:3977:1: (lv_right_3_0= ruleImplicationExpression )
                    // InternalSML.g:3978:3: lv_right_3_0= ruleImplicationExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getImplicationExpressionAccess().getRightImplicationExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleImplicationExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getImplicationExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.ImplicationExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImplicationExpression"


    // $ANTLR start "entryRuleDisjunctionExpression"
    // InternalSML.g:4002:1: entryRuleDisjunctionExpression returns [EObject current=null] : iv_ruleDisjunctionExpression= ruleDisjunctionExpression EOF ;
    public final EObject entryRuleDisjunctionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDisjunctionExpression = null;


        try {
            // InternalSML.g:4003:2: (iv_ruleDisjunctionExpression= ruleDisjunctionExpression EOF )
            // InternalSML.g:4004:2: iv_ruleDisjunctionExpression= ruleDisjunctionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDisjunctionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleDisjunctionExpression=ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDisjunctionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDisjunctionExpression"


    // $ANTLR start "ruleDisjunctionExpression"
    // InternalSML.g:4011:1: ruleDisjunctionExpression returns [EObject current=null] : (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? ) ;
    public final EObject ruleDisjunctionExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_0=null;
        EObject this_ConjunctionExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:4014:28: ( (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? ) )
            // InternalSML.g:4015:1: (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? )
            {
            // InternalSML.g:4015:1: (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? )
            // InternalSML.g:4016:5: this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getDisjunctionExpressionAccess().getConjunctionExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_65);
            this_ConjunctionExpression_0=ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_ConjunctionExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalSML.g:4024:1: ( () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )?
            int alt76=2;
            int LA76_0 = input.LA(1);

            if ( (LA76_0==71) ) {
                alt76=1;
            }
            switch (alt76) {
                case 1 :
                    // InternalSML.g:4024:2: () ( (lv_operator_2_0= '||' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) )
                    {
                    // InternalSML.g:4024:2: ()
                    // InternalSML.g:4025:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getDisjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalSML.g:4030:2: ( (lv_operator_2_0= '||' ) )
                    // InternalSML.g:4031:1: (lv_operator_2_0= '||' )
                    {
                    // InternalSML.g:4031:1: (lv_operator_2_0= '||' )
                    // InternalSML.g:4032:3: lv_operator_2_0= '||'
                    {
                    lv_operator_2_0=(Token)match(input,71,FollowSets000.FOLLOW_55); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_2_0, grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineVerticalLineKeyword_1_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getDisjunctionExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_2_0, "||");
                      	    
                    }

                    }


                    }

                    // InternalSML.g:4045:2: ( (lv_right_3_0= ruleDisjunctionExpression ) )
                    // InternalSML.g:4046:1: (lv_right_3_0= ruleDisjunctionExpression )
                    {
                    // InternalSML.g:4046:1: (lv_right_3_0= ruleDisjunctionExpression )
                    // InternalSML.g:4047:3: lv_right_3_0= ruleDisjunctionExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getDisjunctionExpressionAccess().getRightDisjunctionExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleDisjunctionExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getDisjunctionExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.DisjunctionExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDisjunctionExpression"


    // $ANTLR start "entryRuleConjunctionExpression"
    // InternalSML.g:4071:1: entryRuleConjunctionExpression returns [EObject current=null] : iv_ruleConjunctionExpression= ruleConjunctionExpression EOF ;
    public final EObject entryRuleConjunctionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConjunctionExpression = null;


        try {
            // InternalSML.g:4072:2: (iv_ruleConjunctionExpression= ruleConjunctionExpression EOF )
            // InternalSML.g:4073:2: iv_ruleConjunctionExpression= ruleConjunctionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConjunctionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConjunctionExpression=ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConjunctionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConjunctionExpression"


    // $ANTLR start "ruleConjunctionExpression"
    // InternalSML.g:4080:1: ruleConjunctionExpression returns [EObject current=null] : (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? ) ;
    public final EObject ruleConjunctionExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_0=null;
        EObject this_RelationExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:4083:28: ( (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? ) )
            // InternalSML.g:4084:1: (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? )
            {
            // InternalSML.g:4084:1: (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? )
            // InternalSML.g:4085:5: this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getConjunctionExpressionAccess().getRelationExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_66);
            this_RelationExpression_0=ruleRelationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_RelationExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalSML.g:4093:1: ( () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )?
            int alt77=2;
            int LA77_0 = input.LA(1);

            if ( (LA77_0==72) ) {
                alt77=1;
            }
            switch (alt77) {
                case 1 :
                    // InternalSML.g:4093:2: () ( (lv_operator_2_0= '&&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) )
                    {
                    // InternalSML.g:4093:2: ()
                    // InternalSML.g:4094:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getConjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalSML.g:4099:2: ( (lv_operator_2_0= '&&' ) )
                    // InternalSML.g:4100:1: (lv_operator_2_0= '&&' )
                    {
                    // InternalSML.g:4100:1: (lv_operator_2_0= '&&' )
                    // InternalSML.g:4101:3: lv_operator_2_0= '&&'
                    {
                    lv_operator_2_0=(Token)match(input,72,FollowSets000.FOLLOW_55); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_2_0, grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandAmpersandKeyword_1_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getConjunctionExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_2_0, "&&");
                      	    
                    }

                    }


                    }

                    // InternalSML.g:4114:2: ( (lv_right_3_0= ruleConjunctionExpression ) )
                    // InternalSML.g:4115:1: (lv_right_3_0= ruleConjunctionExpression )
                    {
                    // InternalSML.g:4115:1: (lv_right_3_0= ruleConjunctionExpression )
                    // InternalSML.g:4116:3: lv_right_3_0= ruleConjunctionExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getConjunctionExpressionAccess().getRightConjunctionExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleConjunctionExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getConjunctionExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.ConjunctionExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConjunctionExpression"


    // $ANTLR start "entryRuleRelationExpression"
    // InternalSML.g:4140:1: entryRuleRelationExpression returns [EObject current=null] : iv_ruleRelationExpression= ruleRelationExpression EOF ;
    public final EObject entryRuleRelationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRelationExpression = null;


        try {
            // InternalSML.g:4141:2: (iv_ruleRelationExpression= ruleRelationExpression EOF )
            // InternalSML.g:4142:2: iv_ruleRelationExpression= ruleRelationExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRelationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRelationExpression=ruleRelationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRelationExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelationExpression"


    // $ANTLR start "ruleRelationExpression"
    // InternalSML.g:4149:1: ruleRelationExpression returns [EObject current=null] : (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? ) ;
    public final EObject ruleRelationExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        Token lv_operator_2_3=null;
        Token lv_operator_2_4=null;
        Token lv_operator_2_5=null;
        Token lv_operator_2_6=null;
        EObject this_AdditionExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:4152:28: ( (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? ) )
            // InternalSML.g:4153:1: (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? )
            {
            // InternalSML.g:4153:1: (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? )
            // InternalSML.g:4154:5: this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getRelationExpressionAccess().getAdditionExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_67);
            this_AdditionExpression_0=ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_AdditionExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalSML.g:4162:1: ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )?
            int alt79=2;
            int LA79_0 = input.LA(1);

            if ( ((LA79_0>=73 && LA79_0<=78)) ) {
                alt79=1;
            }
            switch (alt79) {
                case 1 :
                    // InternalSML.g:4162:2: () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) )
                    {
                    // InternalSML.g:4162:2: ()
                    // InternalSML.g:4163:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getRelationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalSML.g:4168:2: ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) )
                    // InternalSML.g:4169:1: ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) )
                    {
                    // InternalSML.g:4169:1: ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) )
                    // InternalSML.g:4170:1: (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' )
                    {
                    // InternalSML.g:4170:1: (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' )
                    int alt78=6;
                    switch ( input.LA(1) ) {
                    case 73:
                        {
                        alt78=1;
                        }
                        break;
                    case 74:
                        {
                        alt78=2;
                        }
                        break;
                    case 75:
                        {
                        alt78=3;
                        }
                        break;
                    case 76:
                        {
                        alt78=4;
                        }
                        break;
                    case 77:
                        {
                        alt78=5;
                        }
                        break;
                    case 78:
                        {
                        alt78=6;
                        }
                        break;
                    default:
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 78, 0, input);

                        throw nvae;
                    }

                    switch (alt78) {
                        case 1 :
                            // InternalSML.g:4171:3: lv_operator_2_1= '=='
                            {
                            lv_operator_2_1=(Token)match(input,73,FollowSets000.FOLLOW_55); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_1, grammarAccess.getRelationExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalSML.g:4183:8: lv_operator_2_2= '!='
                            {
                            lv_operator_2_2=(Token)match(input,74,FollowSets000.FOLLOW_55); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_2, grammarAccess.getRelationExpressionAccess().getOperatorExclamationMarkEqualsSignKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
                              	    
                            }

                            }
                            break;
                        case 3 :
                            // InternalSML.g:4195:8: lv_operator_2_3= '<'
                            {
                            lv_operator_2_3=(Token)match(input,75,FollowSets000.FOLLOW_55); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_3, grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignKeyword_1_1_0_2());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_3, null);
                              	    
                            }

                            }
                            break;
                        case 4 :
                            // InternalSML.g:4207:8: lv_operator_2_4= '>'
                            {
                            lv_operator_2_4=(Token)match(input,76,FollowSets000.FOLLOW_55); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_4, grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignKeyword_1_1_0_3());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_4, null);
                              	    
                            }

                            }
                            break;
                        case 5 :
                            // InternalSML.g:4219:8: lv_operator_2_5= '<='
                            {
                            lv_operator_2_5=(Token)match(input,77,FollowSets000.FOLLOW_55); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_5, grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_1_0_4());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_5, null);
                              	    
                            }

                            }
                            break;
                        case 6 :
                            // InternalSML.g:4231:8: lv_operator_2_6= '>='
                            {
                            lv_operator_2_6=(Token)match(input,78,FollowSets000.FOLLOW_55); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_6, grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_1_0_5());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_6, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalSML.g:4246:2: ( (lv_right_3_0= ruleRelationExpression ) )
                    // InternalSML.g:4247:1: (lv_right_3_0= ruleRelationExpression )
                    {
                    // InternalSML.g:4247:1: (lv_right_3_0= ruleRelationExpression )
                    // InternalSML.g:4248:3: lv_right_3_0= ruleRelationExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getRelationExpressionAccess().getRightRelationExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleRelationExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getRelationExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.RelationExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelationExpression"


    // $ANTLR start "entryRuleTimedExpression"
    // InternalSML.g:4272:1: entryRuleTimedExpression returns [EObject current=null] : iv_ruleTimedExpression= ruleTimedExpression EOF ;
    public final EObject entryRuleTimedExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimedExpression = null;


        try {
            // InternalSML.g:4273:2: (iv_ruleTimedExpression= ruleTimedExpression EOF )
            // InternalSML.g:4274:2: iv_ruleTimedExpression= ruleTimedExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimedExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleTimedExpression=ruleTimedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTimedExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimedExpression"


    // $ANTLR start "ruleTimedExpression"
    // InternalSML.g:4281:1: ruleTimedExpression returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) ( ( (lv_operator_1_1= '==' | lv_operator_1_2= '<' | lv_operator_1_3= '>' | lv_operator_1_4= '<=' | lv_operator_1_5= '>=' ) ) ) ( (lv_value_2_0= RULE_INT ) ) ) ;
    public final EObject ruleTimedExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_operator_1_1=null;
        Token lv_operator_1_2=null;
        Token lv_operator_1_3=null;
        Token lv_operator_1_4=null;
        Token lv_operator_1_5=null;
        Token lv_value_2_0=null;

         enterRule(); 
            
        try {
            // InternalSML.g:4284:28: ( ( ( (otherlv_0= RULE_ID ) ) ( ( (lv_operator_1_1= '==' | lv_operator_1_2= '<' | lv_operator_1_3= '>' | lv_operator_1_4= '<=' | lv_operator_1_5= '>=' ) ) ) ( (lv_value_2_0= RULE_INT ) ) ) )
            // InternalSML.g:4285:1: ( ( (otherlv_0= RULE_ID ) ) ( ( (lv_operator_1_1= '==' | lv_operator_1_2= '<' | lv_operator_1_3= '>' | lv_operator_1_4= '<=' | lv_operator_1_5= '>=' ) ) ) ( (lv_value_2_0= RULE_INT ) ) )
            {
            // InternalSML.g:4285:1: ( ( (otherlv_0= RULE_ID ) ) ( ( (lv_operator_1_1= '==' | lv_operator_1_2= '<' | lv_operator_1_3= '>' | lv_operator_1_4= '<=' | lv_operator_1_5= '>=' ) ) ) ( (lv_value_2_0= RULE_INT ) ) )
            // InternalSML.g:4285:2: ( (otherlv_0= RULE_ID ) ) ( ( (lv_operator_1_1= '==' | lv_operator_1_2= '<' | lv_operator_1_3= '>' | lv_operator_1_4= '<=' | lv_operator_1_5= '>=' ) ) ) ( (lv_value_2_0= RULE_INT ) )
            {
            // InternalSML.g:4285:2: ( (otherlv_0= RULE_ID ) )
            // InternalSML.g:4286:1: (otherlv_0= RULE_ID )
            {
            // InternalSML.g:4286:1: (otherlv_0= RULE_ID )
            // InternalSML.g:4287:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getTimedExpressionRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_68); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getTimedExpressionAccess().getClockClockDeclarationCrossReference_0_0()); 
              	
            }

            }


            }

            // InternalSML.g:4298:2: ( ( (lv_operator_1_1= '==' | lv_operator_1_2= '<' | lv_operator_1_3= '>' | lv_operator_1_4= '<=' | lv_operator_1_5= '>=' ) ) )
            // InternalSML.g:4299:1: ( (lv_operator_1_1= '==' | lv_operator_1_2= '<' | lv_operator_1_3= '>' | lv_operator_1_4= '<=' | lv_operator_1_5= '>=' ) )
            {
            // InternalSML.g:4299:1: ( (lv_operator_1_1= '==' | lv_operator_1_2= '<' | lv_operator_1_3= '>' | lv_operator_1_4= '<=' | lv_operator_1_5= '>=' ) )
            // InternalSML.g:4300:1: (lv_operator_1_1= '==' | lv_operator_1_2= '<' | lv_operator_1_3= '>' | lv_operator_1_4= '<=' | lv_operator_1_5= '>=' )
            {
            // InternalSML.g:4300:1: (lv_operator_1_1= '==' | lv_operator_1_2= '<' | lv_operator_1_3= '>' | lv_operator_1_4= '<=' | lv_operator_1_5= '>=' )
            int alt80=5;
            switch ( input.LA(1) ) {
            case 73:
                {
                alt80=1;
                }
                break;
            case 75:
                {
                alt80=2;
                }
                break;
            case 76:
                {
                alt80=3;
                }
                break;
            case 77:
                {
                alt80=4;
                }
                break;
            case 78:
                {
                alt80=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 80, 0, input);

                throw nvae;
            }

            switch (alt80) {
                case 1 :
                    // InternalSML.g:4301:3: lv_operator_1_1= '=='
                    {
                    lv_operator_1_1=(Token)match(input,73,FollowSets000.FOLLOW_69); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_1_1, grammarAccess.getTimedExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getTimedExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_1_1, null);
                      	    
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:4313:8: lv_operator_1_2= '<'
                    {
                    lv_operator_1_2=(Token)match(input,75,FollowSets000.FOLLOW_69); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_1_2, grammarAccess.getTimedExpressionAccess().getOperatorLessThanSignKeyword_1_0_1());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getTimedExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_1_2, null);
                      	    
                    }

                    }
                    break;
                case 3 :
                    // InternalSML.g:4325:8: lv_operator_1_3= '>'
                    {
                    lv_operator_1_3=(Token)match(input,76,FollowSets000.FOLLOW_69); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_1_3, grammarAccess.getTimedExpressionAccess().getOperatorGreaterThanSignKeyword_1_0_2());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getTimedExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_1_3, null);
                      	    
                    }

                    }
                    break;
                case 4 :
                    // InternalSML.g:4337:8: lv_operator_1_4= '<='
                    {
                    lv_operator_1_4=(Token)match(input,77,FollowSets000.FOLLOW_69); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_1_4, grammarAccess.getTimedExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_0_3());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getTimedExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_1_4, null);
                      	    
                    }

                    }
                    break;
                case 5 :
                    // InternalSML.g:4349:8: lv_operator_1_5= '>='
                    {
                    lv_operator_1_5=(Token)match(input,78,FollowSets000.FOLLOW_69); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_1_5, grammarAccess.getTimedExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_0_4());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getTimedExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_1_5, null);
                      	    
                    }

                    }
                    break;

            }


            }


            }

            // InternalSML.g:4364:2: ( (lv_value_2_0= RULE_INT ) )
            // InternalSML.g:4365:1: (lv_value_2_0= RULE_INT )
            {
            // InternalSML.g:4365:1: (lv_value_2_0= RULE_INT )
            // InternalSML.g:4366:3: lv_value_2_0= RULE_INT
            {
            lv_value_2_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_2_0, grammarAccess.getTimedExpressionAccess().getValueINTTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getTimedExpressionRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_2_0, 
                      		"org.eclipse.xtext.common.Terminals.INT");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimedExpression"


    // $ANTLR start "entryRuleAdditionExpression"
    // InternalSML.g:4390:1: entryRuleAdditionExpression returns [EObject current=null] : iv_ruleAdditionExpression= ruleAdditionExpression EOF ;
    public final EObject entryRuleAdditionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAdditionExpression = null;


        try {
            // InternalSML.g:4391:2: (iv_ruleAdditionExpression= ruleAdditionExpression EOF )
            // InternalSML.g:4392:2: iv_ruleAdditionExpression= ruleAdditionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAdditionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAdditionExpression=ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAdditionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAdditionExpression"


    // $ANTLR start "ruleAdditionExpression"
    // InternalSML.g:4399:1: ruleAdditionExpression returns [EObject current=null] : (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? ) ;
    public final EObject ruleAdditionExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        EObject this_MultiplicationExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:4402:28: ( (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? ) )
            // InternalSML.g:4403:1: (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? )
            {
            // InternalSML.g:4403:1: (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? )
            // InternalSML.g:4404:5: this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getAdditionExpressionAccess().getMultiplicationExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_70);
            this_MultiplicationExpression_0=ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_MultiplicationExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalSML.g:4412:1: ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )?
            int alt82=2;
            int LA82_0 = input.LA(1);

            if ( ((LA82_0>=79 && LA82_0<=80)) ) {
                alt82=1;
            }
            switch (alt82) {
                case 1 :
                    // InternalSML.g:4412:2: () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) )
                    {
                    // InternalSML.g:4412:2: ()
                    // InternalSML.g:4413:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getAdditionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalSML.g:4418:2: ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) )
                    // InternalSML.g:4419:1: ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) )
                    {
                    // InternalSML.g:4419:1: ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) )
                    // InternalSML.g:4420:1: (lv_operator_2_1= '+' | lv_operator_2_2= '-' )
                    {
                    // InternalSML.g:4420:1: (lv_operator_2_1= '+' | lv_operator_2_2= '-' )
                    int alt81=2;
                    int LA81_0 = input.LA(1);

                    if ( (LA81_0==79) ) {
                        alt81=1;
                    }
                    else if ( (LA81_0==80) ) {
                        alt81=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 81, 0, input);

                        throw nvae;
                    }
                    switch (alt81) {
                        case 1 :
                            // InternalSML.g:4421:3: lv_operator_2_1= '+'
                            {
                            lv_operator_2_1=(Token)match(input,79,FollowSets000.FOLLOW_55); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_1, grammarAccess.getAdditionExpressionAccess().getOperatorPlusSignKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getAdditionExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalSML.g:4433:8: lv_operator_2_2= '-'
                            {
                            lv_operator_2_2=(Token)match(input,80,FollowSets000.FOLLOW_55); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_2, grammarAccess.getAdditionExpressionAccess().getOperatorHyphenMinusKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getAdditionExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalSML.g:4448:2: ( (lv_right_3_0= ruleAdditionExpression ) )
                    // InternalSML.g:4449:1: (lv_right_3_0= ruleAdditionExpression )
                    {
                    // InternalSML.g:4449:1: (lv_right_3_0= ruleAdditionExpression )
                    // InternalSML.g:4450:3: lv_right_3_0= ruleAdditionExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getAdditionExpressionAccess().getRightAdditionExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleAdditionExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getAdditionExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.AdditionExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdditionExpression"


    // $ANTLR start "entryRuleMultiplicationExpression"
    // InternalSML.g:4474:1: entryRuleMultiplicationExpression returns [EObject current=null] : iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF ;
    public final EObject entryRuleMultiplicationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMultiplicationExpression = null;


        try {
            // InternalSML.g:4475:2: (iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF )
            // InternalSML.g:4476:2: iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMultiplicationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleMultiplicationExpression=ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMultiplicationExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultiplicationExpression"


    // $ANTLR start "ruleMultiplicationExpression"
    // InternalSML.g:4483:1: ruleMultiplicationExpression returns [EObject current=null] : (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? ) ;
    public final EObject ruleMultiplicationExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        EObject this_NegatedExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:4486:28: ( (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? ) )
            // InternalSML.g:4487:1: (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? )
            {
            // InternalSML.g:4487:1: (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? )
            // InternalSML.g:4488:5: this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getMultiplicationExpressionAccess().getNegatedExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_71);
            this_NegatedExpression_0=ruleNegatedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_NegatedExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalSML.g:4496:1: ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )?
            int alt84=2;
            int LA84_0 = input.LA(1);

            if ( (LA84_0==49||LA84_0==81) ) {
                alt84=1;
            }
            switch (alt84) {
                case 1 :
                    // InternalSML.g:4496:2: () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) )
                    {
                    // InternalSML.g:4496:2: ()
                    // InternalSML.g:4497:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getMultiplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalSML.g:4502:2: ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) )
                    // InternalSML.g:4503:1: ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) )
                    {
                    // InternalSML.g:4503:1: ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) )
                    // InternalSML.g:4504:1: (lv_operator_2_1= '*' | lv_operator_2_2= '/' )
                    {
                    // InternalSML.g:4504:1: (lv_operator_2_1= '*' | lv_operator_2_2= '/' )
                    int alt83=2;
                    int LA83_0 = input.LA(1);

                    if ( (LA83_0==49) ) {
                        alt83=1;
                    }
                    else if ( (LA83_0==81) ) {
                        alt83=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 83, 0, input);

                        throw nvae;
                    }
                    switch (alt83) {
                        case 1 :
                            // InternalSML.g:4505:3: lv_operator_2_1= '*'
                            {
                            lv_operator_2_1=(Token)match(input,49,FollowSets000.FOLLOW_55); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_1, grammarAccess.getMultiplicationExpressionAccess().getOperatorAsteriskKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getMultiplicationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalSML.g:4517:8: lv_operator_2_2= '/'
                            {
                            lv_operator_2_2=(Token)match(input,81,FollowSets000.FOLLOW_55); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_2, grammarAccess.getMultiplicationExpressionAccess().getOperatorSolidusKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getMultiplicationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalSML.g:4532:2: ( (lv_right_3_0= ruleMultiplicationExpression ) )
                    // InternalSML.g:4533:1: (lv_right_3_0= ruleMultiplicationExpression )
                    {
                    // InternalSML.g:4533:1: (lv_right_3_0= ruleMultiplicationExpression )
                    // InternalSML.g:4534:3: lv_right_3_0= ruleMultiplicationExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getMultiplicationExpressionAccess().getRightMultiplicationExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleMultiplicationExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getMultiplicationExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.MultiplicationExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicationExpression"


    // $ANTLR start "entryRuleNegatedExpression"
    // InternalSML.g:4558:1: entryRuleNegatedExpression returns [EObject current=null] : iv_ruleNegatedExpression= ruleNegatedExpression EOF ;
    public final EObject entryRuleNegatedExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNegatedExpression = null;


        try {
            // InternalSML.g:4559:2: (iv_ruleNegatedExpression= ruleNegatedExpression EOF )
            // InternalSML.g:4560:2: iv_ruleNegatedExpression= ruleNegatedExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNegatedExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNegatedExpression=ruleNegatedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNegatedExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNegatedExpression"


    // $ANTLR start "ruleNegatedExpression"
    // InternalSML.g:4567:1: ruleNegatedExpression returns [EObject current=null] : ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression ) ;
    public final EObject ruleNegatedExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_1_1=null;
        Token lv_operator_1_2=null;
        EObject lv_operand_2_0 = null;

        EObject this_BasicExpression_3 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:4570:28: ( ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression ) )
            // InternalSML.g:4571:1: ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression )
            {
            // InternalSML.g:4571:1: ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression )
            int alt86=2;
            int LA86_0 = input.LA(1);

            if ( (LA86_0==80||LA86_0==82) ) {
                alt86=1;
            }
            else if ( ((LA86_0>=RULE_ID && LA86_0<=RULE_STRING)||LA86_0==RULE_BOOL||LA86_0==29||LA86_0==83) ) {
                alt86=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 86, 0, input);

                throw nvae;
            }
            switch (alt86) {
                case 1 :
                    // InternalSML.g:4571:2: ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) )
                    {
                    // InternalSML.g:4571:2: ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) )
                    // InternalSML.g:4571:3: () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) )
                    {
                    // InternalSML.g:4571:3: ()
                    // InternalSML.g:4572:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getNegatedExpressionAccess().getUnaryOperationExpressionAction_0_0(),
                                  current);
                          
                    }

                    }

                    // InternalSML.g:4577:2: ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) )
                    // InternalSML.g:4577:3: ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) )
                    {
                    // InternalSML.g:4590:1: ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) )
                    // InternalSML.g:4591:1: (lv_operator_1_1= '!' | lv_operator_1_2= '-' )
                    {
                    // InternalSML.g:4591:1: (lv_operator_1_1= '!' | lv_operator_1_2= '-' )
                    int alt85=2;
                    int LA85_0 = input.LA(1);

                    if ( (LA85_0==82) ) {
                        alt85=1;
                    }
                    else if ( (LA85_0==80) ) {
                        alt85=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 85, 0, input);

                        throw nvae;
                    }
                    switch (alt85) {
                        case 1 :
                            // InternalSML.g:4592:3: lv_operator_1_1= '!'
                            {
                            lv_operator_1_1=(Token)match(input,82,FollowSets000.FOLLOW_55); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_1_1, grammarAccess.getNegatedExpressionAccess().getOperatorExclamationMarkKeyword_0_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getNegatedExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_1_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalSML.g:4604:8: lv_operator_1_2= '-'
                            {
                            lv_operator_1_2=(Token)match(input,80,FollowSets000.FOLLOW_55); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_1_2, grammarAccess.getNegatedExpressionAccess().getOperatorHyphenMinusKeyword_0_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getNegatedExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_1_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalSML.g:4619:2: ( (lv_operand_2_0= ruleBasicExpression ) )
                    // InternalSML.g:4620:1: (lv_operand_2_0= ruleBasicExpression )
                    {
                    // InternalSML.g:4620:1: (lv_operand_2_0= ruleBasicExpression )
                    // InternalSML.g:4621:3: lv_operand_2_0= ruleBasicExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getNegatedExpressionAccess().getOperandBasicExpressionParserRuleCall_0_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_operand_2_0=ruleBasicExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getNegatedExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"operand",
                              		lv_operand_2_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.BasicExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSML.g:4639:5: this_BasicExpression_3= ruleBasicExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getNegatedExpressionAccess().getBasicExpressionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_BasicExpression_3=ruleBasicExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BasicExpression_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNegatedExpression"


    // $ANTLR start "entryRuleBasicExpression"
    // InternalSML.g:4655:1: entryRuleBasicExpression returns [EObject current=null] : iv_ruleBasicExpression= ruleBasicExpression EOF ;
    public final EObject entryRuleBasicExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBasicExpression = null;


        try {
            // InternalSML.g:4656:2: (iv_ruleBasicExpression= ruleBasicExpression EOF )
            // InternalSML.g:4657:2: iv_ruleBasicExpression= ruleBasicExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBasicExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBasicExpression=ruleBasicExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBasicExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBasicExpression"


    // $ANTLR start "ruleBasicExpression"
    // InternalSML.g:4664:1: ruleBasicExpression returns [EObject current=null] : (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) ) ;
    public final EObject ruleBasicExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject this_Value_0 = null;

        EObject this_Expression_2 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:4667:28: ( (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) ) )
            // InternalSML.g:4668:1: (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) )
            {
            // InternalSML.g:4668:1: (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) )
            int alt87=2;
            int LA87_0 = input.LA(1);

            if ( ((LA87_0>=RULE_ID && LA87_0<=RULE_STRING)||LA87_0==RULE_BOOL||LA87_0==83) ) {
                alt87=1;
            }
            else if ( (LA87_0==29) ) {
                alt87=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 87, 0, input);

                throw nvae;
            }
            switch (alt87) {
                case 1 :
                    // InternalSML.g:4669:5: this_Value_0= ruleValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBasicExpressionAccess().getValueParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Value_0=ruleValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Value_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:4678:6: (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' )
                    {
                    // InternalSML.g:4678:6: (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' )
                    // InternalSML.g:4678:8: otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')'
                    {
                    otherlv_1=(Token)match(input,29,FollowSets000.FOLLOW_55); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBasicExpressionAccess().getExpressionParserRuleCall_1_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_72);
                    this_Expression_2=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Expression_2; 
                              afterParserOrEnumRuleCall();
                          
                    }
                    otherlv_3=(Token)match(input,30,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getBasicExpressionAccess().getRightParenthesisKeyword_1_2());
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBasicExpression"


    // $ANTLR start "entryRuleValue"
    // InternalSML.g:4703:1: entryRuleValue returns [EObject current=null] : iv_ruleValue= ruleValue EOF ;
    public final EObject entryRuleValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleValue = null;


        try {
            // InternalSML.g:4704:2: (iv_ruleValue= ruleValue EOF )
            // InternalSML.g:4705:2: iv_ruleValue= ruleValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleValue=ruleValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValue"


    // $ANTLR start "ruleValue"
    // InternalSML.g:4712:1: ruleValue returns [EObject current=null] : (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess ) ;
    public final EObject ruleValue() throws RecognitionException {
        EObject current = null;

        EObject this_IntegerValue_0 = null;

        EObject this_BooleanValue_1 = null;

        EObject this_StringValue_2 = null;

        EObject this_EnumValue_3 = null;

        EObject this_NullValue_4 = null;

        EObject this_VariableValue_5 = null;

        EObject this_FeatureAccess_6 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:4715:28: ( (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess ) )
            // InternalSML.g:4716:1: (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess )
            {
            // InternalSML.g:4716:1: (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess )
            int alt88=7;
            switch ( input.LA(1) ) {
            case RULE_INT:
            case RULE_SIGNEDINT:
                {
                alt88=1;
                }
                break;
            case RULE_BOOL:
                {
                alt88=2;
                }
                break;
            case RULE_STRING:
                {
                alt88=3;
                }
                break;
            case RULE_ID:
                {
                switch ( input.LA(2) ) {
                case EOF:
                case RULE_ID:
                case 15:
                case 19:
                case 22:
                case 30:
                case 33:
                case 46:
                case 47:
                case 49:
                case 51:
                case 53:
                case 54:
                case 56:
                case 57:
                case 58:
                case 59:
                case 60:
                case 66:
                case 67:
                case 68:
                case 69:
                case 70:
                case 71:
                case 72:
                case 73:
                case 74:
                case 75:
                case 76:
                case 77:
                case 78:
                case 79:
                case 80:
                case 81:
                case 87:
                case 88:
                case 89:
                    {
                    alt88=6;
                    }
                    break;
                case 35:
                    {
                    alt88=7;
                    }
                    break;
                case 24:
                    {
                    alt88=4;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 88, 4, input);

                    throw nvae;
                }

                }
                break;
            case 83:
                {
                alt88=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 88, 0, input);

                throw nvae;
            }

            switch (alt88) {
                case 1 :
                    // InternalSML.g:4717:5: this_IntegerValue_0= ruleIntegerValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getIntegerValueParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntegerValue_0=ruleIntegerValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IntegerValue_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:4727:5: this_BooleanValue_1= ruleBooleanValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getBooleanValueParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_BooleanValue_1=ruleBooleanValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BooleanValue_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalSML.g:4737:5: this_StringValue_2= ruleStringValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getStringValueParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_StringValue_2=ruleStringValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_StringValue_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // InternalSML.g:4747:5: this_EnumValue_3= ruleEnumValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getEnumValueParserRuleCall_3()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_EnumValue_3=ruleEnumValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_EnumValue_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // InternalSML.g:4757:5: this_NullValue_4= ruleNullValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getNullValueParserRuleCall_4()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_NullValue_4=ruleNullValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_NullValue_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 6 :
                    // InternalSML.g:4767:5: this_VariableValue_5= ruleVariableValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getVariableValueParserRuleCall_5()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableValue_5=ruleVariableValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableValue_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 7 :
                    // InternalSML.g:4777:5: this_FeatureAccess_6= ruleFeatureAccess
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getFeatureAccessParserRuleCall_6()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_FeatureAccess_6=ruleFeatureAccess();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_FeatureAccess_6; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValue"


    // $ANTLR start "entryRuleIntegerValue"
    // InternalSML.g:4793:1: entryRuleIntegerValue returns [EObject current=null] : iv_ruleIntegerValue= ruleIntegerValue EOF ;
    public final EObject entryRuleIntegerValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerValue = null;


        try {
            // InternalSML.g:4794:2: (iv_ruleIntegerValue= ruleIntegerValue EOF )
            // InternalSML.g:4795:2: iv_ruleIntegerValue= ruleIntegerValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntegerValue=ruleIntegerValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerValue"


    // $ANTLR start "ruleIntegerValue"
    // InternalSML.g:4802:1: ruleIntegerValue returns [EObject current=null] : ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) ) ;
    public final EObject ruleIntegerValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_1=null;
        Token lv_value_0_2=null;

         enterRule(); 
            
        try {
            // InternalSML.g:4805:28: ( ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) ) )
            // InternalSML.g:4806:1: ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) )
            {
            // InternalSML.g:4806:1: ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) )
            // InternalSML.g:4807:1: ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) )
            {
            // InternalSML.g:4807:1: ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) )
            // InternalSML.g:4808:1: (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT )
            {
            // InternalSML.g:4808:1: (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT )
            int alt89=2;
            int LA89_0 = input.LA(1);

            if ( (LA89_0==RULE_INT) ) {
                alt89=1;
            }
            else if ( (LA89_0==RULE_SIGNEDINT) ) {
                alt89=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 89, 0, input);

                throw nvae;
            }
            switch (alt89) {
                case 1 :
                    // InternalSML.g:4809:3: lv_value_0_1= RULE_INT
                    {
                    lv_value_0_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_value_0_1, grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getIntegerValueRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"value",
                              		lv_value_0_1, 
                              		"org.eclipse.xtext.common.Terminals.INT");
                      	    
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:4824:8: lv_value_0_2= RULE_SIGNEDINT
                    {
                    lv_value_0_2=(Token)match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_value_0_2, grammarAccess.getIntegerValueAccess().getValueSIGNEDINTTerminalRuleCall_0_1()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getIntegerValueRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"value",
                              		lv_value_0_2, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
                      	    
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerValue"


    // $ANTLR start "entryRuleBooleanValue"
    // InternalSML.g:4850:1: entryRuleBooleanValue returns [EObject current=null] : iv_ruleBooleanValue= ruleBooleanValue EOF ;
    public final EObject entryRuleBooleanValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanValue = null;


        try {
            // InternalSML.g:4851:2: (iv_ruleBooleanValue= ruleBooleanValue EOF )
            // InternalSML.g:4852:2: iv_ruleBooleanValue= ruleBooleanValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBooleanValue=ruleBooleanValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanValue"


    // $ANTLR start "ruleBooleanValue"
    // InternalSML.g:4859:1: ruleBooleanValue returns [EObject current=null] : ( (lv_value_0_0= RULE_BOOL ) ) ;
    public final EObject ruleBooleanValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // InternalSML.g:4862:28: ( ( (lv_value_0_0= RULE_BOOL ) ) )
            // InternalSML.g:4863:1: ( (lv_value_0_0= RULE_BOOL ) )
            {
            // InternalSML.g:4863:1: ( (lv_value_0_0= RULE_BOOL ) )
            // InternalSML.g:4864:1: (lv_value_0_0= RULE_BOOL )
            {
            // InternalSML.g:4864:1: (lv_value_0_0= RULE_BOOL )
            // InternalSML.g:4865:3: lv_value_0_0= RULE_BOOL
            {
            lv_value_0_0=(Token)match(input,RULE_BOOL,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_0_0, grammarAccess.getBooleanValueAccess().getValueBOOLTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getBooleanValueRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.BOOL");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanValue"


    // $ANTLR start "entryRuleStringValue"
    // InternalSML.g:4889:1: entryRuleStringValue returns [EObject current=null] : iv_ruleStringValue= ruleStringValue EOF ;
    public final EObject entryRuleStringValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringValue = null;


        try {
            // InternalSML.g:4890:2: (iv_ruleStringValue= ruleStringValue EOF )
            // InternalSML.g:4891:2: iv_ruleStringValue= ruleStringValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStringValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStringValue=ruleStringValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStringValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringValue"


    // $ANTLR start "ruleStringValue"
    // InternalSML.g:4898:1: ruleStringValue returns [EObject current=null] : ( (lv_value_0_0= RULE_STRING ) ) ;
    public final EObject ruleStringValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // InternalSML.g:4901:28: ( ( (lv_value_0_0= RULE_STRING ) ) )
            // InternalSML.g:4902:1: ( (lv_value_0_0= RULE_STRING ) )
            {
            // InternalSML.g:4902:1: ( (lv_value_0_0= RULE_STRING ) )
            // InternalSML.g:4903:1: (lv_value_0_0= RULE_STRING )
            {
            // InternalSML.g:4903:1: (lv_value_0_0= RULE_STRING )
            // InternalSML.g:4904:3: lv_value_0_0= RULE_STRING
            {
            lv_value_0_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_0_0, grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getStringValueRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"org.eclipse.xtext.common.Terminals.STRING");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringValue"


    // $ANTLR start "entryRuleEnumValue"
    // InternalSML.g:4928:1: entryRuleEnumValue returns [EObject current=null] : iv_ruleEnumValue= ruleEnumValue EOF ;
    public final EObject entryRuleEnumValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumValue = null;


        try {
            // InternalSML.g:4929:2: (iv_ruleEnumValue= ruleEnumValue EOF )
            // InternalSML.g:4930:2: iv_ruleEnumValue= ruleEnumValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEnumValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleEnumValue=ruleEnumValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEnumValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumValue"


    // $ANTLR start "ruleEnumValue"
    // InternalSML.g:4937:1: ruleEnumValue returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) ) ;
    public final EObject ruleEnumValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // InternalSML.g:4940:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) ) )
            // InternalSML.g:4941:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) )
            {
            // InternalSML.g:4941:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) )
            // InternalSML.g:4941:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) )
            {
            // InternalSML.g:4941:2: ( (otherlv_0= RULE_ID ) )
            // InternalSML.g:4942:1: (otherlv_0= RULE_ID )
            {
            // InternalSML.g:4942:1: (otherlv_0= RULE_ID )
            // InternalSML.g:4943:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getEnumValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_73); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getEnumValueAccess().getTypeEEnumCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,24,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getEnumValueAccess().getColonKeyword_1());
                  
            }
            // InternalSML.g:4958:1: ( (otherlv_2= RULE_ID ) )
            // InternalSML.g:4959:1: (otherlv_2= RULE_ID )
            {
            // InternalSML.g:4959:1: (otherlv_2= RULE_ID )
            // InternalSML.g:4960:3: otherlv_2= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getEnumValueRule());
              	        }
                      
            }
            otherlv_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_2, grammarAccess.getEnumValueAccess().getValueEEnumLiteralCrossReference_2_0()); 
              	
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumValue"


    // $ANTLR start "entryRuleNullValue"
    // InternalSML.g:4979:1: entryRuleNullValue returns [EObject current=null] : iv_ruleNullValue= ruleNullValue EOF ;
    public final EObject entryRuleNullValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNullValue = null;


        try {
            // InternalSML.g:4980:2: (iv_ruleNullValue= ruleNullValue EOF )
            // InternalSML.g:4981:2: iv_ruleNullValue= ruleNullValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNullValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNullValue=ruleNullValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNullValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNullValue"


    // $ANTLR start "ruleNullValue"
    // InternalSML.g:4988:1: ruleNullValue returns [EObject current=null] : ( () otherlv_1= 'null' ) ;
    public final EObject ruleNullValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalSML.g:4991:28: ( ( () otherlv_1= 'null' ) )
            // InternalSML.g:4992:1: ( () otherlv_1= 'null' )
            {
            // InternalSML.g:4992:1: ( () otherlv_1= 'null' )
            // InternalSML.g:4992:2: () otherlv_1= 'null'
            {
            // InternalSML.g:4992:2: ()
            // InternalSML.g:4993:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getNullValueAccess().getNullValueAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,83,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getNullValueAccess().getNullKeyword_1());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNullValue"


    // $ANTLR start "entryRuleVariableValue"
    // InternalSML.g:5010:1: entryRuleVariableValue returns [EObject current=null] : iv_ruleVariableValue= ruleVariableValue EOF ;
    public final EObject entryRuleVariableValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableValue = null;


        try {
            // InternalSML.g:5011:2: (iv_ruleVariableValue= ruleVariableValue EOF )
            // InternalSML.g:5012:2: iv_ruleVariableValue= ruleVariableValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableValue=ruleVariableValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableValue"


    // $ANTLR start "ruleVariableValue"
    // InternalSML.g:5019:1: ruleVariableValue returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleVariableValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // InternalSML.g:5022:28: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalSML.g:5023:1: ( (otherlv_0= RULE_ID ) )
            {
            // InternalSML.g:5023:1: ( (otherlv_0= RULE_ID ) )
            // InternalSML.g:5024:1: (otherlv_0= RULE_ID )
            {
            // InternalSML.g:5024:1: (otherlv_0= RULE_ID )
            // InternalSML.g:5025:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getVariableValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getVariableValueAccess().getValueVariableCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableValue"


    // $ANTLR start "entryRuleCollectionAccess"
    // InternalSML.g:5044:1: entryRuleCollectionAccess returns [EObject current=null] : iv_ruleCollectionAccess= ruleCollectionAccess EOF ;
    public final EObject entryRuleCollectionAccess() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCollectionAccess = null;


        try {
            // InternalSML.g:5045:2: (iv_ruleCollectionAccess= ruleCollectionAccess EOF )
            // InternalSML.g:5046:2: iv_ruleCollectionAccess= ruleCollectionAccess EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCollectionAccessRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCollectionAccess=ruleCollectionAccess();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCollectionAccess; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCollectionAccess"


    // $ANTLR start "ruleCollectionAccess"
    // InternalSML.g:5053:1: ruleCollectionAccess returns [EObject current=null] : ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' ) ;
    public final EObject ruleCollectionAccess() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Enumerator lv_collectionOperation_0_0 = null;

        EObject lv_parameter_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:5056:28: ( ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' ) )
            // InternalSML.g:5057:1: ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' )
            {
            // InternalSML.g:5057:1: ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' )
            // InternalSML.g:5057:2: ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')'
            {
            // InternalSML.g:5057:2: ( (lv_collectionOperation_0_0= ruleCollectionOperation ) )
            // InternalSML.g:5058:1: (lv_collectionOperation_0_0= ruleCollectionOperation )
            {
            // InternalSML.g:5058:1: (lv_collectionOperation_0_0= ruleCollectionOperation )
            // InternalSML.g:5059:3: lv_collectionOperation_0_0= ruleCollectionOperation
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getCollectionAccessAccess().getCollectionOperationCollectionOperationEnumRuleCall_0_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_19);
            lv_collectionOperation_0_0=ruleCollectionOperation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getCollectionAccessRule());
              	        }
                     		set(
                     			current, 
                     			"collectionOperation",
                      		lv_collectionOperation_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionOperation");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,29,FollowSets000.FOLLOW_74); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getCollectionAccessAccess().getLeftParenthesisKeyword_1());
                  
            }
            // InternalSML.g:5079:1: ( (lv_parameter_2_0= ruleExpression ) )?
            int alt90=2;
            int LA90_0 = input.LA(1);

            if ( ((LA90_0>=RULE_ID && LA90_0<=RULE_STRING)||LA90_0==RULE_BOOL||LA90_0==29||LA90_0==80||(LA90_0>=82 && LA90_0<=83)) ) {
                alt90=1;
            }
            switch (alt90) {
                case 1 :
                    // InternalSML.g:5080:1: (lv_parameter_2_0= ruleExpression )
                    {
                    // InternalSML.g:5080:1: (lv_parameter_2_0= ruleExpression )
                    // InternalSML.g:5081:3: lv_parameter_2_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getCollectionAccessAccess().getParameterExpressionParserRuleCall_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_72);
                    lv_parameter_2_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getCollectionAccessRule());
                      	        }
                             		set(
                             			current, 
                             			"parameter",
                              		lv_parameter_2_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,30,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getCollectionAccessAccess().getRightParenthesisKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollectionAccess"


    // $ANTLR start "entryRuleFeatureAccess"
    // InternalSML.g:5109:1: entryRuleFeatureAccess returns [EObject current=null] : iv_ruleFeatureAccess= ruleFeatureAccess EOF ;
    public final EObject entryRuleFeatureAccess() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureAccess = null;


        try {
            // InternalSML.g:5110:2: (iv_ruleFeatureAccess= ruleFeatureAccess EOF )
            // InternalSML.g:5111:2: iv_ruleFeatureAccess= ruleFeatureAccess EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFeatureAccessRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleFeatureAccess=ruleFeatureAccess();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFeatureAccess; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureAccess"


    // $ANTLR start "ruleFeatureAccess"
    // InternalSML.g:5118:1: ruleFeatureAccess returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) | ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' ) ) ) ;
    public final EObject ruleFeatureAccess() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        EObject lv_value_2_0 = null;

        EObject lv_collectionAccess_4_0 = null;

        EObject lv_value_5_0 = null;

        EObject lv_parameters_7_0 = null;

        EObject lv_parameters_9_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:5121:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) | ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' ) ) ) )
            // InternalSML.g:5122:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) | ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' ) ) )
            {
            // InternalSML.g:5122:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) | ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' ) ) )
            // InternalSML.g:5122:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) | ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' ) )
            {
            // InternalSML.g:5122:2: ( (otherlv_0= RULE_ID ) )
            // InternalSML.g:5123:1: (otherlv_0= RULE_ID )
            {
            // InternalSML.g:5123:1: (otherlv_0= RULE_ID )
            // InternalSML.g:5124:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getFeatureAccessRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_44); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getFeatureAccessAccess().getTargetEObjectCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,35,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getFeatureAccessAccess().getFullStopKeyword_1());
                  
            }
            // InternalSML.g:5139:1: ( ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) | ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' ) )
            int alt94=2;
            int LA94_0 = input.LA(1);

            if ( (LA94_0==RULE_ID) ) {
                int LA94_1 = input.LA(2);

                if ( (LA94_1==EOF||LA94_1==RULE_ID||LA94_1==15||LA94_1==19||LA94_1==22||LA94_1==30||LA94_1==33||LA94_1==35||(LA94_1>=46 && LA94_1<=47)||LA94_1==49||LA94_1==51||(LA94_1>=53 && LA94_1<=54)||(LA94_1>=56 && LA94_1<=60)||(LA94_1>=66 && LA94_1<=81)||(LA94_1>=87 && LA94_1<=89)) ) {
                    alt94=1;
                }
                else if ( (LA94_1==29) ) {
                    alt94=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 94, 1, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 94, 0, input);

                throw nvae;
            }
            switch (alt94) {
                case 1 :
                    // InternalSML.g:5139:2: ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? )
                    {
                    // InternalSML.g:5139:2: ( ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? )
                    // InternalSML.g:5139:3: ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )?
                    {
                    // InternalSML.g:5139:3: ( (lv_value_2_0= ruleStructuralFeatureValue ) )
                    // InternalSML.g:5140:1: (lv_value_2_0= ruleStructuralFeatureValue )
                    {
                    // InternalSML.g:5140:1: (lv_value_2_0= ruleStructuralFeatureValue )
                    // InternalSML.g:5141:3: lv_value_2_0= ruleStructuralFeatureValue
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getValueStructuralFeatureValueParserRuleCall_2_0_0_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_29);
                    lv_value_2_0=ruleStructuralFeatureValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
                      	        }
                             		set(
                             			current, 
                             			"value",
                              		lv_value_2_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.StructuralFeatureValue");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // InternalSML.g:5157:2: (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )?
                    int alt91=2;
                    int LA91_0 = input.LA(1);

                    if ( (LA91_0==35) ) {
                        alt91=1;
                    }
                    switch (alt91) {
                        case 1 :
                            // InternalSML.g:5157:4: otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) )
                            {
                            otherlv_3=(Token)match(input,35,FollowSets000.FOLLOW_75); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                  	newLeafNode(otherlv_3, grammarAccess.getFeatureAccessAccess().getFullStopKeyword_2_0_1_0());
                                  
                            }
                            // InternalSML.g:5161:1: ( (lv_collectionAccess_4_0= ruleCollectionAccess ) )
                            // InternalSML.g:5162:1: (lv_collectionAccess_4_0= ruleCollectionAccess )
                            {
                            // InternalSML.g:5162:1: (lv_collectionAccess_4_0= ruleCollectionAccess )
                            // InternalSML.g:5163:3: lv_collectionAccess_4_0= ruleCollectionAccess
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getCollectionAccessCollectionAccessParserRuleCall_2_0_1_1_0()); 
                              	    
                            }
                            pushFollow(FollowSets000.FOLLOW_2);
                            lv_collectionAccess_4_0=ruleCollectionAccess();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"collectionAccess",
                                      		lv_collectionAccess_4_0, 
                                      		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionAccess");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSML.g:5180:6: ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' )
                    {
                    // InternalSML.g:5180:6: ( ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')' )
                    // InternalSML.g:5180:7: ( (lv_value_5_0= ruleOperationValue ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )? otherlv_10= ')'
                    {
                    // InternalSML.g:5180:7: ( (lv_value_5_0= ruleOperationValue ) )
                    // InternalSML.g:5181:1: (lv_value_5_0= ruleOperationValue )
                    {
                    // InternalSML.g:5181:1: (lv_value_5_0= ruleOperationValue )
                    // InternalSML.g:5182:3: lv_value_5_0= ruleOperationValue
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getValueOperationValueParserRuleCall_2_1_0_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_19);
                    lv_value_5_0=ruleOperationValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
                      	        }
                             		set(
                             			current, 
                             			"value",
                              		lv_value_5_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.OperationValue");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    otherlv_6=(Token)match(input,29,FollowSets000.FOLLOW_74); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_6, grammarAccess.getFeatureAccessAccess().getLeftParenthesisKeyword_2_1_1());
                          
                    }
                    // InternalSML.g:5202:1: ( ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )* )?
                    int alt93=2;
                    int LA93_0 = input.LA(1);

                    if ( ((LA93_0>=RULE_ID && LA93_0<=RULE_STRING)||LA93_0==RULE_BOOL||LA93_0==29||LA93_0==80||(LA93_0>=82 && LA93_0<=83)) ) {
                        alt93=1;
                    }
                    switch (alt93) {
                        case 1 :
                            // InternalSML.g:5202:2: ( (lv_parameters_7_0= ruleExpression ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )*
                            {
                            // InternalSML.g:5202:2: ( (lv_parameters_7_0= ruleExpression ) )
                            // InternalSML.g:5203:1: (lv_parameters_7_0= ruleExpression )
                            {
                            // InternalSML.g:5203:1: (lv_parameters_7_0= ruleExpression )
                            // InternalSML.g:5204:3: lv_parameters_7_0= ruleExpression
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getParametersExpressionParserRuleCall_2_1_2_0_0()); 
                              	    
                            }
                            pushFollow(FollowSets000.FOLLOW_20);
                            lv_parameters_7_0=ruleExpression();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
                              	        }
                                     		add(
                                     			current, 
                                     			"parameters",
                                      		lv_parameters_7_0, 
                                      		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }

                            // InternalSML.g:5220:2: (otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) ) )*
                            loop92:
                            do {
                                int alt92=2;
                                int LA92_0 = input.LA(1);

                                if ( (LA92_0==22) ) {
                                    alt92=1;
                                }


                                switch (alt92) {
                            	case 1 :
                            	    // InternalSML.g:5220:4: otherlv_8= ',' ( (lv_parameters_9_0= ruleExpression ) )
                            	    {
                            	    otherlv_8=(Token)match(input,22,FollowSets000.FOLLOW_55); if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	          	newLeafNode(otherlv_8, grammarAccess.getFeatureAccessAccess().getCommaKeyword_2_1_2_1_0());
                            	          
                            	    }
                            	    // InternalSML.g:5224:1: ( (lv_parameters_9_0= ruleExpression ) )
                            	    // InternalSML.g:5225:1: (lv_parameters_9_0= ruleExpression )
                            	    {
                            	    // InternalSML.g:5225:1: (lv_parameters_9_0= ruleExpression )
                            	    // InternalSML.g:5226:3: lv_parameters_9_0= ruleExpression
                            	    {
                            	    if ( state.backtracking==0 ) {
                            	       
                            	      	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getParametersExpressionParserRuleCall_2_1_2_1_1_0()); 
                            	      	    
                            	    }
                            	    pushFollow(FollowSets000.FOLLOW_20);
                            	    lv_parameters_9_0=ruleExpression();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      	        if (current==null) {
                            	      	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
                            	      	        }
                            	             		add(
                            	             			current, 
                            	             			"parameters",
                            	              		lv_parameters_9_0, 
                            	              		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
                            	      	        afterParserOrEnumRuleCall();
                            	      	    
                            	    }

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop92;
                                }
                            } while (true);


                            }
                            break;

                    }

                    otherlv_10=(Token)match(input,30,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_10, grammarAccess.getFeatureAccessAccess().getRightParenthesisKeyword_2_1_3());
                          
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureAccess"


    // $ANTLR start "entryRuleStructuralFeatureValue"
    // InternalSML.g:5254:1: entryRuleStructuralFeatureValue returns [EObject current=null] : iv_ruleStructuralFeatureValue= ruleStructuralFeatureValue EOF ;
    public final EObject entryRuleStructuralFeatureValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStructuralFeatureValue = null;


        try {
            // InternalSML.g:5255:2: (iv_ruleStructuralFeatureValue= ruleStructuralFeatureValue EOF )
            // InternalSML.g:5256:2: iv_ruleStructuralFeatureValue= ruleStructuralFeatureValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStructuralFeatureValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStructuralFeatureValue=ruleStructuralFeatureValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStructuralFeatureValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStructuralFeatureValue"


    // $ANTLR start "ruleStructuralFeatureValue"
    // InternalSML.g:5263:1: ruleStructuralFeatureValue returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleStructuralFeatureValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // InternalSML.g:5266:28: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalSML.g:5267:1: ( (otherlv_0= RULE_ID ) )
            {
            // InternalSML.g:5267:1: ( (otherlv_0= RULE_ID ) )
            // InternalSML.g:5268:1: (otherlv_0= RULE_ID )
            {
            // InternalSML.g:5268:1: (otherlv_0= RULE_ID )
            // InternalSML.g:5269:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getStructuralFeatureValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStructuralFeatureValue"


    // $ANTLR start "entryRuleOperationValue"
    // InternalSML.g:5288:1: entryRuleOperationValue returns [EObject current=null] : iv_ruleOperationValue= ruleOperationValue EOF ;
    public final EObject entryRuleOperationValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOperationValue = null;


        try {
            // InternalSML.g:5289:2: (iv_ruleOperationValue= ruleOperationValue EOF )
            // InternalSML.g:5290:2: iv_ruleOperationValue= ruleOperationValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOperationValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleOperationValue=ruleOperationValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOperationValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOperationValue"


    // $ANTLR start "ruleOperationValue"
    // InternalSML.g:5297:1: ruleOperationValue returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleOperationValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // InternalSML.g:5300:28: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalSML.g:5301:1: ( (otherlv_0= RULE_ID ) )
            {
            // InternalSML.g:5301:1: ( (otherlv_0= RULE_ID ) )
            // InternalSML.g:5302:1: (otherlv_0= RULE_ID )
            {
            // InternalSML.g:5302:1: (otherlv_0= RULE_ID )
            // InternalSML.g:5303:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getOperationValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getOperationValueAccess().getValueEOperationCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOperationValue"


    // $ANTLR start "ruleScenarioKind"
    // InternalSML.g:5322:1: ruleScenarioKind returns [Enumerator current=null] : ( (enumLiteral_0= 'assumption' ) | (enumLiteral_1= 'guarantee' ) | (enumLiteral_2= 'existential' ) ) ;
    public final Enumerator ruleScenarioKind() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;

         enterRule(); 
        try {
            // InternalSML.g:5324:28: ( ( (enumLiteral_0= 'assumption' ) | (enumLiteral_1= 'guarantee' ) | (enumLiteral_2= 'existential' ) ) )
            // InternalSML.g:5325:1: ( (enumLiteral_0= 'assumption' ) | (enumLiteral_1= 'guarantee' ) | (enumLiteral_2= 'existential' ) )
            {
            // InternalSML.g:5325:1: ( (enumLiteral_0= 'assumption' ) | (enumLiteral_1= 'guarantee' ) | (enumLiteral_2= 'existential' ) )
            int alt95=3;
            switch ( input.LA(1) ) {
            case 84:
                {
                alt95=1;
                }
                break;
            case 85:
                {
                alt95=2;
                }
                break;
            case 86:
                {
                alt95=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 95, 0, input);

                throw nvae;
            }

            switch (alt95) {
                case 1 :
                    // InternalSML.g:5325:2: (enumLiteral_0= 'assumption' )
                    {
                    // InternalSML.g:5325:2: (enumLiteral_0= 'assumption' )
                    // InternalSML.g:5325:4: enumLiteral_0= 'assumption'
                    {
                    enumLiteral_0=(Token)match(input,84,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getScenarioKindAccess().getAssumptionEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getScenarioKindAccess().getAssumptionEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalSML.g:5331:6: (enumLiteral_1= 'guarantee' )
                    {
                    // InternalSML.g:5331:6: (enumLiteral_1= 'guarantee' )
                    // InternalSML.g:5331:8: enumLiteral_1= 'guarantee'
                    {
                    enumLiteral_1=(Token)match(input,85,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getScenarioKindAccess().getGuaranteeEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getScenarioKindAccess().getGuaranteeEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalSML.g:5337:6: (enumLiteral_2= 'existential' )
                    {
                    // InternalSML.g:5337:6: (enumLiteral_2= 'existential' )
                    // InternalSML.g:5337:8: enumLiteral_2= 'existential'
                    {
                    enumLiteral_2=(Token)match(input,86,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getScenarioKindAccess().getExistentialEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getScenarioKindAccess().getExistentialEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleScenarioKind"


    // $ANTLR start "ruleExpectationKind"
    // InternalSML.g:5347:1: ruleExpectationKind returns [Enumerator current=null] : ( (enumLiteral_0= 'eventually' ) | (enumLiteral_1= 'requested' ) | (enumLiteral_2= 'urgent' ) | (enumLiteral_3= 'committed' ) ) ;
    public final Enumerator ruleExpectationKind() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;

         enterRule(); 
        try {
            // InternalSML.g:5349:28: ( ( (enumLiteral_0= 'eventually' ) | (enumLiteral_1= 'requested' ) | (enumLiteral_2= 'urgent' ) | (enumLiteral_3= 'committed' ) ) )
            // InternalSML.g:5350:1: ( (enumLiteral_0= 'eventually' ) | (enumLiteral_1= 'requested' ) | (enumLiteral_2= 'urgent' ) | (enumLiteral_3= 'committed' ) )
            {
            // InternalSML.g:5350:1: ( (enumLiteral_0= 'eventually' ) | (enumLiteral_1= 'requested' ) | (enumLiteral_2= 'urgent' ) | (enumLiteral_3= 'committed' ) )
            int alt96=4;
            switch ( input.LA(1) ) {
            case 57:
                {
                alt96=1;
                }
                break;
            case 87:
                {
                alt96=2;
                }
                break;
            case 88:
                {
                alt96=3;
                }
                break;
            case 89:
                {
                alt96=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 96, 0, input);

                throw nvae;
            }

            switch (alt96) {
                case 1 :
                    // InternalSML.g:5350:2: (enumLiteral_0= 'eventually' )
                    {
                    // InternalSML.g:5350:2: (enumLiteral_0= 'eventually' )
                    // InternalSML.g:5350:4: enumLiteral_0= 'eventually'
                    {
                    enumLiteral_0=(Token)match(input,57,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getExpectationKindAccess().getEventuallyEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getExpectationKindAccess().getEventuallyEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalSML.g:5356:6: (enumLiteral_1= 'requested' )
                    {
                    // InternalSML.g:5356:6: (enumLiteral_1= 'requested' )
                    // InternalSML.g:5356:8: enumLiteral_1= 'requested'
                    {
                    enumLiteral_1=(Token)match(input,87,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getExpectationKindAccess().getRequestedEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getExpectationKindAccess().getRequestedEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalSML.g:5362:6: (enumLiteral_2= 'urgent' )
                    {
                    // InternalSML.g:5362:6: (enumLiteral_2= 'urgent' )
                    // InternalSML.g:5362:8: enumLiteral_2= 'urgent'
                    {
                    enumLiteral_2=(Token)match(input,88,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getExpectationKindAccess().getUrgentEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getExpectationKindAccess().getUrgentEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalSML.g:5368:6: (enumLiteral_3= 'committed' )
                    {
                    // InternalSML.g:5368:6: (enumLiteral_3= 'committed' )
                    // InternalSML.g:5368:8: enumLiteral_3= 'committed'
                    {
                    enumLiteral_3=(Token)match(input,89,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getExpectationKindAccess().getCommittedEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_3, grammarAccess.getExpectationKindAccess().getCommittedEnumLiteralDeclaration_3()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpectationKind"


    // $ANTLR start "ruleCollectionOperation"
    // InternalSML.g:5378:1: ruleCollectionOperation returns [Enumerator current=null] : ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) ) ;
    public final Enumerator ruleCollectionOperation() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;
        Token enumLiteral_7=null;

         enterRule(); 
        try {
            // InternalSML.g:5380:28: ( ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) ) )
            // InternalSML.g:5381:1: ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) )
            {
            // InternalSML.g:5381:1: ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) )
            int alt97=8;
            switch ( input.LA(1) ) {
            case 90:
                {
                alt97=1;
                }
                break;
            case 91:
                {
                alt97=2;
                }
                break;
            case 92:
                {
                alt97=3;
                }
                break;
            case 93:
                {
                alt97=4;
                }
                break;
            case 94:
                {
                alt97=5;
                }
                break;
            case 95:
                {
                alt97=6;
                }
                break;
            case 96:
                {
                alt97=7;
                }
                break;
            case 97:
                {
                alt97=8;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 97, 0, input);

                throw nvae;
            }

            switch (alt97) {
                case 1 :
                    // InternalSML.g:5381:2: (enumLiteral_0= 'any' )
                    {
                    // InternalSML.g:5381:2: (enumLiteral_0= 'any' )
                    // InternalSML.g:5381:4: enumLiteral_0= 'any'
                    {
                    enumLiteral_0=(Token)match(input,90,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalSML.g:5387:6: (enumLiteral_1= 'contains' )
                    {
                    // InternalSML.g:5387:6: (enumLiteral_1= 'contains' )
                    // InternalSML.g:5387:8: enumLiteral_1= 'contains'
                    {
                    enumLiteral_1=(Token)match(input,91,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalSML.g:5393:6: (enumLiteral_2= 'containsAll' )
                    {
                    // InternalSML.g:5393:6: (enumLiteral_2= 'containsAll' )
                    // InternalSML.g:5393:8: enumLiteral_2= 'containsAll'
                    {
                    enumLiteral_2=(Token)match(input,92,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalSML.g:5399:6: (enumLiteral_3= 'first' )
                    {
                    // InternalSML.g:5399:6: (enumLiteral_3= 'first' )
                    // InternalSML.g:5399:8: enumLiteral_3= 'first'
                    {
                    enumLiteral_3=(Token)match(input,93,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_3, grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3()); 
                          
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalSML.g:5405:6: (enumLiteral_4= 'get' )
                    {
                    // InternalSML.g:5405:6: (enumLiteral_4= 'get' )
                    // InternalSML.g:5405:8: enumLiteral_4= 'get'
                    {
                    enumLiteral_4=(Token)match(input,94,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_4, grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4()); 
                          
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalSML.g:5411:6: (enumLiteral_5= 'isEmpty' )
                    {
                    // InternalSML.g:5411:6: (enumLiteral_5= 'isEmpty' )
                    // InternalSML.g:5411:8: enumLiteral_5= 'isEmpty'
                    {
                    enumLiteral_5=(Token)match(input,95,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_5, grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5()); 
                          
                    }

                    }


                    }
                    break;
                case 7 :
                    // InternalSML.g:5417:6: (enumLiteral_6= 'last' )
                    {
                    // InternalSML.g:5417:6: (enumLiteral_6= 'last' )
                    // InternalSML.g:5417:8: enumLiteral_6= 'last'
                    {
                    enumLiteral_6=(Token)match(input,96,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_6, grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6()); 
                          
                    }

                    }


                    }
                    break;
                case 8 :
                    // InternalSML.g:5423:6: (enumLiteral_7= 'size' )
                    {
                    // InternalSML.g:5423:6: (enumLiteral_7= 'size' )
                    // InternalSML.g:5423:8: enumLiteral_7= 'size'
                    {
                    enumLiteral_7=(Token)match(input,97,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_7, grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollectionOperation"


    // $ANTLR start "ruleCollectionModification"
    // InternalSML.g:5433:1: ruleCollectionModification returns [Enumerator current=null] : ( (enumLiteral_0= 'add' ) | (enumLiteral_1= 'addToFront' ) | (enumLiteral_2= 'clear' ) | (enumLiteral_3= 'remove' ) ) ;
    public final Enumerator ruleCollectionModification() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;

         enterRule(); 
        try {
            // InternalSML.g:5435:28: ( ( (enumLiteral_0= 'add' ) | (enumLiteral_1= 'addToFront' ) | (enumLiteral_2= 'clear' ) | (enumLiteral_3= 'remove' ) ) )
            // InternalSML.g:5436:1: ( (enumLiteral_0= 'add' ) | (enumLiteral_1= 'addToFront' ) | (enumLiteral_2= 'clear' ) | (enumLiteral_3= 'remove' ) )
            {
            // InternalSML.g:5436:1: ( (enumLiteral_0= 'add' ) | (enumLiteral_1= 'addToFront' ) | (enumLiteral_2= 'clear' ) | (enumLiteral_3= 'remove' ) )
            int alt98=4;
            switch ( input.LA(1) ) {
            case 98:
                {
                alt98=1;
                }
                break;
            case 99:
                {
                alt98=2;
                }
                break;
            case 100:
                {
                alt98=3;
                }
                break;
            case 101:
                {
                alt98=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 98, 0, input);

                throw nvae;
            }

            switch (alt98) {
                case 1 :
                    // InternalSML.g:5436:2: (enumLiteral_0= 'add' )
                    {
                    // InternalSML.g:5436:2: (enumLiteral_0= 'add' )
                    // InternalSML.g:5436:4: enumLiteral_0= 'add'
                    {
                    enumLiteral_0=(Token)match(input,98,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionModificationAccess().getAddEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getCollectionModificationAccess().getAddEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalSML.g:5442:6: (enumLiteral_1= 'addToFront' )
                    {
                    // InternalSML.g:5442:6: (enumLiteral_1= 'addToFront' )
                    // InternalSML.g:5442:8: enumLiteral_1= 'addToFront'
                    {
                    enumLiteral_1=(Token)match(input,99,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionModificationAccess().getAddToFrontEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getCollectionModificationAccess().getAddToFrontEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalSML.g:5448:6: (enumLiteral_2= 'clear' )
                    {
                    // InternalSML.g:5448:6: (enumLiteral_2= 'clear' )
                    // InternalSML.g:5448:8: enumLiteral_2= 'clear'
                    {
                    enumLiteral_2=(Token)match(input,100,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionModificationAccess().getClearEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getCollectionModificationAccess().getClearEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalSML.g:5454:6: (enumLiteral_3= 'remove' )
                    {
                    // InternalSML.g:5454:6: (enumLiteral_3= 'remove' )
                    // InternalSML.g:5454:8: enumLiteral_3= 'remove'
                    {
                    enumLiteral_3=(Token)match(input,101,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionModificationAccess().getRemoveEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_3, grammarAccess.getCollectionModificationAccess().getRemoveEnumLiteralDeclaration_3()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollectionModification"

    // $ANTLR start synpred1_InternalSML
    public final void synpred1_InternalSML_fragment() throws RecognitionException {   
        // InternalSML.g:134:2: ( 'domain' )
        // InternalSML.g:134:4: 'domain'
        {
        match(input,16,FollowSets000.FOLLOW_2); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred1_InternalSML

    // Delegated rules

    public final boolean synpred1_InternalSML() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred1_InternalSML_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA42 dfa42 = new DFA42(this);
    static final String dfa_1s = "\12\uffff";
    static final String dfa_2s = "\1\4\2\uffff\1\37\6\uffff";
    static final String dfa_3s = "\1\131\2\uffff\1\60\6\uffff";
    static final String dfa_4s = "\1\uffff\1\1\1\2\1\uffff\1\3\1\4\1\5\1\6\1\7\1\10";
    static final String dfa_5s = "\12\uffff}>";
    static final String[] dfa_6s = {
            "\1\3\12\uffff\1\1\36\uffff\2\2\3\uffff\1\4\1\uffff\1\5\1\6\1\uffff\1\7\1\2\2\7\1\10\6\uffff\3\11\21\uffff\3\2",
            "",
            "",
            "\1\11\20\uffff\1\2",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA42 extends DFA {

        public DFA42(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 42;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "1661:1: (this_Interaction_0= ruleInteraction | this_ModalMessage_1= ruleModalMessage | this_Alternative_2= ruleAlternative | this_Loop_3= ruleLoop | this_Parallel_4= ruleParallel | this_ConditionFragment_5= ruleConditionFragment | this_TimedConditionFragment_6= ruleTimedConditionFragment | this_VariableFragment_7= ruleVariableFragment )";
        }
    }
 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000004000L,0x0000000000000002L});
        public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000008000L});
        public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000012BF0000L});
        public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000012BE0000L});
        public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000080010L});
        public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000012B80000L});
        public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000010B80000L});
        public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000010A80000L});
        public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000480000L});
        public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000010880000L});
        public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000001000002L});
        public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000004080010L});
        public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000008000000L});
        public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000013000080000L,0x0000000000700000L});
        public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000010000080000L,0x0000000000700000L});
        public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000020000000L});
        public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000040400000L});
        public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000080000000L});
        public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000100000000L});
        public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x00000000000000F0L});
        public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000200000000L});
        public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000400000000L});
        public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000000000060L});
        public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000000400002L});
        public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000000000080L});
        public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000800000002L});
        public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000008000000000L});
        public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x000000C000000000L});
        public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000010000000000L,0x0000000000700000L});
        public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000020000000000L});
        public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x00003C0000008000L});
        public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000080000000000L});
        public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x00003C0000408000L});
        public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000000200000010L});
        public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x1F68FC0000088010L,0x0000000003800038L});
        public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x2000000000000002L});
        public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0200800000000010L,0x0000000003800000L});
        public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0200800000000000L,0x0000000003800000L});
        public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0001000000000000L});
        public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000000800000000L});
        public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0000000820000002L});
        public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000000000000000L,0x0000003C00000000L});
        public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000000020000002L});
        public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x00060000600002F0L,0x00000000000D0000L});
        public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x00060000200002F0L,0x00000000000D0000L});
        public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x00003C0100008000L});
        public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0010000000000002L});
        public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0080000000000002L});
        public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0200400100000000L});
        public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x0200000100000000L});
        public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x00000000200002F0L,0x00000000000D0000L});
        public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x0100000000000000L});
        public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x0800000000000000L});
        public static final BitSet FOLLOW_58 = new BitSet(new long[]{0x0400000000000000L});
        public static final BitSet FOLLOW_59 = new BitSet(new long[]{0xC400000200000000L,0x0000000000000001L});
        public static final BitSet FOLLOW_60 = new BitSet(new long[]{0x0000000820000000L});
        public static final BitSet FOLLOW_61 = new BitSet(new long[]{0x1F68FC00200882F0L,0x00000000038D0038L});
        public static final BitSet FOLLOW_62 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
        public static final BitSet FOLLOW_63 = new BitSet(new long[]{0x0000000080000002L});
        public static final BitSet FOLLOW_64 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000040L});
        public static final BitSet FOLLOW_65 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000080L});
        public static final BitSet FOLLOW_66 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000100L});
        public static final BitSet FOLLOW_67 = new BitSet(new long[]{0x0000000000000002L,0x0000000000007E00L});
        public static final BitSet FOLLOW_68 = new BitSet(new long[]{0x0000000000000000L,0x0000000000007A00L});
        public static final BitSet FOLLOW_69 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_70 = new BitSet(new long[]{0x0000000000000002L,0x0000000000018000L});
        public static final BitSet FOLLOW_71 = new BitSet(new long[]{0x0002000000000002L,0x0000000000020000L});
        public static final BitSet FOLLOW_72 = new BitSet(new long[]{0x0000000040000000L});
        public static final BitSet FOLLOW_73 = new BitSet(new long[]{0x0000000001000000L});
        public static final BitSet FOLLOW_74 = new BitSet(new long[]{0x00000000600002F0L,0x00000000000D0000L});
        public static final BitSet FOLLOW_75 = new BitSet(new long[]{0x0000000000000000L,0x00000003FC000000L});
    }


}