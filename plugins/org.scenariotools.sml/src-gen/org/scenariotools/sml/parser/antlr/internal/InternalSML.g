/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
grammar InternalSML;

options {
	superClass=AbstractInternalAntlrParser;
	
}

@lexer::header {
package org.scenariotools.sml.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;
}

@parser::header {
package org.scenariotools.sml.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.scenariotools.sml.services.SMLGrammarAccess;

}

@parser::members {

 	private SMLGrammarAccess grammarAccess;
 	
    public InternalSMLParser(TokenStream input, SMLGrammarAccess grammarAccess) {
        this(input);
        this.grammarAccess = grammarAccess;
        registerRules(grammarAccess.getGrammar());
    }
    
    @Override
    protected String getFirstRuleName() {
    	return "Specification";	
   	}
   	
   	@Override
   	protected SMLGrammarAccess getGrammarAccess() {
   		return grammarAccess;
   	}
}

@rulecatch { 
    catch (RecognitionException re) { 
        recover(input,re); 
        appendSkippedTokens();
    } 
}




// Entry rule entryRuleSpecification
entryRuleSpecification returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getSpecificationRule()); }
	 iv_ruleSpecification=ruleSpecification 
	 { $current=$iv_ruleSpecification.current; } 
	 EOF 
;

// Rule Specification
ruleSpecification returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		{ 
	        newCompositeNode(grammarAccess.getSpecificationAccess().getImportsImportParserRuleCall_0_0()); 
	    }
		lv_imports_0_0=ruleImport		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getSpecificationRule());
	        }
       		add(
       			$current, 
       			"imports",
        		lv_imports_0_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.Import");
	        afterParserOrEnumRuleCall();
	    }

)
)*	otherlv_1='specification' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getSpecificationAccess().getSpecificationKeyword_1());
    }
(
(
		lv_name_2_0=RULE_ID
		{
			newLeafNode(lv_name_2_0, grammarAccess.getSpecificationAccess().getNameIDTerminalRuleCall_2_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getSpecificationRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"name",
        		lv_name_2_0, 
        		"org.eclipse.xtext.common.Terminals.ID");
	    }

)
)	otherlv_3='{' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getSpecificationAccess().getLeftCurlyBracketKeyword_3());
    }
((	'domain' 
)=>(	otherlv_4='domain' 
    {
    	newLeafNode(otherlv_4, grammarAccess.getSpecificationAccess().getDomainKeyword_4_0_0());
    }
(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getSpecificationRule());
	        }
        }
		{ 
	        newCompositeNode(grammarAccess.getSpecificationAccess().getDomainsEPackageCrossReference_4_0_1_0()); 
	    }
		ruleFQN		{ 
	        afterParserOrEnumRuleCall();
	    }

)
)))*(	otherlv_6='contexts' 
    {
    	newLeafNode(otherlv_6, grammarAccess.getSpecificationAccess().getContextsKeyword_5_0());
    }
(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getSpecificationRule());
	        }
        }
		{ 
	        newCompositeNode(grammarAccess.getSpecificationAccess().getContextsEPackageCrossReference_5_1_0()); 
	    }
		ruleFQN		{ 
	        afterParserOrEnumRuleCall();
	    }

)
))*(	otherlv_8='controllable' 
    {
    	newLeafNode(otherlv_8, grammarAccess.getSpecificationAccess().getControllableKeyword_6_0());
    }
	otherlv_9='{' 
    {
    	newLeafNode(otherlv_9, grammarAccess.getSpecificationAccess().getLeftCurlyBracketKeyword_6_1());
    }
(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getSpecificationRule());
	        }
        }
	otherlv_10=RULE_ID
	{
		newLeafNode(otherlv_10, grammarAccess.getSpecificationAccess().getControllableEClassesEClassCrossReference_6_2_0()); 
	}

)
)*	otherlv_11='}' 
    {
    	newLeafNode(otherlv_11, grammarAccess.getSpecificationAccess().getRightCurlyBracketKeyword_6_3());
    }
)?(
(
		{ 
	        newCompositeNode(grammarAccess.getSpecificationAccess().getChannelOptionsChannelOptionsParserRuleCall_7_0()); 
	    }
		lv_channelOptions_12_0=ruleChannelOptions		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getSpecificationRule());
	        }
       		set(
       			$current, 
       			"channelOptions",
        		lv_channelOptions_12_0, 
        		"org.scenariotools.sml.SML.ChannelOptions");
	        afterParserOrEnumRuleCall();
	    }

)
)(	otherlv_13='non-spontaneous events' 
    {
    	newLeafNode(otherlv_13, grammarAccess.getSpecificationAccess().getNonSpontaneousEventsKeyword_8_0());
    }
	otherlv_14='{' 
    {
    	newLeafNode(otherlv_14, grammarAccess.getSpecificationAccess().getLeftCurlyBracketKeyword_8_1());
    }
((
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getSpecificationRule());
	        }
        }
		{ 
	        newCompositeNode(grammarAccess.getSpecificationAccess().getNonSpontaneousOperationsETypedElementCrossReference_8_2_0_0()); 
	    }
		ruleFQN		{ 
	        afterParserOrEnumRuleCall();
	    }

)
)(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getSpecificationRule());
	        }
        }
		{ 
	        newCompositeNode(grammarAccess.getSpecificationAccess().getNonSpontaneousOperationsETypedElementCrossReference_8_2_1_0()); 
	    }
		ruleFQN		{ 
	        afterParserOrEnumRuleCall();
	    }

)
)*)?	otherlv_17='}' 
    {
    	newLeafNode(otherlv_17, grammarAccess.getSpecificationAccess().getRightCurlyBracketKeyword_8_3());
    }
)?(	otherlv_18='parameter ranges' 
    {
    	newLeafNode(otherlv_18, grammarAccess.getSpecificationAccess().getParameterRangesKeyword_9_0());
    }
	otherlv_19='{' 
    {
    	newLeafNode(otherlv_19, grammarAccess.getSpecificationAccess().getLeftCurlyBracketKeyword_9_1());
    }
((
(
		{ 
	        newCompositeNode(grammarAccess.getSpecificationAccess().getEventParameterRangesEventParameterRangesParserRuleCall_9_2_0_0()); 
	    }
		lv_eventParameterRanges_20_0=ruleEventParameterRanges		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getSpecificationRule());
	        }
       		add(
       			$current, 
       			"eventParameterRanges",
        		lv_eventParameterRanges_20_0, 
        		"org.scenariotools.sml.SML.EventParameterRanges");
	        afterParserOrEnumRuleCall();
	    }

)
)(	otherlv_21=',' 
    {
    	newLeafNode(otherlv_21, grammarAccess.getSpecificationAccess().getCommaKeyword_9_2_1_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getSpecificationAccess().getEventParameterRangesEventParameterRangesParserRuleCall_9_2_1_1_0()); 
	    }
		lv_eventParameterRanges_22_0=ruleEventParameterRanges		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getSpecificationRule());
	        }
       		add(
       			$current, 
       			"eventParameterRanges",
        		lv_eventParameterRanges_22_0, 
        		"org.scenariotools.sml.SML.EventParameterRanges");
	        afterParserOrEnumRuleCall();
	    }

)
))*)?	otherlv_23='}' 
    {
    	newLeafNode(otherlv_23, grammarAccess.getSpecificationAccess().getRightCurlyBracketKeyword_9_3());
    }
)?((
(
		{ 
	        newCompositeNode(grammarAccess.getSpecificationAccess().getContainedCollaborationsNestedCollaborationParserRuleCall_10_0_0()); 
	    }
		lv_containedCollaborations_24_0=ruleNestedCollaboration		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getSpecificationRule());
	        }
       		add(
       			$current, 
       			"containedCollaborations",
        		lv_containedCollaborations_24_0, 
        		"org.scenariotools.sml.SML.NestedCollaboration");
	        afterParserOrEnumRuleCall();
	    }

)
)
    |(	otherlv_25='include collaboration' 
    {
    	newLeafNode(otherlv_25, grammarAccess.getSpecificationAccess().getIncludeCollaborationKeyword_10_1_0());
    }
(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getSpecificationRule());
	        }
        }
	otherlv_26=RULE_ID
	{
		newLeafNode(otherlv_26, grammarAccess.getSpecificationAccess().getIncludedCollaborationsCollaborationCrossReference_10_1_1_0()); 
	}

)
)))*	otherlv_27='}' 
    {
    	newLeafNode(otherlv_27, grammarAccess.getSpecificationAccess().getRightCurlyBracketKeyword_11());
    }
)
;





// Entry rule entryRuleFQNENUM
entryRuleFQNENUM returns [String current=null] 
	:
	{ newCompositeNode(grammarAccess.getFQNENUMRule()); } 
	 iv_ruleFQNENUM=ruleFQNENUM 
	 { $current=$iv_ruleFQNENUM.current.getText(); }  
	 EOF 
;

// Rule FQNENUM
ruleFQNENUM returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(    this_ID_0=RULE_ID    {
		$current.merge(this_ID_0);
    }

    { 
    newLeafNode(this_ID_0, grammarAccess.getFQNENUMAccess().getIDTerminalRuleCall_0()); 
    }
(
	kw=':' 
    {
        $current.merge(kw);
        newLeafNode(kw, grammarAccess.getFQNENUMAccess().getColonKeyword_1_0()); 
    }
    this_ID_2=RULE_ID    {
		$current.merge(this_ID_2);
    }

    { 
    newLeafNode(this_ID_2, grammarAccess.getFQNENUMAccess().getIDTerminalRuleCall_1_1()); 
    }
)*)
    ;





// Entry rule entryRuleChannelOptions
entryRuleChannelOptions returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getChannelOptionsRule()); }
	 iv_ruleChannelOptions=ruleChannelOptions 
	 { $current=$iv_ruleChannelOptions.current; } 
	 EOF 
;

// Rule ChannelOptions
ruleChannelOptions returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getChannelOptionsAccess().getChannelOptionsAction_0(),
            $current);
    }
)(	otherlv_1='channels' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getChannelOptionsAccess().getChannelsKeyword_1_0());
    }
	otherlv_2='{' 
    {
    	newLeafNode(otherlv_2, grammarAccess.getChannelOptionsAccess().getLeftCurlyBracketKeyword_1_1());
    }
(
(
		lv_allMessagesRequireLinks_3_0=	'all messages require links' 
    {
        newLeafNode(lv_allMessagesRequireLinks_3_0, grammarAccess.getChannelOptionsAccess().getAllMessagesRequireLinksAllMessagesRequireLinksKeyword_1_2_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getChannelOptionsRule());
	        }
       		setWithLastConsumed($current, "allMessagesRequireLinks", true, "all messages require links");
	    }

)
)?(
(
		{ 
	        newCompositeNode(grammarAccess.getChannelOptionsAccess().getMessageChannelsMessageChannelParserRuleCall_1_3_0()); 
	    }
		lv_messageChannels_4_0=ruleMessageChannel		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getChannelOptionsRule());
	        }
       		add(
       			$current, 
       			"messageChannels",
        		lv_messageChannels_4_0, 
        		"org.scenariotools.sml.SML.MessageChannel");
	        afterParserOrEnumRuleCall();
	    }

)
)*	otherlv_5='}' 
    {
    	newLeafNode(otherlv_5, grammarAccess.getChannelOptionsAccess().getRightCurlyBracketKeyword_1_4());
    }
)?)
;





// Entry rule entryRuleMessageChannel
entryRuleMessageChannel returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getMessageChannelRule()); }
	 iv_ruleMessageChannel=ruleMessageChannel 
	 { $current=$iv_ruleMessageChannel.current; } 
	 EOF 
;

// Rule MessageChannel
ruleMessageChannel returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getMessageChannelRule());
	        }
        }
		{ 
	        newCompositeNode(grammarAccess.getMessageChannelAccess().getEventETypedElementCrossReference_0_0()); 
	    }
		ruleFQN		{ 
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_1='over' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getMessageChannelAccess().getOverKeyword_1());
    }
(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getMessageChannelRule());
	        }
        }
		{ 
	        newCompositeNode(grammarAccess.getMessageChannelAccess().getChannelFeatureEStructuralFeatureCrossReference_2_0()); 
	    }
		ruleFQN		{ 
	        afterParserOrEnumRuleCall();
	    }

)
))
;





// Entry rule entryRuleNestedCollaboration
entryRuleNestedCollaboration returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getNestedCollaborationRule()); }
	 iv_ruleNestedCollaboration=ruleNestedCollaboration 
	 { $current=$iv_ruleNestedCollaboration.current; } 
	 EOF 
;

// Rule NestedCollaboration
ruleNestedCollaboration returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='collaboration' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getNestedCollaborationAccess().getCollaborationKeyword_0());
    }
(
(
		lv_name_1_0=RULE_ID
		{
			newLeafNode(lv_name_1_0, grammarAccess.getNestedCollaborationAccess().getNameIDTerminalRuleCall_1_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getNestedCollaborationRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"name",
        		lv_name_1_0, 
        		"org.eclipse.xtext.common.Terminals.ID");
	    }

)
)	otherlv_2='{' 
    {
    	newLeafNode(otherlv_2, grammarAccess.getNestedCollaborationAccess().getLeftCurlyBracketKeyword_2());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getNestedCollaborationAccess().getRolesRoleParserRuleCall_3_0()); 
	    }
		lv_roles_3_0=ruleRole		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getNestedCollaborationRule());
	        }
       		add(
       			$current, 
       			"roles",
        		lv_roles_3_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.Role");
	        afterParserOrEnumRuleCall();
	    }

)
)*(
(
		{ 
	        newCompositeNode(grammarAccess.getNestedCollaborationAccess().getScenariosScenarioParserRuleCall_4_0()); 
	    }
		lv_scenarios_4_0=ruleScenario		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getNestedCollaborationRule());
	        }
       		add(
       			$current, 
       			"scenarios",
        		lv_scenarios_4_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.Scenario");
	        afterParserOrEnumRuleCall();
	    }

)
)*	otherlv_5='}' 
    {
    	newLeafNode(otherlv_5, grammarAccess.getNestedCollaborationAccess().getRightCurlyBracketKeyword_5());
    }
)
;





// Entry rule entryRuleEventParameterRanges
entryRuleEventParameterRanges returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getEventParameterRangesRule()); }
	 iv_ruleEventParameterRanges=ruleEventParameterRanges 
	 { $current=$iv_ruleEventParameterRanges.current; } 
	 EOF 
;

// Rule EventParameterRanges
ruleEventParameterRanges returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getEventParameterRangesRule());
	        }
        }
		{ 
	        newCompositeNode(grammarAccess.getEventParameterRangesAccess().getEventETypedElementCrossReference_0_0()); 
	    }
		ruleFQN		{ 
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_1='(' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getEventParameterRangesAccess().getLeftParenthesisKeyword_1());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getEventParameterRangesAccess().getRangesForParameterRangesForParameterParserRuleCall_2_0()); 
	    }
		lv_rangesForParameter_2_0=ruleRangesForParameter		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getEventParameterRangesRule());
	        }
       		add(
       			$current, 
       			"rangesForParameter",
        		lv_rangesForParameter_2_0, 
        		"org.scenariotools.sml.SML.RangesForParameter");
	        afterParserOrEnumRuleCall();
	    }

)
)(	otherlv_3=',' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getEventParameterRangesAccess().getCommaKeyword_3_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getEventParameterRangesAccess().getRangesForParameterRangesForParameterParserRuleCall_3_1_0()); 
	    }
		lv_rangesForParameter_4_0=ruleRangesForParameter		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getEventParameterRangesRule());
	        }
       		add(
       			$current, 
       			"rangesForParameter",
        		lv_rangesForParameter_4_0, 
        		"org.scenariotools.sml.SML.RangesForParameter");
	        afterParserOrEnumRuleCall();
	    }

)
))*	otherlv_5=')' 
    {
    	newLeafNode(otherlv_5, grammarAccess.getEventParameterRangesAccess().getRightParenthesisKeyword_4());
    }
)
;





// Entry rule entryRuleRangesForParameter
entryRuleRangesForParameter returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getRangesForParameterRule()); }
	 iv_ruleRangesForParameter=ruleRangesForParameter 
	 { $current=$iv_ruleRangesForParameter.current; } 
	 EOF 
;

// Rule RangesForParameter
ruleRangesForParameter returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getRangesForParameterRule());
	        }
        }
	otherlv_0=RULE_ID
	{
		newLeafNode(otherlv_0, grammarAccess.getRangesForParameterAccess().getParameterETypedElementCrossReference_0_0()); 
	}

)
)	otherlv_1='=' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getRangesForParameterAccess().getEqualsSignKeyword_1());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getRangesForParameterAccess().getRangesAbstractRangesParserRuleCall_2_0()); 
	    }
		lv_ranges_2_0=ruleAbstractRanges		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getRangesForParameterRule());
	        }
       		set(
       			$current, 
       			"ranges",
        		lv_ranges_2_0, 
        		"org.scenariotools.sml.SML.AbstractRanges");
	        afterParserOrEnumRuleCall();
	    }

)
))
;





// Entry rule entryRuleAbstractRanges
entryRuleAbstractRanges returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getAbstractRangesRule()); }
	 iv_ruleAbstractRanges=ruleAbstractRanges 
	 { $current=$iv_ruleAbstractRanges.current; } 
	 EOF 
;

// Rule AbstractRanges
ruleAbstractRanges returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='[' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getAbstractRangesAccess().getLeftSquareBracketKeyword_0());
    }
(
    { 
        newCompositeNode(grammarAccess.getAbstractRangesAccess().getIntegerRangesParserRuleCall_1_0()); 
    }
    this_IntegerRanges_1=ruleIntegerRanges
    { 
        $current = $this_IntegerRanges_1.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getAbstractRangesAccess().getStringRangesParserRuleCall_1_1()); 
    }
    this_StringRanges_2=ruleStringRanges
    { 
        $current = $this_StringRanges_2.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getAbstractRangesAccess().getEnumRangesParserRuleCall_1_2()); 
    }
    this_EnumRanges_3=ruleEnumRanges
    { 
        $current = $this_EnumRanges_3.current; 
        afterParserOrEnumRuleCall();
    }
)	otherlv_4=']' 
    {
    	newLeafNode(otherlv_4, grammarAccess.getAbstractRangesAccess().getRightSquareBracketKeyword_2());
    }
)
;





// Entry rule entryRuleIntegerRanges
entryRuleIntegerRanges returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getIntegerRangesRule()); }
	 iv_ruleIntegerRanges=ruleIntegerRanges 
	 { $current=$iv_ruleIntegerRanges.current; } 
	 EOF 
;

// Rule IntegerRanges
ruleIntegerRanges returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((((
(
(
		lv_min_0_1=RULE_INT
		{
			newLeafNode(lv_min_0_1, grammarAccess.getIntegerRangesAccess().getMinINTTerminalRuleCall_0_0_0_0_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getIntegerRangesRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"min",
        		lv_min_0_1, 
        		"org.eclipse.xtext.common.Terminals.INT");
	    }

    |		lv_min_0_2=RULE_SIGNEDINT
		{
			newLeafNode(lv_min_0_2, grammarAccess.getIntegerRangesAccess().getMinSIGNEDINTTerminalRuleCall_0_0_0_0_1()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getIntegerRangesRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"min",
        		lv_min_0_2, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
	    }

)

)
)	otherlv_1='..' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getIntegerRangesAccess().getFullStopFullStopKeyword_0_0_1());
    }
(
(
(
		lv_max_2_1=RULE_INT
		{
			newLeafNode(lv_max_2_1, grammarAccess.getIntegerRangesAccess().getMaxINTTerminalRuleCall_0_0_2_0_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getIntegerRangesRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"max",
        		lv_max_2_1, 
        		"org.eclipse.xtext.common.Terminals.INT");
	    }

    |		lv_max_2_2=RULE_SIGNEDINT
		{
			newLeafNode(lv_max_2_2, grammarAccess.getIntegerRangesAccess().getMaxSIGNEDINTTerminalRuleCall_0_0_2_0_1()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getIntegerRangesRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"max",
        		lv_max_2_2, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
	    }

)

)
))(	otherlv_3=',' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getIntegerRangesAccess().getCommaKeyword_0_1_0());
    }
((
(
(
		lv_values_4_1=RULE_INT
		{
			newLeafNode(lv_values_4_1, grammarAccess.getIntegerRangesAccess().getValuesINTTerminalRuleCall_0_1_1_0_0_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getIntegerRangesRule());
	        }
       		addWithLastConsumed(
       			$current, 
       			"values",
        		lv_values_4_1, 
        		"org.eclipse.xtext.common.Terminals.INT");
	    }

    |		lv_values_4_2=RULE_SIGNEDINT
		{
			newLeafNode(lv_values_4_2, grammarAccess.getIntegerRangesAccess().getValuesSIGNEDINTTerminalRuleCall_0_1_1_0_0_1()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getIntegerRangesRule());
	        }
       		addWithLastConsumed(
       			$current, 
       			"values",
        		lv_values_4_2, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
	    }

)

)
)(	otherlv_5=',' 
    {
    	newLeafNode(otherlv_5, grammarAccess.getIntegerRangesAccess().getCommaKeyword_0_1_1_1_0());
    }
(
(
(
		lv_values_6_1=RULE_INT
		{
			newLeafNode(lv_values_6_1, grammarAccess.getIntegerRangesAccess().getValuesINTTerminalRuleCall_0_1_1_1_1_0_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getIntegerRangesRule());
	        }
       		addWithLastConsumed(
       			$current, 
       			"values",
        		lv_values_6_1, 
        		"org.eclipse.xtext.common.Terminals.INT");
	    }

    |		lv_values_6_2=RULE_SIGNEDINT
		{
			newLeafNode(lv_values_6_2, grammarAccess.getIntegerRangesAccess().getValuesSIGNEDINTTerminalRuleCall_0_1_1_1_1_0_1()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getIntegerRangesRule());
	        }
       		addWithLastConsumed(
       			$current, 
       			"values",
        		lv_values_6_2, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
	    }

)

)
))*))?)
    |((
(
(
		lv_values_7_1=RULE_INT
		{
			newLeafNode(lv_values_7_1, grammarAccess.getIntegerRangesAccess().getValuesINTTerminalRuleCall_1_0_0_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getIntegerRangesRule());
	        }
       		addWithLastConsumed(
       			$current, 
       			"values",
        		lv_values_7_1, 
        		"org.eclipse.xtext.common.Terminals.INT");
	    }

    |		lv_values_7_2=RULE_SIGNEDINT
		{
			newLeafNode(lv_values_7_2, grammarAccess.getIntegerRangesAccess().getValuesSIGNEDINTTerminalRuleCall_1_0_0_1()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getIntegerRangesRule());
	        }
       		addWithLastConsumed(
       			$current, 
       			"values",
        		lv_values_7_2, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
	    }

)

)
)(	otherlv_8=',' 
    {
    	newLeafNode(otherlv_8, grammarAccess.getIntegerRangesAccess().getCommaKeyword_1_1_0());
    }
(
(
(
		lv_values_9_1=RULE_INT
		{
			newLeafNode(lv_values_9_1, grammarAccess.getIntegerRangesAccess().getValuesINTTerminalRuleCall_1_1_1_0_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getIntegerRangesRule());
	        }
       		addWithLastConsumed(
       			$current, 
       			"values",
        		lv_values_9_1, 
        		"org.eclipse.xtext.common.Terminals.INT");
	    }

    |		lv_values_9_2=RULE_SIGNEDINT
		{
			newLeafNode(lv_values_9_2, grammarAccess.getIntegerRangesAccess().getValuesSIGNEDINTTerminalRuleCall_1_1_1_0_1()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getIntegerRangesRule());
	        }
       		addWithLastConsumed(
       			$current, 
       			"values",
        		lv_values_9_2, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
	    }

)

)
))*))
;





// Entry rule entryRuleStringRanges
entryRuleStringRanges returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getStringRangesRule()); }
	 iv_ruleStringRanges=ruleStringRanges 
	 { $current=$iv_ruleStringRanges.current; } 
	 EOF 
;

// Rule StringRanges
ruleStringRanges returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		lv_values_0_0=RULE_STRING
		{
			newLeafNode(lv_values_0_0, grammarAccess.getStringRangesAccess().getValuesSTRINGTerminalRuleCall_0_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getStringRangesRule());
	        }
       		addWithLastConsumed(
       			$current, 
       			"values",
        		lv_values_0_0, 
        		"org.eclipse.xtext.common.Terminals.STRING");
	    }

)
)(	otherlv_1=',' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getStringRangesAccess().getCommaKeyword_1_0());
    }
(
(
		lv_values_2_0=RULE_STRING
		{
			newLeafNode(lv_values_2_0, grammarAccess.getStringRangesAccess().getValuesSTRINGTerminalRuleCall_1_1_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getStringRangesRule());
	        }
       		addWithLastConsumed(
       			$current, 
       			"values",
        		lv_values_2_0, 
        		"org.eclipse.xtext.common.Terminals.STRING");
	    }

)
))*)
;





// Entry rule entryRuleEnumRanges
entryRuleEnumRanges returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getEnumRangesRule()); }
	 iv_ruleEnumRanges=ruleEnumRanges 
	 { $current=$iv_ruleEnumRanges.current; } 
	 EOF 
;

// Rule EnumRanges
ruleEnumRanges returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getEnumRangesRule());
	        }
        }
		{ 
	        newCompositeNode(grammarAccess.getEnumRangesAccess().getValuesEEnumLiteralCrossReference_0_0()); 
	    }
		ruleFQNENUM		{ 
	        afterParserOrEnumRuleCall();
	    }

)
)(	otherlv_1=',' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getEnumRangesAccess().getCommaKeyword_1_0());
    }
(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getEnumRangesRule());
	        }
        }
		{ 
	        newCompositeNode(grammarAccess.getEnumRangesAccess().getValuesEEnumLiteralCrossReference_1_1_0()); 
	    }
		ruleFQNENUM		{ 
	        afterParserOrEnumRuleCall();
	    }

)
))*)
;







// Entry rule entryRuleFQN
entryRuleFQN returns [String current=null] 
	:
	{ newCompositeNode(grammarAccess.getFQNRule()); } 
	 iv_ruleFQN=ruleFQN 
	 { $current=$iv_ruleFQN.current.getText(); }  
	 EOF 
;

// Rule FQN
ruleFQN returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(    this_ID_0=RULE_ID    {
		$current.merge(this_ID_0);
    }

    { 
    newLeafNode(this_ID_0, grammarAccess.getFQNAccess().getIDTerminalRuleCall_0()); 
    }
(
	kw='.' 
    {
        $current.merge(kw);
        newLeafNode(kw, grammarAccess.getFQNAccess().getFullStopKeyword_1_0()); 
    }
    this_ID_2=RULE_ID    {
		$current.merge(this_ID_2);
    }

    { 
    newLeafNode(this_ID_2, grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1()); 
    }
)*)
    ;





// Entry rule entryRuleRole
entryRuleRole returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getRoleRule()); }
	 iv_ruleRole=ruleRole 
	 { $current=$iv_ruleRole.current; } 
	 EOF 
;

// Rule Role
ruleRole returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(((
(
		lv_static_0_0=	'static' 
    {
        newLeafNode(lv_static_0_0, grammarAccess.getRoleAccess().getStaticStaticKeyword_0_0_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getRoleRule());
	        }
       		setWithLastConsumed($current, "static", true, "static");
	    }

)
)
    |(	otherlv_1='dynamic' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getRoleAccess().getDynamicKeyword_0_1_0());
    }
(
(
		lv_multiRole_2_0=	'multi' 
    {
        newLeafNode(lv_multiRole_2_0, grammarAccess.getRoleAccess().getMultiRoleMultiKeyword_0_1_1_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getRoleRule());
	        }
       		setWithLastConsumed($current, "multiRole", true, "multi");
	    }

)
)?))	otherlv_3='role' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getRoleAccess().getRoleKeyword_1());
    }
(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getRoleRule());
	        }
        }
	otherlv_4=RULE_ID
	{
		newLeafNode(otherlv_4, grammarAccess.getRoleAccess().getTypeEClassCrossReference_2_0()); 
	}

)
)(
(
		lv_name_5_0=RULE_ID
		{
			newLeafNode(lv_name_5_0, grammarAccess.getRoleAccess().getNameIDTerminalRuleCall_3_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getRoleRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"name",
        		lv_name_5_0, 
        		"org.eclipse.xtext.common.Terminals.ID");
	    }

)
))
;





// Entry rule entryRuleScenario
entryRuleScenario returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getScenarioRule()); }
	 iv_ruleScenario=ruleScenario 
	 { $current=$iv_ruleScenario.current; } 
	 EOF 
;

// Rule Scenario
ruleScenario returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		lv_singular_0_0=	'singular' 
    {
        newLeafNode(lv_singular_0_0, grammarAccess.getScenarioAccess().getSingularSingularKeyword_0_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getScenarioRule());
	        }
       		setWithLastConsumed($current, "singular", true, "singular");
	    }

)
)?(
(
		{ 
	        newCompositeNode(grammarAccess.getScenarioAccess().getKindScenarioKindEnumRuleCall_1_0()); 
	    }
		lv_kind_1_0=ruleScenarioKind		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getScenarioRule());
	        }
       		set(
       			$current, 
       			"kind",
        		lv_kind_1_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.ScenarioKind");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_2='scenario' 
    {
    	newLeafNode(otherlv_2, grammarAccess.getScenarioAccess().getScenarioKeyword_2());
    }
(
(
		lv_name_3_0=RULE_ID
		{
			newLeafNode(lv_name_3_0, grammarAccess.getScenarioAccess().getNameIDTerminalRuleCall_3_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getScenarioRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"name",
        		lv_name_3_0, 
        		"org.eclipse.xtext.common.Terminals.ID");
	    }

)
)(((
(
		lv_optimizeCost_4_0=	'optimize' 
    {
        newLeafNode(lv_optimizeCost_4_0, grammarAccess.getScenarioAccess().getOptimizeCostOptimizeKeyword_4_0_0_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getScenarioRule());
	        }
       		setWithLastConsumed($current, "optimizeCost", true, "optimize");
	    }

)
)	otherlv_5='cost' 
    {
    	newLeafNode(otherlv_5, grammarAccess.getScenarioAccess().getCostKeyword_4_0_1());
    }
)
    |(	otherlv_6='cost' 
    {
    	newLeafNode(otherlv_6, grammarAccess.getScenarioAccess().getCostKeyword_4_1_0());
    }
	otherlv_7='[' 
    {
    	newLeafNode(otherlv_7, grammarAccess.getScenarioAccess().getLeftSquareBracketKeyword_4_1_1());
    }
(
(
		lv_cost_8_0=RULE_DOUBLE
		{
			newLeafNode(lv_cost_8_0, grammarAccess.getScenarioAccess().getCostDOUBLETerminalRuleCall_4_1_2_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getScenarioRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"cost",
        		lv_cost_8_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.DOUBLE");
	    }

)
)	otherlv_9=']' 
    {
    	newLeafNode(otherlv_9, grammarAccess.getScenarioAccess().getRightSquareBracketKeyword_4_1_3());
    }
))?(	otherlv_10='context' 
    {
    	newLeafNode(otherlv_10, grammarAccess.getScenarioAccess().getContextKeyword_5_0());
    }
(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getScenarioRule());
	        }
        }
	otherlv_11=RULE_ID
	{
		newLeafNode(otherlv_11, grammarAccess.getScenarioAccess().getContextsEClassCrossReference_5_1_0()); 
	}

)
)(	otherlv_12=',' 
    {
    	newLeafNode(otherlv_12, grammarAccess.getScenarioAccess().getCommaKeyword_5_2_0());
    }
(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getScenarioRule());
	        }
        }
	otherlv_13=RULE_ID
	{
		newLeafNode(otherlv_13, grammarAccess.getScenarioAccess().getContextsEClassCrossReference_5_2_1_0()); 
	}

)
))*)?(	otherlv_14='bindings' 
    {
    	newLeafNode(otherlv_14, grammarAccess.getScenarioAccess().getBindingsKeyword_6_0());
    }
	otherlv_15='[' 
    {
    	newLeafNode(otherlv_15, grammarAccess.getScenarioAccess().getLeftSquareBracketKeyword_6_1());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getScenarioAccess().getRoleBindingsRoleBindingConstraintParserRuleCall_6_2_0()); 
	    }
		lv_roleBindings_16_0=ruleRoleBindingConstraint		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getScenarioRule());
	        }
       		add(
       			$current, 
       			"roleBindings",
        		lv_roleBindings_16_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.RoleBindingConstraint");
	        afterParserOrEnumRuleCall();
	    }

)
)*	otherlv_17=']' 
    {
    	newLeafNode(otherlv_17, grammarAccess.getScenarioAccess().getRightSquareBracketKeyword_6_3());
    }
)?(
(
		{ 
	        newCompositeNode(grammarAccess.getScenarioAccess().getOwnedInteractionInteractionParserRuleCall_7_0()); 
	    }
		lv_ownedInteraction_18_0=ruleInteraction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getScenarioRule());
	        }
       		set(
       			$current, 
       			"ownedInteraction",
        		lv_ownedInteraction_18_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
	        afterParserOrEnumRuleCall();
	    }

)
))
;





// Entry rule entryRuleRoleBindingConstraint
entryRuleRoleBindingConstraint returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getRoleBindingConstraintRule()); }
	 iv_ruleRoleBindingConstraint=ruleRoleBindingConstraint 
	 { $current=$iv_ruleRoleBindingConstraint.current; } 
	 EOF 
;

// Rule RoleBindingConstraint
ruleRoleBindingConstraint returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getRoleBindingConstraintRule());
	        }
        }
	otherlv_0=RULE_ID
	{
		newLeafNode(otherlv_0, grammarAccess.getRoleBindingConstraintAccess().getRoleRoleCrossReference_0_0()); 
	}

)
)	otherlv_1='=' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getRoleBindingConstraintAccess().getEqualsSignKeyword_1());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getRoleBindingConstraintAccess().getBindingExpressionBindingExpressionParserRuleCall_2_0()); 
	    }
		lv_bindingExpression_2_0=ruleBindingExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getRoleBindingConstraintRule());
	        }
       		set(
       			$current, 
       			"bindingExpression",
        		lv_bindingExpression_2_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.BindingExpression");
	        afterParserOrEnumRuleCall();
	    }

)
))
;





// Entry rule entryRuleBindingExpression
entryRuleBindingExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getBindingExpressionRule()); }
	 iv_ruleBindingExpression=ruleBindingExpression 
	 { $current=$iv_ruleBindingExpression.current; } 
	 EOF 
;

// Rule BindingExpression
ruleBindingExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:

    { 
        newCompositeNode(grammarAccess.getBindingExpressionAccess().getFeatureAccessBindingExpressionParserRuleCall()); 
    }
    this_FeatureAccessBindingExpression_0=ruleFeatureAccessBindingExpression
    { 
        $current = $this_FeatureAccessBindingExpression_0.current; 
        afterParserOrEnumRuleCall();
    }

;





// Entry rule entryRuleFeatureAccessBindingExpression
entryRuleFeatureAccessBindingExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getFeatureAccessBindingExpressionRule()); }
	 iv_ruleFeatureAccessBindingExpression=ruleFeatureAccessBindingExpression 
	 { $current=$iv_ruleFeatureAccessBindingExpression.current; } 
	 EOF 
;

// Rule FeatureAccessBindingExpression
ruleFeatureAccessBindingExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
		{ 
	        newCompositeNode(grammarAccess.getFeatureAccessBindingExpressionAccess().getFeatureaccessFeatureAccessParserRuleCall_0()); 
	    }
		lv_featureaccess_0_0=ruleFeatureAccess		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFeatureAccessBindingExpressionRule());
	        }
       		set(
       			$current, 
       			"featureaccess",
        		lv_featureaccess_0_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.FeatureAccess");
	        afterParserOrEnumRuleCall();
	    }

)
)
;





// Entry rule entryRuleInteractionFragment
entryRuleInteractionFragment returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getInteractionFragmentRule()); }
	 iv_ruleInteractionFragment=ruleInteractionFragment 
	 { $current=$iv_ruleInteractionFragment.current; } 
	 EOF 
;

// Rule InteractionFragment
ruleInteractionFragment returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getInteractionFragmentAccess().getInteractionParserRuleCall_0()); 
    }
    this_Interaction_0=ruleInteraction
    { 
        $current = $this_Interaction_0.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getInteractionFragmentAccess().getModalMessageParserRuleCall_1()); 
    }
    this_ModalMessage_1=ruleModalMessage
    { 
        $current = $this_ModalMessage_1.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getInteractionFragmentAccess().getAlternativeParserRuleCall_2()); 
    }
    this_Alternative_2=ruleAlternative
    { 
        $current = $this_Alternative_2.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getInteractionFragmentAccess().getLoopParserRuleCall_3()); 
    }
    this_Loop_3=ruleLoop
    { 
        $current = $this_Loop_3.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getInteractionFragmentAccess().getParallelParserRuleCall_4()); 
    }
    this_Parallel_4=ruleParallel
    { 
        $current = $this_Parallel_4.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getInteractionFragmentAccess().getConditionFragmentParserRuleCall_5()); 
    }
    this_ConditionFragment_5=ruleConditionFragment
    { 
        $current = $this_ConditionFragment_5.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getInteractionFragmentAccess().getTimedConditionFragmentParserRuleCall_6()); 
    }
    this_TimedConditionFragment_6=ruleTimedConditionFragment
    { 
        $current = $this_TimedConditionFragment_6.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getInteractionFragmentAccess().getVariableFragmentParserRuleCall_7()); 
    }
    this_VariableFragment_7=ruleVariableFragment
    { 
        $current = $this_VariableFragment_7.current; 
        afterParserOrEnumRuleCall();
    }
)
;





// Entry rule entryRuleVariableFragment
entryRuleVariableFragment returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getVariableFragmentRule()); }
	 iv_ruleVariableFragment=ruleVariableFragment 
	 { $current=$iv_ruleVariableFragment.current; } 
	 EOF 
;

// Rule VariableFragment
ruleVariableFragment returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
(
		{ 
	        newCompositeNode(grammarAccess.getVariableFragmentAccess().getExpressionTypedVariableDeclarationParserRuleCall_0_0()); 
	    }
		lv_expression_0_1=ruleTypedVariableDeclaration		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getVariableFragmentRule());
	        }
       		set(
       			$current, 
       			"expression",
        		lv_expression_0_1, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.TypedVariableDeclaration");
	        afterParserOrEnumRuleCall();
	    }

    |		{ 
	        newCompositeNode(grammarAccess.getVariableFragmentAccess().getExpressionVariableAssignmentParserRuleCall_0_1()); 
	    }
		lv_expression_0_2=ruleVariableAssignment		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getVariableFragmentRule());
	        }
       		set(
       			$current, 
       			"expression",
        		lv_expression_0_2, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.VariableAssignment");
	        afterParserOrEnumRuleCall();
	    }

    |		{ 
	        newCompositeNode(grammarAccess.getVariableFragmentAccess().getExpressionClockDeclarationParserRuleCall_0_2()); 
	    }
		lv_expression_0_3=ruleClockDeclaration		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getVariableFragmentRule());
	        }
       		set(
       			$current, 
       			"expression",
        		lv_expression_0_3, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.ClockDeclaration");
	        afterParserOrEnumRuleCall();
	    }

    |		{ 
	        newCompositeNode(grammarAccess.getVariableFragmentAccess().getExpressionClockAssignmentParserRuleCall_0_3()); 
	    }
		lv_expression_0_4=ruleClockAssignment		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getVariableFragmentRule());
	        }
       		set(
       			$current, 
       			"expression",
        		lv_expression_0_4, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.ClockAssignment");
	        afterParserOrEnumRuleCall();
	    }

)

)
)
;





// Entry rule entryRuleInteraction
entryRuleInteraction returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getInteractionRule()); }
	 iv_ruleInteraction=ruleInteraction 
	 { $current=$iv_ruleInteraction.current; } 
	 EOF 
;

// Rule Interaction
ruleInteraction returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getInteractionAccess().getInteractionAction_0(),
            $current);
    }
)	otherlv_1='{' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getInteractionAccess().getLeftCurlyBracketKeyword_1());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getInteractionAccess().getFragmentsInteractionFragmentParserRuleCall_2_0()); 
	    }
		lv_fragments_2_0=ruleInteractionFragment		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getInteractionRule());
	        }
       		add(
       			$current, 
       			"fragments",
        		lv_fragments_2_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.InteractionFragment");
	        afterParserOrEnumRuleCall();
	    }

)
)*	otherlv_3='}' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getInteractionAccess().getRightCurlyBracketKeyword_3());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getInteractionAccess().getConstraintsConstraintBlockParserRuleCall_4_0()); 
	    }
		lv_constraints_4_0=ruleConstraintBlock		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getInteractionRule());
	        }
       		set(
       			$current, 
       			"constraints",
        		lv_constraints_4_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.ConstraintBlock");
	        afterParserOrEnumRuleCall();
	    }

)
)?)
;





// Entry rule entryRuleModalMessage
entryRuleModalMessage returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getModalMessageRule()); }
	 iv_ruleModalMessage=ruleModalMessage 
	 { $current=$iv_ruleModalMessage.current; } 
	 EOF 
;

// Rule ModalMessage
ruleModalMessage returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		lv_strict_0_0=	'strict' 
    {
        newLeafNode(lv_strict_0_0, grammarAccess.getModalMessageAccess().getStrictStrictKeyword_0_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getModalMessageRule());
	        }
       		setWithLastConsumed($current, "strict", true, "strict");
	    }

)
)?((
(
		lv_monitored_1_0=	'monitored' 
    {
        newLeafNode(lv_monitored_1_0, grammarAccess.getModalMessageAccess().getMonitoredMonitoredKeyword_1_0_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getModalMessageRule());
	        }
       		setWithLastConsumed($current, "monitored", true, "monitored");
	    }

)
)?(
(
		{ 
	        newCompositeNode(grammarAccess.getModalMessageAccess().getExpectationKindExpectationKindEnumRuleCall_1_1_0()); 
	    }
		lv_expectationKind_2_0=ruleExpectationKind		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getModalMessageRule());
	        }
       		set(
       			$current, 
       			"expectationKind",
        		lv_expectationKind_2_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.ExpectationKind");
	        afterParserOrEnumRuleCall();
	    }

)
))?(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getModalMessageRule());
	        }
        }
	otherlv_3=RULE_ID
	{
		newLeafNode(otherlv_3, grammarAccess.getModalMessageAccess().getSenderRoleCrossReference_2_0()); 
	}

)
)	otherlv_4='->' 
    {
    	newLeafNode(otherlv_4, grammarAccess.getModalMessageAccess().getHyphenMinusGreaterThanSignKeyword_3());
    }
(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getModalMessageRule());
	        }
        }
	otherlv_5=RULE_ID
	{
		newLeafNode(otherlv_5, grammarAccess.getModalMessageAccess().getReceiverRoleCrossReference_4_0()); 
	}

)
)	otherlv_6='.' 
    {
    	newLeafNode(otherlv_6, grammarAccess.getModalMessageAccess().getFullStopKeyword_5());
    }
(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getModalMessageRule());
	        }
        }
	otherlv_7=RULE_ID
	{
		newLeafNode(otherlv_7, grammarAccess.getModalMessageAccess().getModelElementETypedElementCrossReference_6_0()); 
	}

)
)(	otherlv_8='.' 
    {
    	newLeafNode(otherlv_8, grammarAccess.getModalMessageAccess().getFullStopKeyword_7_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getModalMessageAccess().getCollectionModificationCollectionModificationEnumRuleCall_7_1_0()); 
	    }
		lv_collectionModification_9_0=ruleCollectionModification		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getModalMessageRule());
	        }
       		set(
       			$current, 
       			"collectionModification",
        		lv_collectionModification_9_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionModification");
	        afterParserOrEnumRuleCall();
	    }

)
))?(	otherlv_10='(' 
    {
    	newLeafNode(otherlv_10, grammarAccess.getModalMessageAccess().getLeftParenthesisKeyword_8_0());
    }
((
(
		{ 
	        newCompositeNode(grammarAccess.getModalMessageAccess().getParametersParameterBindingParserRuleCall_8_1_0_0()); 
	    }
		lv_parameters_11_0=ruleParameterBinding		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getModalMessageRule());
	        }
       		add(
       			$current, 
       			"parameters",
        		lv_parameters_11_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
	        afterParserOrEnumRuleCall();
	    }

)
)(	otherlv_12=',' 
    {
    	newLeafNode(otherlv_12, grammarAccess.getModalMessageAccess().getCommaKeyword_8_1_1_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getModalMessageAccess().getParametersParameterBindingParserRuleCall_8_1_1_1_0()); 
	    }
		lv_parameters_13_0=ruleParameterBinding		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getModalMessageRule());
	        }
       		add(
       			$current, 
       			"parameters",
        		lv_parameters_13_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
	        afterParserOrEnumRuleCall();
	    }

)
))*)?	otherlv_14=')' 
    {
    	newLeafNode(otherlv_14, grammarAccess.getModalMessageAccess().getRightParenthesisKeyword_8_2());
    }
)?)
;





// Entry rule entryRuleParameterBinding
entryRuleParameterBinding returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getParameterBindingRule()); }
	 iv_ruleParameterBinding=ruleParameterBinding 
	 { $current=$iv_ruleParameterBinding.current; } 
	 EOF 
;

// Rule ParameterBinding
ruleParameterBinding returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
		{ 
	        newCompositeNode(grammarAccess.getParameterBindingAccess().getBindingExpressionParameterExpressionParserRuleCall_0()); 
	    }
		lv_bindingExpression_0_0=ruleParameterExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getParameterBindingRule());
	        }
       		set(
       			$current, 
       			"bindingExpression",
        		lv_bindingExpression_0_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.ParameterExpression");
	        afterParserOrEnumRuleCall();
	    }

)
)
;





// Entry rule entryRuleParameterExpression
entryRuleParameterExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getParameterExpressionRule()); }
	 iv_ruleParameterExpression=ruleParameterExpression 
	 { $current=$iv_ruleParameterExpression.current; } 
	 EOF 
;

// Rule ParameterExpression
ruleParameterExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getParameterExpressionAccess().getWildcardParameterExpressionParserRuleCall_0()); 
    }
    this_WildcardParameterExpression_0=ruleWildcardParameterExpression
    { 
        $current = $this_WildcardParameterExpression_0.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getParameterExpressionAccess().getValueParameterExpressionParserRuleCall_1()); 
    }
    this_ValueParameterExpression_1=ruleValueParameterExpression
    { 
        $current = $this_ValueParameterExpression_1.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getParameterExpressionAccess().getVariableBindingParameterExpressionParserRuleCall_2()); 
    }
    this_VariableBindingParameterExpression_2=ruleVariableBindingParameterExpression
    { 
        $current = $this_VariableBindingParameterExpression_2.current; 
        afterParserOrEnumRuleCall();
    }
)
;





// Entry rule entryRuleWildcardParameterExpression
entryRuleWildcardParameterExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getWildcardParameterExpressionRule()); }
	 iv_ruleWildcardParameterExpression=ruleWildcardParameterExpression 
	 { $current=$iv_ruleWildcardParameterExpression.current; } 
	 EOF 
;

// Rule WildcardParameterExpression
ruleWildcardParameterExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getWildcardParameterExpressionAccess().getWildcardParameterExpressionAction_0(),
            $current);
    }
)	otherlv_1='*' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getWildcardParameterExpressionAccess().getAsteriskKeyword_1());
    }
)
;





// Entry rule entryRuleValueParameterExpression
entryRuleValueParameterExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getValueParameterExpressionRule()); }
	 iv_ruleValueParameterExpression=ruleValueParameterExpression 
	 { $current=$iv_ruleValueParameterExpression.current; } 
	 EOF 
;

// Rule ValueParameterExpression
ruleValueParameterExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
		{ 
	        newCompositeNode(grammarAccess.getValueParameterExpressionAccess().getValueExpressionParserRuleCall_0()); 
	    }
		lv_value_0_0=ruleExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getValueParameterExpressionRule());
	        }
       		set(
       			$current, 
       			"value",
        		lv_value_0_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
	        afterParserOrEnumRuleCall();
	    }

)
)
;





// Entry rule entryRuleVariableBindingParameterExpression
entryRuleVariableBindingParameterExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getVariableBindingParameterExpressionRule()); }
	 iv_ruleVariableBindingParameterExpression=ruleVariableBindingParameterExpression 
	 { $current=$iv_ruleVariableBindingParameterExpression.current; } 
	 EOF 
;

// Rule VariableBindingParameterExpression
ruleVariableBindingParameterExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='bind' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getVariableBindingParameterExpressionAccess().getBindKeyword_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getVariableBindingParameterExpressionAccess().getVariableVariableValueParserRuleCall_1_0()); 
	    }
		lv_variable_1_0=ruleVariableValue		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getVariableBindingParameterExpressionRule());
	        }
       		set(
       			$current, 
       			"variable",
        		lv_variable_1_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.VariableValue");
	        afterParserOrEnumRuleCall();
	    }

)
))
;





// Entry rule entryRuleAlternative
entryRuleAlternative returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getAlternativeRule()); }
	 iv_ruleAlternative=ruleAlternative 
	 { $current=$iv_ruleAlternative.current; } 
	 EOF 
;

// Rule Alternative
ruleAlternative returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getAlternativeAccess().getAlternativeAction_0(),
            $current);
    }
)	otherlv_1='alternative' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getAlternativeAccess().getAlternativeKeyword_1());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getAlternativeAccess().getCasesCaseParserRuleCall_2_0()); 
	    }
		lv_cases_2_0=ruleCase		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getAlternativeRule());
	        }
       		add(
       			$current, 
       			"cases",
        		lv_cases_2_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.Case");
	        afterParserOrEnumRuleCall();
	    }

)
)(	otherlv_3='or' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getAlternativeAccess().getOrKeyword_3_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getAlternativeAccess().getCasesCaseParserRuleCall_3_1_0()); 
	    }
		lv_cases_4_0=ruleCase		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getAlternativeRule());
	        }
       		add(
       			$current, 
       			"cases",
        		lv_cases_4_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.Case");
	        afterParserOrEnumRuleCall();
	    }

)
))*)
;





// Entry rule entryRuleCase
entryRuleCase returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getCaseRule()); }
	 iv_ruleCase=ruleCase 
	 { $current=$iv_ruleCase.current; } 
	 EOF 
;

// Rule Case
ruleCase returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getCaseAccess().getCaseAction_0(),
            $current);
    }
)(
(
		{ 
	        newCompositeNode(grammarAccess.getCaseAccess().getCaseConditionConditionParserRuleCall_1_0()); 
	    }
		lv_caseCondition_1_0=ruleCondition		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getCaseRule());
	        }
       		set(
       			$current, 
       			"caseCondition",
        		lv_caseCondition_1_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.Condition");
	        afterParserOrEnumRuleCall();
	    }

)
)?(
(
		{ 
	        newCompositeNode(grammarAccess.getCaseAccess().getCaseInteractionInteractionParserRuleCall_2_0()); 
	    }
		lv_caseInteraction_2_0=ruleInteraction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getCaseRule());
	        }
       		set(
       			$current, 
       			"caseInteraction",
        		lv_caseInteraction_2_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
	        afterParserOrEnumRuleCall();
	    }

)
))
;





// Entry rule entryRuleLoop
entryRuleLoop returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getLoopRule()); }
	 iv_ruleLoop=ruleLoop 
	 { $current=$iv_ruleLoop.current; } 
	 EOF 
;

// Rule Loop
ruleLoop returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='while' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getLoopAccess().getWhileKeyword_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getLoopAccess().getLoopConditionConditionParserRuleCall_1_0()); 
	    }
		lv_loopCondition_1_0=ruleCondition		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getLoopRule());
	        }
       		set(
       			$current, 
       			"loopCondition",
        		lv_loopCondition_1_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.Condition");
	        afterParserOrEnumRuleCall();
	    }

)
)?(
(
		{ 
	        newCompositeNode(grammarAccess.getLoopAccess().getBodyInteractionInteractionParserRuleCall_2_0()); 
	    }
		lv_bodyInteraction_2_0=ruleInteraction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getLoopRule());
	        }
       		set(
       			$current, 
       			"bodyInteraction",
        		lv_bodyInteraction_2_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
	        afterParserOrEnumRuleCall();
	    }

)
))
;





// Entry rule entryRuleParallel
entryRuleParallel returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getParallelRule()); }
	 iv_ruleParallel=ruleParallel 
	 { $current=$iv_ruleParallel.current; } 
	 EOF 
;

// Rule Parallel
ruleParallel returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getParallelAccess().getParallelAction_0(),
            $current);
    }
)	otherlv_1='parallel' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getParallelAccess().getParallelKeyword_1());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getParallelAccess().getParallelInteractionInteractionParserRuleCall_2_0()); 
	    }
		lv_parallelInteraction_2_0=ruleInteraction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getParallelRule());
	        }
       		add(
       			$current, 
       			"parallelInteraction",
        		lv_parallelInteraction_2_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
	        afterParserOrEnumRuleCall();
	    }

)
)(	otherlv_3='and' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getParallelAccess().getAndKeyword_3_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getParallelAccess().getParallelInteractionInteractionParserRuleCall_3_1_0()); 
	    }
		lv_parallelInteraction_4_0=ruleInteraction		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getParallelRule());
	        }
       		add(
       			$current, 
       			"parallelInteraction",
        		lv_parallelInteraction_4_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
	        afterParserOrEnumRuleCall();
	    }

)
))*)
;





// Entry rule entryRuleTimedConditionFragment
entryRuleTimedConditionFragment returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getTimedConditionFragmentRule()); }
	 iv_ruleTimedConditionFragment=ruleTimedConditionFragment 
	 { $current=$iv_ruleTimedConditionFragment.current; } 
	 EOF 
;

// Rule TimedConditionFragment
ruleTimedConditionFragment returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getTimedConditionFragmentAccess().getTimedWaitConditionParserRuleCall_0()); 
    }
    this_TimedWaitCondition_0=ruleTimedWaitCondition
    { 
        $current = $this_TimedWaitCondition_0.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getTimedConditionFragmentAccess().getTimedInterruptConditionParserRuleCall_1()); 
    }
    this_TimedInterruptCondition_1=ruleTimedInterruptCondition
    { 
        $current = $this_TimedInterruptCondition_1.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getTimedConditionFragmentAccess().getTimedViolationConditionParserRuleCall_2()); 
    }
    this_TimedViolationCondition_2=ruleTimedViolationCondition
    { 
        $current = $this_TimedViolationCondition_2.current; 
        afterParserOrEnumRuleCall();
    }
)
;





// Entry rule entryRuleConditionFragment
entryRuleConditionFragment returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getConditionFragmentRule()); }
	 iv_ruleConditionFragment=ruleConditionFragment 
	 { $current=$iv_ruleConditionFragment.current; } 
	 EOF 
;

// Rule ConditionFragment
ruleConditionFragment returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getConditionFragmentAccess().getWaitConditionParserRuleCall_0()); 
    }
    this_WaitCondition_0=ruleWaitCondition
    { 
        $current = $this_WaitCondition_0.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getConditionFragmentAccess().getInterruptConditionParserRuleCall_1()); 
    }
    this_InterruptCondition_1=ruleInterruptCondition
    { 
        $current = $this_InterruptCondition_1.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getConditionFragmentAccess().getViolationConditionParserRuleCall_2()); 
    }
    this_ViolationCondition_2=ruleViolationCondition
    { 
        $current = $this_ViolationCondition_2.current; 
        afterParserOrEnumRuleCall();
    }
)
;





// Entry rule entryRuleWaitCondition
entryRuleWaitCondition returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getWaitConditionRule()); }
	 iv_ruleWaitCondition=ruleWaitCondition 
	 { $current=$iv_ruleWaitCondition.current; } 
	 EOF 
;

// Rule WaitCondition
ruleWaitCondition returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='wait' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getWaitConditionAccess().getWaitKeyword_0());
    }
(
(
		lv_strict_1_0=	'strict' 
    {
        newLeafNode(lv_strict_1_0, grammarAccess.getWaitConditionAccess().getStrictStrictKeyword_1_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getWaitConditionRule());
	        }
       		setWithLastConsumed($current, "strict", true, "strict");
	    }

)
)?(
(
		lv_requested_2_0=	'eventually' 
    {
        newLeafNode(lv_requested_2_0, grammarAccess.getWaitConditionAccess().getRequestedEventuallyKeyword_2_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getWaitConditionRule());
	        }
       		setWithLastConsumed($current, "requested", true, "eventually");
	    }

)
)?	otherlv_3='[' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getWaitConditionAccess().getLeftSquareBracketKeyword_3());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getWaitConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_4_0()); 
	    }
		lv_conditionExpression_4_0=ruleConditionExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getWaitConditionRule());
	        }
       		set(
       			$current, 
       			"conditionExpression",
        		lv_conditionExpression_4_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_5=']' 
    {
    	newLeafNode(otherlv_5, grammarAccess.getWaitConditionAccess().getRightSquareBracketKeyword_5());
    }
)
;





// Entry rule entryRuleInterruptCondition
entryRuleInterruptCondition returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getInterruptConditionRule()); }
	 iv_ruleInterruptCondition=ruleInterruptCondition 
	 { $current=$iv_ruleInterruptCondition.current; } 
	 EOF 
;

// Rule InterruptCondition
ruleInterruptCondition returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='interrupt' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getInterruptConditionAccess().getInterruptKeyword_0());
    }
	otherlv_1='[' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getInterruptConditionAccess().getLeftSquareBracketKeyword_1());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getInterruptConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_2_0()); 
	    }
		lv_conditionExpression_2_0=ruleConditionExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getInterruptConditionRule());
	        }
       		set(
       			$current, 
       			"conditionExpression",
        		lv_conditionExpression_2_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_3=']' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getInterruptConditionAccess().getRightSquareBracketKeyword_3());
    }
)
;





// Entry rule entryRuleViolationCondition
entryRuleViolationCondition returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getViolationConditionRule()); }
	 iv_ruleViolationCondition=ruleViolationCondition 
	 { $current=$iv_ruleViolationCondition.current; } 
	 EOF 
;

// Rule ViolationCondition
ruleViolationCondition returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='violation' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getViolationConditionAccess().getViolationKeyword_0());
    }
	otherlv_1='[' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getViolationConditionAccess().getLeftSquareBracketKeyword_1());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getViolationConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_2_0()); 
	    }
		lv_conditionExpression_2_0=ruleConditionExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getViolationConditionRule());
	        }
       		set(
       			$current, 
       			"conditionExpression",
        		lv_conditionExpression_2_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_3=']' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getViolationConditionAccess().getRightSquareBracketKeyword_3());
    }
)
;





// Entry rule entryRuleTimedWaitCondition
entryRuleTimedWaitCondition returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getTimedWaitConditionRule()); }
	 iv_ruleTimedWaitCondition=ruleTimedWaitCondition 
	 { $current=$iv_ruleTimedWaitCondition.current; } 
	 EOF 
;

// Rule TimedWaitCondition
ruleTimedWaitCondition returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='timed' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getTimedWaitConditionAccess().getTimedKeyword_0());
    }
	otherlv_1='wait' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getTimedWaitConditionAccess().getWaitKeyword_1());
    }
(
(
		lv_strict_2_0=	'strict' 
    {
        newLeafNode(lv_strict_2_0, grammarAccess.getTimedWaitConditionAccess().getStrictStrictKeyword_2_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getTimedWaitConditionRule());
	        }
       		setWithLastConsumed($current, "strict", true, "strict");
	    }

)
)?(
(
		lv_requested_3_0=	'eventually' 
    {
        newLeafNode(lv_requested_3_0, grammarAccess.getTimedWaitConditionAccess().getRequestedEventuallyKeyword_3_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getTimedWaitConditionRule());
	        }
       		setWithLastConsumed($current, "requested", true, "eventually");
	    }

)
)?	otherlv_4='[' 
    {
    	newLeafNode(otherlv_4, grammarAccess.getTimedWaitConditionAccess().getLeftSquareBracketKeyword_4());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getTimedWaitConditionAccess().getTimedConditionExpressionTimedExpressionParserRuleCall_5_0()); 
	    }
		lv_timedConditionExpression_5_0=ruleTimedExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getTimedWaitConditionRule());
	        }
       		set(
       			$current, 
       			"timedConditionExpression",
        		lv_timedConditionExpression_5_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.TimedExpression");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_6=']' 
    {
    	newLeafNode(otherlv_6, grammarAccess.getTimedWaitConditionAccess().getRightSquareBracketKeyword_6());
    }
)
;





// Entry rule entryRuleTimedViolationCondition
entryRuleTimedViolationCondition returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getTimedViolationConditionRule()); }
	 iv_ruleTimedViolationCondition=ruleTimedViolationCondition 
	 { $current=$iv_ruleTimedViolationCondition.current; } 
	 EOF 
;

// Rule TimedViolationCondition
ruleTimedViolationCondition returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='timed' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getTimedViolationConditionAccess().getTimedKeyword_0());
    }
	otherlv_1='violation' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getTimedViolationConditionAccess().getViolationKeyword_1());
    }
	otherlv_2='[' 
    {
    	newLeafNode(otherlv_2, grammarAccess.getTimedViolationConditionAccess().getLeftSquareBracketKeyword_2());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getTimedViolationConditionAccess().getTimedConditionExpressionTimedExpressionParserRuleCall_3_0()); 
	    }
		lv_timedConditionExpression_3_0=ruleTimedExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getTimedViolationConditionRule());
	        }
       		set(
       			$current, 
       			"timedConditionExpression",
        		lv_timedConditionExpression_3_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.TimedExpression");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_4=']' 
    {
    	newLeafNode(otherlv_4, grammarAccess.getTimedViolationConditionAccess().getRightSquareBracketKeyword_4());
    }
)
;





// Entry rule entryRuleTimedInterruptCondition
entryRuleTimedInterruptCondition returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getTimedInterruptConditionRule()); }
	 iv_ruleTimedInterruptCondition=ruleTimedInterruptCondition 
	 { $current=$iv_ruleTimedInterruptCondition.current; } 
	 EOF 
;

// Rule TimedInterruptCondition
ruleTimedInterruptCondition returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='timed' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getTimedInterruptConditionAccess().getTimedKeyword_0());
    }
	otherlv_1='interrupt' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getTimedInterruptConditionAccess().getInterruptKeyword_1());
    }
	otherlv_2='[' 
    {
    	newLeafNode(otherlv_2, grammarAccess.getTimedInterruptConditionAccess().getLeftSquareBracketKeyword_2());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getTimedInterruptConditionAccess().getTimedConditionExpressionTimedExpressionParserRuleCall_3_0()); 
	    }
		lv_timedConditionExpression_3_0=ruleTimedExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getTimedInterruptConditionRule());
	        }
       		set(
       			$current, 
       			"timedConditionExpression",
        		lv_timedConditionExpression_3_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.TimedExpression");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_4=']' 
    {
    	newLeafNode(otherlv_4, grammarAccess.getTimedInterruptConditionAccess().getRightSquareBracketKeyword_4());
    }
)
;





// Entry rule entryRuleCondition
entryRuleCondition returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getConditionRule()); }
	 iv_ruleCondition=ruleCondition 
	 { $current=$iv_ruleCondition.current; } 
	 EOF 
;

// Rule Condition
ruleCondition returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='[' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getConditionAccess().getLeftSquareBracketKeyword_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_1_0()); 
	    }
		lv_conditionExpression_1_0=ruleConditionExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getConditionRule());
	        }
       		set(
       			$current, 
       			"conditionExpression",
        		lv_conditionExpression_1_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_2=']' 
    {
    	newLeafNode(otherlv_2, grammarAccess.getConditionAccess().getRightSquareBracketKeyword_2());
    }
)
;





// Entry rule entryRuleConditionExpression
entryRuleConditionExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getConditionExpressionRule()); }
	 iv_ruleConditionExpression=ruleConditionExpression 
	 { $current=$iv_ruleConditionExpression.current; } 
	 EOF 
;

// Rule ConditionExpression
ruleConditionExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
		{ 
	        newCompositeNode(grammarAccess.getConditionExpressionAccess().getExpressionExpressionParserRuleCall_0()); 
	    }
		lv_expression_0_0=ruleExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getConditionExpressionRule());
	        }
       		set(
       			$current, 
       			"expression",
        		lv_expression_0_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
	        afterParserOrEnumRuleCall();
	    }

)
)
;





// Entry rule entryRuleConstraintBlock
entryRuleConstraintBlock returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getConstraintBlockRule()); }
	 iv_ruleConstraintBlock=ruleConstraintBlock 
	 { $current=$iv_ruleConstraintBlock.current; } 
	 EOF 
;

// Rule ConstraintBlock
ruleConstraintBlock returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getConstraintBlockAccess().getConstraintBlockAction_0(),
            $current);
    }
)	otherlv_1='constraints' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getConstraintBlockAccess().getConstraintsKeyword_1());
    }
	otherlv_2='[' 
    {
    	newLeafNode(otherlv_2, grammarAccess.getConstraintBlockAccess().getLeftSquareBracketKeyword_2());
    }
((	otherlv_3='consider' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getConstraintBlockAccess().getConsiderKeyword_3_0_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getConstraintBlockAccess().getConsiderConstraintMessageParserRuleCall_3_0_1_0()); 
	    }
		lv_consider_4_0=ruleConstraintMessage		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getConstraintBlockRule());
	        }
       		add(
       			$current, 
       			"consider",
        		lv_consider_4_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
	        afterParserOrEnumRuleCall();
	    }

)
))
    |(	otherlv_5='ignore' 
    {
    	newLeafNode(otherlv_5, grammarAccess.getConstraintBlockAccess().getIgnoreKeyword_3_1_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getConstraintBlockAccess().getIgnoreConstraintMessageParserRuleCall_3_1_1_0()); 
	    }
		lv_ignore_6_0=ruleConstraintMessage		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getConstraintBlockRule());
	        }
       		add(
       			$current, 
       			"ignore",
        		lv_ignore_6_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
	        afterParserOrEnumRuleCall();
	    }

)
))
    |(	otherlv_7='forbidden' 
    {
    	newLeafNode(otherlv_7, grammarAccess.getConstraintBlockAccess().getForbiddenKeyword_3_2_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getConstraintBlockAccess().getForbiddenConstraintMessageParserRuleCall_3_2_1_0()); 
	    }
		lv_forbidden_8_0=ruleConstraintMessage		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getConstraintBlockRule());
	        }
       		add(
       			$current, 
       			"forbidden",
        		lv_forbidden_8_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
	        afterParserOrEnumRuleCall();
	    }

)
))
    |(	otherlv_9='interrupt' 
    {
    	newLeafNode(otherlv_9, grammarAccess.getConstraintBlockAccess().getInterruptKeyword_3_3_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getConstraintBlockAccess().getInterruptConstraintMessageParserRuleCall_3_3_1_0()); 
	    }
		lv_interrupt_10_0=ruleConstraintMessage		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getConstraintBlockRule());
	        }
       		add(
       			$current, 
       			"interrupt",
        		lv_interrupt_10_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
	        afterParserOrEnumRuleCall();
	    }

)
)))*	otherlv_11=']' 
    {
    	newLeafNode(otherlv_11, grammarAccess.getConstraintBlockAccess().getRightSquareBracketKeyword_4());
    }
)
;





// Entry rule entryRuleConstraintMessage
entryRuleConstraintMessage returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getConstraintMessageRule()); }
	 iv_ruleConstraintMessage=ruleConstraintMessage 
	 { $current=$iv_ruleConstraintMessage.current; } 
	 EOF 
;

// Rule ConstraintMessage
ruleConstraintMessage returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getConstraintMessageRule());
	        }
        }
	otherlv_0=RULE_ID
	{
		newLeafNode(otherlv_0, grammarAccess.getConstraintMessageAccess().getSenderRoleCrossReference_0_0()); 
	}

)
)	otherlv_1='->' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getConstraintMessageAccess().getHyphenMinusGreaterThanSignKeyword_1());
    }
(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getConstraintMessageRule());
	        }
        }
	otherlv_2=RULE_ID
	{
		newLeafNode(otherlv_2, grammarAccess.getConstraintMessageAccess().getReceiverRoleCrossReference_2_0()); 
	}

)
)	otherlv_3='.' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getConstraintMessageAccess().getFullStopKeyword_3());
    }
(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getConstraintMessageRule());
	        }
        }
	otherlv_4=RULE_ID
	{
		newLeafNode(otherlv_4, grammarAccess.getConstraintMessageAccess().getModelElementETypedElementCrossReference_4_0()); 
	}

)
)(	otherlv_5='.' 
    {
    	newLeafNode(otherlv_5, grammarAccess.getConstraintMessageAccess().getFullStopKeyword_5_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getConstraintMessageAccess().getCollectionModificationCollectionModificationEnumRuleCall_5_1_0()); 
	    }
		lv_collectionModification_6_0=ruleCollectionModification		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getConstraintMessageRule());
	        }
       		set(
       			$current, 
       			"collectionModification",
        		lv_collectionModification_6_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionModification");
	        afterParserOrEnumRuleCall();
	    }

)
))?	otherlv_7='(' 
    {
    	newLeafNode(otherlv_7, grammarAccess.getConstraintMessageAccess().getLeftParenthesisKeyword_6());
    }
((
(
		{ 
	        newCompositeNode(grammarAccess.getConstraintMessageAccess().getParametersParameterBindingParserRuleCall_7_0_0()); 
	    }
		lv_parameters_8_0=ruleParameterBinding		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getConstraintMessageRule());
	        }
       		add(
       			$current, 
       			"parameters",
        		lv_parameters_8_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
	        afterParserOrEnumRuleCall();
	    }

)
)(	otherlv_9=',' 
    {
    	newLeafNode(otherlv_9, grammarAccess.getConstraintMessageAccess().getCommaKeyword_7_1_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getConstraintMessageAccess().getParametersParameterBindingParserRuleCall_7_1_1_0()); 
	    }
		lv_parameters_10_0=ruleParameterBinding		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getConstraintMessageRule());
	        }
       		add(
       			$current, 
       			"parameters",
        		lv_parameters_10_0, 
        		"org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
	        afterParserOrEnumRuleCall();
	    }

)
))*)?	otherlv_11=')' 
    {
    	newLeafNode(otherlv_11, grammarAccess.getConstraintMessageAccess().getRightParenthesisKeyword_8());
    }
)
;







// Entry rule entryRuleImport
entryRuleImport returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getImportRule()); }
	 iv_ruleImport=ruleImport 
	 { $current=$iv_ruleImport.current; } 
	 EOF 
;

// Rule Import
ruleImport returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='import' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
    }
(
(
		lv_importURI_1_0=RULE_STRING
		{
			newLeafNode(lv_importURI_1_0, grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getImportRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"importURI",
        		lv_importURI_1_0, 
        		"org.eclipse.xtext.common.Terminals.STRING");
	    }

)
))
;





// Entry rule entryRuleExpressionRegion
entryRuleExpressionRegion returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getExpressionRegionRule()); }
	 iv_ruleExpressionRegion=ruleExpressionRegion 
	 { $current=$iv_ruleExpressionRegion.current; } 
	 EOF 
;

// Rule ExpressionRegion
ruleExpressionRegion returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getExpressionRegionAccess().getExpressionRegionAction_0(),
            $current);
    }
)	otherlv_1='{' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getExpressionRegionAccess().getLeftCurlyBracketKeyword_1());
    }
((
(
		{ 
	        newCompositeNode(grammarAccess.getExpressionRegionAccess().getExpressionsExpressionOrRegionParserRuleCall_2_0_0()); 
	    }
		lv_expressions_2_0=ruleExpressionOrRegion		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getExpressionRegionRule());
	        }
       		add(
       			$current, 
       			"expressions",
        		lv_expressions_2_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.ExpressionOrRegion");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_3=';' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getExpressionRegionAccess().getSemicolonKeyword_2_1());
    }
)*	otherlv_4='}' 
    {
    	newLeafNode(otherlv_4, grammarAccess.getExpressionRegionAccess().getRightCurlyBracketKeyword_3());
    }
)
;





// Entry rule entryRuleExpressionOrRegion
entryRuleExpressionOrRegion returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getExpressionOrRegionRule()); }
	 iv_ruleExpressionOrRegion=ruleExpressionOrRegion 
	 { $current=$iv_ruleExpressionOrRegion.current; } 
	 EOF 
;

// Rule ExpressionOrRegion
ruleExpressionOrRegion returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getExpressionOrRegionAccess().getExpressionRegionParserRuleCall_0()); 
    }
    this_ExpressionRegion_0=ruleExpressionRegion
    { 
        $current = $this_ExpressionRegion_0.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getExpressionOrRegionAccess().getExpressionAndVariablesParserRuleCall_1()); 
    }
    this_ExpressionAndVariables_1=ruleExpressionAndVariables
    { 
        $current = $this_ExpressionAndVariables_1.current; 
        afterParserOrEnumRuleCall();
    }
)
;





// Entry rule entryRuleExpressionAndVariables
entryRuleExpressionAndVariables returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getExpressionAndVariablesRule()); }
	 iv_ruleExpressionAndVariables=ruleExpressionAndVariables 
	 { $current=$iv_ruleExpressionAndVariables.current; } 
	 EOF 
;

// Rule ExpressionAndVariables
ruleExpressionAndVariables returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getExpressionAndVariablesAccess().getVariableExpressionParserRuleCall_0()); 
    }
    this_VariableExpression_0=ruleVariableExpression
    { 
        $current = $this_VariableExpression_0.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getExpressionAndVariablesAccess().getExpressionParserRuleCall_1()); 
    }
    this_Expression_1=ruleExpression
    { 
        $current = $this_Expression_1.current; 
        afterParserOrEnumRuleCall();
    }
)
;





// Entry rule entryRuleVariableExpression
entryRuleVariableExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getVariableExpressionRule()); }
	 iv_ruleVariableExpression=ruleVariableExpression 
	 { $current=$iv_ruleVariableExpression.current; } 
	 EOF 
;

// Rule VariableExpression
ruleVariableExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getVariableExpressionAccess().getTypedVariableDeclarationParserRuleCall_0()); 
    }
    this_TypedVariableDeclaration_0=ruleTypedVariableDeclaration
    { 
        $current = $this_TypedVariableDeclaration_0.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getVariableExpressionAccess().getVariableAssignmentParserRuleCall_1()); 
    }
    this_VariableAssignment_1=ruleVariableAssignment
    { 
        $current = $this_VariableAssignment_1.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getVariableExpressionAccess().getClockDeclarationParserRuleCall_2()); 
    }
    this_ClockDeclaration_2=ruleClockDeclaration
    { 
        $current = $this_ClockDeclaration_2.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getVariableExpressionAccess().getClockAssignmentParserRuleCall_3()); 
    }
    this_ClockAssignment_3=ruleClockAssignment
    { 
        $current = $this_ClockAssignment_3.current; 
        afterParserOrEnumRuleCall();
    }
)
;







// Entry rule entryRuleVariableAssignment
entryRuleVariableAssignment returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getVariableAssignmentRule()); }
	 iv_ruleVariableAssignment=ruleVariableAssignment 
	 { $current=$iv_ruleVariableAssignment.current; } 
	 EOF 
;

// Rule VariableAssignment
ruleVariableAssignment returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getVariableAssignmentRule());
	        }
        }
	otherlv_0=RULE_ID
	{
		newLeafNode(otherlv_0, grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationCrossReference_0_0()); 
	}

)
)	otherlv_1='=' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getVariableAssignmentAccess().getEqualsSignKeyword_1());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getVariableAssignmentAccess().getExpressionExpressionParserRuleCall_2_0()); 
	    }
		lv_expression_2_0=ruleExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getVariableAssignmentRule());
	        }
       		set(
       			$current, 
       			"expression",
        		lv_expression_2_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
	        afterParserOrEnumRuleCall();
	    }

)
))
;





// Entry rule entryRuleTypedVariableDeclaration
entryRuleTypedVariableDeclaration returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getTypedVariableDeclarationRule()); }
	 iv_ruleTypedVariableDeclaration=ruleTypedVariableDeclaration 
	 { $current=$iv_ruleTypedVariableDeclaration.current; } 
	 EOF 
;

// Rule TypedVariableDeclaration
ruleTypedVariableDeclaration returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='var' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getTypedVariableDeclarationAccess().getVarKeyword_0());
    }
(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getTypedVariableDeclarationRule());
	        }
        }
	otherlv_1=RULE_ID
	{
		newLeafNode(otherlv_1, grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierCrossReference_1_0()); 
	}

)
)(
(
		lv_name_2_0=RULE_ID
		{
			newLeafNode(lv_name_2_0, grammarAccess.getTypedVariableDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getTypedVariableDeclarationRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"name",
        		lv_name_2_0, 
        		"org.eclipse.xtext.common.Terminals.ID");
	    }

)
)(	otherlv_3='=' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getTypedVariableDeclarationAccess().getEqualsSignKeyword_3_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getTypedVariableDeclarationAccess().getExpressionExpressionParserRuleCall_3_1_0()); 
	    }
		lv_expression_4_0=ruleExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getTypedVariableDeclarationRule());
	        }
       		set(
       			$current, 
       			"expression",
        		lv_expression_4_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
	        afterParserOrEnumRuleCall();
	    }

)
))?)
;





// Entry rule entryRuleClockDeclaration
entryRuleClockDeclaration returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getClockDeclarationRule()); }
	 iv_ruleClockDeclaration=ruleClockDeclaration 
	 { $current=$iv_ruleClockDeclaration.current; } 
	 EOF 
;

// Rule ClockDeclaration
ruleClockDeclaration returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getClockDeclarationAccess().getClockDeclarationAction_0(),
            $current);
    }
)	otherlv_1='clock' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getClockDeclarationAccess().getClockKeyword_1());
    }
(
(
		lv_name_2_0=RULE_ID
		{
			newLeafNode(lv_name_2_0, grammarAccess.getClockDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getClockDeclarationRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"name",
        		lv_name_2_0, 
        		"org.eclipse.xtext.common.Terminals.ID");
	    }

)
)(	otherlv_3='=' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getClockDeclarationAccess().getEqualsSignKeyword_3_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getClockDeclarationAccess().getExpressionIntegerValueParserRuleCall_3_1_0()); 
	    }
		lv_expression_4_0=ruleIntegerValue		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getClockDeclarationRule());
	        }
       		set(
       			$current, 
       			"expression",
        		lv_expression_4_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.IntegerValue");
	        afterParserOrEnumRuleCall();
	    }

)
))?)
;







// Entry rule entryRuleClockAssignment
entryRuleClockAssignment returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getClockAssignmentRule()); }
	 iv_ruleClockAssignment=ruleClockAssignment 
	 { $current=$iv_ruleClockAssignment.current; } 
	 EOF 
;

// Rule ClockAssignment
ruleClockAssignment returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='reset clock' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getClockAssignmentAccess().getResetClockKeyword_0());
    }
(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getClockAssignmentRule());
	        }
        }
	otherlv_1=RULE_ID
	{
		newLeafNode(otherlv_1, grammarAccess.getClockAssignmentAccess().getVariableClockDeclarationCrossReference_1_0()); 
	}

)
)(	otherlv_2='=' 
    {
    	newLeafNode(otherlv_2, grammarAccess.getClockAssignmentAccess().getEqualsSignKeyword_2_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getClockAssignmentAccess().getExpressionIntegerValueParserRuleCall_2_1_0()); 
	    }
		lv_expression_3_0=ruleIntegerValue		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getClockAssignmentRule());
	        }
       		set(
       			$current, 
       			"expression",
        		lv_expression_3_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.IntegerValue");
	        afterParserOrEnumRuleCall();
	    }

)
))?)
;





// Entry rule entryRuleExpression
entryRuleExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getExpressionRule()); }
	 iv_ruleExpression=ruleExpression 
	 { $current=$iv_ruleExpression.current; } 
	 EOF 
;

// Rule Expression
ruleExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:

    { 
        newCompositeNode(grammarAccess.getExpressionAccess().getImplicationExpressionParserRuleCall()); 
    }
    this_ImplicationExpression_0=ruleImplicationExpression
    { 
        $current = $this_ImplicationExpression_0.current; 
        afterParserOrEnumRuleCall();
    }

;





// Entry rule entryRuleImplicationExpression
entryRuleImplicationExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getImplicationExpressionRule()); }
	 iv_ruleImplicationExpression=ruleImplicationExpression 
	 { $current=$iv_ruleImplicationExpression.current; } 
	 EOF 
;

// Rule ImplicationExpression
ruleImplicationExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getImplicationExpressionAccess().getDisjunctionExpressionParserRuleCall_0()); 
    }
    this_DisjunctionExpression_0=ruleDisjunctionExpression
    { 
        $current = $this_DisjunctionExpression_0.current; 
        afterParserOrEnumRuleCall();
    }
((
    {
        $current = forceCreateModelElementAndSet(
            grammarAccess.getImplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
            $current);
    }
)(
(
		lv_operator_2_0=	'=>' 
    {
        newLeafNode(lv_operator_2_0, grammarAccess.getImplicationExpressionAccess().getOperatorEqualsSignGreaterThanSignKeyword_1_1_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getImplicationExpressionRule());
	        }
       		setWithLastConsumed($current, "operator", lv_operator_2_0, "=>");
	    }

)
)(
(
		{ 
	        newCompositeNode(grammarAccess.getImplicationExpressionAccess().getRightImplicationExpressionParserRuleCall_1_2_0()); 
	    }
		lv_right_3_0=ruleImplicationExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getImplicationExpressionRule());
	        }
       		set(
       			$current, 
       			"right",
        		lv_right_3_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.ImplicationExpression");
	        afterParserOrEnumRuleCall();
	    }

)
))?)
;





// Entry rule entryRuleDisjunctionExpression
entryRuleDisjunctionExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getDisjunctionExpressionRule()); }
	 iv_ruleDisjunctionExpression=ruleDisjunctionExpression 
	 { $current=$iv_ruleDisjunctionExpression.current; } 
	 EOF 
;

// Rule DisjunctionExpression
ruleDisjunctionExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getDisjunctionExpressionAccess().getConjunctionExpressionParserRuleCall_0()); 
    }
    this_ConjunctionExpression_0=ruleConjunctionExpression
    { 
        $current = $this_ConjunctionExpression_0.current; 
        afterParserOrEnumRuleCall();
    }
((
    {
        $current = forceCreateModelElementAndSet(
            grammarAccess.getDisjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
            $current);
    }
)(
(
		lv_operator_2_0=	'||' 
    {
        newLeafNode(lv_operator_2_0, grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineVerticalLineKeyword_1_1_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getDisjunctionExpressionRule());
	        }
       		setWithLastConsumed($current, "operator", lv_operator_2_0, "||");
	    }

)
)(
(
		{ 
	        newCompositeNode(grammarAccess.getDisjunctionExpressionAccess().getRightDisjunctionExpressionParserRuleCall_1_2_0()); 
	    }
		lv_right_3_0=ruleDisjunctionExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getDisjunctionExpressionRule());
	        }
       		set(
       			$current, 
       			"right",
        		lv_right_3_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.DisjunctionExpression");
	        afterParserOrEnumRuleCall();
	    }

)
))?)
;





// Entry rule entryRuleConjunctionExpression
entryRuleConjunctionExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getConjunctionExpressionRule()); }
	 iv_ruleConjunctionExpression=ruleConjunctionExpression 
	 { $current=$iv_ruleConjunctionExpression.current; } 
	 EOF 
;

// Rule ConjunctionExpression
ruleConjunctionExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getConjunctionExpressionAccess().getRelationExpressionParserRuleCall_0()); 
    }
    this_RelationExpression_0=ruleRelationExpression
    { 
        $current = $this_RelationExpression_0.current; 
        afterParserOrEnumRuleCall();
    }
((
    {
        $current = forceCreateModelElementAndSet(
            grammarAccess.getConjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
            $current);
    }
)(
(
		lv_operator_2_0=	'&&' 
    {
        newLeafNode(lv_operator_2_0, grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandAmpersandKeyword_1_1_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getConjunctionExpressionRule());
	        }
       		setWithLastConsumed($current, "operator", lv_operator_2_0, "&&");
	    }

)
)(
(
		{ 
	        newCompositeNode(grammarAccess.getConjunctionExpressionAccess().getRightConjunctionExpressionParserRuleCall_1_2_0()); 
	    }
		lv_right_3_0=ruleConjunctionExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getConjunctionExpressionRule());
	        }
       		set(
       			$current, 
       			"right",
        		lv_right_3_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.ConjunctionExpression");
	        afterParserOrEnumRuleCall();
	    }

)
))?)
;





// Entry rule entryRuleRelationExpression
entryRuleRelationExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getRelationExpressionRule()); }
	 iv_ruleRelationExpression=ruleRelationExpression 
	 { $current=$iv_ruleRelationExpression.current; } 
	 EOF 
;

// Rule RelationExpression
ruleRelationExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getRelationExpressionAccess().getAdditionExpressionParserRuleCall_0()); 
    }
    this_AdditionExpression_0=ruleAdditionExpression
    { 
        $current = $this_AdditionExpression_0.current; 
        afterParserOrEnumRuleCall();
    }
((
    {
        $current = forceCreateModelElementAndSet(
            grammarAccess.getRelationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
            $current);
    }
)(
(
(
		lv_operator_2_1=	'==' 
    {
        newLeafNode(lv_operator_2_1, grammarAccess.getRelationExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_1_0_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getRelationExpressionRule());
	        }
       		setWithLastConsumed($current, "operator", lv_operator_2_1, null);
	    }

    |		lv_operator_2_2=	'!=' 
    {
        newLeafNode(lv_operator_2_2, grammarAccess.getRelationExpressionAccess().getOperatorExclamationMarkEqualsSignKeyword_1_1_0_1());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getRelationExpressionRule());
	        }
       		setWithLastConsumed($current, "operator", lv_operator_2_2, null);
	    }

    |		lv_operator_2_3=	'<' 
    {
        newLeafNode(lv_operator_2_3, grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignKeyword_1_1_0_2());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getRelationExpressionRule());
	        }
       		setWithLastConsumed($current, "operator", lv_operator_2_3, null);
	    }

    |		lv_operator_2_4=	'>' 
    {
        newLeafNode(lv_operator_2_4, grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignKeyword_1_1_0_3());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getRelationExpressionRule());
	        }
       		setWithLastConsumed($current, "operator", lv_operator_2_4, null);
	    }

    |		lv_operator_2_5=	'<=' 
    {
        newLeafNode(lv_operator_2_5, grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_1_0_4());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getRelationExpressionRule());
	        }
       		setWithLastConsumed($current, "operator", lv_operator_2_5, null);
	    }

    |		lv_operator_2_6=	'>=' 
    {
        newLeafNode(lv_operator_2_6, grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_1_0_5());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getRelationExpressionRule());
	        }
       		setWithLastConsumed($current, "operator", lv_operator_2_6, null);
	    }

)

)
)(
(
		{ 
	        newCompositeNode(grammarAccess.getRelationExpressionAccess().getRightRelationExpressionParserRuleCall_1_2_0()); 
	    }
		lv_right_3_0=ruleRelationExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getRelationExpressionRule());
	        }
       		set(
       			$current, 
       			"right",
        		lv_right_3_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.RelationExpression");
	        afterParserOrEnumRuleCall();
	    }

)
))?)
;





// Entry rule entryRuleTimedExpression
entryRuleTimedExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getTimedExpressionRule()); }
	 iv_ruleTimedExpression=ruleTimedExpression 
	 { $current=$iv_ruleTimedExpression.current; } 
	 EOF 
;

// Rule TimedExpression
ruleTimedExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getTimedExpressionRule());
	        }
        }
	otherlv_0=RULE_ID
	{
		newLeafNode(otherlv_0, grammarAccess.getTimedExpressionAccess().getClockClockDeclarationCrossReference_0_0()); 
	}

)
)(
(
(
		lv_operator_1_1=	'==' 
    {
        newLeafNode(lv_operator_1_1, grammarAccess.getTimedExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_0_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getTimedExpressionRule());
	        }
       		setWithLastConsumed($current, "operator", lv_operator_1_1, null);
	    }

    |		lv_operator_1_2=	'<' 
    {
        newLeafNode(lv_operator_1_2, grammarAccess.getTimedExpressionAccess().getOperatorLessThanSignKeyword_1_0_1());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getTimedExpressionRule());
	        }
       		setWithLastConsumed($current, "operator", lv_operator_1_2, null);
	    }

    |		lv_operator_1_3=	'>' 
    {
        newLeafNode(lv_operator_1_3, grammarAccess.getTimedExpressionAccess().getOperatorGreaterThanSignKeyword_1_0_2());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getTimedExpressionRule());
	        }
       		setWithLastConsumed($current, "operator", lv_operator_1_3, null);
	    }

    |		lv_operator_1_4=	'<=' 
    {
        newLeafNode(lv_operator_1_4, grammarAccess.getTimedExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_0_3());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getTimedExpressionRule());
	        }
       		setWithLastConsumed($current, "operator", lv_operator_1_4, null);
	    }

    |		lv_operator_1_5=	'>=' 
    {
        newLeafNode(lv_operator_1_5, grammarAccess.getTimedExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_0_4());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getTimedExpressionRule());
	        }
       		setWithLastConsumed($current, "operator", lv_operator_1_5, null);
	    }

)

)
)(
(
		lv_value_2_0=RULE_INT
		{
			newLeafNode(lv_value_2_0, grammarAccess.getTimedExpressionAccess().getValueINTTerminalRuleCall_2_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getTimedExpressionRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"value",
        		lv_value_2_0, 
        		"org.eclipse.xtext.common.Terminals.INT");
	    }

)
))
;





// Entry rule entryRuleAdditionExpression
entryRuleAdditionExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getAdditionExpressionRule()); }
	 iv_ruleAdditionExpression=ruleAdditionExpression 
	 { $current=$iv_ruleAdditionExpression.current; } 
	 EOF 
;

// Rule AdditionExpression
ruleAdditionExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getAdditionExpressionAccess().getMultiplicationExpressionParserRuleCall_0()); 
    }
    this_MultiplicationExpression_0=ruleMultiplicationExpression
    { 
        $current = $this_MultiplicationExpression_0.current; 
        afterParserOrEnumRuleCall();
    }
((
    {
        $current = forceCreateModelElementAndSet(
            grammarAccess.getAdditionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
            $current);
    }
)(
(
(
		lv_operator_2_1=	'+' 
    {
        newLeafNode(lv_operator_2_1, grammarAccess.getAdditionExpressionAccess().getOperatorPlusSignKeyword_1_1_0_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getAdditionExpressionRule());
	        }
       		setWithLastConsumed($current, "operator", lv_operator_2_1, null);
	    }

    |		lv_operator_2_2=	'-' 
    {
        newLeafNode(lv_operator_2_2, grammarAccess.getAdditionExpressionAccess().getOperatorHyphenMinusKeyword_1_1_0_1());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getAdditionExpressionRule());
	        }
       		setWithLastConsumed($current, "operator", lv_operator_2_2, null);
	    }

)

)
)(
(
		{ 
	        newCompositeNode(grammarAccess.getAdditionExpressionAccess().getRightAdditionExpressionParserRuleCall_1_2_0()); 
	    }
		lv_right_3_0=ruleAdditionExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getAdditionExpressionRule());
	        }
       		set(
       			$current, 
       			"right",
        		lv_right_3_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.AdditionExpression");
	        afterParserOrEnumRuleCall();
	    }

)
))?)
;





// Entry rule entryRuleMultiplicationExpression
entryRuleMultiplicationExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getMultiplicationExpressionRule()); }
	 iv_ruleMultiplicationExpression=ruleMultiplicationExpression 
	 { $current=$iv_ruleMultiplicationExpression.current; } 
	 EOF 
;

// Rule MultiplicationExpression
ruleMultiplicationExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getMultiplicationExpressionAccess().getNegatedExpressionParserRuleCall_0()); 
    }
    this_NegatedExpression_0=ruleNegatedExpression
    { 
        $current = $this_NegatedExpression_0.current; 
        afterParserOrEnumRuleCall();
    }
((
    {
        $current = forceCreateModelElementAndSet(
            grammarAccess.getMultiplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
            $current);
    }
)(
(
(
		lv_operator_2_1=	'*' 
    {
        newLeafNode(lv_operator_2_1, grammarAccess.getMultiplicationExpressionAccess().getOperatorAsteriskKeyword_1_1_0_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getMultiplicationExpressionRule());
	        }
       		setWithLastConsumed($current, "operator", lv_operator_2_1, null);
	    }

    |		lv_operator_2_2=	'/' 
    {
        newLeafNode(lv_operator_2_2, grammarAccess.getMultiplicationExpressionAccess().getOperatorSolidusKeyword_1_1_0_1());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getMultiplicationExpressionRule());
	        }
       		setWithLastConsumed($current, "operator", lv_operator_2_2, null);
	    }

)

)
)(
(
		{ 
	        newCompositeNode(grammarAccess.getMultiplicationExpressionAccess().getRightMultiplicationExpressionParserRuleCall_1_2_0()); 
	    }
		lv_right_3_0=ruleMultiplicationExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getMultiplicationExpressionRule());
	        }
       		set(
       			$current, 
       			"right",
        		lv_right_3_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.MultiplicationExpression");
	        afterParserOrEnumRuleCall();
	    }

)
))?)
;





// Entry rule entryRuleNegatedExpression
entryRuleNegatedExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getNegatedExpressionRule()); }
	 iv_ruleNegatedExpression=ruleNegatedExpression 
	 { $current=$iv_ruleNegatedExpression.current; } 
	 EOF 
;

// Rule NegatedExpression
ruleNegatedExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(((
    {
        $current = forceCreateModelElement(
            grammarAccess.getNegatedExpressionAccess().getUnaryOperationExpressionAction_0_0(),
            $current);
    }
)((
(
(
	'!' 
 

    |			'-' 
 

)

)
)=>
(
(
		lv_operator_1_1=	'!' 
    {
        newLeafNode(lv_operator_1_1, grammarAccess.getNegatedExpressionAccess().getOperatorExclamationMarkKeyword_0_1_0_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getNegatedExpressionRule());
	        }
       		setWithLastConsumed($current, "operator", lv_operator_1_1, null);
	    }

    |		lv_operator_1_2=	'-' 
    {
        newLeafNode(lv_operator_1_2, grammarAccess.getNegatedExpressionAccess().getOperatorHyphenMinusKeyword_0_1_0_1());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getNegatedExpressionRule());
	        }
       		setWithLastConsumed($current, "operator", lv_operator_1_2, null);
	    }

)

)
)(
(
		{ 
	        newCompositeNode(grammarAccess.getNegatedExpressionAccess().getOperandBasicExpressionParserRuleCall_0_2_0()); 
	    }
		lv_operand_2_0=ruleBasicExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getNegatedExpressionRule());
	        }
       		set(
       			$current, 
       			"operand",
        		lv_operand_2_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.BasicExpression");
	        afterParserOrEnumRuleCall();
	    }

)
))
    |
    { 
        newCompositeNode(grammarAccess.getNegatedExpressionAccess().getBasicExpressionParserRuleCall_1()); 
    }
    this_BasicExpression_3=ruleBasicExpression
    { 
        $current = $this_BasicExpression_3.current; 
        afterParserOrEnumRuleCall();
    }
)
;





// Entry rule entryRuleBasicExpression
entryRuleBasicExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getBasicExpressionRule()); }
	 iv_ruleBasicExpression=ruleBasicExpression 
	 { $current=$iv_ruleBasicExpression.current; } 
	 EOF 
;

// Rule BasicExpression
ruleBasicExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getBasicExpressionAccess().getValueParserRuleCall_0()); 
    }
    this_Value_0=ruleValue
    { 
        $current = $this_Value_0.current; 
        afterParserOrEnumRuleCall();
    }

    |(	otherlv_1='(' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0());
    }

    { 
        newCompositeNode(grammarAccess.getBasicExpressionAccess().getExpressionParserRuleCall_1_1()); 
    }
    this_Expression_2=ruleExpression
    { 
        $current = $this_Expression_2.current; 
        afterParserOrEnumRuleCall();
    }
	otherlv_3=')' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getBasicExpressionAccess().getRightParenthesisKeyword_1_2());
    }
))
;





// Entry rule entryRuleValue
entryRuleValue returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getValueRule()); }
	 iv_ruleValue=ruleValue 
	 { $current=$iv_ruleValue.current; } 
	 EOF 
;

// Rule Value
ruleValue returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getValueAccess().getIntegerValueParserRuleCall_0()); 
    }
    this_IntegerValue_0=ruleIntegerValue
    { 
        $current = $this_IntegerValue_0.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getValueAccess().getBooleanValueParserRuleCall_1()); 
    }
    this_BooleanValue_1=ruleBooleanValue
    { 
        $current = $this_BooleanValue_1.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getValueAccess().getStringValueParserRuleCall_2()); 
    }
    this_StringValue_2=ruleStringValue
    { 
        $current = $this_StringValue_2.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getValueAccess().getEnumValueParserRuleCall_3()); 
    }
    this_EnumValue_3=ruleEnumValue
    { 
        $current = $this_EnumValue_3.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getValueAccess().getNullValueParserRuleCall_4()); 
    }
    this_NullValue_4=ruleNullValue
    { 
        $current = $this_NullValue_4.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getValueAccess().getVariableValueParserRuleCall_5()); 
    }
    this_VariableValue_5=ruleVariableValue
    { 
        $current = $this_VariableValue_5.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getValueAccess().getFeatureAccessParserRuleCall_6()); 
    }
    this_FeatureAccess_6=ruleFeatureAccess
    { 
        $current = $this_FeatureAccess_6.current; 
        afterParserOrEnumRuleCall();
    }
)
;





// Entry rule entryRuleIntegerValue
entryRuleIntegerValue returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getIntegerValueRule()); }
	 iv_ruleIntegerValue=ruleIntegerValue 
	 { $current=$iv_ruleIntegerValue.current; } 
	 EOF 
;

// Rule IntegerValue
ruleIntegerValue returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
(
		lv_value_0_1=RULE_INT
		{
			newLeafNode(lv_value_0_1, grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getIntegerValueRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"value",
        		lv_value_0_1, 
        		"org.eclipse.xtext.common.Terminals.INT");
	    }

    |		lv_value_0_2=RULE_SIGNEDINT
		{
			newLeafNode(lv_value_0_2, grammarAccess.getIntegerValueAccess().getValueSIGNEDINTTerminalRuleCall_0_1()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getIntegerValueRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"value",
        		lv_value_0_2, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
	    }

)

)
)
;





// Entry rule entryRuleBooleanValue
entryRuleBooleanValue returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getBooleanValueRule()); }
	 iv_ruleBooleanValue=ruleBooleanValue 
	 { $current=$iv_ruleBooleanValue.current; } 
	 EOF 
;

// Rule BooleanValue
ruleBooleanValue returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
		lv_value_0_0=RULE_BOOL
		{
			newLeafNode(lv_value_0_0, grammarAccess.getBooleanValueAccess().getValueBOOLTerminalRuleCall_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getBooleanValueRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"value",
        		lv_value_0_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.BOOL");
	    }

)
)
;





// Entry rule entryRuleStringValue
entryRuleStringValue returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getStringValueRule()); }
	 iv_ruleStringValue=ruleStringValue 
	 { $current=$iv_ruleStringValue.current; } 
	 EOF 
;

// Rule StringValue
ruleStringValue returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
		lv_value_0_0=RULE_STRING
		{
			newLeafNode(lv_value_0_0, grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getStringValueRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"value",
        		lv_value_0_0, 
        		"org.eclipse.xtext.common.Terminals.STRING");
	    }

)
)
;





// Entry rule entryRuleEnumValue
entryRuleEnumValue returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getEnumValueRule()); }
	 iv_ruleEnumValue=ruleEnumValue 
	 { $current=$iv_ruleEnumValue.current; } 
	 EOF 
;

// Rule EnumValue
ruleEnumValue returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getEnumValueRule());
	        }
        }
	otherlv_0=RULE_ID
	{
		newLeafNode(otherlv_0, grammarAccess.getEnumValueAccess().getTypeEEnumCrossReference_0_0()); 
	}

)
)	otherlv_1=':' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getEnumValueAccess().getColonKeyword_1());
    }
(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getEnumValueRule());
	        }
        }
	otherlv_2=RULE_ID
	{
		newLeafNode(otherlv_2, grammarAccess.getEnumValueAccess().getValueEEnumLiteralCrossReference_2_0()); 
	}

)
))
;





// Entry rule entryRuleNullValue
entryRuleNullValue returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getNullValueRule()); }
	 iv_ruleNullValue=ruleNullValue 
	 { $current=$iv_ruleNullValue.current; } 
	 EOF 
;

// Rule NullValue
ruleNullValue returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getNullValueAccess().getNullValueAction_0(),
            $current);
    }
)	otherlv_1='null' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getNullValueAccess().getNullKeyword_1());
    }
)
;





// Entry rule entryRuleVariableValue
entryRuleVariableValue returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getVariableValueRule()); }
	 iv_ruleVariableValue=ruleVariableValue 
	 { $current=$iv_ruleVariableValue.current; } 
	 EOF 
;

// Rule VariableValue
ruleVariableValue returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getVariableValueRule());
	        }
        }
	otherlv_0=RULE_ID
	{
		newLeafNode(otherlv_0, grammarAccess.getVariableValueAccess().getValueVariableCrossReference_0()); 
	}

)
)
;





// Entry rule entryRuleCollectionAccess
entryRuleCollectionAccess returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getCollectionAccessRule()); }
	 iv_ruleCollectionAccess=ruleCollectionAccess 
	 { $current=$iv_ruleCollectionAccess.current; } 
	 EOF 
;

// Rule CollectionAccess
ruleCollectionAccess returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		{ 
	        newCompositeNode(grammarAccess.getCollectionAccessAccess().getCollectionOperationCollectionOperationEnumRuleCall_0_0()); 
	    }
		lv_collectionOperation_0_0=ruleCollectionOperation		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getCollectionAccessRule());
	        }
       		set(
       			$current, 
       			"collectionOperation",
        		lv_collectionOperation_0_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionOperation");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_1='(' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getCollectionAccessAccess().getLeftParenthesisKeyword_1());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getCollectionAccessAccess().getParameterExpressionParserRuleCall_2_0()); 
	    }
		lv_parameter_2_0=ruleExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getCollectionAccessRule());
	        }
       		set(
       			$current, 
       			"parameter",
        		lv_parameter_2_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
	        afterParserOrEnumRuleCall();
	    }

)
)?	otherlv_3=')' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getCollectionAccessAccess().getRightParenthesisKeyword_3());
    }
)
;





// Entry rule entryRuleFeatureAccess
entryRuleFeatureAccess returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getFeatureAccessRule()); }
	 iv_ruleFeatureAccess=ruleFeatureAccess 
	 { $current=$iv_ruleFeatureAccess.current; } 
	 EOF 
;

// Rule FeatureAccess
ruleFeatureAccess returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getFeatureAccessRule());
	        }
        }
	otherlv_0=RULE_ID
	{
		newLeafNode(otherlv_0, grammarAccess.getFeatureAccessAccess().getTargetEObjectCrossReference_0_0()); 
	}

)
)	otherlv_1='.' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getFeatureAccessAccess().getFullStopKeyword_1());
    }
(((
(
		{ 
	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getValueStructuralFeatureValueParserRuleCall_2_0_0_0()); 
	    }
		lv_value_2_0=ruleStructuralFeatureValue		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
	        }
       		set(
       			$current, 
       			"value",
        		lv_value_2_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.StructuralFeatureValue");
	        afterParserOrEnumRuleCall();
	    }

)
)(	otherlv_3='.' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getFeatureAccessAccess().getFullStopKeyword_2_0_1_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getCollectionAccessCollectionAccessParserRuleCall_2_0_1_1_0()); 
	    }
		lv_collectionAccess_4_0=ruleCollectionAccess		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
	        }
       		set(
       			$current, 
       			"collectionAccess",
        		lv_collectionAccess_4_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionAccess");
	        afterParserOrEnumRuleCall();
	    }

)
))?)
    |((
(
		{ 
	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getValueOperationValueParserRuleCall_2_1_0_0()); 
	    }
		lv_value_5_0=ruleOperationValue		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
	        }
       		set(
       			$current, 
       			"value",
        		lv_value_5_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.OperationValue");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_6='(' 
    {
    	newLeafNode(otherlv_6, grammarAccess.getFeatureAccessAccess().getLeftParenthesisKeyword_2_1_1());
    }
((
(
		{ 
	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getParametersExpressionParserRuleCall_2_1_2_0_0()); 
	    }
		lv_parameters_7_0=ruleExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
	        }
       		add(
       			$current, 
       			"parameters",
        		lv_parameters_7_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
	        afterParserOrEnumRuleCall();
	    }

)
)(	otherlv_8=',' 
    {
    	newLeafNode(otherlv_8, grammarAccess.getFeatureAccessAccess().getCommaKeyword_2_1_2_1_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getParametersExpressionParserRuleCall_2_1_2_1_1_0()); 
	    }
		lv_parameters_9_0=ruleExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
	        }
       		add(
       			$current, 
       			"parameters",
        		lv_parameters_9_0, 
        		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
	        afterParserOrEnumRuleCall();
	    }

)
))*)?	otherlv_10=')' 
    {
    	newLeafNode(otherlv_10, grammarAccess.getFeatureAccessAccess().getRightParenthesisKeyword_2_1_3());
    }
)))
;





// Entry rule entryRuleStructuralFeatureValue
entryRuleStructuralFeatureValue returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getStructuralFeatureValueRule()); }
	 iv_ruleStructuralFeatureValue=ruleStructuralFeatureValue 
	 { $current=$iv_ruleStructuralFeatureValue.current; } 
	 EOF 
;

// Rule StructuralFeatureValue
ruleStructuralFeatureValue returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getStructuralFeatureValueRule());
	        }
        }
	otherlv_0=RULE_ID
	{
		newLeafNode(otherlv_0, grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureCrossReference_0()); 
	}

)
)
;





// Entry rule entryRuleOperationValue
entryRuleOperationValue returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getOperationValueRule()); }
	 iv_ruleOperationValue=ruleOperationValue 
	 { $current=$iv_ruleOperationValue.current; } 
	 EOF 
;

// Rule OperationValue
ruleOperationValue returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getOperationValueRule());
	        }
        }
	otherlv_0=RULE_ID
	{
		newLeafNode(otherlv_0, grammarAccess.getOperationValueAccess().getValueEOperationCrossReference_0()); 
	}

)
)
;





// Rule ScenarioKind
ruleScenarioKind returns [Enumerator current=null] 
    @init { enterRule(); }
    @after { leaveRule(); }:
((	enumLiteral_0='assumption' 
	{
        $current = grammarAccess.getScenarioKindAccess().getAssumptionEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_0, grammarAccess.getScenarioKindAccess().getAssumptionEnumLiteralDeclaration_0()); 
    }
)
    |(	enumLiteral_1='guarantee' 
	{
        $current = grammarAccess.getScenarioKindAccess().getGuaranteeEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_1, grammarAccess.getScenarioKindAccess().getGuaranteeEnumLiteralDeclaration_1()); 
    }
)
    |(	enumLiteral_2='existential' 
	{
        $current = grammarAccess.getScenarioKindAccess().getExistentialEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_2, grammarAccess.getScenarioKindAccess().getExistentialEnumLiteralDeclaration_2()); 
    }
));



// Rule ExpectationKind
ruleExpectationKind returns [Enumerator current=null] 
    @init { enterRule(); }
    @after { leaveRule(); }:
((	enumLiteral_0='eventually' 
	{
        $current = grammarAccess.getExpectationKindAccess().getEventuallyEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_0, grammarAccess.getExpectationKindAccess().getEventuallyEnumLiteralDeclaration_0()); 
    }
)
    |(	enumLiteral_1='requested' 
	{
        $current = grammarAccess.getExpectationKindAccess().getRequestedEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_1, grammarAccess.getExpectationKindAccess().getRequestedEnumLiteralDeclaration_1()); 
    }
)
    |(	enumLiteral_2='urgent' 
	{
        $current = grammarAccess.getExpectationKindAccess().getUrgentEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_2, grammarAccess.getExpectationKindAccess().getUrgentEnumLiteralDeclaration_2()); 
    }
)
    |(	enumLiteral_3='committed' 
	{
        $current = grammarAccess.getExpectationKindAccess().getCommittedEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_3, grammarAccess.getExpectationKindAccess().getCommittedEnumLiteralDeclaration_3()); 
    }
));



// Rule CollectionOperation
ruleCollectionOperation returns [Enumerator current=null] 
    @init { enterRule(); }
    @after { leaveRule(); }:
((	enumLiteral_0='any' 
	{
        $current = grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_0, grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0()); 
    }
)
    |(	enumLiteral_1='contains' 
	{
        $current = grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_1, grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1()); 
    }
)
    |(	enumLiteral_2='containsAll' 
	{
        $current = grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_2, grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2()); 
    }
)
    |(	enumLiteral_3='first' 
	{
        $current = grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_3, grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3()); 
    }
)
    |(	enumLiteral_4='get' 
	{
        $current = grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_4, grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4()); 
    }
)
    |(	enumLiteral_5='isEmpty' 
	{
        $current = grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_5, grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5()); 
    }
)
    |(	enumLiteral_6='last' 
	{
        $current = grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_6, grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6()); 
    }
)
    |(	enumLiteral_7='size' 
	{
        $current = grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_7, grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7()); 
    }
));



// Rule CollectionModification
ruleCollectionModification returns [Enumerator current=null] 
    @init { enterRule(); }
    @after { leaveRule(); }:
((	enumLiteral_0='add' 
	{
        $current = grammarAccess.getCollectionModificationAccess().getAddEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_0, grammarAccess.getCollectionModificationAccess().getAddEnumLiteralDeclaration_0()); 
    }
)
    |(	enumLiteral_1='addToFront' 
	{
        $current = grammarAccess.getCollectionModificationAccess().getAddToFrontEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_1, grammarAccess.getCollectionModificationAccess().getAddToFrontEnumLiteralDeclaration_1()); 
    }
)
    |(	enumLiteral_2='clear' 
	{
        $current = grammarAccess.getCollectionModificationAccess().getClearEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_2, grammarAccess.getCollectionModificationAccess().getClearEnumLiteralDeclaration_2()); 
    }
)
    |(	enumLiteral_3='remove' 
	{
        $current = grammarAccess.getCollectionModificationAccess().getRemoveEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_3, grammarAccess.getCollectionModificationAccess().getRemoveEnumLiteralDeclaration_3()); 
    }
));



RULE_BOOL : ('false'|'true');

RULE_SIGNEDINT : '-' RULE_INT;

RULE_DOUBLE : '-'? RULE_INT? '.' RULE_INT (('e'|'E') '-'? RULE_INT)?;

RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_INT : ('0'..'9')+;

RULE_STRING : ('"' ('\\' .|~(('\\'|'"')))* '"'|'\'' ('\\' .|~(('\\'|'\'')))* '\'');

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;


