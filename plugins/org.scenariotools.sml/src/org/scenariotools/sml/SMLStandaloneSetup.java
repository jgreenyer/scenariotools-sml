/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml;

import org.eclipse.xtext.ecore.EcoreRuntimeModule;

import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class SMLStandaloneSetup extends SMLStandaloneSetupGenerated{

	public static void doSetup() {
		new SMLStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
	
	@Override
	public Injector createInjectorAndDoEMFRegistration() {
		registerEcoreResourceServiceProvider();
		return  super.createInjectorAndDoEMFRegistration();
	}

	private void registerEcoreResourceServiceProvider() {
		if(!org.eclipse.xtext.resource.IResourceServiceProvider.Registry.INSTANCE.getExtensionToFactoryMap().containsKey("ecore")) {
			org.eclipse.xtext.resource.IResourceServiceProvider.Registry.INSTANCE.getExtensionToFactoryMap().put("ecore", Guice.createInjector(new EcoreRuntimeModule()).getInstance(org.eclipse.xtext.resource.IResourceServiceProvider.class));
		}
		
	}
	
}

