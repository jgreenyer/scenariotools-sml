/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.formatting2;

import com.google.inject.Inject;
import org.eclipse.xtext.formatting2.IFormattableDocument;
import org.scenariotools.sml.ChannelOptions;
import org.scenariotools.sml.Collaboration;
import org.scenariotools.sml.EventParameterRanges;
import org.scenariotools.sml.MessageChannel;
import org.scenariotools.sml.RangesForParameter;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.Specification;
import org.scenariotools.sml.collaboration.formatting2.CollaborationFormatter;
import org.scenariotools.sml.expressions.scenarioExpressions.Import;
import org.scenariotools.sml.services.SMLGrammarAccess;
import org.eclipse.xtext.formatting2.regionaccess.ISemanticRegion
import org.scenariotools.sml.SmlPackage
import org.eclipse.emf.ecore.EReference
import org.eclipse.xtext.build.IncrementalBuilder.Result

class SMLFormatter extends CollaborationFormatter {

	@Inject extension SMLGrammarAccess

	def dispatch void format(Specification specification, extension IFormattableDocument document) {
		for (Import imports : specification.getImports()) {
			format(imports, document);
		}
		
		indentInterior(specification, document)
		
		specification.regionFor.keyword("specification").prepend[newLines = 2]
		specification.regionFor.keyword("domain").prepend[newLine]
		
		formatList(specification.regionFor.keyword("controllable"), document)
		formatList(specification.regionFor.keyword("non-spontaneous events"), document)

		format(specification.getChannelOptions(), document);
		for (EventParameterRanges eventParameterRanges : specification.getEventParameterRanges()) {
			format(eventParameterRanges, document);
		}
		
		for (Collaboration containedCollaborations : specification.getContainedCollaborations()) {
			containedCollaborations.interior[indent]
			formatCollaboration(containedCollaborations, document);
		}
		
		specification.semanticRegions.get(specification.semanticRegions.size - 1).surround[newLine]		
	}

	def dispatch void format(ChannelOptions channeloptions, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		for (MessageChannel messageChannels : channeloptions.getMessageChannels()) {
			format(messageChannels, document);
		}
	}

	def dispatch void format(EventParameterRanges eventparameterranges, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		for (RangesForParameter rangesForParameter : eventparameterranges.getRangesForParameter()) {
			format(rangesForParameter, document);
		}
	}

	def dispatch void format(RangesForParameter rangesforparameter, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		format(rangesforparameter.getRanges(), document);
	}

	def void formatList(ISemanticRegion _region, extension IFormattableDocument document) {
		var region = _region
		region.prepend[newLines = 2]
		region.append[oneSpace]
		region = region.nextSemanticRegion.nextSemanticRegion
		while (region.text != "}") {
			region.prepend[newLine]
			region = region.nextSemanticRegion
		}
		region.prepend[newLine]
	}
}
