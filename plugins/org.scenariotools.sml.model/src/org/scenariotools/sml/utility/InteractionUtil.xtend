/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.sml.utility

import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EObject
import org.scenariotools.sml.Interaction
import org.scenariotools.sml.InteractionFragment
import org.scenariotools.sml.ModalMessage
import org.scenariotools.sml.Scenario
import org.scenariotools.sml.VariableFragment
import org.scenariotools.sml.expressions.scenarioExpressions.Variable
import org.scenariotools.sml.expressions.scenarioExpressions.VariableDeclaration
import org.scenariotools.sml.expressions.scenarioExpressions.ClockDeclaration

class InteractionUtil {

	/**
	 * This returns the Interaction containing the EObject given.
	 * Returns null if the EObject is not contained in an Interaction.
	 * 
	 * @param EObject contained in Interaction.
	 * @return Interaction
	 */
	def static Interaction getContainingInteraction(EObject obj) {
		var interaction = obj.eContainer
		while (!(interaction instanceof Interaction)) {
			interaction = interaction.eContainer
			if (interaction == null)
				return null
		}
		return interaction as Interaction
	}

	/**
	 * This returns relevant variables for the given EObject.
	 * Relevant variables are all variables declared before the given EObject.
	 * 
	 * @param EObject contained in Interaction.
	 * @return EList<Variable> of relevant variables.
	 */
	def static EList<Variable> getRelevantVariablesFor(EObject obj) {
		val scope = new BasicEList<Variable>()
		var containingInteraction = getContainingInteraction(obj)
		// Not contained in interaction -> Return empty scope
		if (containingInteraction == null) {
			return scope
		}
		for (InteractionFragment fragment : containingInteraction.fragments) {
			// Input Object reached -> Stop iteration, because after this object are only non declared variables!
			if (fragment == obj || fragment.eAllContents.toList.contains(obj)) {
				// Recursively search for variables in upper interaction
				scope.addAll(getRelevantVariablesFor(containingInteraction))
				return scope
			}
			// Detect Variable
			if (fragment instanceof VariableFragment) {
				val variable = fragment.expression
				if (variable instanceof VariableDeclaration)
					scope.add(variable)
			}
		}
		return scope
	}
	
	/**
	 * This returns relevant clocks for the given EObject.
	 * Relevant clocks are all clocks declared before the given EObject.
	 * 
	 * @param EObject contained in Interaction.
	 * @return EList<Variable> of relevant clocks.
	 */
	def static EList<ClockDeclaration> getRelevantClocksFor(EObject obj) {
		val scope = new BasicEList<ClockDeclaration>()
		var containingInteraction = getContainingInteraction(obj)
		// Not contained in interaction -> Return empty scope
		if (containingInteraction == null) {
			return scope
		}
		for (InteractionFragment fragment : containingInteraction.fragments) {
			// Input Object reached -> Stop iteration, because after this object are only non declared variables!
			if (fragment == obj || fragment.eAllContents.toList.contains(obj)) {
				// Recursively search for variables in upper interaction
				scope.addAll(getRelevantClocksFor(containingInteraction))
				return scope
			}
			// Detect Clock
			if (fragment instanceof VariableFragment) {
				val clock = fragment.expression
				if (clock instanceof ClockDeclaration)
					scope.add(clock)
			}
		}
		return scope
	}

	def static EList<InteractionFragment> getInitializingFragments(Interaction interaction) {
		val initializingFragments = new BasicEList<InteractionFragment>()

		if (interaction != null && interaction.fragments.size > 0) {
			val fragments = interaction.fragments

			for (var i = 0; i < fragments.size; i++) {
				val fragment = fragments.get(i)

				initializingFragments.add(fragment)
				initializingFragments.addAll(InteractionFragmentUtil.getInitializingFragments(fragment))

				val noOtherInitializingFragments = !InteractionFragmentUtil.canHaveInitializingFragmentsBeyond(fragment)

				if (noOtherInitializingFragments)
					return initializingFragments
			}

		}

		return initializingFragments
	}

	def static EList<ModalMessage> getInitializingMessages(Interaction interaction) {
		val initializingMessages = new BasicEList<ModalMessage>()

		getInitializingFragments(interaction).filter[f|f instanceof ModalMessage].forEach [ m |
			initializingMessages.add(m as ModalMessage)
		]

		return initializingMessages
	}

	def static boolean isPrimaryInteractionOfScenario(Interaction interaction) {
		return interaction.eContainer instanceof Scenario
	}

}
