/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.sml.utility

import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EEnum
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EOperation
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.ecore.ETypedElement
import org.scenariotools.sml.ChannelOptions
import org.scenariotools.sml.Specification

class SpecificationUtil {

	def static Specification getContainingSpecification(EObject obj) {
		var specification = obj
		while (!(specification instanceof Specification)) {
			specification = specification.eContainer
			if (specification == null)
				return null
		}
		return specification as Specification
	}

	def static EList<EClass> getEClassesFromAllDomains(Specification specification) {
		val eclassesFromAllDomains = new BasicEList<EClass>()
		specification.domains.forEach [ domain |
			eclassesFromAllDomains.addAll(domain.getEClassifiers.filter(typeof(EClass)))
		]
		return eclassesFromAllDomains
	}

	def static EList<EEnum> getEEnumTypesFromAllDomains(Specification specification) {
		val eenumTypesFromAllDomains = new BasicEList<EEnum>()
		specification.domains.forEach [ domain |
			eenumTypesFromAllDomains.addAll(domain.getEClassifiers.filter(typeof(EEnum)))
		]
		return eenumTypesFromAllDomains
	}

	def static EList<EClass> getEClassesFromAllHelper(Specification specification) {
		val eclassesFromAllDomains = new BasicEList<EClass>()
		specification.contexts.forEach [ context |
			eclassesFromAllDomains.addAll(context.getEClassifiers.filter(typeof(EClass)))
		]
		return eclassesFromAllDomains
	}

	def static EList<EClass> getAllDefinedEClasses(Specification specification) {
		return getEClassesFromAllDomains(specification)
	}
	
	def static EList<EClass> getAllUncontrollableClasses(Specification specification) {
		val uncontrollableClasses = getEClassesFromAllDomains(specification)
		uncontrollableClasses.removeAll(specification.controllableEClasses)
		return uncontrollableClasses
	}

	def static EList<ETypedElement> getEventsFromAllDefinedEClasses(Specification specification) {
		val eventsFromDefinedEClasses = new BasicEList<ETypedElement>()
		getAllDefinedEClasses(specification).forEach [ class |
			eventsFromDefinedEClasses.addAll(class.getEOperations)
			eventsFromDefinedEClasses.addAll(class.getEAllStructuralFeatures)
			class.getEAllSuperTypes.forEach [ c |
				eventsFromDefinedEClasses.addAll(c.getEOperations)
				eventsFromDefinedEClasses.addAll(c.getEAllStructuralFeatures)
			]

		]
		return eventsFromDefinedEClasses
	}

	def static EList<EStructuralFeature> getEStructuralFeaturesFromAllDefinedEClasses(Specification specification) {
		val eventsFromDefinedEClasses = new BasicEList<EStructuralFeature>()
		getAllDefinedEClasses(specification).forEach [ class |
			eventsFromDefinedEClasses.addAll(class.getEAllStructuralFeatures)
			class.getEAllSuperTypes.forEach [ c |
				eventsFromDefinedEClasses.addAll(c.getEAllStructuralFeatures)
			]

		]
		return eventsFromDefinedEClasses
	}

	def static EList<ETypedElement> getParameterizedEventsFromAllDefinedEClasses(Specification specification) {
		val parameterizedEventsFromAllDefinedEClasses = new BasicEList<ETypedElement>
		val eventsFromDefinedEClasses = getEventsFromAllDefinedEClasses(specification)

		eventsFromDefinedEClasses.forEach [ e |
			if (e instanceof EOperation) {
				if (!e.EParameters.empty) {
					parameterizedEventsFromAllDefinedEClasses.add(e)
				}
			} else if (e instanceof EAttribute) {
				parameterizedEventsFromAllDefinedEClasses.add(e)
			}
		]

		return parameterizedEventsFromAllDefinedEClasses
	}

	def static EList<ETypedElement> getEventsWithMessageChannels(Specification specification) {
		val EList<ETypedElement> events = new BasicEList<ETypedElement>()
		val channelOptions = specification.channelOptions as ChannelOptions
		channelOptions.messageChannels.forEach[messageChannel|events.add(messageChannel.event)]
		return events
	}

}
