/**
 * Copyright (c) 2016 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
 package org.scenariotools.sml.utility

import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EObject
import org.scenariotools.sml.Case
import org.scenariotools.sml.ExpectationKind
import org.scenariotools.sml.InteractionFragment
import org.scenariotools.sml.ModalMessage
import org.scenariotools.sml.ParameterBinding
import org.scenariotools.sml.Role
import org.scenariotools.sml.RoleBindingConstraint
import org.scenariotools.sml.Scenario
import org.scenariotools.sml.VariableBindingParameterExpression
import org.scenariotools.sml.expressions.scenarioExpressions.VariableValue

/**
 * This utility class provides several methods for retrieving information about Scenario objects.
 * 
 * @see Scenario
 * @author Florian Koenig
 */
class ScenarioUtil {

	/**
	 * This method retrieves the containing Scenario of the input object.
	 * Returns null if the object is not contained in a scenario.
	 * 
	 * @param obj EObject to retrieve containing scenario from.
	 * @return Containing scenario of input object.
	 */
	def static Scenario getContainingScenario(EObject obj) {
		var scenario = obj
		while (!(scenario instanceof Scenario)) {
			scenario = scenario.eContainer
		}
		return scenario as Scenario
	}

	def static EList<InteractionFragment> getInitializingFragments(Scenario scenario) {
		val initializingFragments = InteractionUtil.getInitializingFragments(scenario.ownedInteraction)

		return initializingFragments
	}

	/**
	 * This method retrieves all initializing messages of an scenario.
	 * A message is initializing if there:
	 *  - is no loop, interaction or condition before the message
	 *  - is no variable fragment with an expression before the message.
	 * 
	 * @see ModalMessage
	 * @param scenario Scenario to retrieve initializing messages for.
	 * @return EList of initializing messages.
	 */
	def static EList<ModalMessage> getInitializingMessages(Scenario scenario) {
		val initializingMessages = InteractionUtil.getInitializingMessages(scenario.ownedInteraction)

		// Initializing messages must not be requested or strict
		initializingMessages.removeAll(initializingMessages.filter[m|m.isStrict || m.expectationKind != ExpectationKind.NONE]) // TODO: Review correctness of m.executionKind != ExecutionKind.NONE. Was isRequested()

		// Initializing messages must not be in a loop
		initializingMessages.removeAll(initializingMessages.filter[m|InteractionFragmentUtil.isContainedInLoop(m)])

		// Initializing messages must not be in an alternative case with condition
		initializingMessages.removeAll(
			initializingMessages.filter [ m |
			val c = InteractionFragmentUtil.getContainingAlternativeCase(m) as Case
			if (c == null)
				return false
			else if (c.caseCondition == null)
				return false
			return true
		])
		return initializingMessages
	}

	/**
	 * This method retrieves all participating roles of an scenario.
	 * 
	 * @see Role
	 * @param scenario Scenario to retrieve participating roles for.
	 * @return EList of participating roles.
	 */
	def static EList<Role> retrieveParticipatingRolesFor(Scenario scenario) {
		val roles = new BasicEList<Role>()
		// Roles in messages
		scenario.eAllContents.toList.filter(typeof(ModalMessage)).forEach [ c |
			if (!roles.contains(c.sender))
				roles.add(c.sender)
			if (!roles.contains(c.receiver))
				roles.add(c.receiver)
		]
		// Roles in expressions
		scenario.eAllContents.toList.filter(typeof(VariableValue)).forEach [ variableValue |
			if (variableValue instanceof VariableValue) {
				val value = variableValue.value
				if (value instanceof Role) {
					if (!roles.contains(value))
						roles.add(value)
				}
			}
		]
		return roles
	}

	/**
	 * This method retrieves all roles of an scenario with a static binding.
	 * 
	 * @see Role
	 * @param scenario Scenario to retrieve roles for.
	 * @return EList of roles.
	 */
	def static EList<Role> retrieveRolesBoundByStatic(Scenario scenario) {
		val EList<Role> roles = new BasicEList<Role>()
		retrieveParticipatingRolesFor(scenario).forEach [ r |
			if (r.static) {
				roles.add(r)
			}
		]
		return roles
	}

	/**
	 * This method retrieves all roles of an scenario that are bound by initializing messages.
	 * 
	 * @see Role
	 * @param scenario Scenario to retrieve roles for.
	 * @return EList of roles.
	 */
	def static EList<Role> retrieveRolesBoundByMessages(Scenario scenario) {
		val EList<Role> roles = new BasicEList<Role>()
		val initMessages = ScenarioUtil.getInitializingMessages(scenario)
		if (initMessages.size > 0) {
			// all initializing messages must have the same sender and receiver
			val m = initMessages.get(0)
			roles.add(m.sender)
			roles.add(m.receiver)
		}
		return roles
	}

	/**
	 * This method retrieves all roles of an scenario that are bound by dynamic role binding expressions.
	 * 
	 * @see Role
	 * @param scenario Scenario to retrieve roles for.
	 * @return EList of roles.
	 */
	def static EList<Role> retrieveRolesBoundByRoleBindings(Scenario scenario) {
		val EList<Role> roles = new BasicEList<Role>()
		val bindings = scenario.roleBindings
		for (RoleBindingConstraint b : bindings) {
			roles.add(b.role)
		}
		return roles
	}

	/**
	 * This method retrieves all roles of an scenario that are bound by parameter role binding expressions in initializing messages.
	 * 
	 * @see Role
	 * @param scenario Scenario to retrieve roles for.
	 * @return EList of roles.
	 */
	def static EList<Role> retrieveRolesBoundByParameterRoleBindingExpressions(Scenario scenario) {
		val EList<Role> roles = new BasicEList<Role>()
		val initMessages = ScenarioUtil.getInitializingMessages(scenario)
		initMessages.forEach[m|
			for (ParameterBinding parameterBinding : m.parameters) {
				val bindingExpression = parameterBinding.bindingExpression
				if (bindingExpression instanceof VariableBindingParameterExpression) {
					val value = bindingExpression.variable.value
					if (value instanceof Role) {
						roles.add(value)
					}
				}
			}
		]
		return roles
	}

	/**
	 * This method checks whether a role is bound in a scenario.
	 * 
	 * @see Role
	 * @param scenario Scenario to check binding for.
	 * @param role Role to check binding for.
	 * @return Boolean role is bound.
	 */
	def static boolean checkRoleIsBoundInScenario(Scenario scenario, Role role) {

		// Check static roles: Static roles are bound
		if (role != null && role.isStatic) {
			return true
		}

		// Check role bindings: bound roles are bound
		val rolesBoundByRoleBindings = retrieveRolesBoundByRoleBindings(scenario)
		if (rolesBoundByRoleBindings.contains(role)) {
			return true
		}

		// Check initial messages: roles from initial messages are bound
		val rolesBoundByMessages = retrieveRolesBoundByMessages(scenario)
		if (rolesBoundByMessages.contains(role)) {
			return true
		}

		// Check role binding expressions from parameters of first messages
		val rolesBoundByParameterRoleBindingExpressions = retrieveRolesBoundByParameterRoleBindingExpressions(scenario)
		if (rolesBoundByParameterRoleBindingExpressions.contains(role)) {
			return true
		}

		return false
	}

}
