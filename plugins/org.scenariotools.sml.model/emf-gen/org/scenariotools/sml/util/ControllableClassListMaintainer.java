/**
 * Copyright (c) 2017 Joel Greenyer and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * ScenarioTools-URL: www.scenariotools.org
 *    
 * Contributors:
 *     ScenarioTools Team - Initial API and implementation
 */
package org.scenariotools.sml.util;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.scenariotools.sml.SmlPackage;
import org.scenariotools.sml.Specification;
import org.scenariotools.sml.utility.SpecificationUtil;

public class ControllableClassListMaintainer extends EContentAdapter {
	private int featureID;
	private EList<EClass> list;
	private Specification specification;
	
	public ControllableClassListMaintainer(int featureID, EList<EClass> list, Specification specification) {
		this.featureID = featureID;
		this.list = list;
		this.specification = specification;
	}
	
	@Override
	public void notifyChanged(Notification notification) {
		if(!(notification.getFeature() instanceof EReference))
			return;		
		if(((EReference)notification.getFeature()).getFeatureID() != featureID)
			return;
		if(notification.getEventType() != Notification.ADD)
			return;

		EClass theClass = list.get(notification.getPosition());
		removeDuplicates(theClass);

		for(EClass otherClass : SpecificationUtil.getEClassesFromAllDomains(specification)) {
			if(otherClass != theClass && theClass.isSuperTypeOf(otherClass) && !list.contains(otherClass)) {
				list.add(otherClass);
			}
		}
	}
	
	private void removeDuplicates(EClass theClass) {
		int count = 0;
		
		for(EClass otherClass : list) {
			if(otherClass == theClass)
				count += 1;
		}
		
		while(count > 1) {
			list.remove(theClass);
			count -= 1;
		}
	}
}
